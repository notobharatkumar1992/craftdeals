package com.craftedeals.adapters;


import android.content.Intent;
import android.graphics.Bitmap;
import android.graphics.drawable.Animatable;
import android.graphics.drawable.AnimationDrawable;
import android.os.Bundle;
import android.os.Handler;
import android.os.Looper;
import android.support.v4.app.ActivityOptionsCompat;
import android.support.v4.app.FragmentActivity;
import android.support.v4.util.Pair;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.LinearLayout;

import com.craftedeals.AppDelegate;
import com.craftedeals.Models.HeroDealsModel;
import com.craftedeals.R;
import com.craftedeals.Utils.TransitionHelper;
import com.craftedeals.activities.VendorProfileActivity;
import com.craftedeals.constants.Tags;
import com.nostra13.universalimageloader.core.DisplayImageOptions;
import com.nostra13.universalimageloader.core.ImageLoaderConfiguration;
import com.nostra13.universalimageloader.core.assist.FailReason;
import com.nostra13.universalimageloader.core.listener.ImageLoadingListener;

import java.util.ArrayList;

import carbon.widget.ImageView;
import carbon.widget.TextView;

public class HeroDealsAdapter extends RecyclerView.Adapter<HeroDealsAdapter.ViewHolder>
{
    private final ArrayList<HeroDealsModel> heroDealsModels;
    View v;
    FragmentActivity context;
    private Bundle bundle;
    com.nostra13.universalimageloader.core.ImageLoader imageLoader = com.nostra13.universalimageloader.core.ImageLoader.getInstance();
    DisplayImageOptions options = new DisplayImageOptions.Builder().cacheInMemory(true)
            .cacheOnDisc(true).resetViewBeforeLoading(true).
                    build();
    public String str_lat, str_long;


    @Override
    public ViewHolder onCreateViewHolder(ViewGroup parent, int viewType)
    {
        v = LayoutInflater.from(parent.getContext())
                .inflate(R.layout.deals_list_item, parent, false);
        ViewHolder viewHolder = new ViewHolder(v);
        return viewHolder;
    }

    @Override
    public void onBindViewHolder(final ViewHolder holder, final int position)
    {
        final HeroDealsModel heroDeals = heroDealsModels.get(position);
        holder.txt_c_title.setText(heroDeals.herodeal_title + "");
        holder.txt_c_address.setText(heroDeals.company_address + "");

        try
        {
            if (AppDelegate.isValidString(heroDeals.distance))
            {
                holder.ll_distance.setVisibility(View.VISIBLE);
                holder.txt_c_distance.setText(AppDelegate.roundOff(Double.parseDouble(heroDeals.distance)) + " KM");
            }
            else
            {
                holder.ll_distance.setVisibility(View.GONE);
            }
        }
        catch (Exception e)
        {
            AppDelegate.LogE(e);
        }

        holder.txt_c_category.setText(heroDeals.category + "");
        if (AppDelegate.isValidString(heroDeals.vendor_name) && !heroDeals.vendor_name.equalsIgnoreCase("null"))
        {
            holder.txt_c_vendor.setText(heroDeals.vendor_name + "");
        }
//        holder.txt_c_discount.setText(heroDeals.discount_offer + "%");
        holder.img_loading1.setVisibility(View.VISIBLE);
        new Handler(Looper.getMainLooper()).post(new Runnable()
        {
            @Override
            public void run()
            {
                AnimationDrawable frameAnimation = (AnimationDrawable) holder.img_loading1.getDrawable();
                frameAnimation.setCallback(holder.img_loading1);
                frameAnimation.setVisible(true, true);
                frameAnimation.start();
                ((Animatable) holder.img_loading1.getDrawable()).start();
            }
        });
        imageLoader.loadImage(heroDeals.image, options, new ImageLoadingListener()
        {
            @Override
            public void onLoadingStarted(String imageUri, View view)
            {

            }

            @Override
            public void onLoadingFailed(String imageUri, View view, FailReason failReason)
            {
//                holder.img_c_item_image.setImageDrawable(context.getResources().getDrawable(R.drawable.deal_noimage));
                holder.img_loading1.setVisibility(View.GONE);
            }

            @Override
            public void onLoadingComplete(String imageUri, View view, Bitmap loadedImage)
            {
                holder.img_c_item_image.setImageBitmap(loadedImage);
                holder.img_loading1.setVisibility(View.GONE);
            }

            @Override
            public void onLoadingCancelled(String imageUri, View view)
            {

            }
        });
        holder.txt_c_viewdetail.setOnClickListener(new View.OnClickListener()
        {
            @Override
            public void onClick(View v)
            {
//                Intent intent = new Intent(context, VendorProfileActivity.class);
//                intent.putExtra(Tags.deal_id, heroDeals.id);
//                context.startActivity(intent);

                if (AppDelegate.haveNetworkConnection(context))
                {
                    Intent intent = new Intent(context, VendorProfileActivity.class);
                    final Pair<View, String>[] pairs = TransitionHelper.createSafeTransitionParticipants(context, false, new Pair<>(holder.img_c_item_image, "square_blue_name_1"));
                    ActivityOptionsCompat transitionActivityOptions = ActivityOptionsCompat.makeSceneTransitionAnimation(context, pairs);
                    intent.putExtra(Tags.deal_id, heroDeals.id);
                    intent.putExtra(Tags.vendor_id, heroDeals.user_id);
                    if (AppDelegate.isValidString(str_lat) && AppDelegate.isValidString(str_long))
                    {
                        intent.putExtra(Tags.LAT, str_lat + "");
                        intent.putExtra(Tags.LONG, str_long + "");
                        intent.putExtra(Tags.current_location, false);
                    }
                    else
                    {
                        intent.putExtra(Tags.current_location, true);
                    }
                    Bundle bundle = new Bundle();
                    bundle.putParcelable(Tags.deal, heroDeals);
                    intent.putExtras(bundle);
                    context.startActivity(intent, transitionActivityOptions.toBundle());
                }
            }
        });
    }

    public HeroDealsAdapter(FragmentActivity context, ArrayList<HeroDealsModel> heroDealsModels, String str_lat, String str_long)
    {
        this.context = context;
        this.heroDealsModels = heroDealsModels;
        imageLoader.init(ImageLoaderConfiguration.createDefault(context));
        this.str_lat = str_lat;
        this.str_long = str_long;

    }


    public HeroDealsAdapter(FragmentActivity context, ArrayList<HeroDealsModel> heroDealsModels)
    {
        this.context = context;
        this.heroDealsModels = heroDealsModels;
        imageLoader.init(ImageLoaderConfiguration.createDefault(context));
        // this. ds=ds;
    }

    @Override
    public int getItemCount()
    {
        return heroDealsModels.size();
    }

    class ViewHolder extends RecyclerView.ViewHolder
    {
        ImageView img_c_item_image, img_c_like_dislike;
        TextView txt_c_address, txt_c_title, txt_c_category, txt_c_vendor, txt_c_discount, txt_c_viewdetail, txt_c_distance;
        android.widget.ImageView img_loading1;
        LinearLayout ll_distance, ll_value_upto;

        public ViewHolder(View itemView)
        {
            super(itemView);
            img_loading1 = (android.widget.ImageView) itemView.findViewById(R.id.img_loading1);
            img_c_item_image = (ImageView) itemView.findViewById(R.id.img_c_item_image);
            img_c_like_dislike = (ImageView) itemView.findViewById(R.id.img_c_like_dislike);
            txt_c_title = (TextView) itemView.findViewById(R.id.txt_c_title);
            txt_c_address = (TextView) itemView.findViewById(R.id.txt_c_address);
            txt_c_category = (TextView) itemView.findViewById(R.id.txt_c_category);
            txt_c_vendor = (TextView) itemView.findViewById(R.id.txt_c_vendor);
            txt_c_discount = (TextView) itemView.findViewById(R.id.txt_c_discount);
            txt_c_viewdetail = (TextView) itemView.findViewById(R.id.txt_c_viewdetail);
            txt_c_distance = (TextView) itemView.findViewById(R.id.txt_c_distance);
            ll_distance = (LinearLayout) itemView.findViewById(R.id.ll_distance);
            ll_value_upto = (LinearLayout) itemView.findViewById(R.id.ll_value_upto);
            ll_value_upto.setVisibility(View.GONE);
            img_c_like_dislike.setVisibility(View.INVISIBLE);
            itemView.findViewById(R.id.ll_c_main).setOnClickListener(new View.OnClickListener()
            {
                @Override
                public void onClick(View v)
                {

                }
            });
        }
    }
}