package com.craftedeals.adapters;


import android.content.Intent;
import android.graphics.Bitmap;
import android.graphics.drawable.Animatable;
import android.graphics.drawable.AnimationDrawable;
import android.os.Bundle;
import android.os.Handler;
import android.os.Looper;
import android.support.v4.app.ActivityOptionsCompat;
import android.support.v4.app.FragmentActivity;
import android.support.v4.util.Pair;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import com.craftedeals.AppDelegate;
import com.craftedeals.Async.PostAsync;
import com.craftedeals.Models.AdHocDealsModel;
import com.craftedeals.Models.PostAysnc_Model;
import com.craftedeals.R;
import com.craftedeals.Utils.TransitionHelper;
import com.craftedeals.activities.MyPurchaseActivity;
import com.craftedeals.activities.PurchaseAdhocActivity;
import com.craftedeals.constants.ServerRequestConstants;
import com.craftedeals.constants.Tags;
import com.craftedeals.interfaces.OnListItemClickListener;
import com.craftedeals.interfaces.OnReciveServerResponse;
import com.nostra13.universalimageloader.core.DisplayImageOptions;
import com.nostra13.universalimageloader.core.ImageLoaderConfiguration;
import com.nostra13.universalimageloader.core.assist.FailReason;
import com.nostra13.universalimageloader.core.listener.ImageLoadingListener;

import org.json.JSONException;
import org.json.JSONObject;

import java.text.SimpleDateFormat;
import java.util.ArrayList;

import carbon.widget.ImageView;
import carbon.widget.LinearLayout;
import carbon.widget.TextView;

import static com.craftedeals.AppDelegate.DISLIKE;
import static com.craftedeals.AppDelegate.LIKE;
import static com.craftedeals.activities.MyPurchaseActivity.LABEL_ACTIVE;

public class MyPurchaseAdapter extends RecyclerView.Adapter<MyPurchaseAdapter.ViewHolder> implements OnReciveServerResponse {
    public MyPurchaseAdapter.ViewHolder holder;
    public int position;
    private ArrayList<AdHocDealsModel> adHocDealsModelArrayListAll;
    private ArrayList<AdHocDealsModel> adHocDealsModelArrayListActive;
    View v;
    FragmentActivity context;
    int value = LABEL_ACTIVE;

    public OnListItemClickListener onListItemClickListener;
    com.nostra13.universalimageloader.core.ImageLoader imageLoader = com.nostra13.universalimageloader.core.ImageLoader.getInstance();
    DisplayImageOptions options = new DisplayImageOptions.Builder().cacheInMemory(true)
            .cacheOnDisc(true).resetViewBeforeLoading(true).
                    build();

    @Override
    public MyPurchaseAdapter.ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        v = LayoutInflater.from(parent.getContext())
                .inflate(R.layout.purchase_item, parent, false);
        MyPurchaseAdapter.ViewHolder viewHolder = new MyPurchaseAdapter.ViewHolder(v);
        return viewHolder;
    }

    @Override
    public void onBindViewHolder(final MyPurchaseAdapter.ViewHolder holder, final int position) {
        AdHocDealsModel adHocDealsModel = null;
        if (value == MyPurchaseActivity.LABEL_ALL) {
            adHocDealsModel = adHocDealsModelArrayListAll.get(position);
        } else {
            adHocDealsModel = adHocDealsModelArrayListActive.get(position);
        }
        holder.txt_c_title.setText(adHocDealsModel.name + "");
        holder.txt_c_address.setText(adHocDealsModel.company_address + "");
        int count = 0;
        for (int i = 0; i < adHocDealsModel.arrayItems.size(); i++) {
            count += Integer.parseInt(adHocDealsModel.arrayItems.get(i).no_of_item);
        }
        holder.txt_c_qty.setText(count + "");
        try {
            if (AppDelegate.isValidString(adHocDealsModel.distance)) {
                holder.ll_distance.setVisibility(View.VISIBLE);
                holder.txt_c_distance.setText(AppDelegate.roundOff(Double.parseDouble(adHocDealsModel.distance)) + " KM");
            } else
                holder.ll_distance.setVisibility(View.GONE);
        } catch (Exception e) {
            AppDelegate.LogE(e);
        }
        if (AppDelegate.isValidString(adHocDealsModel.vendor_name) && !adHocDealsModel.vendor_name.equalsIgnoreCase("null"))
            holder.txt_c_vendor.setText(adHocDealsModel.vendor_name + "");
        AppDelegate.LogT("adHocDealsModel.total_amount in adapter==" + adHocDealsModel.total_amount);
        holder.txt_c_total_amount.setText("$" + adHocDealsModel.total_amount + "");
        if (adHocDealsModel.liked == DISLIKE) {
            holder.img_c_like_dislike.setSelected(false);
        } else if (adHocDealsModel.liked == LIKE) {
            holder.img_c_like_dislike.setSelected(true);
        }
        if (adHocDealsModel.coupon_status == 0) {

            holder.txt_c_redeemed_status.setText("Purchased On " + "");
            holder.txt_c_viewdetail.setBackgroundColor(context.getResources().getColor(R.color.golden_yellow));
            String date = adHocDealsModel.coupon_created;
            AppDelegate.LogT("adHocDealsModel.date in adapter==" + date);
            try {
                date = date.replaceAll("/+0000", "");
                date = new SimpleDateFormat("dd MMM yyyy hh:mm a").format(new SimpleDateFormat("yyyy-MM-dd'T'HH:mm:ss").parse(date));
                holder.txt_c_date.setText(date);
            } catch (Exception e) {
                AppDelegate.LogE(e);
            }
            if (adHocDealsModel.expired == 0) {
                holder.txt_c_viewdetail.setText(context.getString(R.string.redeem));
            } else {
                holder.txt_c_viewdetail.setBackgroundColor(context.getResources().getColor(R.color.botom_gray_bg_color));
                holder.txt_c_viewdetail.setText(context.getString(R.string.expired));
            }
        } else {
            holder.txt_c_viewdetail.setText("REDEEMED");
            holder.txt_c_redeemed_status.setText("Redeemed On " + "");
            holder.txt_c_viewdetail.setBackgroundColor(context.getResources().getColor(R.color.botom_gray_bg_color));
            String date = adHocDealsModel.coupon_modified;

            try {
                date = date.replaceAll("/+0000", "");
                date = new SimpleDateFormat("dd MMM yyyy hh:mm a").format(new SimpleDateFormat("yyyy-MM-dd'T'HH:mm:ss").parse(date));
                holder.txt_c_date.setText(date);
            } catch (Exception e) {
                AppDelegate.LogE(e);
            }
        }

        final AdHocDealsModel finalAdHocDealsModel = adHocDealsModel;
        holder.img_c_like_dislike.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (onListItemClickListener != null) {
                    onListItemClickListener.setOnListItemClickListener(Tags.favourite, Integer.parseInt(finalAdHocDealsModel.coupon_id), holder.img_c_like_dislike.isSelected());
                }
            }
        });
        holder.img_loading1.setVisibility(View.VISIBLE);
        new Handler(Looper.getMainLooper()).post(new Runnable() {
            @Override
            public void run() {
                AnimationDrawable frameAnimation = (AnimationDrawable) holder.img_loading1.getDrawable();
                frameAnimation.setCallback(holder.img_loading1);
                frameAnimation.setVisible(true, true);
                frameAnimation.start();
                ((Animatable) holder.img_loading1.getDrawable()).start();
            }
        });
        imageLoader.loadImage(adHocDealsModel.image, options, new ImageLoadingListener() {
            @Override
            public void onLoadingStarted(String imageUri, View view) {
            }

            @Override
            public void onLoadingFailed(String imageUri, View view, FailReason failReason) {
                holder.img_c_item_image.setImageDrawable(context.getResources().getDrawable(R.drawable.deal_noimage));
                holder.img_loading1.setVisibility(View.GONE);
            }

            @Override
            public void onLoadingComplete(String imageUri, View view, Bitmap loadedImage) {
                holder.img_c_item_image.setImageBitmap(loadedImage);
                holder.img_loading1.setVisibility(View.GONE);
            }

            @Override
            public void onLoadingCancelled(String imageUri, View view) {
            }
        });
        final AdHocDealsModel finalAdHocDealsModel2 = adHocDealsModel;
        holder.txt_c_viewdetail.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (onListItemClickListener != null) {
                    if (finalAdHocDealsModel2.coupon_status == 1) {
                        AppDelegate.showToast(context, context.getString(R.string.deal_already_redeemed));
                    } else {
                        if (finalAdHocDealsModel2.expired == 0) {
                            onListItemClickListener.setOnListItemClickListener(Tags.redeem, Integer.parseInt(finalAdHocDealsModel.coupon_id));
                        } else {
                            AppDelegate.showToast(context, context.getString(R.string.deal_expired));
                        }
                    }
                }
            }
        });
        final AdHocDealsModel finalAdHocDealsModel1 = adHocDealsModel;
        holder.ll_c_main.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent = new Intent(context, PurchaseAdhocActivity.class);
                final Pair<View, String>[] pairs = TransitionHelper.createSafeTransitionParticipants(context, false, new Pair<>(holder.img_c_item_image, "square_blue_name_1"));
                ActivityOptionsCompat transitionActivityOptions = ActivityOptionsCompat.makeSceneTransitionAnimation(context, pairs);
                intent.putExtra(Tags.deal_id, finalAdHocDealsModel1.adhocdeal_id);
                intent.putExtra(Tags.purchase_id, Integer.parseInt(finalAdHocDealsModel1.purchase_id));
                Bundle bundle = new Bundle();
                bundle.putParcelable(Tags.deal, finalAdHocDealsModel1);
                intent.putExtras(bundle);
                context.startActivity(intent, transitionActivityOptions.toBundle());
            }
        });
    }

    public MyPurchaseAdapter(FragmentActivity context, ArrayList<AdHocDealsModel> adHocDealsModelArrayListAll, OnListItemClickListener onListItemClickListener, int value) {
        this.context = context;
        this.adHocDealsModelArrayListAll = adHocDealsModelArrayListAll;
        this.adHocDealsModelArrayListActive = new ArrayList<>();
        for (int i = 0; i < adHocDealsModelArrayListAll.size(); i++) {
            if (adHocDealsModelArrayListAll.get(i).coupon_status == 0 && adHocDealsModelArrayListAll.get(i).expired == 0) {
                adHocDealsModelArrayListActive.add(adHocDealsModelArrayListAll.get(i));
            }
        }
        AppDelegate.LogT("MyPurchaseAdapter called => " + adHocDealsModelArrayListActive.size() + " = " + adHocDealsModelArrayListAll.size());
        this.onListItemClickListener = onListItemClickListener;
        imageLoader.init(ImageLoaderConfiguration.createDefault(context));
        this.value = value;
        // this. ds=ds;
    }

    @Override
    public int getItemCount() {
        if (value == MyPurchaseActivity.LABEL_ALL) {
            return adHocDealsModelArrayListAll.size();
        } else {
            return adHocDealsModelArrayListActive.size();
        }
    }

    @Override
    public void setOnReciveResult(String apiName, String result) {
        AppDelegate.hideProgressDialog(context);
        if (!AppDelegate.isValidString(result)) {
            AppDelegate.showToast(context, context.getResources().getString(R.string.time_out));
            return;
        }
        if (apiName.equalsIgnoreCase(ServerRequestConstants.ADHOC_LIKE)) {
            parseAdHocLikes(result);
        }
    }

    private void parseAdHocLikes(String result) {
        try {
            JSONObject jsonobj = new JSONObject(result);

            if (jsonobj.getInt(Tags.status) == 1 && jsonobj.getInt(Tags.dataFlow) == 1) {
                AppDelegate.showToast(context, jsonobj.getString(Tags.response));
                if (holder.img_c_like_dislike.isSelected()) {
                    AppDelegate.LogT("img_c_like_dislike.isSelected()true==" + holder.img_c_like_dislike.isSelected());
                    holder.img_c_like_dislike.setSelected(false);
                } else {
                    AppDelegate.LogT("img_c_like_dislike.isSelected()false==" + holder.img_c_like_dislike.isSelected());
                    holder.img_c_like_dislike.setSelected(true);
                }
            } else {
                AppDelegate.showToast(context, jsonobj.getString(Tags.message));
            }
        } catch (JSONException e) {
            AppDelegate.LogE(e);
        }
    }

    class ViewHolder extends RecyclerView.ViewHolder {
        ImageView img_c_item_image, img_c_like_dislike;
        TextView txt_c_distance, txt_c_address, txt_c_title, txt_c_qty, txt_c_vendor, txt_c_redeemed_status, txt_c_viewdetail, txt_c_total_amount, txt_c_date;
        android.widget.ImageView img_loading1;
        LinearLayout ll_c_main;
        android.widget.LinearLayout ll_distance;

        public ViewHolder(View itemView) {
            super(itemView);
            txt_c_distance = (TextView) itemView.findViewById(R.id.txt_c_distance);
            ll_distance = (android.widget.LinearLayout) itemView.findViewById(R.id.ll_distance);
            img_c_item_image = (ImageView) itemView.findViewById(R.id.img_c_item_image);
            img_loading1 = (android.widget.ImageView) itemView.findViewById(R.id.img_loading1);
            img_c_like_dislike = (ImageView) itemView.findViewById(R.id.img_c_like_dislike);
            txt_c_title = (TextView) itemView.findViewById(R.id.txt_c_title);
            txt_c_qty = (TextView) itemView.findViewById(R.id.txt_c_qty);
            txt_c_vendor = (TextView) itemView.findViewById(R.id.txt_c_vendor);
            txt_c_address = (TextView) itemView.findViewById(R.id.txt_c_address);
            txt_c_redeemed_status = (TextView) itemView.findViewById(R.id.txt_c_redeemed_status);
            txt_c_viewdetail = (TextView) itemView.findViewById(R.id.txt_c_viewdetail);
            txt_c_total_amount = (TextView) itemView.findViewById(R.id.txt_c_total_amount);
            txt_c_date = (TextView) itemView.findViewById(R.id.txt_c_date);
            ll_c_main = (LinearLayout) itemView.findViewById(R.id.ll_c_main);
        }
    }

    private void execute_favouriteApi(int like, int id, int user_id) {
        try {
            if (AppDelegate.haveNetworkConnection(context, true)) {
                ArrayList<PostAysnc_Model> mPostArrayList = new ArrayList<PostAysnc_Model>();
                AppDelegate.getInstance(context).setPostParamsSecond(mPostArrayList, Tags.API_KEY, Tags.API_KEY_VALUE);
                AppDelegate.getInstance(context).setPostParamsSecond(mPostArrayList, Tags.deal_id, id);
                AppDelegate.getInstance(context).setPostParamsSecond(mPostArrayList, Tags.user_id, user_id);
                AppDelegate.getInstance(context).setPostParamsSecond(mPostArrayList, Tags.status, like, ServerRequestConstants.Key_PostintValue);
                PostAsync mPostAsyncObj;
                mPostAsyncObj = new PostAsync(context, MyPurchaseAdapter.this, ServerRequestConstants.ADHOC_LIKE,
                        mPostArrayList, null);
                AppDelegate.showProgressDialog(context);
                mPostAsyncObj.execute();
            }
        } catch (Exception e) {
            AppDelegate.showToast(context, context.getString(R.string.try_again));
            AppDelegate.LogE(e);
        }
    }

}