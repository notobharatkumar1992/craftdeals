package com.craftedeals.adapters;


import android.support.v4.app.FragmentActivity;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import com.craftedeals.AppDelegate;
import com.craftedeals.Models.CityModel;
import com.craftedeals.R;
import com.craftedeals.constants.Tags;
import com.craftedeals.interfaces.OnListCityItemClick;

import java.util.ArrayList;

import carbon.widget.LinearLayout;
import carbon.widget.TextView;

public class CityAdapter extends RecyclerView.Adapter<CityAdapter.ViewHolder> {

    private final ArrayList<CityModel> cityModels;
    public ArrayList<CityModel> allCityData;
    View v;
    FragmentActivity context;
    public OnListCityItemClick onListCityItemClick;

    @Override
    public ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        v = LayoutInflater.from(parent.getContext()).inflate(R.layout.city_list_item, parent, false);
        ViewHolder viewHolder = new ViewHolder(v);
        return viewHolder;
    }

    @Override
    public void onBindViewHolder(final ViewHolder holder, final int position) {
        final CityModel cityModel = cityModels.get(position);
        AppDelegate.LogT("cityModels city=" + cityModel.name + "");
        holder.txt_c_title.setText(cityModel.name + "");
        holder.ll_c_main.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (onListCityItemClick != null) {
                    onListCityItemClick.setOnListItemClickListener(Tags.city_name, position, cityModel);
                }
            }
        });
    }

    public CityAdapter(FragmentActivity context, ArrayList<CityModel> cityModels, OnListCityItemClick onListCityItemClick) {
        this.context = context;
        this.allCityData = new ArrayList<>();
        this.allCityData.addAll(cityModels);
        this.cityModels = new ArrayList<>();
        this.cityModels.addAll(allCityData);
        this.onListCityItemClick = onListCityItemClick;
        AppDelegate.LogT("cityModels length=" + cityModels.size() + "");
    }

    public void filterSting(String value) {
        cityModels.clear();
        if (AppDelegate.isValidString(value)) {
            for (CityModel cityModel : allCityData) {
                AppDelegate.LogT("match => " + cityModel.name + " == " + value);
                if (cityModel.name.toString().toLowerCase().contains(value.toLowerCase())) {
                    cityModels.add(cityModel);
                }
            }
        } else {
            cityModels.addAll(allCityData);
        }
        AppDelegate.LogT("cityModels size => " + cityModels.size());
        notifyDataSetChanged();
    }

    @Override
    public int getItemCount() {
        return cityModels.size();
    }

    class ViewHolder extends RecyclerView.ViewHolder {

        public TextView txt_c_title;
        public LinearLayout ll_c_main;

        public ViewHolder(View itemView) {
            super(itemView);
            txt_c_title = (TextView) itemView.findViewById(R.id.txt_c_title);
            ll_c_main = (LinearLayout) itemView.findViewById(R.id.ll_c_main);
        }
    }
}