package com.craftedeals.adapters;


import android.support.v4.app.FragmentActivity;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.LinearLayout;

import com.craftedeals.AppDelegate;
import com.craftedeals.R;
import com.craftedeals.interfaces.OnListCityItemClick;

import java.util.ArrayList;

import carbon.widget.TextView;

public class PremiumPackageListAdapter extends RecyclerView.Adapter<PremiumPackageListAdapter.ViewHolder>
{
    private final ArrayList<String> cityModels;
    View v;
    FragmentActivity context;
    public OnListCityItemClick onListCityItemClick;

    @Override
    public ViewHolder onCreateViewHolder(ViewGroup parent, int viewType)
    {
        v = LayoutInflater.from(parent.getContext()).inflate(R.layout.package_item, parent, false);
        ViewHolder viewHolder = new ViewHolder(v);
        return viewHolder;
    }

    @Override
    public void onBindViewHolder(final ViewHolder holder, final int position)
    {
        holder.txt_c_item.setText(cityModels.get(position) + "");
        holder.ll_main.setOnClickListener(new View.OnClickListener()
        {
            @Override
            public void onClick(View v)
            {
            }
        });
    }

    public PremiumPackageListAdapter(FragmentActivity context, ArrayList<String> cityModels)
    {
        this.context = context;
        this.cityModels = cityModels;
        AppDelegate.LogT("cityModels length=" + cityModels.size() + "");
    }


    @Override
    public int getItemCount()
    {
        return cityModels.size();
    }

    class ViewHolder extends RecyclerView.ViewHolder
    {

        public TextView txt_c_item;
        public LinearLayout ll_main;

        public ViewHolder(View itemView)
        {
            super(itemView);
            txt_c_item = (TextView) itemView.findViewById(R.id.txt_c_item);
            ll_main = (LinearLayout) itemView.findViewById(R.id.ll_main);
        }
    }
}