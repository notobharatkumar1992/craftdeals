package com.craftedeals.adapters;

import android.app.Activity;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;

import com.craftedeals.AppDelegate;
import com.craftedeals.Models.FavBeveragesModel;
import com.craftedeals.R;

import java.util.ArrayList;

import carbon.widget.TextView;


public class CityAdapterSearch extends BaseAdapter {

    public final Activity context;
    public ArrayList<FavBeveragesModel> arrayList, mainArrayList;

    public CityAdapterSearch(Activity context, ArrayList<FavBeveragesModel> arrayList) {
        this.context = context;
        if (this.arrayList == null)
            this.arrayList = new ArrayList<>();
        if (this.mainArrayList == null)
            this.mainArrayList = new ArrayList<>();

        this.mainArrayList.addAll(arrayList);
        this.arrayList.addAll(mainArrayList);
    }

    @Override
    public FavBeveragesModel getItem(int position) {
        return arrayList.get(position);
    }

    @Override
    public int getCount() {
        return arrayList.size();
    }

    @Override
    public long getItemId(int position) {
        return  position;
    }

    @Override
    public View getView(final int position, View view, ViewGroup parent) {
        LayoutInflater inflater = context.getLayoutInflater();
        View rowView = inflater.inflate(R.layout.dialog_listitem, null, true);
        TextView txtarrayList = (TextView) rowView.findViewById(R.id.txt_c_listvalue);
        txtarrayList.setText(arrayList.get(position).name);
        return rowView;
    }

    public void filter(String string) {
        arrayList.clear();
        if (string.length() != 0) {
            for (FavBeveragesModel cityItems : mainArrayList) {
                AppDelegate.LogT("name => " + cityItems.name + " = " + string);
                if (cityItems.name.toLowerCase().contains(string.toLowerCase())) {
                    arrayList.add(cityItems);
                }
            }
        } else {
            arrayList.addAll(mainArrayList);
        }
        notifyDataSetChanged();
    }

}
