package com.craftedeals.adapters;

import android.app.Activity;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;


import com.craftedeals.Models.FavBeveragesModel;
import com.craftedeals.R;

import java.util.ArrayList;

import carbon.widget.TextView;

public class SpinnerAdapter extends BaseAdapter {

    // ArrayList declaration
    ArrayList<FavBeveragesModel> serviceNameList;

    // Global variable declaration
    private Activity mActivity;

    // Local Database declaration

    // Util classes declaration

    public SpinnerAdapter(Activity mActivity, ArrayList<FavBeveragesModel> serviceName) {
        this.mActivity = mActivity;
        serviceNameList = serviceName;
    }

    @Override
    public int getCount() {
        return serviceNameList.size();
    }

    @Override
    public Object getItem(int position) {
        return serviceNameList.get(position);
    }

    @Override
    public long getItemId(int position) {
        return position;
    }

    @Override
    public View getView(final int position, View convertView, ViewGroup viewGroup) {
        final ViewHolder mViewHolder;

        if (convertView == null) {
            convertView = mActivity.getLayoutInflater().inflate(R.layout.spinner_item, null);
            mViewHolder = new ViewHolder();
            mViewHolder.title = (TextView) convertView.findViewById(R.id.txt_Spinner);
            convertView.setTag(mViewHolder);
        } else {
            mViewHolder = (ViewHolder) convertView.getTag();
        }
        mViewHolder.title.setText(serviceNameList.get(position).name);
        return convertView;
    }

    public class ViewHolder {
        TextView title;

       /* public ViewHolder(View view) {
            ButterKnife.bind(this, view);
        }*/
    }

   /* @Override
    protected void finalize() throws Throwable {
        super.finalize();
        mStorePref = null;
        mVcrDatabase = null;
    }*/
}
