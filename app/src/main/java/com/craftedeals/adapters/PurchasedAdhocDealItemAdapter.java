package com.craftedeals.adapters;


import android.graphics.Paint;
import android.support.v4.app.FragmentActivity;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import com.craftedeals.AppDelegate;
import com.craftedeals.Models.ItemsModel;
import com.craftedeals.R;
import com.craftedeals.constants.Tags;
import com.craftedeals.interfaces.OnItemClick;
import com.nostra13.universalimageloader.core.DisplayImageOptions;
import com.nostra13.universalimageloader.core.ImageLoaderConfiguration;

import java.util.ArrayList;

import carbon.widget.ImageView;
import carbon.widget.TextView;

public class PurchasedAdhocDealItemAdapter extends RecyclerView.Adapter<PurchasedAdhocDealItemAdapter.ViewHolder> {
    private final ArrayList<ItemsModel> heroDealsModels;
    ArrayList<ItemsModel> added_items = new ArrayList<>();
    View v;
    FragmentActivity context;

    @Override
    public ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        v = LayoutInflater.from(parent.getContext())
                .inflate(R.layout.purchased_adhoc_deal_item, parent, false);
        ViewHolder viewHolder = new ViewHolder(v);
        return viewHolder;
    }

    @Override
    public void onBindViewHolder(final ViewHolder holder, final int position) {
        final ItemsModel categoryModel = heroDealsModels.get(position);
        holder.txt_c_title.setText(categoryModel.name + "");
        holder.txt_c_offer_price.setText("$"+categoryModel.price + "");
        holder.txt_c_qty.setText(categoryModel.no_of_item);
        added_items.add(position, categoryModel);
    }

    public PurchasedAdhocDealItemAdapter(FragmentActivity context, ArrayList<ItemsModel> heroDealsModels) {
        this.context = context;
        this.heroDealsModels = heroDealsModels;
    }

    @Override
    public int getItemCount() {
        return heroDealsModels.size();
    }


    class ViewHolder extends RecyclerView.ViewHolder {
        TextView txt_c_qty, txt_c_title, txt_c_offer_price;

        public ViewHolder(View itemView) {
            super(itemView);
            txt_c_qty = (TextView) itemView.findViewById(R.id.txt_c_qty);
            txt_c_title = (TextView) itemView.findViewById(R.id.txt_c_title);
            txt_c_offer_price = (TextView) itemView.findViewById(R.id.txt_c_offer_price);
        }
    }
}