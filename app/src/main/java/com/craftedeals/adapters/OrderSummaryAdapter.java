package com.craftedeals.adapters;


import android.graphics.Bitmap;
import android.graphics.Paint;
import android.os.Bundle;
import android.support.v4.app.FragmentActivity;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import com.craftedeals.AppDelegate;
import com.craftedeals.Models.ItemsModel;
import com.craftedeals.R;
import com.craftedeals.Utils.CircleImageView;
import com.craftedeals.constants.Tags;
import com.craftedeals.interfaces.OnItemClick;
import com.nostra13.universalimageloader.core.DisplayImageOptions;
import com.nostra13.universalimageloader.core.ImageLoaderConfiguration;
import com.nostra13.universalimageloader.core.assist.FailReason;
import com.nostra13.universalimageloader.core.listener.ImageLoadingListener;

import java.util.ArrayList;

import carbon.widget.ImageView;
import carbon.widget.TextView;

public class OrderSummaryAdapter extends RecyclerView.Adapter<OrderSummaryAdapter.ViewHolder> {
    View v;
    FragmentActivity context;
    private Bundle bundle;
    com.nostra13.universalimageloader.core.ImageLoader imageLoader = com.nostra13.universalimageloader.core.ImageLoader.getInstance();
    DisplayImageOptions options = new DisplayImageOptions.Builder().cacheInMemory(true)
            .cacheOnDisc(true).resetViewBeforeLoading(true).
                    build();
    private final ArrayList<ItemsModel> heroDealsModels;
    ArrayList<ItemsModel> added_items = new ArrayList<>();
    public OnItemClick onItemClick;
    String image;

    @Override
    public ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        v = LayoutInflater.from(parent.getContext())
                .inflate(R.layout.order_summary_item, parent, false);
        ViewHolder viewHolder = new ViewHolder(v);
        return viewHolder;
    }

    @Override
    public void onBindViewHolder(final ViewHolder holder, final int position) {
        final ItemsModel heroDeals = heroDealsModels.get(position);
        holder.txt_c_real_price.setPaintFlags(holder.txt_c_real_price.getPaintFlags() | Paint.STRIKE_THRU_TEXT_FLAG);
        holder.txt_c_real_price.setPaintFlags(holder.txt_c_real_price.getPaintFlags() | Paint.LINEAR_TEXT_FLAG);
        holder.txt_c_order_title.setText(heroDeals.name + "");
        holder.txt_c_real_price.setText("$" + heroDeals.actual_cost + "");
        holder.txt_c_offer_price.setText("$" + heroDeals.price + "");
        holder.txt_c_discount.setText(heroDeals.discount_cost + " % off");
        holder.txt_c_item_count.setText(heroDeals.item_count + "");
        try {
            added_items.get(position);
            added_items.set(position, heroDeals);
        } catch (IndexOutOfBoundsException e) {
            added_items.add(position, heroDeals);
        }
        if (AppDelegate.isValidString(image))
            imageLoader.loadImage(image, options, new ImageLoadingListener() {
                @Override
                public void onLoadingStarted(String imageUri, View view) {

                }

                @Override
                public void onLoadingFailed(String imageUri, View view, FailReason failReason) {
                    holder.img_circle_order_image.setImageDrawable(context.getResources().getDrawable(R.drawable.deal_noimage));
                }

                @Override
                public void onLoadingComplete(String imageUri, View view, Bitmap loadedImage) {
                    holder.img_circle_order_image.setImageBitmap(loadedImage);

                }

                @Override
                public void onLoadingCancelled(String imageUri, View view) {

                }
            });
        holder.img_c_delete.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                heroDeals.item_count = "0";
                try {
                    added_items.get(position);
                    added_items.remove(position);
                    AppDelegate.LogT("categoryModel.item_count=>" + heroDeals.item_count + " , categoryModel.price=>" + heroDeals.price + "  ,added items==>" + added_items);
                    heroDealsModels.remove(position);
                    OrderSummaryAdapter.this.notifyDataSetChanged();
                    if (onItemClick != null)
                        onItemClick.setOnItemClickListener(Tags.clicks, position, added_items);
                } catch (IndexOutOfBoundsException e) {
                    AppDelegate.LogE(e);
                }
            }
        });
        holder.img_c_increment.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if(heroDeals.validation_type==1){
                    if(heroDeals.available_deal<=Integer.parseInt(holder.txt_c_item_count.getText().toString()))  {
                        holder.txt_c_item_count.setText(String.valueOf(Integer.parseInt(holder.txt_c_item_count.getText().toString())));
                    }else{
                        holder.txt_c_item_count.setText(String.valueOf(Integer.parseInt(holder.txt_c_item_count.getText().toString()) + 1));
                    }
                }else{
                    holder.txt_c_item_count.setText(String.valueOf(Integer.parseInt(holder.txt_c_item_count.getText().toString()) + 1));
                }
//                holder.txt_c_item_count.setText(String.valueOf(Integer.parseInt(holder.txt_c_item_count.getText().toString()) + 1));
                heroDeals.item_count = String.valueOf(Integer.parseInt(holder.txt_c_item_count.getText().toString()));
                AppDelegate.LogT("Position to be add on =" + position);
                try {
                    added_items.get(position);
                    added_items.set(position, heroDeals);
                } catch (IndexOutOfBoundsException e) {
                    added_items.add(position, heroDeals);
                }

                if (onItemClick != null)
                    onItemClick.setOnItemClickListener(Tags.clicks, position, added_items);
                AppDelegate.LogT("categoryModel.item_count=>" + heroDeals.item_count + " , categoryModel.price=>" + heroDeals.price + "  ,added items==>" + added_items);

            }
        });
        holder.img_c_decrement.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (Integer.parseInt(holder.txt_c_item_count.getText().toString()) <= 0) {

                } else {
                    holder.txt_c_item_count.setText(String.valueOf(Integer.parseInt(holder.txt_c_item_count.getText().toString()) - 1));
                    heroDeals.item_count = String.valueOf(Integer.parseInt(holder.txt_c_item_count.getText().toString()));
                    AppDelegate.LogT("Position to be add on =" + position);
                    try {
                        added_items.get(position);
                        added_items.set(position, heroDeals);
                    } catch (IndexOutOfBoundsException e) {
                        added_items.add(position, heroDeals);
                    }

                    if (onItemClick != null)
                        onItemClick.setOnItemClickListener(Tags.clicks, position, added_items);
                    AppDelegate.LogT("categoryModel.item_count=>" + heroDeals.item_count + " , categoryModel.price=>" + heroDeals.price + "  ,added items==>" + added_items);

                }
            }
        });
    }

    public OrderSummaryAdapter(FragmentActivity context, ArrayList<ItemsModel> heroDealsModels, OnItemClick onItemClick, String image) {
        this.context = context;
        this.heroDealsModels = heroDealsModels;
        this.onItemClick = onItemClick;
        this.image = image;
        imageLoader.init(ImageLoaderConfiguration.createDefault(context));
    }

    @Override
    public int getItemCount() {
        return heroDealsModels.size();
    }

    class ViewHolder extends RecyclerView.ViewHolder {
        CircleImageView img_circle_order_image;
        TextView txt_c_order_title, txt_c_real_price, txt_c_offer_price, txt_c_discount, txt_c_item_count;
        ImageView img_c_delete, img_c_decrement, img_c_increment;

        public ViewHolder(View itemView) {
            super(itemView);
            img_circle_order_image = (CircleImageView) itemView.findViewById(R.id.img_circle_order_image);
            txt_c_order_title = (TextView) itemView.findViewById(R.id.txt_c_order_title);
            txt_c_real_price = (TextView) itemView.findViewById(R.id.txt_c_real_price);
            txt_c_offer_price = (TextView) itemView.findViewById(R.id.txt_c_offer_price);
            txt_c_discount = (TextView) itemView.findViewById(R.id.txt_c_discount);
            txt_c_item_count = (TextView) itemView.findViewById(R.id.txt_c_item_count);
            img_c_increment = (ImageView) itemView.findViewById(R.id.img_c_increment);
            img_c_decrement = (ImageView) itemView.findViewById(R.id.img_c_decrement);
            img_c_delete = (ImageView) itemView.findViewById(R.id.img_c_delete);
        }
    }
}