package com.craftedeals.adapters;


import android.content.Intent;
import android.graphics.Bitmap;
import android.graphics.drawable.Animatable;
import android.graphics.drawable.AnimationDrawable;
import android.os.Bundle;
import android.os.Handler;
import android.os.Looper;
import android.support.v4.app.ActivityOptionsCompat;
import android.support.v4.app.FragmentActivity;
import android.support.v4.util.Pair;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.LinearLayout;

import com.craftedeals.AppDelegate;
import com.craftedeals.Async.PostAsync;
import com.craftedeals.Models.AdHocDealsModel;
import com.craftedeals.Models.PostAysnc_Model;
import com.craftedeals.R;
import com.craftedeals.Utils.Prefs;
import com.craftedeals.Utils.TransitionHelper;
import com.craftedeals.activities.AdHocDealDetailActivity;
import com.craftedeals.constants.ServerRequestConstants;
import com.craftedeals.constants.Tags;
import com.craftedeals.interfaces.OnListItemClickListener;
import com.craftedeals.interfaces.OnReciveServerResponse;
import com.nostra13.universalimageloader.core.DisplayImageOptions;
import com.nostra13.universalimageloader.core.ImageLoaderConfiguration;
import com.nostra13.universalimageloader.core.assist.FailReason;
import com.nostra13.universalimageloader.core.listener.ImageLoadingListener;

import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;

import carbon.widget.ImageView;
import carbon.widget.TextView;

import static com.craftedeals.AppDelegate.DISLIKE;
import static com.craftedeals.AppDelegate.LIKE;

public class AdHocDealsAdapter extends RecyclerView.Adapter<AdHocDealsAdapter.ViewHolder> implements OnReciveServerResponse {
    public ViewHolder holder;
    public int position;
    private final ArrayList<AdHocDealsModel> adHocDealsModelArrayList;
    View v;
    FragmentActivity context;
    private Bundle bundle;
    public OnListItemClickListener onListItemClickListener;
    com.nostra13.universalimageloader.core.ImageLoader imageLoader = com.nostra13.universalimageloader.core.ImageLoader.getInstance();
    DisplayImageOptions options = new DisplayImageOptions.Builder().cacheInMemory(true)
            .cacheOnDisc(true).resetViewBeforeLoading(true).
                    build();


    @Override
    public ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        v = LayoutInflater.from(parent.getContext())
                .inflate(R.layout.deals_list_item, parent, false);
        ViewHolder viewHolder = new ViewHolder(v);
        return viewHolder;
    }

    @Override
    public void onBindViewHolder(final ViewHolder holder, final int position) {
        final AdHocDealsModel adHocDealsModel = adHocDealsModelArrayList.get(position);
        holder.txt_c_title.setText(adHocDealsModel.name + "");
        holder.txt_c_address.setText(adHocDealsModel.company_address + "");
        holder.txt_c_category.setText(adHocDealsModel.category + "");
        if (AppDelegate.isValidString(adHocDealsModel.vendor_name) && !adHocDealsModel.vendor_name.equalsIgnoreCase("null"))
            holder.txt_c_vendor.setText(adHocDealsModel.vendor_name + "");
        holder.txt_c_discount.setText(adHocDealsModel.discount_cost + " %");
        if (adHocDealsModel.liked == DISLIKE) {
            holder.img_c_like_dislike.setSelected(false);
        } else if (adHocDealsModel.liked == LIKE) {
            holder.img_c_like_dislike.setSelected(true);
        }
        try {
            if (AppDelegate.isValidString(adHocDealsModel.distance)) {
                holder.ll_distance.setVisibility(View.VISIBLE);
                holder.txt_c_distance.setText(AppDelegate.roundOff(Double.parseDouble(adHocDealsModel.distance)) + " KM");
            } else {
                holder.ll_distance.setVisibility(View.GONE);
            }
        } catch (Exception e) {
            AppDelegate.LogE(e);
        }
        holder.img_c_like_dislike.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (new Prefs(context).getUserdata() != null) {
                    AdHocDealsAdapter.this.holder = holder;
                    AdHocDealsAdapter.this.position = position;
                    if (holder.img_c_like_dislike.isSelected()) {
                        execute_favouriteApi(DISLIKE, adHocDealsModel.id, new Prefs(context).getUserdata().id);
                    } else {
                        execute_favouriteApi(LIKE, adHocDealsModel.id, new Prefs(context).getUserdata().id);
                    }
                } else {
                    if (onListItemClickListener != null)
                        onListItemClickListener.setOnListItemClickListener(Tags.LOGIN, 0);
                }
            }
        });
        holder.img_loading1.setVisibility(View.VISIBLE);
        new Handler(Looper.getMainLooper()).post(new Runnable() {
            @Override
            public void run() {
                AnimationDrawable frameAnimation = (AnimationDrawable) holder.img_loading1.getDrawable();
                frameAnimation.setCallback(holder.img_loading1);
                frameAnimation.setVisible(true, true);
                frameAnimation.start();
                ((Animatable) holder.img_loading1.getDrawable()).start();
            }
        });
        imageLoader.loadImage(adHocDealsModel.image, options, new ImageLoadingListener() {
            @Override
            public void onLoadingStarted(String imageUri, View view) {
            }

            @Override
            public void onLoadingFailed(String imageUri, View view, FailReason failReason) {
//                holder.img_c_item_image.setImageDrawable(context.getResources().getDrawable(R.drawable.deal_noimage));
                holder.img_loading1.setVisibility(View.GONE);
            }

            @Override
            public void onLoadingComplete(String imageUri, View view, Bitmap loadedImage) {
                holder.img_c_item_image.setImageBitmap(loadedImage);
                holder.img_loading1.setVisibility(View.GONE);
            }

            @Override
            public void onLoadingCancelled(String imageUri, View view) {
            }
        });
        holder.txt_c_viewdetail.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent = new Intent(context, AdHocDealDetailActivity.class);
                final Pair<View, String>[] pairs = TransitionHelper.createSafeTransitionParticipants(context, false, new Pair<>(holder.img_c_item_image, "square_blue_name_1"));
                ActivityOptionsCompat transitionActivityOptions = ActivityOptionsCompat.makeSceneTransitionAnimation(context, pairs);
                intent.putExtra(Tags.deal_id, adHocDealsModel.id);
                Bundle bundle = new Bundle();
                bundle.putParcelable(Tags.deal, adHocDealsModel);
                intent.putExtras(bundle);
                context.startActivity(intent, transitionActivityOptions.toBundle());
            }
        });
    }

    public AdHocDealsAdapter(FragmentActivity context, ArrayList<AdHocDealsModel> adHocDealsModelArrayList, OnListItemClickListener onListItemClickListener) {
        this.context = context;
        this.adHocDealsModelArrayList = adHocDealsModelArrayList;
        this.onListItemClickListener = onListItemClickListener;
        imageLoader.init(ImageLoaderConfiguration.createDefault(context));
        // this. ds=ds;
    }

    @Override
    public int getItemCount() {
        return adHocDealsModelArrayList.size();
    }

    @Override
    public void setOnReciveResult(String apiName, String result) {
        AppDelegate.hideProgressDialog(context);
        if (!AppDelegate.isValidString(result)) {
            AppDelegate.showToast(context, context.getResources().getString(R.string.time_out));
            return;
        }
        if (apiName.equalsIgnoreCase(ServerRequestConstants.ADHOC_LIKE)) {
            parseAdHocLikes(result);
        }
    }

    private void parseAdHocLikes(String result) {
        try {
            JSONObject jsonobj = new JSONObject(result);

            if (jsonobj.getInt(Tags.status) == 1 && jsonobj.getInt(Tags.dataFlow) == 1) {
                AppDelegate.showToast(context, jsonobj.getString(Tags.response));
                if (holder.img_c_like_dislike.isSelected()) {
                    AppDelegate.LogT("img_c_like_dislike.isSelected()true==" + holder.img_c_like_dislike.isSelected());
                    holder.img_c_like_dislike.setSelected(false);
                } else {
                    AppDelegate.LogT("img_c_like_dislike.isSelected()false==" + holder.img_c_like_dislike.isSelected());
                    holder.img_c_like_dislike.setSelected(true);
                }
            } else {
                AppDelegate.showToast(context, jsonobj.getString(Tags.message));
            }
        } catch (JSONException e) {
            AppDelegate.LogE(e);
        }
    }

    class ViewHolder extends RecyclerView.ViewHolder {
        ImageView img_c_item_image, img_c_like_dislike;
        TextView txt_c_title, txt_c_category, txt_c_vendor, txt_c_discount, txt_c_viewdetail, txt_c_address, txt_c_distance;
        android.widget.ImageView img_loading1;
        LinearLayout ll_distance;

        public ViewHolder(View itemView) {
            super(itemView);
            img_c_item_image = (ImageView) itemView.findViewById(R.id.img_c_item_image);
            img_loading1 = (android.widget.ImageView) itemView.findViewById(R.id.img_loading1);
            img_c_like_dislike = (ImageView) itemView.findViewById(R.id.img_c_like_dislike);
            txt_c_title = (TextView) itemView.findViewById(R.id.txt_c_title);
            txt_c_address = (TextView) itemView.findViewById(R.id.txt_c_address);
            txt_c_category = (TextView) itemView.findViewById(R.id.txt_c_category);
            txt_c_vendor = (TextView) itemView.findViewById(R.id.txt_c_vendor);
            txt_c_discount = (TextView) itemView.findViewById(R.id.txt_c_discount);
            txt_c_viewdetail = (TextView) itemView.findViewById(R.id.txt_c_viewdetail);
            txt_c_distance = (TextView) itemView.findViewById(R.id.txt_c_distance);
            ll_distance = (LinearLayout) itemView.findViewById(R.id.ll_distance);
            itemView.findViewById(R.id.ll_c_main).setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {

                }
            });
        }
    }

    private void execute_favouriteApi(int like, int id, int user_id) {
        try {
            if (AppDelegate.haveNetworkConnection(context, true)) {
                ArrayList<PostAysnc_Model> mPostArrayList = new ArrayList<PostAysnc_Model>();
                AppDelegate.getInstance(context).setPostParamsSecond(mPostArrayList, Tags.API_KEY, Tags.API_KEY_VALUE);
                AppDelegate.getInstance(context).setPostParamsSecond(mPostArrayList, Tags.deal_id, id);
                AppDelegate.getInstance(context).setPostParamsSecond(mPostArrayList, Tags.user_id, user_id);
                AppDelegate.getInstance(context).setPostParamsSecond(mPostArrayList, Tags.status, like, ServerRequestConstants.Key_PostintValue);
                PostAsync mPostAsyncObj;
                mPostAsyncObj = new PostAsync(context, AdHocDealsAdapter.this, ServerRequestConstants.ADHOC_LIKE,
                        mPostArrayList, null);
                AppDelegate.showProgressDialog(context);
                mPostAsyncObj.execute();
            }
        } catch (Exception e) {
            AppDelegate.showToast(context, context.getString(R.string.try_again));
            AppDelegate.LogE(e);
        }
    }


}