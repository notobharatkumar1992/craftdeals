package com.craftedeals.adapters;

import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentStatePagerAdapter;

import com.craftedeals.fragments.MemberShipFreeFragment;

import java.util.List;

/**
 * The <code>PagerAdapter</code> serves the fragments when paging.
 *
 * @author mwho
 */
public class PagerLoopAdapter extends FragmentStatePagerAdapter
{
    public List<Fragment> fragments;

    /**
     * Constructor of the class
     *
     * @param fragments
     */
    public PagerLoopAdapter(FragmentManager fm, List<Fragment> fragments)
    {
        super(fm);
        this.fragments = fragments;
    }

    /**
     * This method will be invoked when a page is requested to create
     */
    @Override
    public Fragment getItem(int position)
    {
        Fragment fragment = new MemberShipFreeFragment();
        fragment.setArguments(fragments.get(position).getArguments());
        return fragment;
    }

    /**
     * Returns the number of pages
     */
    @Override
    public int getCount()
    {
        return fragments.size();
    }

    @Override
    public int getItemPosition(Object object)
    {
        return super.getItemPosition(object);
    }
}