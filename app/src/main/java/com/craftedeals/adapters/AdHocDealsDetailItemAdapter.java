package com.craftedeals.adapters;


import android.graphics.Paint;
import android.support.v4.app.FragmentActivity;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import com.craftedeals.AppDelegate;
import com.craftedeals.Models.ItemsModel;
import com.craftedeals.R;
import com.craftedeals.constants.Tags;
import com.craftedeals.interfaces.OnItemClick;
import com.nostra13.universalimageloader.core.DisplayImageOptions;
import com.nostra13.universalimageloader.core.ImageLoaderConfiguration;

import java.util.ArrayList;

import carbon.widget.ImageView;
import carbon.widget.TextView;

public class AdHocDealsDetailItemAdapter extends RecyclerView.Adapter<AdHocDealsDetailItemAdapter.ViewHolder> {
    private final ArrayList<ItemsModel> heroDealsModels;
    ArrayList<ItemsModel> added_items = new ArrayList<>();
    View v;
    FragmentActivity context;
    com.nostra13.universalimageloader.core.ImageLoader imageLoader = com.nostra13.universalimageloader.core.ImageLoader.getInstance();
    DisplayImageOptions options = new DisplayImageOptions.Builder().cacheInMemory(true)
            .cacheOnDisc(true).resetViewBeforeLoading(true).
                    build();
    public OnItemClick onItemClick;

    @Override
    public ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        v = LayoutInflater.from(parent.getContext())
                .inflate(R.layout.deals_details_list_item, parent, false);
        ViewHolder viewHolder = new ViewHolder(v);
        return viewHolder;
    }

    @Override
    public void onBindViewHolder(final ViewHolder holder, final int position) {
        final ItemsModel categoryModel = heroDealsModels.get(position);
        holder.txt_c_real_price.setPaintFlags(holder.txt_c_real_price.getPaintFlags() | Paint.STRIKE_THRU_TEXT_FLAG);
        holder.txt_c_real_price.setPaintFlags(holder.txt_c_real_price.getPaintFlags() | Paint.LINEAR_TEXT_FLAG);
        categoryModel.item_count = "0";
        holder.txt_c_real_price.setText("$" + categoryModel.actual_cost + "");
        holder.txt_c_title.setText(categoryModel.name + "");
        holder.txt_c_discount.setText(categoryModel.discount_cost + " % off");
        holder.txt_c_offer_price.setText("$" + categoryModel.price + "");
        holder.txt_c_quantity.setText("0");
        added_items.add(position, categoryModel);
        holder.img_c_minus.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (Integer.parseInt(holder.txt_c_quantity.getText().toString()) <= 0) {
                } else {
                    holder.txt_c_quantity.setText(String.valueOf(Integer.parseInt(holder.txt_c_quantity.getText().toString()) - 1));
                    categoryModel.item_count = String.valueOf(Integer.parseInt(holder.txt_c_quantity.getText().toString()));
                    AppDelegate.LogT("Position to be add on =" + position);
                    try {
                        added_items.get(position);
                        added_items.set(position, categoryModel);
                    } catch (IndexOutOfBoundsException e) {
                        added_items.add(position, categoryModel);
                    }

                    if (onItemClick != null)
                        onItemClick.setOnItemClickListener(Tags.clicks, position, added_items);
                    AppDelegate.LogT("categoryModel.item_count=>" + categoryModel.item_count + " , categoryModel.price=>" + categoryModel.price + "  ,added items==>" + added_items);
                }
            }
        });
        holder.img_c_plus.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (categoryModel.validation_type == 1) {
                    if (categoryModel.available_deal <= Integer.parseInt(holder.txt_c_quantity.getText().toString())) {
                        holder.txt_c_quantity.setText(String.valueOf(Integer.parseInt(holder.txt_c_quantity.getText().toString())));
                    } else {
                        holder.txt_c_quantity.setText(String.valueOf(Integer.parseInt(holder.txt_c_quantity.getText().toString()) + 1));
                    }
                } else {
                    holder.txt_c_quantity.setText(String.valueOf(Integer.parseInt(holder.txt_c_quantity.getText().toString()) + 1));

                }
                categoryModel.item_count = String.valueOf(Integer.parseInt(holder.txt_c_quantity.getText().toString()));
                AppDelegate.LogT("Position to be add on =" + position);
                try {
                    added_items.get(position);
                    added_items.set(position, categoryModel);
                } catch (IndexOutOfBoundsException e) {
                    added_items.add(position, categoryModel);
                }

                if (onItemClick != null)
                    onItemClick.setOnItemClickListener(Tags.clicks, position, added_items);
                AppDelegate.LogT("categoryModel.item_count=>" + categoryModel.item_count + " , categoryModel.price=>" + categoryModel.price + "  ,added items==>" + added_items);

            }
        });
        holder.txt_c_know_more.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                AppDelegate.showAlert(context, categoryModel.details);
            }
        });
    }

    public AdHocDealsDetailItemAdapter(FragmentActivity context, ArrayList<ItemsModel> heroDealsModels, OnItemClick onItemClick) {
        this.context = context;
        this.heroDealsModels = heroDealsModels;
        this.onItemClick = onItemClick;
        imageLoader.init(ImageLoaderConfiguration.createDefault(context));
    }

    @Override
    public int getItemCount() {
        return heroDealsModels.size();
    }


    class ViewHolder extends RecyclerView.ViewHolder {
        TextView txt_c_real_price, txt_c_title, txt_c_discount, txt_c_offer_price, txt_c_know_more, txt_c_quantity;
        ImageView img_c_minus, img_c_plus;

        public ViewHolder(View itemView) {
            super(itemView);
            txt_c_real_price = (TextView) itemView.findViewById(R.id.txt_c_real_price);
            txt_c_title = (TextView) itemView.findViewById(R.id.txt_c_title);
            txt_c_discount = (TextView) itemView.findViewById(R.id.txt_c_discount);
            txt_c_offer_price = (TextView) itemView.findViewById(R.id.txt_c_offer_price);
            txt_c_know_more = (TextView) itemView.findViewById(R.id.txt_c_know_more);
            txt_c_quantity = (TextView) itemView.findViewById(R.id.txt_c_quantity);
            img_c_minus = (ImageView) itemView.findViewById(R.id.img_c_minus);
            img_c_plus = (ImageView) itemView.findViewById(R.id.img_c_plus);
        }
    }
}