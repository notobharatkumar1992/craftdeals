package com.craftedeals.adapters;


import android.content.Intent;
import android.graphics.Bitmap;
import android.support.v4.app.FragmentActivity;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import com.craftedeals.AppDelegate;
import com.craftedeals.Models.NotificationModel;
import com.craftedeals.R;
import com.craftedeals.Utils.CircleImageView;
import com.craftedeals.activities.AdHocDealDetailActivity;
import com.craftedeals.activities.PurchaseAdhocActivity;
import com.craftedeals.activities.PurchaseHeroActivity;
import com.craftedeals.activities.UpgradeMembershipActivity;
import com.craftedeals.activities.VendorProfileActivity;
import com.craftedeals.constants.Tags;
import com.craftedeals.interfaces.OnListCityItemClick;
import com.nostra13.universalimageloader.core.DisplayImageOptions;
import com.nostra13.universalimageloader.core.ImageLoaderConfiguration;
import com.nostra13.universalimageloader.core.assist.FailReason;
import com.nostra13.universalimageloader.core.listener.ImageLoadingListener;

import java.text.SimpleDateFormat;
import java.util.ArrayList;

import carbon.widget.LinearLayout;
import carbon.widget.TextView;

import static com.craftedeals.firebase.MyFirebaseMessagingService.EXPIRED_PURCHASE;
import static com.craftedeals.firebase.MyFirebaseMessagingService.EXPIRING_PURCHASE_DEAL;
import static com.craftedeals.firebase.MyFirebaseMessagingService.MEMBERSHIP_EXPIRED;
import static com.craftedeals.firebase.MyFirebaseMessagingService.MEMBERSHIP_EXPIRING;
import static com.craftedeals.firebase.MyFirebaseMessagingService.NEWLY_ADDED_ADHOC_DEALS;
import static com.craftedeals.firebase.MyFirebaseMessagingService.NEWLY_ADDED_HERO_DEALS;
import static com.craftedeals.firebase.MyFirebaseMessagingService.PURCHASED_DEAL;
import static com.craftedeals.firebase.MyFirebaseMessagingService.REDEEM_HERO_DEAL;
import static com.craftedeals.firebase.MyFirebaseMessagingService.REDEEM_PURCHASE_ADHOC_DEAL;
import static com.craftedeals.firebase.MyFirebaseMessagingService.UPGRADED_MEMBERSHIP;

public class NotificationAdapter extends RecyclerView.Adapter<NotificationAdapter.ViewHolder> {

    private ArrayList<NotificationModel> notificationModelArrayList;
    View v;
    FragmentActivity context;
    public OnListCityItemClick onListCityItemClick;
    com.nostra13.universalimageloader.core.ImageLoader imageLoader = com.nostra13.universalimageloader.core.ImageLoader.getInstance();
    DisplayImageOptions options = new DisplayImageOptions.Builder().cacheInMemory(true)
            .cacheOnDisc(true).resetViewBeforeLoading(true).
                    build();

    @Override
    public ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        v = LayoutInflater.from(parent.getContext()).inflate(R.layout.notification_item, parent, false);
        ViewHolder viewHolder = new ViewHolder(v);
        return viewHolder;
    }

    @Override
    public void onBindViewHolder(final ViewHolder holder, final int position) {
        final NotificationModel notificationModel = notificationModelArrayList.get(position);
        holder.txt_c_dealtitle.setVisibility(View.VISIBLE);
        holder.txt_c_dealtitle.setText(notificationModel.getDeal_title() + "");
        holder.txt_c_message.setText(notificationModel.getMessage() + "");
        holder.txt_c_vendor.setText(notificationModel.getVendor_name() + "");
        try {
            if (AppDelegate.isValidString(notificationModel.created)) {
                String str_date = notificationModel.created.substring(0, notificationModel.created.indexOf("T"));
                str_date = new SimpleDateFormat("dd MMM yyyy").format(new SimpleDateFormat("yyyy-MM-dd").parse(str_date));
                holder.txt_c_date.setText(str_date);
                holder.txt_c_date.setVisibility(View.VISIBLE);
            } else {
                holder.txt_c_date.setVisibility(View.GONE);
            }
        } catch (Exception e) {
            AppDelegate.LogE(e);
            holder.txt_c_date.setVisibility(View.GONE);
        }

        if (notificationModel.getNotification_type() == MEMBERSHIP_EXPIRED || notificationModel.getNotification_type() == MEMBERSHIP_EXPIRING || notificationModel.getNotification_type() == UPGRADED_MEMBERSHIP) {
            holder.txt_c_vendor.setText(context.getString(R.string.app_name) + "");
            holder.txt_c_dealtitle.setVisibility(View.GONE);
        } else {
            imageLoader.loadImage(notificationModel.deal_image, options, new ImageLoadingListener() {
                @Override
                public void onLoadingStarted(String imageUri, View view) {
                }

                @Override
                public void onLoadingFailed(String imageUri, View view, FailReason failReason) {
                    holder.img_circle_notification_image.setImageDrawable(context.getResources().getDrawable(R.drawable.deal_noimage));
                    holder.img_loading1.setVisibility(View.GONE);
                }

                @Override
                public void onLoadingComplete(String imageUri, View view, Bitmap loadedImage) {
                    holder.img_circle_notification_image.setImageBitmap(loadedImage);
                    holder.img_loading1.setVisibility(View.GONE);
                }

                @Override
                public void onLoadingCancelled(String imageUri, View view) {
                }
            });
        }
        holder.ll_c_main.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent;
                if (notificationModel.getNotification_type() != 0 && notificationModel.getNotification_type() == NEWLY_ADDED_ADHOC_DEALS) {
                    intent = new Intent(context, AdHocDealDetailActivity.class);
                    try {
                        intent.putExtra(Tags.deal_id, Integer.parseInt(notificationModel.getDeal_id()));
                        intent.putExtra(Tags.FROM, Tags.notification);
                        intent.putExtra(Tags.notification_id, notificationModel.id);
                        intent.putExtra(Tags.realm_index_id, notificationModel.getId());
                        context.startActivity(intent);
                    } catch (Exception e) {
                        AppDelegate.LogE(e);
                    }
                } else if (notificationModel.getNotification_type() != 0 && notificationModel.getNotification_type() == NEWLY_ADDED_HERO_DEALS) {
                    intent = new Intent(context, VendorProfileActivity.class);
                    try {
                        intent.putExtra(Tags.deal_id, Integer.parseInt(notificationModel.getDeal_id()));
                        intent.putExtra(Tags.FROM, Tags.notification);
                        intent.putExtra(Tags.notification_id, notificationModel.id);
                        intent.putExtra(Tags.vendor_id, Integer.parseInt(notificationModel.getVendor_id()));
                        intent.putExtra(Tags.realm_index_id, notificationModel.getId());
                        context.startActivity(intent);
                    } catch (Exception e) {
                        AppDelegate.LogE(e);
                    }
                } else if (notificationModel.getNotification_type() != 0 && notificationModel.getNotification_type() == PURCHASED_DEAL) {
                    intent = new Intent(context, PurchaseAdhocActivity.class);
                    try {
                        intent.putExtra(Tags.purchase_id, (notificationModel.getPurchase_id()));
                        intent.putExtra(Tags.FROM, Tags.notification);
                        intent.putExtra(Tags.notification_id, notificationModel.id);
                        intent.putExtra(Tags.vendor_id, Integer.parseInt(notificationModel.getVendor_id()));
                        intent.putExtra(Tags.realm_index_id, notificationModel.getId());
                        context.startActivity(intent);
                    } catch (Exception e) {
                        AppDelegate.LogE(e);
                    }
                } else if (notificationModel.getNotification_type() != 0 && notificationModel.getNotification_type() == REDEEM_HERO_DEAL) {
                    intent = new Intent(context, PurchaseHeroActivity.class);
                    try {
                        intent.putExtra(Tags.purchase_id, (notificationModel.getPurchase_id()));
                        intent.putExtra(Tags.FROM, Tags.notification);
                        intent.putExtra(Tags.notification_id, notificationModel.id);
                        intent.putExtra(Tags.vendor_id, Integer.parseInt(notificationModel.getVendor_id()));
                        intent.putExtra(Tags.realm_index_id, notificationModel.getId());
                        context.startActivity(intent);
                    } catch (Exception e) {
                        AppDelegate.LogE(e);
                    }
                } else if (notificationModel.getNotification_type() == REDEEM_PURCHASE_ADHOC_DEAL || notificationModel.getNotification_type() == EXPIRED_PURCHASE || notificationModel.getNotification_type() == EXPIRING_PURCHASE_DEAL) {
                    intent = new Intent(context, PurchaseAdhocActivity.class);
                    try {
                        intent.putExtra(Tags.purchase_id, (notificationModel.getPurchase_id()));
                        intent.putExtra(Tags.FROM, Tags.notification);
                        intent.putExtra(Tags.notification_id, notificationModel.id);
                        intent.putExtra(Tags.vendor_id, Integer.parseInt(notificationModel.getVendor_id()));
                        intent.putExtra(Tags.realm_index_id, notificationModel.getId());
                        context.startActivity(intent);
                    } catch (Exception e) {
                        AppDelegate.LogE(e);
                    }
                } else if (notificationModel.getNotification_type() == MEMBERSHIP_EXPIRED) {
                    intent = new Intent(context, UpgradeMembershipActivity.class);
                    try {
                        intent.putExtra(Tags.user_id, (notificationModel.getUser_id()));
                        intent.putExtra(Tags.notification_id, notificationModel.id);
                        intent.putExtra(Tags.FROM, Tags.notification);
                        context.startActivity(intent);
                    } catch (Exception e) {
                        AppDelegate.LogE(e);
                    }
                } else if (notificationModel.getNotification_type() == MEMBERSHIP_EXPIRING || notificationModel.getNotification_type() == UPGRADED_MEMBERSHIP) {
                    intent = new Intent(context, UpgradeMembershipActivity.class);
                    try {
                        intent.putExtra(Tags.user_id, (notificationModel.getUser_id()));
                        intent.putExtra(Tags.notification_id, notificationModel.id);
                        intent.putExtra(Tags.FROM, Tags.notification);
                        context.startActivity(intent);
                    } catch (Exception e) {
                        AppDelegate.LogE(e);
                    }
                }
            }
        });
    }

    public NotificationAdapter(FragmentActivity context, ArrayList<NotificationModel> notificationModelArrayList) {
        this.context = context;
        this.notificationModelArrayList = notificationModelArrayList;
        imageLoader.init(ImageLoaderConfiguration.createDefault(context));
    }


    @Override
    public int getItemCount() {
        return notificationModelArrayList.size();
    }

    class ViewHolder extends RecyclerView.ViewHolder {

        public TextView txt_c_vendor, txt_c_message, txt_c_dealtitle, txt_c_date;
        public LinearLayout ll_c_main;
        public CircleImageView img_circle_notification_image;
        android.widget.ImageView img_loading1;

        public ViewHolder(View itemView) {
            super(itemView);
            img_loading1 = (android.widget.ImageView) itemView.findViewById(R.id.img_loading1);
            txt_c_vendor = (TextView) itemView.findViewById(R.id.txt_c_vendor);
            txt_c_message = (TextView) itemView.findViewById(R.id.txt_c_message);
            txt_c_dealtitle = (TextView) itemView.findViewById(R.id.txt_c_dealtitle);
            txt_c_date = (TextView) itemView.findViewById(R.id.txt_c_date);
            img_circle_notification_image = (CircleImageView) itemView.findViewById(R.id.img_circle_notification_image);
            ll_c_main = (LinearLayout) itemView.findViewById(R.id.ll_c_main);
        }
    }
}