package com.craftedeals.adapters;

import android.app.Activity;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;

import com.craftedeals.Models.FavBeveragesModel;
import com.craftedeals.R;

import java.util.ArrayList;

import carbon.widget.TextView;


public class AlertAdapter extends ArrayAdapter<FavBeveragesModel> {

    private final Activity context;
    ArrayList<FavBeveragesModel> title;
    Integer image;

    public AlertAdapter(Activity context, ArrayList<FavBeveragesModel> title) {
        super(context, R.layout.dialog_listitem, title);
        this.context = context;
        this.title = title;
    }

    @Override
    public View getView(int position, View view, ViewGroup parent) {
        LayoutInflater inflater = context.getLayoutInflater();
        View rowView = inflater.inflate(R.layout.dialog_listitem, null, true);
        TextView txtTitle = (TextView) rowView.findViewById(R.id.txt_c_listvalue);
        txtTitle.setText(title.get(position).name);
        return rowView;
    }
}
