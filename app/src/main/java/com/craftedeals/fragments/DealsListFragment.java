package com.craftedeals.fragments;

import android.os.Bundle;
import android.os.Handler;
import android.os.Message;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.support.v7.widget.DefaultItemAnimator;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import com.craftedeals.AppDelegate;
import com.craftedeals.Models.HeroDealsModel;
import com.craftedeals.R;
import com.craftedeals.adapters.HeroDealsAdapter;
import com.craftedeals.interfaces.OnReciveServerResponse;

import java.util.ArrayList;


/**
 * Created by admin on 26-07-2016.
 */
public class DealsListFragment extends Fragment implements OnReciveServerResponse {
    RecyclerView recycler_deals_list;
    public static Handler mHandler;

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        return inflater.inflate(R.layout.deals_list, container, false);
    }

    @Override
    public void onViewCreated(View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);
        initView(view);
        setHandler();
    }

    private void setHandler() {
        mHandler = new Handler() {
            @Override
            public void dispatchMessage(Message msg) {
                super.dispatchMessage(msg);
                switch (msg.what) {
                    case 10:
                        AppDelegate.showProgressDialog(getActivity());
                        break;
                    case 11:
                        AppDelegate.hideProgressDialog(getActivity());
                        break;
                    case 1:

                        break;
                    case 2:
                        break;
                    case 3:
                        break;
                }
            }
        };
    }

    @Override
    public void onDestroyView() {
        super.onDestroyView();
        mHandler = null;
    }

    private void initView(View view) {
        recycler_deals_list = (RecyclerView) view.findViewById(R.id.recycler_deals_list);
    }

    @Override
    public void onActivityCreated(@Nullable Bundle savedInstanceState) {
        super.onActivityCreated(savedInstanceState);
        show_DealsList();
    }

   /* private void execute_teamList() {
        try {
            if (AppDelegate.haveNetworkConnection(getActivity(), true)) {
                ArrayList<PostAysnc_Model> mPostArrayList = new ArrayList<PostAysnc_Model>();
                AppDelegate.getInstance(getActivity()).setPostParamsSecond(mPostArrayList, Parameters.API_KEY, Parameters.API_KEY_VALUE);
                AppDelegate.getInstance(getActivity()).setPostParamsSecond(mPostArrayList, Parameters.user_id, new Prefs(getActivity()).getUserdata().userId);
                PostAsync mPostAsyncObj;
                mPostAsyncObj = new PostAsync(getActivity(), FitTeamFragments.this, ServerRequestConstants.TEAM_LIST,
                        mPostArrayList, null);
                AppDelegate.showProgressDialog(getActivity());
                mPostAsyncObj.execute();
            }
        } catch (Exception e) {
            AppDelegate.ShowDialog(getActivity(), "Please try again.", "Alert!!!");
        }
    }*/


    @Override
    public void setOnReciveResult(String apiName, String result) {
        AppDelegate.hideProgressDialog(getActivity());
        if (!AppDelegate.isValidString(result)) {
            AppDelegate.showToast(getActivity(), getActivity().getResources().getString(R.string.time_out));
            return;
        }
       /* if (apiName.equals(ServerRequestConstants.TEAM_LIST)) {
            parseFitTeamList(result);

        }*/
    }


    private void show_DealsList() {
        ArrayList<HeroDealsModel> team_list = new ArrayList<>();
        HeroDealsAdapter mAdapter = new HeroDealsAdapter(getActivity(), team_list);
        RecyclerView.LayoutManager mLayoutManager = new LinearLayoutManager(getActivity());
        recycler_deals_list.setLayoutManager(mLayoutManager);
        recycler_deals_list.setItemAnimator(new DefaultItemAnimator());
        recycler_deals_list.setAdapter(mAdapter);
    }

}
