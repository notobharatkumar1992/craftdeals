package com.craftedeals.fragments;

import android.content.Intent;
import android.os.Bundle;
import android.os.Handler;
import android.os.Message;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import com.craftedeals.AppDelegate;
import com.craftedeals.Async.PostAsync;
import com.craftedeals.Models.CityModel;
import com.craftedeals.Models.Fb_detail_GetSet;
import com.craftedeals.Models.MembershipModel;
import com.craftedeals.Models.PostAysnc_Model;
import com.craftedeals.Models.UserDataModel;
import com.craftedeals.R;
import com.craftedeals.Utils.Prefs;
import com.craftedeals.activities.DashBoardActivity;
import com.craftedeals.activities.LoginActivity;
import com.craftedeals.activities.MemberShipActivity;
import com.craftedeals.activities.MembershipMessageActivity;
import com.craftedeals.activities.SignUpActivity;
import com.craftedeals.constants.ServerRequestConstants;
import com.craftedeals.constants.Tags;
import com.craftedeals.interfaces.OnReciveServerResponse;
import com.craftedeals.parser.JSONParser;

import org.json.JSONObject;

import java.util.ArrayList;

import carbon.widget.TextView;

/**
 * Created by Bharat on 07/19/2016.
 */
public class MemberShipFreeFragment extends Fragment implements View.OnClickListener, OnReciveServerResponse
{

    private Prefs prefs;
    private int position = 0;
    public static String str_transaction_id = "";

    int login_from = 0;

    private TextView txt_c_plan_type, txt_c_plan, txt_c_get4, txt_c_get3, txt_c_get2, txt_c_get1;
    public static Handler mHandler;
    public MembershipModel membershipModel;

    public static MemberShipFreeFragment getMemberShipFreeFragment(int i)
    {
        MemberShipFreeFragment fragment = new MemberShipFreeFragment();
        Bundle bundle = new Bundle();
        bundle.putInt(Tags.POSITION, i);
        fragment.setArguments(bundle);
        return fragment;
    }


    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState)
    {
        return inflater.inflate(R.layout.membership_free_fragment, container, false);
    }

    @Override
    public void onViewCreated(View view, @Nullable Bundle savedInstanceState)
    {
        super.onViewCreated(view, savedInstanceState);
        prefs = new Prefs(getActivity());
        position = getArguments().getInt(Tags.POSITION);
        initView(view);
        setHandler();
        setValues();
    }

    private void setHandler()
    {
        mHandler = new Handler()
        {
            @Override
            public void dispatchMessage(Message msg)
            {
                super.dispatchMessage(msg);
                if (msg.what == 10)
                {
                    AppDelegate.showProgressDialog(getActivity());
                }
                else if (msg.what == 11)
                {
                    AppDelegate.hideProgressDialog(getActivity());
                }
                else if (msg.what == 12)
                {
                    setValues();
                }
                else if (msg.what == 13)
                {
                    callClicksAsync();
                }
            }
        };
    }

    private void setValues()
    {
        if (((MemberShipActivity) getActivity()).arrayMembership == null || ((MemberShipActivity) getActivity()).arrayMembership.size() == 0)
        {
            AppDelegate.LogE("setValues null array at MembershipFreeFragment => " + position);
            return;
        }
        if (position == 1)
        {
            membershipModel = ((MemberShipActivity) getActivity()).arrayMembership.get(0);
            txt_c_plan_type.setText(getString(R.string.starter));
            txt_c_plan.setText(getString(R.string.free_cap));
            txt_c_get1.setText(getString(R.string.get1_free));
            txt_c_get2.setText(getString(R.string.get2_free));
            txt_c_get3.setText(getString(R.string.get3_free));
            txt_c_get4.setText(getString(R.string.get4_free));
        }
        else if (position == 2 && ((MemberShipActivity) getActivity()).arrayMembership.get(1) != null)
        {
            membershipModel = ((MemberShipActivity) getActivity()).arrayMembership.get(1);
            txt_c_plan_type.setText(getString(R.string.hero_deal_membership));
            txt_c_plan.setText("$" + membershipModel.amount + "\n\n" + membershipModel.month_of_membership + " MONTH");
            txt_c_get1.setText(getString(R.string.get1_paid));
            txt_c_get2.setText(getString(R.string.get2_paid));
            txt_c_get3.setText(getString(R.string.get3_paid));
            txt_c_get4.setText(getString(R.string.get4_paid));
        }
        else if (position == 3 && ((MemberShipActivity) getActivity()).arrayMembership.get(2) != null)
        {
            membershipModel = ((MemberShipActivity) getActivity()).arrayMembership.get(2);
            txt_c_plan_type.setText(getString(R.string.hero_deal_membership));
            txt_c_plan.setText("$" + membershipModel.amount + "\n\n" + membershipModel.month_of_membership + " MONTH");
            txt_c_get1.setText(getString(R.string.get1_paid));
            txt_c_get2.setText(getString(R.string.get2_paid));
            txt_c_get3.setText(getString(R.string.get3_paid));
            txt_c_get4.setText(getString(R.string.get4_paid));
        }
    }

    private void initView(View view)
    {
        view.findViewById(R.id.txt_c_sign_up).setOnClickListener(this);
        txt_c_plan_type = view.findViewById(R.id.txt_c_plan_type);
        txt_c_plan = view.findViewById(R.id.txt_c_plan);
        txt_c_get4 = view.findViewById(R.id.txt_c_get4);
        txt_c_get3 = view.findViewById(R.id.txt_c_get3);
        txt_c_get2 = view.findViewById(R.id.txt_c_get2);
        txt_c_get1 = view.findViewById(R.id.txt_c_get1);
        login_from = ((MemberShipActivity) getActivity()).login_from;
    }

    @Override
    public void onClick(View v)
    {
        switch (v.getId())
        {
            case R.id.txt_c_sign_up:
                AppDelegate.LogT("position => " + position);
                if (position == 1)
                {
                    callClicksAsync();
                }
                else
                {
                    ((MemberShipActivity) getActivity()).onBuyMembership(membershipModel.amount, "Paid membership");
                }
                break;
        }
    }

    public void callClicksAsync()
    {
        Log.e("callClicksAsync","signup");
        try
        {
            if (AppDelegate.haveNetworkConnection(getActivity(), true))
            {
                ArrayList<PostAysnc_Model> mPostArrayList = new ArrayList<>();
                AppDelegate.getInstance(getActivity()).setPostParamsSecond(mPostArrayList, Tags.API_KEY, Tags.API_KEY_VALUE);
                AppDelegate.getInstance(getActivity()).setPostParamsSecond(mPostArrayList, Tags.device_id, prefs.getGCMtokenfromTemp());
                AppDelegate.getInstance(getActivity()).setPostParamsSecond(mPostArrayList, Tags.device_type, AppDelegate.DEVICE_TYPE, ServerRequestConstants.Key_PostintValue);
                AppDelegate.getInstance(getActivity()).setPostParamsSecond(mPostArrayList, Tags.role, AppDelegate.BUYER, ServerRequestConstants.Key_PostintValue);
                AppDelegate.getInstance(getActivity()).setPostParamsSecond(mPostArrayList, Tags.membership_id, membershipModel.membership_id + "");

                if (position != 1)
                {
                    // TODO: change it before live
                    AppDelegate.getInstance(getActivity()).setPostParamsSecond(mPostArrayList, Tags.amount, membershipModel.amount + "");
                    //AppDelegate.getInstance(getActivity()).setPostParamsSecond(mPostArrayList, Tags.amount, "1");
                    AppDelegate.getInstance(getActivity()).setPostParamsSecond(mPostArrayList, "paypal_id", str_transaction_id + "");
                }

                PostAsync mPostAsyncObj;
                if (((MemberShipActivity) getActivity()).type == AppDelegate.DATA_TYPE_FB)
                {
                    Fb_detail_GetSet fbUserData = ((MemberShipActivity) getActivity()).fbUserData;
                    AppDelegate.getInstance(getActivity()).setPostParamsSecond(mPostArrayList, Tags.email, fbUserData.emailid);
                    AppDelegate.getInstance(getActivity()).setPostParamsSecond(mPostArrayList, Tags.name, fbUserData.firstname);
                    AppDelegate.getInstance(getActivity()).setPostParamsSecond(mPostArrayList, Tags.last_name, fbUserData.last_name);
                    AppDelegate.getInstance(getActivity()).setPostParamsSecond(mPostArrayList, Tags.heard_about_us, fbUserData.heard_about_us);
                    AppDelegate.getInstance(getActivity()).setPostParamsSecond(mPostArrayList, Tags.dob, fbUserData.bitrhday);

                    AppDelegate.getInstance(getActivity()).setPostParamsSecond(mPostArrayList, Tags.social_id, fbUserData.social_id + "");
                    if (AppDelegate.isValidString(fbUserData.profile_pic))
                    {
                        AppDelegate.getInstance(getActivity()).setPostParamsSecond(mPostArrayList, Tags.user_image, fbUserData.profile_pic + "");
                    }
                    mPostAsyncObj = new PostAsync(getActivity(), this, ServerRequestConstants.SIGNUP_FACEBOOK, mPostArrayList, this);

                }
                else
                {
                    UserDataModel userDataModel = ((MemberShipActivity) getActivity()).userDataModel;
                    AppDelegate.getInstance(getActivity()).setPostParamsSecond(mPostArrayList, Tags.email, userDataModel.email);
                    AppDelegate.getInstance(getActivity()).setPostParamsSecond(mPostArrayList, Tags.name, userDataModel.name);
                    AppDelegate.getInstance(getActivity()).setPostParamsSecond(mPostArrayList, Tags.last_name, userDataModel.last_names);
                    AppDelegate.getInstance(getActivity()).setPostParamsSecond(mPostArrayList, Tags.password, userDataModel.password);
                    AppDelegate.getInstance(getActivity()).setPostParamsSecond(mPostArrayList, Tags.beer_type_id, userDataModel.beer_id, ServerRequestConstants.Key_PostintValue);
                    AppDelegate.getInstance(getActivity()).setPostParamsSecond(mPostArrayList, Tags.brewery_type_id, userDataModel.fav_brewery, ServerRequestConstants.Key_PostintValue);
                    AppDelegate.getInstance(getActivity()).setPostParamsSecond(mPostArrayList, Tags.contact_number, userDataModel.contact_number, ServerRequestConstants.Key_PostintValue);
                    AppDelegate.getInstance(getActivity()).setPostParamsSecond(mPostArrayList, Tags.address, userDataModel.address);
                    AppDelegate.getInstance(getActivity()).setPostParamsSecond(mPostArrayList, Tags.city_id, Integer.parseInt(userDataModel.city_id), ServerRequestConstants.Key_PostintValue);
                    AppDelegate.getInstance(getActivity()).setPostParamsSecond(mPostArrayList, Tags.state_id, Integer.parseInt(userDataModel.state_id), ServerRequestConstants.Key_PostintValue);
                    AppDelegate.getInstance(getActivity()).setPostParamsSecond(mPostArrayList, Tags.dob, userDataModel.dob);
                    AppDelegate.getInstance(getActivity()).setPostParamsSecond(mPostArrayList, Tags.heard_about_us, userDataModel.heard_about_us);

                    if (AppDelegate.isValidString(userDataModel.file_path))
                    {
                        AppDelegate.getInstance(getActivity()).setPostParamsSecond(mPostArrayList, Tags.user_image, userDataModel.file_path + "", ServerRequestConstants.Key_PostFileValue);
                    }
                    mPostAsyncObj = new PostAsync(getActivity(), this, ServerRequestConstants.SIGN_UP, mPostArrayList, this);

                }
                // PostAsync mPostAsyncObj = new PostAsync(getActivity(), this, ServerRequestConstants.SIGNUP_FACEBOOK, mPostArrayList, this);
                mHandler.sendEmptyMessage(10);
                mPostAsyncObj.execute();
            }
        }
        catch (Exception e)
        {
            AppDelegate.LogE(e);
        }
    }


    @Override
    public void setOnReciveResult(String apiName, String result)
    {
        mHandler.sendEmptyMessage(11);
        if (!AppDelegate.isValidString(result))
        {
            AppDelegate.showToast(getActivity(), getString(R.string.time_out));
            return;
        }
        if (apiName.equalsIgnoreCase(ServerRequestConstants.SIGNUP_FACEBOOK))
        {
            parseSignUpFB(result);
        }
        else if (apiName.equalsIgnoreCase(ServerRequestConstants.SIGN_UP))
        {
            parseSignUpFB(result);
        }
    }

    private void parseSignUpFB(String result)
    {
        try
        {
            JSONObject jsonObject = new JSONObject(result);
            if (jsonObject.getString(Tags.status).equalsIgnoreCase("1"))
            {
                AppDelegate.showToast(getActivity(), JSONParser.getString(jsonObject, Tags.message), 1);
                new Prefs(getActivity()).putStringValueinTemp(Tags.EMAIL, "");
                new Prefs(getActivity()).putStringValueinTemp(Tags.PASSWORD, "");
                new Prefs(getActivity()).putStringValueinTemp(Tags.social_id, "");

                prefs.putBooleanValue(Tags.remember, true);

                if (((MemberShipActivity) getActivity()).type == AppDelegate.DATA_TYPE_FB)
                {
                    Fb_detail_GetSet fbUserData = ((MemberShipActivity) getActivity()).fbUserData;
                    prefs.putStringValueinTemp(Tags.PASSWORD, "");
                    prefs.putStringValueinTemp(Tags.EMAIL, fbUserData.emailid);
                    prefs.putStringValueinTemp(Tags.social_id, fbUserData.social_id + "");
                }
                else
                {
                    UserDataModel userDataModel = ((MemberShipActivity) getActivity()).userDataModel;
                    prefs.putStringValueinTemp(Tags.social_id, "");
                    prefs.putStringValueinTemp(Tags.EMAIL, userDataModel.email);
                    prefs.putStringValueinTemp(Tags.PASSWORD, userDataModel.password);
                }

                JSONObject object = jsonObject.getJSONObject(Tags.response);
                UserDataModel userDataModel = new UserDataModel();
                userDataModel.id = JSONParser.getInt(object, Tags.id);
                userDataModel.name = JSONParser.getString(object, Tags.name);
                userDataModel.first_names = JSONParser.getString(object, Tags.name);
                userDataModel.last_names = JSONParser.getString(object, Tags.last_name);
                userDataModel.email = JSONParser.getString(object, Tags.email);
                userDataModel.contact_number = JSONParser.getString(object, Tags.contact_number);
                userDataModel.membership_id = JSONParser.getInt(object, Tags.membership_id);
                userDataModel.image = JSONParser.getString(object, Tags.image);
                userDataModel.device_id = JSONParser.getString(object, Tags.device_id);
                userDataModel.device_type = JSONParser.getInt(object, Tags.device_type);
                userDataModel.created = JSONParser.getString(object, Tags.created);
                userDataModel.address = JSONParser.getString(object, Tags.address);
                userDataModel.address = JSONParser.getString(object, Tags.address);
                userDataModel.fav_brewery = JSONParser.getString(object, Tags.brewery_type_id);

                if (object.has(Tags.user_range))
                {
                    userDataModel.user_range = JSONParser.getInt(object, Tags.user_range);
                }
                if (object.has(Tags.heard_about_us))
                {
                    userDataModel.heard_about_us = JSONParser.getString(object, Tags.heard_about_us);
                }
                if (object.has(Tags.membership_start_date) && object.has(Tags.membership_end_date))
                {
                    userDataModel.membership_start_date = JSONParser.getString(object, Tags.membership_start_date);
                    userDataModel.membership_end_date = JSONParser.getString(object, Tags.membership_end_date);
                }
                if ((object.has(Tags.state)) && AppDelegate.isValidString(object.getString(Tags.state)))
                {
                    JSONObject state = object.getJSONObject(Tags.state);
                    userDataModel.province = JSONParser.getString(state, Tags.region_name);
                    userDataModel.state_id = JSONParser.getInt(state, Tags.id) + "";
                }
                if (object.has(Tags.city) && AppDelegate.isValidString(object.getString(Tags.city)))
                {
                    JSONObject city = object.getJSONObject(Tags.city);
                    userDataModel.city = JSONParser.getString(city, Tags.name);
                    userDataModel.city_id = JSONParser.getInt(city, Tags.id) + "";
                    CityModel cityModel = new CityModel();
                    cityModel.lat = JSONParser.getString(city, Tags.latitude);
                    cityModel.lng = JSONParser.getString(city, Tags.longitude);
                    cityModel.location_type = Tags.other;
                    cityModel.id = JSONParser.getInt(city, Tags.id) + "";
                    cityModel.name = JSONParser.getString(city, Tags.name);
                }
                prefs.setUserData(userDataModel);
                if (login_from == AppDelegate.LOGIN_FROM_VENDOR_PROFILE)
                {
                    Intent intent = new Intent(getActivity(), MembershipMessageActivity.class);
                    intent.putExtra(Tags.login_from, login_from);
                    intent.putExtra(Tags.membership_id, userDataModel.membership_id);
                    if (membershipModel != null)
                    {
                        intent.putExtra(Tags.membership_amount, membershipModel.amount);
                    }
                    startActivity(intent);
                    if (DashBoardActivity.mainActivity != null)
                    {
                        DashBoardActivity.mainActivity.finish();
                    }
                    if (LoginActivity.mActivity != null)
                    {
                        LoginActivity.mActivity.finish();
                    }
                    if (SignUpActivity.mActivity != null)
                    {
                        SignUpActivity.mActivity.finish();
                    }
                }
                else
                {
                    if (DashBoardActivity.mainActivity != null)
                    {
                        DashBoardActivity.mainActivity.finish();
                    }
                    Intent intent = new Intent(getActivity(), MembershipMessageActivity.class);
                    intent.putExtra(Tags.login_from, login_from);
                    intent.putExtra(Tags.membership_id, userDataModel.membership_id);
                    if (membershipModel != null)
                    {
                        intent.putExtra(Tags.membership_amount, membershipModel.amount);
                    }
                    startActivity(intent);
                    if (LoginActivity.mActivity != null)
                    {
                        LoginActivity.mActivity.finish();
                    }
                    if (SignUpActivity.mActivity != null)
                    {
                        SignUpActivity.mActivity.finish();
                    }
                }
//                    } else {
//                        startActivity(new Intent(getActivity(), LoginActivity.class));
//                        if (SignUpActivity.mActivity != null)
//                            SignUpActivity.mActivity.finish();
//                    }
                getActivity().finish();
//                }
            }
            else
            {
                AppDelegate.showAlert(getActivity(), jsonObject.getString(Tags.message));
            }
        }
        catch (Exception e)
        {
            AppDelegate.LogE(e);
            AppDelegate.showAlert(getActivity(), getString(R.string.try_again));
        }
    }
}
