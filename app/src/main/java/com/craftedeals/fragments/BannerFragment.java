package com.craftedeals.fragments;

import android.graphics.Bitmap;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;

import com.craftedeals.AppDelegate;
import com.craftedeals.R;
import com.craftedeals.constants.Tags;
import com.nostra13.universalimageloader.core.DisplayImageOptions;
import com.nostra13.universalimageloader.core.ImageLoaderConfiguration;
import com.nostra13.universalimageloader.core.assist.FailReason;
import com.nostra13.universalimageloader.core.listener.ImageLoadingListener;

import carbon.widget.TextView;

public class BannerFragment extends Fragment {

    private View rootview;
    private Bundle bundle;
    ImageView image;
    TextView txt_c_news_title;
    com.nostra13.universalimageloader.core.ImageLoader imageLoader = com.nostra13.universalimageloader.core.ImageLoader.getInstance();
    DisplayImageOptions options = new DisplayImageOptions.Builder().cacheInMemory(true)
            .cacheOnDisc(true).resetViewBeforeLoading(true)/*.showImageForEmptyUri(R.drawable)*/.
                    build();
    private String imageString;

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        rootview = inflater.inflate(R.layout.banner_image, container, false);
        bundle = getArguments();
        imageString = bundle.getString(Tags.DATA);
        imageLoader.init(ImageLoaderConfiguration.createDefault(getActivity()));
        return rootview;
    }

    @Override
    public void onViewCreated(View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);
        image = (ImageView) rootview.findViewById(R.id.img_c_banner_image);

        if (AppDelegate.isValidString(imageString))
            imageLoader.loadImage(imageString, options, new ImageLoadingListener() {
                @Override
                public void onLoadingStarted(String imageUri, View view) {
                }

                @Override
                public void onLoadingFailed(String imageUri, View view, FailReason failReason) {
                }

                @Override
                public void onLoadingComplete(String imageUri, View view, Bitmap bitmap) {
                    image.setImageBitmap(bitmap);
                    image.invalidate();
                }

                @Override
                public void onLoadingCancelled(String imageUri, View view) {
                }
            });
    }

}

