package com.craftedeals.fragments;

import android.content.DialogInterface;
import android.content.Intent;
import android.os.Bundle;
import android.os.Handler;
import android.os.Message;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.support.v7.app.AlertDialog;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import com.craftedeals.AppDelegate;
import com.craftedeals.Async.PostAsync;
import com.craftedeals.Models.MembershipModel;
import com.craftedeals.Models.PostAysnc_Model;
import com.craftedeals.Models.UserDataModel;
import com.craftedeals.R;
import com.craftedeals.Utils.Prefs;
import com.craftedeals.activities.MembershipMessageActivity;
import com.craftedeals.activities.UpgradePremiumMemberShipActivity;
import com.craftedeals.constants.ServerRequestConstants;
import com.craftedeals.constants.Tags;
import com.craftedeals.interfaces.OnReciveServerResponse;
import com.craftedeals.parser.JSONParser;

import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;

import carbon.widget.TextView;

/**
 * Created by Heena on 30-Jan-17.
 */

public class UpgradePremiumFragment extends Fragment implements OnReciveServerResponse, View.OnClickListener
{
    public MembershipModel membershipModel;
    public static Handler mHandler;
    private TextView txt_c_plan_type, txt_c_plan, txt_c_submit, txt_c_title, txt_c_get4, txt_c_get3, txt_c_get2, txt_c_get1;
    UserDataModel userDataModel;

    private int position = 0;
    public static String str_transaction_id = "";

    public static UpgradePremiumFragment getMemberShipFreeFragment(int i)
    {
        UpgradePremiumFragment fragment = new UpgradePremiumFragment();
        Bundle bundle = new Bundle();
        bundle.putInt(Tags.POSITION, i);
        fragment.setArguments(bundle);
        return fragment;
    }

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState)
    {
        return inflater.inflate(R.layout.paid_upgrade_membership, container, false);
    }

    @Override
    public void onViewCreated(View view, @Nullable Bundle savedInstanceState)
    {
        super.onViewCreated(view, savedInstanceState);
        position = getArguments().getInt(Tags.POSITION);
        initView(view);
        setHandler();
    }

    private void setHandler()
    {
        mHandler = new Handler()
        {
            @Override
            public void dispatchMessage(Message msg)
            {
                super.dispatchMessage(msg);
                switch (msg.what)
                {
                    case 10:
                        AppDelegate.showProgressDialog(getActivity());
                        break;
                    case 11:
                        AppDelegate.hideProgressDialog(getActivity());
                        break;
                    case 9:
                        setMemberShipData();
                        break;

                    case 5:
                        Log.e("result", str_transaction_id);
                        execute_upgradeMembershipAsync(str_transaction_id);
                        break;
                }
            }
        };
    }

    private void setMemberShipData()
    {
        if (((UpgradePremiumMemberShipActivity) getActivity()).arrayMembership == null || ((UpgradePremiumMemberShipActivity) getActivity()).arrayMembership.size() == 0)
        {
            return;
        }
        AppDelegate.LogT("UpgradePremiumFragment setMemberShipData => " + position);
        if (position == 1)
        {
            membershipModel = ((UpgradePremiumMemberShipActivity) getActivity()).arrayMembership.get(1);
            txt_c_plan_type.setText(getString(R.string.hero_deal_membership));
            txt_c_plan.setText("$" + membershipModel.amount + "\n\n" + membershipModel.month_of_membership + " MONTH");
        }
        else if (position == 2)
        {
            membershipModel = ((UpgradePremiumMemberShipActivity) getActivity()).arrayMembership.get(2);
            txt_c_plan_type.setText(getString(R.string.hero_deal_membership));
            txt_c_plan.setText("$" + membershipModel.amount + "\n\n" + membershipModel.month_of_membership + " MONTH");
        }
    }

    private void initView(View view)
    {
        view.findViewById(R.id.toolbar).setVisibility(View.GONE);
        view.findViewById(R.id.img_c_back).setOnClickListener(this);
        txt_c_plan = (TextView) view.findViewById(R.id.txt_c_plan);
        txt_c_plan_type = (TextView) view.findViewById(R.id.txt_c_plan_type);
        txt_c_submit = (TextView) view.findViewById(R.id.txt_c_submit);
        txt_c_submit.setOnClickListener(this);
        txt_c_title = (TextView) view.findViewById(R.id.txt_c_title);
        userDataModel = new Prefs(getActivity()).getUserdata();
        txt_c_get4 = (TextView) view.findViewById(R.id.txt_c_get4);
        txt_c_get3 = (TextView) view.findViewById(R.id.txt_c_get3);
        txt_c_get2 = (TextView) view.findViewById(R.id.txt_c_get2);
        txt_c_get1 = (TextView) view.findViewById(R.id.txt_c_get1);
        txt_c_title.setText("UPGRADE");
        txt_c_submit.setText("UPGRADE");
        if (userDataModel != null && userDataModel.membership_id == 1)
        {
            if (AppDelegate.isValidString(userDataModel.membership_start_date))
            {
                txt_c_get1.setText(getString(R.string.get1_upgrade));
                txt_c_get2.setText(getString(R.string.get2_upgrade));
                txt_c_get3.setText(getString(R.string.get3_upgrade));
                txt_c_get4.setText(getString(R.string.get4_upgrade));
            }
            else
            {
                txt_c_get1.setText(getString(R.string.get1_paid));
                txt_c_get2.setText(getString(R.string.get2_paid));
                txt_c_get3.setText(getString(R.string.get3_paid));
                txt_c_get4.setText(getString(R.string.get4_paid));
            }
        }
        setMemberShipData();
    }

    @Override
    public void setOnReciveResult(String apiName, String result)
    {
        if (mHandler == null)
        {
            return;
        }
        mHandler.sendEmptyMessage(11);
        if (!AppDelegate.isValidString(result))
        {
            AppDelegate.showToast(getActivity(), getString(R.string.time_out));
            return;
        }
        else if (apiName.equalsIgnoreCase(ServerRequestConstants.UPGRADE_MEMBERSHIP))
        {
            parseChangePassword(result);
        }
    }

    private void parseChangePassword(String result)
    {
        try
        {
            JSONObject jsonObject = new JSONObject(result);
            if (jsonObject.getInt(Tags.status) == 1)
            {
                AppDelegate.showToast(getActivity(), jsonObject.getString(Tags.message));
                JSONObject data = jsonObject.getJSONObject("data");
                UserDataModel userDataModel = new Prefs(getActivity()).getUserdata();
                userDataModel.membership_start_date = JSONParser.getString(data, Tags.start_date);
                userDataModel.membership_end_date = JSONParser.getString(data, Tags.end_date);
                userDataModel.membership_id = JSONParser.getInt(data, Tags.membership_id);
                new Prefs(getActivity()).setUserData(userDataModel);
                if (userDataModel.membership_id != 1)
                {
                    Intent intent = new Intent(getActivity(), MembershipMessageActivity.class);
                    intent.putExtra(Tags.membership_id, userDataModel.membership_id);
                    intent.putExtra(Tags.membership_amount, membershipModel.amount);
                    intent.putExtra(Tags.login_from, 0);
                    startActivity(intent);
                }
                getActivity().finish();
            }
            else
            {
                AppDelegate.showToast(getActivity(), jsonObject.getString(Tags.message));
            }
        }
        catch (JSONException e)
        {
            e.printStackTrace();
        }
    }

    @Override
    public void onClick(View v)
    {
        switch (v.getId())
        {
            case R.id.img_c_back:
                getActivity().finish();
                break;
            case R.id.txt_c_submit:
                AlertDialog.Builder mAlert = new AlertDialog.Builder(getActivity());
                mAlert.setCancelable(false);
                mAlert.setTitle("Upgrade Membership");
                mAlert.setMessage("Are you sure, you want to upgrade your account?");
                mAlert.setPositiveButton("OK", new DialogInterface.OnClickListener()
                {
                    @Override
                    public void onClick(DialogInterface dialog, int which)
                    {
                        ((UpgradePremiumMemberShipActivity) getActivity()).onBuyPressed(membershipModel.amount, membershipModel.name);
                        dialog.dismiss();
                    }
                });
                mAlert.setNegativeButton("Cancel", new DialogInterface.OnClickListener()
                {
                    @Override
                    public void onClick(DialogInterface dialog, int which)
                    {
                        dialog.dismiss();
                    }
                });
                mAlert.show();
                break;
        }
    }

    public void execute_upgradeMembershipAsync(String paypal_id)
    {
        try
        {
            if (AppDelegate.haveNetworkConnection(getActivity(), true))
            {
                ArrayList<PostAysnc_Model> mPostArrayList = new ArrayList<>();
                PostAsync mPostAsyncObj;
                AppDelegate.getInstance(getActivity()).setPostParamsSecond(mPostArrayList, Tags.API_KEY, Tags.API_KEY_VALUE);
                AppDelegate.getInstance(getActivity()).setPostParamsSecond(mPostArrayList, Tags.user_id, new Prefs(getActivity()).getUserdata().id);

                AppDelegate.getInstance(getActivity()).setPostParamsSecond(mPostArrayList, Tags.membership_id, membershipModel.membership_id + "");

                // TODO: change it before live
                AppDelegate.getInstance(getActivity()).setPostParamsSecond(mPostArrayList, Tags.total_price, membershipModel.amount);
                //AppDelegate.getInstance(getActivity()).setPostParamsSecond(mPostArrayList, Tags.total_price, "1");
                AppDelegate.getInstance(getActivity()).setPostParamsSecond(mPostArrayList, Tags.paypal_id, paypal_id);

                mPostAsyncObj = new PostAsync(getActivity(), this, ServerRequestConstants.UPGRADE_MEMBERSHIP,
                        mPostArrayList, null);
                mHandler.sendEmptyMessage(10);
                mPostAsyncObj.execute();
            }
        }
        catch (Exception e)
        {
            AppDelegate.LogE(e);
            AppDelegate.showToast(getActivity(), getString(R.string.try_again));
        }
    }
}