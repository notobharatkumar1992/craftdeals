package com.craftedeals.fragments;

import android.content.Context;
import android.location.Location;
import android.location.LocationManager;
import android.os.Bundle;
import android.os.Handler;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.ImageView;
import android.widget.RelativeLayout;
import android.widget.TextView;

import com.craftedeals.AppDelegate;
import com.craftedeals.R;
import com.craftedeals.Utils.Prefs;
import com.craftedeals.constants.Tags;
import com.craftedeals.interfaces.OnReciveServerResponse;
import com.google.android.gms.maps.CameraUpdate;
import com.google.android.gms.maps.CameraUpdateFactory;
import com.google.android.gms.maps.GoogleMap;
import com.google.android.gms.maps.OnMapReadyCallback;
import com.google.android.gms.maps.SupportMapFragment;
import com.google.android.gms.maps.model.CameraPosition;
import com.google.android.gms.maps.model.LatLng;


/**
 * Created by admin on 26-07-2016.
 */
public class DealsMapFragment extends Fragment implements OnClickListener, AdapterView.OnItemClickListener, OnReciveServerResponse{
    private Bundle bundle;
    public  Handler mHandler;
    private SupportMapFragment fragment;
    private GoogleMap googleMap;

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {

        return  inflater.inflate(R.layout.deals_map, container, false);
    }

    @Override
    public void onViewCreated(View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);
        fragment = SupportMapFragment.newInstance();
        getChildFragmentManager().beginTransaction()
                .replace(R.id.map_Frame, fragment, "MAP1").addToBackStack(null)
                .commit();
        fragment.getMapAsync(new OnMapReadyCallback() {
            @Override
            public void onMapReady(GoogleMap googleMap) {
                DealsMapFragment.this.googleMap = googleMap;
                AppDelegate.LogT("Google Map ==" + DealsMapFragment.this.googleMap);
            }
        });
        /*initView();
        setHandler();*/
    }


  /*  private void setHandler() {
        mHandler = new Handler() {
            @Override
            public void dispatchMessage(Message msg) {
                super.dispatchMessage(msg);
                if (msg.what == 10) {
                    AppDelegate.showProgressDialog(getActivity());
                } else if (msg.what == 11) {
                    AppDelegate.hideProgressDialog(getActivity());
                } else if (msg.what == 1) {
                   // if (shouldUpdate) {
                        execute_teamList();
                       // shouldUpdate = false;
                   // }
                }
            }
        };
    }*/

    @Override
    public void onDestroyView() {
        super.onDestroyView();
        mHandler = null;
    }

    @Override
    public void onActivityCreated(@Nullable Bundle savedInstanceState) {
        super.onActivityCreated(savedInstanceState);

        new Handler().postDelayed(new Runnable() {
            @Override
            public void run() {
                showMap();
            }
        }, 1500);
    }
    private void showMap() {
        if (googleMap == null) {
            return;
        }
        googleMap.setMyLocationEnabled(true);
        googleMap.getUiSettings().setMapToolbarEnabled(false);
        googleMap.getUiSettings().setZoomControlsEnabled(false);
        googleMap.getUiSettings().setCompassEnabled(false);
        googleMap.setMapType(GoogleMap.MAP_TYPE_NORMAL);
        googleMap.moveCamera(CameraUpdateFactory.zoomIn());

    }
   /* private void execute_teamList() {
        try {
            if (AppDelegate.haveNetworkConnection(getActivity(), true)) {
                ArrayList<PostAysnc_Model> mPostArrayList = new ArrayList<PostAysnc_Model>();
                AppDelegate.getInstance(getActivity()).setPostParamsSecond(mPostArrayList, Parameters.API_KEY, Parameters.API_KEY_VALUE);
                AppDelegate.getInstance(getActivity()).setPostParamsSecond(mPostArrayList, Parameters.user_id, new Prefs(getActivity()).getUserdata().userId);
                PostAsync mPostAsyncObj;
                mPostAsyncObj = new PostAsync(getActivity(), FitTeamFragments.this, ServerRequestConstants.TEAM_LIST,
                        mPostArrayList, null);
                AppDelegate.showProgressDialog(getActivity());
                mPostAsyncObj.execute();
            }
        } catch (Exception e) {
            AppDelegate.ShowDialog(getActivity(), "Please try again.", "Alert!!!");
        }
    }*/

    @Override
    public void onClick(View v) {
        switch (v.getId()) {

        }
    }

    @Override
    public void onItemClick(AdapterView<?> parent, View view, int position, long id) {
        switch (view.getId()) {

        }
    }



    @Override
    public void setOnReciveResult(String apiName, String result) {
        AppDelegate.hideProgressDialog(getActivity());
        if (!AppDelegate.isValidString(result)) {
            AppDelegate.showToast(getActivity(), getActivity().getResources().getString(R.string.time_out));
            return;
        }
       /* if (apiName.equals(ServerRequestConstants.TEAM_LIST)) {
            parseFitTeamList(result);

        }*/
    }



   /* private void showTeamlist(ArrayList<TeamListModel> team_listArray) {
        TeamListAdapter mAdapter = new TeamListAdapter(getActivity(), team_listArray);
        RecyclerView.LayoutManager mLayoutManager = new LinearLayoutManager(getActivity());
        team_list.setLayoutManager(mLayoutManager);
        team_list.setItemAnimator(new DefaultItemAnimator());
        team_list.setAdapter(mAdapter);
    }
*/

}
