package com.craftedeals.Async;

import android.content.Context;
import android.os.AsyncTask;

import com.craftedeals.AppDelegate;
import com.craftedeals.Models.Place;
import com.craftedeals.constants.Tags;
import com.craftedeals.interfaces.GetLocationResult;
import com.google.android.gms.maps.model.LatLng;

import org.apache.http.HttpEntity;
import org.apache.http.HttpResponse;
import org.apache.http.client.ClientProtocolException;
import org.apache.http.client.HttpClient;
import org.apache.http.client.methods.HttpPost;
import org.apache.http.impl.client.DefaultHttpClient;
import org.apache.http.util.EntityUtils;
import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.io.IOException;


@SuppressWarnings("deprecation")
public class GetLatLongFromPlaceIdAsync extends AsyncTask<Void, Void, Void> {

    private String placeId = "";
    private String getValue = "";
    private double lng = -1.0;
    private double lat = -1.0;
    private Context mContext;
    private GetLocationResult locationResult;
    private String apiname;

    public GetLatLongFromPlaceIdAsync(Context mContext,
                                      GetLocationResult locationResult, String apiname, String placeId) {
        this.mContext = mContext;
        this.placeId = placeId;
        this.locationResult = locationResult;
        this.apiname = apiname;
    }

    @Override
    protected Void doInBackground(Void... params) {
        String uri = "https://maps.googleapis.com/maps/api/place/details/json?placeid="
                + placeId + "&key=" + AppDelegate.API_KEY_2;
        // String uri = "http://maps.google.com/maps/api/geocode/json?address="
        // + youraddress + "&sensor=false";

        HttpClient httpclient = new DefaultHttpClient(
                AppDelegate.getHttpParameters());

        AppDelegate.LogUA("GatLatLngFromPlaceId url " + uri);

        HttpPost httppost;
        HttpResponse response;
        try {
            httppost = new HttpPost(uri);
            response = httpclient.execute(httppost);
            HttpEntity entity = response.getEntity();
            getValue = EntityUtils.toString(entity);
        } catch (ClientProtocolException e) {
            AppDelegate.LogE(e);
            locationResult.onReciveApiResult(apiname, Tags.ERROR, new LatLng(0, 0));
        } catch (IOException e) {
            AppDelegate.LogE(e);
            locationResult.onReciveApiResult(apiname, Tags.ERROR, new LatLng(0, 0));
        }
        return null;
    }

    @Override
    protected void onPostExecute(Void result) {
        super.onPostExecute(result);
        JSONObject jsonObject = new JSONObject();
        try {
            jsonObject = new JSONObject(getValue);
            lng = ((JSONObject) jsonObject.getJSONObject(Tags.result))
                    .getJSONObject(Tags.geometry).getJSONObject(Tags.location)
                    .getDouble(Tags.LNG);

            lat = ((JSONObject) jsonObject.getJSONObject(Tags.result))
                    .getJSONObject(Tags.geometry).getJSONObject(Tags.location)
                    .getDouble(Tags.LAT);

            Place place = null;
            try {
                // Create a JSON object hierarchy from the results
                JSONObject jsonObj = jsonObject.getJSONObject(Tags.result);
                place = new Place();
                place.icon = jsonObj.getString("icon");
//                place.name = jsonObj.getString("name");
                place.formatted_address = jsonObj.getString("formatted_address");

                place.latitude = jsonObj.getJSONObject(Tags.geometry).getJSONObject(Tags.location)
                        .getDouble(Tags.LAT);
                place.longitude = jsonObj.getJSONObject(Tags.geometry).getJSONObject(Tags.location)
                        .getDouble(Tags.LNG);
                if (jsonObj.has("formatted_phone_number")) {
                    place.formatted_phone_number = jsonObj
                            .getString("formatted_phone_number");
                }

                JSONArray addressArray = jsonObj.getJSONArray(Tags.address_components);
                AppDelegate.LogT("addressArray size = " + addressArray.length());
                for (int i = 0; i < addressArray.length(); i++) {
                    JSONObject object = addressArray.getJSONObject(i);
                    String str_types = object.getJSONArray(Tags.types).getString(0);

                    AppDelegate.LogT("str_types = " + str_types);

                    if (str_types.equalsIgnoreCase(Tags.country)) {
                        place.country = object.getString(Tags.long_name);
                    } else if (str_types.equalsIgnoreCase(Tags.administrative_area_level_2)) {
                        place.city = object.getString(Tags.long_name);
                    } else if (str_types.equalsIgnoreCase(Tags.postal_code)) {
                        place.postal_code = object.getString(Tags.long_name);
                    } else if (str_types.equalsIgnoreCase(Tags.LOCALITY)) {
                    } else {
                        if (place.name != null
                                && !place.name.equalsIgnoreCase("")) {
                            place.name = place.name
                                    + ", "
                                    + object.getString(Tags.LONG_NAME);
                        } else {
                            place.name = object.getString(Tags.LONG_NAME);
                        }
                        AppDelegate.LogT("place.name = " + place.name + ", sfaf = " + object.getString(Tags.LONG_NAME));
                    }
                }
            } catch (JSONException e) {
                AppDelegate.LogE(e);
            }

            if (lat == -1.0 && lng == -1.0) {
                AppDelegate.LogE("value for lat long from add = " + getValue);
                AppDelegate.showToast(mContext, "Please make sure that, selected address is correct.");
            } else {
                locationResult.onReciveApiResult(apiname, Tags.SUCCESS,
                        place);
            }

        } catch (JSONException e) {
            AppDelegate.LogE(e);
            locationResult.onReciveApiResult(apiname, Tags.ERROR, new LatLng(0, 0));
        }
    }
}
