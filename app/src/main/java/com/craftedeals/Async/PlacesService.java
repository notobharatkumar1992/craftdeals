package com.craftedeals.Async;

import android.util.Log;

import com.craftedeals.AppDelegate;
import com.craftedeals.Models.Place;
import com.craftedeals.constants.Tags;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.net.HttpURLConnection;
import java.net.MalformedURLException;
import java.net.URL;
import java.net.URLConnection;
import java.net.URLEncoder;
import java.util.ArrayList;
import java.util.List;

/**
 * @author saxman
 */
public class PlacesService {
    private static final String LOG_TAG = "ExampleApp";

    private static final String PLACES_API_BASE = "https://maps.googleapis.com/maps/api/place";

    private static final String TYPE_AUTOCOMPLETE = "/autocomplete";
    private static final String TYPE_DETAILS = "/details";
    private static final String TYPE_SEARCH = "/search";

    private static final String OUT_JSON = "/json";

    // KEY!
    private static final String API_KEY = AppDelegate.API_KEY_2;

    /**
     * Keywprd search api for Location Placs Fragment
     *
     * @param input
     */
    public static void autocomplete(String input) {
        HttpURLConnection conn = null;
        StringBuilder jsonResults = new StringBuilder();
        try {
            StringBuilder sb = new StringBuilder(PLACES_API_BASE);
            sb.append(TYPE_AUTOCOMPLETE);
            sb.append(OUT_JSON);
            sb.append("?sensor=false");
            sb.append("&key=" + API_KEY);
            sb.append("&input=" + URLEncoder.encode(input, "utf8"));
            URL url = new URL(sb.toString());
            AppDelegate.Log(Tags.URL, "autocomplete url = " + url, 1);
            conn = (HttpURLConnection) url.openConnection();
            InputStreamReader in = new InputStreamReader(conn.getInputStream());

            int read;
            char[] buff = new char[1024];
            while ((read = in.read(buff)) != -1) {
                jsonResults.append(buff, 0, read);
            }
        } catch (MalformedURLException e) {
            Log.e(LOG_TAG, "Error processing Places API URL", e);
        } catch (IOException e) {
            Log.e(LOG_TAG, "Error connecting to Places API", e);
        } finally {
            if (conn != null) {
                conn.disconnect();
            }
        }
        try {
            JSONObject jsonObj = new JSONObject(jsonResults.toString());
            JSONArray predsJsonArray = jsonObj.getJSONArray("predictions");

            for (int i = 0; i < predsJsonArray.length(); i++) {
                Place place = new Place();
                place.reference = predsJsonArray.getJSONObject(i).getString(
                        "reference");
                place.id = predsJsonArray.getJSONObject(i)
                        .getString("place_id");
                place.name = predsJsonArray.getJSONObject(i).getString(
                        "description");
                JSONArray subArray = predsJsonArray.getJSONObject(i)
                        .getJSONArray("terms");

                boolean isFirst = true;
                for (int j = 0; j < subArray.length(); j++) {
                    if (isFirst) {
                        isFirst = false;
                        place.formatted_address = subArray.getJSONObject(j)
                                .getString("value");
                        place.name = subArray.getJSONObject(j).getString(
                                "value");
                    } else {
                        if (place.vicinity != null
                                && !place.vicinity.equalsIgnoreCase("")) {
                            place.vicinity = place.vicinity
                                    + ", "
                                    + subArray.getJSONObject(j).getString(
                                    "value");
                            place.add = place.vicinity;
                        } else {
                            place.vicinity = subArray.getJSONObject(j)
                                    .getString("value");
                        }
                    }
                }
//                AddLocationFragment.mapPlacesArray.add(place);
            }
        } catch (JSONException e) {
            AppDelegate.LogE(e);
        }
    }

    /**
     * Keyword search api for Location Map Fragment
     *
     * @param input
     */
    public static List<Place> autocompleteForMap(String input) {
        List<Place> mapPlacesArray = new ArrayList<>();
        HttpURLConnection conn = null;
        StringBuilder jsonResults = new StringBuilder();
        try {
            StringBuilder sb = new StringBuilder(PLACES_API_BASE);
            sb.append(TYPE_AUTOCOMPLETE);
            sb.append(OUT_JSON);
            sb.append("?sensor=false");
            sb.append("&key=" + API_KEY);
            sb.append("&input=" + URLEncoder.encode(input, "utf8") /*+ "%20aus"*/);
            sb.append("&components=country:aus&types=(cities)");
            URL url = new URL(sb.toString());
            AppDelegate.Log(Tags.URL_API, "place url " + url, 1);
            conn = (HttpURLConnection) url.openConnection();
            InputStreamReader in = new InputStreamReader(conn.getInputStream());

            int read;
            char[] buff = new char[1024];
            while ((read = in.read(buff)) != -1) {
                jsonResults.append(buff, 0, read);
            }
        } catch (MalformedURLException e) {
            Log.e(LOG_TAG, "Error processing Places API URL", e);
        } catch (IOException e) {
            Log.e(LOG_TAG, "Error connecting to Places API", e);
        } finally {
            if (conn != null) {
                conn.disconnect();
            }
        }

        try {
            JSONObject jsonObj = new JSONObject(jsonResults.toString());
            JSONArray predsJsonArray = jsonObj.getJSONArray("predictions");
            String str_status = jsonObj.getString("status");
            if (str_status.equalsIgnoreCase("ZERO_RESULTS")) {
                AppDelegate.zeroResult = true;
            } else {
                AppDelegate.zeroResult = false;
            }


            for (int i = 0; i < predsJsonArray.length(); i++) {
                Place place = new Place();
                place.reference = predsJsonArray.getJSONObject(i).getString(
                        "reference");
                place.id = predsJsonArray.getJSONObject(i)
                        .getString("place_id");

//                place.name = predsJsonArray.getJSONObject(i).getString(
//                        "description");

                JSONArray subArray = predsJsonArray.getJSONObject(i)
                        .getJSONArray("terms");

                boolean isFirst = true;
                for (int j = 0; j < subArray.length(); j++) {
                    if (subArray.length() == 1) {
                        if (isFirst) {
                            AppDelegate.LogT("subArray.length() >= 1 " + j);
                            isFirst = false;
                            place.name = subArray.getJSONObject(j).getString(
                                    "value");
                            place.vicinity = "";
                        } else {
                            AppDelegate.LogT("subArray.length() >= 1 else " + j);
                            if (place.vicinity != null
                                    && !place.vicinity.equalsIgnoreCase("")) {
                                place.vicinity = place.vicinity
                                        + ", "
                                        + subArray.getJSONObject(j).getString(
                                        "value");
                            } else {
                                place.vicinity = subArray.getJSONObject(j)
                                        .getString("value");
                            }
                        }
                    } else if (subArray.length() == 2) {
                        if (isFirst) {
                            AppDelegate.LogT("subArray.length() >= 2 " + j);
                            isFirst = false;
                            place.name = subArray.getJSONObject(j).getString(
                                    "value");
                        } else {
                            AppDelegate.LogT("subArray.length() >= 2 else " + j);
                            if (place.vicinity != null
                                    && !place.vicinity.equalsIgnoreCase("")) {
                                place.vicinity = place.vicinity
                                        + ", "
                                        + subArray.getJSONObject(j).getString(
                                        "value");
                            } else {
                                place.vicinity = subArray.getJSONObject(j)
                                        .getString("value");
                            }
                        }
                    } else if (subArray.length() == 3) {
                        if (isFirst) {
                            AppDelegate.LogT("subArray.length() >= 3 " + j);
                            isFirst = false;
                            place.name = subArray.getJSONObject(j).getString(
                                    "value");
                        } else {
                            AppDelegate.LogT("subArray.length() >= 3 else " + j);
                            if (place.vicinity != null
                                    && !place.vicinity.equalsIgnoreCase("")) {
                                place.vicinity = place.vicinity
                                        + ", "
                                        + subArray.getJSONObject(j).getString(
                                        "value");
                            } else {
                                place.vicinity = subArray.getJSONObject(j)
                                        .getString("value");
                            }
                        }
                    } else if (subArray.length() == 4) {
                        if (j < 2) {
                            AppDelegate.LogT("subArray.length() == 4 " + j);
                            if (place.name != null
                                    && !place.name.equalsIgnoreCase("")) {
                                place.name = place.name
                                        + ", "
                                        + subArray.getJSONObject(j).getString(
                                        "value");
                            } else {
                                place.name = subArray.getJSONObject(j)
                                        .getString("value");
                            }
                        } else {
                            AppDelegate.LogT("subArray.length() == 4 else " + j);
                            if (place.vicinity != null
                                    && !place.vicinity.equalsIgnoreCase("")) {
                                place.vicinity = place.vicinity
                                        + ", "
                                        + subArray.getJSONObject(j).getString(
                                        "value");
                            } else {
                                place.vicinity = subArray.getJSONObject(j)
                                        .getString("value");
                            }
                        }
                    } else if (subArray.length() >= 4) {
                        if (j < 2) {
                            AppDelegate.LogT("subArray.length() >= 4 " + j);
                            if (place.name != null
                                    && !place.name.equalsIgnoreCase("")) {
                                place.name = place.name
                                        + ", "
                                        + subArray.getJSONObject(j).getString(
                                        "value");
                            } else {
                                place.name = subArray.getJSONObject(j)
                                        .getString("value");
                            }
                        } else if (subArray.length() - 2 <= j) {
                            AppDelegate.LogT("subArray.length() >= 4 else " + j);
                            if (place.vicinity != null
                                    && !place.vicinity.equalsIgnoreCase("")) {
                                place.vicinity = place.vicinity
                                        + ", "
                                        + subArray.getJSONObject(j).getString(
                                        "value");
                            } else {
                                place.vicinity = subArray.getJSONObject(j)
                                        .getString("value");
                            }
                        }
                    }
                }

                AppDelegate.LogE("place.name => " + place.name);
                AppDelegate.LogE("place.vicinity => " + place.vicinity);
                mapPlacesArray.add(place);
            }
        } catch (JSONException e) {
            AppDelegate.LogE(e);
        }
        return mapPlacesArray;
    }

//    public static String executePlaceFundayReslt(String str_PostRequestURL, ArrayList<PostAysnc_Model> arr_PostModels, Fragment fragment) {
//
//        HttpClient mHttpClient = new DefaultHttpClient(PostAsync.getHttpParameters());
//        HttpContext mHttpContext = new BasicHttpContext();
//        HttpPost mHttpPost = new HttpPost(str_PostRequestURL);
//        HttpGet mHttpGet = new HttpGet(str_PostRequestURL);
//
//        for (int i = 0; i < arr_PostModels.size(); i++) {
//            if (i == 0) {
//                str_PostRequestURL = str_PostRequestURL + "?" + arr_PostModels.get(i).getStr_PostParamKey() + "=" + arr_PostModels.get(i).getObj_PostParamValue();
//            } else {
//                str_PostRequestURL = str_PostRequestURL + "&" + arr_PostModels.get(i).getStr_PostParamKey() + "=" + arr_PostModels.get(i).getObj_PostParamValue();
//            }
//        }
//        str_PostRequestURL = str_PostRequestURL.replace(" ", "%20");
//        mHttpGet = new HttpGet(str_PostRequestURL);
//        // Basic Authentifications
//        mHttpGet.addHeader(BasicScheme.authenticate(new UsernamePasswordCredentials(Credentials.BASIC_AUTH_USERNAME, Credentials.BASIC_AUTH_PASSWORD), "UTF-8", false));
//
//        // Common header used in api
//        mHttpGet.addHeader(new BasicHeader(Credentials.HEADER_KEY_1, Credentials.HEADER_VALUE_1));
//        mHttpGet.addHeader(new BasicHeader(Credentials.HEADER_KEY_2, Credentials.HEADER_VALUE_2));
////                    mHttpGet.addHeader(new BasicHeader(Credentials.HEADER_KEY_3, Credentials.HEADER_VALUE_3));
//        mHttpGet.addHeader(new BasicHeader(Credentials.HEADER_KEY_4, Credentials.HEADER_VALUE_4));
//        mHttpGet.addHeader(new BasicHeader(Credentials.HEADER_KEY_5, Credentials.HEADER_VALUE_5));
//        mHttpGet.addHeader(new BasicHeader(Credentials.HEADER_KEY_6, Credentials.HEADER_VALUE_6));
//        mHttpGet.addHeader(new BasicHeader(Credentials.HEADER_KEY_6, Credentials.HEADER_VALUE_6));
//        AppDelegate.LogUA(str_PostRequestURL);
//
//        String mPostResult = null;
//        try {
//            HttpResponse mHttpResponse = mHttpClient.execute(mHttpGet);
//
//            if (mHttpResponse != null) {
//                HttpEntity mHttpEntity = mHttpResponse.getEntity();
//                InputStream mInputStream = mHttpEntity.getContent();
//                if (mInputStream != null) {
//                    mPostResult = AppDelegate
//                            .convertStreamToString(mInputStream);
//                    AppDelegate.LogT("mPostResult = " + mPostResult);
//                } else {
//                    AppDelegate.LogE("mInputStream = null for api = "
//                            + str_PostRequestURL);
//                    return mPostResult;
//                }
//            } else {
//                AppDelegate.LogE("mHttpResponse = null for api = "
//                        + str_PostRequestURL);
//                return mPostResult;
//            }
//
//        } catch (IOException e) {
//            AppDelegate.LogE(e);
//        }
//        return mPostResult;
//    }

    public static Place details(String reference) {
        HttpURLConnection conn = null;
        StringBuilder jsonResults = new StringBuilder();
        try {
            StringBuilder sb = new StringBuilder(PLACES_API_BASE);
            sb.append(TYPE_DETAILS);
            sb.append(OUT_JSON);
            sb.append("?sensor=false");
            sb.append("&key=" + API_KEY);
            sb.append("&reference=" + URLEncoder.encode(reference, "utf8"));

            URL url = new URL(sb.toString());
            conn = (HttpURLConnection) url.openConnection();
            InputStreamReader in = new InputStreamReader(conn.getInputStream());

            // Load the results into a StringBuilder
            int read;
            char[] buff = new char[1024];
            while ((read = in.read(buff)) != -1) {
                jsonResults.append(buff, 0, read);
            }
        } catch (MalformedURLException e) {
            Log.e(LOG_TAG, "Error processing Places API URL", e);
            return null;
        } catch (IOException e) {
            Log.e(LOG_TAG, "Error connecting to Places API", e);
            return null;
        } finally {
            if (conn != null) {
                conn.disconnect();
            }
        }

        Place place = null;
        try {
            // Create a JSON object hierarchy from the results
            JSONObject jsonObj = new JSONObject(jsonResults.toString())
                    .getJSONObject("result");

            place = new Place();
            place.icon = jsonObj.getString("icon");
            place.name = jsonObj.getString("name");
            place.formatted_address = jsonObj.getString("formatted_address");
            if (jsonObj.has("formatted_phone_number")) {
                place.formatted_phone_number = jsonObj
                        .getString("formatted_phone_number");
            }
        } catch (JSONException e) {
            Log.e(LOG_TAG, "Error processing JSON results", e);
        }

        return place;
    }

//    /**
//     * below method is used to find near by places...
//     *
//     * @param latitude
//     * @param longitude
//     * @param placeSpacification
//     * @return
//     */
//    public static void findPlaces(double latitude, double longitude,
//                                  String placeSpacification) {
//
//        String urlString = makeUrl(latitude, longitude, placeSpacification);
//        AppDelegate.LogUA("place url " + urlString);
//        try {
//            String json = getJSON(urlString);
//            JSONObject object = new JSONObject(json);
//            JSONArray array = object.getJSONArray("results");
//            String status = object.getString(Tags.STATUS);
//            if (status != null && status.equalsIgnoreCase(Tags.ZERO_RESULTS)) {
//                LocationPlacesFragment.zeroResult = true;
//            } else {
//                LocationPlacesFragment.zeroResult = false;
//            }
//            LocationPlacesFragment.placesCollectorArray.clear();
//
//            for (int i = 0; i < array.length(); i++) {
//                try {
//                    Place place = Place
//                            .jsonToPontoReferencia((JSONObject) array.get(i));
//                    LocationPlacesFragment.placesCollectorArray.add(place);
//                } catch (Exception e) {
//                    AppDelegate.LogE(e);
//                }
//            }
//
//        } catch (JSONException ex) {
//            AppDelegate.LogE(ex);
//        }
//    }

    // https://maps.googleapis.com/maps/api/place/search/json?location=28.632808,77.218276&radius=500&types=atm&sensor=false&key=api
    // key
    public static String makeUrl(double latitude, double longitude, String place) {
        StringBuilder urlString = new StringBuilder(
                "https://maps.googleapis.com/maps/api/place/search/json?");
        if (place.equals("")) {
            urlString.append("&location=");
            urlString.append(Double.toString(latitude));
            urlString.append(",");
            urlString.append(Double.toString(longitude));
            urlString.append("&radius=1000");
            urlString.append("&sensor=false&key=" + API_KEY);
        } else {
            urlString.append("&location=");
            urlString.append(Double.toString(latitude));
            urlString.append(",");
            urlString.append(Double.toString(longitude));
            urlString.append("&radius=1000");
            urlString.append("&sensor=false&key=" + API_KEY);
        }
        return urlString.toString();
    }

    public static String getJSON(String url) {
        return getUrlContents(url);
    }

    public static String getUrlContents(String theUrl) {
        StringBuilder content = new StringBuilder();
        try {
            URL url = new URL(theUrl);
            URLConnection urlConnection = url.openConnection();
            BufferedReader bufferedReader = new BufferedReader(
                    new InputStreamReader(urlConnection.getInputStream()), 8);
            String line;
            while ((line = bufferedReader.readLine()) != null) {
                content.append(line + "\n");
            }
            bufferedReader.close();
        } catch (Exception e) {
            e.printStackTrace();
        }
        return content.toString();
    }
}