package com.craftedeals.Utils;

import android.graphics.Rect;
import android.support.v7.widget.RecyclerView;
import android.view.View;

/**
 * Created by bharat on 24/7/16.
 */
public class SpacesChatDecoration extends RecyclerView.ItemDecoration {
    private int space, count = 0;
    private boolean value = false;

    public SpacesChatDecoration(int space) {
        this.space = space;
    }

    public SpacesChatDecoration(int space, boolean value) {
        this.space = space;
        this.value = value;
    }

    public SpacesChatDecoration(int space, int count) {
        this.space = space;
        this.count = count;
    }

    @Override
    public void getItemOffsets(Rect outRect, View view, RecyclerView parent, RecyclerView.State state) {
        outRect.left = 0;
        outRect.right = 0;
        if (!value) {
            outRect.bottom = space + space;
            outRect.top = 0;

            // Add top margin only for the first item to avoid double space between items
            if (count == 0) {
                if (parent.getChildLayoutPosition(view) == 0 || parent.getChildLayoutPosition(view) == 1) {
                    outRect.top = space;
                } else {
                    outRect.top = 0;
                }
            } else {
                if (parent.getChildLayoutPosition(view) == 0 || parent.getChildLayoutPosition(view) == 1 || parent.getChildLayoutPosition(view) == 2) {
                    outRect.top = space;
                } else {
                    outRect.top = 0;
                }
            }
        }
    }
}