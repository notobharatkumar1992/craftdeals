package com.craftedeals.Utils;

import android.annotation.SuppressLint;

public class roundingOff {


    @SuppressLint("DefaultLocale")
    public static float roundOffTo2DecPlaces(float val) {
        float f = Float.valueOf(String.format("%.2f", val));
        // Log.d("Log", "roundOffTo2DecPlaces = " + f);
        return f;
    }

    @SuppressLint("DefaultLocale")
    public static float roundOffTo3DecPlaces(float val) {
        float f = Float.valueOf(String.format("%.3f", val));
        // Log.d("Log", "roundOffTo2DecPlaces = " + f);
        return f;
    }

    @SuppressLint("DefaultLocale")
    public static float roundOff(float val) {
        float f = Float.valueOf(String.format("%.0f", val));
        // Log.d("Log", "roundOff = " + f);
        return f;
    }

}
