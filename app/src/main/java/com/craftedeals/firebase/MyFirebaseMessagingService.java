package com.craftedeals.firebase;

import android.app.Notification;
import android.app.PendingIntent;
import android.content.Context;
import android.content.Intent;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.os.Handler;
import android.util.Log;
import android.view.View;

import com.craftedeals.AppDelegate;
import com.craftedeals.Models.NotificationModel;
import com.craftedeals.Models.UserDataModel;
import com.craftedeals.R;
import com.craftedeals.Utils.Prefs;
import com.craftedeals.activities.AdHocDealDetailActivity;
import com.craftedeals.activities.NotificationActivity;
import com.craftedeals.activities.PurchaseAdhocActivity;
import com.craftedeals.activities.PurchaseHeroActivity;
import com.craftedeals.activities.UpgradeMembershipActivity;
import com.craftedeals.activities.VendorProfileActivity;
import com.craftedeals.constants.Tags;
import com.craftedeals.parser.JSONParser;
import com.google.firebase.messaging.FirebaseMessagingService;
import com.google.firebase.messaging.RemoteMessage;
import com.nostra13.universalimageloader.core.DisplayImageOptions;
import com.nostra13.universalimageloader.core.ImageLoaderConfiguration;
import com.nostra13.universalimageloader.core.listener.SimpleImageLoadingListener;

import org.json.JSONObject;

import java.io.IOException;
import java.io.InputStream;
import java.net.URL;
import java.util.Map;
import java.util.Random;

import br.com.goncalves.pugnotification.interfaces.ImageLoader;
import br.com.goncalves.pugnotification.interfaces.OnImageLoadingCompleted;
import br.com.goncalves.pugnotification.notification.Custom;
import br.com.goncalves.pugnotification.notification.Load;
import br.com.goncalves.pugnotification.notification.PugNotification;

/**
 * Created by Heena on 5/27/2016.
 */

public class MyFirebaseMessagingService extends FirebaseMessagingService implements ImageLoader {
    private com.nostra13.universalimageloader.core.ImageLoader imageLoader = com.nostra13.universalimageloader.core.ImageLoader.getInstance();
    private DisplayImageOptions options = new DisplayImageOptions.Builder().cacheInMemory(true)
            .cacheOnDisc(true).resetViewBeforeLoading(true).
                    build();
    private static final String TAG = "MyFireBaseMsgService";
    public NotificationModel notificationModel = null;

    public static final int PURCHASED_DEAL = 1, NEWLY_ADDED_HERO_DEALS = 2, NEWLY_ADDED_ADHOC_DEALS = 3, REDEEM_PURCHASE_ADHOC_DEAL = 4, REDEEM_HERO_DEAL = 5,
            UPGRADED_MEMBERSHIP = 6, MEMBERSHIP_EXPIRING = 7, MEMBERSHIP_EXPIRED = 8, EXPIRING_PURCHASE_DEAL = 9, EXPIRED_PURCHASE = 10;

    /*
    Notification Type
    1 = Purchase Deal
    2 = Newly added Hero deals
    3 = Newly added Adhoc deals
    4 = Redeem purchase Adhoc deal
    5 = Redeem purchase Hero deal
    6 = Upgraded membership
    7 = Membership expiring
    8 = Membership Expired
    9 = Expiring purchase deal
    10 = Expired purchase
    */
    @Override
    public void onMessageReceived(RemoteMessage remoteMessage) {
        //Displaying data in log
        //It is optional
        Log.d(TAG, "remoteMessage: " + remoteMessage.getData());
        Log.d(TAG, "From: " + remoteMessage.getFrom());
        notificationModel = getNotificationModel(remoteMessage.getData());
        imageLoader.init(ImageLoaderConfiguration.createDefault(this));
        // store_intoRealm(notificationModel);
        //Calling method to generate notification
        if (notificationModel != null) {
            if (notificationModel.getNotification_type() == PURCHASED_DEAL || notificationModel.getNotification_type() == NEWLY_ADDED_HERO_DEALS || notificationModel.getNotification_type() == NEWLY_ADDED_ADHOC_DEALS) {
                // For adding new Hero and Adhoc deal // For purchasing Adhoc deal
                showPugNewDealNotification(this, notificationModel);
            } else if (notificationModel.getNotification_type() == REDEEM_PURCHASE_ADHOC_DEAL || notificationModel.getNotification_type() == EXPIRED_PURCHASE || notificationModel.getNotification_type() == EXPIRING_PURCHASE_DEAL) {
                // For redeem hero deal.
                showPurchaseDealNotification(this, notificationModel);
            } else if (notificationModel.getNotification_type() == REDEEM_HERO_DEAL) {
                // For redeem hero deal.
                showRedeemedHeroDealNotification(this, notificationModel);
            } else if (notificationModel.getNotification_type() == MEMBERSHIP_EXPIRED) {
                // For upgrade membership.
                showUpgradeMembershipNotification(this, notificationModel);
            } else if (notificationModel.getNotification_type() == MEMBERSHIP_EXPIRING || notificationModel.getNotification_type() == UPGRADED_MEMBERSHIP) {
                // For  membership Expired.
                showMembershipExpiredNotification(this, notificationModel);
            }
        }
        if (NotificationActivity.mHandler != null)
            NotificationActivity.mHandler.sendEmptyMessage(5);
    }

    private void showUpgradeMembershipNotification(Context mContext, NotificationModel notificationModel) {
        if (new Prefs(this).getUserdata() != null) {
            UserDataModel userDataModel = new Prefs(this).getUserdata();
            userDataModel.membership_id = 1;
            new Prefs(this).setUserData(userDataModel);
        }
        Intent intent = new Intent(mContext, UpgradeMembershipActivity.class);
        intent.putExtra(Tags.user_id, notificationModel.getUser_id());
        intent.putExtra(Tags.FROM, Tags.notification);
        intent.putExtra(Tags.notification_id, notificationModel.id);
        intent.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK | Intent.FLAG_ACTIVITY_CLEAR_TOP);

        if (UpgradeMembershipActivity.mHandler != null)
            UpgradeMembershipActivity.mHandler.sendEmptyMessage(7);
        PendingIntent fullScreenPendingIntent = PendingIntent.getActivity(this, 0, intent, PendingIntent.FLAG_UPDATE_CURRENT);

        Load mLoad = PugNotification.with(mContext).load()
                .smallIcon(R.drawable.pugnotification_ic_launcher)
                .autoCancel(true)
                .click(fullScreenPendingIntent)
                .smallIcon(R.mipmap.ic_launcher)
                .title(getString(R.string.app_name))
                .message(notificationModel.getMessage())
                .flags(Notification.DEFAULT_ALL);

        final Load finalMLoad = mLoad;
        new Handler(mContext.getMainLooper()).post(new Runnable() {
            @Override
            public void run() {
                finalMLoad.simple().build();
            }
        });
    }

    private void showMembershipExpiredNotification(Context mContext, NotificationModel notificationModel) {
        Intent intent = new Intent(mContext, UpgradeMembershipActivity.class);
        intent.putExtra(Tags.user_id, notificationModel.getUser_id());
        intent.putExtra(Tags.FROM, Tags.notification);
        intent.putExtra(Tags.notification_id, notificationModel.id);

        intent.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK | Intent.FLAG_ACTIVITY_CLEAR_TOP);
        PendingIntent fullScreenPendingIntent = PendingIntent.getActivity(this, 0, intent, PendingIntent.FLAG_UPDATE_CURRENT);
        Load mLoad = PugNotification.with(mContext).load()
                .smallIcon(R.drawable.pugnotification_ic_launcher)
                .autoCancel(true)
                .click(fullScreenPendingIntent)
                .smallIcon(R.mipmap.ic_launcher)
                .title(getString(R.string.app_name))
                .message(notificationModel.getMessage())
                .flags(Notification.DEFAULT_ALL);
        final Load finalMLoad = mLoad;
        new Handler(mContext.getMainLooper()).post(new Runnable() {
            @Override
            public void run() {
                finalMLoad.simple().build();
            }
        });
    }

    private void showRedeemedHeroDealNotification(Context mContext, NotificationModel notificationModel) {
        Intent intent = new Intent(mContext, PurchaseHeroActivity.class);
        intent.putExtra(Tags.purchase_id, (notificationModel.getPurchase_id()));
        intent.putExtra(Tags.FROM, Tags.notification);
        intent.putExtra(Tags.notification_id, notificationModel.id);
        intent.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK | Intent.FLAG_ACTIVITY_CLEAR_TOP);
        PendingIntent fullScreenPendingIntent = PendingIntent.getActivity(this, 0, intent, PendingIntent.FLAG_UPDATE_CURRENT);
//        showGenericDealsNotifications(mContext,)
        Bitmap remote_picture = null;
        try {
            if (AppDelegate.isValidString(notificationModel.getDeal_image()))
                remote_picture = BitmapFactory.decodeStream((InputStream) new URL(notificationModel.getDeal_image()).getContent());
        } catch (IOException e) {
            AppDelegate.LogE(e);
        }
        Custom mLoad;
        if (AppDelegate.isValidString(notificationModel.getDeal_image())) {
            mLoad = PugNotification.with(mContext).load()
                    .title(notificationModel.getDeal_title())
                    .message(notificationModel.getMessage())
                    .click(fullScreenPendingIntent)
                    .bigTextStyle(notificationModel.getMessage())
                    .autoCancel(true)
                    .vibrate(new long[]{1000, 1000})
                    .smallIcon(R.drawable.logo)
                    .largeIcon(R.drawable.logo)
                    .color(android.R.color.background_dark)
                    .custom()
                    .setImageLoader(MyFirebaseMessagingService.this)
                    .background(notificationModel.getDeal_image())
                    .setPlaceholder(R.drawable.pugnotification_ic_placeholder);
        } else {
            mLoad = PugNotification.with(mContext).load()
                    .title(notificationModel.getDeal_title())
                    .message(notificationModel.getMessage())
                    .click(fullScreenPendingIntent)
                    .bigTextStyle(notificationModel.getMessage())
                    .autoCancel(true)
                    .vibrate(new long[]{1000, 1000})
                    .smallIcon(R.drawable.logo)
                    .largeIcon(R.drawable.logo)
                    .color(android.R.color.background_dark)
                    .custom()
                    .setImageLoader(MyFirebaseMessagingService.this)
                    .setPlaceholder(R.drawable.pugnotification_ic_placeholder);
        }
        if (remote_picture != null) {
            mLoad = PugNotification.with(mContext).load()
                    .title(notificationModel.getDeal_title())
                    .message(notificationModel.getMessage())
                    .click(fullScreenPendingIntent)
                    .bigTextStyle(notificationModel.getMessage())
                    .addPerson(notificationModel.getDeal_image())
                    .autoCancel(true)
                    .vibrate(new long[]{1000, 1000})
                    .smallIcon(R.drawable.logo)
                    .largeIcon(remote_picture)
                    .color(android.R.color.background_dark)
                    .custom()
                    .setImageLoader(MyFirebaseMessagingService.this)
                    .background(notificationModel.getDeal_image())
                    .setPlaceholder(R.drawable.pugnotification_ic_placeholder);
        }

        final Custom finalMLoad = mLoad;
        new Handler(mContext.getMainLooper()).post(new Runnable() {
            @Override
            public void run() {
                finalMLoad.build();
            }
        });
    }

    private void showPurchaseDealNotification(Context mContext, NotificationModel notificationModel) {
        Intent intent = new Intent(mContext, PurchaseAdhocActivity.class);
        intent.putExtra(Tags.purchase_id, notificationModel.getPurchase_id());
        intent.putExtra(Tags.FROM, Tags.notification);
        intent.putExtra(Tags.notification_id, notificationModel.id);
        intent.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK | Intent.FLAG_ACTIVITY_CLEAR_TOP);

        PendingIntent fullScreenPendingIntent = PendingIntent.getActivity(this, 0, intent, PendingIntent.FLAG_UPDATE_CURRENT);
//        showGenericDealsNotifications(mContext,)
        Bitmap remote_picture = null;
        try {
            if (AppDelegate.isValidString(notificationModel.getDeal_image()))
                remote_picture = BitmapFactory.decodeStream((InputStream) new URL(notificationModel.getDeal_image()).getContent());
        } catch (IOException e) {
            AppDelegate.LogE(e);
        }
        Custom mLoad;
        if (AppDelegate.isValidString(notificationModel.getDeal_image())) {
            mLoad = PugNotification.with(mContext).load()
                    .title(notificationModel.getDeal_title())
                    .message(notificationModel.getMessage())
                    .click(fullScreenPendingIntent)
                    .bigTextStyle(notificationModel.getMessage())
                    .autoCancel(true)
                    .vibrate(new long[]{1000, 1000})
                    .smallIcon(R.drawable.logo)
                    .largeIcon(R.drawable.logo)
                    .color(android.R.color.background_dark)
                    .custom()
                    .setImageLoader(MyFirebaseMessagingService.this)
                    .background(notificationModel.getDeal_image())
                    .setPlaceholder(R.drawable.pugnotification_ic_placeholder);
        } else {
            mLoad = PugNotification.with(mContext).load()
                    .title(notificationModel.getDeal_title())
                    .message(notificationModel.getMessage())
                    .click(fullScreenPendingIntent)
                    .bigTextStyle(notificationModel.getMessage())
                    .autoCancel(true)
                    .vibrate(new long[]{1000, 1000})
                    .smallIcon(R.drawable.logo)
                    .largeIcon(R.drawable.logo)
                    .color(android.R.color.background_dark)
                    .custom()
                    .setImageLoader(MyFirebaseMessagingService.this)
                    .setPlaceholder(R.drawable.pugnotification_ic_placeholder);
        }
        if (remote_picture != null) {
            mLoad = PugNotification.with(mContext).load()
                    .title(notificationModel.getDeal_title())
                    .message(notificationModel.getMessage())
                    .click(fullScreenPendingIntent)
                    .bigTextStyle(notificationModel.getMessage())
                    .addPerson(notificationModel.getDeal_image())
                    .autoCancel(true)
                    .vibrate(new long[]{1000, 1000})
                    .smallIcon(R.drawable.logo)
                    .largeIcon(remote_picture)
                    .color(android.R.color.background_dark)
                    .custom()
                    .setImageLoader(MyFirebaseMessagingService.this)
                    .background(notificationModel.getDeal_image())
                    .setPlaceholder(R.drawable.pugnotification_ic_placeholder);
        }

        final Custom finalMLoad = mLoad;
        new Handler(mContext.getMainLooper()).post(new Runnable() {
            @Override
            public void run() {
                finalMLoad.build();
            }
        });
    }

    private void showPugNewDealNotification(Context mContext, NotificationModel notificationModel) {
        Intent intent = new Intent();
        if (notificationModel.getNotification_type() == PURCHASED_DEAL) {
            intent = new Intent(this, PurchaseAdhocActivity.class);
            try {
                intent.putExtra(Tags.purchase_id, (notificationModel.getPurchase_id()));
                intent.putExtra(Tags.FROM, Tags.notification);
                intent.putExtra(Tags.notification_id, notificationModel.id);
                intent.putExtra(Tags.realm_index_id, notificationModel.getId());
            } catch (Exception e) {
                AppDelegate.LogE(e);
            }
        } else if (notificationModel.getNotification_type() == NEWLY_ADDED_ADHOC_DEALS && notificationModel.getDeal_type().equalsIgnoreCase("1")) {
            intent = new Intent(this, AdHocDealDetailActivity.class);
            try {
                intent.putExtra(Tags.deal_id, Integer.parseInt(notificationModel.getDeal_id()));
                intent.putExtra(Tags.FROM, Tags.notification);
                intent.putExtra(Tags.notification_id, notificationModel.id);
                intent.putExtra(Tags.realm_index_id, notificationModel.getId());
            } catch (Exception e) {
                AppDelegate.LogE(e);
            }
        } else if (notificationModel.getNotification_type() == NEWLY_ADDED_HERO_DEALS && !notificationModel.getDeal_type().equalsIgnoreCase("1")) {
            intent = new Intent(this, VendorProfileActivity.class);
            try {
                intent.putExtra(Tags.deal_id, Integer.parseInt(notificationModel.getDeal_id()));
                intent.putExtra(Tags.FROM, Tags.notification);
                intent.putExtra(Tags.notification_id, notificationModel.id);
                intent.putExtra(Tags.vendor_id, Integer.parseInt(notificationModel.getVendor_id()));
                intent.putExtra(Tags.realm_index_id, notificationModel.getId());
            } catch (Exception e) {
                AppDelegate.LogE(e);
            }
        }
        intent.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK | Intent.FLAG_ACTIVITY_CLEAR_TOP);

        PendingIntent fullScreenPendingIntent = PendingIntent.getActivity(this, 0, intent, PendingIntent.FLAG_UPDATE_CURRENT);

        Bitmap remote_picture = null;
        try {
            if (AppDelegate.isValidString(notificationModel.getDeal_image()))
                remote_picture = BitmapFactory.decodeStream((InputStream) new URL(notificationModel.getDeal_image()).getContent());
        } catch (IOException e) {
            AppDelegate.LogE(e);
        }

        Custom mLoad;
        if (AppDelegate.isValidString(notificationModel.getDeal_image())) {
            mLoad = PugNotification.with(mContext).load()
                    .title(notificationModel.getDeal_title())
                    .message(notificationModel.getMessage())
                    .click(fullScreenPendingIntent)
                    .bigTextStyle(notificationModel.getMessage())
                    .addPerson(notificationModel.getDeal_image())
                    .autoCancel(true)
                    .vibrate(new long[]{1000, 1000})
                    .smallIcon(R.mipmap.ic_launcher)
                    .largeIcon(R.drawable.logo)
                    .color(android.R.color.background_dark)
                    .custom()
                    .setImageLoader(MyFirebaseMessagingService.this)
                    .background(notificationModel.getDeal_image())
                    .setPlaceholder(R.drawable.pugnotification_ic_placeholder);
        } else {
            mLoad = PugNotification.with(mContext).load()
                    .title(notificationModel.getDeal_title())
                    .message(notificationModel.getMessage())
                    .click(fullScreenPendingIntent)
                    .bigTextStyle(notificationModel.getMessage())
                    .addPerson(notificationModel.getDeal_image())
                    .autoCancel(true)
                    .vibrate(new long[]{1000, 1000})
                    .smallIcon(R.mipmap.ic_launcher)
                    .largeIcon(R.drawable.logo)
                    .color(android.R.color.background_dark)
                    .custom()
                    .setImageLoader(MyFirebaseMessagingService.this)
                    .setPlaceholder(R.drawable.pugnotification_ic_placeholder);
        }
        if (remote_picture != null) {
            mLoad = PugNotification.with(mContext).load()
                    .title(notificationModel.getDeal_title())
                    .message(notificationModel.getMessage())
                    .click(fullScreenPendingIntent)
                    .bigTextStyle(notificationModel.getMessage())
                    .addPerson(notificationModel.getDeal_image())
                    .autoCancel(true)
                    .vibrate(new long[]{1000, 1000})
                    .smallIcon(R.drawable.logo)
                    .largeIcon(remote_picture)
                    .color(android.R.color.background_dark)
                    .custom()
                    .setImageLoader(MyFirebaseMessagingService.this)
                    .background(notificationModel.getDeal_image())
                    .setPlaceholder(R.drawable.pugnotification_ic_placeholder);
        }
        final Custom finalMLoad = mLoad;
        new Handler(mContext.getMainLooper()).post(new Runnable() {
            @Override
            public void run() {
                finalMLoad.build();
            }
        });
    }


    private NotificationModel getNotificationModel(Map<String, String> bundle) {
        AppDelegate.LogT("Notification message==" + bundle);
        AppDelegate.LogT("Notification==" + bundle.get("data"));
        NotificationModel notificationModel = new NotificationModel();
        try {
            JSONObject object = new JSONObject(bundle.get("data"));
            notificationModel.setId(JSONParser.getInt(object, Tags.nid));
            if (object.has(Tags.deal_id))
                notificationModel.setDeal_id(JSONParser.getString(object, Tags.deal_id));
            if (object.has(Tags.vendor_id))
                notificationModel.setVendor_id(JSONParser.getString(object, Tags.vendor_id));
            if (object.has(Tags.deal_type))
                notificationModel.setDeal_type(JSONParser.getString(object, Tags.deal_type));
            if (object.has(Tags.business_name))
                notificationModel.setBusiness_name(JSONParser.getString(object, Tags.business_name));
            if (object.has(Tags.message))
                notificationModel.setMessage(JSONParser.getString(object, Tags.message));
            if (object.has(Tags.deal_title))
                notificationModel.setDeal_title(JSONParser.getString(object, Tags.deal_title));
            if (object.has(Tags.deal_image)) {
                if (AppDelegate.isValidString(JSONParser.getString(object, Tags.deal_image)))
                    notificationModel.setDeal_image(JSONParser.getString(object, Tags.deal_image));
                else {
                    notificationModel.setDeal_image("http://notosolutions.net/craftdeals/img/no-image.png");
                }
            } else {
                notificationModel.setDeal_image("http://notosolutions.net/craftdeals/img/no-image.png");
            }

            if (object.has(Tags.vendor_image))
                notificationModel.setVendor_image(JSONParser.getString(object, Tags.vendor_image));
            if (object.has(Tags.vendor_name))
                notificationModel.setVendor_name(JSONParser.getString(object, Tags.vendor_name));
            if (object.has(Tags.notification_type))
                notificationModel.setNotification_type(JSONParser.getInt(object, Tags.notification_type));
            if (object.has(Tags.purchase_id))
                notificationModel.setPurchase_id(JSONParser.getInt(object, Tags.purchase_id));
            if (object.has(Tags.membership_id))
                notificationModel.membership_id = JSONParser.getString(object, Tags.membership_id);
            if (object.has(Tags.name))
                notificationModel.membership_name = JSONParser.getString(object, Tags.name);
            if (object.has(Tags.amount))
                notificationModel.membership_amount = JSONParser.getString(object, Tags.amount);
            if (object.has(Tags.month_of_membership))
                notificationModel.month_of_membership = JSONParser.getString(object, Tags.month_of_membership);
            if (object.has(Tags.description))
                notificationModel.membership_description = JSONParser.getString(object, Tags.description);
            if (object.has(Tags.status))
                notificationModel.membership_status = JSONParser.getString(object, Tags.status);
            if (object.has(Tags.user_id))
                notificationModel.setUser_id(JSONParser.getInt(object, Tags.user_id));
            if (object.has(Tags.payment_id))
                notificationModel.setPayment_id(JSONParser.getInt(object, Tags.payment_id));
            AppDelegate.LogT("Notification Model by json==" + notificationModel);

        } catch (Exception e) {
            AppDelegate.LogE(e);
        }
        return notificationModel;
    }

    public static int getRandomNumer() {
        Random r = new Random();
        int value = r.nextInt(80 - 65) + 65;
        return value;
    }
//
//    private Target viewTarget;
//
//    private static Target getViewTarget(final OnImageLoadingCompleted onCompleted) {
//        return new Target() {
//            @Override
//            public void onBitmapLoaded(Bitmap bitmap, Picasso.LoadedFrom from) {
//                onCompleted.imageLoadingCompleted(bitmap);
//            }
//
//            @Override
//            public void onBitmapFailed(Drawable errorDrawable) {
//
//            }
//
//            @Override
//            public void onPrepareLoad(Drawable placeHolderDrawable) {
//
//            }
//        };
//    }

    @Override
    public void load(String uri, final OnImageLoadingCompleted onCompleted) {
        imageLoader.loadImage(uri, new SimpleImageLoadingListener() {
            @Override
            public void onLoadingComplete(String imageUri, View view, Bitmap loadedImage) {
                // Do whatever you want with Bitmap
                onCompleted.imageLoadingCompleted(loadedImage);
            }
        });

//        AppDelegate.LogT("uri onCompleted");
//        viewTarget = getViewTarget(onCompleted);
//        Picasso.with(this).load(uri).into(viewTarget);
    }

    @Override
    public void load(int imageResId, OnImageLoadingCompleted onCompleted) {
        AppDelegate.LogT("imageResId onCompleted");
//        viewTarget = getViewTarget(onCompleted);
//        Picasso.with(this).load(imageResId).into(viewTarget);
    }
}