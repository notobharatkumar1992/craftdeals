package com.craftedeals.activities;

import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.IntentSender;
import android.graphics.Color;
import android.graphics.drawable.Drawable;
import android.location.Location;
import android.os.Build;
import android.os.Bundle;
import android.os.Handler;
import android.os.Message;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.v4.app.FragmentActivity;
import android.support.v7.app.AlertDialog;
import android.util.Log;
import android.view.View;
import android.view.Window;
import android.view.WindowManager;
import android.widget.ImageView;

import com.craftedeals.AppDelegate;
import com.craftedeals.Async.LocationAddress;
import com.craftedeals.Async.PostAsync;
import com.craftedeals.Models.CityModel;
import com.craftedeals.Models.PostAysnc_Model;
import com.craftedeals.Models.UserDataModel;
import com.craftedeals.R;
import com.craftedeals.Utils.Prefs;
import com.craftedeals.constants.ServerRequestConstants;
import com.craftedeals.constants.Tags;
import com.craftedeals.interfaces.OnReciveServerResponse;
import com.craftedeals.parser.JSONParser;
import com.google.android.gms.common.ConnectionResult;
import com.google.android.gms.common.api.GoogleApiClient;
import com.google.android.gms.location.LocationListener;
import com.google.android.gms.location.LocationRequest;
import com.google.android.gms.location.LocationServices;

import org.json.JSONObject;

import java.util.ArrayList;

/**
 * Created by NOTO on 5/26/2016.
 */
public class SplashActivity extends FragmentActivity implements GoogleApiClient.ConnectionCallbacks, GoogleApiClient.OnConnectionFailedListener,
        LocationListener, OnReciveServerResponse, View.OnClickListener
{
    private final static int CONNECTION_FAILURE_RESOLUTION_REQUEST = 9000;
    private GoogleApiClient mGoogleApiClient;
    private LocationRequest mLocationRequest;
    private ImageView img_splash;
    private Prefs prefs;
    private double currentLatitude = 0, currentLongitude = 0;
    private Handler mHandler;
    public AlertDialog.Builder alert;


    @Override
    protected void onCreate(Bundle savedInstanceState)
    {
        super.onCreate(savedInstanceState);
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.LOLLIPOP)
        {
            Window window = this.getWindow();
            Drawable background = this.getResources().getDrawable(R.drawable.spalsh);
            window.addFlags(WindowManager.LayoutParams.FLAG_DRAWS_SYSTEM_BAR_BACKGROUNDS);
            window.setStatusBarColor(Color.parseColor("#99000000"));
            window.setNavigationBarColor(Color.parseColor("#99000000"));
            window.setBackgroundDrawable(background);
        }
        setContentView(R.layout.splash_activity);
        prefs = new Prefs(this);
        prefs.setCityData(null);
        initGPS();
        setHandler();
        check_Rememberme();
        initView();
        initFirebase();
        AppDelegate.getHashKey(this);
        // checkValidUser();
    }

    private void setHandler()
    {
        mHandler = new Handler()
        {
            @Override
            public void dispatchMessage(Message msg)
            {
                super.dispatchMessage(msg);
                switch (msg.what)
                {
                    case 10:
                        AppDelegate.showProgressDialog(SplashActivity.this);
                        break;
                    case 11:
                        AppDelegate.hideProgressDialog(SplashActivity.this);
                        break;
                    case 1:
                        break;
                    case 2:
                        setAddressFromGEOcoder(msg.getData());
                        break;
                    case 3:
                        break;
                }
            }
        };
    }

    private void setloc(double latitude, double longitude)
    {
        LocationAddress.getAddressFromLocation(latitude, longitude, this, mHandler);

    }

    private void setAddressFromGEOcoder(Bundle data)
    {
        CityModel cityModel;
        if (new Prefs(this).getCitydata() != null)
        {
            cityModel = new Prefs(this).getCitydata();
        }
        else
        {
            cityModel = new CityModel();
        }
//        if (AppDelegate.isValidString(cityModel.location_type) && cityModel.location_type.equalsIgnoreCase(Tags.other)) {
//
//        } else {
        cityModel.location_type = Tags.current_location;
        cityModel.name = data.getString(Tags.city_param);
        cityModel.lat = String.valueOf(data.getDouble(Tags.LAT)) + "";
        cityModel.lng = String.valueOf(data.getDouble(Tags.LNG)) + "";
        new Prefs(this).setCityData(cityModel);
//        }
    }

    private void initView()
    {
        img_splash = (ImageView) findViewById(R.id.img_splash);
        img_splash.setVisibility(View.VISIBLE);
    }

    private void check_Rememberme()
    {
        AppDelegate.LogT("email id is" + new Prefs(SplashActivity.this).getStringValuefromTemp(Tags.EMAIL, ""));
        if (!AppDelegate.isValidString(new Prefs(SplashActivity.this).getStringValuefromTemp(Tags.EMAIL, "")))
        {
            if (!AppDelegate.isValidString(new Prefs(SplashActivity.this).getStringValuefromTemp(Tags.social_id, "")))
            {
                new Prefs(this).clearSharedPreference();
                checkValidUser();
            }
            else
            {
                if (prefs.getUserdata() != null && AppDelegate.isValidString(prefs.getUserdata().id + ""))
                {
                    callAsyncFacebookVerify(new Prefs(SplashActivity.this).getStringValuefromTemp(Tags.social_id, ""));
                }
                else
                {
                    startActivity(new Intent(SplashActivity.this, DashBoardActivity.class));
                    finish();
                }
            }
        }
        else
        {
            if (prefs.getUserdata() != null && AppDelegate.isValidString(prefs.getUserdata().id + ""))
            {
                executeLogin();
            }
            else
            {
                startActivity(new Intent(SplashActivity.this, DashBoardActivity.class));
                finish();
            }
        }
    }

    private void executeLogin()
    {
        try
        {
            if (AppDelegate.haveNetworkConnection(this, false))
            {
                ArrayList<PostAysnc_Model> mPostArrayList = new ArrayList<PostAysnc_Model>();
                PostAsync mPostAsyncObj;
                AppDelegate.getInstance(this).setPostParamsSecond(mPostArrayList, Tags.API_KEY, Tags.API_KEY_VALUE);
                AppDelegate.getInstance(this).setPostParamsSecond(mPostArrayList, Tags.email, new Prefs(SplashActivity.this).getStringValuefromTemp(Tags.EMAIL, ""));
                AppDelegate.getInstance(this).setPostParamsSecond(mPostArrayList, Tags.password, new Prefs(SplashActivity.this).getStringValuefromTemp(Tags.PASSWORD, ""));
                AppDelegate.getInstance(this).setPostParamsSecond(mPostArrayList, Tags.device_id, prefs.getGCMtokenfromTemp());
                AppDelegate.getInstance(this).setPostParamsSecond(mPostArrayList, Tags.device_type, AppDelegate.DEVICE_TYPE, ServerRequestConstants.Key_PostintValue);
                String lat = null, lng = null;
                if (prefs.getCitydata() != null)
                {
                    if (AppDelegate.isValidString(prefs.getCitydata().lat))
                    {
                        lat = prefs.getCitydata().lat;
                        lng = prefs.getCitydata().lng;
                    }
                }
                else
                {
                    lat = currentLatitude + "";
                    lng = currentLongitude + "";
                }
                if (AppDelegate.isValidString(lat) && AppDelegate.isValidString(lng))
                {
                    AppDelegate.getInstance(this).setPostParamsSecond(mPostArrayList, Tags.latitude, lat);
                    AppDelegate.getInstance(this).setPostParamsSecond(mPostArrayList, Tags.longitude, lng);
                }
                AppDelegate.getInstance(this).setPostParamsSecond(mPostArrayList, Tags.role, AppDelegate.BUYER, ServerRequestConstants.Key_PostintValue);
                mPostAsyncObj = new PostAsync(this, this, ServerRequestConstants.LOGIN, mPostArrayList, null);
                mPostAsyncObj.execute();
            }
            else
            {
                showRetryAlert(this, ServerRequestConstants.LOGIN, getResources().getString(R.string.no_internet_retry));
            }
        }
        catch (Exception e)
        {
            AppDelegate.LogE(e);
            AppDelegate.showToast(this, getResources().getString(R.string.try_again));
        }
    }

    private void initGPS()
    {
        mGoogleApiClient = new GoogleApiClient.Builder(this)
                .addConnectionCallbacks(this)
                .addOnConnectionFailedListener(this)
                .addApi(LocationServices.API)
                .build();
        AppDelegate.LogT("mGoogleApiClient Initialited");
        mLocationRequest = LocationRequest.create()
                .setPriority(LocationRequest.PRIORITY_HIGH_ACCURACY)
                .setInterval(500)        // 10 seconds, in milliseconds
                .setFastestInterval(500); // 10 second, in milliseconds
        AppDelegate.LogT("mLocationRequest Initialited");
    }

    @Override
    protected void onPause()
    {
        super.onPause();
        try
        {
            if (mGoogleApiClient.isConnected())
            {
                LocationServices.FusedLocationApi.removeLocationUpdates(mGoogleApiClient, SplashActivity.this);
                mGoogleApiClient.disconnect();
            }
        }
        catch (Exception e)
        {
            AppDelegate.LogE(e);
        }
    }

    private void checkValidUser()
    {
        new Handler().postDelayed(new Runnable()
        {
            @Override
            public void run()
            {
                startActivity(new Intent(SplashActivity.this, DashBoardActivity.class));
                finish();
            }
        }, 1000);
    }

    private void initFirebase()
    {
    }


    @Override
    protected void onDestroy()
    {
        super.onDestroy();
        mHandler = null;
        //prefs.clearTempPrefs();
    }

    @Override
    public void onConnected(@Nullable Bundle bundle)
    {
        Location location = LocationServices.FusedLocationApi.getLastLocation(mGoogleApiClient);
        AppDelegate.LogT("onConnected Initialited");
        if (location == null)
        {
            try
            {
                AppDelegate.LogT("onConnected Initialited== null");
                LocationServices.FusedLocationApi.requestLocationUpdates(mGoogleApiClient, mLocationRequest, SplashActivity.this);
            }
            catch (Exception e)
            {
                AppDelegate.LogE(e);
            }
        }
        else
        {
            currentLatitude = location.getLatitude();
            currentLongitude = location.getLongitude();
            setloc(currentLatitude, currentLongitude);
            AppDelegate.LogT("latLng = " + currentLatitude + ", " + currentLongitude);
            new Prefs(this).putStringValue(Tags.LAT, String.valueOf(currentLatitude));
            new Prefs(this).putStringValue(Tags.LNG, String.valueOf(currentLongitude));
        }
    }

    @Override
    public void onConnectionSuspended(int i)
    {

    }

    @Override
    public void onConnectionFailed(@NonNull ConnectionResult connectionResult)
    {
        if (connectionResult.hasResolution())
        {
            try
            {
                AppDelegate.LogT("onConnectionFailed Initialited" + connectionResult);
                connectionResult.startResolutionForResult(this, CONNECTION_FAILURE_RESOLUTION_REQUEST);

            }
            catch (IntentSender.SendIntentException e)
            {
                AppDelegate.LogE(e);
            }
        }
        else
        {
            Log.e("Error", "Location services connection failed with code " + connectionResult.getErrorCode());
        }
    }

    @Override
    protected void onResume()
    {
        super.onResume();
        AppDelegate.hideKeyBoard(this);
        mGoogleApiClient.connect();
    }

    @Override
    public void onLocationChanged(Location location)
    {
        currentLatitude = location.getLatitude();
        currentLongitude = location.getLongitude();
        setloc(currentLatitude, currentLongitude);
        AppDelegate.LogGP("onLocationChanged latLng = " + currentLatitude + ", " + currentLongitude);
        new Prefs(this).putStringValue(Tags.LAT, String.valueOf(currentLatitude));
        new Prefs(this).putStringValue(Tags.LNG, String.valueOf(currentLongitude));
        try
        {
            if (mGoogleApiClient.isConnected())
            {
                LocationServices.FusedLocationApi.removeLocationUpdates(mGoogleApiClient, SplashActivity.this);
                mGoogleApiClient.disconnect();
                AppDelegate.LogGP("Fused Location api disconnect called");
            }
        }
        catch (Exception e)
        {
            AppDelegate.LogE(e);
        }
    }

    @Override
    public void setOnReciveResult(String apiName, String result)
    {
        //mHandler.sendEmptyMessage(11);
        if (mHandler == null)
        {
            return;
        }
        if (!AppDelegate.isValidString(result))
        {
            showRetryAlert(this, apiName, getResources().getString(R.string.service_time_out_retry));
            // AppDelegate.showToast(this, "Service Time Out!!!");
            return;
        }
        else if (apiName.equalsIgnoreCase(ServerRequestConstants.LOGIN))
        {
            parseLoginResponse(result);
        }
        else if (apiName.equalsIgnoreCase(ServerRequestConstants.CHK_FACEBOOK))
        {
            parseCheckFbResponse(result);
        }
    }

    private void parseLoginResponse(String result)
    {
        try
        {
            JSONObject jsonObject = new JSONObject(result);
            if (jsonObject.getString(Tags.status).equalsIgnoreCase("1"))
            {
                //AppDelegate.showToast(SplashActivity.this, jsonObject.getString(Tags.message));
                JSONObject object = jsonObject.getJSONObject(Tags.response);
                UserDataModel userDataModel = new UserDataModel();
                userDataModel.id = JSONParser.getInt(object, Tags.id);
                userDataModel.name = JSONParser.getString(object, Tags.name);
                userDataModel.email = JSONParser.getString(object, Tags.email);
                userDataModel.contact_number = JSONParser.getString(object, Tags.contact_number);
                userDataModel.membership_id = JSONParser.getInt(object, Tags.membership_id);
                userDataModel.image = JSONParser.getString(object, Tags.image);
                userDataModel.device_id = JSONParser.getString(object, Tags.device_id);
                userDataModel.device_type = JSONParser.getInt(object, Tags.device_type);
                userDataModel.created = JSONParser.getString(object, Tags.created);
                userDataModel.address = JSONParser.getString(object, Tags.address);
                userDataModel.state_id = JSONParser.getString(object, Tags.state_id);
                userDataModel.city_id = JSONParser.getString(object, Tags.city_id);
                if (object.has(Tags.user_range))
                {
                    userDataModel.user_range = JSONParser.getInt(object, Tags.user_range);
                }
                if (object.has(Tags.membership_start_date) && object.has(Tags.membership_end_date))
                {
                    userDataModel.membership_start_date = JSONParser.getString(object, Tags.membership_start_date);
                    userDataModel.membership_end_date = JSONParser.getString(object, Tags.membership_end_date);
                }
                if ((object.has(Tags.state)) && AppDelegate.isValidString(object.getString(Tags.state)))
                {
                    JSONObject state = object.getJSONObject(Tags.state);
                    userDataModel.province = JSONParser.getString(state, Tags.region_name);
                    userDataModel.state_id = JSONParser.getInt(state, Tags.id) + "";
                }
                if (object.has(Tags.city) && AppDelegate.isValidString(object.getString(Tags.city)))
                {
                    JSONObject city = object.getJSONObject(Tags.city);
                    userDataModel.city = JSONParser.getString(city, Tags.name);
                    userDataModel.city_id = JSONParser.getInt(city, Tags.id) + "";
                    if (prefs.getCitydata() != null)
                    {
                    }
                    else
                    {
                        CityModel cityModel = new CityModel();
                        cityModel.lat = JSONParser.getString(city, Tags.latitude);
                        cityModel.lng = JSONParser.getString(city, Tags.longitude);
                        cityModel.location_type = Tags.other;
                        cityModel.id = JSONParser.getInt(city, Tags.id) + "";
                        cityModel.name = JSONParser.getString(city, Tags.name);
//                        prefs.setCityData(cityModel);
                    }
                }
                if (object.has(Tags.beer_type) && AppDelegate.isValidString(object.getString(Tags.beer_type)))
                {
                    JSONObject beer = object.getJSONObject(Tags.beer_type);
                    userDataModel.fav_beer = JSONParser.getString(beer, Tags.name);
                    userDataModel.beer_id = JSONParser.getInt(beer, Tags.id);
                }
                if (object.has(Tags.brewery_type) && AppDelegate.isValidString(object.getString(Tags.brewery_type)))
                {
                    JSONObject brewery = object.getJSONObject(Tags.brewery_type);
                    userDataModel.fav_brewery = JSONParser.getString(brewery, Tags.name);
                    userDataModel.brewery_id = JSONParser.getInt(brewery, Tags.id);
                }

                prefs.setUserData(userDataModel);
                startActivity(new Intent(SplashActivity.this, DashBoardActivity.class));
                finish();
            }
            else
            {
                showAlert(this, getResources().getString(R.string.alert), JSONParser.getString(jsonObject, Tags.message));
            }
        }
        catch (Exception e)
        {
            AppDelegate.LogE(e);
        }

    }

    private void callAsyncFacebookVerify(String social_id)
    {
        if (AppDelegate.haveNetworkConnection(this, false))
        {
            try
            {
                ArrayList<PostAysnc_Model> mPostArrayList = new ArrayList<PostAysnc_Model>();
                AppDelegate.getInstance(this).setPostParamsSecond(mPostArrayList, Tags.API_KEY, Tags.API_KEY_VALUE);
                AppDelegate.getInstance(this).setPostParamsSecond(mPostArrayList, Tags.device_id, prefs.getGCMtokenfromTemp());
                AppDelegate.getInstance(this).setPostParamsSecond(mPostArrayList, Tags.device_type, AppDelegate.DEVICE_TYPE, ServerRequestConstants.Key_PostintValue);
                AppDelegate.getInstance(this).setPostParamsSecond(mPostArrayList, Tags.social_id, social_id);
                String lat = null, lng = null;
                if (prefs.getCitydata() != null)
                {
                    if (AppDelegate.isValidString(prefs.getCitydata().lat))
                    {
                        lat = prefs.getCitydata().lat;
                        lng = prefs.getCitydata().lng;
                    }
                }
                else
                {
                    lat = currentLatitude + "";
                    lng = currentLongitude + "";
                }
                if (AppDelegate.isValidString(lat) && AppDelegate.isValidString(lng))
                {
                    AppDelegate.getInstance(this).setPostParamsSecond(mPostArrayList, Tags.latitude, lat);
                    AppDelegate.getInstance(this).setPostParamsSecond(mPostArrayList, Tags.longitude, lng);
                }
                PostAsync mPostAsyncObj = new PostAsync(this, this, ServerRequestConstants.CHK_FACEBOOK,
                        mPostArrayList, null);
                // mHandler.sendEmptyMessage(10);
                mPostAsyncObj.execute();
            }
            catch (Exception e)
            {
                AppDelegate.LogE(e);
            }
        }
        else
        {
            showRetryAlert(this, ServerRequestConstants.CHK_FACEBOOK, getResources().getString(R.string.no_internet_retry));
        }
    }

    private void parseCheckFbResponse(String result)
    {
        try
        {
            JSONObject jsonObject = new JSONObject(result);
            if (jsonObject.getString(Tags.status).equalsIgnoreCase("2"))
            {
                if (AppDelegate.isValidString(JSONParser.getString(jsonObject, Tags.message)))
                {
                    AppDelegate.showToast(SplashActivity.this, jsonObject.getString(Tags.message));
                }
            }
            else if (jsonObject.getString(Tags.status).equalsIgnoreCase("1"))
            {
                if (AppDelegate.isValidString(JSONParser.getString(jsonObject, Tags.message)))
                {
                    AppDelegate.showToast(SplashActivity.this, jsonObject.getString(Tags.message));
                }
                JSONObject object = jsonObject.getJSONObject(Tags.DATA);
                UserDataModel userDataModel = new UserDataModel();
                userDataModel.id = JSONParser.getInt(object, Tags.id);
                userDataModel.name = JSONParser.getString(object, Tags.name);
                userDataModel.email = JSONParser.getString(object, Tags.email);
                userDataModel.contact_number = JSONParser.getString(object, Tags.contact_number);
                userDataModel.membership_id = JSONParser.getInt(object, Tags.membership_id);
                userDataModel.image = JSONParser.getString(object, Tags.image);
                userDataModel.device_id = JSONParser.getString(object, Tags.device_id);
                userDataModel.device_type = JSONParser.getInt(object, Tags.device_type);
                userDataModel.created = JSONParser.getString(object, Tags.created);
                userDataModel.address = JSONParser.getString(object, Tags.address);
                if (object.has(Tags.user_range))
                {
                    userDataModel.user_range = JSONParser.getInt(object, Tags.user_range);
                }
                if (object.has(Tags.membership_start_date) && object.has(Tags.membership_end_date))
                {
                    userDataModel.membership_start_date = JSONParser.getString(object, Tags.membership_start_date);
                    userDataModel.membership_end_date = JSONParser.getString(object, Tags.membership_end_date);
                }
                if (AppDelegate.isValidString(object.getString(Tags.state)))
                {
                    JSONObject state = object.getJSONObject(Tags.state);
                    userDataModel.province = JSONParser.getString(state, Tags.region_name);
                    userDataModel.state_id = JSONParser.getInt(state, Tags.id) + "";
                }
                if (AppDelegate.isValidString(object.getString(Tags.city)))
                {
                    JSONObject city = object.getJSONObject(Tags.city);
                    userDataModel.city = JSONParser.getString(city, Tags.name);
                    userDataModel.city_id = JSONParser.getInt(city, Tags.id) + "";
                    CityModel cityModel = new CityModel();
                    cityModel.lat = JSONParser.getString(city, Tags.latitude);
                    cityModel.lng = JSONParser.getString(city, Tags.longitude);
                    cityModel.location_type = Tags.other;
                    cityModel.id = JSONParser.getInt(city, Tags.id) + "";
                    cityModel.name = JSONParser.getString(city, Tags.name);
                    // prefs.setCityData(cityModel);
                }
                if (AppDelegate.isValidString(object.getString(Tags.beer_type)))
                {
                    JSONObject beer = object.getJSONObject(Tags.beer_type);
                    userDataModel.fav_beer = JSONParser.getString(beer, Tags.name);
                    userDataModel.beer_id = JSONParser.getInt(beer, Tags.id);
                }
                if (AppDelegate.isValidString(object.getString(Tags.brewery_type)))
                {
                    JSONObject brewery = object.getJSONObject(Tags.brewery_type);
                    userDataModel.fav_brewery = JSONParser.getString(brewery, Tags.name);
                    userDataModel.brewery_id = JSONParser.getInt(brewery, Tags.id);
                }
                prefs.setUserData(userDataModel);
                if (DashBoardActivity.mainActivity != null)
                {
                    DashBoardActivity.mainActivity.finish();
                }
                startActivity(new Intent(SplashActivity.this, DashBoardActivity.class));
                finish();
            }
            else
            {
                showAlert(this, getResources().getString(R.string.alert), JSONParser.getString(jsonObject, Tags.message));
            }
        }
        catch (Exception e)
        {
            AppDelegate.LogE(e);
        }

    }

    public void showAlert(Context mContext, String Title, String
            Message)
    {
        try
        {
            alert = new AlertDialog.Builder(mContext);
            alert.setCancelable(false);
            alert.setTitle(Title);
            alert.setMessage(Message);
            alert.setPositiveButton(
                    Tags.OK,
                    new DialogInterface.OnClickListener()
                    {

                        @Override
                        public void onClick(DialogInterface dialog, int which)
                        {
                            startActivity(new Intent(SplashActivity.this, DashBoardActivity.class));
                            new Prefs(SplashActivity.this).setUserData(null);
                            dialog.dismiss();
                            finish();
                        }
                    });
            alert.show();
        }
        catch (Exception e)
        {
            AppDelegate.LogE(e);
        }
    }

    public void showRetryAlert(Context mContext, final String apiname, String
            Message)
    {
        try
        {

            alert = new AlertDialog.Builder(mContext);
            alert.setCancelable(false);
            alert.setMessage(Message);
            alert.setPositiveButton(
                    Tags.OK,
                    new DialogInterface.OnClickListener()
                    {

                        @Override
                        public void onClick(DialogInterface dialog, int which)
                        {
                            // startActivity(new Intent(SplashActivity.this, DashBoardActivity.class));
                            if (apiname.equalsIgnoreCase(ServerRequestConstants.LOGIN))
                            {
                                executeLogin();
                            }
                            else
                            {
                                if (AppDelegate.isValidString(new Prefs(SplashActivity.this).getStringValuefromTemp(Tags.social_id, "")))
                                {
                                    callAsyncFacebookVerify(new Prefs(SplashActivity.this).getStringValuefromTemp(Tags.social_id, ""));
                                }
                            }
                        }
                    }).setNegativeButton(Tags.CANCEL, new DialogInterface.OnClickListener()
            {
                @Override
                public void onClick(DialogInterface dialog, int which)
                {
                    dialog.dismiss();
                    System.exit(0);
                }
            });
            alert.show();
        }
        catch (Exception e)
        {
            AppDelegate.LogE(e);
        }
    }

    @Override
    public void onClick(View v)
    {
        if (v == null)
        {

        }
    }

}
