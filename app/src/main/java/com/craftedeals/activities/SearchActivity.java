package com.craftedeals.activities;

import android.app.Activity;
import android.app.Dialog;
import android.content.Intent;
import android.graphics.Typeface;
import android.graphics.drawable.ColorDrawable;
import android.graphics.drawable.Drawable;
import android.os.Build;
import android.os.Bundle;
import android.os.Handler;
import android.os.Message;
import android.support.v4.app.ActivityOptionsCompat;
import android.support.v4.util.Pair;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.DefaultItemAnimator;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.text.Editable;
import android.text.TextWatcher;
import android.view.View;
import android.view.Window;
import android.view.WindowManager;
import android.widget.EditText;
import android.widget.LinearLayout;
import android.widget.ListView;

import com.craftedeals.AppDelegate;
import com.craftedeals.Async.LocationAddress;
import com.craftedeals.Async.PlacesService;
import com.craftedeals.Async.PostAsync;
import com.craftedeals.Models.AdHocDealsModel;
import com.craftedeals.Models.CategoryModel;
import com.craftedeals.Models.HeroDealsModel;
import com.craftedeals.Models.Place;
import com.craftedeals.Models.PostAysnc_Model;
import com.craftedeals.R;
import com.craftedeals.Utils.Prefs;
import com.craftedeals.Utils.TransitionHelper;
import com.craftedeals.adapters.CategoryListItemAdapter;
import com.craftedeals.adapters.LocationMapAdapter;
import com.craftedeals.constants.ServerRequestConstants;
import com.craftedeals.constants.Tags;
import com.craftedeals.interfaces.OnListItemClickListener;
import com.craftedeals.interfaces.OnReciveServerResponse;
import com.github.aakira.expandablelayout.ExpandableRelativeLayout;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.List;
import java.util.Timer;
import java.util.TimerTask;

import carbon.widget.ImageView;
import carbon.widget.ProgressBar;
import carbon.widget.TextView;

/**
 * Created by juned on 10-Oct-16.
 */
public class SearchActivity extends AppCompatActivity implements View.OnClickListener, OnReciveServerResponse, OnListItemClickListener
{
    public static Activity mActivity;
    public static final int ADHOC = 1, HERO = 2;
    private Handler mHandler;
    private LinearLayout ll_adhoc_deals;
    private ImageView img_c_adhoc, img_c_hero;
    private EditText et_location, et_postal_code, et_search_by_keyword;
    private TextView txt_c_category;

    private HeroDealsModel heroDealsModel;
    private AdHocDealsModel adHocDealModel;
    double latitude = 0, longitude = 0;

    @Override
    protected void onCreate(Bundle savedInstanceState)
    {
        super.onCreate(savedInstanceState);
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.LOLLIPOP)
        {
            Window window = this.getWindow();
            Drawable background = this.getResources().getDrawable(R.drawable.topbg);
            window.addFlags(WindowManager.LayoutParams.FLAG_DRAWS_SYSTEM_BAR_BACKGROUNDS);
            window.setStatusBarColor(getResources().getColor(android.R.color.transparent));
            window.setNavigationBarColor(getResources().getColor(android.R.color.transparent));
            window.setBackgroundDrawable(background);
        }
        mActivity = this;
        setContentView(R.layout.search);
        setHandler();
        initView();
        AppDelegate.setStictModePermission();



    }

    @Override
    protected void onDestroy()
    {
        super.onDestroy();
        mActivity = null;
    }

    private void setHandler()
    {
        mHandler = new Handler()
        {
            @Override
            public void dispatchMessage(Message msg)
            {
                super.dispatchMessage(msg);
                AppDelegate.LogT("Handler called => " + msg.what);
                switch (msg.what)
                {
                    case 10:
                        AppDelegate.showProgressDialog(SearchActivity.this);
                        break;
                    case 11:
                        AppDelegate.hideProgressDialog(SearchActivity.this);
                        break;
                    case 2:
                        getLatLngFromGEOcoder(msg.getData());
                        break;
                    case 3:
                        if (locationSelected)
                        {
                            mHandler.sendEmptyMessage(10);
                            LocationAddress.getLocationFromReverseGeocoder(et_location.getText().toString(), SearchActivity.this, mHandler);
                        }
                        else
                        {
                            AppDelegate.showToast(SearchActivity.this, "Please select valid location.");
                        }
                        break;
                    case 4:
                        pb_c_location_progressbar.setVisibility(View.INVISIBLE);
                        try
                        {
                            if (adapter != null && search_list != null)
                            {
                                adapter.notifyDataSetChanged();
                                search_list.invalidate();
                            }
                            else
                            {
                                adapter = new LocationMapAdapter(SearchActivity.this, mapPlacesArray, SearchActivity.this);
                                search_list.setAdapter(adapter);
                            }
                        }
                        catch (Exception e)
                        {
                            AppDelegate.LogE(e);
                            adapter = new LocationMapAdapter(SearchActivity.this, mapPlacesArray, SearchActivity.this);
                            search_list.setAdapter(adapter);
                        }
                        if (mapPlacesArray != null && mapPlacesArray.size() > 0)
                        {
                            if (et_location.length() > 0)
                            {
                                search_list.setVisibility(View.VISIBLE);
                                no_location_text.setVisibility(View.GONE);
                            }
                            else
                            {
                                expandableView(exp_layout, COLLAPSE);
                            }
                        }
                        else
                        {
                            search_list.setVisibility(View.GONE);
                            if (AppDelegate.zeroResult)
                            {
                                search_list.setVisibility(View.GONE);
                                no_location_text.setText(getResources().getString(R.string.no_location_available_for_this_address));
                            }
                            else
                            {
                                search_list.setVisibility(View.GONE);
                                no_location_text.setText(getResources().getString(R.string.OVER_QUERY_LIMIT));
                            }
                            no_location_text.setVisibility(View.VISIBLE);
                            pb_c_location_progressbar.setVisibility(View.INVISIBLE);
                        }
                        break;
                    case 5:
                        AppDelegate.LogT("handler 5 canSearch => " + canSearch);
                        if (canSearch)
                        {
                            locationSelected = false;
                            latitude = 0;
                            longitude = 0;
                            if (et_location.length() != 0)
                            {
                                pb_c_location_progressbar.setVisibility(View.VISIBLE);
                                startTimerAndStop();
                                no_location_text.setVisibility(View.GONE);
                                expandableView(exp_layout, EXPAND);
                            }
                            else
                            {
                                expandableView(exp_layout, COLLAPSE);
                            }
                        }
                        else
                        {
                            if (et_location.length() != 0)
                            {
                            }
                            else
                            {
                                expandableView(exp_layout, COLLAPSE);
                            }
                        }
                        break;
                    case 21:
                        no_location_text.setText(getResources().getString(R.string.not_connected));
                        pb_c_location_progressbar.setVisibility(View.INVISIBLE);
                        no_location_text.setVisibility(View.VISIBLE);
                        search_list.setVisibility(View.GONE);
                        expandableView(exp_layout, EXPAND);
                        break;
                    case 22:
                        if (et_location.length() != 0)
                        {
                        }
                        else
                        {
                            expandableView(exp_layout, COLLAPSE);
                        }
                        break;
                }
            }
        };
    }

    private void getLatLngFromGEOcoder(Bundle data)
    {
        AppDelegate.LogT("for latlng => " + data.getString(Tags.forLatlng) + ", latitude =" + latitude + ", longitude = " + longitude);
        if (AppDelegate.isValidString(data.getString(Tags.forLatlng)) && data.getString(Tags.forLatlng).equalsIgnoreCase(Tags.forLatlng))
        {
            try
            {
                latitude = data.getDouble(Tags.latitude);
                longitude = data.getDouble(Tags.longitude);
                execute_Search();
            }
            catch (Exception e)
            {
                AppDelegate.LogE(e);
            }
        }
        else
        {
            AppDelegate.showToast(SearchActivity.this, "Please try again.");
        }
    }

    private void initView()
    {
        txt_c_category = (TextView) findViewById(R.id.txt_c_category);
        txt_c_category.setOnClickListener(this);

        findViewById(R.id.img_menu).setOnClickListener(this);
        img_c_hero = (ImageView) findViewById(R.id.img_c_hero);
        img_c_adhoc = (ImageView) findViewById(R.id.img_c_adhoc);
        findViewById(R.id.ll_adhoc_deals).setOnClickListener(this);
        findViewById(R.id.ll_hero_deals).setOnClickListener(this);
        setselection(HERO);
        et_location = (EditText) findViewById(R.id.et_location);
        et_location.setTypeface(Typeface.createFromAsset(getAssets(), getString(R.string.font_regular)));
        et_postal_code = (EditText) findViewById(R.id.et_postal_code);
        et_postal_code.setTypeface(Typeface.createFromAsset(getAssets(), getString(R.string.font_regular)));
        et_postal_code.setOnClickListener(this);
        et_search_by_keyword = (EditText) findViewById(R.id.et_search_by_keyword);
        et_search_by_keyword.setTypeface(Typeface.createFromAsset(getAssets(), getString(R.string.font_regular)));
        findViewById(R.id.txt_c_search).setOnClickListener(this);
        addtextWatcher();
        /*Location init*/
        no_location_text = (TextView) findViewById(R.id.no_location_text);
        search_list = (ListView) findViewById(R.id.search_list);
        adapter = new LocationMapAdapter(SearchActivity.this, mapPlacesArray, this);
        search_list.setAdapter(adapter);
        pb_c_location_progressbar = (ProgressBar) findViewById(R.id.pb_c_location_progressbar);
        exp_layout = (ExpandableRelativeLayout) findViewById(R.id.exp_layout);
        expandableView(exp_layout, COLLAPSE);
    }

    private void addtextWatcher()
    {
        et_location.addTextChangedListener(new TextWatcher()
        {
            @Override
            public void beforeTextChanged(CharSequence s, int start, int count, int after)
            {
            }

            @Override
            public void onTextChanged(CharSequence s, int start, int before, int count)
            {
            }

            @Override
            public void afterTextChanged(Editable s)
            {
//                clr_View(LABEL_LOCATION);
                AppDelegate.LogT("et_location afterTextChanged called");
                mHandler.sendEmptyMessage(5);
            }
        });
        et_postal_code.addTextChangedListener(new TextWatcher()
        {
            @Override
            public void beforeTextChanged(CharSequence s, int start, int count, int after)
            {
            }

            @Override
            public void onTextChanged(CharSequence s, int start, int before, int count)
            {
            }

            @Override
            public void afterTextChanged(Editable s)
            {
//                clr_View(LABEL_POSTAL_CODE);
            }
        });
        et_search_by_keyword.addTextChangedListener(new TextWatcher()
        {
            @Override
            public void beforeTextChanged(CharSequence s, int start, int count, int after)
            {
            }

            @Override
            public void onTextChanged(CharSequence s, int start, int before, int count)
            {
            }

            @Override
            public void afterTextChanged(Editable s)
            {
//                clr_View(LABEL_KEYWORD);
            }
        });
    }

    @Override
    public void onClick(View v)
    {
        switch (v.getId())
        {
            case R.id.img_menu:
                finish();
                break;
            case R.id.ll_adhoc_deals:
//                setselection(ADHOC);
                AppDelegate.showAlert(SearchActivity.this, "\"Exciting Tap-A-Deals\" coming soon");
                break;
            case R.id.ll_hero_deals:
                setselection(HERO);
                break;
            case R.id.txt_c_search:
                if (et_location.length() != 0)
                {
                    mHandler.sendEmptyMessage(3);
                }
                else
                {
                    execute_Search();
                }
                break;
            case R.id.txt_c_category:
                showCustomCategoryDialog();
                break;
        }
    }

    /* show Category dialog */
    private RecyclerView recycler_deals;
    private CategoryListItemAdapter mAdapter;
    private ArrayList<CategoryModel> categoryModelArrayList;
    private boolean[] is_checked;
    private String category_id = "";

    private void showCustomCategoryDialog()
    {
        categoryModelArrayList = new Prefs(this).getCategoryArryList();
        if (categoryModelArrayList != null)
        {
            if (is_checked == null || is_checked.length != categoryModelArrayList.size())
            {
                for (int i = 0; i < categoryModelArrayList.size(); i++)
                {
                    categoryModelArrayList.get(i).checked = 1;
                }
                is_checked = new boolean[categoryModelArrayList.size()];
                for (int i = 0; i < categoryModelArrayList.size(); i++)
                {
                    if (category_id.equalsIgnoreCase(categoryModelArrayList.get(i).id + ""))
                    {
                        is_checked[i] = true;
                    }
                    else
                    {
                        is_checked[i] = false;
                    }
                }
            }
            else
            {
                for (int i = 0; i < is_checked.length; i++)
                {
                    categoryModelArrayList.get(i).checked = is_checked[i] ? 1 : 0;
                }
            }

            final Dialog categoryDialog = new Dialog(this, android.R.style.Theme_Light);
            categoryDialog.requestWindowFeature(Window.FEATURE_NO_TITLE);
            categoryDialog.getWindow().setBackgroundDrawable(new ColorDrawable(getResources().getColor(android.R.color.transparent)));
            categoryDialog.setContentView(R.layout.dialog_filter);
            recycler_deals = (RecyclerView) categoryDialog.findViewById(R.id.recycler_deals);
            recycler_deals.setNestedScrollingEnabled(false);
            recycler_deals.setLayoutManager(new LinearLayoutManager(this));
            recycler_deals.setItemAnimator(new DefaultItemAnimator());

            mAdapter = new CategoryListItemAdapter(this, categoryModelArrayList, new OnListItemClickListener()
            {
                @Override
                public void setOnListItemClickListener(String name, int position)
                {
                }

                @Override
                public void setOnListItemClickListener(String name, int position, boolean like_dislike)
                {
                    AppDelegate.LogT("interface value => " + like_dislike);
                    categoryModelArrayList.get(position).checked = like_dislike ? 0 : 1;
                    mAdapter.notifyDataSetChanged();
                    recycler_deals.invalidate();
                }
            });
            recycler_deals.setAdapter(mAdapter);

            categoryDialog.findViewById(R.id.txt_c_submit).setOnClickListener(new View.OnClickListener()
            {
                @Override
                public void onClick(View v)
                {
                    StringBuilder stringBuilder = null;
                    for (int i = 0; i < categoryModelArrayList.size(); i++)
                    {
                        if (categoryModelArrayList.get(i).checked == 1)
                        {
                            is_checked[i] = true;
                            if (stringBuilder == null)
                            {
                                stringBuilder = new StringBuilder();
                                stringBuilder.append("" + categoryModelArrayList.get(i).id);
                            }
                            else
                            {
                                stringBuilder.append("," + categoryModelArrayList.get(i).id);
                            }
                        }
                        else
                        {
                            is_checked[i] = false;
                        }
                    }
                    if (stringBuilder != null)
                    {
                        category_id = stringBuilder.toString();
                        categoryDialog.dismiss();
                        setName();
                    }
                    else
                    {
                        AppDelegate.showToast(SearchActivity.this, "Please select category.");
                    }
                }
            });
            categoryDialog.findViewById(R.id.txt_c_cancel).setOnClickListener(new View.OnClickListener()
            {
                @Override
                public void onClick(View v)
                {
                    categoryDialog.dismiss();
                }
            });
            categoryDialog.show();
        }
    }

    public void setName()
    {
        AppDelegate.LogT("setName => category_id = " + category_id);
        if (AppDelegate.isValidString(category_id))
        {
            if (category_id.contains(","))
            {
                String[] array = category_id.split(",");
                StringBuilder stringBuilder = null;
                for (int j = 0; j < array.length; j++)
                {
                    for (int i = 0; i < categoryModelArrayList.size(); i++)
                    {
                        if (array[j].equalsIgnoreCase(categoryModelArrayList.get(i).id + ""))
                        {
                            if (stringBuilder == null)
                            {
                                stringBuilder = new StringBuilder();
                                stringBuilder.append(categoryModelArrayList.get(i).title + "");
                            }
                            else
                            {
                                stringBuilder.append(", " + categoryModelArrayList.get(i).title + "");
                            }
                        }
                    }
                }
                if (stringBuilder != null)
                {
                    txt_c_category.setText(stringBuilder.toString());
                }
            }
            else
            {
                for (int i = 0; i < categoryModelArrayList.size(); i++)
                {
                    if (category_id.equalsIgnoreCase(categoryModelArrayList.get(i).id + ""))
                    {
                        txt_c_category.setText(categoryModelArrayList.get(i).title);
                    }
                }
            }
        }
    }


    private void execute_Search()
    {
        try
        {
            if (AppDelegate.haveNetworkConnection(this, true))
            {
                ArrayList<PostAysnc_Model> mPostArrayList = new ArrayList<>();
                AppDelegate.getInstance(this).setPostParamsSecond(mPostArrayList, Tags.API_KEY, Tags.API_KEY_VALUE);
                //Log.e(Tags.API_KEY, Tags.API_KEY_VALUE);

                AppDelegate.getInstance(this).setPostParamsSecond(mPostArrayList, Tags.LAT, "", ServerRequestConstants.Key_PostDoubleValue);
                //Log.e("Tags.LAT 1", "blank in params");
                AppDelegate.getInstance(this).setPostParamsSecond(mPostArrayList, Tags.LONG, "", ServerRequestConstants.Key_PostDoubleValue);
                //Log.e("Tags.LONG 1", "blank in params");

                if (img_c_adhoc.isSelected())
                {
                    AppDelegate.getInstance(this).setPostParamsSecond(mPostArrayList, Tags.dealStatus, DealsActivity.DEAL_ADHOC);
                    //Log.e(Tags.dealStatus, DealsActivity.DEAL_ADHOC + "");
                }
                else
                {
                    AppDelegate.getInstance(this).setPostParamsSecond(mPostArrayList, Tags.dealStatus, DealsActivity.DEAL_HERO);
                    //Log.e(Tags.dealStatus, DealsActivity.DEAL_HERO + "");
                }

                if (AppDelegate.isValidString(et_postal_code.getText().toString()))
                {
                    AppDelegate.getInstance(this).setPostParamsSecond(mPostArrayList, Tags.postalCode, Integer.parseInt(et_postal_code.getText().toString()), ServerRequestConstants.Key_PostintValue);
                    //Log.e(Tags.postalCode, Integer.parseInt(et_postal_code.getText().toString()) + "");
                }

                AppDelegate.LogT("Latitude=" + latitude + ", longitude=" + longitude);
                if (et_search_by_keyword.length() == 0)
                {
                    if (AppDelegate.isValidString(et_location.getText().toString()) && latitude != 0 && longitude != 0)
                    {
                        AppDelegate.getInstance(this).setPostParamsSecond(mPostArrayList, Tags.LAT, latitude, ServerRequestConstants.Key_PostDoubleValue);
                        //Log.e(Tags.LAT, latitude + "");
                        AppDelegate.getInstance(this).setPostParamsSecond(mPostArrayList, Tags.LONG, longitude, ServerRequestConstants.Key_PostDoubleValue);
                        //Log.e(Tags.LONG, longitude + "");
                    }
                    else
                    {
                        if (new Prefs(this).getCitydata() != null)
                        {
                            AppDelegate.getInstance(this).setPostParamsSecond(mPostArrayList, Tags.LAT, new Prefs(this).getCitydata().lat);
                            //Log.e(Tags.LAT, new Prefs(this).getCitydata().lat + "");
                            AppDelegate.getInstance(this).setPostParamsSecond(mPostArrayList, Tags.LONG, new Prefs(this).getCitydata().lng);
                            //Log.e(Tags.LONG, new Prefs(this).getCitydata().lng + "");
                        }
                    }
                }
                else
                {
                    if (AppDelegate.isValidString(et_location.getText().toString()) && latitude != 0 && longitude != 0)
                    {
                        AppDelegate.getInstance(this).setPostParamsSecond(mPostArrayList, Tags.LAT, latitude, ServerRequestConstants.Key_PostDoubleValue);
                        //Log.e(Tags.LAT, latitude + "");

                        AppDelegate.getInstance(this).setPostParamsSecond(mPostArrayList, Tags.LONG, longitude, ServerRequestConstants.Key_PostDoubleValue);
                        //Log.e(Tags.LONG, longitude + "");
                    }
                }

                if (new Prefs(SearchActivity.this).getUserdata() != null)
                {
                    AppDelegate.getInstance(this).setPostParamsSecond(mPostArrayList, Tags.user_id, new Prefs(SearchActivity.this).getUserdata().id);
                    //Log.e(Tags.user_id, new Prefs(SearchActivity.this).getUserdata().id + "");
                }

                AppDelegate.getInstance(this).setPostParamsSecond(mPostArrayList, Tags.sortby, "near_me");
                //Log.e(Tags.sortby, "near_me");

                AppDelegate.getInstance(this).setPostParamsSecond(mPostArrayList, Tags.postalCode, "");
                //Log.e(Tags.postalCode, "");

                AppDelegate.getInstance(this).setPostParamsSecond(mPostArrayList, Tags.categoryId, AppDelegate.isValidString(category_id) ? category_id : "");
                //Log.e(Tags.categoryId, AppDelegate.isValidString(category_id) ? category_id : "");

                AppDelegate.getInstance(this).setPostParamsSecond(mPostArrayList, Tags.keyword, et_search_by_keyword.getText().toString());
                //Log.e(Tags.keyword, et_search_by_keyword.getText().toString());

                PostAsync mPostAsyncObj;
                mPostAsyncObj = new PostAsync(this, this, ServerRequestConstants.SEARCH, mPostArrayList, null);
                mHandler.sendEmptyMessage(10);
                mPostAsyncObj.execute();
            }
        }
        catch (Exception e)
        {
            AppDelegate.showToast(this, "Please try again.");
            AppDelegate.LogE(e);
        }
    }


    private void setselection(int adhoc)
    {
        img_c_adhoc.setSelected(false);
        img_c_hero.setSelected(false);
        switch (adhoc)
        {
            case ADHOC:
                img_c_adhoc.setSelected(true);
                break;
            case HERO:
                img_c_hero.setSelected(true);
                break;
        }
    }

    @Override
    public void setOnReciveResult(String apiName, String result)
    {
        mHandler.sendEmptyMessage(11);
        if (!AppDelegate.isValidString(result))
        {
            AppDelegate.showToast(SearchActivity.this, getResources().getString(R.string.time_out));
            return;
        }
        if (apiName.equals(ServerRequestConstants.SEARCH))
        {
            if (img_c_hero.isSelected())
            {
                parseHeroDeals(result);
            }
            else
            {
                parseAdHocDeals(result);
            }
        }
    }

    private void parseAdHocDeals(String result)
    {
        try
        {
            JSONObject jsonobj = new JSONObject(result);
            if (jsonobj.getInt(Tags.status) == 1 && jsonobj.getInt(Tags.dataFlow) == 1)
            {
                CategoryModel category = new CategoryModel();
                ArrayList<AdHocDealsModel> adHocDealModelArrayList = new ArrayList<>();
                JSONArray array = jsonobj.getJSONArray(Tags.response);
                for (int i = 0; i < array.length(); i++)
                {
                    adHocDealModel = new AdHocDealsModel();
                    JSONObject heroobj = array.getJSONObject(i);
                    adHocDealModel.distance = heroobj.optString(Tags.distance);
                    adHocDealModel.id = heroobj.getInt(Tags.id);
                    adHocDealModel.user_id = heroobj.getInt(Tags.user_id);
                    adHocDealModel.name = heroobj.getString(Tags.name);
                    adHocDealModel.use_the_offer = heroobj.getString(Tags.use_the_offer);
                    adHocDealModel.things_to_remebers = heroobj.getString(Tags.things_to_remebers);
                    adHocDealModel.deal_category_id = heroobj.getInt(Tags.deal_category_id);
                    adHocDealModel.sub_category_id = heroobj.getInt(Tags.sub_category_id);
                    adHocDealModel.facilities = heroobj.getString(Tags.facilities);
                    adHocDealModel.item_detail = heroobj.getString(Tags.item_detail);
                    adHocDealModel.deal_type = heroobj.getInt(Tags.deal_type);
                    adHocDealModel.starting_date = heroobj.getString(Tags.starting_date);
                    adHocDealModel.expiry_date = heroobj.getString(Tags.expiry_date);
                    adHocDealModel.price = heroobj.getInt(Tags.price);
                    adHocDealModel.discount_cost = heroobj.getInt(Tags.discount_cost);
                    adHocDealModel.total_deal = heroobj.getInt(Tags.total_deal);
                    adHocDealModel.volume_of_deal = heroobj.getInt(Tags.volume_of_deal);
                    adHocDealModel.timing_to = heroobj.getString(Tags.timing_to);
                    adHocDealModel.timing_from = heroobj.getString(Tags.timing_from);
                    adHocDealModel.valid_on = heroobj.getString(Tags.valid_on);
                    adHocDealModel.vendor_delete_deal = heroobj.getInt(Tags.vendor_delete_deal);
                    adHocDealModel.favourite = heroobj.getInt(Tags.favourite);
                    if (heroobj.has(Tags.liked))
                    {
                        adHocDealModel.liked = heroobj.getInt(Tags.liked);
                    }
                    if (AppDelegate.isValidString(heroobj.getString(Tags.user)))
                    {
                        JSONObject vendor = heroobj.getJSONObject(Tags.user);
                        adHocDealModel.vendor_name = vendor.getString(Tags.name);
                        if (vendor.has(Tags.busines) && AppDelegate.isValidString(vendor.getString(Tags.busines)))
                        {
                            //  JSONObject business = vendor.getJSONObject(Tags.busines);
                            adHocDealModel.latitude = vendor.getJSONObject(Tags.busines).getString(Tags.latitude);
                            adHocDealModel.longitude = vendor.getJSONObject(Tags.busines).getString(Tags.longitude);
                            adHocDealModel.company_address = vendor.getJSONObject(Tags.busines).getString(Tags.location_address);
                            adHocDealModel.cityName = vendor.getJSONObject(Tags.busines).getString(Tags.cityName);
                        }
                    }
                    try
                    {
                        if (AppDelegate.isValidString(heroobj.getString(Tags.images)))
                        {
                            JSONArray img = heroobj.getJSONArray(Tags.images);
                            AppDelegate.LogT("Image array length is=" + img.length() + "");
                            for (int k = 0; k < img.length(); k++)
                            {
                                AppDelegate.LogT("Image array is=" + img);
                                JSONObject json = img.getJSONObject(k);
                                adHocDealModel.image = json.getString(Tags.image);
                                if (json.has(Tags.type_id))
                                {
                                    adHocDealModel.adhocdeal_id = json.getInt(Tags.type_id);
                                }
                            }
                        }
                    }
                    catch (Exception e)
                    {
                        AppDelegate.LogE(e);
                    }
                    if (heroobj.has(Tags.image))
                    {
                        adHocDealModel.image = heroobj.getString(Tags.image);
                    }
                    JSONObject obj = heroobj.getJSONObject(Tags.deal_category);
                    category = new CategoryModel();
                    category.id = obj.getInt(Tags.id);
                    category.title = obj.getString(Tags.title);
                    category.description = obj.getString(Tags.description);
                    category.image = obj.getString(Tags.image);
                    adHocDealModel.category = category.title;
                    adHocDealModel.markerImage = obj.getString(Tags.marker_icon);
                    adHocDealModelArrayList.add(adHocDealModel);
                }
                AppDelegate.LogT("adHocDealModelArrayList==" + adHocDealModelArrayList.size());

                startActivity(adHocDealModelArrayList);
            }
            else
            {
                AppDelegate.showToast(this, jsonobj.getString(Tags.message));
            }
        }
        catch (JSONException e)
        {
            AppDelegate.LogE(e);
        }
    }

    private void startActivity(Object object)
    {
        Intent intent = new Intent(this, SearchedDealsActivity.class);
        if (img_c_adhoc.isSelected())
        {
            intent.putExtra(Tags.deal_type, DealsActivity.DEAL_ADHOC);
            intent.putParcelableArrayListExtra(Tags.deal, (ArrayList<AdHocDealsModel>) object);
        }
        else
        {
            intent.putExtra(Tags.deal_type, DealsActivity.DEAL_HERO);
            intent.putParcelableArrayListExtra(Tags.deal, (ArrayList<HeroDealsModel>) object);
        }

        intent.putExtra(Tags.category_id, category_id);
        intent.putExtra(Tags.postal_code, et_postal_code.getText().toString());
        if (AppDelegate.isValidString(et_location.getText().toString()) && latitude != 0 && longitude != 0)
        {
            intent.putExtra(Tags.LAT, latitude + "");
            intent.putExtra(Tags.LONG, longitude + "");
            intent.putExtra(Tags.current_location, false);
        }
        intent.putExtra(Tags.keyword, et_search_by_keyword.getText().toString() + "");
        intent.putParcelableArrayListExtra(Tags.category, new Prefs(this).getCategoryArryList());

        final Pair<View, String>[] pairs = TransitionHelper.createSafeTransitionParticipants(SearchActivity.this, false, new Pair<>(findViewById(R.id.txt_c_location_title), "square_blue_name"));
        ActivityOptionsCompat transitionActivityOptions = ActivityOptionsCompat.makeSceneTransitionAnimation(SearchActivity.this, pairs);
        startActivity(intent, transitionActivityOptions.toBundle());
    }

    private void parseHeroDeals(String result)
    {
        try
        {
            JSONObject jsonobj = new JSONObject(result);
            if (jsonobj.getInt(Tags.status) == 1 && jsonobj.getInt(Tags.dataFlow) == 1)
            {
                CategoryModel category = new CategoryModel();
                ArrayList<HeroDealsModel> heroDealsModelArrayList = new ArrayList<>();
                JSONArray array = jsonobj.getJSONArray(Tags.response);
                for (int i = 0; i < array.length(); i++)
                {
                    heroDealsModel = new HeroDealsModel();
                    JSONObject heroobj = array.getJSONObject(i);
                    heroDealsModel.distance = heroobj.optString(Tags.distance);
                    heroDealsModel.id = heroobj.getInt(Tags.id);
                    heroDealsModel.user_id = heroobj.getInt(Tags.user_id);
                    heroDealsModel.name = heroobj.getString(Tags.name);
                    heroDealsModel.herodeal_title = heroobj.getString(Tags.name);
                    if (heroobj.has(Tags.deal_category))
                    {
                        heroDealsModel.category = heroobj.getJSONObject(Tags.deal_category).getString(Tags.title);
                    }
                    heroDealsModel.deal_category_id = heroobj.getInt(Tags.deal_category_id);
                    heroDealsModel.image = heroobj.getString(Tags.image);
                    heroDealsModel.sub_category_id = heroobj.getInt(Tags.sub_category_id);
                    heroDealsModel.description = heroobj.getString(Tags.description);
                    heroDealsModel.value_upto = heroobj.getInt(Tags.value_upto);
                    heroDealsModel.discount_offer = heroobj.getString(Tags.discount_offer);
                    heroDealsModel.starting_date = heroobj.getString(Tags.starting_date);
                    heroDealsModel.expiry_date = heroobj.getString(Tags.expiry_date);
                    heroDealsModel.trems_and_conditions = heroobj.getString(Tags.trems_and_conditions);
                    heroDealsModel.total_deal = heroobj.getInt(Tags.total_deal);
                    if (AppDelegate.isValidString(heroobj.getString(Tags.user)))
                    {
                        JSONObject vendor = heroobj.getJSONObject(Tags.user);
                        heroDealsModel.vendor_name = vendor.getString(Tags.name);
                        if (vendor.has(Tags.busines) && AppDelegate.isValidString(vendor.getString(Tags.busines)))
                        {
                            JSONObject business = vendor.getJSONObject(Tags.busines);
                            heroDealsModel.latitude = business.getString(Tags.latitude);
                            heroDealsModel.longitude = business.getString(Tags.longitude);
                            heroDealsModel.company_address = business.getString(Tags.location_address);
                            heroDealsModel.cityName = business.getString(Tags.cityName);
                        }
                    }
                    JSONObject obj = heroobj.getJSONObject(Tags.deal_category);
                    category = new CategoryModel();
                    category.id = obj.getInt(Tags.id);
                    category.title = obj.getString(Tags.title);
                    category.description = obj.getString(Tags.description);
                    category.image = obj.getString(Tags.image);
                    heroDealsModel.category = category.title;
                    heroDealsModel.markerImage = obj.getString(Tags.marker_icon);
                    heroDealsModelArrayList.add(heroDealsModel);
                }
                AppDelegate.LogT("heroDealsModelArrayList==> " + heroDealsModelArrayList.size());
                startActivity(heroDealsModelArrayList);
            }
            else
            {
                AppDelegate.showToast(this, jsonobj.getString(Tags.message));
            }
        }
        catch (JSONException e)
        {
            AppDelegate.LogE(e);
        }
    }

    /* Location search */
    public List<Place> mapPlacesArray = new ArrayList<>();
    public Thread mThreadOnSearch;
    public LocationMapAdapter adapter;
    public ListView search_list;
    public TextView no_location_text;
    public ProgressBar pb_c_location_progressbar;
    private boolean canSearch = true, locationSelected = false;
    public Place placeDetail;
    public ExpandableRelativeLayout exp_layout;

    public static final int COLLAPSE = 0, EXPAND = 1;

    public void expandableView(final ExpandableRelativeLayout expLayout, final int value)
    {
        expLayout.post(new Runnable()
        {

            @Override
            public void run()
            {
                if (value == COLLAPSE)
                {
                    expLayout.collapse();
                }
                else
                {
                    expLayout.expand();
                }
            }
        });
    }

    private Timer mTimer;
    private TimerTask mtimerTask;
    private Handler mTimerHandler = new Handler();
    private Runnable mTimerRunnable = new Runnable()
    {
        @Override
        public void run()
        {
            if (!AppDelegate.haveNetworkConnection(SearchActivity.this, false))
            {
                mHandler.sendEmptyMessage(21);
            }
            else if (et_location.length() == 0)
            {
                mHandler.sendEmptyMessage(22);
            }
            else
            {
                mapPlacesArray.clear();
                mapPlacesArray.addAll(PlacesService.autocompleteForMap(et_location.getText().toString()));
                mHandler.sendEmptyMessage(4);
                canSearch = true;
            }
        }
    };

    private void stopTimer()
    {
        if (mTimer != null)
        {
            mTimer.cancel();
            mTimer.purge();
        }
        if (mTimerHandler != null && mTimerRunnable != null)
        {
            mTimerHandler.removeCallbacks(mTimerRunnable);
        }
    }

    private void startTimerAndStop()
    {
        stopTimer();
        onlyStopThread(mThreadOnSearch);
        if (!AppDelegate.haveNetworkConnection(SearchActivity.this, false))
        {
            no_location_text.setText(getResources().getString(R.string.not_connected));
            pb_c_location_progressbar.setVisibility(View.INVISIBLE);
            no_location_text.setVisibility(View.VISIBLE);
            search_list.setVisibility(View.GONE);
            expandableView(exp_layout, EXPAND);
        }
        else
        {
            expandableView(exp_layout, EXPAND);
            mTimer = new Timer();
            mtimerTask = new TimerTask()
            {
                public void run()
                {
                    mThreadOnSearch = new Thread(mTimerRunnable);
                    mThreadOnSearch.start();
                }
            };
            mTimer.schedule(mtimerTask, 1000);
        }
    }

    private void onlyStopThread(Thread mThreadOnSearch)
    {
        if (mThreadOnSearch != null)
        {
            mThreadOnSearch.interrupt();
            mThreadOnSearch = null;
        }
    }

    @Override
    public void setOnListItemClickListener(String name, int position)
    {
        AppDelegate.hideKeyBoard(SearchActivity.this);
        if (name.equalsIgnoreCase(Tags.ADDRESS) && AppDelegate.haveNetworkConnection(SearchActivity.this))
        {
            canSearch = false;
            if (mapPlacesArray.size() > position)
            {
                placeDetail = mapPlacesArray.get(position);
                if (AppDelegate.isValidString(placeDetail.name) && AppDelegate.isValidString(placeDetail.vicinity))
                {
                    et_location.setText(placeDetail.name + ", " + placeDetail.vicinity);
                }
                else if (AppDelegate.isValidString(placeDetail.name))
                {
                    et_location.setText(placeDetail.name);
                }
                else if (AppDelegate.isValidString(placeDetail.vicinity))
                {
                    et_location.setText(placeDetail.vicinity);
                }
                et_location.setSelection(et_location.length());
                expandableView(exp_layout, COLLAPSE);
                locationSelected = true;
            }
            new Handler().postDelayed(new Runnable()
            {
                @Override
                public void run()
                {
                    canSearch = true;
                }
            }, 200);
        }
    }

    @Override
    public void setOnListItemClickListener(String name, int position, boolean like_dislike)
    {
    }

}
