package com.craftedeals.activities;

import android.app.Activity;
import android.content.DialogInterface;
import android.content.Intent;
import android.graphics.drawable.Drawable;
import android.os.Build;
import android.os.Bundle;
import android.os.Handler;
import android.os.Message;
import android.support.annotation.Nullable;
import android.support.v7.app.AlertDialog;
import android.support.v7.app.AppCompatActivity;
import android.util.Log;
import android.view.View;
import android.view.Window;
import android.view.WindowManager;

import com.craftedeals.AppDelegate;
import com.craftedeals.Async.PostAsync;
import com.craftedeals.Models.MembershipModel;
import com.craftedeals.Models.PostAysnc_Model;
import com.craftedeals.Models.UserDataModel;
import com.craftedeals.R;
import com.craftedeals.Utils.Prefs;
import com.craftedeals.constants.ServerRequestConstants;
import com.craftedeals.constants.Tags;
import com.craftedeals.interfaces.OnReciveServerResponse;
import com.craftedeals.parser.JSONParser;
import com.paypal.android.sdk.payments.PayPalAuthorization;
import com.paypal.android.sdk.payments.PayPalFuturePaymentActivity;
import com.paypal.android.sdk.payments.PayPalPayment;
import com.paypal.android.sdk.payments.PayPalProfileSharingActivity;
import com.paypal.android.sdk.payments.PayPalService;
import com.paypal.android.sdk.payments.PaymentActivity;
import com.paypal.android.sdk.payments.PaymentConfirmation;

import org.json.JSONException;
import org.json.JSONObject;

import java.math.BigDecimal;
import java.util.ArrayList;

import carbon.widget.TextView;

/**
 * Created by Heena on 30-Jan-17.
 */
public class PaidUpgradeMembershipActivity extends AppCompatActivity implements OnReciveServerResponse, View.OnClickListener
{

    private MembershipModel membershipModel;
    public static Handler mHandler;
    private TextView txt_c_plan_type, txt_c_plan, txt_c_submit, txt_c_title, txt_c_get4, txt_c_get3, txt_c_get2, txt_c_get1;
    UserDataModel userDataModel;

    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState)
    {
        super.onCreate(savedInstanceState);
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.LOLLIPOP)
        {
            Window window = this.getWindow();
            Drawable background = this.getResources().getDrawable(R.drawable.topbg);
            window.addFlags(WindowManager.LayoutParams.FLAG_DRAWS_SYSTEM_BAR_BACKGROUNDS);
            window.setStatusBarColor(getResources().getColor(android.R.color.transparent));
            window.setNavigationBarColor(getResources().getColor(android.R.color.transparent));
            window.setBackgroundDrawable(background);
        }
        setContentView(R.layout.paid_upgrade_membership);
        initView();
        setHandler();
        mHandler.sendEmptyMessage(7);
    }

    @Override
    protected void onDestroy()
    {
        super.onDestroy();
        mHandler = null;
    }

    private void setHandler()
    {
        mHandler = new Handler()
        {
            @Override
            public void dispatchMessage(Message msg)
            {
                super.dispatchMessage(msg);
                switch (msg.what)
                {
                    case 10:
                        AppDelegate.showProgressDialog(PaidUpgradeMembershipActivity.this);
                        break;
                    case 11:
                        AppDelegate.hideProgressDialog(PaidUpgradeMembershipActivity.this);
                        break;
                    case 9:
                        setMemberShipData();
                        break;
                    case 7:
                        callMembershipAsync();
                        break;
                }
            }
        };
    }

    private void setMemberShipData()
    {
        txt_c_plan_type.setText(getString(R.string.PAID));
        txt_c_plan.setText(membershipModel.amount + "$\n\n" + membershipModel.month_of_membership + " MONTH");
    }

    private void initView()
    {
        findViewById(R.id.img_c_back).setOnClickListener(this);
        txt_c_plan = (TextView) findViewById(R.id.txt_c_plan);
        txt_c_plan_type = (TextView) findViewById(R.id.txt_c_plan_type);
        txt_c_submit = (TextView) findViewById(R.id.txt_c_submit);
        txt_c_submit.setOnClickListener(this);
        txt_c_title = (TextView) findViewById(R.id.txt_c_title);
        userDataModel = new Prefs(this).getUserdata();
        txt_c_get4 = (TextView) findViewById(R.id.txt_c_get4);
        txt_c_get3 = (TextView) findViewById(R.id.txt_c_get3);
        txt_c_get2 = (TextView) findViewById(R.id.txt_c_get2);
        txt_c_get1 = (TextView) findViewById(R.id.txt_c_get1);
        txt_c_title.setText("UPGRADE");
        txt_c_submit.setText("UPGRADE");
        if (userDataModel != null && userDataModel.membership_id == 1)
        {
            if (AppDelegate.isValidString(userDataModel.membership_start_date))
            {
                txt_c_get1.setText(getString(R.string.get1_upgrade));
                txt_c_get2.setText(getString(R.string.get2_upgrade));
                txt_c_get3.setText(getString(R.string.get3_upgrade));
                txt_c_get4.setText(getString(R.string.get4_upgrade));
            }
            else
            {
                txt_c_get1.setText(getString(R.string.get1_paid));
                txt_c_get2.setText(getString(R.string.get2_paid));
                txt_c_get3.setText(getString(R.string.get3_paid));
                txt_c_get4.setText(getString(R.string.get4_paid));
            }
        }
    }

    private void callMembershipAsync()
    {
        if (AppDelegate.haveNetworkConnection(this, true))
        {
            ArrayList<PostAysnc_Model> mPostArrayList = new ArrayList<PostAysnc_Model>();
            AppDelegate.getInstance(this).setPostParamsSecond(mPostArrayList, Tags.API_KEY, Tags.API_KEY_VALUE);
            PostAsync mPostAsyncObj = new PostAsync(this, this, ServerRequestConstants.GET_MEMBERSHIP_LIST, mPostArrayList, null);
            mHandler.sendEmptyMessage(10);
            mPostAsyncObj.execute();
        }
    }

    @Override
    public void setOnReciveResult(String apiName, String result)
    {
        if (mHandler == null)
        {
            return;
        }
        mHandler.sendEmptyMessage(11);
        if (!AppDelegate.isValidString(result))
        {
            AppDelegate.showToast(this, getString(R.string.time_out));
            return;
        }
        else if (apiName.equalsIgnoreCase(ServerRequestConstants.UPGRADE_MEMBERSHIP))
        {
            parseChangePassword(result);
        }
        else if (apiName.equalsIgnoreCase(ServerRequestConstants.GET_MEMBERSHIP_LIST))
        {
            parseMembershipList(result);
        }
    }

    private void parseMembershipList(String result)
    {
        try
        {
            JSONObject jsonObject = new JSONObject(result);
            JSONObject object = jsonObject.getJSONArray(Tags.response).getJSONObject(1);
            membershipModel = new MembershipModel();
            membershipModel.amount = JSONParser.getString(object, Tags.amount);
            membershipModel.membership_id = JSONParser.getString(object, Tags.membership_id);
            membershipModel.name = JSONParser.getString(object, Tags.name);
            membershipModel.month_of_membership = JSONParser.getString(object, Tags.month_of_membership);
            membershipModel.description = JSONParser.getString(object, Tags.description);
            membershipModel.status = JSONParser.getString(object, Tags.status);
            membershipModel.created = JSONParser.getString(object, Tags.created);
            mHandler.sendEmptyMessage(9);
        }
        catch (Exception e)
        {
            AppDelegate.LogE(e);
        }
    }

    private void parseChangePassword(String result)
    {
        try
        {
            JSONObject jsonObject = new JSONObject(result);
            if (jsonObject.getInt(Tags.status) == 1)
            {
                AppDelegate.showToast(this, jsonObject.getString(Tags.message));
                JSONObject data = jsonObject.getJSONObject("data");
                UserDataModel userDataModel = new Prefs(this).getUserdata();
                userDataModel.membership_start_date = JSONParser.getString(data, Tags.start_date);
                userDataModel.membership_end_date = JSONParser.getString(data, Tags.end_date);
                userDataModel.membership_id = JSONParser.getInt(data, Tags.membership_id);
                new Prefs(this).setUserData(userDataModel);
                if (userDataModel.membership_id == 2)
                {
                    Intent intent = new Intent(this, MembershipMessageActivity.class);
                    intent.putExtra(Tags.membership_id, userDataModel.membership_id);
                    intent.putExtra(Tags.membership_amount, membershipModel.amount);
                    intent.putExtra(Tags.login_from, 0);
                    startActivity(intent);
                }
                onBackPressed();

//                finish();
            }
            else
            {
                AppDelegate.showToast(this, jsonObject.getString(Tags.message));
            }
        }
        catch (JSONException e)
        {
            e.printStackTrace();
        }
    }

    @Override
    public void onClick(View v)
    {
        switch (v.getId())
        {
            case R.id.img_c_back:
                onBackPressed();
                break;
            case R.id.txt_c_submit:
                AlertDialog.Builder mAlert = new AlertDialog.Builder(PaidUpgradeMembershipActivity.this);
                mAlert.setCancelable(false);
                mAlert.setTitle("Upgrade Membership");
                mAlert.setMessage("Are you sure, you want to upgrade your account?");
                mAlert.setPositiveButton("OK", new DialogInterface.OnClickListener()
                {
                    @Override
                    public void onClick(DialogInterface dialog, int which)
                    {
                        onBuyPressed(membershipModel.amount, membershipModel.name);
                        dialog.dismiss();
                    }
                });
                mAlert.setNegativeButton("Cancel", new DialogInterface.OnClickListener()
                {
                    @Override
                    public void onClick(DialogInterface dialog, int which)
                    {
                        dialog.dismiss();
                    }
                });
                mAlert.show();
                break;
        }
    }

    private void execute_upgradeMembershipAsync(String paypal_id)
    {
        try
        {
            if (AppDelegate.haveNetworkConnection(this, true))
            {
                ArrayList<PostAysnc_Model> mPostArrayList = new ArrayList<PostAysnc_Model>();
                PostAsync mPostAsyncObj;
                AppDelegate.getInstance(this).setPostParamsSecond(mPostArrayList, Tags.API_KEY, Tags.API_KEY_VALUE);
                AppDelegate.getInstance(this).setPostParamsSecond(mPostArrayList, Tags.user_id, new Prefs(this).getUserdata().id);

                AppDelegate.getInstance(this).setPostParamsSecond(mPostArrayList, Tags.membership_id, membershipModel.membership_id + "");
                AppDelegate.getInstance(this).setPostParamsSecond(mPostArrayList, Tags.total_price, membershipModel.amount);
                AppDelegate.getInstance(this).setPostParamsSecond(mPostArrayList, Tags.paypal_id, paypal_id);

                mPostAsyncObj = new PostAsync(this, this, ServerRequestConstants.UPGRADE_MEMBERSHIP,
                        mPostArrayList, null);
                mHandler.sendEmptyMessage(10);
                mPostAsyncObj.execute();
            }
        }
        catch (Exception e)
        {
            AppDelegate.LogE(e);
            AppDelegate.showToast(this, getString(R.string.try_again));
        }
    }


    /*paypal integration code Start*/
    public void onBuyPressed(String str_amount, String str_item_name)
    {
        /*
         * PAYMENT_INTENT_SALE will cause the payment to complete immediately.
         * Change PAYMENT_INTENT_SALE to
         *   - PAYMENT_INTENT_AUTHORIZE to only authorize payment and capture funds later.
         *   - PAYMENT_INTENT_ORDER to create a payment for authorization and capture
         *     later via calls from your server.
         *
         * Also, to include additional payment details and an item list, see getStuffToBuy() below.
         */
        PayPalPayment thingToBuy = new PayPalPayment(new BigDecimal(str_amount), "USD", str_item_name, PayPalPayment.PAYMENT_INTENT_SALE);

        /*
         * See getStuffToBuy(..) for examples of some available payment options.
         */
        Intent intent = new Intent(PaidUpgradeMembershipActivity.this, PaymentActivity.class);

        // send the same configuration for restart resiliency
        intent.putExtra(PayPalService.EXTRA_PAYPAL_CONFIGURATION, AppDelegate.config);
        intent.putExtra(PaymentActivity.EXTRA_PAYMENT, thingToBuy);
        startActivityForResult(intent, AppDelegate.REQUEST_CODE_PAYMENT);
    }

//    private PayPalPayment getThingToBuy(String paymentIntent) {
//        return new PayPalPayment(new BigDecimal("0.01"), "USD", "sample item",
//                paymentIntent);
//    }

    private void startPayPalService()
    {
        Intent intent = new Intent(this, PayPalService.class);
        intent.putExtra(PayPalService.EXTRA_PAYPAL_CONFIGURATION, AppDelegate.config);
        startService(intent);
    }

    public String TAG = "paypal";

    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data)
    {
        if (requestCode == AppDelegate.REQUEST_CODE_PAYMENT)
        {
            if (resultCode == Activity.RESULT_OK)
            {
                PaymentConfirmation confirm =
                        data.getParcelableExtra(PaymentActivity.EXTRA_RESULT_CONFIRMATION);
                if (confirm != null)
                {
                    try
                    {
                        Log.i(TAG, confirm.toJSONObject().toString(4));
                        Log.i(TAG, confirm.getPayment().toJSONObject().toString(4));
                        /**
                         *  TODO: send 'confirm' (and possibly confirm.getPayment() to your server for verification
                         * or consent completion.
                         * See https://developer.paypal.com/webapps/developer/docs/integration/mobile/verify-mobile-payment/
                         * for more details.
                         *
                         * For sample mobile backend interactions, see
                         * https://github.com/paypal/rest-api-sdk-python/tree/master/samples/mobile_backend
                         */
                        JSONObject jsonObject = new JSONObject(confirm.toJSONObject().toString(4));
                        jsonObject.getJSONObject("response").getString("id");
                        AppDelegate.trackUpgradeAccount(new Prefs(this).getUserdata().name, new Prefs(this).getUserdata().id, "UPGRADE MEMBERSHIP ACTIVITY");
                        execute_upgradeMembershipAsync(jsonObject.getJSONObject("response").getString("id"));

                        AppDelegate.showToast(PaidUpgradeMembershipActivity.this, "PaymentConfirmation info received from PayPal, T ID is = " + jsonObject.getJSONObject("response").getString("id"));

                    }
                    catch (JSONException e)
                    {
                        Log.e(TAG, "an extremely unlikely failure occurred: ", e);
                    }
                }
            }
            else if (resultCode == Activity.RESULT_CANCELED)
            {
                Log.i(TAG, "The user cancelled.");
            }
            else if (resultCode == PaymentActivity.RESULT_EXTRAS_INVALID)
            {
                Log.i(
                        TAG,
                        "An invalid Payment or PayPalConfiguration was submitted. Please see the docs.");
            }
        }
        else if (requestCode == AppDelegate.REQUEST_CODE_FUTURE_PAYMENT)
        {
            if (resultCode == Activity.RESULT_OK)
            {
                PayPalAuthorization auth =
                        data.getParcelableExtra(PayPalFuturePaymentActivity.EXTRA_RESULT_AUTHORIZATION);
                if (auth != null)
                {
                    try
                    {
                        Log.i("FuturePaymentExample", auth.toJSONObject().toString(4));

                        String authorization_code = auth.getAuthorizationCode();
                        Log.i("FuturePaymentExample", authorization_code);

                        sendAuthorizationToServer(auth);
                        AppDelegate.showToast(PaidUpgradeMembershipActivity.this, "Future Payment code received from PayPal");

                    }
                    catch (JSONException e)
                    {
                        Log.e("FuturePaymentExample", "an extremely unlikely failure occurred: ", e);
                    }
                }
            }
            else if (resultCode == Activity.RESULT_CANCELED)
            {
                Log.i("FuturePaymentExample", "The user canceled.");
            }
            else if (resultCode == PayPalFuturePaymentActivity.RESULT_EXTRAS_INVALID)
            {
                Log.i(
                        "FuturePaymentExample",
                        "Probably the attempt to previously start the PayPalService had an invalid PayPalConfiguration. Please see the docs.");
            }
        }
        else if (requestCode == AppDelegate.REQUEST_CODE_PROFILE_SHARING)
        {
            if (resultCode == Activity.RESULT_OK)
            {
                PayPalAuthorization auth =
                        data.getParcelableExtra(PayPalProfileSharingActivity.EXTRA_RESULT_AUTHORIZATION);
                if (auth != null)
                {
                    try
                    {
                        Log.i("ProfileSharingExample", auth.toJSONObject().toString(4));

                        String authorization_code = auth.getAuthorizationCode();
                        Log.i("ProfileSharingExample", authorization_code);

                        sendAuthorizationToServer(auth);
                        AppDelegate.showToast(PaidUpgradeMembershipActivity.this, "Profile Sharing code received from PayPal");

                    }
                    catch (JSONException e)
                    {
                        Log.e("ProfileSharingExample", "an extremely unlikely failure occurred: ", e);
                    }
                }
            }
            else if (resultCode == Activity.RESULT_CANCELED)
            {
                Log.i("ProfileSharingExample", "The user canceled.");
            }
            else if (resultCode == PayPalFuturePaymentActivity.RESULT_EXTRAS_INVALID)
            {
                Log.i(
                        "ProfileSharingExample",
                        "Probably the attempt to previously start the PayPalService had an invalid PayPalConfiguration. Please see the docs.");
            }
        }
    }

    private void sendAuthorizationToServer(PayPalAuthorization authorization)
    {
        /**
         * TODO: Send the authorization response to your server, where it can
         * exchange the authorization code for OAuth access and refresh tokens.
         * Your server must then store these tokens, so that your server code
         * can execute payments for this user in the future.
         * A more complete example that includes the required app-server to
         * PayPal-server integration is available from
         * https://github.com/paypal/rest-api-sdk-python/tree/master/samples/mobile_backend
         */

    }
    /*paypal integration code Exit*/
}
