package com.craftedeals.activities;

import android.graphics.drawable.Drawable;
import android.os.Build;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v7.app.AppCompatActivity;
import android.text.Html;
import android.view.View;
import android.view.Window;
import android.view.WindowManager;

import com.craftedeals.AppDelegate;
import com.craftedeals.R;
import com.craftedeals.constants.Tags;

import carbon.widget.TextView;

import static com.craftedeals.activities.DealsActivity.DEAL_ADHOC;

/**
 * Created by Bharat on 27-Dec-16.
 */

public class CongratulationActivity extends AppCompatActivity {

    private TextView txt_c_message, txt_steps_1, txt_steps_2, txt_steps_3, txt_steps_4, txt_c_step;
    public static final int DEAL_BOUGHT = 1, DEAL_REDEEMED = 2;
    private int type = 0, deal_type = DEAL_ADHOC;
    public String str_deal_name = "Deal";

    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.LOLLIPOP) {
            Window window = this.getWindow();
            Drawable background = this.getResources().getDrawable(R.drawable.topbg);
            window.addFlags(WindowManager.LayoutParams.FLAG_DRAWS_SYSTEM_BAR_BACKGROUNDS);
            window.setStatusBarColor(getResources().getColor(android.R.color.transparent));
            window.setNavigationBarColor(getResources().getColor(android.R.color.transparent));
            window.setBackgroundDrawable(background);
        }
        setContentView(R.layout.congratulation_activity);
        str_deal_name = AppDelegate.isValidString(getIntent().getStringExtra(Tags.deal)) ? getIntent().getStringExtra(Tags.deal) : "Deal";
        type = getIntent().getIntExtra(Tags.types, 0);
        deal_type = getIntent().getIntExtra(Tags.deal_type, DEAL_ADHOC);
        initView();
    }

    private void initView() {
        txt_c_message = (TextView) findViewById(R.id.txt_c_message);
        txt_steps_1 = (TextView) findViewById(R.id.txt_steps_1);
        txt_steps_2 = (TextView) findViewById(R.id.txt_steps_2);
        txt_steps_3 = (TextView) findViewById(R.id.txt_steps_3);
        txt_steps_4 = (TextView) findViewById(R.id.txt_steps_4);
        txt_c_step = (TextView) findViewById(R.id.txt_c_step);
        if (type == DEAL_BOUGHT) {
            txt_c_step.setVisibility(View.VISIBLE);
            txt_c_message.setText(Html.fromHtml("You have successfully purchased your Craftedeal at  <b>" + str_deal_name + "</b> ."));
        } else {
            txt_c_step.setVisibility(View.GONE);
            if (deal_type == DEAL_ADHOC)
                txt_c_message.setText(Html.fromHtml("You have successfully redeemed your Craftedeal at  <b>" + str_deal_name + "</b> ."));
            else
                txt_c_message.setText(Html.fromHtml("You have successfully redeemed your Hero Deal at  <b>" + str_deal_name + "</b> ."));
        }
        if (deal_type == DEAL_ADHOC) {
            if (type == DEAL_BOUGHT) {
                txt_steps_1.setText(getString(R.string.step1_adhoc));
                txt_steps_2.setText(getString(R.string.step2_adhoc));
                txt_steps_3.setText(getString(R.string.step3_adhoc));
                txt_steps_4.setText(getString(R.string.step4_adhoc));
            } else {
                txt_steps_1.setText(getString(R.string.step1_hero));
                txt_steps_2.setText(getString(R.string.step2_adhocredeem));
                txt_steps_3.setText(getString(R.string.step3_hero));
                txt_steps_4.setText(getString(R.string.step4_hero));
            }
        } else {
            txt_steps_1.setText(getString(R.string.step1_hero));
            txt_steps_2.setText(getString(R.string.step2_hero));
            txt_steps_3.setText(getString(R.string.step3_hero));
            txt_steps_4.setText(getString(R.string.step4_hero));
        }
        findViewById(R.id.img_back).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                onBackPressed();
            }
        });
    }
}
