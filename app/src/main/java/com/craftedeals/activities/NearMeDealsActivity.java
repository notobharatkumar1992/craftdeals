package com.craftedeals.activities;

import android.app.Dialog;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.IntentSender;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.graphics.Color;
import android.graphics.drawable.Animatable;
import android.graphics.drawable.AnimationDrawable;
import android.graphics.drawable.BitmapDrawable;
import android.graphics.drawable.ColorDrawable;
import android.graphics.drawable.Drawable;
import android.location.Location;
import android.location.LocationManager;
import android.os.AsyncTask;
import android.os.Build;
import android.os.Bundle;
import android.os.Handler;
import android.os.Looper;
import android.os.Message;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.support.v7.app.AlertDialog;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.DefaultItemAnimator;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.text.Html;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.Window;
import android.view.WindowManager;
import android.widget.FrameLayout;
import android.widget.ImageView;
import android.widget.RelativeLayout;

import com.craftedeals.AppDelegate;
import com.craftedeals.Async.LocationAddress;
import com.craftedeals.Async.PostAsync;
import com.craftedeals.Models.AdHocDealsModel;
import com.craftedeals.Models.CategoryModel;
import com.craftedeals.Models.CityModel;
import com.craftedeals.Models.HeroDealsModel;
import com.craftedeals.Models.Person;
import com.craftedeals.Models.PostAysnc_Model;
import com.craftedeals.R;
import com.craftedeals.Utils.CircleImageView;
import com.craftedeals.Utils.Prefs;
import com.craftedeals.adapters.AdHocDealsAdapter;
import com.craftedeals.adapters.CategoryListItemAdapter;
import com.craftedeals.adapters.HeroDealsAdapter;
import com.craftedeals.constants.ServerRequestConstants;
import com.craftedeals.constants.Tags;
import com.craftedeals.interfaces.OnListItemClickListener;
import com.craftedeals.interfaces.OnReciveServerResponse;
import com.google.android.gms.common.ConnectionResult;
import com.google.android.gms.common.api.CommonStatusCodes;
import com.google.android.gms.common.api.GoogleApiClient;
import com.google.android.gms.common.api.PendingResult;
import com.google.android.gms.common.api.ResultCallback;
import com.google.android.gms.common.api.Status;
import com.google.android.gms.location.LocationRequest;
import com.google.android.gms.location.LocationServices;
import com.google.android.gms.location.LocationSettingsRequest;
import com.google.android.gms.location.LocationSettingsResult;
import com.google.android.gms.location.LocationSettingsStates;
import com.google.android.gms.location.LocationSettingsStatusCodes;
import com.google.android.gms.maps.CameraUpdateFactory;
import com.google.android.gms.maps.GoogleMap;
import com.google.android.gms.maps.OnMapReadyCallback;
import com.google.android.gms.maps.SupportMapFragment;
import com.google.android.gms.maps.model.BitmapDescriptorFactory;
import com.google.android.gms.maps.model.CameraPosition;
import com.google.android.gms.maps.model.LatLng;
import com.google.android.gms.maps.model.LatLngBounds;
import com.google.android.gms.maps.model.Marker;
import com.google.android.gms.maps.model.MarkerOptions;
import com.nostra13.universalimageloader.core.DisplayImageOptions;
import com.nostra13.universalimageloader.core.ImageLoader;
import com.nostra13.universalimageloader.core.ImageLoaderConfiguration;
import com.nostra13.universalimageloader.core.assist.FailReason;
import com.nostra13.universalimageloader.core.listener.ImageLoadingListener;

import org.apache.commons.lang3.text.WordUtils;
import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.io.IOException;
import java.io.InputStream;
import java.net.URL;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.Timer;
import java.util.TimerTask;

import carbon.widget.ProgressBar;
import carbon.widget.TextView;

/**
 * Created by Heena on 07-Oct-16.
 */
public class NearMeDealsActivity extends AppCompatActivity implements View.OnClickListener, OnReciveServerResponse, OnListItemClickListener, GoogleApiClient.ConnectionCallbacks, GoogleApiClient.OnConnectionFailedListener,
        com.google.android.gms.location.LocationListener {

    public static final int DEAL_HERO = 2, DEAL_ADHOC = 1, FROM_DASHBOARD = 0;
    private int deal_type = DEAL_HERO;
    private ArrayList<CategoryModel> categoryModelArrayList;

    private Prefs prefs;
    private TextView txt_c_title, txt_c_map, txt_c_list, txt_c_viewdeals;
    private FrameLayout list_container, map_container;
    public static Handler mHandler;
    public static final int LABEL_MAP = 1, LABEL_LIST = 2;
    private RecyclerView recycler_deals_list;
    private SupportMapFragment fragment;
    private GoogleMap googleMap;

    private HeroDealsModel heroDealsModel;
    private ArrayList<HeroDealsModel> heroDealsModelArrayList = new ArrayList<>();
    private ImageLoader imageLoader = ImageLoader.getInstance();
    private DisplayImageOptions options = new DisplayImageOptions.Builder().cacheInMemory(true)
            .cacheOnDisc(true).resetViewBeforeLoading(true).
                    build();
    private Drawable userImg;
    ArrayList<AdHocDealsModel> adHocDealModelArrayList = new ArrayList<>();
    private AdHocDealsModel adHocDealModel = new AdHocDealsModel();
    public static OnListItemClickListener onListItemClickListener;

    public String str_keyword = "", str_postal_code = "", str_lat = "", str_long = "";
    public AlertDialog.Builder alert;
    public String category_id = "";
    boolean apiShouldcall = false, firstTimeZoom = true;
    private HashMap<String, Person> markerPersonMap = new HashMap<>();
    private Marker customMarker;
    private ArrayList<Bitmap> arrayBitmap = new ArrayList<>();
    private double difference;

    private ProgressBar pb_c_location_progressbar;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.LOLLIPOP) {
            Window window = this.getWindow();
            Drawable background = this.getResources().getDrawable(R.drawable.topbg);
            window.addFlags(WindowManager.LayoutParams.FLAG_DRAWS_SYSTEM_BAR_BACKGROUNDS);
            window.setStatusBarColor(getResources().getColor(android.R.color.transparent));
            window.setNavigationBarColor(getResources().getColor(android.R.color.transparent));
            window.setBackgroundDrawable(background);
        }
        setContentView(R.layout.deals);
        imageLoader.init(ImageLoaderConfiguration.createDefault(this));
        prefs = new Prefs(this);
        onListItemClickListener = this;
        initView();
        initGPS();
        getValues();
        setHandler();
//        displayView(LABEL_LIST);
        new Handler().postDelayed(new Runnable() {
            @Override
            public void run() {
                fragment = SupportMapFragment.newInstance();
                getSupportFragmentManager().beginTransaction()
                        .replace(R.id.map_container, fragment, "MAP1").addToBackStack(null)
                        .commit();
                fragment.getMapAsync(new OnMapReadyCallback() {
                    @Override
                    public void onMapReady(GoogleMap googleMap) {
                        NearMeDealsActivity.this.googleMap = googleMap;
                        AppDelegate.LogT("Google Map ==" + NearMeDealsActivity.this.googleMap);
                        showMap();
                        if (mHandler == null)
                            return;
                        mHandler.sendEmptyMessage(3);
                        new DownloadImages().execute();
                    }
                });
            }
        }, 400);
    }

    @Override
    protected void onDestroy() {
        super.onDestroy();
        mHandler = null;
    }

    private void initGPS() {
        locationManager = (LocationManager) getSystemService(LOCATION_SERVICE);
        mGoogleApiClient = new GoogleApiClient.Builder(this)
                .addConnectionCallbacks(this)
                .addOnConnectionFailedListener(this)
                .addApi(LocationServices.API)
                .build();
        AppDelegate.LogT("mGoogleApiClient Initialited");
        mLocationRequest = LocationRequest.create()
                .setPriority(LocationRequest.PRIORITY_HIGH_ACCURACY)
                .setInterval(500)        // 10 seconds, in milliseconds
                .setFastestInterval(500); // 10 second, in milliseconds
        AppDelegate.LogT("mLocationRequest Initialited");
    }

    private void setloc(double latitude, double longitude) {
        // mHandler.sendEmptyMessage(10);
        LocationAddress.getAddressFromLocation(latitude, longitude, this, mHandler);

    }

    private void setAddressFromGEOcoder(Bundle data) {
        CityModel cityModel;
        if (new Prefs(this).getCitydata() != null)
            cityModel = new Prefs(this).getCitydata();
        else {
            cityModel = new CityModel();
            cityModel.location_type = Tags.current_location;
            cityModel.name = data.getString(Tags.city_param);
            cityModel.lat = String.valueOf(data.getDouble(Tags.LAT)) + "";
            cityModel.lng = String.valueOf(data.getDouble(Tags.LNG)) + "";
            new Prefs(this).setCityData(cityModel);
        }
        str_lat = cityModel.lat;
        str_long = cityModel.lng;
        switchSortBy(1);
        isClicked = false;
        execute_searchDeals(category_id);
    }


    private void setHandler() {
        mHandler = new Handler() {
            @Override
            public void dispatchMessage(Message msg) {
                super.dispatchMessage(msg);
                switch (msg.what) {
                    case 10:
                        AppDelegate.showProgressDialog(NearMeDealsActivity.this);
                        break;
                    case 11:
                        AppDelegate.hideProgressDialog(NearMeDealsActivity.this);
                        break;
                    case 20:
                        pb_c_location_progressbar.setVisibility(View.VISIBLE);
                        break;
                    case 21:
                        pb_c_location_progressbar.setVisibility(View.GONE);
                        break;
                    case 1:
                        break;
                    case 2:
                        setAddressFromGEOcoder(msg.getData());
                        break;
                    case 3:
                        deal_type = getIntent().getIntExtra(Tags.deal_type, DEAL_HERO);
                        str_postal_code = AppDelegate.isValidString(getIntent().getStringExtra(Tags.postal_code)) ? getIntent().getStringExtra(Tags.postal_code) : "";
                        str_lat = AppDelegate.isValidString(getIntent().getStringExtra(Tags.LAT)) ? getIntent().getStringExtra(Tags.LAT) : "";
                        str_long = AppDelegate.isValidString(getIntent().getStringExtra(Tags.LONG)) ? getIntent().getStringExtra(Tags.LONG) : "";
                        str_keyword = AppDelegate.isValidString(getIntent().getStringExtra(Tags.keyword)) ? getIntent().getStringExtra(Tags.keyword) : "";
                        if (deal_type == DEAL_HERO) {
                            heroDealsModelArrayList = getIntent().getParcelableArrayListExtra(Tags.deal);
                            txt_c_viewdeals.setText(getString(R.string.click_to_view_adhoc_deals));
                        } else {
                            adHocDealModelArrayList = getIntent().getParcelableArrayListExtra(Tags.deal);
                            txt_c_viewdeals.setText(getString(R.string.click_to_view_hero_deals));
                        }
                        categoryModelArrayList = getIntent().getParcelableArrayListExtra(Tags.category);
                        break;
                    case 4:
                        execute_searchDeals(category_id);
                        break;
                    case 25:
                        if (deal_type == DEAL_HERO)
                            show_DealsList(heroDealsModelArrayList);
                        else
                            show_AdHocDealsList(adHocDealModelArrayList);
                        break;
                }
            }
        };
    }

    private double distance(double lat1, double lon1, double lat2, double lon2) {
        double theta = lon1 - lon2;
        double dist = Math.sin(deg2rad(lat1)) * Math.sin(deg2rad(lat2)) + Math.cos(deg2rad(lat1)) * Math.cos(deg2rad(lat2)) * Math.cos(deg2rad(theta));
        dist = Math.acos(dist);
        dist = rad2deg(dist);
        dist = dist * 60 * 1.1515;
        dist = dist * 1.609344;
        return (dist);
    }

    private double deg2rad(double deg) {
        return (deg * Math.PI / 180.0);
    }

    private double rad2deg(double rad) {
        return (rad * 180.0 / Math.PI);
    }

    private void initView() {
        findViewById(R.id.img_c_back).setOnClickListener(this);
        txt_c_title = (TextView) findViewById(R.id.txt_c_title);
        txt_c_title.setText("Search");
        if (AppDelegate.isValidString(getIntent().getStringExtra(Tags.title))) {
            txt_c_title.setText(getIntent().getStringExtra(Tags.title));
        }

        findViewById(R.id.img_c_search).setOnClickListener(this);
        findViewById(R.id.ll_c_filter).setOnClickListener(this);
        findViewById(R.id.ll_c_sort).setOnClickListener(this);
        txt_c_map = (TextView) findViewById(R.id.txt_c_map);
        txt_c_map.setOnClickListener(this);
        txt_c_list = (TextView) findViewById(R.id.txt_c_list);
        txt_c_list.setOnClickListener(this);
        txt_c_viewdeals = (TextView) findViewById(R.id.txt_c_viewdeals);
        txt_c_viewdeals.setOnClickListener(this);
        list_container = (FrameLayout) findViewById(R.id.list_container);
        map_container = (FrameLayout) findViewById(R.id.map_container);
        recycler_deals_list = (RecyclerView) findViewById(R.id.recycler_deals_list);
        setselection(LABEL_MAP);

        pb_c_location_progressbar = (ProgressBar) findViewById(R.id.pb_c_location_progressbar);
        pb_c_location_progressbar.setVisibility(View.GONE);
    }

    private void showMap() {
        if (googleMap == null) {
            return;
        }
        googleMap.setMyLocationEnabled(true);
        googleMap.getUiSettings().setMapToolbarEnabled(false);
        googleMap.getUiSettings().setZoomControlsEnabled(false);
        googleMap.getUiSettings().setCompassEnabled(false);
        googleMap.setMapType(GoogleMap.MAP_TYPE_NORMAL);
        googleMap.moveCamera(CameraUpdateFactory.zoomIn());

        try {
            if (firstTimeZoom) {
                googleMap.moveCamera(CameraUpdateFactory.newLatLngZoom(new LatLng(currentLatitude, currentLongitude), 10));
                firstTimeZoom = false;
            }
//            googleMap.animateCamera(CameraUpdateFactory.newLatLngZoom(new LatLng(Double.parseDouble(new Prefs(this).getCitydata().lat), Double.parseDouble(new Prefs(this).getCitydata().lng)), 10));
        } catch (Exception e) {
            AppDelegate.LogE(e);
        }
        googleMap.setOnCameraChangeListener(new GoogleMap.OnCameraChangeListener() {
            @Override
            public void onCameraChange(final CameraPosition cameraPosition) {
                AppDelegate.LogT("onCameraChange => " + apiExecuting);
                if (apiExecuting) {
                    currentLatitude = cameraPosition.target.latitude;
                    currentLongitude = cameraPosition.target.longitude;
                    startTimerAndStop();
                }
            }
        });
    }

    public Thread mThreadOnSearch;
    private boolean apiExecuting = false;
    private Timer mTimer;
    private TimerTask mtimerTask;
    private Handler mTimerHandler = new Handler();
    private Runnable mTimerRunnable = new Runnable() {
        @Override
        public void run() {
            if (mHandler == null)
                return;
            if (!AppDelegate.haveNetworkConnection(NearMeDealsActivity.this, false)) {
                mHandler.sendEmptyMessage(21);
            } else {
                mHandler.sendEmptyMessage(20);
                apiExecuting = false;
                execute_searchDeals(category_id);
            }
        }
    };

    private void stopTimer() {
        if (mTimer != null) {
            mTimer.cancel();
            mTimer.purge();
        }
        if (mTimerHandler != null && mTimerRunnable != null) {
            mTimerHandler.removeCallbacks(mTimerRunnable);
        }
    }

    private void startTimerAndStop() {
        stopTimer();
        onlyStopThread(mThreadOnSearch);
        if (!AppDelegate.haveNetworkConnection(NearMeDealsActivity.this, false)) {
        } else {
            mTimer = new Timer();
            mtimerTask = new TimerTask() {
                public void run() {
                    mThreadOnSearch = new Thread(mTimerRunnable);
                    mThreadOnSearch.start();
                }
            };
            mTimer.schedule(mtimerTask, 1000);
        }
    }

    private void onlyStopThread(Thread mThreadOnSearch) {
        if (mThreadOnSearch != null) {
            mThreadOnSearch.interrupt();
            mThreadOnSearch = null;
        }
    }

    void setselection(int position) {
        txt_c_map.setSelected(false);
        txt_c_list.setSelected(false);
        list_container.setVisibility(View.GONE);
        map_container.setVisibility(View.GONE);
        switch (position) {
            case LABEL_MAP:
                map_container.setVisibility(View.VISIBLE);
                txt_c_map.setSelected(true);
                break;
            case LABEL_LIST:
                list_container.setVisibility(View.VISIBLE);
                txt_c_list.setSelected(true);
                break;
        }
    }


    private void displayView(int position) {
        AppDelegate.LogT("displayView called at NearMeDealsActivity");
        AppDelegate.hideKeyBoard(this);
        setselection(position);
        Fragment fragment = null;
        switch (position) {
            case LABEL_LIST:
                apiExecuting = false;
                AppDelegate.LogT("click heroDealsModelArrayList==" + heroDealsModelArrayList);
                if (deal_type == DEAL_HERO)
                    show_DealsList(heroDealsModelArrayList);
                else
                    show_AdHocDealsList(adHocDealModelArrayList);
                break;
            case LABEL_MAP:
                apiExecuting = false;
                if (deal_type == DEAL_HERO)
                    showpinOnMap(heroDealsModelArrayList);
                else
                    showAdHocpinOnMap(adHocDealModelArrayList);
                new Handler().postDelayed(new Runnable() {
                    @Override
                    public void run() {
                        apiExecuting = true;
                    }
                }, 800);
                break;

            default:
                break;
        }
        if (position != 2) {
            AppDelegate.LogE("Error in creating fragment");
        }
    }

    private void show_DealsList(ArrayList<HeroDealsModel> heroDealsModelArrayList) {
        HeroDealsAdapter mAdapter = new HeroDealsAdapter(this, heroDealsModelArrayList);
        RecyclerView.LayoutManager mLayoutManager = new LinearLayoutManager(this);
        recycler_deals_list.setLayoutManager(mLayoutManager);
        recycler_deals_list.setItemAnimator(new DefaultItemAnimator());
        recycler_deals_list.setAdapter(mAdapter);
    }

    @Override
    public void onClick(View v) {
        switch (v.getId()) {
            case R.id.img_c_back:
                onBackPressed();
                break;
            case R.id.img_c_search:
                startActivity(new Intent(this, SearchActivity.class));
                break;
            case R.id.ll_c_filter:
//                showCategoryDialog();
                showCustomCategoryDialog();
                break;
            case R.id.ll_c_sort:
                isClicked = false;
                showSortByDialog();
                break;
            case R.id.txt_c_map:
                //if (!(getSupportFragmentManager().findFragmentById(R.id.map_container) instanceof DealsMapFragment))
                displayView(LABEL_MAP);
                break;
            case R.id.txt_c_list:
                //if (!(getSupportFragmentManager().findFragmentById(R.id.list_container) instanceof DealsListFragment))
                displayView(LABEL_LIST);
                break;
            case R.id.list_container:
                break;
            case R.id.map_container:
                break;
            case R.id.txt_c_viewdeals:
                deal_type = (deal_type == DEAL_HERO ? DEAL_ADHOC : DEAL_HERO);
                if (deal_type == DEAL_ADHOC) {
                    txt_c_viewdeals.setText(getString(R.string.click_to_view_hero_deals));
                } else {
                    txt_c_viewdeals.setText(getString(R.string.click_to_view_adhoc_deals));
                }
                execute_searchDeals(category_id);
                break;
        }
    }

    @Override
    public void onBackPressed() {
        super.onBackPressed();
        super.onBackPressed();
    }

    private GoogleApiClient mGoogleApiClient;
    private LocationRequest mLocationRequest;

    public LocationManager locationManager;
    public boolean isClicked = false;
    private double currentLatitude = 0, currentLongitude = 0;
    private final static int CONNECTION_FAILURE_RESOLUTION_REQUEST = 9000;

    private void showGPSalert() {
        if (mGoogleApiClient == null) {
            mGoogleApiClient = new GoogleApiClient.Builder(NearMeDealsActivity.this)
                    .addApi(LocationServices.API)
                    .addConnectionCallbacks(NearMeDealsActivity.this)
                    .addOnConnectionFailedListener(this).build();
            mGoogleApiClient.connect();
        }

        LocationSettingsRequest.Builder builder = new LocationSettingsRequest.Builder().addLocationRequest(mLocationRequest);
        //**************
        builder.setAlwaysShow(true); //this is the key ingredient
        //**************************
        PendingResult<LocationSettingsResult> result =
                LocationServices.SettingsApi.checkLocationSettings(mGoogleApiClient, builder.build());
        result.setResultCallback(new ResultCallback<LocationSettingsResult>() {
            @Override
            public void onResult(LocationSettingsResult result) {
                final Status status = result.getStatus();
                final LocationSettingsStates state = result.getLocationSettingsStates();
                AppDelegate.LogGP("status.getStatusCode() => " + status.getStatusCode());
                switch (status.getStatusCode()) {
                    case LocationSettingsStatusCodes.SUCCESS:
                        // All location settings are satisfied. The client can initialize location
                        // requests here.
                        AppDelegate.LogGP("GPS SUCCESS ");

                        Location location = LocationServices.FusedLocationApi.getLastLocation(mGoogleApiClient);
                        AppDelegate.LogT("onConnected Initialited");
                        if (location == null) {
                            try {
                                AppDelegate.LogT("onConnected Initialited== null");
                                LocationServices.FusedLocationApi.requestLocationUpdates(mGoogleApiClient, mLocationRequest, NearMeDealsActivity.this);
                            } catch (Exception e) {
                                AppDelegate.LogE(e);
                            }
                        } else {
                            currentLatitude = location.getLatitude();
                            currentLongitude = location.getLongitude();
                            setloc(currentLatitude, currentLongitude);
                            AppDelegate.LogT("latLng = " + currentLatitude + ", " + currentLongitude);
                        }

                        break;
                    case LocationSettingsStatusCodes.RESOLUTION_REQUIRED:
                        // Location settings are not satisfied. But could be fixed by showing the user
                        // a dialog.
                        AppDelegate.LogGP("GPS RESOLUTION_REQUIRED ");
                        try {
                            // Show the dialog by calling startResolutionForResult(),
                            // and check the result in onActivityResult().
                            status.startResolutionForResult(NearMeDealsActivity.this, 500);
                        } catch (IntentSender.SendIntentException e) {
                            // Ignore the error.
                        }
                        break;
                    case LocationSettingsStatusCodes.SETTINGS_CHANGE_UNAVAILABLE:
                        // Location settings are not satisfied. However, we have no way to fix the
                        // settings so we won't show the dialog.
                        AppDelegate.LogGP("GPS SETTINGS_CHANGE_UNAVAILABLE ");
                        break;

                    case CommonStatusCodes.CANCELED:
                        AppDelegate.LogGP("GPS CANCELED ");
                        break;
                }
            }
        });
    }

    public int selected_item = 1;
    public String selectedSortByValue = "near_me";
    public ImageView img_popular, img_near_me, img_price_low, img_price_high, img_freshness;
    public android.widget.TextView txt_popular, txt_near_me, txt_price_low, txt_price_high, txt_freshness;

    private void showSortByDialog() {
        final Dialog bannerDialog = new Dialog(this, android.R.style.Theme_Light);
        bannerDialog.requestWindowFeature(Window.FEATURE_NO_TITLE);
        bannerDialog.getWindow().setBackgroundDrawable(new ColorDrawable(getResources().getColor(android.R.color.transparent)));
        bannerDialog.setContentView(R.layout.dialog_sort_by);
        img_popular = (ImageView) bannerDialog.findViewById(R.id.img_popular);
        img_near_me = (ImageView) bannerDialog.findViewById(R.id.img_near_me);
        img_price_low = (ImageView) bannerDialog.findViewById(R.id.img_price_low);
        img_price_high = (ImageView) bannerDialog.findViewById(R.id.img_price_high);
        img_freshness = (ImageView) bannerDialog.findViewById(R.id.img_freshness);
        txt_popular = (android.widget.TextView) bannerDialog.findViewById(R.id.txt_popular);
        txt_near_me = (android.widget.TextView) bannerDialog.findViewById(R.id.txt_near_me);
        txt_price_low = (android.widget.TextView) bannerDialog.findViewById(R.id.txt_price_low);
        txt_price_high = (android.widget.TextView) bannerDialog.findViewById(R.id.txt_price_high);
        txt_freshness = (android.widget.TextView) bannerDialog.findViewById(R.id.txt_freshness);

        bannerDialog.findViewById(R.id.ll_c_popular).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                selected_item = 0;
                switchSortBy(0);
                execute_searchDeals(category_id);
                bannerDialog.dismiss();
            }
        });
        bannerDialog.findViewById(R.id.ll_c_near_me).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
//                selected_item = 1;
//                isClicked = true;
//                showGPSalert();
                selected_item = 1;
                switchSortBy(1);
                execute_searchDeals(category_id);

                //  execute_searchDeals(category_id);
                bannerDialog.dismiss();
            }
        });
        bannerDialog.findViewById(R.id.ll_c_price_low).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                selected_item = 2;
                switchSortBy(2);
                execute_searchDeals(category_id);
                bannerDialog.dismiss();
            }
        });
        bannerDialog.findViewById(R.id.ll_c_price_high).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                selected_item = 3;
                switchSortBy(3);
                execute_searchDeals(category_id);
                bannerDialog.dismiss();
            }
        });
        bannerDialog.findViewById(R.id.ll_c_freshness).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                selected_item = 4;
                switchSortBy(4);
                execute_searchDeals(category_id);
                bannerDialog.dismiss();
            }
        });
        switchSortBy(selected_item);
        bannerDialog.show();
    }

    private void execute_searchDeals(String cat_id) {
        try {
            apiShouldcall = false;

            if (AppDelegate.haveNetworkConnection(this, true)) {
                ArrayList<PostAysnc_Model> mPostArrayList = new ArrayList<>();
                AppDelegate.getInstance(this).setPostParamsSecond(mPostArrayList, Tags.API_KEY, Tags.API_KEY_VALUE);

                AppDelegate.getInstance(this).setPostParamsSecond(mPostArrayList, Tags.dealStatus, deal_type == DEAL_ADHOC ? DEAL_ADHOC : DEAL_HERO);

                AppDelegate.getInstance(this).setPostParamsSecond(mPostArrayList, Tags.postalCode, AppDelegate.isValidString(str_postal_code) ? str_postal_code : "");
//                if (AppDelegate.isValidString(str_lat) && AppDelegate.isValidString(str_long)) {
//                    AppDelegate.getInstance(this).setPostParamsSecond(mPostArrayList, Tags.LAT, str_lat);
//                    AppDelegate.getInstance(this).setPostParamsSecond(mPostArrayList, Tags.LONG, str_long);
//                } else {
                AppDelegate.getInstance(this).setPostParamsSecond(mPostArrayList, Tags.LAT, currentLatitude);
                AppDelegate.getInstance(this).setPostParamsSecond(mPostArrayList, Tags.LONG, currentLongitude);
//                }
                AppDelegate.getInstance(this).setPostParamsSecond(mPostArrayList, Tags.keyword, AppDelegate.isValidString(str_keyword) ? str_keyword : "");

                AppDelegate.getInstance(this).setPostParamsSecond(mPostArrayList, Tags.categoryId, AppDelegate.isValidString(cat_id) ? cat_id : "");
                AppDelegate.getInstance(this).setPostParamsSecond(mPostArrayList, Tags.sortby, selectedSortByValue);

                if (new Prefs(NearMeDealsActivity.this).getUserdata() != null) {
                    AppDelegate.getInstance(this).setPostParamsSecond(mPostArrayList, Tags.user_id, new Prefs(NearMeDealsActivity.this).getUserdata().id);
                }

                PostAsync mPostAsyncObj = new PostAsync(this, this, ServerRequestConstants.SEARCH, mPostArrayList, null);
                mHandler.sendEmptyMessage(20);
                mPostAsyncObj.execute();
            }
        } catch (Exception e) {
            AppDelegate.showToast(this, getString(R.string.try_again));
            AppDelegate.LogE(e);
        }
    }

    /* show Category dialog */
    private RecyclerView recycler_deals;
    private CategoryListItemAdapter mAdapter;

    private boolean[] is_checked;

    private void showCustomCategoryDialog() {
        AppDelegate.LogT("categoryModelArrayList size => " + categoryModelArrayList.size());
        if (categoryModelArrayList != null) {
            if (is_checked == null || is_checked.length != categoryModelArrayList.size()) {
                is_checked = new boolean[categoryModelArrayList.size()];
                AppDelegate.LogT("showCustomCategoryDialog => category_id = " + category_id);
                if (AppDelegate.isValidString(category_id)) {
                    if (category_id.contains(",")) {
                        String[] separated_category = category_id.split(",");
                        if (separated_category != null && separated_category.length > 0) {
                            for (int k = 0; k < separated_category.length; k++) {
                                for (int i = 0; i < categoryModelArrayList.size(); i++) {
                                    if (separated_category[k].equalsIgnoreCase(categoryModelArrayList.get(i).id + "")) {
                                        is_checked[i] = true;
                                        categoryModelArrayList.get(i).checked = 1;
                                    }
                                }
                            }
                        }
                    } else {
                        for (int i = 0; i < categoryModelArrayList.size(); i++) {
                            if (category_id.equalsIgnoreCase(categoryModelArrayList.get(i).id + "")) {
                                is_checked[i] = true;
                            } else
                                is_checked[i] = false;
                        }
                    }
                } else {
                    StringBuilder stringBuilder = null;
                    for (int i = 0; i < categoryModelArrayList.size(); i++) {
                        is_checked[i] = true;
                        categoryModelArrayList.get(i).checked = 1;
                        if (stringBuilder == null) {
                            stringBuilder = new StringBuilder();
                            stringBuilder.append("" + categoryModelArrayList.get(i).id);
                        } else {
                            stringBuilder.append("," + categoryModelArrayList.get(i).id);
                        }
                    }
                    if (stringBuilder != null) {
                        category_id = stringBuilder.toString();
                    }
                }
            } else {
                for (int i = 0; i < is_checked.length; i++) {
                    categoryModelArrayList.get(i).checked = is_checked[i] ? 1 : 0;
                }
            }


            final Dialog categoryDialog = new Dialog(this, android.R.style.Theme_Light);
            categoryDialog.requestWindowFeature(Window.FEATURE_NO_TITLE);
            categoryDialog.getWindow().setBackgroundDrawable(new ColorDrawable(getResources().getColor(android.R.color.transparent)));
            categoryDialog.setContentView(R.layout.dialog_filter);
            recycler_deals = (RecyclerView) categoryDialog.findViewById(R.id.recycler_deals);
            recycler_deals.setNestedScrollingEnabled(false);
            recycler_deals.setLayoutManager(new LinearLayoutManager(this));
            recycler_deals.setItemAnimator(new DefaultItemAnimator());

            mAdapter = new CategoryListItemAdapter(this, categoryModelArrayList, new OnListItemClickListener() {
                @Override
                public void setOnListItemClickListener(String name, int position) {
                }

                @Override
                public void setOnListItemClickListener(String name, int position, boolean like_dislike) {
                    AppDelegate.LogT("interface value => " + like_dislike);
                    categoryModelArrayList.get(position).checked = like_dislike ? 0 : 1;
                    mAdapter.notifyDataSetChanged();
                    recycler_deals.invalidate();
                }
            });
            recycler_deals.setAdapter(mAdapter);

            categoryDialog.findViewById(R.id.txt_c_submit).setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    StringBuilder stringBuilder = null;
                    for (int i = 0; i < categoryModelArrayList.size(); i++) {
                        if (categoryModelArrayList.get(i).checked == 1) {
                            is_checked[i] = true;
                            if (stringBuilder == null) {
                                stringBuilder = new StringBuilder();
                                stringBuilder.append("" + categoryModelArrayList.get(i).id);
                            } else {
                                stringBuilder.append("," + categoryModelArrayList.get(i).id);
                            }
                        } else {
                            is_checked[i] = false;
                        }
                    }
                    if (stringBuilder != null) {
                        category_id = stringBuilder.toString();
                        execute_searchDeals(category_id);
                        categoryDialog.dismiss();
                    } else {
                        AppDelegate.showToast(NearMeDealsActivity.this, getString(R.string.please_select_category));
                    }
                }
            });
            categoryDialog.findViewById(R.id.txt_c_cancel).setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    categoryDialog.dismiss();
                }
            });
            categoryDialog.show();
        }
    }

    public void switchSortBy(int value) {
        img_popular.setImageResource(R.drawable.popular);
        img_near_me.setImageResource(R.drawable.nearme);
        img_price_low.setImageResource(R.drawable.lowtohigh);
        img_price_high.setImageResource(R.drawable.highttolow);
        img_freshness.setImageResource(R.drawable.freshness);
        txt_popular.setTextColor(Color.BLACK);
        txt_near_me.setTextColor(Color.BLACK);
        txt_price_low.setTextColor(Color.BLACK);
        txt_price_high.setTextColor(Color.BLACK);
        txt_freshness.setTextColor(Color.BLACK);
        switch (value) {
            case 0:
                selectedSortByValue = "Popular";
                txt_popular.setTextColor(getResources().getColor(R.color.ripple_color));
                img_popular.setImageResource(R.drawable.popular_checked);
                break;
            case 1:
                selectedSortByValue = "near_me";
                txt_near_me.setTextColor(getResources().getColor(R.color.ripple_color));
                img_near_me.setImageResource(R.drawable.nearme_checked);

                break;
            case 2:
                selectedSortByValue = "Low to High";
                txt_price_low.setTextColor(getResources().getColor(R.color.ripple_color));
                img_price_low.setImageResource(R.drawable.lowtohigh_checked);
                break;
            case 3:
                selectedSortByValue = "High to Low";
                txt_price_high.setTextColor(getResources().getColor(R.color.ripple_color));
                img_price_high.setImageResource(R.drawable.highttolow_checked);
                break;
            case 4:
                selectedSortByValue = "freshness";
                txt_freshness.setTextColor(getResources().getColor(R.color.ripple_color));
                img_freshness.setImageResource(R.drawable.freshness_checked);
                break;
        }
    }


    @Override
    public void setOnReciveResult(String apiName, String result) {
        if (mHandler == null)
            return;
        if (!AppDelegate.isValidString(result)) {
            apiExecuting = true;
            clearListMap();
            AppDelegate.showToast(NearMeDealsActivity.this, getResources().getString(R.string.time_out));
            recycler_deals_list.setAdapter(null);
            mHandler.sendEmptyMessage(11);
            return;
        }
        if (apiName.equals(ServerRequestConstants.SEARCH)) {
            recycler_deals_list.setAdapter(null);
            mHandler.sendEmptyMessage(21);
            if (googleMap != null)
                googleMap.clear();
            if (deal_type == DEAL_ADHOC) {
                parseAdHocDeals(result);
            } else {
                parseHeroDeals(result);
            }
        } else if (apiName.equalsIgnoreCase(ServerRequestConstants.GET_ADHOC_DEALS)) {
            recycler_deals_list.setAdapter(null);
            if (googleMap != null)
                googleMap.clear();
            mHandler.sendEmptyMessage(11);
            parseAdHocDeals(result);
        } else if (apiName.equalsIgnoreCase(ServerRequestConstants.ADHOC_LIKE)) {
            mHandler.sendEmptyMessage(11);
            parseAdHocLikes(result);
        }
    }

    private void clearListMap() {
        if (googleMap != null) {
            googleMap.clear();
        }
        if (adHocDealModelArrayList != null && adHocDealModelArrayList.size() > 0) {
            adHocDealModelArrayList.clear();
            show_AdHocDealsList(adHocDealModelArrayList);
        }
        if (heroDealsModelArrayList != null && heroDealsModelArrayList.size() > 0) {
            heroDealsModelArrayList.clear();
            show_DealsList(heroDealsModelArrayList);
        }
    }

    private void parseAdHocLikes(String result) {
    }

    private void parseAdHocDeals(String result) {
        try {
            JSONObject jsonobj = new JSONObject(result);
            adHocDealModelArrayList.clear();
            if (jsonobj.getInt(Tags.status) == 1 && jsonobj.getInt(Tags.dataFlow) == 1) {
                CategoryModel category = new CategoryModel();
                JSONArray array = jsonobj.getJSONArray(Tags.response);
                for (int i = 0; i < array.length(); i++) {
                    adHocDealModel = new AdHocDealsModel();
                    JSONObject heroobj = array.getJSONObject(i);
                    adHocDealModel.distance = heroobj.getString(Tags.distance);
                    adHocDealModel.id = heroobj.getInt(Tags.id);
                    adHocDealModel.user_id = heroobj.getInt(Tags.user_id);
                    adHocDealModel.name = heroobj.getString(Tags.name);
                    adHocDealModel.use_the_offer = heroobj.getString(Tags.use_the_offer);
                    adHocDealModel.things_to_remebers = heroobj.getString(Tags.things_to_remebers);
                    adHocDealModel.deal_category_id = heroobj.getInt(Tags.deal_category_id);
                    adHocDealModel.sub_category_id = heroobj.getInt(Tags.sub_category_id);
                    adHocDealModel.facilities = heroobj.getString(Tags.facilities);
                    adHocDealModel.item_detail = heroobj.getString(Tags.item_detail);
                    adHocDealModel.deal_type = heroobj.getInt(Tags.deal_type);
                    adHocDealModel.discount_cost = heroobj.getInt(Tags.discount_cost);
                    adHocDealModel.starting_date = heroobj.getString(Tags.starting_date);
                    adHocDealModel.expiry_date = heroobj.getString(Tags.expiry_date);
                    adHocDealModel.price = heroobj.getInt(Tags.price);
                    adHocDealModel.total_deal = heroobj.getInt(Tags.total_deal);
                    adHocDealModel.volume_of_deal = heroobj.getInt(Tags.volume_of_deal);
                    adHocDealModel.timing_to = heroobj.getString(Tags.timing_to);
                    adHocDealModel.timing_from = heroobj.getString(Tags.timing_from);
                    adHocDealModel.valid_on = heroobj.getString(Tags.valid_on);
                    adHocDealModel.vendor_delete_deal = heroobj.getInt(Tags.vendor_delete_deal);
                    adHocDealModel.favourite = heroobj.getInt(Tags.favourite);
                    if (heroobj.has(Tags.deal_category))
                        adHocDealModel.markerImage = heroobj.getJSONObject(Tags.deal_category).getString(Tags.marker_icon);
                    AppDelegate.LogT("liked => " + heroobj.optString(Tags.liked));
                    if (AppDelegate.isValidString(heroobj.optString(Tags.liked)))
                        adHocDealModel.liked = heroobj.getInt(Tags.liked);

                    if (AppDelegate.isValidString(heroobj.getString(Tags.user))) {
                        JSONObject vendor = heroobj.getJSONObject(Tags.user);
                        adHocDealModel.vendor_name = vendor.getString(Tags.name);
                        if (vendor.has(Tags.busines) && AppDelegate.isValidString(vendor.getString(Tags.busines))) {
                            //  JSONObject business = vendor.getJSONObject(Tags.busines);
                            adHocDealModel.latitude = vendor.getJSONObject(Tags.busines).getString(Tags.latitude);
                            adHocDealModel.longitude = vendor.getJSONObject(Tags.busines).getString(Tags.longitude);
                            adHocDealModel.company_address = vendor.getJSONObject(Tags.busines).getString(Tags.location_address);
                            adHocDealModel.cityName = vendor.getJSONObject(Tags.busines).getString(Tags.cityName);
                        }
                    }
                    try {
                        if (AppDelegate.isValidString(heroobj.getString(Tags.images))) {
                            JSONArray img = heroobj.getJSONArray(Tags.images);
                            for (int k = 0; k < img.length(); k++) {
                                JSONObject json = img.getJSONObject(k);
                                adHocDealModel.image = json.getString(Tags.image);
//                            adHocDealModel.adhocdeal_id = json.getInt(Tags.adhocdeal_id);
                            }
                        }
                    } catch (JSONException e) {
                        AppDelegate.LogE(e);
                    }
                    if (heroobj.has(Tags.image))
                        adHocDealModel.image = heroobj.getString(Tags.image);
                    JSONObject obj = heroobj.getJSONObject(Tags.deal_category);
                    category = new CategoryModel();
                    category.id = obj.getInt(Tags.id);
                    category.title = obj.getString(Tags.title);
                    category.description = obj.getString(Tags.description);
                    category.image = obj.getString(Tags.image);
                    adHocDealModel.category = category.title;
                    adHocDealModelArrayList.add(adHocDealModel);
                }
                AppDelegate.LogT("adHocDealModelArrayList==" + adHocDealModelArrayList);
//                startKMTimer();
                apiExecuting = false;
                show_AdHocDealsList(adHocDealModelArrayList);
                new DownloadImages().execute();
            } else {
                googleMap.clear();
                apiExecuting = true;
                AppDelegate.showToast(this, jsonobj.getString(Tags.message));
            }
        } catch (JSONException e) {
            googleMap.clear();
            AppDelegate.showToast(this, "Please try again later.");
            apiExecuting = true;
            AppDelegate.LogE(e);
        }
    }

    private void show_AdHocDealsList(ArrayList<AdHocDealsModel> adHocDealModelArrayList) {
        AdHocDealsAdapter mAdapter = new AdHocDealsAdapter(this, adHocDealModelArrayList, this);
        RecyclerView.LayoutManager mLayoutManager = new LinearLayoutManager(this);
        recycler_deals_list.setLayoutManager(mLayoutManager);
        recycler_deals_list.setItemAnimator(new DefaultItemAnimator());
        recycler_deals_list.setAdapter(mAdapter);
    }

    private void showAdHocpinOnMap(ArrayList<AdHocDealsModel> adHocDealModelArrayList) {
        AppDelegate.LogT("pin on map==" + adHocDealModelArrayList);
        googleMap.clear();
//        try {
//            if (firstTimeZoom) {
//                googleMap.moveCamera(CameraUpdateFactory.newLatLngZoom(new LatLng(currentLatitude, currentLongitude), 10));
//                firstTimeZoom = false;
//            }
////          googleMap.animateCamera(CameraUpdateFactory.newLatLngZoom(new LatLng(Double.parseDouble(new Prefs(this).getCitydata().lat), Double.parseDouble(new Prefs(this).getCitydata().lng)), 10));
//        } catch (Exception e) {
//            AppDelegate.LogE(e);
//        }
        LatLngBounds.Builder builder = LatLngBounds.builder();
        for (int i = 0; i < adHocDealModelArrayList.size(); i++) {
            if (AppDelegate.isValidString(adHocDealModelArrayList.get(i).latitude) && AppDelegate.isValidString(adHocDealModelArrayList.get(i).longitude)) {
                double lat = Double.parseDouble(adHocDealModelArrayList.get(i).latitude);
                double lon = Double.parseDouble(adHocDealModelArrayList.get(i).longitude);
                LatLng latlong = new LatLng(lat, lon);
                builder.include(latlong);
                imageLoader.loadImage(adHocDealModelArrayList.get(i).image, options, new ImageLoadingListener() {
                    @Override
                    public void onLoadingStarted(String imageUri, View view) {
                        userImg = getResources().getDrawable(android.R.color.transparent);
                    }

                    @Override
                    public void onLoadingFailed(String imageUri, View view, FailReason failReason) {
                        userImg = getResources().getDrawable(android.R.color.transparent);
                    }

                    @Override
                    public void onLoadingComplete(String imageUri, View view, Bitmap loadedImage) {
                        userImg = new BitmapDrawable(getResources(), loadedImage);
                    }

                    @Override
                    public void onLoadingCancelled(String imageUri, View view) {
                        userImg = getResources().getDrawable(android.R.color.transparent);
                    }
                });
                createMarker(new Person(latlong, adHocDealModelArrayList.get(i).id, adHocDealModelArrayList.get(i).name, null, adHocDealModelArrayList.get(i), DEAL_ADHOC), i);
//                mClusterManager.addItem(new Person(latlong, adHocDealModelArrayList.get(i).id, adHocDealModelArrayList.get(i).name, userImg, null, adHocDealModelArrayList.get(i), DEAL_ADHOC));
            } else {
                AppDelegate.LogE("lat long not valid for deal => " + adHocDealModelArrayList.get(i).title);
            }
        }
//        try {
//            LatLngBounds bounds = builder.build();
//            int width = getResources().getDisplayMetrics().widthPixels - AppDelegate.dpToPix(this, 100);
//            int height = getResources().getDisplayMetrics().heightPixels - AppDelegate.dpToPix(this, 100);
//            int padding = (int) (width * 0.35);
////            googleMap.moveCamera(CameraUpdateFactory.newLatLngBounds(bounds, 50));
//            AppDelegate.LogT("bounds => " + bounds);
//            googleMap.moveCamera(CameraUpdateFactory.newLatLngBounds(bounds, width, height, padding));
//        } catch (Exception e) {
//            AppDelegate.LogE(e);
//            if (builder != null) {
//                try {
//                    CameraUpdate location = CameraUpdateFactory.newLatLngZoom(builder.build().getCenter(), 10);
//                    googleMap.moveCamera(location);
//                } catch (Exception t) {
//                    AppDelegate.LogE(t);
//                }
//            }
//        }
    }

    private void parseHeroDeals(String result) {
        try {
            JSONObject jsonobj = new JSONObject(result);
            heroDealsModelArrayList.clear();
            if (jsonobj.getInt(Tags.status) == 1 && jsonobj.getInt(Tags.dataFlow) == 1) {
                CategoryModel category = new CategoryModel();

                JSONArray array = jsonobj.getJSONArray(Tags.response);
                for (int i = 0; i < array.length(); i++) {
                    heroDealsModel = new HeroDealsModel();
                    JSONObject heroobj = array.getJSONObject(i);
                    heroDealsModel.distance = heroobj.getString(Tags.distance);
                    heroDealsModel.id = heroobj.getInt(Tags.id);
                    heroDealsModel.user_id = heroobj.getInt(Tags.user_id);
                    heroDealsModel.name = heroobj.getString(Tags.name);
                    heroDealsModel.herodeal_title = heroobj.getString(Tags.name);
                    if (heroobj.has(Tags.deal_category))
                        heroDealsModel.category = heroobj.getJSONObject(Tags.deal_category).getString(Tags.title);
                    heroDealsModel.deal_category_id = heroobj.getInt(Tags.deal_category_id);
                    heroDealsModel.image = heroobj.getString(Tags.image);
                    heroDealsModel.sub_category_id = heroobj.getInt(Tags.sub_category_id);
                    heroDealsModel.description = heroobj.getString(Tags.description);
                    heroDealsModel.value_upto = heroobj.getInt(Tags.value_upto);
                    heroDealsModel.discount_offer = heroobj.getString(Tags.discount_offer);
                    heroDealsModel.starting_date = heroobj.getString(Tags.starting_date);
                    heroDealsModel.expiry_date = heroobj.getString(Tags.expiry_date);
                    heroDealsModel.trems_and_conditions = heroobj.getString(Tags.trems_and_conditions);
                    heroDealsModel.total_deal = heroobj.getInt(Tags.total_deal);
                    if (heroobj.has(Tags.deal_category))
                        heroDealsModel.markerImage = heroobj.getJSONObject(Tags.deal_category).getString(Tags.marker_icon);

                    if (AppDelegate.isValidString(heroobj.getString(Tags.user))) {
                        JSONObject vendor = heroobj.getJSONObject(Tags.user);
                        heroDealsModel.vendor_name = vendor.getString(Tags.name);
                        /*JSONObject business = vendor.getJSONObject(Tags.busines);
                        heroDealsModel.latitude = business.getString(Tags.latitude);
                        heroDealsModel.longitude = business.getString(Tags.longitude);*/
                        if (vendor.has(Tags.busines)) {
                            if (AppDelegate.isValidString(vendor.getString(Tags.busines))) {
                                heroDealsModel.latitude = vendor.getJSONObject(Tags.busines).getString(Tags.latitude);
                                heroDealsModel.longitude = vendor.getJSONObject(Tags.busines).getString(Tags.longitude);
                                heroDealsModel.company_address = vendor.getJSONObject(Tags.busines).getString(Tags.location_address);
                                heroDealsModel.cityName = vendor.getJSONObject(Tags.busines).getString(Tags.cityName);
                            }
                        }
                    }
                    JSONObject obj = heroobj.getJSONObject(Tags.deal_category);
                    category = new CategoryModel();
                    category.id = obj.getInt(Tags.id);
                    category.title = obj.getString(Tags.title);
                    category.description = obj.getString(Tags.description);
                    category.image = obj.getString(Tags.image);
                    heroDealsModel.category = category.title;
                    heroDealsModelArrayList.add(heroDealsModel);
                }
                AppDelegate.LogT("heroDealsModelArrayList==" + heroDealsModelArrayList);
                show_DealsList(heroDealsModelArrayList);
//                startKMTimer();
                apiExecuting = false;
                new DownloadImages().execute();
            } else {
                googleMap.clear();
                apiExecuting = true;
                AppDelegate.showToast(this, jsonobj.getString(Tags.message));
            }
        } catch (JSONException e) {
            AppDelegate.showAlert(NearMeDealsActivity.this, getString(R.string.try_again));
            googleMap.clear();
            apiExecuting = true;
            AppDelegate.LogE(e);
        }
    }

    private void showpinOnMap(ArrayList<HeroDealsModel> heroDealsModelArrayList) {
        AppDelegate.LogT("pin on map==" + heroDealsModelArrayList);
        if (googleMap == null)
            return;
        googleMap.clear();
//        try {
//            googleMap.moveCamera(CameraUpdateFactory.newLatLngZoom(new LatLng(currentLatitude, currentLongitude), 10));
////            googleMap.animateCamera(CameraUpdateFactory.newLatLngZoom(new LatLng(Double.parseDouble(new Prefs(this).getCitydata().lat), Double.parseDouble(new Prefs(this).getCitydata().lng)), 10));
//        } catch (Exception e) {
//            AppDelegate.LogE(e);
//        }
        LatLngBounds.Builder builder = LatLngBounds.builder();
        for (int i = 0; i < heroDealsModelArrayList.size(); i++) {
            if (AppDelegate.isValidString(heroDealsModelArrayList.get(i).latitude) && AppDelegate.isValidString(heroDealsModelArrayList.get(i).longitude)) {
                double lat = Double.parseDouble(heroDealsModelArrayList.get(i).latitude);
                double lon = Double.parseDouble(heroDealsModelArrayList.get(i).longitude);
                LatLng latlong = new LatLng(lat, lon);
                builder.include(latlong);
                imageLoader.loadImage(heroDealsModelArrayList.get(i).image, options, new ImageLoadingListener() {
                    @Override
                    public void onLoadingStarted(String imageUri, View view) {
                        userImg = getResources().getDrawable(android.R.color.transparent);
                    }

                    @Override
                    public void onLoadingFailed(String imageUri, View view, FailReason failReason) {
                        userImg = getResources().getDrawable(android.R.color.transparent);
                    }

                    @Override
                    public void onLoadingComplete(String imageUri, View view, Bitmap loadedImage) {
                        userImg = new BitmapDrawable(getResources(), loadedImage);
                    }

                    @Override
                    public void onLoadingCancelled(String imageUri, View view) {
                        userImg = getResources().getDrawable(android.R.color.transparent);
                    }
                });
                createMarker(new Person(latlong, heroDealsModelArrayList.get(i).id, heroDealsModelArrayList.get(i).name, heroDealsModelArrayList.get(i), null, DEAL_HERO), i);
            } else {
                AppDelegate.LogE("lat long not valid for deal => " + heroDealsModelArrayList.get(i).herodeal_title);
            }
        }

//        try {
//            LatLngBounds bounds = builder.build();
//            int width = getResources().getDisplayMetrics().widthPixels - AppDelegate.dpToPix(this, 100);
//            int height = getResources().getDisplayMetrics().heightPixels - AppDelegate.dpToPix(this, 100);
//            int padding = (int) (width * 0.35);
////            googleMap.moveCamera(CameraUpdateFactory.newLatLngBounds(bounds, 50));
//            AppDelegate.LogT("bounds => " + bounds);
////            googleMap.moveCamera(CameraUpdateFactory.newLatLngBounds(bounds, width, height, padding));
//        } catch (Exception e) {
//            AppDelegate.LogE(e);
//            if (builder != null) {
//                try {
//                    CameraUpdate location = CameraUpdateFactory.newLatLngZoom(builder.build().getCenter(), 10);
////                    googleMap.moveCamera(location);
//                } catch (Exception t) {
//                    AppDelegate.LogE(t);
//                }
//            }
//        }
    }

    @Override
    public void setOnListItemClickListener(String name, int position) {
        if (name.equalsIgnoreCase(Tags.LOGIN)) {
            showLoginAlert(this, "", getString(R.string.login_first));
        }
    }

    @Override
    public void setOnListItemClickListener(String name, int position, boolean like_dislike) {
        if (name.equalsIgnoreCase(Tags.LIKE_DISLIKE_CLICKED)) {
            if (adHocDealModelArrayList != null) {
                if (like_dislike) {
                    execute_favouriteApi(AppDelegate.LIKE, adHocDealModelArrayList.get(position).id, adHocDealModelArrayList.get(position).user_id);
                } else {
                    execute_favouriteApi(AppDelegate.DISLIKE, adHocDealModelArrayList.get(position).id, adHocDealModelArrayList.get(position).user_id);
                }
            }
        }
    }

    private void execute_favouriteApi(int like, int id, int user_id) {
        try {
            if (AppDelegate.haveNetworkConnection(this, true)) {
                ArrayList<PostAysnc_Model> mPostArrayList = new ArrayList<PostAysnc_Model>();
                AppDelegate.getInstance(this).setPostParamsSecond(mPostArrayList, Tags.API_KEY, Tags.API_KEY_VALUE);
                AppDelegate.getInstance(this).setPostParamsSecond(mPostArrayList, Tags.deal_id, id);
                AppDelegate.getInstance(this).setPostParamsSecond(mPostArrayList, Tags.user_id, user_id);
                AppDelegate.getInstance(this).setPostParamsSecond(mPostArrayList, Tags.status, like, ServerRequestConstants.Key_PostintValue);
                PostAsync mPostAsyncObj;
                mPostAsyncObj = new PostAsync(this, this, ServerRequestConstants.ADHOC_LIKE,
                        mPostArrayList, null);
                mHandler.sendEmptyMessage(10);
                mPostAsyncObj.execute();
            }
        } catch (Exception e) {
            AppDelegate.showToast(this, getString(R.string.try_again));
            AppDelegate.LogE(e);
        }
    }

    public class DownloadImages extends AsyncTask<Void, Void, Void> {

        @Override
        protected void onPreExecute() {
            super.onPreExecute();
            if (mHandler == null)
                return;
            mHandler.sendEmptyMessage(20);
        }

        @Override
        protected Void doInBackground(Void... params) {
            Bitmap remote_picture = null;
            try {
                arrayBitmap.clear();
                if (deal_type == DEAL_ADHOC) {
                    for (int i = 0; i < adHocDealModelArrayList.size(); i++) {
                        boolean isDownloadedBefore = false;
                        for (int j = 0; j < AppDelegate.arrayBitmap.size(); j++) {
                            if (AppDelegate.arrayBitmap.get(j).category_id.equalsIgnoreCase(adHocDealModelArrayList.get(i).deal_category_id + "")) {
                                isDownloadedBefore = true;
                                arrayBitmap.add(AppDelegate.arrayBitmap.get(j).bitmap);
                            }
                        }
                        if (!isDownloadedBefore) {
                            remote_picture = BitmapFactory.decodeStream((InputStream) new URL(adHocDealModelArrayList.get(i).markerImage).getContent());
                            arrayBitmap.add(remote_picture);
                        }
                    }
                } else {
                    for (int i = 0; i < heroDealsModelArrayList.size(); i++) {
                        boolean isDownloadedBefore = false;
                        for (int j = 0; j < AppDelegate.arrayBitmap.size(); j++) {
                            if (AppDelegate.arrayBitmap.get(j).category_id.equalsIgnoreCase(heroDealsModelArrayList.get(i).deal_category_id + "")) {
                                isDownloadedBefore = true;
                                arrayBitmap.add(AppDelegate.arrayBitmap.get(j).bitmap);
                            }
                        }
                        if (!isDownloadedBefore) {
                            remote_picture = BitmapFactory.decodeStream((InputStream) new URL(heroDealsModelArrayList.get(i).markerImage).getContent());
                            arrayBitmap.add(remote_picture);
                        }
                    }
                }
            } catch (IOException e) {
                AppDelegate.LogE(e);
            }
            return null;
        }

        @Override
        protected void onPostExecute(Void aVoid) {
            super.onPostExecute(aVoid);
            if (mHandler == null)
                return;
            mHandler.sendEmptyMessage(21);
            if (deal_type == DEAL_ADHOC)
                showAdHocpinOnMap(adHocDealModelArrayList);
            else
                showpinOnMap(heroDealsModelArrayList);
            new Handler().postDelayed(new Runnable() {
                @Override
                public void run() {
                    apiExecuting = true;
                }
            }, 800);
        }
    }

    public void getValues() {
        category_id = getIntent().getStringExtra(Tags.category_id);
        AppDelegate.LogT("category_id==" + category_id);
        deal_type = getIntent().getIntExtra(Tags.deal_type, DEAL_HERO);
        str_postal_code = AppDelegate.isValidString(getIntent().getStringExtra(Tags.postal_code)) ? getIntent().getStringExtra(Tags.postal_code) : "";
        str_lat = AppDelegate.isValidString(getIntent().getStringExtra(Tags.LAT)) ? getIntent().getStringExtra(Tags.LAT) : (new Prefs(this).getCitydata() != null ? new Prefs(this).getCitydata().lat : "");
        currentLatitude = Double.parseDouble(str_lat);
        str_long = AppDelegate.isValidString(getIntent().getStringExtra(Tags.LONG)) ? getIntent().getStringExtra(Tags.LONG) : (new Prefs(this).getCitydata() != null ? new Prefs(this).getCitydata().lng : "");
        currentLongitude = Double.parseDouble(str_long);
        str_keyword = AppDelegate.isValidString(getIntent().getStringExtra(Tags.keyword)) ? getIntent().getStringExtra(Tags.keyword) : "";
        if (deal_type == DEAL_HERO) {
            heroDealsModelArrayList = getIntent().getParcelableArrayListExtra(Tags.deal);
            txt_c_viewdeals.setText(getString(R.string.click_to_view_adhoc_deals));
        } else {
            adHocDealModelArrayList = getIntent().getParcelableArrayListExtra(Tags.deal);
            txt_c_viewdeals.setText(getString(R.string.click_to_view_hero_deals));
        }
        categoryModelArrayList = getIntent().getParcelableArrayListExtra(Tags.category);
    }


    @Override
    public void onConnected(@Nullable Bundle bundle) {
        Location location = LocationServices.FusedLocationApi.getLastLocation(mGoogleApiClient);
        AppDelegate.LogT("onConnected Initialited");
        if (location == null) {
            try {
                AppDelegate.LogT("onConnected Initialited== null");
                LocationServices.FusedLocationApi.requestLocationUpdates(mGoogleApiClient, mLocationRequest, NearMeDealsActivity.this);
            } catch (Exception e) {
                AppDelegate.LogE(e);
            }
        } else {
            currentLatitude = location.getLatitude();
            currentLongitude = location.getLongitude();

            AppDelegate.LogT("latLng = " + currentLatitude + ", " + currentLongitude);
        }
    }

    @Override
    public void onConnectionSuspended(int i) {

    }

    @Override
    public void onConnectionFailed(@NonNull ConnectionResult connectionResult) {
        if (connectionResult.hasResolution()) {
            try {
                AppDelegate.LogT("onConnectionFailed Initialited" + connectionResult);
                connectionResult.startResolutionForResult(this, CONNECTION_FAILURE_RESOLUTION_REQUEST);

            } catch (IntentSender.SendIntentException e) {
                AppDelegate.LogE(e);
            }
        } else {
            Log.e("Error", "Location services connection failed with code " + connectionResult.getErrorCode());
        }
    }

    @Override
    protected void onResume() {
        super.onResume();
        AppDelegate.hideKeyBoard(this);
        mGoogleApiClient.connect();
        if (apiShouldcall) {
            AppDelegate.LogT("api should call ==" + apiShouldcall);
            AppDelegate.LogT("category_id==" + category_id);
            execute_searchDeals(category_id);
        }
    }

    @Override
    public void onLocationChanged(Location location) {
        if (location != null && location.getLatitude() != 0.0 && location.getLongitude() != 0.0) {
            currentLatitude = location.getLatitude();
            currentLongitude = location.getLongitude();

            AppDelegate.LogT("latLng onLocationChanged= " + currentLatitude + ", " + currentLongitude);
            try {
                if (isClicked) {
                    AppDelegate.LogT("latLng onLocationChanged= " + currentLatitude + ", " + currentLongitude);
                    setloc(currentLatitude, currentLongitude);
                    if (mGoogleApiClient.isConnected()) {
                        LocationServices.FusedLocationApi.removeLocationUpdates(mGoogleApiClient, NearMeDealsActivity.this);
                        mGoogleApiClient.disconnect();
                        AppDelegate.LogGP("Fused Location api disconnect called");
                    }
                }
            } catch (Exception e) {
                AppDelegate.LogE(e);
            }
        }
    }

    public void showLoginAlert(Context mContext, String Title, String
            Message) {
        try {
            alert = new AlertDialog.Builder(mContext);
            alert.setCancelable(false);
            alert.setMessage(Message);
            alert.setPositiveButton(
                    "LOGIN",
                    new DialogInterface.OnClickListener() {

                        @Override
                        public void onClick(DialogInterface dialog, int which) {
                            Intent intent = new Intent(NearMeDealsActivity.this, LoginActivity.class);
                            intent.putExtra(Tags.login_from, AppDelegate.LOGIN_FROM_VENDOR_PROFILE);
                            apiShouldcall = true;
                            startActivity(intent);
                            dialog.dismiss();
                        }
                    }).setNegativeButton(Tags.CANCEL, new DialogInterface.OnClickListener() {
                @Override
                public void onClick(DialogInterface dialog, int which) {
                    dialog.dismiss();
                }
            });
            alert.show();
        } catch (Exception e) {
            AppDelegate.LogE(e);
        }
    }

    protected Marker createMarker(Person markers, int position) {
        try {
            View marker = ((LayoutInflater) getSystemService(Context.LAYOUT_INFLATER_SERVICE)).inflate(R.layout.multi_profile, null);
            final ImageView cimg_user = (ImageView) marker.findViewById(R.id.imageView);
            final ImageView img_loading = (ImageView) marker.findViewById(R.id.img_loading);
            img_loading.setVisibility(View.VISIBLE);
            new Handler(Looper.getMainLooper()).post(new Runnable() {
                @Override
                public void run() {
                    AnimationDrawable frameAnimation = (AnimationDrawable) img_loading.getDrawable();
                    frameAnimation.setCallback(img_loading);
                    frameAnimation.setVisible(true, true);
                    frameAnimation.start();
                    ((Animatable) img_loading.getDrawable()).start();
                }
            });
            if (markers.type == DEAL_HERO) {
                LatLng latlong = new LatLng(Double.parseDouble(markers.heroDealsModel.latitude), Double.parseDouble(markers.heroDealsModel.longitude));
                if (AppDelegate.isValidString(markers.heroDealsModel.markerImage)) {
                    AppDelegate.LogT("markerImage=>" + AppDelegate.isValidString(markers.heroDealsModel.markerImage));
                    Bitmap remote_picture = null;
                    remote_picture = arrayBitmap.get(position);
                    cimg_user.setImageBitmap(remote_picture);
                    img_loading.setVisibility(View.GONE);

                }
                customMarker = googleMap.addMarker(new MarkerOptions()
                        .position(latlong)
                        .title(markers.heroDealsModel.name)
                        .snippet(markers.heroDealsModel.cityName)
                        .icon(BitmapDescriptorFactory.fromBitmap(AppDelegate.createDrawableFromView(this, marker))));
                markerPersonMap.put(customMarker.getId(), markers);

            } else {
                LatLng latlong = new LatLng(Double.parseDouble(markers.adHocDealsModel.latitude), Double.parseDouble(markers.adHocDealsModel.longitude));
                if (AppDelegate.isValidString(markers.adHocDealsModel.markerImage)) {
                    Bitmap remote_picture = null;
                    remote_picture = arrayBitmap.get(position);
                    cimg_user.setImageBitmap(remote_picture);
                    img_loading.setVisibility(View.GONE);
                }
                customMarker = googleMap.addMarker(new MarkerOptions()
                        .position(latlong)
                        .title(markers.adHocDealsModel.name)
                        .snippet(markers.adHocDealsModel.cityName)
                        .icon(BitmapDescriptorFactory.fromBitmap(AppDelegate.createDrawableFromView(this, marker))));
                markerPersonMap.put(customMarker.getId(), markers);


            }
            show_pin_dialog(markers);
            googleMap.setOnInfoWindowClickListener(new GoogleMap.OnInfoWindowClickListener() {
                @Override
                public void onInfoWindowClick(Marker marker) {
                    Person person = markerPersonMap.get(marker.getId());
                    if (person.type == DEAL_ADHOC) {
                        Intent intent = new Intent(NearMeDealsActivity.this, AdHocDealDetailActivity.class);
                        intent.putExtra(Tags.deal_id, person.adHocDealsModel.id);
                        Bundle bundle = new Bundle();
                        bundle.putParcelable(Tags.deal, person.adHocDealsModel);
                        intent.putExtras(bundle);
                        startActivity(intent);
                    } else {
                        Intent intent = new Intent(NearMeDealsActivity.this, VendorProfileActivity.class);
                        intent.putExtra(Tags.deal_id, person.heroDealsModel.id);
                        intent.putExtra(Tags.vendor_id, person.heroDealsModel.user_id);
                        Bundle bundle = new Bundle();
                        bundle.putParcelable(Tags.deal, person.heroDealsModel);
                        intent.putExtras(bundle);
                        startActivity(intent);
                    }
                }
            });
        } catch (Exception e) {
            e.printStackTrace();
        }
        return customMarker;
    }

    private void show_pin_dialog(Person person) {
        googleMap.setInfoWindowAdapter(new GoogleMap.InfoWindowAdapter() {
            @Override
            public View getInfoWindow(Marker marker) {
                apiExecuting = false;
                new Handler().postDelayed(new Runnable() {
                    @Override
                    public void run() {
                        apiExecuting = true;
                    }
                }, 500);
                Person person = markerPersonMap.get(marker.getId());
                View v = getLayoutInflater().inflate(R.layout.map_pin_showinfo_row, null);
                v.setLayoutParams(new RelativeLayout.LayoutParams(300, RelativeLayout.LayoutParams.WRAP_CONTENT));
                final carbon.widget.ImageView img_c_loading1;
                final CircleImageView cimg_user;
                TextView txt_c_vendor_name = (TextView) v.findViewById(R.id.txt_c_vendor_name);
                TextView txt_c_company_name = (TextView) v.findViewById(R.id.txt_c_company_name);
                TextView txt_c_country = (TextView) v.findViewById(R.id.txt_c_address);
                img_c_loading1 = (carbon.widget.ImageView) v.findViewById(R.id.img_c_loading1);
                cimg_user = (CircleImageView) v.findViewById(R.id.cimg_user);
                txt_c_company_name.setText(WordUtils.capitalize(marker.getTitle() + ""));
                txt_c_country.setText(marker.getSnippet() + "");
                if (person.type == DEAL_HERO) {
                    AppDelegate.LogT("is DEAL_HERO");
                    txt_c_company_name.setText(WordUtils.capitalize(person.name + ""));
                    txt_c_country.setText(person.heroDealsModel.cityName + "");
                    txt_c_vendor_name.setText(Html.fromHtml("<b>Vendor: </b>" + person.heroDealsModel.vendor_name));
                    AppDelegate.LogT("setValues called== person.heroDealsModel.cityName" + person.heroDealsModel.cityName);
                    if (AppDelegate.isValidString(person.heroDealsModel.image)) {
                        img_c_loading1.setVisibility(View.VISIBLE);
                        AnimationDrawable frameAnimation = (AnimationDrawable) img_c_loading1.getDrawable();
                        frameAnimation.setCallback(img_c_loading1);
                        frameAnimation.setVisible(true, true);
                        frameAnimation.start();

                        imageLoader.loadImage(person.heroDealsModel.image, options, new ImageLoadingListener() {
                            @Override
                            public void onLoadingStarted(String imageUri, View view) {

                            }

                            @Override
                            public void onLoadingFailed(String imageUri, View view, FailReason failReason) {
                                cimg_user.setImageDrawable(getResources().getDrawable(R.drawable.user_noimage));
                            }

                            @Override
                            public void onLoadingComplete(String imageUri, View view, Bitmap bitmap) {
                                AppDelegate.LogT("MainActivity onBitmapLoaded");
                                cimg_user.setImageBitmap(bitmap);
                                img_c_loading1.setVisibility(View.GONE);
                            }

                            @Override
                            public void onLoadingCancelled(String imageUri, View view) {

                            }
                        });

                    }
                } else {
                    txt_c_company_name.setText(WordUtils.capitalize(person.name + ""));
                    txt_c_country.setText(/*city + ", " + state + ", " +*/ person.adHocDealsModel.cityName + "");
                    AppDelegate.LogT("setValues called");
                    txt_c_vendor_name.setText(Html.fromHtml("<b>Vendor: </b>" + person.adHocDealsModel.vendor_name));
                    if (AppDelegate.isValidString(person.adHocDealsModel.image)) {
                        img_c_loading1.setVisibility(View.VISIBLE);
                        AnimationDrawable frameAnimation = (AnimationDrawable) img_c_loading1.getDrawable();
                        frameAnimation.setCallback(img_c_loading1);
                        frameAnimation.setVisible(true, true);
                        frameAnimation.start();

                        imageLoader.loadImage(person.adHocDealsModel.image, options, new ImageLoadingListener() {
                            @Override
                            public void onLoadingStarted(String imageUri, View view) {

                            }

                            @Override
                            public void onLoadingFailed(String imageUri, View view, FailReason failReason) {
                                cimg_user.setImageDrawable(getResources().getDrawable(R.drawable.user_noimage));
                            }

                            @Override
                            public void onLoadingComplete(String imageUri, View view, Bitmap bitmap) {
                                AppDelegate.LogT("MainActivity onBitmapLoaded");
                                cimg_user.setImageBitmap(bitmap);
                                img_c_loading1.setVisibility(View.GONE);
                            }

                            @Override
                            public void onLoadingCancelled(String imageUri, View view) {

                            }
                        });
                    }

                }
                return v;
            }

            @Override
            public View getInfoContents(final Marker marker) {
                return null;
            }
        });
    }


}
