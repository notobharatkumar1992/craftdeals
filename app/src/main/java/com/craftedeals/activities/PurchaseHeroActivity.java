package com.craftedeals.activities;

import android.content.Intent;
import android.graphics.Bitmap;
import android.graphics.drawable.AnimationDrawable;
import android.graphics.drawable.Drawable;
import android.os.Build;
import android.os.Bundle;
import android.os.Handler;
import android.os.Message;
import android.support.v4.app.ActivityOptionsCompat;
import android.support.v4.util.Pair;
import android.support.v7.app.AppCompatActivity;
import android.text.Html;
import android.view.View;
import android.view.Window;
import android.view.WindowManager;
import android.widget.LinearLayout;

import com.craftedeals.AppDelegate;
import com.craftedeals.Async.PostAsync;
import com.craftedeals.Models.HeroDealsModel;
import com.craftedeals.Models.OpenHoursModel;
import com.craftedeals.Models.PostAysnc_Model;
import com.craftedeals.R;
import com.craftedeals.Utils.CircleImageView;
import com.craftedeals.Utils.Prefs;
import com.craftedeals.Utils.TransitionHelper;
import com.craftedeals.constants.ServerRequestConstants;
import com.craftedeals.constants.Tags;
import com.craftedeals.interfaces.OnReciveServerResponse;
import com.craftedeals.parser.JSONParser;
import com.nostra13.universalimageloader.core.DisplayImageOptions;
import com.nostra13.universalimageloader.core.ImageLoader;
import com.nostra13.universalimageloader.core.assist.FailReason;
import com.nostra13.universalimageloader.core.listener.ImageLoadingListener;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;

import carbon.widget.ImageView;
import carbon.widget.TextView;

/**
 * Created by Heena on 07-Dec-16.
 */
public class PurchaseHeroActivity extends AppCompatActivity implements View.OnClickListener, OnReciveServerResponse
{

    private Handler mHandler;
    TextView txt_c_comapny_open_hour, txt_c_comapny_contact_number, txt_c_company_name, txt_c_comapny_address, txt_c_title, txt_c_vendor_company_name, txt_c_redeem_status, txt_c_redeemed_on, txt_c_value_upto, txt_c_description;
    private int deal_id, purchase_id;
    private HeroDealsModel heroDealsModel;
    private ImageLoader imageLoader = ImageLoader.getInstance();
    private DisplayImageOptions options = new DisplayImageOptions.Builder().cacheInMemory(true)
            .cacheOnDisc(true).resetViewBeforeLoading(true).
                    build();
    private ImageView img_c_deal;
    android.widget.ImageView img_loading1, img_loading2;
    public String str_from;
    CircleImageView img_circle_deal_image;

    public LinearLayout ll_distance;
    public TextView txt_c_distance;

    @Override
    protected void onCreate(Bundle savedInstanceState)
    {
        super.onCreate(savedInstanceState);
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.LOLLIPOP)
        {
            Window window = this.getWindow();
            Drawable background = this.getResources().getDrawable(R.drawable.topbg);
            window.addFlags(WindowManager.LayoutParams.FLAG_DRAWS_SYSTEM_BAR_BACKGROUNDS);
            window.setStatusBarColor(getResources().getColor(android.R.color.transparent));
            window.setNavigationBarColor(getResources().getColor(android.R.color.transparent));
            window.setBackgroundDrawable(background);
        }

        setContentView(R.layout.purchase_herodeals);
        initView();
        setHandler();
        deal_id = getIntent().getIntExtra(Tags.deal_id, 0);
        purchase_id = getIntent().getIntExtra(Tags.purchase_id, 0);
        AppDelegate.LogT("hero deal id is ==>" + deal_id);
        str_from = getIntent().getStringExtra(Tags.FROM);
        if (getIntent() != null && AppDelegate.isValidString(getIntent().getStringExtra(Tags.FROM)))
        {
            if (getIntent().getStringExtra(Tags.FROM).equalsIgnoreCase(Tags.notification))
            {
                execute_ViewNotificationApi(getIntent().getIntExtra(Tags.notification_id, 0));
            }
        }
//        if (!AppDelegate.isValidString(str_from) && getIntent().getExtras() != null && getIntent().getExtras().getParcelable(Tags.deal) != null) {
//            heroDealsModel = getIntent().getExtras().getParcelable(Tags.deal);
//            mHandler.sendEmptyMessage(3);
//        } else {
        execute_getHeroDealModel(purchase_id);
//        }
    }

    private void execute_ViewNotificationApi(int notification_id)
    {
        try
        {
            if (AppDelegate.haveNetworkConnection(this, true))
            {
                ArrayList<PostAysnc_Model> mPostArrayList = new ArrayList<PostAysnc_Model>();
                PostAsync mPostAsyncObj;
                AppDelegate.getInstance(this).setPostParamsSecond(mPostArrayList, Tags.API_KEY, Tags.API_KEY_VALUE);
                AppDelegate.getInstance(this).setPostParamsSecond(mPostArrayList, Tags.user_id, new Prefs(this).getUserdata().id);
                AppDelegate.getInstance(this).setPostParamsSecond(mPostArrayList, Tags.nid, notification_id);
                mPostAsyncObj = new PostAsync(this, this, ServerRequestConstants.VIEW_NOTIFICATION,
                        mPostArrayList, null);
                mHandler.sendEmptyMessage(10);
                mPostAsyncObj.execute();
            }
        }
        catch (Exception e)
        {
            AppDelegate.LogE(e);
            AppDelegate.showToast(this, getString(R.string.try_again));
        }
    }

    private void execute_getHeroDealModel(int deal_id)
    {
        try
        {
            if (AppDelegate.haveNetworkConnection(this, true))
            {
                ArrayList<PostAysnc_Model> mPostArrayList = new ArrayList<PostAysnc_Model>();
                AppDelegate.getInstance(this).setPostParamsSecond(mPostArrayList, Tags.API_KEY, Tags.API_KEY_VALUE);
                AppDelegate.getInstance(this).setPostParamsSecond(mPostArrayList, Tags.user_id, (new Prefs(this).getUserdata().id), ServerRequestConstants.Key_PostintValue);
                AppDelegate.getInstance(this).setPostParamsSecond(mPostArrayList, Tags.cart_id, deal_id + "");
                if (new Prefs(this).getCitydata() != null)
                {
                    AppDelegate.getInstance(this).setPostParamsSecond(mPostArrayList, Tags.LAT, new Prefs(this).getCitydata().lat);
                    AppDelegate.getInstance(this).setPostParamsSecond(mPostArrayList, Tags.LONG, new Prefs(this).getCitydata().lng);
                }
                PostAsync mPostAsyncObj;
                mPostAsyncObj = new PostAsync(this, this, ServerRequestConstants.GET_REDEEMED_HERO_DEAL_DATA,
                        mPostArrayList, null);
                mHandler.sendEmptyMessage(10);
                mPostAsyncObj.execute();
            }
        }
        catch (Exception e)
        {
            AppDelegate.showToast(this, getResources().getString(R.string.try_again));
            AppDelegate.LogE(e);
        }
    }

    private void setHandler()
    {
        mHandler = new Handler()
        {
            @Override
            public void dispatchMessage(Message msg)
            {
                super.dispatchMessage(msg);
                switch (msg.what)
                {
                    case 10:
                        AppDelegate.showProgressDialog(PurchaseHeroActivity.this);
                        break;
                    case 11:
                        AppDelegate.hideProgressDialog(PurchaseHeroActivity.this);
                        break;
                    case 1:
                        break;
                    case 2:
                        break;
                    case 3:
                        if (heroDealsModel != null)
                        {
                            setview();
                        }
                        break;

                }
            }
        };
    }

    private void setview()
    {
        txt_c_comapny_contact_number.setText(Html.fromHtml(heroDealsModel.company_contact_number));
        try
        {
            if (AppDelegate.isValidString(heroDealsModel.company_opening_hours))
            {
                JSONArray object = new JSONArray(heroDealsModel.company_opening_hours);
                ArrayList<OpenHoursModel> openHoursModels = new ArrayList<>();
                StringBuilder sb = new StringBuilder();
                for (int i = 0; i < object.length(); i++)
                {
                    JSONObject object1 = object.getJSONObject(i);
                    OpenHoursModel openHoursModel = new OpenHoursModel();
                    openHoursModel.day = JSONParser.getString(object1, "day");
                    openHoursModel.from = JSONParser.getString(object1, "from");
                    openHoursModel.to = JSONParser.getString(object1, "to");
                    try
                    {
                        openHoursModel.from = new SimpleDateFormat("hh:mm a").format(new SimpleDateFormat("HH:mm").parse(openHoursModel.from));
                        openHoursModel.to = new SimpleDateFormat("hh:mm a").format(new SimpleDateFormat("HH:mm").parse(openHoursModel.to));
                    }
                    catch (ParseException e)
                    {
                        AppDelegate.LogE(e);
                    }
                    sb.append("<b>" + openHoursModel.day + " : </b>" + openHoursModel.from + " - " + openHoursModel.to + "<br>");
//                openHoursModels.add(openHoursModel);
                }
                String hours = sb.toString();
                if (hours.length() > 0)
                {
                    hours = hours.substring(0, hours.length() - 1).toString();
                    txt_c_comapny_open_hour.setText(Html.fromHtml(hours));
                }
            }
            try
            {
                if (AppDelegate.isValidString(heroDealsModel.distance))
                {
                    ll_distance.setVisibility(View.VISIBLE);
                    txt_c_distance.setText(AppDelegate.roundOff(Double.parseDouble(heroDealsModel.distance)) + " KM");
                }
                else
                {
                    ll_distance.setVisibility(View.GONE);
                }
            }
            catch (Exception e)
            {
                AppDelegate.LogE(e);
            }
//            setHours(openHoursModels);
        }
        catch (JSONException e)
        {
            AppDelegate.LogE(e);
        }

        img_loading1.setVisibility(View.VISIBLE);
        AnimationDrawable frameAnimation = (AnimationDrawable) img_loading1.getDrawable();
        frameAnimation.setCallback(img_loading1);
        frameAnimation.setVisible(true, true);
        frameAnimation.start();
        txt_c_company_name.setText(heroDealsModel.company_name);
        txt_c_comapny_address.setText(heroDealsModel.company_address);
        imageLoader.loadImage(heroDealsModel.image, options, new ImageLoadingListener()
        {
            @Override
            public void onLoadingStarted(String imageUri, View view)
            {
            }

            @Override
            public void onLoadingFailed(String imageUri, View view, FailReason failReason)
            {
                img_c_deal.setImageDrawable(getResources().getDrawable(R.drawable.deal_noimage));
                img_loading1.setVisibility(View.GONE);
            }

            @Override
            public void onLoadingComplete(String imageUri, View view, Bitmap loadedImage)
            {
                img_c_deal.setImageBitmap(loadedImage);
                img_loading1.setVisibility(View.GONE);
            }

            @Override
            public void onLoadingCancelled(String imageUri, View view)
            {
            }
        });
        img_loading2.setVisibility(View.VISIBLE);
        frameAnimation = (AnimationDrawable) img_loading2.getDrawable();
        frameAnimation.setCallback(img_loading2);
        frameAnimation.setVisible(true, true);
        frameAnimation.start();

        imageLoader.loadImage(heroDealsModel.company_image, options, new ImageLoadingListener()
        {
            @Override
            public void onLoadingStarted(String imageUri, View view)
            {
            }

            @Override
            public void onLoadingFailed(String imageUri, View view, FailReason failReason)
            {
                img_circle_deal_image.setImageDrawable(getResources().getDrawable(R.drawable.user_noimage));
                img_loading2.setVisibility(View.GONE);
            }

            @Override
            public void onLoadingComplete(String imageUri, View view, Bitmap loadedImage)
            {
                img_circle_deal_image.setImageBitmap(loadedImage);
                img_loading2.setVisibility(View.GONE);
            }

            @Override
            public void onLoadingCancelled(String imageUri, View view)
            {
            }
        });
        txt_c_title.setText(AppDelegate.isValidString(heroDealsModel.herodeal_title) ? heroDealsModel.herodeal_title : "Craftedeals");
        txt_c_vendor_company_name.setText(AppDelegate.isValidString(heroDealsModel.vendor_name) ? heroDealsModel.vendor_name : "");
        txt_c_value_upto.setText("Value up to " + heroDealsModel.value_upto);

        try
        {
            String date = heroDealsModel.modified;
            AppDelegate.LogT("adHocDealsModel.date in adapter==" + date);
            date = date.replaceAll("/+0000", "");
            date = new SimpleDateFormat("dd MMM yyyy ").format(new SimpleDateFormat("yyyy-MM-dd'T'HH:mm:ss").parse(date));
            txt_c_redeemed_on.setText("Redeemed on " + date);
            txt_c_description.setText(heroDealsModel.discount_offer + "");
        }
        catch (Exception e)
        {
            AppDelegate.LogE(e);
        }
    }

    private void initView()
    {
        txt_c_title = (TextView) findViewById(R.id.txt_c_title);
        txt_c_vendor_company_name = (TextView) findViewById(R.id.txt_c_vendor_company_name);
        txt_c_redeem_status = (TextView) findViewById(R.id.txt_c_redeem_status);
        img_loading1 = (android.widget.ImageView) findViewById(R.id.img_loading1);
        img_c_deal = (ImageView) findViewById(R.id.img_c_deal);
        findViewById(R.id.img_c_back).setOnClickListener(this);
        txt_c_comapny_contact_number = (TextView) findViewById(R.id.txt_c_comapny_contact_number);
        txt_c_comapny_contact_number.setOnClickListener(this);
        txt_c_comapny_open_hour = (TextView) findViewById(R.id.txt_c_comapny_open_hour);
        txt_c_redeemed_on = (TextView) findViewById(R.id.txt_c_redeemed_on);
        txt_c_value_upto = (TextView) findViewById(R.id.txt_c_value_upto);
        txt_c_description = (TextView) findViewById(R.id.txt_c_description);
        img_circle_deal_image = (CircleImageView) findViewById(R.id.img_circle_deal_image);
        img_loading2 = (android.widget.ImageView) findViewById(R.id.img_loading2);
        txt_c_company_name = (TextView) findViewById(R.id.txt_c_company_name);
        txt_c_comapny_address = (TextView) findViewById(R.id.txt_c_comapny_address);
        findViewById(R.id.txt_c_view_profile).setOnClickListener(this);

        ll_distance = (LinearLayout) findViewById(R.id.ll_distance);
        txt_c_distance = (TextView) findViewById(R.id.txt_c_distance);
    }


    @Override
    public void onClick(View v)
    {
        switch (v.getId())
        {
            case R.id.img_c_back:
                onBackPressed();
                break;
            case R.id.txt_c_view_profile:
                if (heroDealsModel != null)
                {
                    Intent intent = new Intent(this, VendorProfileActivity.class);
                    final Pair<View, String>[] pairs = TransitionHelper.createSafeTransitionParticipants(this, false, new Pair<>(txt_c_title, "square_blue_name_1"));
                    ActivityOptionsCompat transitionActivityOptions = ActivityOptionsCompat.makeSceneTransitionAnimation(this, pairs);
                    intent.putExtra(Tags.deal_id, heroDealsModel.id);
                    intent.putExtra(Tags.vendor_id, heroDealsModel.user_id);
                    Bundle bundle = new Bundle();
                    bundle.putParcelable(Tags.deal, heroDealsModel);
                    intent.putExtra(Tags.type, 2);
                    startActivity(intent/*, transitionActivityOptions.toBundle()*/);
                }
                break;
            case R.id.txt_c_comapny_contact_number:
                if (heroDealsModel != null)
                {
                    AppDelegate.makeCall(PurchaseHeroActivity.this, heroDealsModel != null ? heroDealsModel.company_contact_number : "");
                }
                break;
        }
    }

    @Override
    public void setOnReciveResult(String apiName, String result)
    {
        mHandler.sendEmptyMessage(11);
        if (!AppDelegate.isValidString(result))
        {
            AppDelegate.showToast(PurchaseHeroActivity.this, getResources().getString(R.string.time_out));
            return;
        }
        if (apiName.equalsIgnoreCase(ServerRequestConstants.GET_REDEEMED_HERO_DEAL_DATA))
        {
            parsePurchaseDealModel(result);
        }
        else if (apiName.equalsIgnoreCase(ServerRequestConstants.VIEW_NOTIFICATION))
        {
            parseViewNotification(result);
        }
    }

    private void parseViewNotification(String result)
    {
        try
        {
            JSONObject jsonObject = new JSONObject(result);
//            if (jsonObject.getInt(Tags.status) == 1) {
//                AppDelegate.showToast(this, jsonObject.getString(Tags.message));
//            } else {
//                AppDelegate.showToast(this, jsonObject.getString(Tags.message));
//            }
        }
        catch (JSONException e)
        {
            AppDelegate.LogE(e);
        }
    }

    private void parsePurchaseDealModel(String result)
    {
        try
        {
            JSONObject jsonobj = new JSONObject(result);
            if (jsonobj.getInt(Tags.status) == 1)
            {
                JSONObject obj = jsonobj.getJSONObject(Tags.data);
                heroDealsModel = new HeroDealsModel();
                heroDealsModel.distance = obj.optString(Tags.distance);

                heroDealsModel.coupon_id = obj.getInt(Tags.coupon_id);
                heroDealsModel.deal_id = obj.getInt(Tags.deal_id);

                // heroDealsModel.name = obj.getString(Tags.name);
                heroDealsModel.deal_status = obj.getInt(Tags.type);

                JSONObject heroobj = obj.getJSONObject(Tags.herodeal);
                heroDealsModel.herodeal_title = heroobj.getString(Tags.name);
                heroDealsModel.user_id = heroobj.getInt(Tags.user_id);
                heroDealsModel.deal_status = obj.getInt(Tags.type);
                heroDealsModel.image = heroobj.getString(Tags.image);
                heroDealsModel.deal_category_id = heroobj.getInt(Tags.deal_category_id);
                heroDealsModel.sub_category_id = heroobj.getInt(Tags.sub_category_id);
                heroDealsModel.description = heroobj.getString(Tags.description);
                heroDealsModel.value_upto = heroobj.getInt(Tags.value_upto);
                heroDealsModel.discount_offer = heroobj.getString(Tags.discount_offer);
                heroDealsModel.starting_date = heroobj.getString(Tags.starting_date);
                heroDealsModel.expiry_date = heroobj.getString(Tags.expiry_date);
                heroDealsModel.created = heroobj.getString(Tags.created);
                heroDealsModel.modified = heroobj.getString(Tags.modified);
                heroDealsModel.trems_and_conditions = heroobj.getString(Tags.trems_and_conditions);
                heroDealsModel.total_deal = heroobj.getInt(Tags.total_deal);
                if (AppDelegate.isValidString(heroobj.getString(Tags.user)))
                {
                    JSONObject vendor = heroobj.getJSONObject(Tags.user);
                    heroDealsModel.vendor_name = vendor.getString(Tags.name);
                    if (vendor.has(Tags.busines))
                    {
                        if (AppDelegate.isValidString(vendor.getString(Tags.busines)))
                        {
                            heroDealsModel.company_name = vendor.getJSONObject(Tags.busines).getString(Tags.name);
                            heroDealsModel.company_image = vendor.getJSONObject(Tags.busines).getString(Tags.image);
                            heroDealsModel.latitude = vendor.getJSONObject(Tags.busines).getString(Tags.latitude);
                            heroDealsModel.longitude = vendor.getJSONObject(Tags.busines).getString(Tags.longitude);
                            heroDealsModel.company_address = vendor.getJSONObject(Tags.busines).getString(Tags.location_address);
                            heroDealsModel.company_opening_hours = vendor.getJSONObject(Tags.busines).getString(Tags.open_hours);
                            heroDealsModel.company_contact_number = vendor.getJSONObject(Tags.busines).getString(Tags.contact_number);
//                            heroDealsModel.cityName = vendor.getJSONObject(Tags.busines).optString(Tags.cityName);
                        }
                    }
                }
                mHandler.sendEmptyMessage(3);
            }
            else
            {
                AppDelegate.showAlert(this, jsonobj.getString(Tags.message));
            }
        }
        catch (JSONException e)
        {
            AppDelegate.LogE(e);
        }
    }
}
