package com.craftedeals.activities;

import android.app.Activity;
import android.content.Intent;
import android.graphics.drawable.Drawable;
import android.os.Build;
import android.os.Bundle;
import android.os.Handler;
import android.os.Message;
import android.support.annotation.Nullable;
import android.support.v4.app.ActivityOptionsCompat;
import android.support.v4.util.Pair;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.DefaultItemAnimator;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.View;
import android.view.Window;
import android.view.WindowManager;

import com.craftedeals.AppDelegate;
import com.craftedeals.Async.PostAsync;
import com.craftedeals.Models.NotificationModel;
import com.craftedeals.Models.PostAysnc_Model;
import com.craftedeals.Models.UserDataModel;
import com.craftedeals.R;
import com.craftedeals.Utils.Prefs;
import com.craftedeals.Utils.TransitionHelper;
import com.craftedeals.adapters.PremiumPackageListAdapter;
import com.craftedeals.constants.ServerRequestConstants;
import com.craftedeals.constants.Tags;
import com.craftedeals.interfaces.OnReciveServerResponse;
import com.craftedeals.parser.JSONParser;
import com.paypal.android.sdk.payments.PayPalAuthorization;
import com.paypal.android.sdk.payments.PayPalFuturePaymentActivity;
import com.paypal.android.sdk.payments.PayPalPayment;
import com.paypal.android.sdk.payments.PayPalProfileSharingActivity;
import com.paypal.android.sdk.payments.PayPalService;
import com.paypal.android.sdk.payments.PaymentActivity;
import com.paypal.android.sdk.payments.PaymentConfirmation;

import org.json.JSONException;
import org.json.JSONObject;

import java.math.BigDecimal;
import java.text.SimpleDateFormat;
import java.util.ArrayList;

import carbon.widget.TextView;

/**
 * Created by Bharat on 07-Dec-16.
 */
public class UpgradeMembershipActivity extends AppCompatActivity implements OnReciveServerResponse, View.OnClickListener
{

    //    private MembershipModel membershipModel;
    public static Handler mHandler;
    private TextView txt_c_status, txt_c_subscription, txt_c_start_date, txt_c_expiry_date;
    private RecyclerView recycler_package;
    private PremiumPackageListAdapter mAdapter;
    ArrayList<String> packageList = new ArrayList<>();
    NotificationModel notificationModel;


    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState)
    {
        super.onCreate(savedInstanceState);
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.LOLLIPOP)
        {
            Window window = this.getWindow();
            Drawable background = this.getResources().getDrawable(R.drawable.topbg);
            window.addFlags(WindowManager.LayoutParams.FLAG_DRAWS_SYSTEM_BAR_BACKGROUNDS);
            window.setStatusBarColor(getResources().getColor(android.R.color.transparent));
            window.setNavigationBarColor(getResources().getColor(android.R.color.transparent));
            window.setBackgroundDrawable(background);
        }
        setContentView(R.layout.upgrade_membership);
        initView();
        setHandler();
        try
        {
            if (getIntent() != null && AppDelegate.isValidString(getIntent().getStringExtra(Tags.FROM)))
            {
                if (getIntent().getStringExtra(Tags.FROM).equalsIgnoreCase(Tags.notification))
                {
                    execute_ViewNotificationApi(getIntent().getIntExtra(Tags.notification_id, 0));
                }
            }
        }
        catch (Exception e)
        {
            AppDelegate.LogE(e);
        }
        setMemberShipData();
        execute_getMyProfile();
    }

    private void execute_getMyProfile()
    {
        try
        {
            if (AppDelegate.haveNetworkConnection(UpgradeMembershipActivity.this, true))
            {
                ArrayList<PostAysnc_Model> mPostArrayList = new ArrayList<PostAysnc_Model>();
                AppDelegate.getInstance(UpgradeMembershipActivity.this).setPostParamsSecond(mPostArrayList, Tags.API_KEY, Tags.API_KEY_VALUE);
                AppDelegate.getInstance(UpgradeMembershipActivity.this).setPostParamsSecond(mPostArrayList, Tags.user_id, new Prefs(this).getUserdata().id, ServerRequestConstants.Key_PostintValue);

                PostAsync mPostAsyncObj;
                mPostAsyncObj = new PostAsync(UpgradeMembershipActivity.this, this, ServerRequestConstants.GET_MYPROFILE, mPostArrayList, null);
                AppDelegate.showProgressDialog(UpgradeMembershipActivity.this);
                mPostAsyncObj.execute();
            }
        }
        catch (Exception e)
        {
            AppDelegate.showToast(UpgradeMembershipActivity.this, getResources().getString(R.string.try_again));
        }
    }

    private void execute_ViewNotificationApi(int notification_id)
    {
        try
        {
            if (AppDelegate.haveNetworkConnection(this, true))
            {
                ArrayList<PostAysnc_Model> mPostArrayList = new ArrayList<PostAysnc_Model>();
                PostAsync mPostAsyncObj;
                AppDelegate.getInstance(this).setPostParamsSecond(mPostArrayList, Tags.API_KEY, Tags.API_KEY_VALUE);
                AppDelegate.getInstance(this).setPostParamsSecond(mPostArrayList, Tags.user_id, new Prefs(this).getUserdata().id);
                AppDelegate.getInstance(this).setPostParamsSecond(mPostArrayList, Tags.nid, notification_id);
                mPostAsyncObj = new PostAsync(this, this, ServerRequestConstants.VIEW_NOTIFICATION,
                        mPostArrayList, null);
                if (mHandler == null)
                {
                    mHandler = new Handler();
                }
//                mHandler.sendEmptyMessage(10);
                mPostAsyncObj.execute();
            }
        }
        catch (Exception e)
        {
            AppDelegate.LogE(e);
            AppDelegate.showToast(this, getString(R.string.try_again));
        }
    }

    @Override
    protected void onDestroy()
    {
        super.onDestroy();
        mHandler = null;
    }

    private void setHandler()
    {
        mHandler = new Handler()
        {
            @Override
            public void dispatchMessage(Message msg)
            {
                super.dispatchMessage(msg);
                switch (msg.what)
                {
                    case 10:
                        AppDelegate.showProgressDialog(UpgradeMembershipActivity.this);
                        break;
                    case 11:
                        AppDelegate.hideProgressDialog(UpgradeMembershipActivity.this);
                        break;
                    case 7:
                        execute_getMyProfile();
                        break;
                    case 9:
                        setMemberShipData();
                        break;
                }
            }
        };
    }

    @Override
    protected void onResume()
    {
        super.onResume();
        if (mHandler != null)
        {
            mHandler.sendEmptyMessage(7);
        }
    }

    private void setMemberShipData()
    {
        UserDataModel userDataModel = new Prefs(this).getUserdata();
        if ((userDataModel != null) && userDataModel.membership_id != 1)
        {
            packageList.clear();
            packageList.add(getString(R.string.get1_paid));
            packageList.add(getString(R.string.get2_paid));
            packageList.add(getString(R.string.get3_paid));
            packageList.add(getString(R.string.get4_paid));
            findViewById(R.id.txt_c_submit).setVisibility(View.GONE);
            showPackageList(packageList);
        }
        else
        {
            findViewById(R.id.txt_c_submit).setVisibility(View.VISIBLE);
            if (AppDelegate.isValidString(userDataModel.membership_start_date))
            {
                packageList.clear();
                packageList.add(getString(R.string.get1_upgrade));
                packageList.add(getString(R.string.get2_upgrade));
                packageList.add(getString(R.string.get3_upgrade));
                packageList.add(getString(R.string.get4_upgrade));
                showPackageList(packageList);
            }
            else
            {
                packageList.clear();
                packageList.add(getString(R.string.get1_free));
                packageList.add(getString(R.string.get2_free));
                packageList.add(getString(R.string.get3_free));
                packageList.add(getString(R.string.get4_free));
                showPackageList(packageList);
            }

        }

        if (userDataModel != null && userDataModel.membership_id != 1)
        {
            try
            {
                String starting_date = userDataModel.membership_start_date;
                starting_date = starting_date.replaceAll("/+0000", "");
                String expiry_date = userDataModel.membership_end_date;
                expiry_date = expiry_date.replaceAll("/+0000", "");
                starting_date = new SimpleDateFormat("dd MMM yyyy").format(new SimpleDateFormat("yyyy-MM-dd'T'HH:mm:ss").parse(starting_date));
                expiry_date = new SimpleDateFormat("dd MMM yyyy").format(new SimpleDateFormat("yyyy-MM-dd'T'HH:mm:ss").parse(expiry_date));

                txt_c_start_date.setText(starting_date + "");
                txt_c_expiry_date.setText(expiry_date + "");
                txt_c_status.setText("Active");
                txt_c_subscription.setText("Hero Membership");
            }
            catch (Exception e)
            {
                AppDelegate.LogE(e);
            }
        }
        else if (userDataModel != null && userDataModel.membership_id == 1 && AppDelegate.isValidString(userDataModel.membership_start_date) && AppDelegate.isValidString(userDataModel.membership_end_date))
        {
//            if (membershipModel != null && AppDelegate.isValidString(membershipModel.month_of_membership)) {
            try
            {
                String starting_date = userDataModel.membership_start_date;
                starting_date = starting_date.replaceAll("/+0000", "");
                String expiry_date = userDataModel.membership_end_date;
                expiry_date = expiry_date.replaceAll("/+0000", "");
                starting_date = new SimpleDateFormat("dd MMM yyyy").format(new SimpleDateFormat("yyyy-MM-dd'T'HH:mm:ss").parse(starting_date));
                expiry_date = new SimpleDateFormat("dd MMM yyyy").format(new SimpleDateFormat("yyyy-MM-dd'T'HH:mm:ss").parse(expiry_date));
                txt_c_start_date.setText(starting_date + "");
                txt_c_expiry_date.setText(expiry_date + "");
                txt_c_status.setText("Expired");
                txt_c_subscription.setText("Hero Membership");
            }
            catch (Exception e)
            {
                AppDelegate.LogE(e);
                txt_c_start_date.setText("-");
                txt_c_expiry_date.setText("-");
                txt_c_status.setText("Active");
                txt_c_subscription.setText("Free Membership");
            }
        }
        else
        {
            txt_c_start_date.setText("-");
            txt_c_expiry_date.setText("-");
            txt_c_status.setText("Active");
            txt_c_subscription.setText("Free Membership");
        }
    }

    private void initView()
    {
        findViewById(R.id.img_c_back).setOnClickListener(this);
        txt_c_status = (TextView) findViewById(R.id.txt_c_status);
        txt_c_subscription = (TextView) findViewById(R.id.txt_c_subscription);
        txt_c_start_date = (TextView) findViewById(R.id.txt_c_start_date);
        txt_c_expiry_date = (TextView) findViewById(R.id.txt_c_expiry_date);
        findViewById(R.id.txt_c_submit).setOnClickListener(this);
        recycler_package = (RecyclerView) findViewById(R.id.recycler_package);
        recycler_package.setLayoutManager(new LinearLayoutManager(this));
        recycler_package.setHasFixedSize(true);
        recycler_package.setItemAnimator(new DefaultItemAnimator());
    }

    private void callMembershipAsync()
    {
        if (AppDelegate.haveNetworkConnection(this, true))
        {
            ArrayList<PostAysnc_Model> mPostArrayList = new ArrayList<PostAysnc_Model>();
            AppDelegate.getInstance(this).setPostParamsSecond(mPostArrayList, Tags.API_KEY, Tags.API_KEY_VALUE);
            PostAsync mPostAsyncObj = new PostAsync(this, this, ServerRequestConstants.GET_MEMBERSHIP_LIST, mPostArrayList, null);
            mHandler.sendEmptyMessage(10);
            mPostAsyncObj.execute();
        }
    }

    @Override
    public void onClick(View v)
    {
        switch (v.getId())
        {
            case R.id.img_c_back:
                onBackPressed();
                break;

            case R.id.txt_c_submit:
                Intent intent = new Intent(UpgradeMembershipActivity.this, UpgradePremiumMemberShipActivity.class);
                final Pair<View, String>[] pairs = TransitionHelper.createSafeTransitionParticipants(UpgradeMembershipActivity.this, false, new Pair<>(findViewById(R.id.txt_c_title), "upgrade_membership"));
                ActivityOptionsCompat transitionActivityOptions = ActivityOptionsCompat.makeSceneTransitionAnimation(UpgradeMembershipActivity.this, pairs);
                startActivity(intent, transitionActivityOptions.toBundle());
                break;
        }
    }

    private void execute_upgradeMembershipAsync(String paypal_id)
    {
        try
        {
            if (AppDelegate.haveNetworkConnection(this, true))
            {
                ArrayList<PostAysnc_Model> mPostArrayList = new ArrayList<PostAysnc_Model>();
                PostAsync mPostAsyncObj;
                AppDelegate.getInstance(this).setPostParamsSecond(mPostArrayList, Tags.API_KEY, Tags.API_KEY_VALUE);
                AppDelegate.getInstance(this).setPostParamsSecond(mPostArrayList, Tags.user_id, new Prefs(this).getUserdata().id);

//                AppDelegate.getInstance(this).setPostParamsSecond(mPostArrayList, Tags.membership_id, membershipModel.membership_id + "");
//                AppDelegate.getInstance(this).setPostParamsSecond(mPostArrayList, Tags.total_price, membershipModel.amount);
                AppDelegate.getInstance(this).setPostParamsSecond(mPostArrayList, Tags.paypal_id, paypal_id);

                mPostAsyncObj = new PostAsync(this, this, ServerRequestConstants.UPGRADE_MEMBERSHIP,
                        mPostArrayList, null);
                mHandler.sendEmptyMessage(10);
                mPostAsyncObj.execute();
            }
        }
        catch (Exception e)
        {
            AppDelegate.LogE(e);
            AppDelegate.showToast(this, getString(R.string.try_again));
        }
    }

    @Override
    public void setOnReciveResult(String apiName, String result)
    {
        if (mHandler == null)
        {
            return;
        }
        mHandler.sendEmptyMessage(11);
        if (!AppDelegate.isValidString(result))
        {
            AppDelegate.showToast(this, getString(R.string.time_out));
            return;
        }
        if (apiName.equalsIgnoreCase(ServerRequestConstants.UPGRADE_MEMBERSHIP))
        {
            parseChangePassword(result);
        }
        else if (apiName.equalsIgnoreCase(ServerRequestConstants.GET_MEMBERSHIP_LIST))
        {
            parseMembershipList(result);
        }
        else if (apiName.equalsIgnoreCase(ServerRequestConstants.VIEW_NOTIFICATION))
        {
            parseViewNotification(result);
        }
        else if (apiName.equalsIgnoreCase(ServerRequestConstants.GET_MYPROFILE))
        {
            mHandler.sendEmptyMessage(11);
            parseGetMyProfile(result);
        }
    }

    private void parseGetMyProfile(String result)
    {
        try
        {
            JSONObject jsonObject = new JSONObject(result);
            if (jsonObject.getString(Tags.status).equalsIgnoreCase("1"))
            {
                //  AppDelegate.showToast(MyProfileActivity.this, jsonObject.getString(Tags.message));
                JSONObject object = jsonObject.getJSONObject(Tags.response);
                UserDataModel userDataModel = new UserDataModel();
                userDataModel.id = JSONParser.getInt(object, Tags.id);
                userDataModel.name = JSONParser.getString(object, Tags.name);
                userDataModel.first_names = JSONParser.getString(object, Tags.name);
                userDataModel.last_names = JSONParser.getString(object, Tags.last_name);
                userDataModel.email = JSONParser.getString(object, Tags.email);
                userDataModel.contact_number = JSONParser.getString(object, Tags.contact_number);
                userDataModel.membership_id = JSONParser.getInt(object, Tags.membership_id);
                userDataModel.image = JSONParser.getString(object, Tags.image);
                userDataModel.device_id = JSONParser.getString(object, Tags.device_id);
                userDataModel.device_type = JSONParser.getInt(object, Tags.device_type);
                userDataModel.created = JSONParser.getString(object, Tags.created);
                userDataModel.address = JSONParser.getString(object, Tags.address);
                userDataModel.total_purchase = JSONParser.getInt(object, Tags.total_purchase);
                userDataModel.total_redeemed = JSONParser.getInt(object, Tags.total_redeemed);
                userDataModel.favourite = JSONParser.getInt(object, Tags.favourite);
                userDataModel.social_id = JSONParser.getString(object, Tags.social_id);
                userDataModel.fav_brewery = JSONParser.getString(object, Tags.brewery_type_id);
                if (object.has(Tags.user_range))
                {
                    userDataModel.user_range = JSONParser.getInt(object, Tags.user_range);
                }
                if (object.has(Tags.heard_about_us))
                {
                    userDataModel.heard_about_us = JSONParser.getString(object, Tags.heard_about_us);
                }
                if (object.has(Tags.membership_start_date) && object.has(Tags.membership_end_date))
                {
                    userDataModel.membership_start_date = JSONParser.getString(object, Tags.membership_start_date);
                    userDataModel.membership_end_date = JSONParser.getString(object, Tags.membership_end_date);
                }
                userDataModel.dob = JSONParser.getString(object, Tags.dob);
                if (AppDelegate.isValidString(object.getString(Tags.state)))
                {
                    JSONObject state = object.getJSONObject(Tags.state);
                    userDataModel.province = JSONParser.getString(state, Tags.region_name);
                    userDataModel.state_id = JSONParser.getInt(state, Tags.id) + "";
                }
                if (AppDelegate.isValidString(object.getString(Tags.city)))
                {
                    JSONObject city = object.getJSONObject(Tags.city);
                    userDataModel.city = JSONParser.getString(city, Tags.name);
                    userDataModel.city_id = JSONParser.getInt(city, Tags.id) + "";
                }
                new Prefs(this).setUserData(userDataModel);
                mHandler.sendEmptyMessage(9);
            }
            else
            {
                AppDelegate.showAlert(UpgradeMembershipActivity.this, jsonObject.getString(Tags.message));
            }
        }
        catch (Exception e)
        {
            AppDelegate.LogE(e);
        }
    }

    private void parseViewNotification(String result)
    {
        try
        {
            JSONObject jsonObject = new JSONObject(result);
//            if (jsonObject.getInt(Tags.status) == 1) {
//                AppDelegate.showToast(this, jsonObject.getString(Tags.message));
//            } else {
//                AppDelegate.showToast(this, jsonObject.getString(Tags.message));
//            }
        }
        catch (JSONException e)
        {
            AppDelegate.LogE(e);
        }
    }

    private void showPackageList(ArrayList<String> packageList)
    {
        LinearLayoutManager mLinearLayoutManager = new LinearLayoutManager(this);
        mLinearLayoutManager.setOrientation(LinearLayoutManager.VERTICAL);
        mAdapter = new PremiumPackageListAdapter(this, packageList);
        recycler_package.setAdapter(mAdapter);
    }

    private void parseMembershipList(String result)
    {
    }

    private void parseChangePassword(String result)
    {
        try
        {
            JSONObject jsonObject = new JSONObject(result);
            if (jsonObject.getInt(Tags.status) == 1)
            {
                AppDelegate.showToast(this, jsonObject.getString(Tags.message));
                JSONObject data = jsonObject.getJSONObject("data");
                UserDataModel userDataModel = new Prefs(this).getUserdata();
                userDataModel.membership_start_date = JSONParser.getString(data, Tags.start_date);
                userDataModel.membership_end_date = JSONParser.getString(data, Tags.end_date);
                userDataModel.membership_id = JSONParser.getInt(data, Tags.membership_id);
                new Prefs(this).setUserData(userDataModel);
                mHandler.sendEmptyMessage(9);
                finish();
            }
            else
            {
                AppDelegate.showToast(this, jsonObject.getString(Tags.message));
            }
        }
        catch (JSONException e)
        {
            e.printStackTrace();
        }
    }

    /*paypal integration code Start*/
    public void onBuyPressed(String str_amount, String str_item_name)
    {
        /*
         * PAYMENT_INTENT_SALE will cause the payment to complete immediately.
         * Change PAYMENT_INTENT_SALE to
         *   - PAYMENT_INTENT_AUTHORIZE to only authorize payment and capture funds later.
         *   - PAYMENT_INTENT_ORDER to create a payment for authorization and capture
         *     later via calls from your server.
         *
         * Also, to include additional payment details and an item list, see getStuffToBuy() below.
         */
        PayPalPayment thingToBuy = new PayPalPayment(new BigDecimal(str_amount), "AUD", str_item_name, PayPalPayment.PAYMENT_INTENT_SALE);

        /*
         * See getStuffToBuy(..) for examples of some available payment options.
         */
        Intent intent = new Intent(UpgradeMembershipActivity.this, PaymentActivity.class);

        // send the same configuration for restart resiliency
        intent.putExtra(PayPalService.EXTRA_PAYPAL_CONFIGURATION, AppDelegate.config);
        intent.putExtra(PaymentActivity.EXTRA_PAYMENT, thingToBuy);
        startActivityForResult(intent, AppDelegate.REQUEST_CODE_PAYMENT);
    }

//    private PayPalPayment getThingToBuy(String paymentIntent) {
//        return new PayPalPayment(new BigDecimal("0.01"), "USD", "sample item",
//                paymentIntent);
//    }

    private void startPayPalService()
    {
        Intent intent = new Intent(this, PayPalService.class);
        intent.putExtra(PayPalService.EXTRA_PAYPAL_CONFIGURATION, AppDelegate.config);
        startService(intent);
    }

    public String TAG = "paypal";

    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data)
    {
        if (requestCode == AppDelegate.REQUEST_CODE_PAYMENT)
        {
            if (resultCode == Activity.RESULT_OK)
            {
                PaymentConfirmation confirm =
                        data.getParcelableExtra(PaymentActivity.EXTRA_RESULT_CONFIRMATION);
                if (confirm != null)
                {
                    try
                    {
                        Log.i(TAG, confirm.toJSONObject().toString(4));
                        Log.i(TAG, confirm.getPayment().toJSONObject().toString(4));
                        /**
                         *  TODO: send 'confirm' (and possibly confirm.getPayment() to your server for verification
                         * or consent completion.
                         * See https://developer.paypal.com/webapps/developer/docs/integration/mobile/verify-mobile-payment/
                         * for more details.
                         *
                         * For sample mobile backend interactions, see
                         * https://github.com/paypal/rest-api-sdk-python/tree/master/samples/mobile_backend
                         */
                        JSONObject jsonObject = new JSONObject(confirm.toJSONObject().toString(4));
                        jsonObject.getJSONObject("response").getString("id");
                        AppDelegate.trackUpgradeAccount(new Prefs(this).getUserdata().name, new Prefs(this).getUserdata().id, "UPGRADE MEMBERSHIP ACTIVITY");
                        execute_upgradeMembershipAsync(jsonObject.getJSONObject("response").getString("id"));

                        AppDelegate.showToast(UpgradeMembershipActivity.this, "PaymentConfirmation info received from PayPal, T ID is = " + jsonObject.getJSONObject("response").getString("id"));

                    }
                    catch (JSONException e)
                    {
                        Log.e(TAG, "an extremely unlikely failure occurred: ", e);
                    }
                }
            }
            else if (resultCode == Activity.RESULT_CANCELED)
            {
                Log.i(TAG, "The user canceled.");
            }
            else if (resultCode == PaymentActivity.RESULT_EXTRAS_INVALID)
            {
                Log.i(
                        TAG,
                        "An invalid Payment or PayPalConfiguration was submitted. Please see the docs.");
            }
        }
        else if (requestCode == AppDelegate.REQUEST_CODE_FUTURE_PAYMENT)
        {
            if (resultCode == Activity.RESULT_OK)
            {
                PayPalAuthorization auth =
                        data.getParcelableExtra(PayPalFuturePaymentActivity.EXTRA_RESULT_AUTHORIZATION);
                if (auth != null)
                {
                    try
                    {
                        Log.i("FuturePaymentExample", auth.toJSONObject().toString(4));

                        String authorization_code = auth.getAuthorizationCode();
                        Log.i("FuturePaymentExample", authorization_code);

                        sendAuthorizationToServer(auth);
                        AppDelegate.showToast(UpgradeMembershipActivity.this, "Future Payment code received from PayPal");

                    }
                    catch (JSONException e)
                    {
                        Log.e("FuturePaymentExample", "an extremely unlikely failure occurred: ", e);
                    }
                }
            }
            else if (resultCode == Activity.RESULT_CANCELED)
            {
                Log.i("FuturePaymentExample", "The user canceled.");
            }
            else if (resultCode == PayPalFuturePaymentActivity.RESULT_EXTRAS_INVALID)
            {
                Log.i(
                        "FuturePaymentExample",
                        "Probably the attempt to previously start the PayPalService had an invalid PayPalConfiguration. Please see the docs.");
            }
        }
        else if (requestCode == AppDelegate.REQUEST_CODE_PROFILE_SHARING)
        {
            if (resultCode == Activity.RESULT_OK)
            {
                PayPalAuthorization auth =
                        data.getParcelableExtra(PayPalProfileSharingActivity.EXTRA_RESULT_AUTHORIZATION);
                if (auth != null)
                {
                    try
                    {
                        Log.i("ProfileSharingExample", auth.toJSONObject().toString(4));

                        String authorization_code = auth.getAuthorizationCode();
                        Log.i("ProfileSharingExample", authorization_code);

                        sendAuthorizationToServer(auth);
                        AppDelegate.showToast(UpgradeMembershipActivity.this, "Profile Sharing code received from PayPal");

                    }
                    catch (JSONException e)
                    {
                        Log.e("ProfileSharingExample", "an extremely unlikely failure occurred: ", e);
                    }
                }
            }
            else if (resultCode == Activity.RESULT_CANCELED)
            {
                Log.i("ProfileSharingExample", "The user canceled.");
            }
            else if (resultCode == PayPalFuturePaymentActivity.RESULT_EXTRAS_INVALID)
            {
                Log.i(
                        "ProfileSharingExample",
                        "Probably the attempt to previously start the PayPalService had an invalid PayPalConfiguration. Please see the docs.");
            }
        }
    }

    private void sendAuthorizationToServer(PayPalAuthorization authorization)
    {
        /**
         * TODO: Send the authorization response to your server, where it can
         * exchange the authorization code for OAuth access and refresh tokens.
         *
         * Your server must then store these tokens, so that your server code
         * can execute payments for this user in the future.
         *
         * A more complete example that includes the required app-server to
         * PayPal-server integration is available from
         * https://github.com/paypal/rest-api-sdk-python/tree/master/samples/mobile_backend
         */

    }
    /*paypal integration code Exit*/
}
