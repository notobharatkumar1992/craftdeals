package com.craftedeals.activities;

import android.app.Dialog;
import android.content.Intent;
import android.graphics.PorterDuff;
import android.graphics.drawable.ColorDrawable;
import android.graphics.drawable.Drawable;
import android.os.Build;
import android.os.Bundle;
import android.os.Handler;
import android.os.Message;
import android.support.v4.app.ActivityOptionsCompat;
import android.support.v4.content.ContextCompat;
import android.support.v4.graphics.drawable.DrawableCompat;
import android.support.v4.util.Pair;
import android.support.v7.app.AppCompatActivity;
import android.view.View;
import android.view.Window;
import android.view.WindowManager;
import android.widget.SeekBar;

import com.craftedeals.AppDelegate;
import com.craftedeals.Async.PostAsync;
import com.craftedeals.Models.PostAysnc_Model;
import com.craftedeals.Models.UserDataModel;
import com.craftedeals.R;
import com.craftedeals.Utils.Prefs;
import com.craftedeals.Utils.TransitionHelper;
import com.craftedeals.constants.ServerRequestConstants;
import com.craftedeals.constants.Tags;
import com.craftedeals.interfaces.OnReciveServerResponse;

import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;

import carbon.widget.RelativeLayout;
import carbon.widget.TextView;

public class SettingActivity extends AppCompatActivity implements View.OnClickListener, OnReciveServerResponse {

    public Handler mHandler;
    public TextView txt_c_range;
    public UserDataModel userDataModel;
    private RelativeLayout rl_c_change_password, rl_c_upgrade_membership;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.LOLLIPOP) {
            Window window = this.getWindow();
            Drawable background = this.getResources().getDrawable(R.drawable.topbg);
            window.addFlags(WindowManager.LayoutParams.FLAG_DRAWS_SYSTEM_BAR_BACKGROUNDS);
            window.setStatusBarColor(getResources().getColor(android.R.color.transparent));
            window.setNavigationBarColor(getResources().getColor(android.R.color.transparent));
            window.setBackgroundDrawable(background);
        }
        setContentView(R.layout.setting);
        userDataModel = new Prefs(this).getUserdata();
        int_seek_range = userDataModel.user_range;
        initView();
        setValues();
        setHandler();
    }

    public void initView() {
        txt_c_range = (TextView) findViewById(R.id.txt_c_range);
        rl_c_change_password = (RelativeLayout) findViewById(R.id.rl_c_change_password);
        rl_c_upgrade_membership = (RelativeLayout) findViewById(R.id.rl_c_upgrade_membership);
        txt_c_range.setOnClickListener(this);
        findViewById(R.id.img_c_back).setOnClickListener(this);
        findViewById(R.id.txt_c_change_password).setOnClickListener(this);
        findViewById(R.id.txt_c_upgrade).setOnClickListener(this);
    }

    public void setValues() {
//        if (AppDelegate.isValidString(new Prefs(SettingActivity.this).getStringValuefromTemp(Tags.social_id, ""))) {
//            rl_c_change_password.setVisibility(View.GONE);
//        } else {
//            rl_c_change_password.setVisibility(View.VISIBLE);
//        }
        if (AppDelegate.isValidString(new Prefs(SettingActivity.this).getUserdata().social_id)) {
            rl_c_change_password.setVisibility(View.GONE);
        } else {
            rl_c_change_password.setVisibility(View.VISIBLE);
        }
       /* if (new Prefs(this).getUserdata() != null) {
            if (new Prefs(this).getUserdata().membership_id == 2) {
                rl_c_upgrade_membership.setVisibility(View.GONE);
            } else {
                rl_c_upgrade_membership.setVisibility(View.VISIBLE);
            }
        } else {
            rl_c_upgrade_membership.setVisibility(View.GONE);
        }*/
    }

    @Override
    protected void onResume() {
        super.onResume();
        setValues();
    }

    private void setHandler() {
        mHandler = new Handler() {
            @Override
            public void dispatchMessage(Message msg) {
                super.dispatchMessage(msg);
                switch (msg.what) {
                    case 10:
                        AppDelegate.showProgressDialog(SettingActivity.this);
                        break;
                    case 11:
                        AppDelegate.hideProgressDialog(SettingActivity.this);
                        break;
                }
            }
        };
    }

    public Intent intent;

    @Override
    public void onClick(View v) {
        switch (v.getId()) {
            case R.id.img_c_back:
                onBackPressed();
                break;
            case R.id.txt_c_range:
                showRangeDialog();
                break;
            case R.id.txt_c_change_password: {
                intent = new Intent(SettingActivity.this, ChangePasswordActivity.class);
                final Pair<View, String>[] pairs = TransitionHelper.createSafeTransitionParticipants(SettingActivity.this, false, new Pair<>(findViewById(R.id.txt_c_change_password_ph), "change_password"));
                ActivityOptionsCompat transitionActivityOptions = ActivityOptionsCompat.makeSceneTransitionAnimation(SettingActivity.this, pairs);
                startActivity(intent, transitionActivityOptions.toBundle());
                break;
            }
            case R.id.txt_c_upgrade: {
                intent = new Intent(SettingActivity.this, UpgradeMembershipActivity.class);
                final Pair<View, String>[] pairs = TransitionHelper.createSafeTransitionParticipants(SettingActivity.this, false, new Pair<>(findViewById(R.id.txt_c_upgrade_ph), "upgrade_membership"));
                ActivityOptionsCompat transitionActivityOptions = ActivityOptionsCompat.makeSceneTransitionAnimation(SettingActivity.this, pairs);
                startActivity(intent, transitionActivityOptions.toBundle());
                break;
            }
        }
    }

    private void showRangeDialog() {
        final Dialog rangeDialog = new Dialog(this, android.R.style.Theme_Light);
        rangeDialog.requestWindowFeature(Window.FEATURE_NO_TITLE);
        rangeDialog.getWindow().setBackgroundDrawable(new ColorDrawable(getResources().getColor(android.R.color.transparent)));
        rangeDialog.setContentView(R.layout.dialog_range);

        final TextView txt_c_range = (TextView) rangeDialog.findViewById(R.id.txt_c_range);
        txt_c_range.setText(int_seek_range + "");
        SeekBar sb_range = (SeekBar) rangeDialog.findViewById(R.id.sb_range);
        try {
            if (Build.VERSION.SDK_INT < Build.VERSION_CODES.LOLLIPOP) {

                Drawable wrapDrawable = DrawableCompat.wrap(sb_range.getIndeterminateDrawable());
                DrawableCompat.setTint(wrapDrawable, ContextCompat.getColor(this, R.color.ripple_color));
                sb_range.setIndeterminateDrawable(DrawableCompat.unwrap(wrapDrawable));
            } else {
                sb_range.getIndeterminateDrawable().setColorFilter(ContextCompat.getColor(this, R.color.ripple_color), PorterDuff.Mode.SRC_IN);
            }
        } catch (Exception e) {
            AppDelegate.LogE(e);
        }
        final int MIN_VALUE = 1;
        sb_range.setOnSeekBarChangeListener(new SeekBar.OnSeekBarChangeListener() {
            public void onProgressChanged(SeekBar seekBar, int progress, boolean fromUser) {
                AppDelegate.LogT("setOnSeekBarChangeListener progress => " + progress);
                if (progress < MIN_VALUE) {
                /* if seek bar value is lesser than min value then set min value to seek bar */
                    seekBar.setProgress(MIN_VALUE);
                    int_seek_range = 1;
                    txt_c_range.setText(int_seek_range + "");
                    AppDelegate.LogT("setOnSeekBarChangeListener progress after  => " + progress);
                } else {
                    int_seek_range = progress;
                    txt_c_range.setText(int_seek_range + "");
                }
            }

            public void onStartTrackingTouch(SeekBar arg0) {
                //do something
                txt_c_range.setText(int_seek_range + "");
            }

            public void onStopTrackingTouch(SeekBar seekBar) {
                //do something
                txt_c_range.setText(int_seek_range + "");
            }

        });
        sb_range.setProgress(int_seek_range);

        rangeDialog.findViewById(R.id.txt_c_submit).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                execute_updateRangeApi(int_seek_range + "");
                rangeDialog.dismiss();
            }
        });

        rangeDialog.findViewById(R.id.txt_c_cancel).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                rangeDialog.dismiss();
            }
        });
        rangeDialog.show();
    }

    private void execute_updateRangeApi(String str_selected_range) {
        try {
            if (AppDelegate.haveNetworkConnection(this, true)) {
                ArrayList<PostAysnc_Model> mPostArrayList = new ArrayList<PostAysnc_Model>();
                PostAsync mPostAsyncObj;
                AppDelegate.getInstance(this).setPostParamsSecond(mPostArrayList, Tags.API_KEY, Tags.API_KEY_VALUE);
                AppDelegate.getInstance(this).setPostParamsSecond(mPostArrayList, Tags.user_id, new Prefs(this).getUserdata().id);
                AppDelegate.getInstance(this).setPostParamsSecond(mPostArrayList, Tags.user_range, str_selected_range);

                mPostAsyncObj = new PostAsync(this, this, ServerRequestConstants.UPDATE_RANGE,
                        mPostArrayList, null);
                mHandler.sendEmptyMessage(10);
                mPostAsyncObj.execute();
            }
        } catch (Exception e) {
            AppDelegate.LogE(e);
            AppDelegate.showToast(this, getString(R.string.try_again));
        }
    }

    @Override
    public void setOnReciveResult(String apiName, String result) {
        mHandler.sendEmptyMessage(11);
        if (!AppDelegate.isValidString(result)) {
            AppDelegate.showToast(this, getString(R.string.time_out));
            return;
        }
        if (apiName.equalsIgnoreCase(ServerRequestConstants.UPDATE_RANGE)) {
            parseUpdateRange(result);
        }
    }

    private void parseUpdateRange(String result) {
        try {
            JSONObject jsonObject = new JSONObject(result);
            if (jsonObject.getInt(Tags.status) == 1) {
                AppDelegate.showToast(this, jsonObject.getString(Tags.message));
                UserDataModel userDataModel = new Prefs(this).getUserdata();
                userDataModel.user_range = int_seek_range;
                new Prefs(this).setUserData(userDataModel);
                // finish();
            } else {
                AppDelegate.showToast(this, jsonObject.getString(Tags.message));
            }
        } catch (JSONException e) {
            AppDelegate.LogE(e);
        }

    }

    public int int_seek_range = 10;
}
