package com.craftedeals.activities;

import android.app.Activity;
import android.app.DatePickerDialog;
import android.app.Dialog;
import android.content.ContentResolver;
import android.content.Context;
import android.content.Intent;
import android.database.Cursor;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.graphics.Color;
import android.graphics.Matrix;
import android.graphics.Typeface;
import android.graphics.drawable.AnimationDrawable;
import android.graphics.drawable.ColorDrawable;
import android.graphics.drawable.Drawable;
import android.media.ExifInterface;
import android.net.Uri;
import android.os.AsyncTask;
import android.os.Build;
import android.os.Bundle;
import android.os.Environment;
import android.os.Handler;
import android.os.Message;
import android.provider.MediaStore;
import android.support.annotation.NonNull;
import android.support.v4.app.FragmentActivity;
import android.support.v7.app.AlertDialog;
import android.support.v7.app.AppCompatActivity;
import android.text.Editable;
import android.text.TextWatcher;
import android.util.Log;
import android.view.KeyEvent;
import android.view.View;
import android.view.Window;
import android.view.WindowManager;
import android.view.inputmethod.EditorInfo;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.DatePicker;
import android.widget.EditText;
import android.widget.ListView;
import android.widget.Toast;

import com.craftedeals.AppDelegate;
import com.craftedeals.Async.PostAsync;
import com.craftedeals.Models.FavBeveragesModel;
import com.craftedeals.Models.PostAysnc_Model;
import com.craftedeals.Models.UserDataModel;
import com.craftedeals.R;
import com.craftedeals.Utils.CircleImageView;
import com.craftedeals.Utils.Prefs;
import com.craftedeals.adapters.AlertAdapter;
import com.craftedeals.adapters.CityAdapterSearch;
import com.craftedeals.constants.ServerRequestConstants;
import com.craftedeals.constants.Tags;
import com.craftedeals.interfaces.OnPictureResult;
import com.craftedeals.interfaces.OnReciveServerResponse;
import com.craftedeals.parser.JSONParser;
import com.nostra13.universalimageloader.core.DisplayImageOptions;
import com.nostra13.universalimageloader.core.ImageLoaderConfiguration;
import com.nostra13.universalimageloader.core.assist.FailReason;
import com.nostra13.universalimageloader.core.listener.ImageLoadingListener;
import com.yalantis.ucrop.UCrop;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.io.File;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;

import carbon.widget.ImageView;
import carbon.widget.RelativeLayout;
import carbon.widget.TextView;

public class EditProfileActivity extends AppCompatActivity implements View.OnClickListener, OnReciveServerResponse, OnPictureResult {

    public static Activity mActivity;
    private EditText et_fav_brewery, et_username, et_lastname, et_password, et_email, et_confirm_password, et_contact_no, et_address;
    public Handler mHandler;
    private Prefs prefs;
    ArrayList<FavBeveragesModel> favBeerArraylist = new ArrayList<>();
    ArrayList<FavBeveragesModel> favBreweryArrayList = new ArrayList<>();
    ArrayList<FavBeveragesModel> cityArray = new ArrayList<>();
    ArrayList<FavBeveragesModel> provinceArray = new ArrayList<>();
    private CircleImageView cimg_user;
    private FavBeveragesModel favBeerModel, favBreweryModel;
    // Spinner spinner_favbrewery, spinner_favbeer, spinner_province, spinner_city;
    ImageView img_c_loading1;
    private int mYear, mMonth, mDay;
    TextView txt_c_dob;
    private Dialog dialogue;
    private AlertAdapter breweryAdapter, beerAdapter, heard_about_us_Adapter;
    CityAdapterSearch provinceAdapter, cityAdpter;
    private TextView txt_c_heard_about_us, txt_c_search_dialog_title, txt_c_dialog_title, txt_c_province, txt_c_city, txt_c_fav_beer, txt_c_title, txt_c_sign_up;
    private ListView list_view;
    public com.nostra13.universalimageloader.core.ImageLoader imageLoader = com.nostra13.universalimageloader.core.ImageLoader.getInstance();
    public DisplayImageOptions options = new DisplayImageOptions.Builder().cacheInMemory(true)
            .cacheOnDisc(true).resetViewBeforeLoading(true).
                    build();
    RelativeLayout rl_c_password, rl_c_confirm_password;
    private UserDataModel userDataModel;
    private EditText et_search;
    public int province_id = 0, city_id = 0, beer_id = 0, brewery_id = 0;
    public String last_selected_date = null;
    ArrayList<FavBeveragesModel> heard_about_us_array = new ArrayList<>();

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.LOLLIPOP) {
            Window window = this.getWindow();
            Drawable background = this.getResources().getDrawable(R.drawable.topbg);
            window.addFlags(WindowManager.LayoutParams.FLAG_DRAWS_SYSTEM_BAR_BACKGROUNDS);
            window.setStatusBarColor(getResources().getColor(android.R.color.transparent));
            window.setNavigationBarColor(getResources().getColor(android.R.color.transparent));
            window.setBackgroundDrawable(background);
        }
        setContentView(R.layout.sign_up);
        setheard_about_us();

        imageLoader.init(ImageLoaderConfiguration.createDefault(this));
        mActivity = this;
        prefs = new Prefs(this);
        setHandler();
        initView();
        mHandler.sendEmptyMessage(3);
    }

    private void setheard_about_us() {
        FavBeveragesModel favBeveragesModel = new FavBeveragesModel();
        favBeveragesModel.name = getString(R.string.Facebook);
        heard_about_us_array.add(favBeveragesModel);
        favBeveragesModel = new FavBeveragesModel();
        favBeveragesModel.name = getString(R.string.Google);
        heard_about_us_array.add(favBeveragesModel);
        favBeveragesModel = new FavBeveragesModel();
        favBeveragesModel.name = getString(R.string.Instagram);
        heard_about_us_array.add(favBeveragesModel);
        favBeveragesModel = new FavBeveragesModel();
        favBeveragesModel.name = getString(R.string.WordofMouth);
        heard_about_us_array.add(favBeveragesModel);
        favBeveragesModel = new FavBeveragesModel();
        favBeveragesModel.name = getString(R.string.FrothMagazine);
        heard_about_us_array.add(favBeveragesModel);
        favBeveragesModel = new FavBeveragesModel();
        favBeveragesModel.name = getString(R.string.OtherPublication);
        heard_about_us_array.add(favBeveragesModel);
    }

    @Override
    protected void onDestroy() {
        super.onDestroy();
        mActivity = null;
    }

    private void showDateDialog() {
        Calendar calendar = Calendar.getInstance();
        if (AppDelegate.isValidString(last_selected_date)) {

            String parts[] = last_selected_date.split("/");
            int day = Integer.parseInt(parts[0]);
            int month = Integer.parseInt(parts[1]);
            int year = Integer.parseInt(parts[2]);

            calendar.set(Calendar.YEAR, year);
            calendar.set(Calendar.MONTH, month);
            calendar.add(Calendar.MONTH, -1);
            calendar.set(Calendar.DAY_OF_MONTH, day);
            mYear = calendar.get(Calendar.YEAR);
            mMonth = calendar.get(Calendar.MONTH);
            mDay = calendar.get(Calendar.DAY_OF_MONTH);
        } else {
            calendar = Calendar.getInstance();
            mYear = calendar.get(Calendar.YEAR);
            mMonth = calendar.get(Calendar.MONTH);
            mDay = calendar.get(Calendar.DAY_OF_MONTH);

        }

        DatePickerDialog dpd = new DatePickerDialog(this,
                new DatePickerDialog.OnDateSetListener() {
                    @Override
                    public void onDateSet(DatePicker view, int year, int monthOfYear, int dayOfMonth) {
                        // txt_c_dob.setText((monthOfYear + 1) + "-" + dayOfMonth + "-" + year);
                        txt_c_dob.setText(dayOfMonth + "/" + (monthOfYear + 1) + "/" + year);
                        txt_c_dob.setError(null);
                        txt_c_dob.clearFocus();
                        last_selected_date = txt_c_dob.getText().toString();
                        //  if (!AppDelegate.isValidString(txt_c_fav_beer.getText().toString()))
                        new Handler().postDelayed(new Runnable() {
                            @Override
                            public void run() {

                                txt_c_province.performClick();
                            }
                        }, 300);
                    }
                }, mYear, mMonth, mDay);
        Calendar c = Calendar.getInstance();
        c.get(Calendar.YEAR);
        c.add(Calendar.YEAR, -18);
        c.set(Calendar.YEAR, c.get(Calendar.YEAR));
        c.set(Calendar.MONTH, c.get(Calendar.MONTH));
        c.set(Calendar.DAY_OF_MONTH, c.get(Calendar.DAY_OF_MONTH));
        dpd.getDatePicker().setMaxDate(c.getTimeInMillis()/*c.getTimeInMillis()*//*System.currentTimeMillis() - 10000*/);
        dpd.show();
    }

    private void initView() {
        img_c_loading1 = (ImageView) findViewById(R.id.img_c_loading1);
        cimg_user = (CircleImageView) findViewById(R.id.cimg_user);
        cimg_user.setOnClickListener(this);
        et_fav_brewery = (EditText) findViewById(R.id.et_fav_brewery);
        et_fav_brewery.setTypeface(Typeface.createFromAsset(getAssets(), getString(R.string.font_regular)));
        et_username = (EditText) findViewById(R.id.et_username);
        et_username.setTypeface(Typeface.createFromAsset(getAssets(), getString(R.string.font_regular)));
        et_lastname = (EditText) findViewById(R.id.et_lastname);
        et_lastname.setTypeface(Typeface.createFromAsset(getAssets(), getString(R.string.font_regular)));
        et_password = (EditText) findViewById(R.id.et_password);
        et_password.setTypeface(Typeface.createFromAsset(getAssets(), getString(R.string.font_regular)));
        et_email = (EditText) findViewById(R.id.et_email);
        et_email.setTypeface(Typeface.createFromAsset(getAssets(), getString(R.string.font_regular)));
        et_confirm_password = (EditText) findViewById(R.id.et_confirm_password);
        et_confirm_password.setTypeface(Typeface.createFromAsset(getAssets(), getString(R.string.font_regular)));
        txt_c_title = (TextView) findViewById(R.id.txt_c_title);
        txt_c_title.setText(getResources().getString(R.string.edit_profile));
        txt_c_sign_up = (TextView) findViewById(R.id.txt_c_sign_up);
        txt_c_sign_up.setOnClickListener(this);
        txt_c_sign_up.setText(getResources().getString(R.string.submit));
        txt_c_heard_about_us = (TextView) findViewById(R.id.txt_c_heard_about_us);
        txt_c_heard_about_us.setOnClickListener(this);
        txt_c_heard_about_us.setVisibility(View.GONE);
        findViewById(R.id.img_back).setOnClickListener(this);
        txt_c_fav_beer = (TextView) findViewById(R.id.txt_c_fav_beer);
        txt_c_city = (TextView) findViewById(R.id.txt_c_city);
        txt_c_province = (TextView) findViewById(R.id.txt_c_province);
        txt_c_dob = (TextView) findViewById(R.id.txt_c_dob);
        txt_c_fav_beer.setOnClickListener(this);
        txt_c_city.setOnClickListener(this);
        txt_c_province.setOnClickListener(this);
        txt_c_dob.setOnClickListener(this);
        et_contact_no = (EditText) findViewById(R.id.et_contact_no);
        et_contact_no.setTypeface(Typeface.createFromAsset(getAssets(), getString(R.string.font_regular)));
        et_address = (EditText) findViewById(R.id.et_address);
        et_address.setTypeface(Typeface.createFromAsset(getAssets(), getString(R.string.font_regular)));
        rl_c_password = (RelativeLayout) findViewById(R.id.rl_c_password);
        rl_c_confirm_password = (RelativeLayout) findViewById(R.id.rl_c_confirm_password);
        rl_c_confirm_password.setVisibility(View.GONE);
        rl_c_password.setVisibility(View.GONE);
        et_address.setImeOptions(EditorInfo.IME_ACTION_DONE);
        // et_address.setCursorVisible(false);
        et_fav_brewery.setOnEditorActionListener(new android.widget.TextView.OnEditorActionListener() {
            @Override
            public boolean onEditorAction(android.widget.TextView v, int actionId, KeyEvent event) {
                if (actionId == EditorInfo.IME_ACTION_NEXT) {
                    // Creating uri to be opened
                    AppDelegate.hideKeyBoard(EditProfileActivity.this);
                    //    if (!AppDelegate.isValidString(txt_c_dob.getText().toString()))
//                    txt_c_dob.performClick();
                }
                return false;
            }
        });
        et_address.setOnEditorActionListener(new android.widget.TextView.OnEditorActionListener() {
            @Override
            public boolean onEditorAction(android.widget.TextView v, int actionId, KeyEvent event) {
                if (actionId == EditorInfo.IME_ACTION_DONE) {
                    // Creating uri to be opened
                    et_address.setCursorVisible(false);
                }
                return false;
            }
        });
    }

    private boolean isLoaded = false;

    void setValues() {
        userDataModel = new Prefs(this).getUserdata();
        if (userDataModel != null && AppDelegate.isValidString(userDataModel.image)) {
            img_c_loading1.setVisibility(View.VISIBLE);
            AnimationDrawable frameAnimation = (AnimationDrawable) img_c_loading1.getDrawable();
            frameAnimation.setCallback(img_c_loading1);
            frameAnimation.setVisible(true, true);
            frameAnimation.start();

            imageLoader.loadImage(new Prefs(this).getUserdata().image, options, new ImageLoadingListener() {
                @Override
                public void onLoadingStarted(String imageUri, View view) {
                }

                @Override
                public void onLoadingFailed(String imageUri, View view, FailReason failReason) {
//                    AppDelegate.LogE(" onLoadingFailed");
//                    cimg_user.setImageDrawable(getResources().getDrawable(R.drawable.user_noimage));
                    img_c_loading1.setVisibility(View.GONE);
                    isLoaded = true;
                }

                @Override
                public void onLoadingComplete(String imageUri, View view, Bitmap bitmap) {
                    AppDelegate.LogT(" onBitmapLoaded");
                    cimg_user.setImageBitmap(bitmap);
                    img_c_loading1.setVisibility(View.GONE);
                    isLoaded = true;
                }

                @Override
                public void onLoadingCancelled(String imageUri, View view) {
                    isLoaded = true;
                    img_c_loading1.setVisibility(View.GONE);
                }
            });
        } else {
            cimg_user.setImageDrawable(getResources().getDrawable(R.drawable.logo));
        }
        if (userDataModel != null) {
            try {
                if (AppDelegate.isValidString(userDataModel.state_id)) {
                    province_id = Integer.parseInt(userDataModel.state_id);
                }
                if (AppDelegate.isValidString(userDataModel.city_id)) {
                    city_id = Integer.parseInt(userDataModel.city_id);
                }
//                beer_id = (userDataModel.beer_id);
//                brewery_id = (userDataModel.brewery_id);

            } catch (Exception e) {
                AppDelegate.LogE(e);
            }
            et_username.setText(userDataModel.name);
            et_lastname.setText(userDataModel.last_names);
            et_address.setText(userDataModel.address);
            et_email.setText(userDataModel.email);
            et_contact_no.setText(userDataModel.contact_number);
            et_fav_brewery.setText(userDataModel.fav_brewery);
            txt_c_fav_beer.setText(userDataModel.fav_beer);
            txt_c_province.setText(userDataModel.province);
            txt_c_city.setText(userDataModel.city);
            txt_c_heard_about_us.setText(userDataModel.heard_about_us + "");
            String starting_date = userDataModel.dob;
            try {
                if (AppDelegate.isValidString(starting_date)) {
                    starting_date = starting_date.replaceAll("/+0000", "");
                    starting_date = new SimpleDateFormat("dd/MM/yyyy").format(new SimpleDateFormat("yyyy-MM-dd'T'HH:mm:ss").parse(starting_date));
                    txt_c_dob.setText(starting_date);
                    last_selected_date = txt_c_dob.getText().toString();
                }
            } catch (Exception e) {
                AppDelegate.LogE(e);
            }
        }
    }

    private void setHandler() {
        mHandler = new Handler() {
            @Override
            public void dispatchMessage(Message msg) {
                super.dispatchMessage(msg);
                switch (msg.what) {
                    case 10:
                        AppDelegate.showProgressDialog(EditProfileActivity.this);
                        break;
                    case 11:
                        AppDelegate.hideProgressDialog(EditProfileActivity.this);
                        break;
                    case 1:
                        execute_getBeveragesApi();
                        break;
                    case 2:
                        if (clicked == brewery_clicked) {
                            if (favBreweryArrayList != null && favBreweryArrayList.size() > 0) {

                            }
//                                breweryDialog(favBreweryArrayList);
                        } else if (clicked == beer_clicked) {
                            if (favBreweryArrayList != null && favBreweryArrayList.size() > 0)
                                beerDialog(favBeerArraylist);
                        }
                        break;
                    case 3:
                        setValues();
                        break;
                    case 5:
                        if (provinceArray != null && provinceArray.size() > 0)
                            provinceDialog(provinceArray);
                        break;
                    case 6:
                        AppDelegate.LogT("city array ==>" + cityArray);
                        if (cityArray != null && cityArray.size() > 0)
                            cityDialog(cityArray);
                        break;
                    case 7:
                        dialog();
                    case 8:
                        execute_getMyProfile();
                        break;
                }
            }
        };
    }

    private void execute_getMyProfile() {
        try {
            if (AppDelegate.haveNetworkConnection(EditProfileActivity.this, true)) {
                ArrayList<PostAysnc_Model> mPostArrayList = new ArrayList<PostAysnc_Model>();
                AppDelegate.getInstance(EditProfileActivity.this).setPostParamsSecond(mPostArrayList, Tags.API_KEY, Tags.API_KEY_VALUE);
                AppDelegate.getInstance(EditProfileActivity.this).setPostParamsSecond(mPostArrayList, Tags.user_id, new Prefs(this).getUserdata().id, ServerRequestConstants.Key_PostintValue);

                PostAsync mPostAsyncObj;
                mPostAsyncObj = new PostAsync(EditProfileActivity.this, this, ServerRequestConstants.GET_MYPROFILE, mPostArrayList, null);
                AppDelegate.showProgressDialog(EditProfileActivity.this);
                mPostAsyncObj.execute();
            }
        } catch (Exception e) {
            AppDelegate.showToast(EditProfileActivity.this, getResources().getString(R.string.try_again));
        }
    }

    private void execute_getBeveragesApi() {
        try {
            if (AppDelegate.haveNetworkConnection(this, true)) {
                ArrayList<PostAysnc_Model> mPostArrayList = new ArrayList<PostAysnc_Model>();
                PostAsync mPostAsyncObj;
                AppDelegate.getInstance(this).setPostParamsSecond(mPostArrayList, Tags.API_KEY, Tags.API_KEY_VALUE);
                mPostAsyncObj = new PostAsync(this, this, ServerRequestConstants.GET_BEVERAGES,
                        mPostArrayList, null);
                mHandler.sendEmptyMessage(10);
                mPostAsyncObj.execute();
            }
        } catch (Exception e) {
            AppDelegate.LogE(e);
        }
    }

    int clicked = 0;
    final int beer_clicked = 1, brewery_clicked = 2;


    @Override
    public void onClick(View v) {
        switch (v.getId()) {
            case R.id.cimg_user:
                showImageSelectorList();
                break;
            case R.id.txt_c_sign_up:
                chk_validation();
                break;
            case R.id.img_back:
                onBackPressed();
                break;
            case R.id.txt_c_dob:
                showDateDialog();
                break;
            case R.id.txt_c_fav_beer:
                if (favBeerArraylist.size() == 0) {
                    clicked = beer_clicked;
                    execute_getBeveragesApi();
                } else {
                    clicked = 0;
                    beerDialog(favBeerArraylist);
                }
                break;
            case R.id.txt_c_province:
                if (provinceArray.size() == 0) {
                    execute_getProvince();
                } else {
                    provinceDialog(provinceArray);
                }
                break;
            case R.id.txt_c_city:
                if (cityArray.size() == 0) {
                    execute_getCityList();
                } else {
                    cityDialog(cityArray);
                }
                break;
            case R.id.txt_c_heard_about_us:
                AppDelegate.LogT("city clicked");
                if (heard_about_us_array != null && heard_about_us_array.size() > 0)
                    heard_about_us_Dialog(heard_about_us_array);
                break;

        }
    }

    private void chk_validation() {
        if (!AppDelegate.isValidString(et_username.getText().toString())) {
            et_username.setError(getResources().getString(R.string.forname));
            et_username.requestFocus();
//            AppDelegate.showToast(this, getResources().getString(R.string.forname));
        } else if (!AppDelegate.isValidString(et_lastname.getText().toString())) {
            et_lastname.setError(getResources().getString(R.string.forlastname));
            et_lastname.requestFocus();
//            AppDelegate.showToast(this, getResources().getString(R.string.forlastname));
        } else if (!AppDelegate.CheckEmail(et_email.getText().toString())) {
            et_email.setError(getResources().getString(R.string.foremail));
            et_email.requestFocus();
//            AppDelegate.showToast(this, getResources().getString(R.string.foremail));
//        } else if (!AppDelegate.isValidString(et_contact_no.getText().toString())) {
//            et_contact_no.setError(getResources().getString(R.string.forcontact));
//            et_contact_no.requestFocus();
//            AppDelegate.showToast(this, getResources().getString(R.string.forcontact));
        } else if ((et_contact_no.getText().toString().length() < 10)) {
            et_contact_no.setError(getResources().getString(R.string.foracontact_length));
            et_contact_no.requestFocus();
        } else if (!AppDelegate.isValidString(et_fav_brewery.getText().toString())) {
            et_fav_brewery.setError(getResources().getString(R.string.Select_favBrewery));
            et_fav_brewery.requestFocus();
//            AppDelegate.showToast(this, getResources().getString(R.string.Select_favBrewery));
        } else if (!AppDelegate.isValidString(txt_c_dob.getText().toString())) {
            txt_c_dob.setError(getResources().getString(R.string.forbirth));
            txt_c_dob.requestFocus();
//            AppDelegate.showToast(this, getResources().getString(R.string.forbirth));
        }/* else if (!AppDelegate.isValidString(txt_c_fav_beer.getText().toString())) {
            AppDelegate.showToast(this, getResources().getString(R.string.select_favBeer));
        }*/ else if (!AppDelegate.isValidString(txt_c_province.getText().toString())) {

            txt_c_province.setError(getResources().getString(R.string.forprovince));
            txt_c_province.requestFocus();
//            AppDelegate.showToast(this, getResources().getString(R.string.forprovince));
        } else if (!AppDelegate.isValidString(txt_c_city.getText().toString())) {
            txt_c_city.setError(getResources().getString(R.string.forcity));
            txt_c_city.requestFocus();
//            AppDelegate.showToast(this, getResources().getString(R.string.forcity));
        }/* else if (!AppDelegate.isValidString(txt_c_heard_about_us.getText().toString())) {
            txt_c_heard_about_us.setError(getResources().getString(R.string.for_heard_about_us));
            txt_c_heard_about_us.requestFocus();
//            AppDelegate.showToast(this, getResources().getString(R.string.for_heard_about_us));
        } *//* else if (!AppDelegate.isValidString(et_address.getText().toString())) {
            AppDelegate.showToast(this, getResources().getString(R.string.foraddress));
        } */ else if (AppDelegate.haveNetworkConnection(this, false)) {
            executeEditProfile();
        } else {
            AppDelegate.showToast(this, getResources().getString(R.string.not_connected));
        }
    }

    @Override
    public void setOnReciveResult(String apiName, String result) {
        mHandler.sendEmptyMessage(11);
        if (!AppDelegate.isValidString(result)) {
            AppDelegate.showToast(this, getResources().getString(R.string.time_out));
            return;
        }
        if (apiName.equalsIgnoreCase(ServerRequestConstants.EDIT_PROFILE)) {
            parseProfile(result);
            capturedFile = null;
        } else if (apiName.equalsIgnoreCase(ServerRequestConstants.GET_BEVERAGES)) {
            parseBeverages(result);
        } else if (apiName.equalsIgnoreCase(ServerRequestConstants.GET_PROVINCE)) {
            parseProvince(result);
        } else if (apiName.equalsIgnoreCase(ServerRequestConstants.GET_CITY)) {
            parseCity(result);
        } else if (apiName.equalsIgnoreCase(ServerRequestConstants.GET_MYPROFILE)) {
            parseProfile(result);
        }
    }

    private void parseProfile(String result) {
        try {
            JSONObject jsonObject = new JSONObject(result);

            if (jsonObject.getString(Tags.status).equalsIgnoreCase("1")) {
                AppDelegate.showToast(this, jsonObject.getString(Tags.message));
                //  AppDelegate.showToast(EditProfileActivity.this, jsonObject.getString(Tags.message));
                JSONObject object = jsonObject.getJSONObject(Tags.response);
                userDataModel = new UserDataModel();
                userDataModel.id = JSONParser.getInt(object, Tags.id);
                userDataModel.name = JSONParser.getString(object, Tags.name);
                userDataModel.first_names = JSONParser.getString(object, Tags.name);
                userDataModel.last_names = JSONParser.getString(object, Tags.last_name);
                userDataModel.email = JSONParser.getString(object, Tags.email);
                userDataModel.contact_number = JSONParser.getString(object, Tags.contact_number);
                userDataModel.membership_id = JSONParser.getInt(object, Tags.membership_id);
                userDataModel.image = JSONParser.getString(object, Tags.image);
                userDataModel.device_id = JSONParser.getString(object, Tags.device_id);
                userDataModel.device_type = JSONParser.getInt(object, Tags.device_type);
                userDataModel.created = JSONParser.getString(object, Tags.created);
                userDataModel.address = JSONParser.getString(object, Tags.address);
                userDataModel.total_purchase = JSONParser.getInt(object, Tags.total_purchase);
                userDataModel.total_redeemed = JSONParser.getInt(object, Tags.total_redeemed);
                userDataModel.favourite = JSONParser.getInt(object, Tags.favourite);
                userDataModel.social_id = JSONParser.getString(object, Tags.social_id);
                userDataModel.fav_brewery = JSONParser.getString(object, Tags.brewery_type_id);
                if (object.has(Tags.heard_about_us)) {
                    userDataModel.heard_about_us = JSONParser.getString(object, Tags.heard_about_us);
                }
                if (object.has(Tags.user_range)) {
                    userDataModel.user_range = JSONParser.getInt(object, Tags.user_range);
                }
                if (object.has(Tags.membership_start_date) && object.has(Tags.membership_end_date)) {
                    userDataModel.membership_start_date = JSONParser.getString(object, Tags.membership_start_date);
                    userDataModel.membership_end_date = JSONParser.getString(object, Tags.membership_end_date);
                }
                userDataModel.dob = JSONParser.getString(object, Tags.dob);
                JSONObject state = object.getJSONObject(Tags.state);
                userDataModel.province = JSONParser.getString(state, Tags.region_name);
                province_id = JSONParser.getInt(state, Tags.id);
                userDataModel.state_id = JSONParser.getInt(state, Tags.id) + "";
                JSONObject city = object.getJSONObject(Tags.city);
                userDataModel.city = JSONParser.getString(city, Tags.name);
                city_id = JSONParser.getInt(city, Tags.id);
                userDataModel.city_id = JSONParser.getInt(city, Tags.id) + "";
//                JSONObject beer = object.getJSONObject(Tags.beer_type);
//                userDataModel.fav_beer = JSONParser.getString(beer, Tags.name);
//                beer_id = JSONParser.getInt(beer, Tags.id);
//                userDataModel.beer_id = JSONParser.getInt(beer, Tags.id);
//                JSONObject brewery = object.getJSONObject(Tags.brewery_type);
//                userDataModel.fav_brewery = JSONParser.getString(brewery, Tags.name);
//                brewery_id = JSONParser.getInt(brewery, Tags.id);
//                userDataModel.brewery_id = JSONParser.getInt(brewery, Tags.id);
                new Prefs(this).setUserData(userDataModel);
                finish();
            } else {
                AppDelegate.showAlert(this, jsonObject.getString(Tags.message));
            }
        } catch (Exception e) {
            AppDelegate.LogE(e);
        }
    }

    private void parseProvince(String result) {
        try {
            JSONObject jsonObject = new JSONObject(result);
            if (jsonObject.getString(Tags.status).equalsIgnoreCase("1")) {
                JSONArray jsonArray = jsonObject.getJSONArray(Tags.response);
                FavBeveragesModel cityModel = new FavBeveragesModel();
                for (int i = 0; i < jsonArray.length(); i++) {
                    JSONObject object = jsonArray.getJSONObject(i);
                    cityModel = new FavBeveragesModel();
                    cityModel.id = (JSONParser.getInt(object, Tags.id));
                    cityModel.name = JSONParser.getString(object, Tags.region_name);
                    provinceArray.add(cityModel);
                }
                mHandler.sendEmptyMessage(5);
            } else {
                AppDelegate.showAlert(EditProfileActivity.this, jsonObject.getString(Tags.message));
            }
        } catch (Exception e) {
            AppDelegate.LogE(e);
            AppDelegate.showToast(EditProfileActivity.this, getResources().getString(R.string.try_again));
        }
    }

    private void parseCity(String result) {
        try {
            JSONObject jsonObject = new JSONObject(result);
            if (jsonObject.getString(Tags.status).equalsIgnoreCase("1")) {
                JSONArray jsonArray = jsonObject.getJSONArray(Tags.response);
                FavBeveragesModel cityModel = new FavBeveragesModel();
                for (int i = 0; i < jsonArray.length(); i++) {
                    JSONObject object = jsonArray.getJSONObject(i);
                    cityModel = new FavBeveragesModel();
                    cityModel.id = (JSONParser.getInt(object, Tags.id));
                    cityModel.name = JSONParser.getString(object, Tags.name);
                    cityArray.add(cityModel);
                }
                mHandler.sendEmptyMessage(6);
            } else {
                AppDelegate.showAlert(EditProfileActivity.this, jsonObject.getString(Tags.message));
            }
        } catch (Exception e) {
            AppDelegate.LogE(e);
            AppDelegate.showToast(EditProfileActivity.this, getResources().getString(R.string.try_again));
        }
    }

    private void execute_getProvince() {
        try {
            if (AppDelegate.haveNetworkConnection(EditProfileActivity.this, true)) {
                ArrayList<PostAysnc_Model> mPostArrayList = new ArrayList<PostAysnc_Model>();
                AppDelegate.getInstance(EditProfileActivity.this).setPostParamsSecond(mPostArrayList, Tags.API_KEY, Tags.API_KEY_VALUE);
                PostAsync mPostAsyncObj;
                mPostAsyncObj = new PostAsync(EditProfileActivity.this, this, ServerRequestConstants.GET_PROVINCE, mPostArrayList, null);
                AppDelegate.showProgressDialog(EditProfileActivity.this);
                mPostAsyncObj.execute();
            }
        } catch (Exception e) {
            AppDelegate.showAlert(EditProfileActivity.this, getResources().getString(R.string.try_again), getResources().getString(R.string.alert));
        }
    }

    private void execute_getCityList() {
        try {
            if (AppDelegate.haveNetworkConnection(EditProfileActivity.this, true)) {
                ArrayList<PostAysnc_Model> mPostArrayList = new ArrayList<PostAysnc_Model>();
                AppDelegate.getInstance(EditProfileActivity.this).setPostParamsSecond(mPostArrayList, Tags.API_KEY, Tags.API_KEY_VALUE);
                AppDelegate.getInstance(EditProfileActivity.this).setPostParamsSecond(mPostArrayList, Tags.state_id, province_id);

                PostAsync mPostAsyncObj;
                mPostAsyncObj = new PostAsync(EditProfileActivity.this, this, ServerRequestConstants.GET_CITY, mPostArrayList, null);
                AppDelegate.showProgressDialog(EditProfileActivity.this);
                mPostAsyncObj.execute();
            }
        } catch (Exception e) {
            AppDelegate.showAlert(EditProfileActivity.this, getResources().getString(R.string.try_again), getResources().getString(R.string.alert));
        }
    }

    private void parseBeverages(String result) {
        try {
            JSONObject jsonObject = new JSONObject(result);
            if (jsonObject.getInt(Tags.status) == 1 && jsonObject.getInt(Tags.dataFlow) == 1) {
                JSONObject array = jsonObject.getJSONObject(Tags.response);
                JSONArray beerArray = array.getJSONArray(Tags.Beer);
                JSONArray breweryArray = array.getJSONArray(Tags.Brewery);
                for (int i = 0; i < beerArray.length(); i++) {
                    jsonObject = beerArray.getJSONObject(i);
                    favBeerModel = new FavBeveragesModel();
                    favBeerModel.id = JSONParser.getInt(jsonObject, Tags.id);
                    favBeerModel.name = JSONParser.getString(jsonObject, Tags.name);
                    favBeerModel.created = JSONParser.getString(jsonObject, Tags.created);
                    favBeerArraylist.add(favBeerModel);
                }
                for (int i = 0; i < breweryArray.length(); i++) {
                    jsonObject = breweryArray.getJSONObject(i);
                    favBreweryModel = new FavBeveragesModel();
                    favBreweryModel.id = JSONParser.getInt(jsonObject, Tags.id);
                    favBreweryModel.name = JSONParser.getString(jsonObject, Tags.name);
                    favBreweryModel.created = JSONParser.getString(jsonObject, Tags.created);
                    favBreweryArrayList.add(favBreweryModel);
                }
                mHandler.sendEmptyMessage(2);
            } else {
                AppDelegate.showToast(this, jsonObject.getString(Tags.message));
            }
        } catch (JSONException e) {
            AppDelegate.LogE(e);
        }
    }

    private void parseEditProfile(String result) {
        try {
            JSONObject jsonObject = new JSONObject(result);
            if (jsonObject.getInt(Tags.status) == 1 && jsonObject.getInt(Tags.dataFlow) == 0) {
                AppDelegate.showToast(this, jsonObject.getString(Tags.message));
                finish();
            } else {
                AppDelegate.showToast(this, jsonObject.getString(Tags.message));
            }
        } catch (JSONException e) {
            AppDelegate.LogE(e);
        }
    }

    /* image selection*/
    public static File capturedFile;
    public static Uri imageURI = null;

    public void showImageSelectorList() {
        AppDelegate.hideKeyBoard(EditProfileActivity.this);
        AlertDialog.Builder builder = new AlertDialog.Builder(EditProfileActivity.this);
        ListView modeList = new ListView(EditProfileActivity.this);
        String[] stringArray = new String[]{"  Camera", "  Gallery", "  Cancel"};
        ArrayAdapter<String> modeAdapter = new ArrayAdapter<>(EditProfileActivity.this, R.layout.spinner_simple_list_item, stringArray);
        modeList.setAdapter(modeAdapter);
        builder.setView(modeList);
        final Dialog dialog = builder.create();
        modeList.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView<?> adapterView, View view, int i, long l) {
                switch (i) {
                    case 0:
                        dialog.dismiss();
                        new OpenCamera().execute();
                        break;
                    case 1:
                        dialog.dismiss();
                        openGallery();
                        break;
                    case 2:
                        dialog.dismiss();
                        break;
                }
            }
        });
        dialog.show();
    }

    @Override
    public void setOnReceivePictureResult(String apiName, Uri picUri) {
        if (apiName.equalsIgnoreCase(Tags.PICTURE)) {
            try {
                Bitmap OriginalPhoto = MediaStore.Images.Media.getBitmap(getContentResolver(), picUri);
                OriginalPhoto = Bitmap.createScaledBitmap(OriginalPhoto, 240, 240, true);
                cimg_user.setImageBitmap(OriginalPhoto);
                if (!isLoaded) {
                    img_c_loading1.setVisibility(View.GONE);
                    imageLoader.cancelDisplayTask(cimg_user);
                }
            } catch (IOException e) {
                AppDelegate.LogE(e);
            }
        }
    }

    class OpenCamera extends AsyncTask<Void, Void, Void> {

        @Override
        protected void onPreExecute() {
            super.onPreExecute();
            mHandler.sendEmptyMessage(10);
        }

        @Override
        protected Void doInBackground(Void... params) {
            Intent intent = new Intent("android.media.action.IMAGE_CAPTURE");
            String str_file_path = getNewFile(EditProfileActivity.this);
            if (str_file_path == null ? true : str_file_path.length() > 0 ? false : true) {
                AppDelegate.showToast(EditProfileActivity.this, getResources().getString(R.string.file_not_created));
                return null;
            }
            imageURI = Uri.fromFile(capturedFile);
            intent.putExtra(MediaStore.EXTRA_OUTPUT, imageURI);
            startActivityForResult(intent, AppDelegate.CAPTURE_IMAGE_FULLSIZE_ACTIVITY_REQUEST_CODE);
            return null;
        }

        @Override
        protected void onPostExecute(Void aVoid) {
            super.onPostExecute(aVoid);
            mHandler.sendEmptyMessage(11);
        }
    }

    public static String getNewFile(Context mContext) {
        File directoryFile;
        if (AppDelegate.isSDcardAvailable()) {
            directoryFile = new File(Environment.getExternalStorageDirectory()
                    + "/" + mContext.getString(R.string.app_name));
        } else {
            directoryFile = mContext.getDir(mContext.getString(R.string.app_name), Context.MODE_PRIVATE);
        }
        if (directoryFile.exists() && directoryFile.isDirectory()
                || directoryFile.mkdirs()) {
            capturedFile = new File(directoryFile, "Image_" + System.currentTimeMillis()
                    + ".png");
            try {
                if (capturedFile.createNewFile()) {
                    AppDelegate.LogT("File created = " + capturedFile.getAbsolutePath());
                    return capturedFile.getAbsolutePath();
                }
            } catch (IOException e) {
                AppDelegate.LogE(e);
            }
        }
        AppDelegate.LogE("no file created.");
        return null;
    }

    public void openGallery() {
        Intent intent = new Intent();
        intent.setType("image/*");
        intent.setAction(Intent.ACTION_GET_CONTENT);
        intent.addCategory(Intent.CATEGORY_OPENABLE);
        startActivityForResult(Intent.createChooser(intent, "Select Picture"), AppDelegate.SELECT_PICTURE);
    }


    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        AppDelegate.LogT("onActivityResult MainActivity==>" + requestCode);
        if (resultCode == Activity.RESULT_OK) {
            if (requestCode == AppDelegate.SELECT_PICTURE) {
                final Uri selectedUri = data.getData();
                if (selectedUri != null) {
                    startCropActivity(EditProfileActivity.this, data.getData());
                } else {
                    Toast.makeText(EditProfileActivity.this, R.string.toast_cannot_retrieve_selected_image, Toast.LENGTH_SHORT).show();
                }
            } else if (requestCode == UCrop.REQUEST_CROP) {
                handleCropResult(data);
            } else if (requestCode == AppDelegate.CAPTURE_IMAGE_FULLSIZE_ACTIVITY_REQUEST_CODE) {
                if (imageURI != null) {
                    startCropActivity(EditProfileActivity.this, imageURI);
                } else {
                    Toast.makeText(this, R.string.toast_cannot_retrieve_selected_image, Toast.LENGTH_SHORT).show();
                }
            }
        }
    }

    private void handleCropResult(@NonNull Intent result) {
        final Uri resultUri = UCrop.getOutput(result);
        if (resultUri != null) {
            setOnReceivePictureResult(Tags.PICTURE, resultUri);
        } else {
            Toast.makeText(this, R.string.toast_cannot_retrieve_cropped_image, Toast.LENGTH_SHORT).show();
        }
    }

    @SuppressWarnings("ThrowableResultOfMethodCallIgnored")
    private void handleCropError(@NonNull Intent result) {
        final Throwable cropError = UCrop.getError(result);
        if (cropError != null) {
            Toast.makeText(this, cropError.getMessage(), Toast.LENGTH_LONG).show();
        } else {
            Toast.makeText(this, R.string.toast_unexpected_error, Toast.LENGTH_SHORT).show();
        }
    }

    private static final String SAMPLE_CROPPED_IMAGE_NAME = "SampleCropImage";

    public static void startCropActivity(FragmentActivity mActivity, Uri uri) {
        String destinationFileName = SAMPLE_CROPPED_IMAGE_NAME;
        destinationFileName += ".png";
        capturedFile = new File(mActivity.getCacheDir(), destinationFileName);
        UCrop uCrop = UCrop.of(uri, Uri.fromFile(capturedFile));
        uCrop = basisConfig(uCrop);
        uCrop = advancedConfig(uCrop);
        uCrop.start(mActivity);
    }

    public static boolean imageUriIsValid(FragmentActivity mActivity, Uri contentUri) {
        ContentResolver cr = mActivity.getContentResolver();
        String[] projection = {MediaStore.MediaColumns.DATA};
        Cursor cur = cr.query(contentUri, projection, null, null, null);
        if (cur != null) {
            if (cur.moveToFirst()) {
                String filePath = cur.getString(0);
                if (new File(filePath).exists()) {
                    // do something if it exists
                    cur.close();
                    return true;
                } else {
                    // File was not found
                    cur.close();
                    AppDelegate.showToast(mActivity, "File not found please try again later.");
                    return false;
                }
            } else {
                // Uri was ok but no entry found.
                AppDelegate.showToast(mActivity, "No image found please try again later.");
                return false;
            }
        } else {
            // content Uri was invalid or some other error occurred
            AppDelegate.showToast(mActivity, "Image path was invalid some exception occured at your device.");
            return false;
        }
    }

    /**
     * In most cases you need only to set crop aspect ration and max size for resulting image.
     *
     * @param uCrop - ucrop builder instance
     * @return - ucrop builder instance
     */
    public static UCrop basisConfig(@NonNull UCrop uCrop) {
        uCrop = uCrop.withAspectRatio(1, 1);
        return uCrop;
    }

    /**
     * Sometimes you want to adjust more options, it's done via {@link UCrop.Options} class.
     *
     * @param uCrop - ucrop builder instance
     * @return - ucrop builder instance
     */
    public static UCrop advancedConfig(@NonNull UCrop uCrop) {
        UCrop.Options options = new UCrop.Options();

        options.setCompressionFormat(Bitmap.CompressFormat.PNG);
        options.setCompressionQuality(00);

//        if (SellItemFragment.onPictureResult != null) {
//            options.setHideBottomControls(false);
//            options.setFreeStyleCropEnabled(false);
//        } else {
        options.setHideBottomControls(true);
        options.setFreeStyleCropEnabled(false);
//        }


        return uCrop.withOptions(options);
    }


    public static Bitmap rotateImage(Bitmap source, float angle) {
        Matrix matrix = new Matrix();
        matrix.postRotate(angle);
        return Bitmap.createBitmap(source, 0, 0, source.getWidth(), source.getHeight(), matrix, true);
    }

    public void writeImageFile(Bitmap bitmap, String filePath) {
        if (AppDelegate.isValidString(filePath)) {
            capturedFile = new File(filePath);
        } else {
            return;
        }
        FileOutputStream fOut = null;
        try {
            fOut = new FileOutputStream(capturedFile);
        } catch (FileNotFoundException e) {
            AppDelegate.LogE(e);
        }
        bitmap.compress(Bitmap.CompressFormat.PNG, 85, fOut);
        try {
            fOut.flush();
        } catch (IOException e) {
            AppDelegate.LogE(e);
        }
        try {
            fOut.close();
        } catch (IOException e) {
            AppDelegate.LogE(e);
        }
    }

    private Bitmap getBitmap(String path) {
        Uri uri = Uri.fromFile(new File(path));
        InputStream in = null;
        try {
            final int IMAGE_MAX_SIZE = 1200000; // 1.2MP
            in = getContentResolver().openInputStream(uri);

            // Decode image size
            BitmapFactory.Options o = new BitmapFactory.Options();
            o.inJustDecodeBounds = true;
            BitmapFactory.decodeStream(in, null, o);
            in.close();


            int scale = 1;
            while ((o.outWidth * o.outHeight) * (1 / Math.pow(scale, 2)) >
                    IMAGE_MAX_SIZE) {
                scale++;
            }
            Log.d("", "scale = " + scale + ", orig-width: " + o.outWidth + ", orig-height: " + o.outHeight);

            Bitmap b = null;
            in = getContentResolver().openInputStream(uri);
            if (scale > 1) {
                scale--;
                // scale to max possible inSampleSize that still yields an image
                // larger than target
                o = new BitmapFactory.Options();
                o.inSampleSize = scale;
                b = BitmapFactory.decodeStream(in, null, o);

                // resize to desired dimensions
                int height = b.getHeight();
                int width = b.getWidth();
                Log.d("", "1th scale operation dimenions - width: " + width + ", height: " + height);

                double y = Math.sqrt(IMAGE_MAX_SIZE
                        / (((double) width) / height));
                double x = (y / height) * width;

                Bitmap scaledBitmap = Bitmap.createScaledBitmap(b, (int) x,
                        (int) y, true);
                b.recycle();
                b = scaledBitmap;

                System.gc();
            } else {
                b = BitmapFactory.decodeStream(in);
            }
            in.close();

            Log.d("", "bitmap size - width: " + b.getWidth() + ", height: " +
                    b.getHeight());
            if (b != null) {
                ExifInterface ei = null;
                try {
                    ei = new ExifInterface(path);
                } catch (IOException e) {
                    e.printStackTrace();
                }
                int orientation = ei.getAttributeInt(ExifInterface.TAG_ORIENTATION,
                        ExifInterface.ORIENTATION_UNDEFINED);
                switch (orientation) {
                    case ExifInterface.ORIENTATION_ROTATE_90:
                        rotateImage(b, 90);
                        break;
                    case ExifInterface.ORIENTATION_ROTATE_180:
                        rotateImage(b, 180);
                        break;
                    case ExifInterface.ORIENTATION_ROTATE_270:
                        rotateImage(b, 270);
                        break;
                    case ExifInterface.ORIENTATION_NORMAL:
                    default:
                        break;
                }
                writeImageFile(b, path);
            }
            return b;
        } catch (IOException e) {
            Log.e("", e.getMessage(), e);
            return null;
        }
    }

    public void dialog() {
        dialogue = new Dialog(this);
        dialogue.requestWindowFeature(Window.FEATURE_NO_TITLE);
        dialogue.setContentView(R.layout.dialog);
        dialogue.getWindow().setBackgroundDrawable(new ColorDrawable(Color.TRANSPARENT));
        txt_c_dialog_title = (TextView) dialogue.findViewById(R.id.txt_c_dialog_title);
        list_view = (ListView) dialogue.findViewById(R.id.dialog_list);
    }

    public void dialogsearch() {
        dialogue = new Dialog(this);
        dialogue.requestWindowFeature(Window.FEATURE_NO_TITLE);
        dialogue.setContentView(R.layout.dialog_search);
        dialogue.getWindow().setBackgroundDrawable(new ColorDrawable(Color.TRANSPARENT));
        et_search = (EditText) dialogue.findViewById(R.id.et_search);
        txt_c_search_dialog_title = (TextView) dialogue.findViewById(R.id.txt_c_search_dialog_title);
        et_search.setTypeface(Typeface.createFromAsset(getAssets(), getString(R.string.font_regular)));
        list_view = (ListView) dialogue.findViewById(R.id.dialog_list);
    }

    private void provinceDialog(final ArrayList<FavBeveragesModel> provinceArray) {
        dialogsearch();
        txt_c_search_dialog_title.setText(getString(R.string.select_province));
        provinceAdapter = new CityAdapterSearch(this, provinceArray);
        list_view.setAdapter(provinceAdapter);
        list_view.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView<?> parent, View view, int position, long id) {
                txt_c_province.setText(provinceArray.get(position).name);
                txt_c_province.setError(null);
                txt_c_province.clearFocus();
                AppDelegate.LogT("onItemClick = " + provinceAdapter.getItem(position).id + "");
                for (FavBeveragesModel cityItems : provinceArray) {
                    if (cityItems.name.equalsIgnoreCase(provinceAdapter.getItem(position).name + "")) {
                        txt_c_province.setText(cityItems.name);
                        province_id = cityItems.id;
                        txt_c_city.setText("");
                        et_address.setText("");
                        cityArray.clear();
                        if (EditProfileActivity.this != null && dialogue != null && dialogue.isShowing()) {
                            dialogue.dismiss();
                            // if (!AppDelegate.isValidString(txt_c_city.getText().toString()))
                            new Handler().postDelayed(new Runnable() {
                                @Override
                                public void run() {
                                    txt_c_city.performClick();
                                }
                            }, 300);
                        }
                        break;
                    }
                }
            }
        });
        et_search.addTextChangedListener(new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence s, int start, int count, int after) {

            }

            @Override
            public void onTextChanged(CharSequence s, int start, int before, int count) {

            }

            @Override
            public void afterTextChanged(Editable s) {
                provinceAdapter.filter(et_search.getText().toString());
            }
        });
        if (dialogue != null && dialogue.isShowing()) {
            dialogue.dismiss();
            dialogue.show();
        } else {
            if (dialogue != null)
                dialogue.show();
        }
    }

    private void cityDialog(final ArrayList<FavBeveragesModel> cityeArray) {
        dialogsearch();
        txt_c_search_dialog_title.setText(getString(R.string.select_city));
        cityAdpter = new CityAdapterSearch(this, cityeArray);
        list_view.setAdapter(cityAdpter);
        list_view.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView<?> parent, View view, int position, long id) {

                AppDelegate.LogT("onItemClick = " + cityAdpter.getItem(position).id + "");
                for (FavBeveragesModel cityItems : cityeArray) {
                    if (cityItems.name.equalsIgnoreCase(cityAdpter.getItem(position).name + "")) {
                        txt_c_city.setText(cityItems.name);
                        txt_c_city.setError(null);
                        txt_c_city.clearFocus();
                        city_id = cityItems.id;
                        if (EditProfileActivity.this != null && dialogue != null && dialogue.isShowing()) {
                            dialogue.dismiss();
//                            if (new Prefs(EditProfileActivity.this).getUserdata() != null && !AppDelegate.isValidString(new Prefs(EditProfileActivity.this).getUserdata().social_id))
//                                heard_about_us_Dialog(heard_about_us_array);
//                            // if (!AppDelegate.isValidString(et_address.getText().toString())) {
//                            et_password.post(
//                                    new Runnable() {
//                                        public void run() {
//                                            InputMethodManager inputMethodManager = (InputMethodManager) getSystemService(INPUT_METHOD_SERVICE);
//                                            inputMethodManager.toggleSoftInputFromWindow(et_password.getApplicationWindowToken(), InputMethodManager.SHOW_FORCED, 0);
//                                            et_password.requestFocus();
//                                        }
//                                    });
//                            //   }
                        }
                        break;
                    }
                }
            }
        });
        et_search.addTextChangedListener(new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence s, int start, int count, int after) {

            }

            @Override
            public void onTextChanged(CharSequence s, int start, int before, int count) {

            }

            @Override
            public void afterTextChanged(Editable s) {
                cityAdpter.filter(et_search.getText().toString());
            }
        });
        if (dialogue != null && dialogue.isShowing()) {
            dialogue.dismiss();
            dialogue.show();
        } else {
            if (dialogue != null)
                dialogue.show();
        }
    }

//    public void breweryDialog(final ArrayList<FavBeveragesModel> breweryArray) {
//        clicked = 0;
//        dialog();
//        txt_c_dialog_title.setText("Select Brewery");
//        breweryAdapter = new AlertAdapter(this, breweryArray);
//        list_view.setAdapter(breweryAdapter);
//        list_view.setOnItemClickListener(new AdapterView.OnItemClickListener() {
//            @Override
//            public void onItemClick(AdapterView<?> parent, View view, int position, long id) {
//                txt_c_fav_brewery.setText(breweryArray.get(position).name);
//                brewery_id = breweryArray.get(position).id;
//                if (dialogue != null && dialogue.isShowing()) {
//                    dialogue.dismiss();
//                    //  if (!AppDelegate.isValidString(txt_c_province.getText().toString()))
//                    new Handler().postDelayed(new Runnable() {
//                        @Override
//                        public void run() {
//                            txt_c_province.performClick();
//                        }
//                    }, 300);
//                }
//            }
//        });
//        if (dialogue != null && dialogue.isShowing()) {
//            dialogue.dismiss();
//            dialogue.show();
//        } else {
//            if (dialogue != null)
//                dialogue.show();
//        }
//    }

    public void beerDialog(final ArrayList<FavBeveragesModel> beerArray) {
        clicked = 0;
        dialog();
        txt_c_dialog_title.setText("Select Beer");
        beerAdapter = new AlertAdapter(this, beerArray);
        list_view.setAdapter(beerAdapter);
        list_view.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView<?> parent, View view, int position, long id) {
                txt_c_fav_beer.setText(beerArray.get(position).name);
                beer_id = beerArray.get(position).id;
                if (dialogue != null && dialogue.isShowing()) {
                    dialogue.dismiss();
                    // if (!AppDelegate.isValidString(txt_c_fav_brewery.getText().toString()))
                    new Handler().postDelayed(new Runnable() {
                        @Override
                        public void run() {
//                            txt_c_fav_brewery.performClick();
                        }
                    }, 300);
                }
            }
        });
        if (dialogue != null && dialogue.isShowing()) {
            dialogue.dismiss();
            dialogue.show();
        } else {
            if (dialogue != null)
                dialogue.show();
        }
    }

    public void heard_about_us_Dialog(final ArrayList<FavBeveragesModel> heard_about_us_array) {
        dialog();
        txt_c_dialog_title.setText(getString(R.string.heard_about_us));
        heard_about_us_Adapter = new AlertAdapter(this, heard_about_us_array);
        list_view.setAdapter(heard_about_us_Adapter);
        list_view.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView<?> parent, View view, int position, long id) {
                txt_c_heard_about_us.setText(heard_about_us_array.get(position).name);
                txt_c_heard_about_us.setError(null);
                txt_c_heard_about_us.clearFocus();
                if (dialogue != null && dialogue.isShowing()) {
                    dialogue.dismiss();
                    // if (!AppDelegate.isValidString(txt_c_fav_brewery.getText().toString()))
                }
            }
        });
        if (dialogue != null && dialogue.isShowing()) {
            dialogue.dismiss();
            dialogue.show();
        } else {
            if (dialogue != null)
                dialogue.show();
        }
    }

    private void executeEditProfile() {
        try {
            if (AppDelegate.haveNetworkConnection(this, true)) {
                ArrayList<PostAysnc_Model> mPostArrayList = new ArrayList<PostAysnc_Model>();
                PostAsync mPostAsyncObj;
                AppDelegate.getInstance(this).setPostParamsSecond(mPostArrayList, Tags.API_KEY, Tags.API_KEY_VALUE);
                AppDelegate.getInstance(this).setPostParamsSecond(mPostArrayList, Tags.device_id, prefs.getGCMtokenfromTemp());
                AppDelegate.getInstance(this).setPostParamsSecond(mPostArrayList, Tags.device_type, AppDelegate.DEVICE_TYPE, ServerRequestConstants.Key_PostintValue);
                AppDelegate.getInstance(this).setPostParamsSecond(mPostArrayList, Tags.last_name, et_lastname.getText().toString());

                AppDelegate.getInstance(this).setPostParamsSecond(mPostArrayList, Tags.email, et_email.getText().toString());
                AppDelegate.getInstance(this).setPostParamsSecond(mPostArrayList, Tags.name, et_username.getText().toString());
                AppDelegate.getInstance(this).setPostParamsSecond(mPostArrayList, Tags.beer_type_id, beer_id, ServerRequestConstants.Key_PostintValue);
                AppDelegate.getInstance(this).setPostParamsSecond(mPostArrayList, Tags.brewery_type_id, et_fav_brewery.getText().toString(), ServerRequestConstants.Key_PostintValue);
                AppDelegate.getInstance(this).setPostParamsSecond(mPostArrayList, Tags.contact_number, et_contact_no.getText().toString());
                AppDelegate.getInstance(this).setPostParamsSecond(mPostArrayList, Tags.address, "");
                AppDelegate.getInstance(this).setPostParamsSecond(mPostArrayList, Tags.city_id, city_id, ServerRequestConstants.Key_PostintValue);
                AppDelegate.getInstance(this).setPostParamsSecond(mPostArrayList, Tags.state_id, province_id, ServerRequestConstants.Key_PostintValue);
                AppDelegate.getInstance(this).setPostParamsSecond(mPostArrayList, Tags.dob, txt_c_dob.getText().toString());
                AppDelegate.getInstance(this).setPostParamsSecond(mPostArrayList, Tags.role, AppDelegate.BUYER, ServerRequestConstants.Key_PostintValue);
                AppDelegate.getInstance(this).setPostParamsSecond(mPostArrayList, Tags.user_id, new Prefs(this).getUserdata().id, ServerRequestConstants.Key_PostintValue);

                if (capturedFile != null)
                    AppDelegate.getInstance(this).setPostParamsSecond(mPostArrayList, Tags.user_image, capturedFile.getAbsolutePath() + "", ServerRequestConstants.Key_PostFileValue);
                mPostAsyncObj = new PostAsync(this, this, ServerRequestConstants.EDIT_PROFILE,
                        mPostArrayList, null);
                mHandler.sendEmptyMessage(10);
                mPostAsyncObj.execute();
            }
        } catch (Exception e) {
            AppDelegate.LogE(e);
            AppDelegate.showToast(this, getResources().getString(R.string.try_again));
        }
    }
}
