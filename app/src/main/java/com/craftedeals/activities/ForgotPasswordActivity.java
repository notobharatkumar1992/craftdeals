package com.craftedeals.activities;

import android.graphics.Typeface;
import android.graphics.drawable.Drawable;
import android.os.Build;
import android.os.Bundle;
import android.os.Handler;
import android.os.Message;
import android.support.v7.app.AppCompatActivity;
import android.view.View;
import android.view.Window;
import android.view.WindowManager;
import android.widget.EditText;

import com.craftedeals.AppDelegate;
import com.craftedeals.Async.PostAsync;
import com.craftedeals.Models.PostAysnc_Model;
import com.craftedeals.R;
import com.craftedeals.Utils.Prefs;
import com.craftedeals.constants.ServerRequestConstants;
import com.craftedeals.constants.Tags;
import com.craftedeals.interfaces.OnReciveServerResponse;

import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;

public class ForgotPasswordActivity extends AppCompatActivity implements View.OnClickListener, OnReciveServerResponse {

    public EditText et_email;
    public Handler mHandler;
    private Prefs prefs;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.LOLLIPOP) {
            Window window = this.getWindow();
            Drawable background = this.getResources().getDrawable(R.drawable.spalsh);
            window.addFlags(WindowManager.LayoutParams.FLAG_DRAWS_SYSTEM_BAR_BACKGROUNDS);
            window.setStatusBarColor(getResources().getColor(android.R.color.transparent));
            window.setNavigationBarColor(getResources().getColor(android.R.color.transparent));
            window.setBackgroundDrawable(background);
        }
        setContentView(R.layout.forgot_password);
        prefs = new Prefs(this);
        initView();
        setHandler();
    }

    private void initView() {
        et_email = (EditText) findViewById(R.id.et_email);
        et_email.setTypeface(Typeface.createFromAsset(getAssets(), getString(R.string.font_regular)));
        findViewById(R.id.txt_c_submit).setOnClickListener(this);
        findViewById(R.id.img_back).setOnClickListener(this);
    }

    private void setHandler() {
        mHandler = new Handler() {
            @Override
            public void dispatchMessage(Message msg) {
                super.dispatchMessage(msg);
                switch (msg.what) {
                    case 10:
                        AppDelegate.showProgressDialog(ForgotPasswordActivity.this);
                        break;
                    case 11:
                        AppDelegate.hideProgressDialog(ForgotPasswordActivity.this);
                        break;
                    case 3:
                        executeForgotApi();
                        break;
                }
            }
        };
    }

    private void executeForgotApi() {
        try {
            if (AppDelegate.haveNetworkConnection(this, true)) {
                ArrayList<PostAysnc_Model> mPostArrayList = new ArrayList<PostAysnc_Model>();
                PostAsync mPostAsyncObj;
                AppDelegate.getInstance(this).setPostParamsSecond(mPostArrayList, Tags.API_KEY, Tags.API_KEY_VALUE);
                AppDelegate.getInstance(this).setPostParamsSecond(mPostArrayList, Tags.email, et_email.getText().toString());
                AppDelegate.getInstance(this).setPostParamsSecond(mPostArrayList, Tags.device_id, prefs.getGCMtokenfromTemp());
                AppDelegate.getInstance(this).setPostParamsSecond(mPostArrayList, Tags.device_type, 2, ServerRequestConstants.Key_PostintValue);
                AppDelegate.getInstance(this).setPostParamsSecond(mPostArrayList, Tags.role, AppDelegate.BUYER, ServerRequestConstants.Key_PostintValue);
                mPostAsyncObj = new PostAsync(this, this, ServerRequestConstants.FORGOT_PASSWORD,
                        mPostArrayList, null);
                mHandler.sendEmptyMessage(10);
                mPostAsyncObj.execute();
            }
        } catch (Exception e) {
            AppDelegate.LogE(e);
            AppDelegate.showToast(this, getResources().getString(R.string.try_again));
        }
    }

    @Override
    public void onClick(View v) {
        switch (v.getId()) {
            case R.id.txt_c_submit:
                chk_validation();
                break;
            case R.id.img_back:
                onBackPressed();
                break;
        }
    }

    private void chk_validation() {
        if (!AppDelegate.CheckEmail(et_email.getText().toString())) {
            AppDelegate.showToast(this, getResources().getString(R.string.foremail));
        } else if (AppDelegate.haveNetworkConnection(this, false)) {
            mHandler.sendEmptyMessage(3);
        } else {
            AppDelegate.showToast(this, getResources().getString(R.string.not_connected));
        }
    }

    @Override
    public void setOnReciveResult(String apiName, String result) {
        mHandler.sendEmptyMessage(11);
        if (!AppDelegate.isValidString(result)) {
            AppDelegate.showToast(this, getResources().getString(R.string.time_out));
            return;
        }
        if (apiName.equalsIgnoreCase(ServerRequestConstants.FORGOT_PASSWORD)) {
            parseForgot(result);
        }
    }

    private void parseForgot(String result) {
        try {
            JSONObject jsonObject = new JSONObject(result);
            if (jsonObject.getInt(Tags.status) == 1) {
                AppDelegate.showToast(this, jsonObject.getString(Tags.message));
                finish();
            } else {
                AppDelegate.showToast(this, jsonObject.getString(Tags.message));
            }
        } catch (JSONException e) {
            e.printStackTrace();
        }

    }
}
