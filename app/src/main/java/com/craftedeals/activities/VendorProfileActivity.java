package com.craftedeals.activities;

import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.graphics.Bitmap;
import android.graphics.drawable.Animatable;
import android.graphics.drawable.AnimationDrawable;
import android.graphics.drawable.Drawable;
import android.os.Build;
import android.os.Bundle;
import android.os.Handler;
import android.os.Looper;
import android.os.Message;
import android.support.v4.app.ActivityOptionsCompat;
import android.support.v4.app.Fragment;
import android.support.v4.util.Pair;
import android.support.v4.view.ViewPager;
import android.support.v7.app.AlertDialog;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.RecyclerView;
import android.text.Html;
import android.view.View;
import android.view.Window;
import android.view.WindowManager;

import com.craftedeals.AppDelegate;
import com.craftedeals.Async.PostAsync;
import com.craftedeals.Models.AdHocDealsModel;
import com.craftedeals.Models.BusinessModel;
import com.craftedeals.Models.CategoryModel;
import com.craftedeals.Models.CityModel;
import com.craftedeals.Models.HeroDealsModel;
import com.craftedeals.Models.OpenHoursModel;
import com.craftedeals.Models.Person;
import com.craftedeals.Models.PostAysnc_Model;
import com.craftedeals.R;
import com.craftedeals.Utils.Prefs;
import com.craftedeals.Utils.TransitionHelper;
import com.craftedeals.adapters.PagerAdapter;
import com.craftedeals.constants.ServerRequestConstants;
import com.craftedeals.constants.Tags;
import com.craftedeals.fragments.BannerFragment;
import com.craftedeals.interfaces.OnListItemClickListener;
import com.craftedeals.interfaces.OnReciveServerResponse;
import com.craftedeals.parser.JSONParser;
import com.google.android.gms.maps.model.LatLng;
import com.nostra13.universalimageloader.core.DisplayImageOptions;
import com.nostra13.universalimageloader.core.ImageLoader;
import com.nostra13.universalimageloader.core.ImageLoaderConfiguration;
import com.nostra13.universalimageloader.core.assist.FailReason;
import com.nostra13.universalimageloader.core.listener.ImageLoadingListener;

import org.apache.commons.lang3.text.WordUtils;
import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.List;

import carbon.widget.ImageView;
import carbon.widget.TextView;

import static com.craftedeals.activities.DealsActivity.DEAL_HERO;

public class VendorProfileActivity extends AppCompatActivity implements View.OnClickListener, OnReciveServerResponse, OnListItemClickListener
{
    public static Handler mHandler;
    Prefs prefs;
    ImageView img_c_item_image, img_c_vendor_image, img_c_viewlocation;
    TextView txt_c_distance, txt_c_adhocdeal_label, txt_c_comapny_open_hour, txt_c_comapny_contact_number, txt_c_comapny_address, txt_c_hero_deal_category, txt_c_hero_deal_title, /*txt_c_value_upto, txt_c_off,*/
            txt_c_discount_offer, txt_c_titlename, txt_c_compamyname, txt_c_about_business, txt_c_url_ph, txt_c_url, txt_c_validity, txt_c_terms_conditions, txt_c_redeemnow;
    android.widget.ImageView img_loading1, img_loading2;
    ImageLoader imageLoader = ImageLoader.getInstance();
    DisplayImageOptions options = new DisplayImageOptions.Builder().cacheInMemory(true)
            .cacheOnDisc(true).resetViewBeforeLoading(true).
                    build();
    int deal_id = 0, vendor_id, type = 1;
    public String str_lat = "", str_long = "";
    private HeroDealsModel heroDealsModel;
    ArrayList<AdHocDealsModel> adHocDealModelArrayList = new ArrayList<>();
    private AdHocDealsModel adHocDealModel = new AdHocDealsModel();
    private RecyclerView recycler_deals_list;
    //private GoogleApiClient client;
    private BusinessModel vendorProfile;
    public AlertDialog.Builder alert;
    boolean apiShouldcall = false;
    private PagerAdapter bannerPagerAdapter, bannerPagerAdapter_hero;
    private List<Fragment> bannerFragment = new ArrayList();
    private List<Fragment> bannerFragment_hero = new ArrayList();
    android.widget.LinearLayout pager_indicator, pager_indicator_hero;
    ViewPager view_pager, view_pager_hero;

    @Override
    protected void onCreate(Bundle savedInstanceState)
    {
        super.onCreate(savedInstanceState);
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.LOLLIPOP)
        {
            Window window = this.getWindow();
            Drawable background = this.getResources().getDrawable(R.drawable.topbg);
            window.addFlags(WindowManager.LayoutParams.FLAG_DRAWS_SYSTEM_BAR_BACKGROUNDS);
            window.setStatusBarColor(getResources().getColor(android.R.color.transparent));
            window.setNavigationBarColor(getResources().getColor(android.R.color.transparent));
            window.setBackgroundDrawable(background);
        }
        setContentView(R.layout.vendor_profile);
        imageLoader.init(ImageLoaderConfiguration.createDefault(this));
        prefs = new Prefs(this);
        initView();
        setHandler();
        deal_id = getIntent().getIntExtra(Tags.deal_id, 0);
        vendor_id = getIntent().getIntExtra(Tags.vendor_id, 0);

        str_lat = AppDelegate.isValidString(getIntent().getStringExtra(Tags.LAT)) ? getIntent().getStringExtra(Tags.LAT) : "";
        str_long = AppDelegate.isValidString(getIntent().getStringExtra(Tags.LONG)) ? getIntent().getStringExtra(Tags.LONG) : "";

        type = getIntent().getIntExtra(Tags.type, 1);
        if (getIntent().getExtras() != null && getIntent().getExtras().getParcelable(Tags.deal) != null)
        {
            heroDealsModel = getIntent().getExtras().getParcelable(Tags.deal);
            show_HeroDeal(heroDealsModel);
        }
        if (getIntent() != null && AppDelegate.isValidString(getIntent().getStringExtra(Tags.FROM)))
        {
            if (getIntent().getStringExtra(Tags.FROM).equalsIgnoreCase(Tags.notification))
            {
                execute_ViewNotificationApi(getIntent().getIntExtra(Tags.notification_id, 0));
            }
        }
        //client = new GoogleApiClient.Builder(this).addApi(AppIndex.API).build();
        new Handler().postDelayed(new Runnable()
        {
            @Override
            public void run()
            {
                executeVendorProfileApi();
            }
        }, 200);
    }

    private void execute_ViewNotificationApi(int notification_id)
    {
        try
        {
            if (AppDelegate.haveNetworkConnection(this, true))
            {
                ArrayList<PostAysnc_Model> mPostArrayList = new ArrayList<>();
                PostAsync mPostAsyncObj;
                AppDelegate.getInstance(this).setPostParamsSecond(mPostArrayList, Tags.API_KEY, Tags.API_KEY_VALUE);
                AppDelegate.getInstance(this).setPostParamsSecond(mPostArrayList, Tags.user_id, new Prefs(this).getUserdata().id);
                AppDelegate.getInstance(this).setPostParamsSecond(mPostArrayList, Tags.nid, notification_id);
                mPostAsyncObj = new PostAsync(this, this, ServerRequestConstants.VIEW_NOTIFICATION,
                        mPostArrayList, null);
                mHandler.sendEmptyMessage(10);
                mPostAsyncObj.execute();
            }
        }
        catch (Exception e)
        {
            AppDelegate.LogE(e);
            AppDelegate.showToast(this, getString(R.string.try_again));
        }
    }

    private void initView()
    {
        txt_c_distance = (TextView) findViewById(R.id.txt_c_distance);
        img_c_item_image = (ImageView) findViewById(R.id.img_c_item_image);
        img_loading1 = (android.widget.ImageView) findViewById(R.id.img_loading1);
        txt_c_validity = (TextView) findViewById(R.id.txt_c_validity);
        txt_c_terms_conditions = (TextView) findViewById(R.id.txt_c_terms_conditions);
        recycler_deals_list = (RecyclerView) findViewById(R.id.recycler_deals_list);
        recycler_deals_list.setNestedScrollingEnabled(false);
        img_c_vendor_image = (ImageView) findViewById(R.id.img_c_vendor_image);
        img_loading2 = (android.widget.ImageView) findViewById(R.id.img_loading2);
        txt_c_compamyname = (TextView) findViewById(R.id.txt_c_compamyname);
        txt_c_hero_deal_title = (TextView) findViewById(R.id.txt_c_hero_deal_title);
        txt_c_about_business = (TextView) findViewById(R.id.txt_c_about_business);
        txt_c_url_ph = (TextView) findViewById(R.id.txt_c_url_ph);
        txt_c_url = (TextView) findViewById(R.id.txt_c_url);
        txt_c_url.setOnClickListener(this);
        img_c_viewlocation = (ImageView) findViewById(R.id.img_c_viewlocation);
        txt_c_titlename = (TextView) findViewById(R.id.txt_c_titlename);
        findViewById(R.id.txt_c_redeemnow).setOnClickListener(this);
        txt_c_redeemnow = (TextView) findViewById(R.id.txt_c_redeemnow);
        txt_c_hero_deal_category = (TextView) findViewById(R.id.txt_c_hero_deal_category);
        txt_c_redeemnow.setOnClickListener(this);
        img_c_viewlocation.setOnClickListener(this);
        findViewById(R.id.img_c_back).setOnClickListener(this);
//        txt_c_value_upto = (TextView) findViewById(R.id.txt_c_value_upto);
//        txt_c_off = (TextView) findViewById(R.id.txt_c_off);
        txt_c_discount_offer = (TextView) findViewById(R.id.txt_c_discount_offer);
        findViewById(R.id.img_c_share).setOnClickListener(this);
        findViewById(R.id.txt_c_read_more_vendor).setOnClickListener(this);

        view_pager = (ViewPager) findViewById(R.id.view_pager);
        pager_indicator = (android.widget.LinearLayout) findViewById(R.id.pager_indicator);
        view_pager_hero = (ViewPager) findViewById(R.id.view_pager_hero);
        pager_indicator_hero = (android.widget.LinearLayout) findViewById(R.id.pager_indicator_hero);
        txt_c_comapny_address = (TextView) findViewById(R.id.txt_c_comapny_address);
        txt_c_comapny_contact_number = (TextView) findViewById(R.id.txt_c_comapny_contact_number);
        txt_c_comapny_contact_number.setOnClickListener(this);
        txt_c_comapny_open_hour = (TextView) findViewById(R.id.txt_c_comapny_open_hour);
        txt_c_adhocdeal_label = (TextView) findViewById(R.id.txt_c_adhocdeal_label);
    }

    private void setHandler()
    {
        mHandler = new Handler()
        {
            @Override
            public void dispatchMessage(Message msg)
            {
                super.dispatchMessage(msg);
                switch (msg.what)
                {
                    case 10:
                        AppDelegate.showProgressDialog(VendorProfileActivity.this);
                        break;
                    case 11:
                        AppDelegate.hideProgressDialog(VendorProfileActivity.this);
                        break;
                    case 1:
                        break;
                    case 2:
                        // setVendorProfile();
                        break;
                    case 3:
                        executeVendorProfileApi();
                        break;
                }
            }
        };
    }

    public void downloadImageAndGetPath()
    {
        AppDelegate.showProgressDialog(VendorProfileActivity.this);
        if (AppDelegate.haveNetworkConnection(VendorProfileActivity.this, true))
        {
            Intent shareIntent = new Intent(android.content.Intent.ACTION_SEND);
            shareIntent.setType("text/plain");
            if (AppDelegate.isValidString(heroDealsModel.herodeal_title))
            {
                shareIntent.putExtra(android.content.Intent.EXTRA_SUBJECT, heroDealsModel.herodeal_title);
                shareIntent.putExtra(Intent.EXTRA_REFERRER_NAME, heroDealsModel.herodeal_title);
                shareIntent.putExtra(Intent.EXTRA_TITLE, heroDealsModel.herodeal_title);
            }
            shareIntent.putExtra(android.content.Intent.EXTRA_TEXT, heroDealsModel.url);
            startActivity(Intent.createChooser(shareIntent, "Share Deal"));
        }
//            new Thread(new Runnable() {
//                @Override
//                public void run() {
//                    imageLoader.loadImage(heroDealsModel.image, options, new ImageLoadingListener() {
//                        @Override
//                        public void onLoadingStarted(String imageUri, View view) {
//                            AppDelegate.LogT("onLoadingStarted called");
//                        }
//
//                        @Override
//                        public void onLoadingFailed(String imageUri, View view, FailReason failReason) {
//                            AppDelegate.hideProgressDialog(VendorProfileActivity.this);
//                            AppDelegate.LogT("onLoadingFailed called = " + failReason.getCause().toString());
//
//                            Intent shareIntent = new Intent(android.content.Intent.ACTION_SEND);
//                            shareIntent.setType("text/plain");
//                            if (AppDelegate.isValidString(heroDealsModel.herodeal_title)) {
//                                shareIntent.putExtra(android.content.Intent.EXTRA_SUBJECT, heroDealsModel.herodeal_title);
//                                shareIntent.putExtra(android.content.Intent.EXTRA_TEXT, heroDealsModel.url);
//                            }
//                            startActivity(Intent.createChooser(shareIntent, "Share Deal"));
//                        }
//
//                        @Override
//                        public void onLoadingComplete(String imageUri, View view, Bitmap loadedImage) {
//                            AppDelegate.hideProgressDialog(VendorProfileActivity.this);
//                            String file_path = AppDelegate.getDefaultFile(VendorProfileActivity.this);
//                            AppDelegate.LogT("onLoadingComplete => file_path = " + file_path);
//                            if (!AppDelegate.isValidString(file_path)) {
//                                AppDelegate.showToast(VendorProfileActivity.this, getString(R.string.sharing_failed));
//                                return;
//                            }
//                            AppDelegate.saveFile(VendorProfileActivity.this, loadedImage, file_path);
//                            Uri uri = Uri.parse("file:///" + file_path);
//                            Intent shareIntent = new Intent(android.content.Intent.ACTION_SEND);
//                            shareIntent.setType("text/plain");
//                            if (AppDelegate.isValidString(heroDealsModel.herodeal_title)) {
//                                shareIntent.putExtra(android.content.Intent.EXTRA_SUBJECT, heroDealsModel.herodeal_title);
//                                shareIntent.putExtra(android.content.Intent.EXTRA_TEXT, heroDealsModel.url);
//                                shareIntent.putExtra(Intent.EXTRA_REFERRER_NAME, heroDealsModel.herodeal_title);
//                                shareIntent.putExtra(Intent.EXTRA_TITLE, heroDealsModel.herodeal_title);
//                                shareIntent.putExtra(Intent.EXTRA_PROCESS_TEXT_READONLY, heroDealsModel.herodeal_title);
//                            }
//                            shareIntent.putExtra(Intent.EXTRA_STREAM, uri);
//                            shareIntent.addFlags(Intent.FLAG_GRANT_READ_URI_PERMISSION);
//                            startActivity(Intent.createChooser(shareIntent, "Share Deal"));
//                        }
//
//                        @Override
//                        public void onLoadingCancelled(String imageUri, View view) {
//                            AppDelegate.hideProgressDialog(VendorProfileActivity.this);
//                            AppDelegate.LogT("onLoadingCancelled called");
//
//                            Intent shareIntent = new Intent(android.content.Intent.ACTION_SEND);
//                            shareIntent.setType("text/plain");
//                            if (AppDelegate.isValidString(heroDealsModel.herodeal_title)) {
//                                shareIntent.putExtra(android.content.Intent.EXTRA_SUBJECT, heroDealsModel.herodeal_title);
//                                shareIntent.putExtra(android.content.Intent.EXTRA_TEXT, heroDealsModel.url);
//                            }
//                            shareIntent.addFlags(Intent.FLAG_GRANT_READ_URI_PERMISSION);
//                            startActivity(Intent.createChooser(shareIntent, "Share Deal"));
//                        }
//                    });
//                }
//            }).start();
        if (heroDealsModel != null && AppDelegate.isValidString(heroDealsModel.name))
        {
            AppDelegate.trackSharedDeal(heroDealsModel.name, DEAL_HERO, "VENDOR PROFILE");
        }

        AppDelegate.hideProgressDialog(VendorProfileActivity.this);
    }

    @Override
    protected void onDestroy()
    {
        super.onDestroy();
        mHandler = null;
    }

    @Override
    protected void onResume()
    {
        super.onResume();
        try
        {
            if (prefs.getUserdata() != null && heroDealsModel != null && vendorProfile != null)
            {
                if (apiShouldcall)
                {
                    executeVendorProfileApi();
                }
                setVendorProfile(vendorProfile);
            }
        }
        catch (Exception e)
        {
            AppDelegate.LogE(e);
        }
    }

    @Override
    public void setOnReciveResult(String apiName, String result)
    {
        if (mHandler == null)
        {
            return;
        }
        mHandler.sendEmptyMessage(11);
        if (!AppDelegate.isValidString(result))
        {
            AppDelegate.showToast(this, getString(R.string.time_out));
            return;
        }
        if (apiName.equals(ServerRequestConstants.VENDOR_PROFILE))
        {
            parseVendorProfile(result);
        }
        else if (apiName.equals(ServerRequestConstants.REDEEM_DEAL))
        {
            parseRedeemDeal(result);
        }
        else if (apiName.equalsIgnoreCase(ServerRequestConstants.VIEW_NOTIFICATION))
        {
            parseViewNotification(result);
        }
    }

    private void parseViewNotification(String result)
    {
        try
        {
            JSONObject jsonObject = new JSONObject(result);
//            if (jsonObject.getInt(Tags.status) == 1) {
//                AppDelegate.showToast(this, jsonObject.getString(Tags.message));
//            } else {
//                AppDelegate.showToast(this, jsonObject.getString(Tags.message));
//            }
        }
        catch (JSONException e)
        {
            AppDelegate.LogE(e);
        }
    }

    private void parseRedeemDeal(String result)
    {
        try
        {
            JSONObject jsonobj = new JSONObject(result);
            if (jsonobj.getInt(Tags.status) == 1 && jsonobj.getInt(Tags.dataFlow) == 1)
            {
                // AppDelegate.showToast(this, jsonobj.getString(Tags.message));
                heroDealsModel.redeem = 1;
                txt_c_redeemnow.setText("Redeemed");
                txt_c_redeemnow.setBackgroundColor(getResources().getColor(R.color.botom_gray_bg_color));
                AppDelegate.trackRedeemDeal(heroDealsModel.name, DEAL_HERO, "Verdor Profile");
                Intent intent = new Intent(this, CongratulationActivity.class);
                intent.putExtra(Tags.deal_type, DEAL_HERO);
                intent.putExtra(Tags.deal, heroDealsModel.company_name);
                intent.putExtra(Tags.types, CongratulationActivity.DEAL_REDEEMED);
                startActivity(intent);
            }
            else
            {
                AppDelegate.showAlert(this, jsonobj.getString(Tags.message));
            }
        }
        catch (JSONException e)
        {
            AppDelegate.LogE(e);
        }
    }

    private void parseVendorProfile(String result)
    {
        try
        {
            JSONObject jsonobj = new JSONObject(result);
            if (jsonobj.getInt(Tags.status) == 1 && jsonobj.getInt(Tags.dataFlow) == 1)
            {
                CategoryModel category = new CategoryModel();
                JSONObject array = jsonobj.getJSONObject(Tags.response);
                vendorProfile = new BusinessModel();
                vendorProfile.business_id = array.getInt(Tags.business_id);
                vendorProfile.user_id = array.getInt(Tags.user_id);
                vendorProfile.name = array.getString(Tags.name);
                vendorProfile.website_url = array.getString(Tags.website_url);
                vendorProfile.about_business = array.getString(Tags.about_business);
                vendorProfile.contact_name = array.getString(Tags.contact_name);
                vendorProfile.contact_number = array.getString(Tags.contact_number);
                vendorProfile.open_hours = array.getString(Tags.open_hours);
                vendorProfile.image = array.getString(Tags.image);
                vendorProfile.email = array.getString(Tags.email);
                vendorProfile.address1 = array.getString(Tags.address1);
                vendorProfile.address2 = array.getString(Tags.address2);
                vendorProfile.city = array.getString(Tags.city);
                vendorProfile.state = array.getString(Tags.state);
                vendorProfile.country = array.getString(Tags.country);
                vendorProfile.postal_code = array.getString(Tags.postal_code);
                vendorProfile.location_address = array.getString(Tags.location_address);
                vendorProfile.longitude = array.getString(Tags.longitude);
                vendorProfile.latitude = array.getString(Tags.latitude);
                vendorProfile.thumb = array.getString(Tags.thumb);
                try
                {
                    if (AppDelegate.isValidString(array.getString(Tags.images)))
                    {
                        JSONArray img = array.getJSONArray(Tags.images);
                        ArrayList<String> imgArray = new ArrayList<>();
                        if (img.length() > 0)
                        {
                            for (int j = 0; j < img.length(); j++)
                            {
                                imgArray.add(img.getJSONObject(j).getString(Tags.image));
                            }
                        }
                        else
                        {
                            imgArray.add(array.getString(Tags.image));
                        }
                        setBanner(imgArray);
                    }
                }
                catch (Exception e)
                {
                    AppDelegate.LogE(e);
                }
                heroDealsModel = new HeroDealsModel();
                JSONObject heroobj = array.getJSONObject("herodeals");
                heroDealsModel.url = heroobj.getString(Tags.url);
                heroDealsModel.distance = heroobj.getString(Tags.distance);
                heroDealsModel.id = heroobj.getInt(Tags.id);
                heroDealsModel.user_id = heroobj.getInt(Tags.user_id);
                heroDealsModel.name = heroobj.getString(Tags.name);
                heroDealsModel.herodeal_title = heroobj.getString(Tags.name);
                heroDealsModel.category = category.title;
                heroDealsModel.deal_category_id = heroobj.getInt(Tags.deal_category_id);
                heroDealsModel.image = heroobj.getString(Tags.image);
                heroDealsModel.sub_category_id = heroobj.getInt(Tags.sub_category_id);
                heroDealsModel.description = heroobj.getString(Tags.description);
                heroDealsModel.value_upto = heroobj.getInt(Tags.value_upto);
                heroDealsModel.discount_offer = heroobj.getString(Tags.discount_offer);
                heroDealsModel.starting_date = heroobj.getString(Tags.starting_date);
                heroDealsModel.expiry_date = heroobj.getString(Tags.expiry_date);
                heroDealsModel.trems_and_conditions = heroobj.getString(Tags.trems_and_conditions);
                heroDealsModel.total_deal = heroobj.getInt(Tags.total_deal);
                AppDelegate.LogT("Redeem => " + heroobj.has(Tags.redeem) + ", " + heroobj.getInt(Tags.redeem));
                ArrayList<String> imgArray2 = new ArrayList<>();
                try
                {
                    if (heroobj.has(Tags.images))
                    {
                        if (AppDelegate.isValidString(heroobj.getString(Tags.images)))
                        {
                            JSONArray img = heroobj.getJSONArray(Tags.images);

                            if (img.length() > 0)
                            {
                                for (int j = 0; j < img.length(); j++)
                                {
                                    imgArray2.add(img.getJSONObject(j).getString(Tags.image));
                                }
                            }
                            else
                            {
                                imgArray2.add(heroobj.getString(Tags.image));

                            }
                        }
                    }

                }
                catch (Exception e)
                {
                    AppDelegate.LogE(e);
                }
                setBanner_hero(imgArray2);
                if (heroobj.has(Tags.redeem))
                {
                    heroDealsModel.redeem = heroobj.getInt(Tags.redeem);
                }
                if (AppDelegate.isValidString(heroobj.getString(Tags.user)))
                {
                    JSONObject vendor = heroobj.getJSONObject(Tags.user);
                    heroDealsModel.vendor_name = vendor.getString(Tags.name);
                    vendorProfile.contact_name = vendor.getString(Tags.name);
                        /*JSONObject business = vendor.getJSONObject(Tags.busines);
                        heroDealsModel.latitude = business.getString(Tags.latitude);
                        heroDealsModel.longitude = business.getString(Tags.longitude);*/
                    if (vendor.has(Tags.busines))
                    {
                        if (AppDelegate.isValidString(vendor.getString(Tags.busines)))
                        {
                            heroDealsModel.company_name = vendor.getJSONObject(Tags.busines).getString(Tags.name);
                            heroDealsModel.latitude = vendor.getJSONObject(Tags.busines).getString(Tags.latitude);
                            heroDealsModel.longitude = vendor.getJSONObject(Tags.busines).getString(Tags.longitude);
                            heroDealsModel.company_address = vendor.getJSONObject(Tags.busines).getString(Tags.location_address);

                        }
                    }
                }
                heroDealsModel.cityName = array.getString(Tags.cityName);
                JSONObject obj = heroobj.getJSONObject(Tags.deal_category);
                category = new CategoryModel();
                category.id = obj.getInt(Tags.id);
                category.title = obj.getString(Tags.title);
                category.description = obj.getString(Tags.description);
                category.image = obj.getString(Tags.image);
                heroDealsModel.category = category.title;
                heroDealsModel.markerImage = obj.getString(Tags.marker_icon);
                setVendorProfile(vendorProfile);
                show_HeroDeal(heroDealsModel);
                //showpinOnMap(heroDealsModelArrayList);
                JSONArray adhocarray = array.getJSONArray("adhocdeals");
                adHocDealModelArrayList.clear();
                for (int i = 0; i < adhocarray.length(); i++)
                {
                    adHocDealModel = new AdHocDealsModel();
                    JSONObject adhocobj = adhocarray.getJSONObject(i);
                    adHocDealModel.distance = adhocobj.getString(Tags.distance);
                    adHocDealModel.id = adhocobj.getInt(Tags.id);
                    adHocDealModel.user_id = adhocobj.getInt(Tags.user_id);
                    adHocDealModel.name = adhocobj.getString(Tags.name);
                    adHocDealModel.use_the_offer = adhocobj.getString(Tags.use_the_offer);
                    adHocDealModel.things_to_remebers = adhocobj.getString(Tags.things_to_remebers);
                    adHocDealModel.deal_category_id = adhocobj.getInt(Tags.deal_category_id);
                    adHocDealModel.sub_category_id = adhocobj.getInt(Tags.sub_category_id);
                    adHocDealModel.facilities = adhocobj.getString(Tags.facilities);
                    adHocDealModel.item_detail = adhocobj.getString(Tags.item_detail);
                    adHocDealModel.deal_type = adhocobj.getInt(Tags.deal_type);
                    adHocDealModel.starting_date = adhocobj.getString(Tags.starting_date);
                    adHocDealModel.expiry_date = adhocobj.getString(Tags.expiry_date);
                    adHocDealModel.price = adhocobj.getInt(Tags.price);
                    adHocDealModel.discount_cost = adhocobj.getInt(Tags.discount_cost);
                    adHocDealModel.total_deal = adhocobj.getInt(Tags.total_deal);
                    adHocDealModel.volume_of_deal = adhocobj.getInt(Tags.volume_of_deal);
                    adHocDealModel.timing_to = adhocobj.getString(Tags.timing_to);
                    adHocDealModel.timing_from = adhocobj.getString(Tags.timing_from);
                    adHocDealModel.valid_on = adhocobj.getString(Tags.valid_on);
                    adHocDealModel.vendor_delete_deal = adhocobj.getInt(Tags.vendor_delete_deal);
                    adHocDealModel.favourite = adhocobj.getInt(Tags.favourite);
                    if (adhocobj.has(Tags.liked))
                    {
                        adHocDealModel.liked = adhocobj.getInt(Tags.liked);
                    }
                    try
                    {
                        if (AppDelegate.isValidString(adhocobj.getString(Tags.images)))
                        {
                            JSONArray img = adhocobj.getJSONArray(Tags.images);
                            for (int k = 0; k < img.length(); k++)
                            {
                                JSONObject json = img.getJSONObject(k);
                                adHocDealModel.image = json.getString(Tags.image);
                            }
                        }
                    }
                    catch (JSONException e)
                    {
                        AppDelegate.LogE(e);
                    }
                    if (AppDelegate.isValidString(adhocobj.getString(Tags.user)))
                    {
                        JSONObject vendor = adhocobj.getJSONObject(Tags.user);
                        adHocDealModel.vendor_name = vendor.getString(Tags.name);
                        if (vendor.has(Tags.busines) && AppDelegate.isValidString(vendor.getString(Tags.busines)))
                        {
                            //  JSONObject business = vendor.getJSONObject(Tags.busines);
                            adHocDealModel.latitude = vendor.getJSONObject(Tags.busines).getString(Tags.latitude);
                            adHocDealModel.longitude = vendor.getJSONObject(Tags.busines).getString(Tags.longitude);
                            adHocDealModel.company_address = vendor.getJSONObject(Tags.busines).getString(Tags.location_address);
//                            adHocDealModel.cityName = vendor.getJSONObject(Tags.busines).getString(Tags.cityName);
                        }
                    }
                    if (adhocobj.has(Tags.image))
                    {
                        adHocDealModel.image = adhocobj.getString(Tags.image);
                    }
                    JSONObject object = adhocobj.getJSONObject(Tags.deal_category);
                    category = new CategoryModel();
                    category.id = object.getInt(Tags.id);
                    category.title = object.getString(Tags.title);
                    category.description = object.getString(Tags.description);
                    category.image = object.getString(Tags.image);
                    adHocDealModel.category = category.title;
                    adHocDealModelArrayList.add(adHocDealModel);
                }
                AppDelegate.LogT("adHocDealModelArrayList==" + adHocDealModelArrayList);
                show_AdHocDealsList(adHocDealModelArrayList);

            }
            else
            {
                AppDelegate.showToast(this, jsonobj.getString(Tags.message));
                finish();
            }
        }
        catch (JSONException e)
        {
            AppDelegate.LogE(e);
        }
    }

    private void setVendorProfile(BusinessModel vendorProfile)
    {
        img_loading2 = (android.widget.ImageView) findViewById(R.id.img_loading2);
        AppDelegate.LogT("vendorProfile.name = " + vendorProfile.name + ", " + vendorProfile.contact_name);
        if (AppDelegate.isValidString(vendorProfile.name))
        {
            txt_c_titlename.setText(WordUtils.capitalize(vendorProfile.name));
        }
        txt_c_compamyname.setText(WordUtils.capitalize(vendorProfile.contact_name));
        txt_c_comapny_address.setText(vendorProfile.location_address);
        txt_c_about_business.setText(Html.fromHtml("<b>About Business: </b>" + vendorProfile.about_business));
        if (AppDelegate.isValidString(vendorProfile.website_url))
        {
            txt_c_url_ph.setText(Html.fromHtml("<b>Url: </b>"));
            txt_c_url.setText(Html.fromHtml(vendorProfile.website_url));
        }
        else
        {
            findViewById(R.id.ll_url).setVisibility(View.GONE);
        }
        txt_c_comapny_contact_number.setText(Html.fromHtml(vendorProfile.contact_number + ""));
        try
        {
            if (AppDelegate.isValidString(vendorProfile.open_hours))
            {
                JSONArray object = new JSONArray(vendorProfile.open_hours);
                ArrayList<OpenHoursModel> openHoursModels = new ArrayList<>();
                StringBuilder sb = new StringBuilder();
                for (int i = 0; i < object.length(); i++)
                {
                    JSONObject object1 = object.getJSONObject(i);
                    OpenHoursModel openHoursModel = new OpenHoursModel();
                    openHoursModel.day = JSONParser.getString(object1, "day");
                    openHoursModel.from = JSONParser.getString(object1, "from");
                    openHoursModel.to = JSONParser.getString(object1, "to");
                    try
                    {
                        openHoursModel.from = new SimpleDateFormat("hh:mm a").format(new SimpleDateFormat("HH:mm").parse(openHoursModel.from));
                        openHoursModel.to = new SimpleDateFormat("hh:mm a").format(new SimpleDateFormat("HH:mm").parse(openHoursModel.to));
                    }
                    catch (Exception e)
                    {
                        AppDelegate.LogE(e);
                    }
                    sb.append("<b>" + openHoursModel.day + " : </b>" + openHoursModel.from + " - " + openHoursModel.to + "<br>");
//                openHoursModels.add(openHoursModel);
                }
                String hours = sb.toString();
                if (hours.length() > 0)
                {
                    hours = hours.substring(0, hours.length() - 1).toString();
                    txt_c_comapny_open_hour.setText(Html.fromHtml(hours));
                }
            }
//            setHours(openHoursModels);
        }
        catch (JSONException e)
        {
            AppDelegate.LogE(e);
        }


//        AppDelegate.LogT("new Prefs(this).getUserdata().membership_id => " + new Prefs(this).getUserdata().membership_id);
//        AppDelegate.LogT("heroDealsModel.redeem => " + heroDealsModel.redeem);
        if (new Prefs(this).getUserdata() != null)
        {
            if (new Prefs(this).getUserdata().membership_id == 1)
            {
                txt_c_redeemnow.setText(getResources().getString(R.string.upgrade_to_access_deal));
                txt_c_redeemnow.setBackgroundColor(getResources().getColor(R.color.botom_gray_bg_color));
            }
            else
            {
                if (heroDealsModel.redeem == 1)
                {
                    txt_c_redeemnow.setText(getResources().getString(R.string.redeemed));
                    txt_c_redeemnow.setBackgroundColor(getResources().getColor(R.color.botom_gray_bg_color));
                }
                else
                {
                    txt_c_redeemnow.setText(getResources().getString(R.string.redeem));
                    txt_c_redeemnow.setBackgroundColor(getResources().getColor(R.color.dark_golden));
                }

            }
        }
        else
        {
            txt_c_redeemnow.setText(getResources().getString(R.string.login_or_join_to_access));
            txt_c_redeemnow.setBackgroundColor(getResources().getColor(R.color.botom_gray_bg_color));
        }

        img_loading2.setVisibility(View.VISIBLE);
        new Handler(Looper.getMainLooper()).post(new Runnable()
        {
            @Override
            public void run()
            {
                AnimationDrawable frameAnimation = (AnimationDrawable) img_loading2.getDrawable();
                frameAnimation.setCallback(img_loading2);
                frameAnimation.setVisible(true, true);
                frameAnimation.start();
                ((Animatable) img_loading2.getDrawable()).start();
            }
        });
        imageLoader.loadImage(vendorProfile.image, options, new ImageLoadingListener()
        {
            @Override
            public void onLoadingStarted(String imageUri, View view)
            {

            }

            @Override
            public void onLoadingFailed(String imageUri, View view, FailReason failReason)
            {
                img_c_vendor_image.setImageDrawable(getResources().getDrawable(R.drawable.deal_noimage));
                img_loading2.setVisibility(View.GONE);
            }

            @Override
            public void onLoadingComplete(String imageUri, View view, Bitmap loadedImage)
            {
                img_c_vendor_image.setImageBitmap(loadedImage);
                img_loading2.setVisibility(View.GONE);
            }

            @Override
            public void onLoadingCancelled(String imageUri, View view)
            {

            }
        });
    }

    private void show_AdHocDealsList(ArrayList<AdHocDealsModel> adHocDealModelArrayList)
    {
//        if (adHocDealModelArrayList != null && adHocDealModelArrayList.size() > 0) {
////            txt_c_adhocdeal_label.setVisibility(View.VISIBLE);
//        } else {
//            txt_c_adhocdeal_label.setVisibility(View.GONE);
//        }
        txt_c_adhocdeal_label.setVisibility(View.GONE);
        recycler_deals_list.setVisibility(View.GONE);
//        AdHocDealsAdapter mAdapter = new AdHocDealsAdapter(this, adHocDealModelArrayList, this);
//        RecyclerView.LayoutManager mLayoutManager = new LinearLayoutManager(this);
//        recycler_deals_list.requestDisallowInterceptTouchEvent(false);
//        recycler_deals_list.setLayoutManager(mLayoutManager);
//        recycler_deals_list.setItemAnimator(new DefaultItemAnimator());
//        recycler_deals_list.setAdapter(mAdapter);
    }

    private void show_HeroDeal(HeroDealsModel heroDealsModel)
    {
        if (heroDealsModel != null && AppDelegate.isValidString(heroDealsModel.name))
        {
            AppDelegate.trackDealNameAndtype(heroDealsModel.name, DEAL_HERO, "VENDOR PROFILE");
            txt_c_hero_deal_title.setText(heroDealsModel.name + "");
        }
        try
        {
            if (AppDelegate.isValidString(heroDealsModel.distance + ""))
            {
                txt_c_distance.setText(Html.fromHtml("<b>Distance: </b>" + (AppDelegate.roundOff(Double.parseDouble(heroDealsModel.distance)) + " KM.")));
            }
            else
            {
                txt_c_distance.setVisibility(View.GONE);
            }
        }
        catch (Exception e)
        {
            AppDelegate.LogE(e);
        }
        AppDelegate.LogT("heroDealsModel.name== " + heroDealsModel.name);
        txt_c_hero_deal_category.setText(heroDealsModel.category + "");
//        txt_c_value_upto.setText("$ " + heroDealsModel.value_upto + "");
//        txt_c_off.setText(heroDealsModel.discount_offer + "% off total bill");
        txt_c_discount_offer.setText(heroDealsModel.discount_offer + "");
        String starting_date = heroDealsModel.starting_date;
        starting_date = starting_date.replaceAll("/+0000", "");
        String expiry_date = heroDealsModel.expiry_date;
        expiry_date = expiry_date.replaceAll("/+0000", "");
        try
        {
            starting_date = new SimpleDateFormat("dd MMM yyyy").format(new SimpleDateFormat("yyyy-MM-dd'T'HH:mm:ss").parse(starting_date));
            expiry_date = new SimpleDateFormat("dd MMM yyyy").format(new SimpleDateFormat("yyyy-MM-dd'T'HH:mm:ss").parse(expiry_date));

        }
        catch (Exception e)
        {
            AppDelegate.LogE(e);
        }
        if (new Prefs(VendorProfileActivity.this).getUserdata() != null)
        {
            txt_c_validity.setText(Html.fromHtml(starting_date + "<b> to </b>" + expiry_date));
        }
        else
        {
            txt_c_validity.setVisibility(View.GONE);
        }

        txt_c_terms_conditions.setText(Html.fromHtml("<b>Terms & Condition: </b>" + heroDealsModel.trems_and_conditions + ""));
        img_loading1.setVisibility(View.VISIBLE);
        new Handler(Looper.getMainLooper()).post(new Runnable()
        {
            @Override
            public void run()
            {
                AnimationDrawable frameAnimation = (AnimationDrawable) img_loading1.getDrawable();
                frameAnimation.setCallback(img_loading1);
                frameAnimation.setVisible(true, true);
                frameAnimation.start();
                ((Animatable) img_loading1.getDrawable()).start();
            }
        });
        imageLoader.loadImage(heroDealsModel.image, options, new ImageLoadingListener()
        {
            @Override
            public void onLoadingStarted(String imageUri, View view)
            {

            }

            @Override
            public void onLoadingFailed(String imageUri, View view, FailReason failReason)
            {
                img_c_item_image.setImageDrawable(getResources().getDrawable(R.drawable.deal_noimage));
                img_loading1.setVisibility(View.GONE);
            }

            @Override
            public void onLoadingComplete(String imageUri, View view, Bitmap loadedImage)
            {
                img_c_item_image.setImageBitmap(loadedImage);
                img_loading1.setVisibility(View.GONE);
            }

            @Override
            public void onLoadingCancelled(String imageUri, View view)
            {
            }
        });
    }

    @Override
    public void onClick(View v)
    {
        switch (v.getId())
        {
            case R.id.img_c_back:
                onBackPressed();
                break;
            case R.id.txt_c_read_more_vendor:
                AppDelegate.showAlert(VendorProfileActivity.this, vendorProfile != null ? vendorProfile.about_business : "");
                break;
            case R.id.img_c_share:
                downloadImageAndGetPath();
                break;
            case R.id.txt_c_comapny_contact_number:
                AppDelegate.makeCall(VendorProfileActivity.this, vendorProfile != null ? vendorProfile.contact_number : "");
                break;
            case R.id.txt_c_url:
                AppDelegate.openURL(VendorProfileActivity.this, txt_c_url.getText().toString());
                break;
            case R.id.txt_c_redeemnow:
                if (new Prefs(this).getUserdata() != null)
                {
//                    AppDelegate.LogT("new Prefs(this).getUserdata().membership_id==" + new Prefs(this).getUserdata().membership_id);
                    if (new Prefs(this).getUserdata().membership_id == 1)
                    {
                        startActivity(new Intent(this, UpgradeMembershipActivity.class));
                        //  AppDelegate.showToast(this, getString(R.string.please_upgrade_your_account));
                    }
                    else
                    {
                        if (heroDealsModel.redeem == 1)
                        {
                            AppDelegate.showToast(this, getString(R.string.already_redeemed_deal));
                        }
                        else
                        {
                            showAlert(this, getString(R.string.alert_hero), getString(R.string.alert_hero_message));
                        }
                    }
                }
                else
                {
                    Intent intent = new Intent(VendorProfileActivity.this, LoginActivity.class);
                    intent.putExtra(Tags.login_from, AppDelegate.LOGIN_FROM_VENDOR_PROFILE);
                    apiShouldcall = true;
                    startActivity(intent);
//                    showLoginAlert(this, "", getString(R.string.login_first));
                }
                break;
            case R.id.img_c_viewlocation:

                try
                {
                    Intent intent = new Intent(VendorProfileActivity.this, MapDetailActivity.class);
                    AppDelegate.LogT("sending lat and long==" + vendorProfile.latitude + ", " + vendorProfile.longitude);
                    intent.putExtra(Tags.person, new Person(new LatLng(Double.parseDouble(vendorProfile.latitude), Double.parseDouble(vendorProfile.longitude)), heroDealsModel.id, heroDealsModel.name, heroDealsModel, null, DEAL_HERO));
                    intent.putExtra(Tags.LAT, vendorProfile.latitude);
                    intent.putExtra(Tags.LNG, vendorProfile.longitude);
                    intent.putExtra(Tags.name, vendorProfile.contact_name);
                    intent.putExtra(Tags.country, vendorProfile.location_address);
                    intent.putExtra(Tags.state, vendorProfile.address2);
                    intent.putExtra(Tags.city, vendorProfile.address1);
                    intent.putExtra(Tags.image, vendorProfile.image);
                    intent.putExtra(Tags.company_name, vendorProfile.name);
                    final Pair<View, String>[] pairs = TransitionHelper.createSafeTransitionParticipants(VendorProfileActivity.this, false, new Pair<>(txt_c_titlename, "location"));
                    ActivityOptionsCompat transitionActivityOptions = ActivityOptionsCompat.makeSceneTransitionAnimation(VendorProfileActivity.this, pairs);
                    startActivity(intent, transitionActivityOptions.toBundle());
                }
                catch (Exception e)
                {
                    AppDelegate.LogE(e);
                }
                break;
        }
    }

    private void executeRedeemDealApi(int deal_id)
    {
        try
        {
            if (AppDelegate.haveNetworkConnection(this, true))
            {
                ArrayList<PostAysnc_Model> mPostArrayList = new ArrayList<PostAysnc_Model>();
                PostAsync mPostAsyncObj;
                AppDelegate.getInstance(this).setPostParamsSecond(mPostArrayList, Tags.API_KEY, Tags.API_KEY_VALUE);
                AppDelegate.getInstance(this).setPostParamsSecond(mPostArrayList, Tags.deal_id, deal_id, ServerRequestConstants.Key_PostintValue);
//                AppDelegate.getInstance(this).setPostParamsSecond(mPostArrayList, Tags.type, type, ServerRequestConstants.Key_PostintValue);
                if (new Prefs(VendorProfileActivity.this).getUserdata() != null)
                {
                    AppDelegate.getInstance(this).setPostParamsSecond(mPostArrayList, Tags.user_id, new Prefs(VendorProfileActivity.this).getUserdata().id);
                }

                mPostAsyncObj = new PostAsync(this, this, ServerRequestConstants.REDEEM_DEAL,
                        mPostArrayList, null);
                mHandler.sendEmptyMessage(10);
                mPostAsyncObj.execute();
            }
        }
        catch (Exception e)
        {
            AppDelegate.LogE(e);
            AppDelegate.showToast(this, getString(R.string.try_again));
        }
    }

    private void executeVendorProfileApi()
    {
        try
        {
            apiShouldcall = false;
            if (AppDelegate.haveNetworkConnection(this, true))
            {
                ArrayList<PostAysnc_Model> mPostArrayList = new ArrayList<>();
                PostAsync mPostAsyncObj;
                AppDelegate.getInstance(this).setPostParamsSecond(mPostArrayList, Tags.API_KEY, Tags.API_KEY_VALUE);
                AppDelegate.getInstance(this).setPostParamsSecond(mPostArrayList, "deal_id", deal_id);
                //Log.e("deal_id", deal_id+"");
                //Log.e(Tags.API_KEY,Tags.API_KEY_VALUE);

                AppDelegate.getInstance(this).setPostParamsSecond(mPostArrayList, Tags.vendor_id, vendor_id, ServerRequestConstants.Key_PostintValue);
                //Log.e(Tags.vendor_id,vendor_id+"");

                CityModel cityModel = new Prefs(this).getCitydata();
                if (AppDelegate.isValidString(str_lat) && AppDelegate.isValidString(str_long))
                {
                    AppDelegate.getInstance(this).setPostParamsSecond(mPostArrayList, Tags.LAT, str_lat);
                    AppDelegate.getInstance(this).setPostParamsSecond(mPostArrayList, Tags.LONG, str_long);
                    //Log.e("lat long ",str_lat+" "+str_long);

                }
                else if (cityModel != null && AppDelegate.isValidString(cityModel.lat) && AppDelegate.isValidString(cityModel.lat))
                {
                    AppDelegate.getInstance(this).setPostParamsSecond(mPostArrayList, Tags.LAT, cityModel.lat);
                    AppDelegate.getInstance(this).setPostParamsSecond(mPostArrayList, Tags.LONG, cityModel.lng);
                    //Log.e("lat long ",cityModel.lat+" "+cityModel.lng);
                }
                if (new Prefs(VendorProfileActivity.this).getUserdata() != null)
                {
                    AppDelegate.getInstance(this).setPostParamsSecond(mPostArrayList, Tags.user_id, new Prefs(VendorProfileActivity.this).getUserdata().id);
                    //Log.e(Tags.user_id ,new Prefs(VendorProfileActivity.this).getUserdata().id+"");
                }

                mPostAsyncObj = new PostAsync(this, this, ServerRequestConstants.VENDOR_PROFILE,
                        mPostArrayList, null);
                if (mHandler == null)
                {
                    setHandler();
                }
                mHandler.sendEmptyMessage(10);
                mPostAsyncObj.execute();
            }
        }
        catch (Exception e)
        {
            AppDelegate.LogE(e);
            AppDelegate.showToast(this, getString(R.string.try_again));
        }
    }

    @Override
    public void onStart()
    {
        super.onStart();
    }

    @Override
    public void onStop()
    {
        super.onStop();
    }


    public void showAlert(Context mContext, String Title, String Message)
    {
        try
        {
            alert = new AlertDialog.Builder(mContext);
            alert.setCancelable(false);
            alert.setTitle(Title);
            alert.setMessage(Message);
            alert.setPositiveButton(
                    Tags.OK,
                    new DialogInterface.OnClickListener()
                    {

                        @Override
                        public void onClick(DialogInterface dialog, int which)
                        {
                            executeRedeemDealApi(heroDealsModel.id);
                            dialog.dismiss();
                        }
                    }).setNegativeButton(Tags.CANCEL, new DialogInterface.OnClickListener()
            {
                @Override
                public void onClick(DialogInterface dialog, int which)
                {
                    dialog.dismiss();
                }
            });
            alert.show();
        }
        catch (Exception e)
        {
            AppDelegate.LogE(e);
        }
    }

    public void showLoginAlert(Context mContext, String Title, String
            Message)
    {
        try
        {
            alert = new AlertDialog.Builder(mContext);
            alert.setCancelable(false);
            alert.setMessage(Message);
            alert.setPositiveButton(
                    "LOGIN",
                    new DialogInterface.OnClickListener()
                    {

                        @Override
                        public void onClick(DialogInterface dialog, int which)
                        {
                            Intent intent = new Intent(VendorProfileActivity.this, LoginActivity.class);
                            intent.putExtra(Tags.login_from, AppDelegate.LOGIN_FROM_VENDOR_PROFILE);
                            apiShouldcall = true;
                            startActivity(intent);
                            dialog.dismiss();
                        }
                    }).setNegativeButton(Tags.CANCEL, new DialogInterface.OnClickListener()
            {
                @Override
                public void onClick(DialogInterface dialog, int which)
                {
                    dialog.dismiss();
                }
            });
            alert.show();
        }
        catch (Exception e)
        {
            AppDelegate.LogE(e);
        }
    }

    @Override
    public void setOnListItemClickListener(String name, int position)
    {
        if (name.equalsIgnoreCase(Tags.LOGIN))
        {
            Intent intent = new Intent(VendorProfileActivity.this, LoginActivity.class);
            intent.putExtra(Tags.login_from, AppDelegate.LOGIN_FROM_VENDOR_PROFILE);
            apiShouldcall = true;
            startActivity(intent);
//            showLoginAlert(this, "", getString(R.string.login_first));
        }
    }

    @Override
    public void setOnListItemClickListener(String name, int position, boolean like_dislike)
    {

    }

    private android.widget.ImageView[] dots;
    private android.widget.ImageView dots_Hero[];
    private int dotsCount, dotsCountHero, item_position = 0, selected_tab = 0, click_type = 0;

    private void setUiPageViewController()
    {
        pager_indicator.removeAllViews();
        dotsCount = bannerFragment.size();
        if (dotsCount > 0)
        {
            dots = new android.widget.ImageView[dotsCount];
            for (int i = 0; i < dotsCount; i++)
            {
                dots[i] = new android.widget.ImageView(VendorProfileActivity.this);
                dots[i].setImageDrawable(getResources().getDrawable(R.drawable.white_radius_square));
                android.widget.LinearLayout.LayoutParams params = new android.widget.LinearLayout.LayoutParams(AppDelegate.getInstance(VendorProfileActivity.this).dpToPix(this, 7), AppDelegate.getInstance(VendorProfileActivity.this).dpToPix(this, 7));
                params.setMargins(AppDelegate.getInstance(VendorProfileActivity.this).dpToPix(this, 3), 0, AppDelegate.getInstance(VendorProfileActivity.this).dpToPix(this, 3), 0);
                pager_indicator.addView(dots[i], params);
            }
            dots[0].setImageDrawable(getResources().getDrawable(R.drawable.yellow_radius_square));
        }
        view_pager.setOnPageChangeListener(new ViewPager.OnPageChangeListener()
        {
            @Override
            public void onPageScrolled(int position, float positionOffset, int positionOffsetPixels)
            {
            }

            @Override
            public void onPageSelected(int position)
            {
                switchBannerPage(position);
            }

            @Override
            public void onPageScrollStateChanged(int state)
            {
            }
        });
        switchBannerPage(0);
    }

    private void switchBannerPage(int position)
    {
        if (dotsCount > 0)
        {
            for (int i = 0; i < dotsCount; i++)
            {
                dots[i].setImageResource(R.drawable.white_radius_square);
            }
            dots[position].setImageResource(R.drawable.yellow_radius_square);
        }
    }

    private void switchBannerPage_Hero(int position)
    {
        if (dotsCountHero > 0)
        {
            for (int i = 0; i < dotsCountHero; i++)
            {
                dots_Hero[i].setImageResource(R.drawable.white_radius_square);
            }
            dots_Hero[position].setImageResource(R.drawable.yellow_radius_square);
        }
    }

    void setBanner(ArrayList<String> imageArray)
    {
        for (int i = 0; i < imageArray.size(); i++)
        {
            Fragment fragment = new BannerFragment();
            Bundle bundle = new Bundle();
            bundle.putString(Tags.DATA, imageArray.get(i));
            fragment.setArguments(bundle);
            bannerFragment.add(fragment);
        }
        AppDelegate.LogT("bannerFragment. size==>" + bannerFragment.size() + "");
        bannerPagerAdapter = new PagerAdapter(getSupportFragmentManager(), bannerFragment);
        view_pager.setAdapter(bannerPagerAdapter);
        setUiPageViewController();
    }

    void setBanner_hero(ArrayList<String> imageArray)
    {
        for (int i = 0; i < imageArray.size(); i++)
        {
            Fragment fragment = new BannerFragment();
            Bundle bundle = new Bundle();
            bundle.putString(Tags.DATA, imageArray.get(i));
            fragment.setArguments(bundle);
            bannerFragment_hero.add(fragment);
        }
        AppDelegate.LogT("bannerFragment. size==>" + bannerFragment.size() + "");
        bannerPagerAdapter_hero = new PagerAdapter(getSupportFragmentManager(), bannerFragment_hero);
        view_pager_hero.setAdapter(bannerPagerAdapter_hero);
        setUiPageViewController_hero();
    }

    private void setUiPageViewController_hero()
    {
        pager_indicator_hero.removeAllViews();
        dotsCountHero = bannerFragment_hero.size();
        if (dotsCountHero > 0)
        {
            dots_Hero = new android.widget.ImageView[dotsCountHero];
            for (int i = 0; i < dotsCountHero; i++)
            {
                dots_Hero[i] = new android.widget.ImageView(VendorProfileActivity.this);
                dots_Hero[i].setImageDrawable(getResources().getDrawable(R.drawable.white_radius_square));
                android.widget.LinearLayout.LayoutParams params = new android.widget.LinearLayout.LayoutParams(AppDelegate.getInstance(VendorProfileActivity.this).dpToPix(this, 7), AppDelegate.getInstance(VendorProfileActivity.this).dpToPix(this, 7));
                params.setMargins(AppDelegate.getInstance(VendorProfileActivity.this).dpToPix(this, 3), 0, AppDelegate.getInstance(VendorProfileActivity.this).dpToPix(this, 3), 0);
                pager_indicator_hero.addView(dots_Hero[i], params);
            }
            dots_Hero[0].setImageDrawable(getResources().getDrawable(R.drawable.yellow_radius_square));
        }
        view_pager_hero.setOnPageChangeListener(new ViewPager.OnPageChangeListener()
        {
            @Override
            public void onPageScrolled(int position, float positionOffset, int positionOffsetPixels)
            {
            }

            @Override
            public void onPageSelected(int position)
            {
                switchBannerPage_Hero(position);
            }

            @Override
            public void onPageScrollStateChanged(int state)
            {
            }
        });
        switchBannerPage_Hero(0);
    }
}
