package com.craftedeals.activities;

import android.app.Activity;
import android.app.AlertDialog;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.graphics.Paint;
import android.graphics.drawable.Animatable;
import android.graphics.drawable.AnimationDrawable;
import android.graphics.drawable.Drawable;
import android.os.Build;
import android.os.Bundle;
import android.os.Handler;
import android.os.Looper;
import android.os.Message;
import android.support.annotation.Nullable;
import android.support.v4.app.ActivityOptionsCompat;
import android.support.v4.app.Fragment;
import android.support.v4.util.Pair;
import android.support.v4.view.ViewPager;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.DefaultItemAnimator;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.text.Html;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.Window;
import android.view.WindowManager;
import android.widget.RelativeLayout;
import android.widget.ScrollView;

import com.craftedeals.AppDelegate;
import com.craftedeals.Async.PostAsync;
import com.craftedeals.Models.AdHocDealsModel;
import com.craftedeals.Models.CategoryModel;
import com.craftedeals.Models.CityModel;
import com.craftedeals.Models.ItemsModel;
import com.craftedeals.Models.OpenHoursModel;
import com.craftedeals.Models.Person;
import com.craftedeals.Models.PostAysnc_Model;
import com.craftedeals.R;
import com.craftedeals.Utils.CircleImageView;
import com.craftedeals.Utils.DateUtils;
import com.craftedeals.Utils.Prefs;
import com.craftedeals.Utils.TransitionHelper;
import com.craftedeals.adapters.AdHocDealsDetailItemAdapter;
import com.craftedeals.adapters.PagerAdapter;
import com.craftedeals.constants.ServerRequestConstants;
import com.craftedeals.constants.Tags;
import com.craftedeals.fragments.BannerFragment;
import com.craftedeals.interfaces.OnItemClick;
import com.craftedeals.interfaces.OnReciveServerResponse;
import com.craftedeals.parser.JSONParser;
import com.github.aakira.expandablelayout.ExpandableRelativeLayout;
import com.google.android.gms.maps.CameraUpdateFactory;
import com.google.android.gms.maps.GoogleMap;
import com.google.android.gms.maps.OnMapReadyCallback;
import com.google.android.gms.maps.SupportMapFragment;
import com.google.android.gms.maps.model.BitmapDescriptorFactory;
import com.google.android.gms.maps.model.CameraPosition;
import com.google.android.gms.maps.model.LatLng;
import com.google.android.gms.maps.model.Marker;
import com.google.android.gms.maps.model.MarkerOptions;
import com.nostra13.universalimageloader.core.DisplayImageOptions;
import com.nostra13.universalimageloader.core.ImageLoader;
import com.nostra13.universalimageloader.core.ImageLoaderConfiguration;
import com.nostra13.universalimageloader.core.assist.FailReason;
import com.nostra13.universalimageloader.core.listener.ImageLoadingListener;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.io.IOException;
import java.io.InputStream;
import java.net.URL;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.List;

import carbon.widget.ImageView;
import carbon.widget.LinearLayout;
import carbon.widget.TextView;

import static com.craftedeals.activities.DealsActivity.DEAL_ADHOC;
import static com.craftedeals.activities.DealsActivity.DEAL_HERO;

/**
 * Created by Heena on 07-Oct-16.
 */
public class AdHocDealDetailActivity extends AppCompatActivity implements View.OnClickListener, OnReciveServerResponse, OnItemClick {
    public AlertDialog.Builder alert;
    private Prefs prefs;

    private TextView txt_c_distance, txt_c_comapny_open_hour, txt_c_comapny_contact_number, txt_c_expiry_label, txt_c_expiry_date, txt_c_about_me, txt_c_start_date_label, txt_c_start_date, txt_c_end_date_label, txt_c_end_date, txt_c_deal_prize_real, txt_c_deal_prize_offer, txt_c_buy_now, txt_c_select_from, txt_c_total_amount, txt_c_about_me_vendor, txt_c_title, txt_c_deal_prize_like, txt_c_company_name, txt_c_comapny_address, txt_c_facilities, txt_c_things_to_remember, txt_c_use_the_offer;
    public static Handler mHandler;
    private SupportMapFragment fragment;
    private GoogleMap googleMap;
    private RecyclerView recycler_options;
    private ImageView img_c_expand_collapse, img_c_like_dislike;
    android.widget.ImageView img_loading1, img_loading2;
    public ExpandableRelativeLayout exp_layout;
    private ScrollView scrollview;
    int deal_id = 0;
    private AdHocDealsModel adHocDealModel;
    private ImageLoader imageLoader = ImageLoader.getInstance();
    private DisplayImageOptions options = new DisplayImageOptions.Builder().cacheInMemory(true)
            .cacheOnDisc(true).resetViewBeforeLoading(true).
                    build();
    CircleImageView img_circle_deal_image;
    RelativeLayout rl_c_select_from;
    ArrayList<ItemsModel> arrayItems;
    String total_amount_string = "0";
    ArrayList<ItemsModel> itemsModelArrayList;
    long realm_index_id = 0;
    LinearLayout ll_c_validation;
    private boolean apiShouldcall = false;
    private PagerAdapter bannerPagerAdapter;
    private List<Fragment> bannerFragment = new ArrayList();

    android.widget.LinearLayout pager_indicator;
    ViewPager view_pager;
    private Marker customMarker;
    public static Activity mActivity;
//    private CameraPosition cameraPosition;

    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.LOLLIPOP) {
            Window window = this.getWindow();
            Drawable background = this.getResources().getDrawable(R.drawable.topbg);
            window.addFlags(WindowManager.LayoutParams.FLAG_DRAWS_SYSTEM_BAR_BACKGROUNDS);
            window.setStatusBarColor(getResources().getColor(android.R.color.transparent));
            window.setNavigationBarColor(getResources().getColor(android.R.color.transparent));
            window.setBackgroundDrawable(background);
        }
        setContentView(R.layout.adhocdeals_details);
        mActivity = this;
        imageLoader.init(ImageLoaderConfiguration.createDefault(this));
        prefs = new Prefs(this);
        deal_id = getIntent().getIntExtra(Tags.deal_id, 0);
        initView();
        setHandler();
        if (getIntent().getExtras() != null && getIntent().getExtras().getParcelable(Tags.deal) != null) {
            adHocDealModel = getIntent().getExtras().getParcelable(Tags.deal);
//            showAdHocDetail(adHocDealModel);
        }
        if (getIntent() != null && AppDelegate.isValidString(getIntent().getStringExtra(Tags.FROM))) {
            if (getIntent().getStringExtra(Tags.FROM).equalsIgnoreCase(Tags.notification))
                execute_ViewNotificationApi(getIntent().getIntExtra(Tags.notification_id, 0));
        }
        new Handler().postDelayed(new Runnable() {
            @Override
            public void run() {
                scrollview.fullScroll(ScrollView.FOCUS_UP);
                fragment = SupportMapFragment.newInstance();
                getSupportFragmentManager().beginTransaction()
                        .replace(R.id.map_container, fragment, "MAP1").addToBackStack(null)
                        .commit();
                fragment.getMapAsync(new OnMapReadyCallback() {
                    @Override
                    public void onMapReady(GoogleMap googleMap) {
                        AdHocDealDetailActivity.this.googleMap = googleMap;
                        AppDelegate.LogT("Google Map ==" + AdHocDealDetailActivity.this.googleMap);
                        scrollview.fullScroll(ScrollView.FOCUS_UP);
                    }
                });
            }
        }, 600);
        mHandler.sendEmptyMessage(3);
    }

    private void execute_ViewNotificationApi(int notification_id) {
        try {
            if (AppDelegate.haveNetworkConnection(this, true)) {
                ArrayList<PostAysnc_Model> mPostArrayList = new ArrayList<PostAysnc_Model>();
                PostAsync mPostAsyncObj;
                AppDelegate.getInstance(this).setPostParamsSecond(mPostArrayList, Tags.API_KEY, Tags.API_KEY_VALUE);
                AppDelegate.getInstance(this).setPostParamsSecond(mPostArrayList, Tags.user_id, new Prefs(this).getUserdata().id);
                AppDelegate.getInstance(this).setPostParamsSecond(mPostArrayList, Tags.nid, notification_id);

                mPostAsyncObj = new PostAsync(this, this, ServerRequestConstants.VIEW_NOTIFICATION,
                        mPostArrayList, null);
                mHandler.sendEmptyMessage(10);
                mPostAsyncObj.execute();
            }
        } catch (Exception e) {
            AppDelegate.LogE(e);
            AppDelegate.showToast(this, getString(R.string.try_again));
        }
    }

    @Override
    protected void onDestroy() {
        super.onDestroy();
        mHandler = null;
        mActivity = null;
    }

    private void show_DealsList(ArrayList<ItemsModel> modelArrayList) {
        AdHocDealsDetailItemAdapter mAdapter = new AdHocDealsDetailItemAdapter(this, modelArrayList, this);
        RecyclerView.LayoutManager mLayoutManager = new LinearLayoutManager(this);
        setListViewHeightBasedOnChildren(recycler_options);
        recycler_options.setLayoutManager(mLayoutManager);
        recycler_options.setItemAnimator(new DefaultItemAnimator());
        recycler_options.setAdapter(mAdapter);
    }

    private void initView() {
        txt_c_distance = (TextView) findViewById(R.id.txt_c_distance);
        txt_c_end_date = (TextView) findViewById(R.id.txt_c_end_date);
        txt_c_start_date = (TextView) findViewById(R.id.txt_c_start_date);
        txt_c_start_date_label = (TextView) findViewById(R.id.txt_c_start_date_label);
        txt_c_end_date_label = (TextView) findViewById(R.id.txt_c_end_date_label);
        ll_c_validation = (LinearLayout) findViewById(R.id.ll_c_validation);
        img_loading1 = (android.widget.ImageView) findViewById(R.id.img_loading1);
        findViewById(R.id.img_c_back).setOnClickListener(this);
        findViewById(R.id.img_c_share).setOnClickListener(this);
        txt_c_use_the_offer = (TextView) findViewById(R.id.txt_c_use_the_offer);
        txt_c_things_to_remember = (TextView) findViewById(R.id.txt_c_things_to_remember);
        txt_c_facilities = (TextView) findViewById(R.id.txt_c_facilities);
        txt_c_deal_prize_real = (TextView) findViewById(R.id.txt_c_deal_prize_real);
        txt_c_deal_prize_offer = (TextView) findViewById(R.id.txt_c_deal_prize_offer);
        txt_c_expiry_date = (TextView) findViewById(R.id.txt_c_expiry_date);
        txt_c_expiry_label = (TextView) findViewById(R.id.txt_c_expiry_label);
        txt_c_buy_now = (TextView) findViewById(R.id.txt_c_buy_now);
        txt_c_about_me = (TextView) findViewById(R.id.txt_c_about_me);
        txt_c_comapny_contact_number = (TextView) findViewById(R.id.txt_c_comapny_contact_number);
        txt_c_comapny_contact_number.setOnClickListener(this);
        txt_c_comapny_open_hour = (TextView) findViewById(R.id.txt_c_comapny_open_hour);
        txt_c_buy_now.setOnClickListener(this);
        txt_c_title = (TextView) findViewById(R.id.txt_c_title);
        txt_c_deal_prize_like = (TextView) findViewById(R.id.txt_c_deal_prize_like);
        recycler_options = (RecyclerView) findViewById(R.id.recycler_options);
        recycler_options.setNestedScrollingEnabled(false);
        txt_c_deal_prize_real.setPaintFlags(txt_c_deal_prize_real.getPaintFlags() | Paint.STRIKE_THRU_TEXT_FLAG);
        txt_c_deal_prize_real.setPaintFlags(txt_c_deal_prize_real.getPaintFlags() | Paint.LINEAR_TEXT_FLAG);
        img_c_expand_collapse = (ImageView) findViewById(R.id.img_c_expand_collapse);
        img_c_expand_collapse.setOnClickListener(this);
        img_c_expand_collapse.setSelected(false);
        view_pager = (ViewPager) findViewById(R.id.view_pager);
        pager_indicator = (android.widget.LinearLayout) findViewById(R.id.pager_indicator);
        img_circle_deal_image = (CircleImageView) findViewById(R.id.img_circle_deal_image);
        img_loading2 = (android.widget.ImageView) findViewById(R.id.img_loading2);
        img_c_like_dislike = (ImageView) findViewById(R.id.img_c_like_dislike);
        txt_c_company_name = (TextView) findViewById(R.id.txt_c_company_name);
        txt_c_comapny_address = (TextView) findViewById(R.id.txt_c_comapny_address);
        txt_c_total_amount = (TextView) findViewById(R.id.txt_c_total_amount);
        txt_c_about_me_vendor = (TextView) findViewById(R.id.txt_c_about_me_vendor);
        txt_c_select_from = (TextView) findViewById(R.id.txt_c_select_from);
        rl_c_select_from = (RelativeLayout) findViewById(R.id.rl_c_select_from);
        img_c_like_dislike.setOnClickListener(this);
        scrollview = (ScrollView) findViewById(R.id.scrollview);
        scrollview.fullScroll(ScrollView.FOCUS_UP);
        findViewById(R.id.txt_c_view_profile).setOnClickListener(this);
        exp_layout = (ExpandableRelativeLayout) findViewById(R.id.exp_layout);
        setUiPageViewController();
        findViewById(R.id.view_map).setOnClickListener(this);
        new Handler().postDelayed(new Runnable() {
            @Override
            public void run() {
                showMap();
            }
        }, 1500);
        img_c_like_dislike.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (new Prefs(AdHocDealDetailActivity.this).getUserdata() != null) {
                    AppDelegate.LogT("img_c_like_dislike.isSelected()=" + img_c_like_dislike.isSelected());
                    if (img_c_like_dislike.isSelected()) {
                        if (adHocDealModel != null)
                            execute_favouriteApi(AppDelegate.DISLIKE, adHocDealModel.id, new Prefs(AdHocDealDetailActivity.this).getUserdata().id);
                    } else {
                        if (adHocDealModel != null)
                            execute_favouriteApi(AppDelegate.LIKE, adHocDealModel.id, new Prefs(AdHocDealDetailActivity.this).getUserdata().id);
                    }
                } else {
                    Intent intent = new Intent(AdHocDealDetailActivity.this, LoginActivity.class);
                    intent.putExtra(Tags.login_from, AppDelegate.LOGIN_FROM_VENDOR_PROFILE);
                    apiShouldcall = true;
                    startActivity(intent);                 // showLoginAlert(AdHocDealDetailActivity.this, "", getString(R.string.login_first));
                    //AppDelegate.showToast(AdHocDealDetailActivity.this, getResources().getString(R.string.login_first));
                }
            }
        });
        txt_c_total_amount.setText("Total Amount: $ " + "0");
//        if (new Prefs(AdHocDealDetailActivity.this).getUserdata() == null || new Prefs(AdHocDealDetailActivity.this).getUserdata().id <= 0) {
//            txt_c_buy_now.setText(getString(R.string.login_or_join_to_access));
//        } else
        txt_c_buy_now.setText("BUY NOW");

    }

    public static final int COLLAPSE = 0, EXPAND = 1;

    public void expandableView(final ExpandableRelativeLayout expLayout, final int value) {
        expLayout.post(new Runnable() {
            @Override
            public void run() {
                if (value == COLLAPSE) {
                    expLayout.collapse();
                } else {
                    expLayout.expand();
                }
            }
        });
    }

    private void showMap() {
        if (googleMap == null) {
            return;
        }
        googleMap.setMyLocationEnabled(true);
        googleMap.getUiSettings().setMapToolbarEnabled(false);
        googleMap.getUiSettings().setZoomControlsEnabled(false);
        googleMap.getUiSettings().setCompassEnabled(false);
        googleMap.setMapType(GoogleMap.MAP_TYPE_NORMAL);
        googleMap.moveCamera(CameraUpdateFactory.zoomIn());
    }

    @Override
    public void onBackPressed() {
        try {
            super.onBackPressed();
            super.onBackPressed();
        } catch (Exception e) {
            AppDelegate.LogE(e);
        }
    }

    private void setHandler() {
        mHandler = new Handler() {
            @Override
            public void dispatchMessage(Message msg) {
                super.dispatchMessage(msg);
                switch (msg.what) {
                    case 10:
                        AppDelegate.showProgressDialog(AdHocDealDetailActivity.this);
                        break;
                    case 11:
                        AppDelegate.hideProgressDialog(AdHocDealDetailActivity.this);
                        break;
                    case 1:
                        break;
                    case 2:
                        if (adHocDealModel != null) {
                            if (adHocDealModel.liked == AppDelegate.DISLIKE) {
                                img_c_like_dislike.setSelected(false);
                            } else if (adHocDealModel.liked == AppDelegate.LIKE) {
                                img_c_like_dislike.setSelected(true);
                            }
                        }
                        break;
                    case 3:
                        execute_getAdhocDealsDetails();
                        break;
                    case 4:
                        AppDelegate.LogT("onBackPressed called");
                        new Handler().postDelayed(new Runnable() {
                            @Override
                            public void run() {
                                onBackPressed();
                            }
                        }, 500);

                        break;
                }
            }
        };
    }

    private void execute_getAdhocDealsDetails() {
        try {
            apiShouldcall = false;
            if (AppDelegate.haveNetworkConnection(this, true)) {
                ArrayList<PostAysnc_Model> mPostArrayList = new ArrayList<PostAysnc_Model>();
                AppDelegate.getInstance(this).setPostParamsSecond(mPostArrayList, Tags.API_KEY, Tags.API_KEY_VALUE);
                AppDelegate.getInstance(this).setPostParamsSecond(mPostArrayList, Tags.dealId, deal_id, ServerRequestConstants.Key_PostintValue);
                CityModel cityModel = new Prefs(this).getCitydata();
                if (cityModel != null && AppDelegate.isValidString(cityModel.lat) && AppDelegate.isValidString(cityModel.lat)) {
                    AppDelegate.getInstance(this).setPostParamsSecond(mPostArrayList, Tags.LAT, cityModel.lat);
                    AppDelegate.getInstance(this).setPostParamsSecond(mPostArrayList, Tags.LONG, cityModel.lng);
                }
                if (new Prefs(this).getUserdata() != null)
                    AppDelegate.getInstance(this).setPostParamsSecond(mPostArrayList, Tags.user_id, new Prefs(this).getUserdata().id, ServerRequestConstants.Key_PostintValue);

                PostAsync mPostAsyncObj;
                mPostAsyncObj = new PostAsync(this, this, ServerRequestConstants.GET_ADHOC_DETAILS,
                        mPostArrayList, null);
                if (mHandler == null)
                    setHandler();
                mHandler.sendEmptyMessage(10);
                mPostAsyncObj.execute();
            }
        } catch (Exception e) {
            AppDelegate.showToast(this, getResources().getString(R.string.try_again));
            AppDelegate.LogE(e);
        }
    }

    @Override
    public void onClick(View v) {
        switch (v.getId()) {
            case R.id.img_c_back:
                onBackPressed();
                onBackPressed();
                break;
            case R.id.img_c_share:
                downloadImageAndGetPath();
                break;
            case R.id.img_c_expand_collapse:
                AppDelegate.LogT("img_c_expand_collapse.isSelected() => " + img_c_expand_collapse.isSelected());
                if (img_c_expand_collapse.isSelected()) {
                    img_c_expand_collapse.setSelected(false);
                    expandableView(exp_layout, EXPAND);
                } else {
                    img_c_expand_collapse.setSelected(true);
                    expandableView(exp_layout, COLLAPSE);
                }
                break;
            case R.id.img_c_like_dislike:
                mHandler.sendEmptyMessage(2);
                break;
            case R.id.txt_c_map:
                //if (!(getSupportFragmentManager().findFragmentById(R.id.map_container) instanceof DealsMapFragment))
                break;
            case R.id.txt_c_list:
                //if (!(getSupportFragmentManager().findFragmentById(R.id.list_container) instanceof DealsListFragment))
                break;
            case R.id.list_container:
                break;
            case R.id.view_map:
                Intent intent = new Intent(AdHocDealDetailActivity.this, MapDetailActivity.class);
                intent.putExtra(Tags.person, new Person(new LatLng(Double.parseDouble(adHocDealModel.latitude), Double.parseDouble(adHocDealModel.longitude)), adHocDealModel.id, adHocDealModel.name, null, adHocDealModel, DEAL_ADHOC));
                AppDelegate.LogT("sending adHocDealModel lat and long==" + adHocDealModel.latitude + ", " + adHocDealModel.longitude);
                intent.putExtra(Tags.LAT, adHocDealModel.latitude);
                intent.putExtra(Tags.LNG, adHocDealModel.longitude);
                intent.putExtra(Tags.name, adHocDealModel.vendor_name);
                intent.putExtra(Tags.country, adHocDealModel.company_address);
                intent.putExtra(Tags.state, adHocDealModel.address2);
                intent.putExtra(Tags.city, adHocDealModel.address1);
                intent.putExtra(Tags.image, adHocDealModel.company_image);
                intent.putExtra(Tags.company_name, adHocDealModel.company_name);
                startActivity(intent);
                break;
            case R.id.txt_c_buy_now:
                if (new Prefs(AdHocDealDetailActivity.this).getUserdata() == null || new Prefs(AdHocDealDetailActivity.this).getUserdata().id <= 0) {
                    intent = new Intent(AdHocDealDetailActivity.this, LoginActivity.class);
                    intent.putExtra(Tags.login_from, AppDelegate.LOGIN_FROM_VENDOR_PROFILE);
                    apiShouldcall = true;
                    startActivity(intent);
//                    showLoginAlert(this, "", getString(R.string.login_first));
                    // AppDelegate.showAlert(AdHocDealDetailActivity.this, getResources().getString(R.string.login_before_purchase_deal));
                } else if (itemsModelArrayList != null && itemsModelArrayList.size() > 0) {
                    int count = 0;
                    for (int i = 0; i < itemsModelArrayList.size(); i++) {
                        count = count + Integer.parseInt(itemsModelArrayList.get(i).item_count);
                    }
                    if (count == 0) {
                        AppDelegate.showAlert(AdHocDealDetailActivity.this, getResources().getString(R.string.add_item_to_cart));
                    } else {
                        intent = (new Intent(AdHocDealDetailActivity.this, OrderSummaryActivity.class));
                        intent.putParcelableArrayListExtra(Tags.items, itemsModelArrayList);
                        intent.putExtra(Tags.amount, total_amount_string);
                        intent.putExtra(Tags.adhoc_deal_id, adHocDealModel.id + "");
                        intent.putExtra(Tags.image, adHocDealModel.image + "");
                        intent.putExtra(Tags.deal, adHocDealModel.company_name + "");
                        startActivity(intent);
                    }
                } else {
                    AppDelegate.showAlert(AdHocDealDetailActivity.this, getResources().getString(R.string.add_item_to_cart));
                }

                break;
            case R.id.txt_c_view_profile:
                intent = new Intent(this, VendorProfileActivity.class);
                final Pair<View, String>[] pairs = TransitionHelper.createSafeTransitionParticipants(this, false, new Pair<>(txt_c_title, "square_blue_name_1"));
                ActivityOptionsCompat transitionActivityOptions = ActivityOptionsCompat.makeSceneTransitionAnimation(this, pairs);
                intent.putExtra(Tags.deal_id, adHocDealModel.id);
                intent.putExtra(Tags.vendor_id, adHocDealModel.user_id);
                intent.putExtra(Tags.type, 2);
                startActivity(intent/*, transitionActivityOptions.toBundle()*/);
                break;
            case R.id.txt_c_comapny_contact_number:
                AppDelegate.makeCall(AdHocDealDetailActivity.this, adHocDealModel != null ? adHocDealModel.company_contact_number : "");
                break;
        }
    }

    private void execute_favouriteApi(int like, int id, int user_id) {
        try {
            if (AppDelegate.haveNetworkConnection(this, true)) {
                ArrayList<PostAysnc_Model> mPostArrayList = new ArrayList<PostAysnc_Model>();
                AppDelegate.getInstance(this).setPostParamsSecond(mPostArrayList, Tags.API_KEY, Tags.API_KEY_VALUE);
                AppDelegate.getInstance(this).setPostParamsSecond(mPostArrayList, Tags.deal_id, id);
                AppDelegate.getInstance(this).setPostParamsSecond(mPostArrayList, Tags.user_id, user_id);
                AppDelegate.getInstance(this).setPostParamsSecond(mPostArrayList, Tags.status, like, ServerRequestConstants.Key_PostintValue);
                PostAsync mPostAsyncObj;
                mPostAsyncObj = new PostAsync(this, this, ServerRequestConstants.ADHOC_LIKE,
                        mPostArrayList, null);
                mHandler.sendEmptyMessage(10);
                mPostAsyncObj.execute();
            }
        } catch (Exception e) {
            AppDelegate.showToast(this, getResources().getString(R.string.try_again));
            AppDelegate.LogE(e);
        }
    }

    @Override
    protected void onResume() {
        super.onResume();
        if (apiShouldcall)
            execute_getAdhocDealsDetails();
    }

    @Override
    public void setOnReciveResult(String apiName, String result) {
        if (mHandler == null) {
            return;
        }
        mHandler.sendEmptyMessage(11);
        if (!AppDelegate.isValidString(result)) {
            AppDelegate.showToast(AdHocDealDetailActivity.this, getResources().getString(R.string.time_out));
            return;
        }
        if (apiName.equals(ServerRequestConstants.GET_ADHOC_DETAILS)) {
            parseAdhocDetail(result);
        } else if (apiName.equalsIgnoreCase(ServerRequestConstants.ADHOC_LIKE)) {
            parseAdHocLikes(result);
        } else if (apiName.equalsIgnoreCase(ServerRequestConstants.VIEW_NOTIFICATION)) {
            parseViewNotification(result);
        }
    }

    private void parseViewNotification(String result) {
        try {
            JSONObject jsonObject = new JSONObject(result);
//            if (jsonObject.getInt(Tags.status) == 1) {
//                AppDelegate.showToast(this, jsonObject.getString(Tags.message));
//            } else {
//                AppDelegate.showToast(this, jsonObject.getString(Tags.message));
//            }
        } catch (JSONException e) {
            AppDelegate.LogE(e);
        }
    }


    private void parseAdHocLikes(String result) {
        try {
            JSONObject jsonobj = new JSONObject(result);
            if (jsonobj.getInt(Tags.status) == 1 && jsonobj.getInt(Tags.dataFlow) == 1) {
                AppDelegate.showToast(AdHocDealDetailActivity.this, jsonobj.getString(Tags.response));
                if (img_c_like_dislike.isSelected()) {
                    AppDelegate.LogT("img_c_like_dislike.isSelected()true==" + img_c_like_dislike.isSelected());
                    img_c_like_dislike.setSelected(false);
                    adHocDealModel.favourite -= 1;
                    txt_c_deal_prize_like.setText(adHocDealModel.favourite + "");
                } else {
                    AppDelegate.LogT("img_c_like_dislike.isSelected()false==" + img_c_like_dislike.isSelected());
                    img_c_like_dislike.setSelected(true);
                    adHocDealModel.favourite += 1;
                    txt_c_deal_prize_like.setText(adHocDealModel.favourite + "");
                }
                if (VendorProfileActivity.mHandler != null) {
                    VendorProfileActivity.mHandler.sendEmptyMessage(3);
                }
                if (DealsActivity.mHandler != null) {
                    DealsActivity.mHandler.sendEmptyMessage(4);
                }
                if (SearchedDealsActivity.mHandler != null) {
                    SearchedDealsActivity.mHandler.sendEmptyMessage(4);
                }
            } else {
                AppDelegate.showToast(AdHocDealDetailActivity.this, jsonobj.getString(Tags.message));
            }
        } catch (JSONException e) {
            AppDelegate.LogE(e);
        }
    }

    private void parseAdhocDetail(String result) {
        try {
            JSONObject jsonobj = new JSONObject(result);
            if (jsonobj.getInt(Tags.status) == 1 && jsonobj.getInt(Tags.dataFlow) == 1) {
                CategoryModel category = new CategoryModel();
                JSONObject heroobj = jsonobj.getJSONObject(Tags.response);
                adHocDealModel = new AdHocDealsModel();
                adHocDealModel.url = heroobj.getString(Tags.url);
                adHocDealModel.distance = heroobj.getString(Tags.distance);
                adHocDealModel.id = heroobj.getInt(Tags.id);
                adHocDealModel.user_id = heroobj.getInt(Tags.user_id);
                adHocDealModel.name = heroobj.getString(Tags.name);
                adHocDealModel.title = heroobj.getString(Tags.name);
                adHocDealModel.use_the_offer = heroobj.getString(Tags.use_the_offer);
                adHocDealModel.things_to_remebers = heroobj.getString(Tags.things_to_remebers);
                adHocDealModel.deal_category_id = heroobj.getInt(Tags.deal_category_id);
                adHocDealModel.sub_category_id = heroobj.getInt(Tags.sub_category_id);
                adHocDealModel.facilities = heroobj.getString(Tags.facilities);
                adHocDealModel.item_detail = heroobj.getString(Tags.item_detail);
                adHocDealModel.deal_type = heroobj.getInt(Tags.deal_type);
                adHocDealModel.starting_date = heroobj.getString(Tags.starting_date);
                adHocDealModel.expiry_date = heroobj.getString(Tags.expiry_date);
                adHocDealModel.price = heroobj.getInt(Tags.price);
                adHocDealModel.discount_cost = heroobj.getInt(Tags.discount_cost);
                try {
                    adHocDealModel.deal_price_real = heroobj.getInt(Tags.actual_cost);
                    adHocDealModel.deal_price_offer = heroobj.getInt(Tags.price);
                } catch (Exception e) {
                    AppDelegate.LogE(e);
                }
                adHocDealModel.total_deal = heroobj.getInt(Tags.total_deal);
                adHocDealModel.volume_of_deal = heroobj.getInt(Tags.volume_of_deal);
                adHocDealModel.timing_to = heroobj.getString(Tags.timing_to);
                adHocDealModel.timing_from = heroobj.getString(Tags.timing_from);
                adHocDealModel.valid_on = heroobj.getString(Tags.valid_on);
                adHocDealModel.vendor_delete_deal = heroobj.getInt(Tags.vendor_delete_deal);
                adHocDealModel.favourite = heroobj.getInt(Tags.favourite);
                if (heroobj.has(Tags.liked))
                    adHocDealModel.liked = heroobj.getInt(Tags.liked);
                if (AppDelegate.isValidString(heroobj.getString(Tags.user))) {
                    JSONObject vendor = heroobj.getJSONObject(Tags.user);
                    adHocDealModel.vendor_name = vendor.getString(Tags.name);
                    if (vendor.has(Tags.busines) && AppDelegate.isValidString(vendor.getString(Tags.busines))) {
                        adHocDealModel.latitude = vendor.getJSONObject(Tags.busines).getString(Tags.latitude);
                        adHocDealModel.longitude = vendor.getJSONObject(Tags.busines).getString(Tags.longitude);
                        adHocDealModel.about_me = vendor.getJSONObject(Tags.busines).getString(Tags.about_business);
                        adHocDealModel.company_address = vendor.getJSONObject(Tags.busines).getString(Tags.location_address);
                        adHocDealModel.company_name = vendor.getJSONObject(Tags.busines).getString(Tags.name);
                        adHocDealModel.address1 = vendor.getJSONObject(Tags.busines).getString(Tags.address1);
                        adHocDealModel.address2 = vendor.getJSONObject(Tags.busines).getString(Tags.address2);
                        adHocDealModel.company_image = vendor.getJSONObject(Tags.busines).getString(Tags.image);
                        adHocDealModel.company_opening_hours = vendor.getJSONObject(Tags.busines).getString(Tags.open_hours);
                        adHocDealModel.company_contact_number = vendor.getJSONObject(Tags.busines).getString(Tags.contact_number);
                        adHocDealModel.cityName = vendor.getJSONObject(Tags.busines).getString(Tags.cityName);
                    }
                }
                try {
                    if (AppDelegate.isValidString(heroobj.getString(Tags.images))) {
                        JSONArray img = heroobj.getJSONArray(Tags.images);
                        ArrayList<String> imgArray = new ArrayList<>();
                        if (img.length() > 0) {
                            for (int j = 0; j < img.length(); j++) {
                                imgArray.add(img.getJSONObject(j).getString(Tags.image));
                            }
                        } else {
                            if (heroobj.has(Tags.image))
                                imgArray.add(heroobj.getString(Tags.image));
                        }
                        setBanner(imgArray);
                    }
                } catch (Exception e) {
                    AppDelegate.LogE(e);
                }

                if (heroobj.has(Tags.image))
                    adHocDealModel.image = heroobj.getString(Tags.image);

                JSONArray itemsArray = heroobj.getJSONArray(Tags.items);

                try {
                    if (itemsArray.length() > 0) {
                        for (int i = 0; i < itemsArray.length(); i++) {
                            arrayItems = new ArrayList<>();
                            JSONObject object = itemsArray.getJSONObject(i);
                            ItemsModel itemsModel = new ItemsModel();
                            itemsModel.id = JSONParser.getInt(object, Tags.id);
                            itemsModel.adhocdeal_id = JSONParser.getInt(object, Tags.adhocdeal_id);
                            itemsModel.name = JSONParser.getString(object, Tags.name);
                            itemsModel.actual_cost = JSONParser.getInt(object, Tags.actual_cost);
                            itemsModel.price = JSONParser.getInt(object, Tags.price);
                            itemsModel.discount_cost = JSONParser.getInt(object, Tags.discount_cost);
                            itemsModel.details = JSONParser.getString(object, Tags.details);
                            itemsModel.created = JSONParser.getString(object, Tags.created);
                            itemsModel.validation_type = adHocDealModel.deal_type;
                            if (adHocDealModel.deal_type == 1) {
                                itemsModel.available_deal = adHocDealModel.volume_of_deal - adHocDealModel.total_deal;
                            }
                            arrayItems.add(itemsModel);
                        }
                    }
                } catch (Exception e) {
                    AppDelegate.LogE(e);
                }
                adHocDealModel.category = heroobj.getJSONObject(Tags.deal_category).getString(Tags.title);
                adHocDealModel.markerImage = heroobj.getJSONObject(Tags.deal_category).getString(Tags.marker_icon);
                showAdHocDetail(adHocDealModel);
            } else {
                AppDelegate.showToast(this, jsonobj.getString(Tags.message));
                onBackPressed();
                onBackPressed();
            }

        } catch (JSONException e) {
            AppDelegate.LogE(e);
        }
    }


    public void downloadImageAndGetPath() {
        AppDelegate.showProgressDialog(AdHocDealDetailActivity.this);

        if (AppDelegate.haveNetworkConnection(AdHocDealDetailActivity.this, true)) {
            Intent shareIntent = new Intent(android.content.Intent.ACTION_SEND);
            shareIntent.setType("text/plain");
            if (AppDelegate.isValidString(adHocDealModel.title)) {
                shareIntent.putExtra(android.content.Intent.EXTRA_SUBJECT, adHocDealModel.title);
                shareIntent.putExtra(Intent.EXTRA_REFERRER_NAME, adHocDealModel.title);
                shareIntent.putExtra(Intent.EXTRA_TITLE, adHocDealModel.title);
            }
            if (AppDelegate.isValidString(adHocDealModel.item_detail))
                shareIntent.putExtra(Intent.EXTRA_PROCESS_TEXT_READONLY, adHocDealModel.item_detail);
            shareIntent.putExtra(android.content.Intent.EXTRA_TEXT, adHocDealModel.url);
            startActivity(Intent.createChooser(shareIntent, "Share Deal"));
        }
//            new Thread(new Runnable() {
//                @Override
//                public void run() {
//                    imageLoader.loadImage(adHocDealModel.image, options, new ImageLoadingListener() {
//                        @Override
//                        public void onLoadingStarted(String imageUri, View view) {
//                            AppDelegate.LogT("onLoadingStarted called");
//                        }
//
//                        @Override
//                        public void onLoadingFailed(String imageUri, View view, FailReason failReason) {
//                            AppDelegate.hideProgressDialog(AdHocDealDetailActivity.this);
//                            AppDelegate.LogT("onLoadingFailed called = " + failReason.getCause().toString());
//                            Intent shareIntent = new Intent(android.content.Intent.ACTION_SEND);
//                            shareIntent.setType("text/plain");
//                            if (AppDelegate.isValidString(adHocDealModel.title)) {
//                                shareIntent.putExtra(android.content.Intent.EXTRA_SUBJECT, adHocDealModel.title);
//                                shareIntent.putExtra(Intent.EXTRA_REFERRER_NAME, adHocDealModel.title);
//                                shareIntent.putExtra(Intent.EXTRA_TITLE, adHocDealModel.title);
//                            }
//                            if (AppDelegate.isValidString(adHocDealModel.item_detail))
//                                shareIntent.putExtra(Intent.EXTRA_PROCESS_TEXT_READONLY, adHocDealModel.item_detail);
//                            shareIntent.putExtra(android.content.Intent.EXTRA_TEXT, adHocDealModel.url);
//                            startActivity(Intent.createChooser(shareIntent, "Share Deal"));
//                        }
//
//                        @Override
//                        public void onLoadingComplete(String imageUri, View view, Bitmap loadedImage) {
//                            AppDelegate.hideProgressDialog(AdHocDealDetailActivity.this);
//                            AppDelegate.LogT("onLoadingComplete called");
//                            String file_path = AppDelegate.getDefaultFile(AdHocDealDetailActivity.this);
//                            if (!AppDelegate.isValidString(file_path)) {
//                                AppDelegate.showToast(AdHocDealDetailActivity.this, getResources().getString(R.string.sharing_failed));
//                                return;
//                            }
//                            AppDelegate.saveFile(AdHocDealDetailActivity.this, loadedImage, file_path);
//                            Uri uri = Uri.parse("file:///" + file_path);
//
//                            Intent shareIntent = new Intent(android.content.Intent.ACTION_SEND);
//                            shareIntent.setType("*/*");
////                            shareIntent.setType("text/*");
//                            if (AppDelegate.isValidString(adHocDealModel.title)) {
//                                shareIntent.putExtra(android.content.Intent.EXTRA_SUBJECT, adHocDealModel.title);
//                                shareIntent.putExtra(android.content.Intent.EXTRA_TEXT, adHocDealModel.title);
//                                shareIntent.putExtra(Intent.EXTRA_REFERRER_NAME, adHocDealModel.title);
//                                shareIntent.putExtra(Intent.EXTRA_TITLE, adHocDealModel.title);
//                                shareIntent.putExtra(Intent.EXTRA_TEXT, adHocDealModel.title);
//                            }
//                            if (AppDelegate.isValidString(adHocDealModel.item_detail))
//                                shareIntent.putExtra(Intent.EXTRA_PROCESS_TEXT_READONLY, adHocDealModel.item_detail);
//                            shareIntent.putExtra(Intent.EXTRA_STREAM, uri);
////                            shareIntent.addCategory(Intent.CATEGORY_LAUNCHER);
////                            shareIntent.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK | Intent.FLAG_ACTIVITY_RESET_TASK_IF_NEEDED);
//                            startActivity(Intent.createChooser(shareIntent, "Share Deal"));
//                        }
//
//                        @Override
//                        public void onLoadingCancelled(String imageUri, View view) {
//                            AppDelegate.hideProgressDialog(AdHocDealDetailActivity.this);
//                            AppDelegate.LogT("onLoadingCancelled called");
//
//                            Intent shareIntent = new Intent(android.content.Intent.ACTION_SEND);
//                            shareIntent.setType("text/plain");
//                            if (AppDelegate.isValidString(adHocDealModel.title)) {
//                                shareIntent.putExtra(android.content.Intent.EXTRA_SUBJECT, adHocDealModel.title);
//                                shareIntent.putExtra(android.content.Intent.EXTRA_TEXT, adHocDealModel.title);
//                            }
//                            shareIntent.addCategory(Intent.CATEGORY_LAUNCHER);
//                            shareIntent.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK | Intent.FLAG_ACTIVITY_RESET_TASK_IF_NEEDED);
//                            startActivity(Intent.createChooser(shareIntent, "Share Deal"));
//                        }
//                    });
//                }
//            }).start();
        if (adHocDealModel != null && AppDelegate.isValidString(adHocDealModel.name))
            AppDelegate.trackSharedDeal(adHocDealModel.name, DEAL_ADHOC, "ADHOC DEAL DETAIL");
    }

    public void setListViewHeightBasedOnChildren(RecyclerView listView) {
        try {
            if (arrayItems == null && arrayItems.size() == 0) {
                // pre-condition
                AppDelegate.LogE("Adapter is null");
                ViewGroup.LayoutParams params = listView.getLayoutParams();
                params.height = 80;
                listView.setLayoutParams(params);
                listView.requestLayout();
                return;
            }
            int totalHeight = 0;
            float itemCount = arrayItems.size();
            for (int i = 0; i < itemCount; i++) {
                totalHeight += AppDelegate.dpToPix(AdHocDealDetailActivity.this, 77);
            }
            ViewGroup.LayoutParams params = listView.getLayoutParams();
            params.height = totalHeight;
            AppDelegate.LogT("height = " + totalHeight);
            exp_layout.setLayoutParams(params);
            exp_layout.requestLayout();
        } catch (Exception e) {
            AppDelegate.LogE(e);
        }
    }

    private void showAdHocDetail(final AdHocDealsModel adHocDealModel) {
        if (googleMap != null) {
            CameraPosition cameraPosition = new CameraPosition.Builder()
                    .target(new LatLng(Double.parseDouble(adHocDealModel.latitude), Double.parseDouble(adHocDealModel.longitude)))
                    .zoom(14.0f).build();
            googleMap.moveCamera(CameraUpdateFactory.newCameraPosition(cameraPosition));
            mHandler.sendEmptyMessage(10);
            new Handler(this.getMainLooper()).post(new Runnable() {
                @Override
                public void run() {
                    AppDelegate.setStictModePermission();
                    createMarker(new Person(new LatLng(Double.parseDouble(adHocDealModel.latitude), Double.parseDouble(adHocDealModel.longitude)), adHocDealModel.id, adHocDealModel.name, null, adHocDealModel, DEAL_ADHOC));
                    CameraPosition cameraPosition = new CameraPosition.Builder()
                            .target(new LatLng(Double.parseDouble(adHocDealModel.latitude), Double.parseDouble(adHocDealModel.longitude)))
                            .zoom(14.0f).build();
                    mHandler.sendEmptyMessage(11);
//                    googleMap.moveCamera(CameraUpdateFactory.newCameraPosition(cameraPosition));
                    googleMap.moveCamera(CameraUpdateFactory.newCameraPosition(cameraPosition));
                } // This is your code
            });
        }
        try {
            if (AppDelegate.isValidString(adHocDealModel.distance))
                txt_c_distance.setText(Html.fromHtml("<b>Distance: </b>" + AppDelegate.roundOff(Double.parseDouble(adHocDealModel.distance)) + " KM."));
            else
                txt_c_distance.setVisibility(View.GONE);
        } catch (Exception e) {
            AppDelegate.LogE(e);
        }
        if (adHocDealModel != null && AppDelegate.isValidString(adHocDealModel.name))
            AppDelegate.trackDealNameAndtype(adHocDealModel.name, DEAL_ADHOC, "ADHOC DEAL DETAIL");

        if (adHocDealModel.deal_type == 1) {
            txt_c_start_date_label.setText("Available Deal :");
            try {
                txt_c_start_date.setText(adHocDealModel.volume_of_deal - adHocDealModel.total_deal + "");
            } catch (Exception e) {
                AppDelegate.LogE(e);
            }
            txt_c_end_date_label.setText("Purchased Deal :");
            txt_c_end_date.setText(adHocDealModel.total_deal + "");

            try {
                String starting_date = adHocDealModel.starting_date;
                String expiry_date = adHocDealModel.expiry_date;
                expiry_date = expiry_date.replaceAll("/+0000", "");
                expiry_date = new SimpleDateFormat("dd MMM yyyy").format(new SimpleDateFormat("yyyy-MM-dd'T'HH:mm:ss").parse(expiry_date));
                txt_c_expiry_label.setVisibility(View.VISIBLE);
                txt_c_expiry_date.setVisibility(View.VISIBLE);
                if (new Prefs(AdHocDealDetailActivity.this).getUserdata() != null) {
                    txt_c_expiry_label.setText("Purchase deal before it expires: ");
                } else findViewById(R.id.ll_expiry_date).setVisibility(View.GONE);
                txt_c_expiry_date.setText(expiry_date);/*validation on buy now*/
                Calendar cal = Calendar.getInstance();
                SimpleDateFormat sdf = new SimpleDateFormat("dd MMM yyyy");
                cal.setTime(sdf.parse(expiry_date));
                if (DateUtils.isAfterDay(Calendar.getInstance().getTime(), cal.getTime())) {
                    AppDelegate.LogT("cal.getTime()==" + cal.getTime() + "  Calendar.getInstance().getTime()==" + Calendar.getInstance().getTime() + "true");
                    txt_c_buy_now.setVisibility(View.GONE);
                } else {
                    if (adHocDealModel.volume_of_deal == adHocDealModel.total_deal) {
                        txt_c_buy_now.setVisibility(View.GONE);
                    } else {
                        txt_c_buy_now.setVisibility(View.VISIBLE);
                    }
                }

            } catch (Exception e) {
                AppDelegate.LogE(e);
            }
        } else {
            txt_c_expiry_label.setVisibility(View.GONE);
            txt_c_expiry_date.setVisibility(View.GONE);
            try {
//                String starting_date = adHocDealModel.starting_date;
//                starting_date = starting_date.replaceAll("/+0000", "");
//                starting_date = new SimpleDateFormat("dd MMM yyyy").format(new SimpleDateFormat("yyyy-MM-dd'T'HH:mm:ss").parse(starting_date));
//                txt_c_start_date_label.setText("Start date :");
//                txt_c_start_date.setText(starting_date);

                txt_c_start_date_label.setText("Purchased Deal :");
                try {
                    txt_c_start_date.setText(adHocDealModel.total_deal + "");
                } catch (Exception e) {
                    AppDelegate.LogE(e);
                }

                String expiry_date = adHocDealModel.expiry_date;
                expiry_date = expiry_date.replaceAll("/+0000", "");
                expiry_date = new SimpleDateFormat("dd MMM yyyy").format(new SimpleDateFormat("yyyy-MM-dd'T'HH:mm:ss").parse(expiry_date));
                txt_c_end_date_label.setText("Purchase deal before it expires::");
                txt_c_end_date.setText(expiry_date + "");
            } catch (Exception e) {
                AppDelegate.LogE(e);
            }
        }
        txt_c_title.setText(adHocDealModel.name);
        txt_c_comapny_contact_number.setText(Html.fromHtml("Phone:  <b>" + adHocDealModel.company_contact_number + "<b>"));
        try {
            if (AppDelegate.isValidString(adHocDealModel.company_opening_hours)) {
                JSONArray object = new JSONArray(adHocDealModel.company_opening_hours);
                ArrayList<OpenHoursModel> openHoursModels = new ArrayList<>();
                StringBuilder sb = new StringBuilder();
                for (int i = 0; i < object.length(); i++) {
                    JSONObject object1 = object.getJSONObject(i);
                    OpenHoursModel openHoursModel = new OpenHoursModel();
                    openHoursModel.day = JSONParser.getString(object1, "day");
                    openHoursModel.from = JSONParser.getString(object1, "from");
                    openHoursModel.to = JSONParser.getString(object1, "to");
                    try {
                        openHoursModel.from = new SimpleDateFormat("hh:mm a").format(new SimpleDateFormat("HH:mm").parse(openHoursModel.from));
                        openHoursModel.to = new SimpleDateFormat("hh:mm a").format(new SimpleDateFormat("HH:mm").parse(openHoursModel.to));
                    } catch (ParseException e) {
                        AppDelegate.LogE(e);
                    }
                    sb.append(openHoursModel.day + " : " + openHoursModel.from + " - " + openHoursModel.to + "\n");
//                openHoursModels.add(openHoursModel);
                }
                String hours = sb.toString();
                if (hours.length() > 0) {
                    hours = hours.substring(0, hours.length() - 1).toString();
                    txt_c_comapny_open_hour.setText(hours);
                }
            }
//            setHours(openHoursModels);
        } catch (JSONException e) {
            AppDelegate.LogE(e);
        }

//        txt_c_comapny_open_hour.setText(Html.fromHtml("<b>" + adHocDealModel.company_opening_hours + "<b>"));
        txt_c_deal_prize_real.setText("$ " + adHocDealModel.deal_price_real + "");
        txt_c_deal_prize_offer.setText("$ " + adHocDealModel.deal_price_offer + "");
        txt_c_deal_prize_like.setText(adHocDealModel.favourite + "");
        // txt_c_select_from.setText("");
        txt_c_about_me.setText("About " + adHocDealModel.company_name);
        txt_c_about_me_vendor.setText(adHocDealModel.about_me);
        txt_c_company_name.setText(adHocDealModel.company_name);
        txt_c_comapny_address.setText(adHocDealModel.company_address);
        txt_c_things_to_remember.setText(adHocDealModel.things_to_remebers + "");
        txt_c_use_the_offer.setText(adHocDealModel.use_the_offer + "");
        txt_c_facilities.setText(adHocDealModel.facilities + "");
        //txt_c_total_amount.setText();
        mHandler.sendEmptyMessage(2);
        if (arrayItems != null) {
//            txt_c_select_from.setText("Select from " + arrayItems.size() + "" + " options");
            txt_c_select_from.setText("Purchase this awesome deal here");
            rl_c_select_from.setVisibility(View.VISIBLE);
            show_DealsList(arrayItems);
            expandableView(exp_layout, EXPAND);
        } else {
            rl_c_select_from.setVisibility(View.GONE);
            expandableView(exp_layout, COLLAPSE);
        }

        img_loading2.setVisibility(View.VISIBLE);
        AnimationDrawable frameAnimation = (AnimationDrawable) img_loading2.getDrawable();
        frameAnimation.setCallback(img_loading2);
        frameAnimation.setVisible(true, true);
        frameAnimation.start();

        imageLoader.loadImage(adHocDealModel.company_image, options, new ImageLoadingListener() {
            @Override
            public void onLoadingStarted(String imageUri, View view) {
            }

            @Override
            public void onLoadingFailed(String imageUri, View view, FailReason failReason) {
                img_circle_deal_image.setImageDrawable(getResources().getDrawable(R.drawable.user_noimage));
                img_loading2.setVisibility(View.GONE);
            }

            @Override
            public void onLoadingComplete(String imageUri, View view, Bitmap loadedImage) {
                img_circle_deal_image.setImageBitmap(loadedImage);
                img_loading2.setVisibility(View.GONE);
            }

            @Override
            public void onLoadingCancelled(String imageUri, View view) {
            }
        });
    }

    private void setHours(ArrayList<OpenHoursModel> openHoursModels) {
    }


    @Override
    public void setOnItemClickListener(String name, int position, ArrayList<ItemsModel> itemsModelArrayList) {
        AppDelegate.LogT("itemsModelArrayList==" + itemsModelArrayList);
        this.itemsModelArrayList = itemsModelArrayList;
        total_amount_string = "0";
        if (name.equalsIgnoreCase(Tags.clicks)) {
            for (ItemsModel itemsModel : itemsModelArrayList) {
                if (itemsModel.item_count.equalsIgnoreCase("0")) {
                    total_amount_string = String.valueOf(Integer.parseInt(itemsModel.item_count) * itemsModel.price + Integer.parseInt(total_amount_string));
                    txt_c_total_amount.setText("Total Amount: $ " + total_amount_string + "");
                    AppDelegate.LogT("itemsModel.item_count=0==>" + total_amount_string);
                } else {
                    total_amount_string = String.valueOf(Integer.parseInt(itemsModel.item_count) * itemsModel.price + Integer.parseInt(total_amount_string));
                    txt_c_total_amount.setText("Total Amount: $ " + total_amount_string + "");
                    AppDelegate.LogT("itemsModel.item_count===>" + total_amount_string);
                }

            }
        }
    }

    public void showLoginAlert(Context mContext, String Title, String
            Message) {
        try {
            alert = new AlertDialog.Builder(mContext);
            alert.setCancelable(false);
            alert.setMessage(Message);
            alert.setPositiveButton(
                    "LOGIN",
                    new DialogInterface.OnClickListener() {

                        @Override
                        public void onClick(DialogInterface dialog, int which) {
                            Intent intent = new Intent(AdHocDealDetailActivity.this, LoginActivity.class);
                            intent.putExtra(Tags.login_from, AppDelegate.LOGIN_FROM_VENDOR_PROFILE);
                            apiShouldcall = true;
                            startActivity(intent);
                            dialog.dismiss();
                        }
                    }).setNegativeButton(Tags.CANCEL, new DialogInterface.OnClickListener() {
                @Override
                public void onClick(DialogInterface dialog, int which) {
                    dialog.dismiss();
                }
            });
            alert.show();
        } catch (Exception e) {
            AppDelegate.LogE(e);
        }
    }

    private android.widget.ImageView[] dots;
    private int dotsCount, item_position = 0, selected_tab = 0, click_type = 0;

    private void setUiPageViewController() {
        pager_indicator.removeAllViews();
        dotsCount = bannerFragment.size();
        if (dotsCount > 0) {
            dots = new android.widget.ImageView[dotsCount];
            for (int i = 0; i < dotsCount; i++) {
                dots[i] = new android.widget.ImageView(AdHocDealDetailActivity.this);
                dots[i].setImageDrawable(getResources().getDrawable(R.drawable.white_radius_square));
                android.widget.LinearLayout.LayoutParams params = new android.widget.LinearLayout.LayoutParams(AppDelegate.getInstance(AdHocDealDetailActivity.this).dpToPix(this, 7), AppDelegate.getInstance(AdHocDealDetailActivity.this).dpToPix(this, 7));
                params.setMargins(AppDelegate.getInstance(AdHocDealDetailActivity.this).dpToPix(this, 3), 0, AppDelegate.getInstance(AdHocDealDetailActivity.this).dpToPix(this, 3), 0);
                pager_indicator.addView(dots[i], params);
            }
            dots[0].setImageDrawable(getResources().getDrawable(R.drawable.yellow_radius_square));
        }
        view_pager.setOnPageChangeListener(new ViewPager.OnPageChangeListener() {
            @Override
            public void onPageScrolled(int position, float positionOffset, int positionOffsetPixels) {
            }

            @Override
            public void onPageSelected(int position) {
                switchBannerPage(position);
            }

            @Override
            public void onPageScrollStateChanged(int state) {
            }
        });
        switchBannerPage(0);
    }

    private void switchBannerPage(int position) {
        if (dotsCount > 0) {
            for (int i = 0; i < dotsCount; i++) {
                dots[i].setImageResource(R.drawable.white_radius_square);
            }
            dots[position].setImageResource(R.drawable.yellow_radius_square);
        }
    }

    void setBanner(ArrayList<String> imageArray) {
        for (int i = 0; i < imageArray.size(); i++) {
            BannerFragment fragment = new BannerFragment();
            Bundle bundle = new Bundle();
            bundle.putString(Tags.DATA, imageArray.get(i));
            fragment.setArguments(bundle);
            bannerFragment.add(fragment);
        }
        AppDelegate.LogT("bannerFragment. size==>" + bannerFragment.size() + "");
        bannerPagerAdapter = new PagerAdapter(getSupportFragmentManager(), bannerFragment);
        view_pager.setAdapter(bannerPagerAdapter);
        setUiPageViewController();
    }

    protected Marker createMarker(Person markers) {
        try {
            View marker = ((LayoutInflater) getSystemService(Context.LAYOUT_INFLATER_SERVICE)).inflate(R.layout.multi_profile, null);
            final android.widget.ImageView cimg_user = (android.widget.ImageView) marker.findViewById(R.id.imageView);
            final android.widget.ImageView img_loading = (android.widget.ImageView) marker.findViewById(R.id.img_loading);
            img_loading.setVisibility(View.VISIBLE);
            new Handler(Looper.getMainLooper()).post(new Runnable() {
                @Override
                public void run() {
                    AnimationDrawable frameAnimation = (AnimationDrawable) img_loading.getDrawable();
                    frameAnimation.setCallback(img_loading);
                    frameAnimation.setVisible(true, true);
                    frameAnimation.start();
                    ((Animatable) img_loading.getDrawable()).start();
                }
            });
            if (markers.type == DEAL_HERO) {
                LatLng latlong = new LatLng(Double.parseDouble(markers.heroDealsModel.latitude), Double.parseDouble(markers.heroDealsModel.longitude));
                if (AppDelegate.isValidString(markers.heroDealsModel.markerImage)) {
                    Bitmap remote_picture = null;
                    try {
                        if (AppDelegate.isValidString(markers.heroDealsModel.markerImage))
                            remote_picture = BitmapFactory.decodeStream((InputStream) new URL(markers.heroDealsModel.markerImage).getContent());
                        cimg_user.setImageBitmap(remote_picture);
                        img_loading.setVisibility(View.GONE);
                    } catch (IOException e) {
                        AppDelegate.LogE(e);
                    }
                }
                customMarker = googleMap.addMarker(new MarkerOptions()
                        .position(latlong)
                        .title(markers.heroDealsModel.name)
                        .snippet(markers.heroDealsModel.company_address)
                        .icon(BitmapDescriptorFactory.fromBitmap(AppDelegate.createDrawableFromView(this, marker))));

            } else {
                LatLng latlong = new LatLng(Double.parseDouble(markers.adHocDealsModel.latitude), Double.parseDouble(markers.adHocDealsModel.longitude));
                if (AppDelegate.isValidString(markers.adHocDealsModel.markerImage)) {
                    Bitmap remote_picture = null;
                    try {
                        if (AppDelegate.isValidString(markers.adHocDealsModel.markerImage))
                            remote_picture = BitmapFactory.decodeStream((InputStream) new URL(markers.adHocDealsModel.markerImage).getContent());
                        cimg_user.setImageBitmap(remote_picture);
                        img_loading.setVisibility(View.GONE);
                    } catch (IOException e) {
                        AppDelegate.LogE(e);
                    }
                }
                customMarker = googleMap.addMarker(new MarkerOptions()
                        .position(latlong)
                        .title(markers.adHocDealsModel.name)
                        .snippet(markers.adHocDealsModel.company_address)
                        .icon(BitmapDescriptorFactory.fromBitmap(AppDelegate.createDrawableFromView(this, marker))));
                CameraPosition cameraPosition = new CameraPosition.Builder()
                        .target(new LatLng(Double.parseDouble(adHocDealModel.latitude), Double.parseDouble(adHocDealModel.longitude)))
                        .zoom(14.0f).build();
                googleMap.moveCamera(CameraUpdateFactory.newCameraPosition(cameraPosition));

//                googleMap.moveCamera(center);
//                googleMap.moveCamera(zoom);
            }
        } catch (Exception e) {
            e.printStackTrace();
        }
        return customMarker;
    }
}
