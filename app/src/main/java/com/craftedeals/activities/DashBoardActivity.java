package com.craftedeals.activities;

import android.Manifest;
import android.annotation.TargetApi;
import android.content.Context;
import android.content.Intent;
import android.content.IntentSender;
import android.content.pm.PackageManager;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.graphics.drawable.Drawable;
import android.location.Location;
import android.location.LocationManager;
import android.net.Uri;
import android.os.AsyncTask;
import android.os.Build;
import android.os.Bundle;
import android.os.Handler;
import android.os.Message;
import android.support.v4.app.ActivityCompat;
import android.support.v4.app.ActivityOptionsCompat;
import android.support.v4.app.FragmentActivity;
import android.support.v4.content.ContextCompat;
import android.support.v4.util.Pair;
import android.support.v7.widget.DefaultItemAnimator;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.View;
import android.view.ViewGroup;
import android.view.Window;
import android.view.WindowManager;
import android.view.animation.AlphaAnimation;
import android.view.animation.Animation;
import android.widget.AdapterView;
import android.widget.GridView;
import android.widget.ListView;

import com.craftedeals.AppDelegate;
import com.craftedeals.Async.LocationAddress;
import com.craftedeals.Async.PostAsync;
import com.craftedeals.Models.CategoryImageModel;
import com.craftedeals.Models.CategoryModel;
import com.craftedeals.Models.CityModel;
import com.craftedeals.Models.HeroDealsModel;
import com.craftedeals.Models.PostAysnc_Model;
import com.craftedeals.Models.UserDataModel;
import com.craftedeals.R;
import com.craftedeals.Utils.CircleImageView;
import com.craftedeals.Utils.Prefs;
import com.craftedeals.Utils.TransitionHelper;
import com.craftedeals.adapters.DealsCategoryAdapter;
import com.craftedeals.adapters.HeroDealsAdapter;
import com.craftedeals.constants.ServerRequestConstants;
import com.craftedeals.constants.Tags;
import com.craftedeals.interfaces.OnReciveServerResponse;
import com.google.android.gms.common.ConnectionResult;
import com.google.android.gms.common.api.GoogleApiClient;
import com.google.android.gms.common.api.PendingResult;
import com.google.android.gms.common.api.ResultCallback;
import com.google.android.gms.common.api.Status;
import com.google.android.gms.location.LocationRequest;
import com.google.android.gms.location.LocationServices;
import com.google.android.gms.location.LocationSettingsRequest;
import com.google.android.gms.location.LocationSettingsResult;
import com.google.android.gms.location.LocationSettingsStates;
import com.google.android.gms.location.LocationSettingsStatusCodes;
import com.nostra13.universalimageloader.core.DisplayImageOptions;
import com.nostra13.universalimageloader.core.ImageLoaderConfiguration;
import com.nostra13.universalimageloader.core.assist.FailReason;
import com.nostra13.universalimageloader.core.listener.ImageLoadingListener;

import org.apache.http.HttpEntity;
import org.apache.http.HttpResponse;
import org.apache.http.client.HttpClient;
import org.apache.http.client.methods.HttpGet;
import org.apache.http.impl.client.DefaultHttpClient;
import org.apache.http.protocol.BasicHttpContext;
import org.apache.http.protocol.HttpContext;
import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.io.IOException;
import java.io.InputStream;
import java.net.URL;
import java.util.ArrayList;
import java.util.Timer;
import java.util.TimerTask;

import SlidingPaneLayout.SlidingPaneLayout1;
import carbon.widget.LinearLayout;
import carbon.widget.TextView;

public class DashBoardActivity extends FragmentActivity implements View.OnClickListener, GoogleApiClient.ConnectionCallbacks, GoogleApiClient.OnConnectionFailedListener,
        com.google.android.gms.location.LocationListener, OnReciveServerResponse
{
    public Prefs prefs;
    TextView txt_c_location_title, txt_c_deal_type, txt_c_not_data;
    private final static int CONNECTION_FAILURE_RESOLUTION_REQUEST = 9000;
    private GoogleApiClient mGoogleApiClient;
    private LocationRequest mLocationRequest;
    private double currentLatitude = 0, currentLongitude = 0;
    public static DashBoardActivity mainActivity;
    public static Handler mHandler;
    GridView grid_category;
    RecyclerView recycler_deals;
    public static final int PANEL_HOME = 0, PANEL_MYPROFILE = 1, PANEL_MYPURCHASE = 2, PANEL_MYFAVOURITES = 3, PANEL_MYHERODEALS = 4, PANEL_NOTIFICATIONS = 6, PANEL_EMAILSUBSCRIPTION = 5, PANEL_SETTING = 7, PANEL_HELP = 8, PANEL_LOGIN = 9, PANEL_LOGOUT = 10, PANEL_PRIVACY_POLICY = 11, PANEL_TERMS_CONDITION = 12, PANEL_FAQ = 13;
    LinearLayout ll_c_home, ll_c_login, ll_c_logout, ll_c_my_profile, ll_c_my_purchase, ll_c_my_hero_deals, ll_c_my_favourite, ll_c_email_subscription, ll_c_settings, ll_c_help, ll_c_notifications, ll_c_privacy_policy, ll_c_terms_condition, ll_c_faq;
    TextView txt_c_help, txt_c_login, txt_c_settings, txt_c_email_subscription, txt_c_my_favourite, txt_c_my_hero_deals, txt_c_my_purchase, txt_c_my_profile, txt_c_home, txt_c_notifications, txt_c_privacy_policy, txt_c_terms_condition, txt_c_faq;
    public android.widget.LinearLayout side_panel;
    public SlidingPaneLayout1 mSlidingPaneLayout;
    public SlideMenuClickListener menuClickListener = new SlideMenuClickListener();
    public boolean isSlideOpen = false;
    public int ratio, ride_cancel_time = 30;
    public float init = 0.0f;
    private ArrayList<CategoryModel> categoryModelArrayList = new ArrayList<>();
    public CircleImageView user_img;
    public UserDataModel userDataModel;
    public com.nostra13.universalimageloader.core.ImageLoader imageLoader = com.nostra13.universalimageloader.core.ImageLoader.getInstance();
    public DisplayImageOptions options = new DisplayImageOptions.Builder().cacheInMemory(true)
            .cacheOnDisc(true).resetViewBeforeLoading(true).
                    build();

    @Override
    protected void onCreate(Bundle savedInstanceState)
    {
        super.onCreate(savedInstanceState);
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.LOLLIPOP)
        {
            Window window = this.getWindow();
            Drawable background = this.getResources().getDrawable(R.drawable.topbg);
            window.addFlags(WindowManager.LayoutParams.FLAG_DRAWS_SYSTEM_BAR_BACKGROUNDS);
            window.setStatusBarColor(getResources().getColor(android.R.color.transparent));
            window.setNavigationBarColor(getResources().getColor(android.R.color.transparent));
            window.setBackgroundDrawable(background);
        }
        setContentView(R.layout.dashboard);
        imageLoader.init(ImageLoaderConfiguration.createDefault(this));
        prefs = new Prefs(this);
        userDataModel = prefs.getUserdata();
        mainActivity = this;
        initView();
        initGPS();
        setHandler();
        setValues();
        showGPSalert();
        setInitailSideBar(PANEL_HOME);

        locationPermission();

    }

    public void setValues()
    {
        userDataModel = new Prefs(this).getUserdata();
        if (userDataModel != null && AppDelegate.isValidString(userDataModel.image))
        {
            AppDelegate.LogT("userDataModel.image => " + userDataModel.image);
            imageLoader.loadImage(userDataModel.image, options, new ImageLoadingListener()
            {
                @Override
                public void onLoadingStarted(String imageUri, View view)
                {
                    AppDelegate.LogT("onLoadingStarted userDataModel.image => " + userDataModel.image);
                }

                @Override
                public void onLoadingFailed(String imageUri, View view, FailReason failReason)
                {
                    user_img.setImageDrawable(getResources().getDrawable(R.drawable.logo));
                    AppDelegate.LogT("onLoadingFailed userDataModel.image => " + userDataModel.image);
                }

                @Override
                public void onLoadingComplete(String imageUri, View view, Bitmap loadedImage)
                {
                    user_img.setImageBitmap(loadedImage);
                    AppDelegate.LogT("onLoadingComplete userDataModel.image => " + userDataModel.image);
                }

                @Override
                public void onLoadingCancelled(String imageUri, View view)
                {
                    AppDelegate.LogT("onLoadingCancelled userDataModel.image => " + userDataModel.image);
                }
            });
        }
        else
        {
            user_img.setImageDrawable(getResources().getDrawable(R.drawable.logo));
        }
    }

    @Override
    protected void onDestroy()
    {
        super.onDestroy();
        mainActivity = null;
        mHandler = null;
        if (mGoogleApiClient != null && mGoogleApiClient.isConnected())
        {
            LocationServices.FusedLocationApi.removeLocationUpdates(mGoogleApiClient, DashBoardActivity.this);
            mGoogleApiClient.disconnect();
            AppDelegate.LogGP("Fused Location api disconnect called");
        }
    }

    @Override
    public void onActivityResult(int requestCode, int resultCode, Intent data)
    {
        super.onActivityResult(requestCode, resultCode, data);
        AppDelegate.LogT("LoginActivity onActivityResult called");
    }

    private void showGPSalert()
    {
        if (mGoogleApiClient == null)
        {
            mGoogleApiClient = new GoogleApiClient.Builder(DashBoardActivity.this)
                    .addApi(LocationServices.API)
                    .addConnectionCallbacks(this)
                    .addOnConnectionFailedListener(this).build();
            mGoogleApiClient.connect();
        }
        LocationRequest locationRequest = LocationRequest.create();
        locationRequest.setPriority(LocationRequest.PRIORITY_HIGH_ACCURACY);
        locationRequest.setInterval(10000);
        locationRequest.setFastestInterval(10000);
        LocationSettingsRequest.Builder builder = new LocationSettingsRequest.Builder()
                .addLocationRequest(locationRequest);
        //**************************
        builder.setAlwaysShow(true); //this is the key ingredient
        //**************************
        PendingResult<LocationSettingsResult> result =
                LocationServices.SettingsApi.checkLocationSettings(mGoogleApiClient, builder.build());
        result.setResultCallback(new ResultCallback<LocationSettingsResult>()
        {
            @Override
            public void onResult(LocationSettingsResult result)
            {
                final Status status = result.getStatus();
                final LocationSettingsStates state = result.getLocationSettingsStates();
                switch (status.getStatusCode())
                {
                    case LocationSettingsStatusCodes.SUCCESS:
                        // All location settings are satisfied. The client can initialize location
                        // requests here.
                        break;
                    case LocationSettingsStatusCodes.RESOLUTION_REQUIRED:
                        // Location settings are not satisfied. But could be fixed by showing the user
                        // a dialog.
                        try
                        {
                            // Show the dialog by calling startResolutionForResult(),
                            // and check the result in onActivityResult().
                            status.startResolutionForResult(
                                    DashBoardActivity.this, 1000);
                        }
                        catch (IntentSender.SendIntentException e)
                        {
                            // Ignore the error.
                        }
                        break;
                    case LocationSettingsStatusCodes.SETTINGS_CHANGE_UNAVAILABLE:
                        // Location settings are not satisfied. However, we have no way to fix the
                        // settings so we won't show the dialog.
                        break;
                }
            }
        });
    }

    public static DashBoardActivity getInstance()
    {
        return mainActivity;
    }

    private void initGPS()
    {
        mGoogleApiClient = new GoogleApiClient.Builder(this)
                .addConnectionCallbacks(this)
                .addOnConnectionFailedListener(this)
                .addApi(LocationServices.API)
                .build();
        AppDelegate.LogT("mGoogleApiClient Initialited");
        mLocationRequest = LocationRequest.create()
                .setPriority(LocationRequest.PRIORITY_HIGH_ACCURACY)
                .setInterval(10000)                     // 10 seconds, in milliseconds
                .setFastestInterval(10000);             // 10 second, in milliseconds
        AppDelegate.LogT("mLocationRequest Initialited");
    }

    private void initView()
    {
        mSlidingPaneLayout = (SlidingPaneLayout1) findViewById(R.id.mSlidingPaneLayout);
        mSlidingPaneLayout.setSliderFadeColor(getResources().getColor(android.R.color.transparent));
        mSlidingPaneLayout.setPanelSlideListener(new SliderListener());
        side_panel = (android.widget.LinearLayout) findViewById(R.id.side_panel);
        findViewById(R.id.img_c_menu).setOnClickListener(this);
        findViewById(R.id.img_c_location).setOnClickListener(this);
        findViewById(R.id.rl_search_location).setOnClickListener(this);
        txt_c_location_title = (TextView) findViewById(R.id.txt_c_location_title);
        findViewById(R.id.img_c_search).setOnClickListener(this);
        grid_category = (GridView) findViewById(R.id.grid_category);
        grid_category.setOnItemClickListener(new AdapterView.OnItemClickListener()
        {
            @TargetApi(Build.VERSION_CODES.JELLY_BEAN)
            @Override
            public void onItemClick(AdapterView<?> parent, View view, int position, long id)
            {
                Intent intent = new Intent(DashBoardActivity.this, DealsActivity.class);
                intent.putExtra(Tags.name, categoryModelArrayList.get(position).title);
                intent.putExtra(Tags.category_id, categoryModelArrayList.get(position).id);
                for (int i = 0; i < categoryModelArrayList.size(); i++)
                {
                    categoryModelArrayList.get(i).checked = 0;
                }
                categoryModelArrayList.get(position).checked = 1;
                intent.putParcelableArrayListExtra(Tags.category, categoryModelArrayList);
                final Pair<View, String>[] pairs = TransitionHelper.createSafeTransitionParticipants(DashBoardActivity.this, false, new Pair<>(view.findViewById(R.id.txt_c_name), "square_blue_name"));
                ActivityOptionsCompat transitionActivityOptions = ActivityOptionsCompat.makeSceneTransitionAnimation(DashBoardActivity.this, pairs);
                startActivity(intent, transitionActivityOptions.toBundle());
            }
        });
        txt_c_deal_type = (TextView) findViewById(R.id.txt_c_deal_type);
        txt_c_not_data = (TextView) findViewById(R.id.txt_c_not_data);
        recycler_deals = (RecyclerView) findViewById(R.id.recycler_deals);
        recycler_deals.setNestedScrollingEnabled(false);

        user_img = (CircleImageView) findViewById(R.id.user_img);
        ll_c_home = (LinearLayout) findViewById(R.id.ll_c_home);
        ll_c_my_profile = (LinearLayout) findViewById(R.id.ll_c_my_profile);
        ll_c_my_purchase = (LinearLayout) findViewById(R.id.ll_c_my_purchase);
        ll_c_my_hero_deals = (LinearLayout) findViewById(R.id.ll_c_my_hero_deals);
        ll_c_my_favourite = (LinearLayout) findViewById(R.id.ll_c_my_favourite);
        ll_c_email_subscription = (LinearLayout) findViewById(R.id.ll_c_email_subscription);
        ll_c_notifications = (LinearLayout) findViewById(R.id.ll_c_notifications);
        ll_c_settings = (LinearLayout) findViewById(R.id.ll_c_settings);
        ll_c_help = (LinearLayout) findViewById(R.id.ll_c_help);
        ll_c_privacy_policy = (LinearLayout) findViewById(R.id.ll_c_privacy_policy);
        ll_c_privacy_policy.setOnClickListener(this);
        ll_c_terms_condition = (LinearLayout) findViewById(R.id.ll_c_terms_condition);
        ll_c_terms_condition.setOnClickListener(this);
        ll_c_faq = (LinearLayout) findViewById(R.id.ll_c_faq);
        ll_c_faq.setOnClickListener(this);
        txt_c_privacy_policy = (TextView) findViewById(R.id.txt_c_privacy_policy);
        txt_c_terms_condition = (TextView) findViewById(R.id.txt_c_terms_condition);
        txt_c_faq = (TextView) findViewById(R.id.txt_c_faq);

        txt_c_help = (TextView) findViewById(R.id.txt_c_help);
        txt_c_settings = (TextView) findViewById(R.id.txt_c_settings);
        txt_c_email_subscription = (TextView) findViewById(R.id.txt_c_email_subscription);
        txt_c_my_favourite = (TextView) findViewById(R.id.txt_c_my_favourite);
        txt_c_my_hero_deals = (TextView) findViewById(R.id.txt_c_my_hero_deals);
        txt_c_my_purchase = (TextView) findViewById(R.id.txt_c_my_purchase);
        txt_c_my_profile = (TextView) findViewById(R.id.txt_c_my_profile);
        txt_c_home = (TextView) findViewById(R.id.txt_c_home);
        txt_c_notifications = (TextView) findViewById(R.id.txt_c_notifications);
        txt_c_login = (TextView) findViewById(R.id.txt_c_login);
        ll_c_login = (LinearLayout) findViewById(R.id.ll_c_login);
        ll_c_logout = (LinearLayout) findViewById(R.id.ll_c_logout);
        ll_c_home.setOnClickListener(this);
        ll_c_my_profile.setOnClickListener(this);
        ll_c_my_purchase.setOnClickListener(this);
        ll_c_my_favourite.setOnClickListener(this);
        ll_c_my_hero_deals.setOnClickListener(this);
        ll_c_notifications.setOnClickListener(this);
        ll_c_email_subscription.setOnClickListener(this);
        ll_c_settings.setOnClickListener(this);
        ll_c_help.setOnClickListener(this);
        ll_c_login.setOnClickListener(this);
        ll_c_logout.setOnClickListener(this);
    }

    private void setloc(double latitude, double longitude)
    {
        LocationAddress.getAddressFromLocation(latitude, longitude, this, mHandler);
    }

    private void setHandler()
    {
        mHandler = new Handler()
        {
            @Override
            public void dispatchMessage(Message msg)
            {
                super.dispatchMessage(msg);
                switch (msg.what)
                {
                    case 10:
                        AppDelegate.showProgressDialog(DashBoardActivity.this);
                        break;
                    case 11:
                        AppDelegate.hideProgressDialog(DashBoardActivity.this);
                        break;
                    case 2:
                        setAddressFromGEOcoder(msg.getData());
                        break;
                    case 3:
                        setValues();
                        break;
                    case 4:
                        execute_getDashboard();
                        break;

                    case 25:
                        show_DealsList(heroDealsModelArrayList);
                        break;

                }
            }
        };
    }


    @Override
    public void onClick(View v)
    {
        switch (v.getId())
        {
            case R.id.ll_c_home:
                toggleSlider();
                menuClickListener.onItemClick(null, v, PANEL_HOME, PANEL_HOME);
                break;
            case R.id.ll_c_my_profile:
                toggleSlider();
                menuClickListener.onItemClick(null, v, PANEL_MYPROFILE, PANEL_MYPROFILE);
                break;
            case R.id.ll_c_my_favourite:
                toggleSlider();
                menuClickListener.onItemClick(null, v, PANEL_MYFAVOURITES, PANEL_MYFAVOURITES);
                break;
            case R.id.ll_c_my_purchase:
                toggleSlider();
                menuClickListener.onItemClick(null, v, PANEL_MYPURCHASE, PANEL_MYPURCHASE);
                break;
            case R.id.ll_c_my_hero_deals:
                toggleSlider();
                menuClickListener.onItemClick(null, v, PANEL_MYHERODEALS, PANEL_MYHERODEALS);
                break;
            case R.id.ll_c_email_subscription:
                toggleSlider();
                menuClickListener.onItemClick(null, v, PANEL_EMAILSUBSCRIPTION, PANEL_EMAILSUBSCRIPTION);
                break;
            case R.id.ll_c_notifications:
                toggleSlider();
                menuClickListener.onItemClick(null, v, PANEL_NOTIFICATIONS, PANEL_NOTIFICATIONS);
                break;
            case R.id.ll_c_settings:
                toggleSlider();
                menuClickListener.onItemClick(null, v, PANEL_SETTING, PANEL_SETTING);
                break;
            case R.id.ll_c_help:
                toggleSlider();
                menuClickListener.onItemClick(null, v, PANEL_HELP, PANEL_HELP);
                break;
            case R.id.ll_c_privacy_policy:
                toggleSlider();
                menuClickListener.onItemClick(null, v, PANEL_PRIVACY_POLICY, PANEL_PRIVACY_POLICY);
                break;
            case R.id.ll_c_terms_condition:
                toggleSlider();
                menuClickListener.onItemClick(null, v, PANEL_TERMS_CONDITION, PANEL_TERMS_CONDITION);
                break;
            case R.id.ll_c_faq:
                toggleSlider();
                menuClickListener.onItemClick(null, v, PANEL_FAQ, PANEL_FAQ);
                break;
            case R.id.ll_c_logout:
                toggleSlider();
                menuClickListener.onItemClick(null, v, PANEL_LOGOUT, PANEL_LOGOUT);
                break;
            case R.id.ll_c_login:
                toggleSlider();
                menuClickListener.onItemClick(null, v, PANEL_LOGIN, PANEL_LOGIN);
                break;
            case R.id.img_c_menu:
                toggleSlider();
                break;
            case R.id.img_c_location:
                nearMyLocationClicked = true;
                if (currentLatitude != 0 && currentLongitude != 0)
                {
                    nearMyLocationClicked = false;
                    execute_Search();
                }
                else
                {
                    showGPSalert();
                }
//                startActivity(new Intent(this, Near_Me_Activity.class));
                break;
            case R.id.img_c_search:
                startActivity(new Intent(this, SearchActivity.class));
                break;
        }
    }

    public void locationPermission()
    {
        if (ContextCompat.checkSelfPermission(this,
                Manifest.permission.ACCESS_FINE_LOCATION) != PackageManager.PERMISSION_GRANTED)
        {
            if (ActivityCompat.shouldShowRequestPermissionRationale(this, Manifest.permission.ACCESS_FINE_LOCATION))
            {
                // Show an explanation to the user *asynchronously* -- don't block
                // this thread waiting for the user's response! After the user
                // sees the explanation, try again to request the permission.
                ActivityCompat.requestPermissions(this, new String[]{Manifest.permission.ACCESS_FINE_LOCATION}, 333);
            }
            else
            {
                ActivityCompat.requestPermissions(this, new String[]{Manifest.permission.ACCESS_FINE_LOCATION}, 333);
            }
        }
    }

    private void execute_Search()
    {
        try
        {
            if (AppDelegate.haveNetworkConnection(this, true))
            {
                ArrayList<PostAysnc_Model> mPostArrayList = new ArrayList<PostAysnc_Model>();
                AppDelegate.getInstance(this).setPostParamsSecond(mPostArrayList, Tags.API_KEY, Tags.API_KEY_VALUE);
                AppDelegate.getInstance(this).setPostParamsSecond(mPostArrayList, Tags.dealStatus, DealsActivity.DEAL_HERO);

                AppDelegate.getInstance(this).setPostParamsSecond(mPostArrayList, Tags.postalCode, "");
                if (currentLatitude != 0 && currentLongitude != 0)
                {
                    AppDelegate.getInstance(this).setPostParamsSecond(mPostArrayList, Tags.LAT, currentLatitude, ServerRequestConstants.Key_PostDoubleValue);
                    AppDelegate.getInstance(this).setPostParamsSecond(mPostArrayList, Tags.LONG, currentLongitude, ServerRequestConstants.Key_PostDoubleValue);
                }
                else
                {
                    if (new Prefs(this).getCitydata() != null)
                    {
                        AppDelegate.getInstance(this).setPostParamsSecond(mPostArrayList, Tags.LAT, new Prefs(this).getCitydata().lat);
                        AppDelegate.getInstance(this).setPostParamsSecond(mPostArrayList, Tags.LONG, new Prefs(this).getCitydata().lng);
                    }
                }
                if (new Prefs(DashBoardActivity.this).getUserdata() != null)
                {
                    AppDelegate.getInstance(this).setPostParamsSecond(mPostArrayList, Tags.user_id, new Prefs(DashBoardActivity.this).getUserdata().id);
                }
                AppDelegate.getInstance(this).setPostParamsSecond(mPostArrayList, Tags.categoryId, "");
                AppDelegate.getInstance(this).setPostParamsSecond(mPostArrayList, Tags.sortby, "near_me");

                AppDelegate.getInstance(this).setPostParamsSecond(mPostArrayList, Tags.keyword, "");
                PostAsync mPostAsyncObj;
                mPostAsyncObj = new PostAsync(this, this, ServerRequestConstants.SEARCH, mPostArrayList, null);
                mHandler.sendEmptyMessage(10);
                mPostAsyncObj.execute();
            }
        }
        catch (Exception e)
        {
            AppDelegate.showToast(this, "Please try again.");
            AppDelegate.LogE(e);
        }
    }


    boolean nearMyLocationClicked = false;

    private void nearByDeals(ArrayList<HeroDealsModel> heroDealsModelArrayList)
    {
        nearMyLocationClicked = false;
        Intent intent = new Intent(this, NearMeDealsActivity.class);
        intent.putExtra(Tags.deal_type, DealsActivity.DEAL_HERO);
        intent.putParcelableArrayListExtra(Tags.deal, heroDealsModelArrayList);
        intent.putExtra(Tags.title, "Near By Deals");

        intent.putExtra(Tags.category_id, "");

        intent.putExtra(Tags.postal_code, "");
        intent.putExtra(Tags.LAT, currentLatitude + "");
        intent.putExtra(Tags.LONG, currentLongitude + "");

        intent.putExtra(Tags.keyword, "");
        intent.putParcelableArrayListExtra(Tags.category, new Prefs(this).getCategoryArryList());

        final Pair<View, String>[] pairs = TransitionHelper.createSafeTransitionParticipants(DashBoardActivity.this, false, new Pair<>(findViewById(R.id.txt_c_location_title), "square_blue_name"));
        ActivityOptionsCompat transitionActivityOptions = ActivityOptionsCompat.makeSceneTransitionAnimation(DashBoardActivity.this, pairs);
        startActivity(intent, transitionActivityOptions.toBundle());
    }

    class SliderListener extends SlidingPaneLayout1.SimplePanelSlideListener
    {
        @Override
        public void onPanelOpened(View panel)
        {
            if (!isSlideOpen)
            {
                isSlideOpen = true;
            }
            AppDelegate.hideKeyBoard(DashBoardActivity.this);
        }

        @Override
        public void onPanelClosed(View panel)
        {
            if (isSlideOpen)
            {
                isSlideOpen = false;
            }
        }

        @Override
        public void onPanelSlide(View view, float slideOffset)
        {
            ratio = (int) -(0 - (slideOffset) * 255);
            whileSlide(ratio);
        }
    }

    public void toggleSlider()
    {
        if (mSlidingPaneLayout != null)
        {
            if (!mSlidingPaneLayout.isOpen())
            {
                mSlidingPaneLayout.openPane();
            }
            else
            {
                mSlidingPaneLayout.closePane();
            }
        }
    }

    public void whileSlide(int view)
    {
        int newalfa = 255 - view;
        float subvalue = newalfa / 2.55f;
        float f = (100f - subvalue) * 0.01f;
        Animation alphaAnimation = new AlphaAnimation(init, f);
        alphaAnimation.setDuration(0);
        alphaAnimation.setFillAfter(true);
        init = f;
        side_panel.startAnimation(alphaAnimation);
    }

    private class SlideMenuClickListener implements
            ListView.OnItemClickListener
    {
        @Override
        public void onItemClick(AdapterView<?> parent, View view, final int position, long id)
        {
            new Handler().postDelayed(new Runnable()
            {
                @Override
                public void run()
                {
                    displayView(position);
                }
            }, 600);
        }
    }

    private void displayView(int position)
    {
        AppDelegate.hideKeyBoard(DashBoardActivity.this);
        setInitailSideBar(position);
        switch (position)
        {
            case PANEL_HOME:
                return;
            case PANEL_MYPROFILE:
                startActivity(new Intent(this, MyProfileActivity.class));
                break;
            case PANEL_MYPURCHASE:
                startActivity(new Intent(this, MyPurchaseActivity.class));
                break;
            case PANEL_MYFAVOURITES:
                startActivity(new Intent(this, MyFavouriteDealsActivity.class));
                break;
            case PANEL_MYHERODEALS:
                startActivity(new Intent(this, MyHeroDealsActivity.class));
                break;

            case PANEL_EMAILSUBSCRIPTION:
                AppDelegate.LogT("PANEL_EMAILSUBSCRIPTION clicked");
                startActivity(new Intent(this, EmailSubscriptionsActivity.class));
                break;
            case PANEL_NOTIFICATIONS:
                startActivity(new Intent(this, NotificationActivity.class));
                break;
            case PANEL_SETTING:
                startActivity(new Intent(this, SettingActivity.class));
                break;
            case PANEL_HELP:
                openUrl(ServerRequestConstants.HELP_PAEG);
                break;
            case PANEL_PRIVACY_POLICY:
                openUrl(ServerRequestConstants.PRIVACY_POLICY);
                break;
            case PANEL_TERMS_CONDITION:
                openUrl(ServerRequestConstants.TERMS_CONDITIONS);
                break;
            case PANEL_FAQ:
                openUrl(ServerRequestConstants.FAQ);
                break;
            case PANEL_LOGOUT:
                //prefs.clearTempPrefs();
                prefs.clearSharedPreference();
                startActivity(new Intent(DashBoardActivity.this, LoginActivity.class));
                finish();
                break;
            case PANEL_LOGIN:
                startActivity(new Intent(this, LoginActivity.class));
                break;
        }
    }

    public void openUrl(String str_url)
    {
        Intent i = new Intent(Intent.ACTION_VIEW);
        i.setData(Uri.parse(str_url));
        startActivity(i);
    }

    private void show_deals_category(ArrayList<CategoryModel> categoryModelArrayList)
    {
        int grid_height = AppDelegate.dpToPix(DashBoardActivity.this, 150);
        AppDelegate.LogT("grid_height=" + grid_height + "");
        int no_of_items = categoryModelArrayList.size();
        if (no_of_items % 2 == 0)
        {
            no_of_items = no_of_items / 2;
        }
        else
        {
            no_of_items = (no_of_items + 1) / 2;
        }
//        no_of_items = no_of_items / 2;
        AppDelegate.LogT("Number of rows= " + no_of_items + "");
        ViewGroup.LayoutParams params = grid_category.getLayoutParams();
        params.height = no_of_items * grid_height;
        AppDelegate.LogT("height of grid view =" + "in pixel =" + no_of_items * grid_height + "In dp=" + AppDelegate.pixToDP(DashBoardActivity.this, no_of_items * grid_height));
        grid_category.setLayoutParams(params);

        grid_category.requestLayout();
        DealsCategoryAdapter dealsCategoryAdapter = new DealsCategoryAdapter(DashBoardActivity.this, categoryModelArrayList);
        grid_category.setAdapter(dealsCategoryAdapter);
        grid_category.invalidate();
    }

    public boolean apiShouldCall = false;

    private void execute_getDashboard()
    {
        try
        {
            if (AppDelegate.haveNetworkConnection(this, true))
            {
                ArrayList<PostAysnc_Model> mPostArrayList = new ArrayList<PostAysnc_Model>();
                AppDelegate.getInstance(this).setPostParamsSecond(mPostArrayList, Tags.API_KEY, Tags.API_KEY_VALUE);
                String lat = null, lng = null;
                if (prefs.getCitydata() != null)
                {
                    if (AppDelegate.isValidString(prefs.getCitydata().lat))
                    {
                        txt_c_location_title.setText(prefs.getCitydata().name);
                        lat = prefs.getCitydata().lat;
                        lng = prefs.getCitydata().lng;
                    }
                }
                else if (currentLatitude != 0 && currentLongitude != 0)
                {
                    AppDelegate.LogT("currentLatitude==>" + currentLatitude + ", currentLongitude==>" + currentLongitude);
                    lat = currentLatitude + "";
                    lng = currentLongitude + "";
                }
                else
                {
                    apiShouldCall = true;
                    return;
                }
                apiShouldCall = false;
                AppDelegate.LogT("lat => " + lat + ", lng => ");
                if (AppDelegate.isValidString(lat) && AppDelegate.isValidString(lng))
                {
                    AppDelegate.getInstance(this).setPostParamsSecond(mPostArrayList, Tags.lat, lat);
                    AppDelegate.getInstance(this).setPostParamsSecond(mPostArrayList, Tags.LONG, lng);
                }
                if (prefs.getUserdata() != null && AppDelegate.isValidString(prefs.getUserdata().id + ""))
                {
                    AppDelegate.getInstance(this).setPostParamsSecond(mPostArrayList, Tags.user_id, prefs.getUserdata().id);
                }
                PostAsync mPostAsyncObj;
                mPostAsyncObj = new PostAsync(this, this, ServerRequestConstants.GET_DASHBOARD,
                        mPostArrayList, null);
                // AppDelegate.showProgressDialog(this);
                if (mHandler != null)
                {
                    mHandler.sendEmptyMessage(10);
                }
                mPostAsyncObj.execute();
            }
        }
        catch (Exception e)
        {
            AppDelegate.showToast(this, getResources().getString(R.string.try_again));
            AppDelegate.LogE(e);
        }
    }

    private void show_DealsList(ArrayList<HeroDealsModel> heroDealsModelArrayList)
    {
        HeroDealsAdapter mAdapter = new HeroDealsAdapter(this, heroDealsModelArrayList);
        RecyclerView.LayoutManager mLayoutManager = new LinearLayoutManager(this);
        recycler_deals.setLayoutManager(mLayoutManager);
        recycler_deals.setItemAnimator(new DefaultItemAnimator());
        recycler_deals.setAdapter(mAdapter);
        if (heroDealsModelArrayList != null && heroDealsModelArrayList.size() > 0)
        {
            txt_c_not_data.setVisibility(View.GONE);
        }
        else
        {
            txt_c_not_data.setVisibility(View.VISIBLE);
        }
    }

    public void setAddressFromGEOcoder(Bundle data)
    {
        CityModel cityModel;
//        if (new Prefs(this).getCitydata() != null)
//            cityModel = new Prefs(this).getCitydata();
//        else {
        cityModel = new CityModel();
        cityModel.location_type = Tags.current_location;
        cityModel.name = data.getString(Tags.city_param);
        cityModel.lat = String.valueOf(data.getDouble(Tags.LAT)) + "";
        cityModel.lng = String.valueOf(data.getDouble(Tags.LNG)) + "";
        new Prefs(this).setCityData(cityModel);
//        }
        if (AppDelegate.isValidString(cityModel.name))
        {
            txt_c_location_title.setText(cityModel.name);
        }
        else
        {
            txt_c_location_title.setText(getString(R.string.app_name));
        }
    }


    @Override
    public void onConnected(Bundle bundle)
    {
        Location location = LocationServices.FusedLocationApi.getLastLocation(mGoogleApiClient);
        AppDelegate.LogT("onConnected Initialited");
        if (location == null)
        {
        }
        else
        {
            currentLatitude = location.getLatitude();
            currentLongitude = location.getLongitude();
//            setloc(currentLatitude, currentLongitude);
            AppDelegate.LogT("latLng = " + currentLatitude + ", " + currentLongitude);
            new Prefs(this).putStringValue(Tags.LAT, String.valueOf(currentLatitude));
            new Prefs(this).putStringValue(Tags.LNG, String.valueOf(currentLongitude));

            if (apiShouldCall)
            {
                CityModel cityModel = new Prefs(this).getCitydata();
                if (cityModel == null)
                {
                    cityModel = new CityModel();
                }
                cityModel.location_type = Tags.current_location;
                cityModel.lat = currentLatitude + "";
                cityModel.lng = currentLongitude + "";
                new Prefs(this).setCityData(cityModel);
                try
                {
                    mHandler.sendEmptyMessage(10);
                }
                catch (Exception e)
                {
                    e.printStackTrace();
                }
                LocationAddress.getAddressFromLocation(currentLatitude, currentLongitude, this, mHandler);
                execute_getDashboard();
            }
        }
        try
        {
            AppDelegate.LogT("onConnected Initialited== null");
            if (ActivityCompat.checkSelfPermission(this, Manifest.permission.ACCESS_FINE_LOCATION) != PackageManager.PERMISSION_GRANTED && ActivityCompat.checkSelfPermission(this, Manifest.permission.ACCESS_COARSE_LOCATION) != PackageManager.PERMISSION_GRANTED)
            {
                //    ActivityCompat#requestPermissions
                // here to request the missing permissions, and then overriding
                //   public void onRequestPermissionsResult(int requestCode, String[] permissions,
                //                                          int[] grantResults)
                // to handle the case where the user grants the permission. See the documentation
                // for ActivityCompat#requestPermissions for more details.
                return;
            }
            LocationServices.FusedLocationApi.requestLocationUpdates(mGoogleApiClient, mLocationRequest, DashBoardActivity.this);
        }
        catch (Exception e)
        {
            AppDelegate.LogE(e);
        }
    }

    @Override
    public void onConnectionSuspended(int i)
    {
    }

    @Override
    public void onConnectionFailed(ConnectionResult connectionResult)
    {
        if (connectionResult.hasResolution())
        {
            try
            {
                AppDelegate.LogT("onConnectionFailed Initialited" + connectionResult);
                connectionResult.startResolutionForResult(this, CONNECTION_FAILURE_RESOLUTION_REQUEST);

            }
            catch (IntentSender.SendIntentException e)
            {
                AppDelegate.LogE(e);
            }
        }
        else
        {
            Log.e("Error", "Location services connection failed with code " + connectionResult.getErrorCode());
        }
    }

    public void setInitSideMenu()
    {
        AppDelegate.hideKeyBoard(this);
        if (mSlidingPaneLayout != null)
        {
            if (mSlidingPaneLayout.isOpen())
            {
                mSlidingPaneLayout.closePane();
            }
        }
        setInitailSideBar(PANEL_HOME);
        displayView(PANEL_HOME);
        try
        {
            if (mHandler != null)
            {
                mHandler.sendEmptyMessage(3);
            }
        }
        catch (Exception e)
        {
            AppDelegate.LogE(e);
        }
        if (prefs.getCitydata() != null)
        {
            if (AppDelegate.isValidString(prefs.getCitydata().name))
            {
                txt_c_location_title.setText(prefs.getCitydata().name);
            }
        }
        else
        {
            txt_c_location_title.setText(getString(R.string.app_name));
        }
    }

    @Override
    protected void onResume()
    {
        super.onResume();
        setValues();
        setInitSideMenu();
        apiShouldCall = true;
        try
        {
            if (mGoogleApiClient.isConnected())
            {
//                LocationServices.FusedLocationApi.removeLocationUpdates(mGoogleApiClient, DashBoardActivity.this);
                mGoogleApiClient.disconnect();
            }
        }
        catch (Exception e)
        {
            AppDelegate.LogE(e);
        }
        mGoogleApiClient.connect();
        AppDelegate.LogT("onResume called");

//        if (categoryModelArrayList == null) {
//        execute_getDashboard();
//        }

        if (currentLatitude == 0 || currentLongitude == 0)
        {
            if (apiShouldCall)
            {
                showGPSalert();
            }
        }
        else
        {
            if (((LocationManager) getSystemService(Context.LOCATION_SERVICE)).isProviderEnabled(LocationManager.GPS_PROVIDER))
            {
                //Do what you need if enabled...
                showGPSalert();
            }
            else
            {
                execute_getDashboard();
            }
        }
    }

    @Override
    protected void onPause()
    {
        super.onPause();
//        try {
//            if (mGoogleApiClient.isConnected()) {
////                LocationServices.FusedLocationApi.removeLocationUpdates(mGoogleApiClient, DashBoardActivity.this);
//                mGoogleApiClient.disconnect();
//            }
//        } catch (Exception e) {
//            AppDelegate.LogE(e);
//        }
    }

    @Override
    public void onLocationChanged(Location location)
    {
        currentLatitude = location.getLatitude();
        currentLongitude = location.getLongitude();
        AppDelegate.LogGP("onLocationChanged latLng = " + currentLatitude + ", " + currentLongitude + ", apiShouldCall => " + apiShouldCall);

        new Prefs(this).putStringValue(Tags.LAT, String.valueOf(currentLatitude));
        new Prefs(this).putStringValue(Tags.LNG, String.valueOf(currentLongitude));
        try
        {
            if (apiShouldCall)
            {
                CityModel cityModel = new Prefs(this).getCitydata();
                if (cityModel == null)
                {
                    cityModel = new CityModel();
                }
                cityModel.location_type = Tags.current_location;
                cityModel.lat = currentLatitude + "";
                cityModel.lng = currentLongitude + "";
                new Prefs(this).setCityData(cityModel);
                mHandler.sendEmptyMessage(10);
                LocationAddress.getAddressFromLocation(currentLatitude, currentLongitude, this, mHandler);
                execute_getDashboard();
            }
            else if (nearMyLocationClicked)
            {
                nearMyLocationClicked = false;
                execute_Search();
            }
//            if (mGoogleApiClient.isConnected()) {
//                LocationServices.FusedLocationApi.removeLocationUpdates(mGoogleApiClient, DashBoardActivity.this);
//                mGoogleApiClient.disconnect();
//                AppDelegate.LogGP("Fused Location api disconnect called");
//            }
        }
        catch (Exception e)
        {
            AppDelegate.LogE(e);
        }
    }


    @Override
    public void onBackPressed()
    {
        finish();
    }

    public void setInitailSideBar(int value)
    {
        AppDelegate.LogT("initial side bar called== prefs user value is==>" + new Prefs(this).getUserdata());
        if (new Prefs(this).getUserdata() != null)
        {
            ll_c_login.setVisibility(View.GONE);
            ll_c_logout.setVisibility(View.VISIBLE);
            ll_c_my_profile.setVisibility(View.VISIBLE);
            ll_c_my_purchase.setVisibility(View.GONE);
            ll_c_my_favourite.setVisibility(View.GONE);
            ll_c_my_hero_deals.setVisibility(View.VISIBLE);
            ll_c_notifications.setVisibility(View.VISIBLE);
            ll_c_email_subscription.setVisibility(View.VISIBLE);
            ll_c_settings.setVisibility(View.VISIBLE);
        }
        else
        {
            ll_c_login.setVisibility(View.VISIBLE);
            ll_c_my_profile.setVisibility(View.GONE);
            ll_c_my_purchase.setVisibility(View.GONE);
            ll_c_my_favourite.setVisibility(View.GONE);
            ll_c_my_hero_deals.setVisibility(View.GONE);
            ll_c_notifications.setVisibility(View.GONE);
            ll_c_email_subscription.setVisibility(View.GONE);
            ll_c_settings.setVisibility(View.GONE);
            ll_c_logout.setVisibility(View.GONE);
        }
        ll_c_home.setSelected(false);
        ll_c_my_profile.setSelected(false);
        ll_c_my_purchase.setSelected(false);
        ll_c_my_favourite.setSelected(false);
        ll_c_my_hero_deals.setSelected(false);
        ll_c_notifications.setSelected(false);
        ll_c_email_subscription.setSelected(false);
        ll_c_settings.setSelected(false);
        ll_c_help.setSelected(false);
        ll_c_login.setSelected(false);
        txt_c_home.setSelected(false);
        txt_c_my_profile.setSelected(false);
        txt_c_my_purchase.setSelected(false);
        txt_c_my_favourite.setSelected(false);
        txt_c_my_hero_deals.setSelected(false);
        txt_c_notifications.setSelected(false);
        txt_c_email_subscription.setSelected(false);
        txt_c_settings.setSelected(false);
        txt_c_help.setSelected(false);
        txt_c_login.setSelected(false);

        switch (value)
        {
            case PANEL_HOME:
                //  bar1.setVisibility(View.VISIBLE);
                ll_c_home.setSelected(true);
                txt_c_home.setSelected(true);
                break;
            case PANEL_MYPROFILE:
                //bar2.setVisibility(View.VISIBLE);
                ll_c_my_profile.setSelected(true);
                txt_c_my_profile.setSelected(true);
                break;
            case PANEL_MYPURCHASE:
                // bar3.setVisibility(View.VISIBLE);
                txt_c_my_purchase.setSelected(true);
                ll_c_my_purchase.setSelected(true);
                break;
            case PANEL_MYFAVOURITES:
                //bar4.setVisibility(View.VISIBLE);
                txt_c_my_favourite.setSelected(true);
                ll_c_my_favourite.setSelected(true);
                break;
            case PANEL_MYHERODEALS:
                // bar5.setVisibility(View.VISIBLE);
                txt_c_my_hero_deals.setSelected(true);
                ll_c_my_hero_deals.setSelected(true);
                break;
            case PANEL_NOTIFICATIONS:
                //bar6.setVisibility(View.VISIBLE);
                txt_c_notifications.setSelected(true);
                ll_c_notifications.setSelected(true);
                break;
            case PANEL_EMAILSUBSCRIPTION:
                AppDelegate.LogT("PANEL_EMAILSUBSCRIPTION");
                //bar6.setVisibility(View.VISIBLE);
                txt_c_email_subscription.setSelected(true);
                ll_c_email_subscription.setSelected(true);
                break;
            case PANEL_SETTING:
                // bar5.setVisibility(View.VISIBLE);
                ll_c_settings.setSelected(true);
                txt_c_settings.setSelected(true);
                break;
            case PANEL_HELP:
                //bar6.setVisibility(View.VISIBLE);
                txt_c_help.setSelected(true);
                ll_c_help.setSelected(true);
                break;
            case PANEL_LOGIN:
                //bar6.setVisibility(View.VISIBLE);
                txt_c_login.setSelected(true);
                ll_c_login.setSelected(true);
                break;
        }
    }

    @Override
    public void setOnReciveResult(String apiName, String result)
    {
        if (mHandler == null)
        {
            return;
        }
        mHandler.sendEmptyMessage(11);
        if (!AppDelegate.isValidString(result))
        {
            AppDelegate.showToast(DashBoardActivity.this, getResources().getString(R.string.time_out));
            return;
        }
        if (apiName.equals(ServerRequestConstants.GET_DASHBOARD))
        {
            parseDashBoard(result);
        }
        else if (apiName.equals(ServerRequestConstants.SEARCH))
        {
            parseHeroDeals(result);
        }
    }

    private void parseHeroDeals(String result)
    {
        try
        {
            JSONObject jsonobj = new JSONObject(result);
            if (jsonobj.getInt(Tags.status) == 1 && jsonobj.getInt(Tags.dataFlow) == 1)
            {
                CategoryModel category = new CategoryModel();
                ArrayList<HeroDealsModel> heroDealsModelArrayList = new ArrayList<>();
                JSONArray array = jsonobj.getJSONArray(Tags.response);
                for (int i = 0; i < array.length(); i++)
                {
                    HeroDealsModel heroDealsModel = new HeroDealsModel();
                    JSONObject heroobj = array.getJSONObject(i);
                    heroDealsModel.distance = heroobj.getString(Tags.distance);
                    heroDealsModel.id = heroobj.getInt(Tags.id);
                    heroDealsModel.user_id = heroobj.getInt(Tags.user_id);
                    heroDealsModel.name = heroobj.getString(Tags.name);
                    heroDealsModel.herodeal_title = heroobj.getString(Tags.name);
                    if (heroobj.has(Tags.deal_category))
                    {
                        heroDealsModel.category = heroobj.getJSONObject(Tags.deal_category).getString(Tags.title);
                    }
                    heroDealsModel.deal_category_id = heroobj.getInt(Tags.deal_category_id);
                    heroDealsModel.image = heroobj.getString(Tags.image);
                    heroDealsModel.sub_category_id = heroobj.getInt(Tags.sub_category_id);
                    heroDealsModel.description = heroobj.getString(Tags.description);
                    heroDealsModel.value_upto = heroobj.getInt(Tags.value_upto);
                    heroDealsModel.discount_offer = heroobj.getString(Tags.discount_offer);
                    heroDealsModel.starting_date = heroobj.getString(Tags.starting_date);
                    heroDealsModel.expiry_date = heroobj.getString(Tags.expiry_date);
                    heroDealsModel.trems_and_conditions = heroobj.getString(Tags.trems_and_conditions);
                    heroDealsModel.total_deal = heroobj.getInt(Tags.total_deal);
                    if (AppDelegate.isValidString(heroobj.getString(Tags.user)))
                    {
                        JSONObject vendor = heroobj.getJSONObject(Tags.user);
                        heroDealsModel.vendor_name = vendor.getString(Tags.name);
                        if (vendor.has(Tags.busines) && AppDelegate.isValidString(vendor.getString(Tags.busines)))
                        {
                            JSONObject business = vendor.getJSONObject(Tags.busines);
                            heroDealsModel.latitude = business.getString(Tags.latitude);
                            heroDealsModel.longitude = business.getString(Tags.longitude);
                            heroDealsModel.company_address = business.getString(Tags.location_address);
                            heroDealsModel.cityName = business.getString(Tags.cityName);
                        }
                    }
                    JSONObject obj = heroobj.getJSONObject(Tags.deal_category);
                    category = new CategoryModel();
                    category.id = obj.getInt(Tags.id);
                    category.title = obj.getString(Tags.title);
                    category.description = obj.getString(Tags.description);
                    category.image = obj.getString(Tags.image);
                    heroDealsModel.category = category.title;
                    heroDealsModel.markerImage = obj.getString(Tags.marker_icon);
                    heroDealsModelArrayList.add(heroDealsModel);
                }
                AppDelegate.LogT("heroDealsModelArrayList==> " + heroDealsModelArrayList.size());
                nearByDeals(heroDealsModelArrayList);
            }
            else
            {
                AppDelegate.showToast(this, jsonobj.getString(Tags.message));
            }
        }
        catch (JSONException e)
        {
            AppDelegate.LogE(e);
        }
    }

    private void parseDashBoard(String result)
    {
        try
        {
            JSONObject jsonobj = new JSONObject(result);
            if (jsonobj.getInt(Tags.status) == 1 && jsonobj.getInt(Tags.dataFlow) == 1)
            {
                CategoryModel category = new CategoryModel();
                categoryModelArrayList.clear();
                HeroDealsModel heroDealsModel = new HeroDealsModel();
                heroDealsModelArrayList = new ArrayList<>();
                JSONObject jsonObject = jsonobj.getJSONObject(Tags.response);
                JSONArray array = jsonObject.getJSONArray(Tags.categories);
                for (int i = 0; i < array.length(); i++)
                {
                    category = new CategoryModel();
                    JSONObject obj = array.getJSONObject(i);
                    category.id = obj.getInt(Tags.id);
                    category.title = obj.getString(Tags.title);
                    category.description = obj.getString(Tags.description);
                    category.marker_icon = "http://notosolutions.net/craftdeals/uploads/category/" + obj.getString(Tags.marker_icon);
                    category.image = obj.getString(Tags.image);
                    category.thumb = obj.getString(Tags.thumb);
                    categoryModelArrayList.add(category);
                }

                prefs.setCategoryArryList(categoryModelArrayList);
                JSONArray jsonarray = jsonObject.getJSONArray(Tags.herodeals);
                for (int j = 0; j < jsonarray.length(); j++)
                {
                    heroDealsModel = new HeroDealsModel();
                    JSONObject heroobj = jsonarray.getJSONObject(j);
                    heroDealsModel.distance = heroobj.getString(Tags.distance);
                    heroDealsModel.id = heroobj.getInt(Tags.id);
                    heroDealsModel.user_id = heroobj.getInt(Tags.user_id);
                    heroDealsModel.name = heroobj.getString(Tags.name);
                    heroDealsModel.herodeal_title = heroobj.getString(Tags.name);
                    if (heroobj.has(Tags.deal_category))
                    {
                        heroDealsModel.category = heroobj.getJSONObject(Tags.deal_category).getString(Tags.title);
                    }
                    heroDealsModel.deal_category_id = heroobj.getInt(Tags.deal_category_id);
                    heroDealsModel.image = heroobj.getString(Tags.image);
                    heroDealsModel.sub_category_id = heroobj.getInt(Tags.sub_category_id);
                    heroDealsModel.description = heroobj.getString(Tags.description);
                    heroDealsModel.value_upto = heroobj.getInt(Tags.value_upto);
                    heroDealsModel.discount_offer = heroobj.getString(Tags.discount_offer);
                    heroDealsModel.starting_date = heroobj.getString(Tags.starting_date);
                    heroDealsModel.expiry_date = heroobj.getString(Tags.expiry_date);
                    heroDealsModel.trems_and_conditions = heroobj.getString(Tags.trems_and_conditions);
                    heroDealsModel.total_deal = heroobj.getInt(Tags.total_deal);
                    if (heroobj.has(Tags.user) && heroobj.getJSONObject(Tags.user).has(Tags.busines))
                    {
                        if (AppDelegate.isValidString(heroobj.getJSONObject(Tags.user).getString(Tags.busines)))
                        {
                            heroDealsModel.latitude = heroobj.getJSONObject(Tags.user).getJSONObject(Tags.busines).getString(Tags.latitude);
                            heroDealsModel.longitude = heroobj.getJSONObject(Tags.user).getJSONObject(Tags.busines).getString(Tags.longitude);
                            heroDealsModel.company_address = heroobj.getJSONObject(Tags.user).getJSONObject(Tags.busines).getString(Tags.location_address);
//                            heroDealsModel.cityName = heroobj.getJSONObject(Tags.user).getJSONObject(Tags.busines).getString(Tags.cityName);
                        }
                    }
                    if (AppDelegate.isValidString(heroobj.getString(Tags.user)))
                    {
                        JSONObject vendor = heroobj.getJSONObject(Tags.user);
                        heroDealsModel.vendor_name = vendor.getString(Tags.name);
                    }
                    heroDealsModelArrayList.add(heroDealsModel);
                }
//                this.categoryModelArrayList = categoryModelArrayList;
                show_DealsList(heroDealsModelArrayList);
                show_deals_category(categoryModelArrayList);
                if (AppDelegate.arrayBitmap == null || AppDelegate.arrayBitmap.size() == 0 || AppDelegate.arrayBitmap.size() != categoryModelArrayList.size())
                {
                    new DownloadImages().execute();
                }
            }
            else
            {
                AppDelegate.showToast(this, jsonobj.getString(Tags.message));
            }
        }
        catch (JSONException e)
        {
            AppDelegate.LogE(e);
        }
    }

    ArrayList<HeroDealsModel> heroDealsModelArrayList = new ArrayList<>();

    /*********
     * Fetching KM of current & deals Location
     **************/
    public Thread mThreadKM;
    private Timer mTimerKM;
    private TimerTask mTimerTaskKM;
    private Handler mTimerHandlerKM = new Handler();
    private Runnable mTimerRunnableKM = new Runnable()
    {
        @Override
        public void run()
        {
            if (!AppDelegate.haveNetworkConnection(DashBoardActivity.this))
            {
            }
            else
            {
                mHandler.sendEmptyMessage(10);
                for (int i = 0; i < heroDealsModelArrayList.size(); i++)
                {
                    try
                    {
                        String mUrl = "http://maps.googleapis.com/maps/api/directions/json?"
                                + "origin=" + heroDealsModelArrayList.get(i).latitude + "," + heroDealsModelArrayList.get(i).longitude
                                + "&destination=" + new Prefs(DashBoardActivity.this).getCitydata().lat + "," + new Prefs(DashBoardActivity.this).getCitydata().lng
                                + "&sensor=false&units=metric&mode=driving";
                        AppDelegate.LogUA("direction url => " + mUrl);
                        HttpClient mHttpClient = new DefaultHttpClient(AppDelegate.getHttpParameters());
                        HttpContext mHttpContext = new BasicHttpContext();
                        mUrl = mUrl.replace(" ", "%20");
                        HttpGet mHttpget = new HttpGet(mUrl.trim());
                        HttpResponse mHttpResponse = mHttpClient.execute(mHttpget,
                                mHttpContext);
                        if (mHttpResponse != null)
                        {
                            HttpEntity mHttpEntity = mHttpResponse.getEntity();
                            InputStream inStream = mHttpEntity.getContent();
                            if (inStream != null)
                            {
                                String result = AppDelegate.convertStreamToString(inStream);
                                if (result != null && result.length() > 0)
                                {
                                    JSONObject json = new JSONObject(result);
                                    AppDelegate.LogUR(json.toString());
                                    JSONArray routeArray = json.getJSONArray("routes");
                                    if (routeArray.length() > 0)
                                    {
                                        JSONObject routes = routeArray.getJSONObject(0);
                                        heroDealsModelArrayList.get(i).distance = routes.getJSONArray("legs").getJSONObject(0).getJSONObject("distance").getString("text");
                                    }
                                }
                            }
                        }
//                        AppDelegate.LogT("distance => " + heroDealsModelArrayList.get(i).distance);
//                        if (!AppDelegate.isValidString(heroDealsModelArrayList.get(i).distance)) {
//                            heroDealsModelArrayList.get(i).distance = AppDelegate.distance(Double.parseDouble(new Prefs(DashBoardActivity.this).getCitydata().lat),
//                                    Double.parseDouble(new Prefs(DashBoardActivity.this).getCitydata().lng), Double.parseDouble(heroDealsModelArrayList.get(i).latitude),
//                                    Double.parseDouble(heroDealsModelArrayList.get(i).longitude)) + " KM";
//                        }
                    }
                    catch (Exception e)
                    {
                        e.printStackTrace();
                    }
                }
            }
            if (mHandler != null)
            {
                mHandler.sendEmptyMessage(11);
                mHandler.sendEmptyMessage(25);
            }
        }
    };

    private void stopTimer()
    {
        if (mTimerKM != null)
        {
            mTimerKM.cancel();
            mTimerKM.purge();
        }
        if (mTimerHandlerKM != null && mTimerRunnableKM != null)
        {
            mTimerHandlerKM.removeCallbacks(mTimerRunnableKM);
        }
    }

    private void startTimerAndStop()
    {
        stopTimer();
        onlyStopThread(mThreadKM);
        if (!AppDelegate.haveNetworkConnection(DashBoardActivity.this, false))
        {
        }
        else
        {
            mTimerKM = new Timer();
            mTimerTaskKM = new TimerTask()
            {
                public void run()
                {
                    mThreadKM = new Thread(mTimerRunnableKM);
                    mThreadKM.start();
                }
            };
            mTimerKM.schedule(mTimerTaskKM, 0);
        }
    }

    private void onlyStopThread(Thread mThreadKM)
    {
        if (mThreadKM != null)
        {
            mThreadKM.interrupt();
            mThreadKM = null;
        }
    }

    /*********
     * Fetching KM of current & deals Location
     **************/

    public class DownloadImages extends AsyncTask<Void, Void, Void>
    {

        @Override
        protected void onPreExecute()
        {
            super.onPreExecute();
            if (mHandler == null)
            {
                return;
            }
            mHandler.sendEmptyMessage(10);
            AppDelegate.LogUA("");
        }

        @Override
        protected Void doInBackground(Void... params)
        {
            Bitmap remote_picture = null;
            try
            {
                for (int i = 0; i < categoryModelArrayList.size(); i++)
                {
                    if (AppDelegate.arrayBitmap != null && AppDelegate.arrayBitmap.size() > 0)
                    {
                        boolean isDownloadedBefore = false;
                        for (int j = 0; j < AppDelegate.arrayBitmap.size(); j++)
                        {
                            if (AppDelegate.arrayBitmap.get(j).category_id.equalsIgnoreCase(categoryModelArrayList.get(i).id + ""))
                            {
                                isDownloadedBefore = true;
                            }
                        }
                        if (!isDownloadedBefore)
                        {
                            remote_picture = BitmapFactory.decodeStream((InputStream) new URL(categoryModelArrayList.get(i).marker_icon).getContent());
                            AppDelegate.arrayBitmap.add(new CategoryImageModel(categoryModelArrayList.get(i).id + "", categoryModelArrayList.get(i).marker_icon, remote_picture));
                        }
                    }
                    else
                    {
                        remote_picture = BitmapFactory.decodeStream((InputStream) new URL(categoryModelArrayList.get(i).marker_icon).getContent());
                        AppDelegate.arrayBitmap.add(new CategoryImageModel(categoryModelArrayList.get(i).id + "", categoryModelArrayList.get(i).marker_icon, remote_picture));
                    }
                }
            }
            catch (IOException e)
            {
                AppDelegate.LogE(e);
            }
            return null;
        }

        @Override
        protected void onPostExecute(Void aVoid)
        {
            super.onPostExecute(aVoid);
            if (mHandler == null)
            {
                return;
            }
            mHandler.sendEmptyMessage(11);
        }
    }

}

