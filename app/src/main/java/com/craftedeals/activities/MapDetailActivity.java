package com.craftedeals.activities;

import android.content.Context;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.graphics.drawable.Animatable;
import android.graphics.drawable.AnimationDrawable;
import android.graphics.drawable.Drawable;
import android.os.Build;
import android.os.Bundle;
import android.os.Handler;
import android.os.Looper;
import android.os.Message;
import android.support.annotation.Nullable;
import android.support.v7.app.AppCompatActivity;
import android.text.Html;
import android.view.LayoutInflater;
import android.view.View;
import android.view.Window;
import android.view.WindowManager;
import android.widget.RelativeLayout;

import com.craftedeals.AppDelegate;
import com.craftedeals.Async.LocationAddress;
import com.craftedeals.Models.Person;
import com.craftedeals.R;
import com.craftedeals.Utils.CircleImageView;
import com.craftedeals.Utils.WorkaroundMapFragment;
import com.craftedeals.constants.Tags;
import com.google.android.gms.maps.CameraUpdateFactory;
import com.google.android.gms.maps.GoogleMap;
import com.google.android.gms.maps.OnMapReadyCallback;
import com.google.android.gms.maps.model.BitmapDescriptorFactory;
import com.google.android.gms.maps.model.CameraPosition;
import com.google.android.gms.maps.model.LatLng;
import com.google.android.gms.maps.model.Marker;
import com.google.android.gms.maps.model.MarkerOptions;
import com.nostra13.universalimageloader.core.DisplayImageOptions;
import com.nostra13.universalimageloader.core.ImageLoaderConfiguration;
import com.nostra13.universalimageloader.core.assist.FailReason;
import com.nostra13.universalimageloader.core.listener.ImageLoadingListener;

import org.apache.commons.lang3.text.WordUtils;

import java.io.IOException;
import java.io.InputStream;
import java.net.URL;

import carbon.widget.ImageView;
import carbon.widget.TextView;

import static com.craftedeals.activities.DealsActivity.DEAL_HERO;

/**
 * Created by Bharat on 07/29/2016.
 */
public class MapDetailActivity extends AppCompatActivity {

    private GoogleMap map_business;
    private double lat = 0.0, lng = 0.0;
    private String name = "", country = "", state = "", city = "", image = "", company_name = "";
    private Handler mHandler;
    private TextView txt_c_title;
    public com.nostra13.universalimageloader.core.ImageLoader imageLoader = com.nostra13.universalimageloader.core.ImageLoader.getInstance();
    public DisplayImageOptions options = new DisplayImageOptions.Builder().cacheInMemory(true)
            .cacheOnDisc(true).resetViewBeforeLoading(true).
                    build();
    ImageView img_c_loading1;
    CircleImageView cimg_user;
    Person person;
    private Marker customMarker;

    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.LOLLIPOP) {
            Window window = this.getWindow();
            Drawable background = this.getResources().getDrawable(R.drawable.topbg);
            window.addFlags(WindowManager.LayoutParams.FLAG_DRAWS_SYSTEM_BAR_BACKGROUNDS);
            window.setStatusBarColor(getResources().getColor(android.R.color.transparent));
            window.setNavigationBarColor(getResources().getColor(android.R.color.transparent));
            window.setBackgroundDrawable(background);
        }
        setContentView(R.layout.map_detail_page);
        imageLoader.init(ImageLoaderConfiguration.createDefault(this));
        setHandler();
        initView();
    }

    private void initView() {
        txt_c_title = (TextView) findViewById(R.id.txt_c_title);
        findViewById(R.id.img_c_back).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                onBackPressed();
            }
        });
        try {
            lat = Double.parseDouble(AppDelegate.isValidString(getIntent().getStringExtra(Tags.LAT)) ? getIntent().getStringExtra(Tags.LAT) : "26.2414");
            lng = Double.parseDouble(AppDelegate.isValidString(getIntent().getStringExtra(Tags.LNG)) ? getIntent().getStringExtra(Tags.LNG) : "78.2414");
            name = AppDelegate.isValidString(getIntent().getStringExtra(Tags.name)) ? getIntent().getStringExtra(Tags.name) : "Vendor name";
            country = AppDelegate.isValidString(getIntent().getStringExtra(Tags.country)) ? getIntent().getStringExtra(Tags.country) : "Country";
            state = AppDelegate.isValidString(getIntent().getStringExtra(Tags.state)) ? getIntent().getStringExtra(Tags.state) : "State";
            city = AppDelegate.isValidString(getIntent().getStringExtra(Tags.city)) ? getIntent().getStringExtra(Tags.city) : "Suburb";
            image = AppDelegate.isValidString(getIntent().getStringExtra(Tags.image)) ? getIntent().getStringExtra(Tags.image) : "https:// www.hbhc";
            company_name = AppDelegate.isValidString(getIntent().getStringExtra(Tags.company_name)) ? getIntent().getStringExtra(Tags.company_name) : "Company name";
            txt_c_title.setText(WordUtils.capitalize(company_name));
            person = getIntent().getParcelableExtra(Tags.person);
        } catch (Exception e) {
            AppDelegate.LogE(e);
        }
        new Handler().postDelayed(
                new Runnable() {
                    @Override
                    public void run() {
                        try {
                            if (AppDelegate.haveNetworkConnection(MapDetailActivity.this) && !AppDelegate.isValidString(lat + "")) {
                                setLatLngAndFindAddress(new LatLng(lat, lng), 100);
                            }
                            ((WorkaroundMapFragment) getSupportFragmentManager().findFragmentById(R.id.fragment_map)).getMapAsync(new OnMapReadyCallback() {
                                @Override
                                public void onMapReady(GoogleMap googleMap) {
                                    map_business = googleMap;
                                    showMap();
                                }
                            });
                        } catch (Exception e) {
                            AppDelegate.LogE(e);
                        }
                    }
                }

                , 300);


    }


    private void setHandler() {
        mHandler = new Handler() {
            @Override
            public void dispatchMessage(Message msg) {
                super.dispatchMessage(msg);
                if (msg.what == 10) {
                    AppDelegate.showProgressDialog(MapDetailActivity.this);
                } else if (msg.what == 11) {
                    AppDelegate.hideProgressDialog(MapDetailActivity.this);
                } else if (msg.what == 2) {
                    setResultFromGeoCoderApi(msg.getData());
                }
            }
        };
    }

    private String place_name = "", place_address = "";

    public void setResultFromGeoCoderApi(Bundle bundle) {
        mHandler.sendEmptyMessage(11);
        if (bundle.getString(Tags.PLACE_NAME) != null) {
            place_name = bundle.getString(Tags.PLACE_NAME);
            place_address = bundle.getString(Tags.PLACE_ADD);
            showMap();
        } else {
            AppDelegate.showToast(MapDetailActivity.this, getResources().getString(R.string.no_location_available));
        }
    }

    public void setLatLngAndFindAddress(final LatLng latLng, long countDownTime) {
        LatLng arg0 = AppDelegate.getRoundedLatLng(latLng);
        LocationAddress.getAddressFromLocation(arg0.latitude, arg0.longitude, this, mHandler);
    }

    private void showMap() {
        if (map_business == null) {
            return;
        }
        map_business.setMyLocationEnabled(true);
        map_business.getUiSettings().setMapToolbarEnabled(false);
        map_business.getUiSettings().setZoomControlsEnabled(false);
        map_business.getUiSettings().setCompassEnabled(false);
        map_business.setMapType(GoogleMap.MAP_TYPE_NORMAL);
        AppDelegate.LogT(" lat ==" + lat + "  , long==" + lng);
        map_business.clear();
        mHandler.sendEmptyMessage(10);
        new Handler(this.getMainLooper()).post(new Runnable() {
            @Override
            public void run() {
                AppDelegate.setStictModePermission();
                createMarker(person);
                mHandler.sendEmptyMessage(11);
                CameraPosition cameraPosition = new CameraPosition.Builder().target(new LatLng(lat, lng))
                        .zoom(14).build();
                map_business.moveCamera(CameraUpdateFactory.newCameraPosition(cameraPosition));
            } // This is your code
        });

    }
//    private void show_pin_dialog() {
//        // TODO Auto-generated method stub
//        map_business.setInfoWindowAdapter(new GoogleMap.InfoWindowAdapter() {
//            // @Override
//           /* public View getInfoWindow(Marker marker) {
//                return null;
//            }*/
//
//            @Override
//            public View getInfoWindow(Marker marker) {
//                // TODO Auto-generated method stub
//                View v = getLayoutInflater().inflate(R.layout.map_pin_showinfo_row, null);
//                // Set desired height and width
//                //  v.setLayoutParams(new RelativeLayout.LayoutParams(500, RelativeLayout.LayoutParams.WRAP_CONTENT));
//                TextView txt_c_company_name = (TextView) v.findViewById(R.id.txt_c_company_name);
//                TextView txt_c_country = (TextView) v.findViewById(R.id.txt_c_address);
//                img_c_loading1 = (ImageView) v.findViewById(R.id.img_c_loading1);
//                cimg_user = (CircleImageView) v.findViewById(R.id.cimg_user);
//                txt_c_company_name.setText(WordUtils.capitalize(name));
//                txt_c_country.setText(/*city + ", " + state + ", " +*/ country + "");
//                AppDelegate.LogT("setValues called");
//                if (AppDelegate.isValidString(image)) {
//                    img_c_loading1.setVisibility(View.VISIBLE);
//                    AnimationDrawable frameAnimation = (AnimationDrawable) img_c_loading1.getDrawable();
//                    frameAnimation.setCallback(img_c_loading1);
//                    frameAnimation.setVisible(true, true);
//                    frameAnimation.start();
//
//                    imageLoader.loadImage(image, options, new ImageLoadingListener() {
//                        @Override
//                        public void onLoadingStarted(String imageUri, View view) {
//
//                        }
//
//                        @Override
//                        public void onLoadingFailed(String imageUri, View view, FailReason failReason) {
//                            cimg_user.setImageDrawable(getResources().getDrawable(R.drawable.user_noimage));
//                        }
//
//                        @Override
//                        public void onLoadingComplete(String imageUri, View view, Bitmap bitmap) {
//                            AppDelegate.LogT("MainActivity onBitmapLoaded");
//                            cimg_user.setImageBitmap(bitmap);
//                            img_c_loading1.setVisibility(View.GONE);
//                        }
//
//                        @Override
//                        public void onLoadingCancelled(String imageUri, View view) {
//
//                        }
//                    });
//                }
//
//                map_business.setOnInfoWindowClickListener(new GoogleMap.OnInfoWindowClickListener() {
//                    public void onInfoWindowClick(Marker marker) {
//
//                    }
//                });
//                return v;
//            }
//
//            @Override
//            public View getInfoContents(Marker marker) {
//                return null;
//            }
//        });
//    }


    protected Marker createMarker(Person markers) {
        try {
            AppDelegate.LogT("person data in vendor profile" + person);
            View marker = ((LayoutInflater) getSystemService(Context.LAYOUT_INFLATER_SERVICE)).inflate(R.layout.multi_profile, null);
            final android.widget.ImageView cimg_user = (android.widget.ImageView) marker.findViewById(R.id.imageView);
            final android.widget.ImageView img_loading = (android.widget.ImageView) marker.findViewById(R.id.img_loading);
            img_loading.setVisibility(View.VISIBLE);
            new Handler(Looper.getMainLooper()).post(new Runnable() {
                @Override
                public void run() {
                    AnimationDrawable frameAnimation = (AnimationDrawable) img_loading.getDrawable();
                    frameAnimation.setCallback(img_loading);
                    frameAnimation.setVisible(true, true);
                    frameAnimation.start();
                    ((Animatable) img_loading.getDrawable()).start();
                }
            });
            if (markers.type == DEAL_HERO) {
                if (AppDelegate.isValidString(markers.heroDealsModel.markerImage)) {
                    Bitmap remote_picture = null;
                    try {
                        if (AppDelegate.isValidString(markers.heroDealsModel.markerImage))
                            remote_picture = BitmapFactory.decodeStream((InputStream) new URL(markers.heroDealsModel.markerImage).getContent());
                        else AppDelegate.LogE("image not available");
                        cimg_user.setImageBitmap(remote_picture);
                        img_loading.setVisibility(View.GONE);
                    } catch (IOException e) {
                        AppDelegate.LogE(e);
                    }
                }
                customMarker = map_business.addMarker(new MarkerOptions()
                        .position(person.latlong)
                        .title(markers.heroDealsModel.name)
                        .snippet(markers.heroDealsModel.cityName)
                        .icon(BitmapDescriptorFactory.fromBitmap(AppDelegate.createDrawableFromView(this, marker))));
                show_pin_dialog(markers);
            } else {
                if (AppDelegate.isValidString(markers.adHocDealsModel.markerImage)) {
                    Bitmap remote_picture = null;
                    try {
                        if (AppDelegate.isValidString(markers.adHocDealsModel.markerImage))
                            remote_picture = BitmapFactory.decodeStream((InputStream) new URL(markers.adHocDealsModel.markerImage).getContent());
                        cimg_user.setImageBitmap(remote_picture);
                        img_loading.setVisibility(View.GONE);
                    } catch (IOException e) {
                        AppDelegate.LogE(e);
                    }
                }
                customMarker = map_business.addMarker(new MarkerOptions()
                        .position(person.latlong)
                        .title(markers.adHocDealsModel.name)
                        .snippet(markers.adHocDealsModel.cityName)
                        .icon(BitmapDescriptorFactory.fromBitmap(AppDelegate.createDrawableFromView(this, marker))));
                show_pin_dialog(markers);
            }
        } catch (Exception e) {
            e.printStackTrace();
        }
        return customMarker;
    }

    private void show_pin_dialog(final Person person) {
        // TODO Auto-generated method stub
        map_business.setInfoWindowAdapter(new GoogleMap.InfoWindowAdapter() {
            @Override
            public View getInfoWindow(Marker marker) {
//                Person person = markerPersonMap.get(marker.getId());
                View v = getLayoutInflater().inflate(R.layout.map_pin_showinfo_row, null);
                v.setLayoutParams(new RelativeLayout.LayoutParams(300, RelativeLayout.LayoutParams.WRAP_CONTENT));
                final carbon.widget.ImageView img_c_loading1;
                final CircleImageView cimg_user;
                TextView txt_c_company_name = (TextView) v.findViewById(R.id.txt_c_company_name);
                TextView txt_c_vendor_name = (TextView) v.findViewById(R.id.txt_c_vendor_name);
                TextView txt_c_country = (TextView) v.findViewById(R.id.txt_c_address);
                img_c_loading1 = (carbon.widget.ImageView) v.findViewById(R.id.img_c_loading1);
                cimg_user = (CircleImageView) v.findViewById(R.id.cimg_user);
                txt_c_company_name.setText(WordUtils.capitalize(marker.getTitle() + ""));
                txt_c_country.setText(marker.getSnippet() + "");
                if (person.type == DEAL_HERO) {
                    AppDelegate.LogT("is DEAL_HERO");
                    txt_c_company_name.setText(WordUtils.capitalize(person.name + ""));
                    txt_c_country.setText(person.heroDealsModel.cityName + "");
                    txt_c_vendor_name.setText(Html.fromHtml("<b>Vendor: </b>" + person.heroDealsModel.vendor_name));

                    AppDelegate.LogT("setValues called== person.heroDealsModel.company_address" + person.heroDealsModel.company_address);
                    if (AppDelegate.isValidString(person.heroDealsModel.image)) {
                        img_c_loading1.setVisibility(View.VISIBLE);
                        AnimationDrawable frameAnimation = (AnimationDrawable) img_c_loading1.getDrawable();
                        frameAnimation.setCallback(img_c_loading1);
                        frameAnimation.setVisible(true, true);
                        frameAnimation.start();

                        imageLoader.loadImage(person.heroDealsModel.image, options, new ImageLoadingListener() {
                            @Override
                            public void onLoadingStarted(String imageUri, View view) {

                            }

                            @Override
                            public void onLoadingFailed(String imageUri, View view, FailReason failReason) {
                                cimg_user.setImageDrawable(getResources().getDrawable(R.drawable.user_noimage));
                            }

                            @Override
                            public void onLoadingComplete(String imageUri, View view, Bitmap bitmap) {
                                AppDelegate.LogT("MainActivity onBitmapLoaded");
                                cimg_user.setImageBitmap(bitmap);
                                img_c_loading1.setVisibility(View.GONE);
                            }

                            @Override
                            public void onLoadingCancelled(String imageUri, View view) {

                            }
                        });
                    }
                } else {
                    txt_c_company_name.setText(WordUtils.capitalize(person.name + ""));
                    txt_c_country.setText(/*city + ", " + state + ", " +*/ person.adHocDealsModel.cityName + "");
                    AppDelegate.LogT("setValues called");
                    txt_c_vendor_name.setText(Html.fromHtml("<b>Vendor: </b>" + person.adHocDealsModel.vendor_name));

                    if (AppDelegate.isValidString(person.adHocDealsModel.image)) {
                        img_c_loading1.setVisibility(View.VISIBLE);
                        AnimationDrawable frameAnimation = (AnimationDrawable) img_c_loading1.getDrawable();
                        frameAnimation.setCallback(img_c_loading1);
                        frameAnimation.setVisible(true, true);
                        frameAnimation.start();

                        imageLoader.loadImage(person.adHocDealsModel.image, options, new ImageLoadingListener() {
                            @Override
                            public void onLoadingStarted(String imageUri, View view) {

                            }

                            @Override
                            public void onLoadingFailed(String imageUri, View view, FailReason failReason) {
                                cimg_user.setImageDrawable(getResources().getDrawable(R.drawable.user_noimage));
                            }

                            @Override
                            public void onLoadingComplete(String imageUri, View view, Bitmap bitmap) {
                                AppDelegate.LogT("MainActivity onBitmapLoaded");
                                cimg_user.setImageBitmap(bitmap);
                                img_c_loading1.setVisibility(View.GONE);
                            }

                            @Override
                            public void onLoadingCancelled(String imageUri, View view) {

                            }
                        });
                    }

                }
                return v;
            }

            @Override
            public View getInfoContents(final Marker marker) {

                return null;
            }
        });
    }
}
