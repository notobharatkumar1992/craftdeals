package com.craftedeals.activities;

import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.graphics.Bitmap;
import android.graphics.drawable.AnimationDrawable;
import android.graphics.drawable.Drawable;
import android.os.Build;
import android.os.Bundle;
import android.os.Handler;
import android.os.Message;
import android.support.v4.app.ActivityOptionsCompat;
import android.support.v4.util.Pair;
import android.support.v7.app.AlertDialog;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.DefaultItemAnimator;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.text.Html;
import android.view.View;
import android.view.ViewGroup;
import android.view.Window;
import android.view.WindowManager;
import android.widget.RelativeLayout;

import com.craftedeals.AppDelegate;
import com.craftedeals.Async.PostAsync;
import com.craftedeals.Models.AdHocDealsModel;
import com.craftedeals.Models.ItemsModel;
import com.craftedeals.Models.OpenHoursModel;
import com.craftedeals.Models.PostAysnc_Model;
import com.craftedeals.R;
import com.craftedeals.Utils.CircleImageView;
import com.craftedeals.Utils.Prefs;
import com.craftedeals.Utils.TransitionHelper;
import com.craftedeals.adapters.PurchasedAdhocDealItemAdapter;
import com.craftedeals.constants.ServerRequestConstants;
import com.craftedeals.constants.Tags;
import com.craftedeals.interfaces.OnReciveServerResponse;
import com.craftedeals.parser.JSONParser;
import com.github.aakira.expandablelayout.ExpandableRelativeLayout;
import com.nostra13.universalimageloader.core.DisplayImageOptions;
import com.nostra13.universalimageloader.core.ImageLoader;
import com.nostra13.universalimageloader.core.assist.FailReason;
import com.nostra13.universalimageloader.core.listener.ImageLoadingListener;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;

import carbon.widget.ImageView;
import carbon.widget.LinearLayout;
import carbon.widget.TextView;

import static com.craftedeals.activities.AdHocDealDetailActivity.COLLAPSE;
import static com.craftedeals.activities.AdHocDealDetailActivity.EXPAND;
import static com.craftedeals.activities.DealsActivity.DEAL_ADHOC;


/**
 * Created by Heena on 07-Dec-16.
 */
public class PurchaseAdhocActivity extends AppCompatActivity implements View.OnClickListener, OnReciveServerResponse {

    private Handler mHandler;
    TextView txt_c_comapny_open_hour, txt_c_comapny_contact_number, txt_c_company_name, txt_c_comapny_address, txt_c_expiry_label, txt_c_expiry_date, txt_c_start_date_label, txt_c_start_date, txt_c_end_date_label, txt_c_end_date, txt_c_title, txt_c_vendor_company_name, txt_c_coupan_id, txt_c_redeem, txt_c_redeem_status, txt_c_purchase_id, txt_c_purchase_on_id, txt_c_redeem_at, txt_c_select_from, txt_c_qty, txt_c_total_amount;
    ImageView img_c_expand_collapse;
    RecyclerView recycler_options;
    ExpandableRelativeLayout exp_layout;
    RelativeLayout rl_redeem;
    LinearLayout ll_c_validation;

    private int deal_id, purchase_id;
    private AdHocDealsModel adHocDealModel;
    private ImageView img_c_deal;
    android.widget.ImageView img_loading1, img_loading2;
    private PurchasedAdhocDealItemAdapter mAdapter;
    private String str_from;
    private ImageLoader imageLoader = ImageLoader.getInstance();
    private DisplayImageOptions options = new DisplayImageOptions.Builder().cacheInMemory(true)
            .cacheOnDisc(true).resetViewBeforeLoading(true).
                    build();
    CircleImageView img_circle_deal_image;
    public AlertDialog.Builder alert;

    public android.widget.LinearLayout ll_distance;
    public TextView txt_c_distance;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.LOLLIPOP) {
            Window window = this.getWindow();
            Drawable background = this.getResources().getDrawable(R.drawable.topbg);
            window.addFlags(WindowManager.LayoutParams.FLAG_DRAWS_SYSTEM_BAR_BACKGROUNDS);
            window.setStatusBarColor(getResources().getColor(android.R.color.transparent));
            window.setNavigationBarColor(getResources().getColor(android.R.color.transparent));
            window.setBackgroundDrawable(background);
        }
        setContentView(R.layout.purchase_adhocdeal);
        deal_id = getIntent().getIntExtra(Tags.deal_id, 0);
        purchase_id = getIntent().getIntExtra(Tags.purchase_id, 0);
        str_from = getIntent().getStringExtra(Tags.FROM);
        AppDelegate.LogT("Deal id is==>" + deal_id);
        initView();
        setHandler();
//        try {
//            RealmController.with(getApplication()).deleteEntryIfPurchaseAdhocDealExist(purchase_id);
//        } catch (Exception e) {
//            AppDelegate.LogE(e);
//        }
        if (getIntent() != null && AppDelegate.isValidString(getIntent().getStringExtra(Tags.FROM))) {
            if (getIntent().getStringExtra(Tags.FROM).equalsIgnoreCase(Tags.notification))
                execute_ViewNotificationApi(getIntent().getIntExtra(Tags.notification_id, 0));
        }
        if (!AppDelegate.isValidString(str_from) && getIntent().getExtras() != null && getIntent().getExtras().getParcelable(Tags.deal) != null) {
            adHocDealModel = getIntent().getExtras().getParcelable(Tags.deal);
        } else {
            execute_getPurchaseModel(purchase_id);
        }
        mHandler.sendEmptyMessage(3);
    }

    private void execute_ViewNotificationApi(int notification_id) {
        try {
            if (AppDelegate.haveNetworkConnection(this, true)) {
                ArrayList<PostAysnc_Model> mPostArrayList = new ArrayList<PostAysnc_Model>();
                PostAsync mPostAsyncObj;
                AppDelegate.getInstance(this).setPostParamsSecond(mPostArrayList, Tags.API_KEY, Tags.API_KEY_VALUE);
                AppDelegate.getInstance(this).setPostParamsSecond(mPostArrayList, Tags.user_id, new Prefs(this).getUserdata().id);
                AppDelegate.getInstance(this).setPostParamsSecond(mPostArrayList, Tags.nid, notification_id);
                mPostAsyncObj = new PostAsync(this, this, ServerRequestConstants.VIEW_NOTIFICATION,
                        mPostArrayList, null);
                mHandler.sendEmptyMessage(10);
                mPostAsyncObj.execute();
            }
        } catch (Exception e) {
            AppDelegate.LogE(e);
            AppDelegate.showToast(this, getString(R.string.try_again));
        }
    }

    private void execute_getPurchaseModel(int deal_id) {
        try {
            AppDelegate.LogT("deal_id id is==>" + deal_id);
            if (AppDelegate.haveNetworkConnection(this, true)) {
                ArrayList<PostAysnc_Model> mPostArrayList = new ArrayList<PostAysnc_Model>();
                AppDelegate.getInstance(this).setPostParamsSecond(mPostArrayList, Tags.API_KEY, Tags.API_KEY_VALUE);
                AppDelegate.getInstance(this).setPostParamsSecond(mPostArrayList, Tags.user_id, (new Prefs(this).getUserdata().id), ServerRequestConstants.Key_PostintValue);
                AppDelegate.getInstance(this).setPostParamsSecond(mPostArrayList, Tags.cart_id, deal_id + "");
                if (new Prefs(this).getCitydata() != null) {
                    AppDelegate.getInstance(this).setPostParamsSecond(mPostArrayList, Tags.LAT, new Prefs(this).getCitydata().lat);
                    AppDelegate.getInstance(this).setPostParamsSecond(mPostArrayList, Tags.LONG, new Prefs(this).getCitydata().lng);
                }
                PostAsync mPostAsyncObj;
                mPostAsyncObj = new PostAsync(this, this, ServerRequestConstants.GET_PURCHASE_DATA,
                        mPostArrayList, null);
                mHandler.sendEmptyMessage(10);
                mPostAsyncObj.execute();
            }
        } catch (Exception e) {
            AppDelegate.showToast(this, getResources().getString(R.string.try_again));
            AppDelegate.LogE(e);
        }
    }

    private void setHandler() {
        mHandler = new Handler() {
            @Override
            public void dispatchMessage(Message msg) {
                super.dispatchMessage(msg);
                switch (msg.what) {
                    case 10:
                        AppDelegate.showProgressDialog(PurchaseAdhocActivity.this);
                        break;
                    case 11:
                        AppDelegate.hideProgressDialog(PurchaseAdhocActivity.this);
                        break;
                    case 1:
                        break;
                    case 2:
                        break;
                    case 3:
                        if (adHocDealModel != null)
                            setview();
                        break;
                }
            }
        };
    }

    private void setview() {
        txt_c_comapny_contact_number.setText(Html.fromHtml("<b> Phone:  </b>" + adHocDealModel.company_contact_number + ""));
        try {
            if (AppDelegate.isValidString(adHocDealModel.company_opening_hours)) {
                JSONArray object = new JSONArray(adHocDealModel.company_opening_hours);
                ArrayList<OpenHoursModel> openHoursModels = new ArrayList<>();
                StringBuilder sb = new StringBuilder();
                for (int i = 0; i < object.length(); i++) {
                    JSONObject object1 = object.getJSONObject(i);
                    OpenHoursModel openHoursModel = new OpenHoursModel();
                    openHoursModel.day = JSONParser.getString(object1, "day");
                    openHoursModel.from = JSONParser.getString(object1, "from");
                    openHoursModel.to = JSONParser.getString(object1, "to");
                    try {
                        openHoursModel.from = new SimpleDateFormat("hh:mm a").format(new SimpleDateFormat("HH:mm").parse(openHoursModel.from));
                        openHoursModel.to = new SimpleDateFormat("hh:mm a").format(new SimpleDateFormat("HH:mm").parse(openHoursModel.to));
                    } catch (ParseException e) {
                        AppDelegate.LogE(e);
                    }
                    sb.append("<b>" + openHoursModel.day + " : </b>" + openHoursModel.from + " - " + openHoursModel.to + "<br>");
//                openHoursModels.add(openHoursModel);
                }
                String hours = sb.toString();
                if (hours.length() > 0) {
                    hours = hours.substring(0, hours.length() - 1).toString();
                    txt_c_comapny_open_hour.setText(Html.fromHtml(hours));
                }
            }

            try {
                if (AppDelegate.isValidString(adHocDealModel.distance)) {
                    ll_distance.setVisibility(View.VISIBLE);
                    txt_c_distance.setText(AppDelegate.roundOff(Double.parseDouble(adHocDealModel.distance)) + " KM");
                } else
                    ll_distance.setVisibility(View.GONE);
            } catch (Exception e) {
                AppDelegate.LogE(e);
            }
// setHours(openHoursModels);
        } catch (JSONException e) {
            AppDelegate.LogE(e);
        }
        img_loading1.setVisibility(View.VISIBLE);
        AnimationDrawable frameAnimation = (AnimationDrawable) img_loading1.getDrawable();
        frameAnimation.setCallback(img_loading1);
        frameAnimation.setVisible(true, true);
        frameAnimation.start();

        imageLoader.loadImage(adHocDealModel.image, options, new ImageLoadingListener() {
            @Override
            public void onLoadingStarted(String imageUri, View view) {
            }

            @Override
            public void onLoadingFailed(String imageUri, View view, FailReason failReason) {
                img_c_deal.setImageDrawable(getResources().getDrawable(R.drawable.deal_noimage));
                img_loading1.setVisibility(View.GONE);
            }

            @Override
            public void onLoadingComplete(String imageUri, View view, Bitmap loadedImage) {
                img_c_deal.setImageBitmap(loadedImage);
                img_loading1.setVisibility(View.GONE);
            }

            @Override
            public void onLoadingCancelled(String imageUri, View view) {
            }
        });
        txt_c_company_name.setText(adHocDealModel.company_name);
        txt_c_comapny_address.setText(adHocDealModel.company_address);
        txt_c_title.setText(AppDelegate.isValidString(adHocDealModel.name) ? adHocDealModel.name : "Craftedeals");
        txt_c_vendor_company_name.setText(AppDelegate.isValidString(adHocDealModel.company_name) ? adHocDealModel.company_name : "");
        txt_c_coupan_id.setText("Coupan Code: " + adHocDealModel.coupon_name);
        txt_c_purchase_id.setText(adHocDealModel.payment_id + "");
        String date = adHocDealModel.coupon_created;
        AppDelegate.LogT("adHocDealsModel.date in adapter==" + date);
        try {
            date = date.replaceAll("/+0000", "");
            date = new SimpleDateFormat("dd MMM yyyy hh:mm a").format(new SimpleDateFormat("yyyy-MM-dd'T'HH:mm:ss").parse(date));
            txt_c_purchase_on_id.setText(date);
        } catch (Exception e) {
            AppDelegate.LogE(e);
        }
        img_loading2.setVisibility(View.VISIBLE);
        frameAnimation = (AnimationDrawable) img_loading2.getDrawable();
        frameAnimation.setCallback(img_loading2);
        frameAnimation.setVisible(true, true);
        frameAnimation.start();

        imageLoader.loadImage(adHocDealModel.company_image, options, new ImageLoadingListener() {
            @Override
            public void onLoadingStarted(String imageUri, View view) {
            }

            @Override
            public void onLoadingFailed(String imageUri, View view, FailReason failReason) {
                img_circle_deal_image.setImageDrawable(getResources().getDrawable(R.drawable.user_noimage));
                img_loading2.setVisibility(View.GONE);
            }

            @Override
            public void onLoadingComplete(String imageUri, View view, Bitmap loadedImage) {
                img_circle_deal_image.setImageBitmap(loadedImage);
                img_loading2.setVisibility(View.GONE);
            }

            @Override
            public void onLoadingCancelled(String imageUri, View view) {
            }
        });
        if (adHocDealModel.coupon_status == 1) {
            txt_c_redeem_status.setText("REDEEMED");
            txt_c_redeem_status.setVisibility(View.VISIBLE);
            rl_redeem.setVisibility(View.GONE);
            ll_c_validation.setVisibility(View.GONE);
            txt_c_redeem_at.setVisibility(View.VISIBLE);
            txt_c_expiry_label.setVisibility(View.GONE);
            txt_c_expiry_date.setVisibility(View.GONE);
            try {
                date = adHocDealModel.coupon_modified;
                date = date.replaceAll("/+0000", "");
                date = new SimpleDateFormat("dd MMM yyyy hh:mm a").format(new SimpleDateFormat("yyyy-MM-dd'T'HH:mm:ss").parse(date));
                txt_c_redeem_at.setText("Redeemed at " + date);
            } catch (Exception e) {
                AppDelegate.LogE(e);
            }
        } else {
            ll_c_validation.setVisibility(View.VISIBLE);
            if (adHocDealModel.deal_type == 1) {
                txt_c_start_date_label.setText("Available Deal :");
                try {
                    txt_c_start_date.setText(adHocDealModel.volume_of_deal - adHocDealModel.total_deal + "");
                } catch (Exception e) {
                    AppDelegate.LogE(e);
                }
                txt_c_end_date_label.setText("Purchased Deal :");
                txt_c_end_date.setText(adHocDealModel.total_deal + "");
                txt_c_expiry_label.setVisibility(View.VISIBLE);
                txt_c_expiry_date.setVisibility(View.VISIBLE);
                txt_c_expiry_label.setText("Redeem deal before it expires : ");
                try {
                    String expiry_date = adHocDealModel.expiry_date;
                    expiry_date = expiry_date.replaceAll("/+0000", "");
                    expiry_date = new SimpleDateFormat("dd MMM yyyy").format(new SimpleDateFormat("yyyy-MM-dd'T'HH:mm:ss").parse(expiry_date));
                    txt_c_expiry_date.setText(expiry_date + "");
                } catch (Exception e) {
                    AppDelegate.LogE(e);
                }
            } else {
                txt_c_expiry_label.setVisibility(View.GONE);
                txt_c_expiry_date.setVisibility(View.GONE);
                try {
//                    String starting_date = adHocDealModel.starting_date;
//                    starting_date = starting_date.replaceAll("/+0000", "");
//                    starting_date = new SimpleDateFormat("dd MMM yyyy").format(new SimpleDateFormat("yyyy-MM-dd'T'HH:mm:ss").parse(starting_date));
//                    txt_c_start_date_label.setText("Start date :");
//                    txt_c_start_date.setText(starting_date);

                    txt_c_start_date_label.setText("Purchased Deal :");
                    txt_c_start_date.setText(adHocDealModel.total_deal + "");

                    String expiry_date = adHocDealModel.expiry_date;
                    expiry_date = expiry_date.replaceAll("/+0000", "");
                    expiry_date = new SimpleDateFormat("dd MMM yyyy").format(new SimpleDateFormat("yyyy-MM-dd'T'HH:mm:ss").parse(expiry_date));
                    txt_c_end_date_label.setText("Redeem deal before it expires :");
                    txt_c_end_date.setText(expiry_date + "");
                } catch (Exception e) {
                    AppDelegate.LogE(e);
                }
            }
            if (adHocDealModel != null && adHocDealModel.expired == 0) {
                txt_c_redeem_status.setVisibility(View.GONE);
                rl_redeem.setVisibility(View.VISIBLE);
            } else {
                rl_redeem.setVisibility(View.GONE);
                txt_c_redeem_status.setVisibility(View.VISIBLE);
                txt_c_redeem_status.setText(getString(R.string.expired));
            }
            txt_c_redeem_at.setVisibility(View.GONE);
        }

//        txt_c_select_from.setText(" Select from " + adHocDealModel.arrayItems.size() + " options");
        txt_c_total_amount.setText("$" + adHocDealModel.total_amount + "");
        int count = 0;
        for (int i = 0; i < adHocDealModel.arrayItems.size(); i++) {
            count += Integer.parseInt(adHocDealModel.arrayItems.get(i).no_of_item);
        }
        txt_c_qty.setText("Qty: " + count + "");
        showMypurchaselist(adHocDealModel.arrayItems);
    }

    private void initView() {
        txt_c_expiry_date = (TextView) findViewById(R.id.txt_c_expiry_date);
        txt_c_expiry_label = (TextView) findViewById(R.id.txt_c_expiry_label);
        txt_c_title = (TextView) findViewById(R.id.txt_c_title);
        txt_c_vendor_company_name = (TextView) findViewById(R.id.txt_c_vendor_company_name);
        txt_c_coupan_id = (TextView) findViewById(R.id.txt_c_coupan_id);
        txt_c_redeem = (TextView) findViewById(R.id.txt_c_redeem);
        txt_c_redeem_status = (TextView) findViewById(R.id.txt_c_redeem_status);
        txt_c_purchase_id = (TextView) findViewById(R.id.txt_c_purchase_id);
        txt_c_purchase_on_id = (TextView) findViewById(R.id.txt_c_purchase_on_id);
        txt_c_redeem_at = (TextView) findViewById(R.id.txt_c_redeem_at);
        txt_c_select_from = (TextView) findViewById(R.id.txt_c_select_from);
        txt_c_qty = (TextView) findViewById(R.id.txt_c_qty);
        txt_c_total_amount = (TextView) findViewById(R.id.txt_c_total_amount);
        rl_redeem = (RelativeLayout) findViewById(R.id.rl_redeem);
        img_c_expand_collapse = (ImageView) findViewById(R.id.img_c_expand_collapse);
        recycler_options = (RecyclerView) findViewById(R.id.recycler_options);
        exp_layout = (ExpandableRelativeLayout) findViewById(R.id.exp_layout);
        img_loading1 = (android.widget.ImageView) findViewById(R.id.img_loading1);
        img_c_deal = (ImageView) findViewById(R.id.img_c_deal);
        img_circle_deal_image = (CircleImageView) findViewById(R.id.img_circle_deal_image);
        img_loading2 = (android.widget.ImageView) findViewById(R.id.img_loading2);
        txt_c_company_name = (TextView) findViewById(R.id.txt_c_company_name);
        txt_c_comapny_address = (TextView) findViewById(R.id.txt_c_comapny_address);
        findViewById(R.id.txt_c_view_profile).setOnClickListener(this);
//      expandableView(exp_layout, COLLAPSE);
        txt_c_comapny_contact_number = (TextView) findViewById(R.id.txt_c_comapny_contact_number);
        txt_c_comapny_contact_number.setOnClickListener(this);
        txt_c_comapny_open_hour = (TextView) findViewById(R.id.txt_c_comapny_open_hour);
        recycler_options.setLayoutManager(new LinearLayoutManager(this));
        recycler_options.setHasFixedSize(true);
        recycler_options.setItemAnimator(new DefaultItemAnimator());
        recycler_options.setNestedScrollingEnabled(false);
        img_c_expand_collapse.setOnClickListener(this);
        img_c_expand_collapse.setSelected(false);
        txt_c_redeem.setOnClickListener(this);
        findViewById(R.id.img_c_back).setOnClickListener(this);
        txt_c_end_date = (TextView) findViewById(R.id.txt_c_end_date);
        txt_c_start_date = (TextView) findViewById(R.id.txt_c_start_date);
        txt_c_start_date_label = (TextView) findViewById(R.id.txt_c_start_date_label);
        txt_c_end_date_label = (TextView) findViewById(R.id.txt_c_end_date_label);
        ll_c_validation = (LinearLayout) findViewById(R.id.ll_c_validation);

        ll_distance = (android.widget.LinearLayout) findViewById(R.id.ll_distance);
        txt_c_distance = (TextView) findViewById(R.id.txt_c_distance);
    }

    public void expandableView(final ExpandableRelativeLayout expLayout, final int value) {
        expLayout.post(new Runnable() {
            @Override
            public void run() {
                if (value == COLLAPSE) {
                    expLayout.collapse();
                } else {
                    expLayout.expand();
                }
            }
        });
    }

    private void showMypurchaselist(ArrayList<ItemsModel> cart_itemsArrayList) {
        AppDelegate.LogT("cart_itemsArrayList==" + cart_itemsArrayList);
        mAdapter = new PurchasedAdhocDealItemAdapter(this, cart_itemsArrayList);
        setListViewHeightBasedOnChildren(recycler_options, cart_itemsArrayList);
        recycler_options.setAdapter(mAdapter);
    }

    public void setListViewHeightBasedOnChildren(RecyclerView listView, ArrayList<ItemsModel> arrayItems) {
        try {
            if (arrayItems == null && arrayItems.size() == 0) {
                //pre-condition
                AppDelegate.LogE("Adapter is null");
                ViewGroup.LayoutParams params = listView.getLayoutParams();
                params.height = 80;
                listView.setLayoutParams(params);
                listView.requestLayout();
                return;
            }
            int totalHeight = 0;
            float itemCount = arrayItems.size();
            for (int i = 0; i < itemCount; i++) {
                totalHeight += AppDelegate.dpToPix(PurchaseAdhocActivity.this, 77);
            }
            ViewGroup.LayoutParams params = listView.getLayoutParams();
            params.height = totalHeight;
            AppDelegate.LogT("height = " + totalHeight);
            exp_layout.setLayoutParams(params);
            exp_layout.requestLayout();
            recycler_options.setLayoutParams(params);
            recycler_options.requestLayout();
        } catch (Exception e) {
            AppDelegate.LogE(e);
        }
    }

    @Override
    public void onClick(View v) {
        switch (v.getId()) {
            case R.id.img_c_back:
                onBackPressed();
                onBackPressed();
                break;
            case R.id.img_c_expand_collapse:
                if (img_c_expand_collapse.isSelected()) {
                    img_c_expand_collapse.setSelected(false);
                    expandableView(exp_layout, EXPAND);
                } else {
                    img_c_expand_collapse.setSelected(true);
                    expandableView(exp_layout, COLLAPSE);
                }
                AppDelegate.LogT("img_c_expand_collapse.isSelected() => " + img_c_expand_collapse.isSelected());
                break;
            case R.id.txt_c_redeem:
                if (adHocDealModel != null && adHocDealModel.coupon_status == 1) {
                    AppDelegate.showToast(this, "This deal already redeemed");
                } else {
                    if (adHocDealModel != null && adHocDealModel.expired == 0)
                        showredeemAlert(this, getString(R.string.alert_adhoc), getString(R.string.alert_adhoc_message));
                    else
                        AppDelegate.showToast(this, getString(R.string.deal_expired));
                }
                break;
            case R.id.txt_c_view_profile:
                Intent intent = new Intent(this, VendorProfileActivity.class);
                final Pair<View, String>[] pairs = TransitionHelper.createSafeTransitionParticipants(this, false, new Pair<>(txt_c_title, "square_blue_name_1"));
                ActivityOptionsCompat transitionActivityOptions = ActivityOptionsCompat.makeSceneTransitionAnimation(this, pairs);
                intent.putExtra(Tags.deal_id, adHocDealModel.id);
                intent.putExtra(Tags.vendor_id, adHocDealModel.user_id);
                intent.putExtra(Tags.type, 2);
                startActivity(intent/*, transitionActivityOptions.toBundle()*/);
                break;
            case R.id.txt_c_comapny_contact_number:
                AppDelegate.makeCall(PurchaseAdhocActivity.this, adHocDealModel != null ? adHocDealModel.company_contact_number : "");
                break;
        }
    }


    private void executeRedeemApi() {
        try {
            if (AppDelegate.haveNetworkConnection(this, true)) {
                ArrayList<PostAysnc_Model> mPostArrayList = new ArrayList<PostAysnc_Model>();
                AppDelegate.getInstance(this).setPostParamsSecond(mPostArrayList, Tags.API_KEY, Tags.API_KEY_VALUE);
                if (new Prefs(this).getUserdata() != null)
                    AppDelegate.getInstance(this).setPostParamsSecond(mPostArrayList, Tags.user_id, (new Prefs(this).getUserdata().id), ServerRequestConstants.Key_PostintValue);
                AppDelegate.getInstance(this).setPostParamsSecond(mPostArrayList, Tags.coupon_id, adHocDealModel.coupon_id);
                AppDelegate.getInstance(this).setPostParamsSecond(mPostArrayList, Tags.deal_id, adHocDealModel.adhocdeal_id);

                PostAsync mPostAsyncObj;
                mPostAsyncObj = new PostAsync(this, this, ServerRequestConstants.REDEEM_ADHOC,
                        mPostArrayList, null);
                mHandler.sendEmptyMessage(10);
                mPostAsyncObj.execute();
            }
        } catch (Exception e) {
            AppDelegate.showToast(this, "Please try again.");
            AppDelegate.LogE(e);
        }
    }

    @Override
    public void setOnReciveResult(String apiName, String result) {

        mHandler.sendEmptyMessage(11);
        if (!AppDelegate.isValidString(result)) {
            AppDelegate.showToast(PurchaseAdhocActivity.this, getResources().getString(R.string.time_out));
            return;
        }
        if (apiName.equals(ServerRequestConstants.REDEEM_ADHOC)) {
            parseRedeem(result);
        } else if (apiName.equalsIgnoreCase(ServerRequestConstants.GET_PURCHASE_DATA)) {
            parsePurchaseDealModel(result);
        } else if (apiName.equalsIgnoreCase(ServerRequestConstants.VIEW_NOTIFICATION)) {
            parseViewNotification(result);
        }
    }

    private void parseViewNotification(String result) {
        try {
            JSONObject jsonObject = new JSONObject(result);
//            if (jsonObject.getInt(Tags.status) == 1) {
//                AppDelegate.showToast(this, jsonObject.getString(Tags.message));
//            } else {
//                AppDelegate.showToast(this, jsonObject.getString(Tags.message));
//            }
        } catch (JSONException e) {
            AppDelegate.LogE(e);
        }
    }

    private void parsePurchaseDealModel(String result) {
        try {
            JSONObject jsonobj = new JSONObject(result);
            if (jsonobj.getInt(Tags.status) == 1) {
                JSONObject object = jsonobj.getJSONObject(Tags.data);
                adHocDealModel = new AdHocDealsModel();
                adHocDealModel.distance = object.optString(Tags.distance);

                adHocDealModel.purchase_id = JSONParser.getString(object, Tags.id);
                adHocDealModel.payment_id = JSONParser.getString(object, Tags.payment_id);
                adHocDealModel.coupon_id = JSONParser.getString(object, Tags.coupon_id);
                adHocDealModel.coupon_name = JSONParser.getString(object.getJSONObject("coupon"), Tags.name);
                adHocDealModel.coupon_status = JSONParser.getInt(object.getJSONObject("coupon"), Tags.status);
                adHocDealModel.coupon_created = JSONParser.getString(object.getJSONObject("coupon"), Tags.created);
                adHocDealModel.coupon_modified = JSONParser.getString(object.getJSONObject("coupon"), Tags.modified);
                AppDelegate.LogT("adHocDealModel.coupon_created==" + adHocDealModel.coupon_created);
                AppDelegate.LogT("adHocDealModel.coupon_modified==" + adHocDealModel.coupon_modified);
                adHocDealModel.total_amount = JSONParser.getString(object, Tags.total_amount);
                AppDelegate.LogT("adHocDealModel.total_amount==" + adHocDealModel.total_amount);
                JSONArray cart_items_array = object.getJSONArray(Tags.cart_items);

                JSONObject heroobj = object.getJSONObject("adhocdeal");

                adHocDealModel.adhocdeal_id = heroobj.getInt(Tags.id);
                adHocDealModel.user_id = heroobj.getInt(Tags.user_id);
                adHocDealModel.name = heroobj.getString(Tags.name);
                adHocDealModel.use_the_offer = heroobj.getString(Tags.use_the_offer);
                adHocDealModel.things_to_remebers = heroobj.getString(Tags.things_to_remebers);
                adHocDealModel.deal_category_id = heroobj.getInt(Tags.deal_category_id);
                adHocDealModel.sub_category_id = heroobj.getInt(Tags.sub_category_id);
                adHocDealModel.facilities = heroobj.getString(Tags.facilities);
                adHocDealModel.item_detail = heroobj.getString(Tags.item_detail);
                adHocDealModel.deal_type = heroobj.getInt(Tags.deal_type);
                adHocDealModel.starting_date = heroobj.getString(Tags.starting_date);
                adHocDealModel.expiry_date = heroobj.getString(Tags.expiry_date);
                adHocDealModel.price = heroobj.getInt(Tags.price);
                adHocDealModel.discount_cost = heroobj.getInt(Tags.discount_cost);
                adHocDealModel.total_deal = heroobj.getInt(Tags.total_deal);
                adHocDealModel.volume_of_deal = heroobj.getInt(Tags.volume_of_deal);
                adHocDealModel.timing_to = heroobj.getString(Tags.timing_to);
                adHocDealModel.timing_from = heroobj.getString(Tags.timing_from);
                adHocDealModel.valid_on = heroobj.getString(Tags.valid_on);
                adHocDealModel.vendor_delete_deal = heroobj.getInt(Tags.vendor_delete_deal);
                adHocDealModel.favourite = heroobj.getInt(Tags.favourite);
                if (heroobj.has(Tags.liked))
                    adHocDealModel.liked = heroobj.getInt(Tags.liked);
                if (heroobj.has(Tags.expired))
                    adHocDealModel.expired = heroobj.getInt(Tags.expired);
                if (AppDelegate.isValidString(heroobj.getString(Tags.user))) {
                    JSONObject vendor = heroobj.getJSONObject(Tags.user);
                    adHocDealModel.vendor_name = vendor.getString(Tags.name);
                    if (vendor.has(Tags.busines) && AppDelegate.isValidString(vendor.getString(Tags.busines))) {
                        adHocDealModel.latitude = vendor.getJSONObject(Tags.busines).getString(Tags.latitude);
                        adHocDealModel.longitude = vendor.getJSONObject(Tags.busines).getString(Tags.longitude);
                        adHocDealModel.about_me = vendor.getJSONObject(Tags.busines).getString(Tags.about_business);
                        adHocDealModel.company_address = vendor.getJSONObject(Tags.busines).getString(Tags.location_address);
                        adHocDealModel.company_name = vendor.getJSONObject(Tags.busines).getString(Tags.name);
                        adHocDealModel.company_image = vendor.getJSONObject(Tags.busines).getString(Tags.image);
                        adHocDealModel.company_opening_hours = vendor.getJSONObject(Tags.busines).getString(Tags.open_hours);
                        adHocDealModel.company_contact_number = vendor.getJSONObject(Tags.busines).getString(Tags.contact_number);
//                        adHocDealModel.cityName = vendor.getJSONObject(Tags.busines).getString(Tags.cityName);
                    }
                }

                if (AppDelegate.isValidString(heroobj.getString(Tags.images))) {
                    JSONArray img = heroobj.getJSONArray(Tags.images);
                    for (int k = 0; k < img.length(); k++) {
                        JSONObject json = img.getJSONObject(k);
                        adHocDealModel.image = json.getString(Tags.image);
                    }
                }

                ArrayList<ItemsModel> arrayItems = new ArrayList<>();
                for (int k = 0; k < cart_items_array.length(); k++) {
                    JSONObject cartObject = cart_items_array.getJSONObject(k);
                    JSONObject cartItemobj = cartObject.getJSONObject("item");
                    ItemsModel itemsModel = new ItemsModel();
                    itemsModel.id = JSONParser.getInt(cartItemobj, Tags.id);
                    itemsModel.adhocdeal_id = JSONParser.getInt(cartItemobj, Tags.adhocdeal_id);
                    itemsModel.name = JSONParser.getString(cartItemobj, Tags.name);
                    itemsModel.actual_cost = JSONParser.getInt(cartItemobj, Tags.actual_cost);
                    itemsModel.price = JSONParser.getInt(cartItemobj, Tags.price);
                    itemsModel.discount_cost = JSONParser.getInt(cartItemobj, Tags.discount_cost);
                    itemsModel.details = JSONParser.getString(cartItemobj, Tags.details);
                    itemsModel.created = JSONParser.getString(cartItemobj, Tags.created);
                    itemsModel.total_amount = JSONParser.getString(cartObject, Tags.total_amount);
                    itemsModel.no_of_item = JSONParser.getString(cartObject, Tags.no_of_item);

                    arrayItems.add(itemsModel);
                }
                adHocDealModel.arrayItems = arrayItems;
                mHandler.sendEmptyMessage(3);
            } else {
                AppDelegate.showAlert(this, jsonobj.getString(Tags.message));
            }
        } catch (JSONException e) {
            AppDelegate.LogE(e);
        }
    }

    private void parseRedeem(String result) {
        try {
            JSONObject jsonobj = new JSONObject(result);
            if (jsonobj.getInt(Tags.status) == 1 && jsonobj.getInt(Tags.dataFlow) == 1) {
                // AppDelegate.showToast(this, jsonobj.getString(Tags.message));
                adHocDealModel.coupon_status = 1;
                txt_c_redeem.setBackgroundColor(getResources().getColor(R.color.botom_gray_bg_color));
                txt_c_redeem.setText("REDEEMED");
                mAdapter.notifyDataSetChanged();
                adHocDealModel.coupon_status = 1;
                adHocDealModel.coupon_modified = jsonobj.getJSONObject(Tags.data).getString(Tags.modified);
                mHandler.sendEmptyMessage(3);
                if (MyPurchaseActivity.mHandler != null) {
                    MyPurchaseActivity.mHandler.sendEmptyMessage(3);
                }
                AppDelegate.trackRedeemDeal(adHocDealModel.name, DEAL_ADHOC, "My Purchase Detail");
                Intent intent = new Intent(this, CongratulationActivity.class);
                intent.putExtra(Tags.deal, adHocDealModel.company_name);
                intent.putExtra(Tags.deal_type, DEAL_ADHOC);
                intent.putExtra(Tags.types, CongratulationActivity.DEAL_REDEEMED);
                startActivity(intent);
            } else {
                AppDelegate.showAlert(this, jsonobj.getString(Tags.message));
            }
        } catch (JSONException e) {
            AppDelegate.LogE(e);
        }
    }

    public void showredeemAlert(Context mContext, String Title, String Message) {
        try {
            alert = new AlertDialog.Builder(mContext);
            alert.setCancelable(false);
            alert.setTitle(Title);
            alert.setMessage(Message);
            alert.setPositiveButton(
                    Tags.OK,
                    new DialogInterface.OnClickListener() {

                        @Override
                        public void onClick(DialogInterface dialog, int which) {
                            executeRedeemApi();
                            dialog.dismiss();
                        }
                    }).setNegativeButton(Tags.CANCEL, new DialogInterface.OnClickListener() {
                @Override
                public void onClick(DialogInterface dialog, int which) {
                    dialog.dismiss();
                }
            });
            alert.show();
        } catch (Exception e) {
            AppDelegate.LogE(e);
        }
    }
}
