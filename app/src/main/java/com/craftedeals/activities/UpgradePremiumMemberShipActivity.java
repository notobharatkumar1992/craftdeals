package com.craftedeals.activities;

import android.app.Activity;
import android.content.DialogInterface;
import android.content.Intent;
import android.os.AsyncTask;
import android.os.Bundle;
import android.os.Handler;
import android.os.Message;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentActivity;
import android.support.v4.view.ViewPager;
import android.support.v7.app.AlertDialog;
import android.util.Log;
import android.view.View;
import android.widget.ImageView;
import android.widget.Toast;

import com.braintreepayments.api.BraintreeFragment;
import com.braintreepayments.api.dropin.DropInActivity;
import com.braintreepayments.api.dropin.DropInRequest;
import com.braintreepayments.api.dropin.DropInResult;
import com.braintreepayments.api.exceptions.InvalidArgumentException;
import com.craftedeals.AppDelegate;
import com.craftedeals.Async.PostAsync;
import com.craftedeals.Models.MembershipModel;
import com.craftedeals.Models.PostAysnc_Model;
import com.craftedeals.Models.UserDataModel;
import com.craftedeals.R;
import com.craftedeals.Utils.Prefs;
import com.craftedeals.adapters.PagerUpgradeMembershipAdapter;
import com.craftedeals.constants.ServerRequestConstants;
import com.craftedeals.constants.Tags;
import com.craftedeals.fragments.UpgradePremiumFragment;
import com.craftedeals.interfaces.OnReciveServerResponse;
import com.craftedeals.parser.JSONParser;
import com.paypal.android.sdk.payments.PayPalAuthorization;
import com.paypal.android.sdk.payments.PayPalFuturePaymentActivity;
import com.paypal.android.sdk.payments.PayPalProfileSharingActivity;
import com.paypal.android.sdk.payments.PayPalService;
import com.paypal.android.sdk.payments.PaymentActivity;
import com.paypal.android.sdk.payments.PaymentConfirmation;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.io.BufferedReader;
import java.io.BufferedWriter;
import java.io.IOException;
import java.io.InputStreamReader;
import java.io.OutputStream;
import java.io.OutputStreamWriter;
import java.io.UnsupportedEncodingException;
import java.net.HttpURLConnection;
import java.net.URL;
import java.net.URLEncoder;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.Map;

import javax.net.ssl.HttpsURLConnection;

import carbon.widget.TextView;

/**
 * Created by Bharat on 23-Nov-16.
 */
public class UpgradePremiumMemberShipActivity extends FragmentActivity implements OnReciveServerResponse, View.OnClickListener
{
    String brainTreeToken;
    private String str_amount;
    private Handler mHandler;
    private ViewPager view_pager;
    private PagerUpgradeMembershipAdapter mPagerAdapter;
    private ArrayList<Fragment> arrayList = new ArrayList<>();
    private ImageView img_01, img_02;
    TextView txt_c_title;
    public UserDataModel userDataModel;
    private MembershipModel membershipModel;
    public ArrayList<MembershipModel> arrayMembership = new ArrayList<>();
    BraintreeFragment mBraintreeFragment;

    @Override
    protected void onCreate(Bundle savedInstanceState)
    {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.membership_activity);
        initView();
        setHandler();
        callMembershipAsync();

        try
        {
            mBraintreeFragment = BraintreeFragment.newInstance(this, "eyJ2ZXJzaW9uIjoyLCJhdXRob3JpemF0aW9uRmluZ2VycHJpbnQiOiJiYjRkNTI5NTJkYzQ1ODFhZDAwMTVjOTc5ZjU1MDM1MDE2OTNmOTE2ZmUwMTM3ZDBkZGVhOTVhYzBkNjM0NDUwfGNyZWF0ZWRfYXQ9MjAxNy0wOC0xNlQwOToyNjoxMi4zMjM0MzY5NTQrMDAwMFx1MDAyNm1lcmNoYW50X2lkPTM0OHBrOWNnZjNiZ3l3MmJcdTAwMjZwdWJsaWNfa2V5PTJuMjQ3ZHY4OWJxOXZtcHIiLCJjb25maWdVcmwiOiJodHRwczovL2FwaS5zYW5kYm94LmJyYWludHJlZWdhdGV3YXkuY29tOjQ0My9tZXJjaGFudHMvMzQ4cGs5Y2dmM2JneXcyYi9jbGllbnRfYXBpL3YxL2NvbmZpZ3VyYXRpb24iLCJjaGFsbGVuZ2VzIjpbXSwiZW52aXJvbm1lbnQiOiJzYW5kYm94IiwiY2xpZW50QXBpVXJsIjoiaHR0cHM6Ly9hcGkuc2FuZGJveC5icmFpbnRyZWVnYXRld2F5LmNvbTo0NDMvbWVyY2hhbnRzLzM0OHBrOWNnZjNiZ3l3MmIvY2xpZW50X2FwaSIsImFzc2V0c1VybCI6Imh0dHBzOi8vYXNzZXRzLmJyYWludHJlZWdhdGV3YXkuY29tIiwiYXV0aFVybCI6Imh0dHBzOi8vYXV0aC52ZW5tby5zYW5kYm94LmJyYWludHJlZWdhdGV3YXkuY29tIiwiYW5hbHl0aWNzIjp7InVybCI6Imh0dHBzOi8vY2xpZW50LWFuYWx5dGljcy5zYW5kYm94LmJyYWludHJlZWdhdGV3YXkuY29tLzM0OHBrOWNnZjNiZ3l3MmIifSwidGhyZWVEU2VjdXJlRW5hYmxlZCI6dHJ1ZSwicGF5cGFsRW5hYmxlZCI6dHJ1ZSwicGF5cGFsIjp7ImRpc3BsYXlOYW1lIjoiQWNtZSBXaWRnZXRzLCBMdGQuIChTYW5kYm94KSIsImNsaWVudElkIjpudWxsLCJwcml2YWN5VXJsIjoiaHR0cDovL2V4YW1wbGUuY29tL3BwIiwidXNlckFncmVlbWVudFVybCI6Imh0dHA6Ly9leGFtcGxlLmNvbS90b3MiLCJiYXNlVXJsIjoiaHR0cHM6Ly9hc3NldHMuYnJhaW50cmVlZ2F0ZXdheS5jb20iLCJhc3NldHNVcmwiOiJodHRwczovL2NoZWNrb3V0LnBheXBhbC5jb20iLCJkaXJlY3RCYXNlVXJsIjpudWxsLCJhbGxvd0h0dHAiOnRydWUsImVudmlyb25tZW50Tm9OZXR3b3JrIjp0cnVlLCJlbnZpcm9ubWVudCI6Im9mZmxpbmUiLCJ1bnZldHRlZE1lcmNoYW50IjpmYWxzZSwiYnJhaW50cmVlQ2xpZW50SWQiOiJtYXN0ZXJjbGllbnQzIiwiYmlsbGluZ0FncmVlbWVudHNFbmFibGVkIjp0cnVlLCJtZXJjaGFudEFjY291bnRJZCI6ImFjbWV3aWRnZXRzbHRkc2FuZGJveCIsImN1cnJlbmN5SXNvQ29kZSI6IlVTRCJ9LCJtZXJjaGFudElkIjoiMzQ4cGs5Y2dmM2JneXcyYiIsInZlbm1vIjoib2ZmIn0=");
            // mBraintreeFragment is ready to use!
        }
        catch (InvalidArgumentException e)
        {
            // There was an issue with your authorization string.
        }
    }

    private void setHandler()
    {
        mHandler = new Handler()
        {
            @Override
            public void dispatchMessage(Message msg)
            {
                super.dispatchMessage(msg);
                if (msg.what == 10)
                {
                    AppDelegate.showProgressDialog(UpgradePremiumMemberShipActivity.this);
                }
                else if (msg.what == 11)
                {
                    AppDelegate.hideProgressDialog(UpgradePremiumMemberShipActivity.this);
                }
            }
        };
    }

    private void initView()
    {
        view_pager = findViewById(R.id.view_pager);


        txt_c_title = findViewById(R.id.txt_c_title);
        txt_c_title.setText("MEMBERSHIP");
        view_pager.setOnPageChangeListener(new ViewPager.OnPageChangeListener()
        {
            @Override
            public void onPageScrolled(int position, float positionOffset, int positionOffsetPixels)
            {
                if (position == 0)
                {
                    img_01.setImageResource(R.drawable.yellow_radius_square);
                    img_02.setImageResource(R.drawable.gray_radius_square);
                }
                else if (position == 1)
                {
                    img_01.setImageResource(R.drawable.gray_radius_square);
                    img_02.setImageResource(R.drawable.yellow_radius_square);
                }
            }

            @Override
            public void onPageSelected(int position)
            {
                if (position == 0)
                {
                    img_01.setImageResource(R.drawable.yellow_radius_square);
                    img_02.setImageResource(R.drawable.gray_radius_square);
                }
                else if (position == 1)
                {
                    img_01.setImageResource(R.drawable.gray_radius_square);
                    img_02.setImageResource(R.drawable.yellow_radius_square);
                }
            }

            @Override
            public void onPageScrollStateChanged(int state)
            {
            }
        });

        img_01 = findViewById(R.id.img_01);
        img_01.setOnClickListener(this);
        img_02 = findViewById(R.id.img_02);
        img_02.setOnClickListener(this);
        findViewById(R.id.img_back).setOnClickListener(this);

    }

    @Override
    public void onClick(View v)
    {
        switch (v.getId())
        {
            case R.id.img_back:
                onBackPressed();
                break;
            case R.id.img_01:
                view_pager.setCurrentItem(0);
                break;
            case R.id.img_02:
                view_pager.setCurrentItem(1);
                break;
        }
    }

    private void callMembershipAsync()
    {
        if (AppDelegate.haveNetworkConnection(this, true))
        {
            ArrayList<PostAysnc_Model> mPostArrayList = new ArrayList<PostAysnc_Model>();
            AppDelegate.getInstance(this).setPostParamsSecond(mPostArrayList, Tags.API_KEY, Tags.API_KEY_VALUE);
            PostAsync mPostAsyncObj = new PostAsync(this, this, ServerRequestConstants.GET_MEMBERSHIP_LIST, mPostArrayList, null);
            mHandler.sendEmptyMessage(10);
            mPostAsyncObj.execute();
        }
    }

    @Override
    protected void onDestroy()
    {
        super.onDestroy();
        UpgradePremiumFragment.str_transaction_id = "";
        UpgradePremiumFragment.mHandler = null;
    }

    @Override
    public void setOnReciveResult(String apiName, String result)
    {
        mHandler.sendEmptyMessage(11);
        if (apiName.equalsIgnoreCase(ServerRequestConstants.GET_MEMBERSHIP_LIST))
        {
            parseMembershipList(result);
        }
    }

    private void parseMembershipList(String result)
    {
        try
        {
            JSONObject jsonObject = new JSONObject(result);
            JSONArray jsonArray = jsonObject.getJSONArray(Tags.response);
            arrayList.clear();
            arrayMembership.clear();
            for (int i = 0; i < jsonArray.length(); i++)
            {
                JSONObject object = jsonArray.getJSONObject(i);
                membershipModel = new MembershipModel();
                membershipModel.amount = JSONParser.getString(object, Tags.amount);
                membershipModel.membership_id = JSONParser.getString(object, Tags.membership_id);
                membershipModel.name = JSONParser.getString(object, Tags.name);
                membershipModel.month_of_membership = JSONParser.getString(object, Tags.month_of_membership);
                membershipModel.description = JSONParser.getString(object, Tags.description);
                membershipModel.status = JSONParser.getString(object, Tags.status);
                membershipModel.created = JSONParser.getString(object, Tags.created);
                arrayMembership.add(membershipModel);
            }
            arrayList.add(UpgradePremiumFragment.getMemberShipFreeFragment(1));
//            arrayList.add(UpgradePremiumFragment.getMemberShipFreeFragment(2));

            mPagerAdapter = new PagerUpgradeMembershipAdapter(getSupportFragmentManager(), arrayList);
            view_pager.setAdapter(mPagerAdapter);

            if (UpgradePremiumFragment.mHandler != null)
            {
                UpgradePremiumFragment.mHandler.sendEmptyMessage(9);
            }
        }
        catch (Exception e)
        {
            AppDelegate.LogE(e);
        }
    }

    /*paypal integration code Start*/
    public void onBuyPressed(String str_amount, String str_item_name)
    {
        this.str_amount = str_amount;

        /*AlertDialog.Builder dialogBuilder = new AlertDialog.Builder(UpgradePremiumMemberShipActivity.this);
        LayoutInflater inflater = this.getLayoutInflater();
        final View dialogView = inflater.inflate(R.layout.membership_error_dialog, null);
        dialogBuilder.setView(dialogView);
        AlertDialog b = dialogBuilder.create();
        b.show();*/
        new GetJsonObjectFromUrl().execute("https://craftedeals.com.au/getBraintreeToken.php");

        /*PayPalPayment thingToBuy = new PayPalPayment(new BigDecimal(str_amount), "AUD", str_item_name, PayPalPayment.PAYMENT_INTENT_SALE);
        Intent intent = new Intent(UpgradePremiumMemberShipActivity.this, PaymentActivity.class);//b
        intent.putExtra(PayPalService.EXTRA_PAYPAL_CONFIGURATION, AppDelegate.config);

        intent.putExtra(PaymentActivity.EXTRA_PAYMENT, thingToBuy);
        startActivityForResult(intent, AppDelegate.REQUEST_CODE_PAYMENT);*/
    }

//    private PayPalPayment getThingToBuy(String paymentIntent) {
//        return new PayPalPayment(new BigDecimal("0.01"), "USD", "sample item",
//                paymentIntent);
//    }

    private class GetJsonObjectFromUrl extends AsyncTask<String, Void, JSONObject>
    {
        @Override
        protected void onPreExecute()
        {
            super.onPreExecute();
            AppDelegate.showProgressDialog(UpgradePremiumMemberShipActivity.this);
        }

        @Override
        protected JSONObject doInBackground(String... params)
        {
            try
            {
                return getJSONObjectFromURL(params[0]);
            }
            catch (IOException | JSONException e)
            {
                e.printStackTrace();
                return null;
            }
        }

        @Override
        protected void onPostExecute(JSONObject jSONObject)
        {
            super.onPostExecute(jSONObject);
            Log.e("response", jSONObject.toString());

            if (jSONObject.optInt("status") == 1)
            {
                brainTreeToken = jSONObject.optString("token");
                DropInRequest dropInRequest = new DropInRequest()
                        .clientToken(brainTreeToken);

                //Toast.makeText(UpgradePremiumMemberShipActivity.this, "Hello", Toast.LENGTH_SHORT).show();

                // TODO: change amount
                dropInRequest.amount(str_amount);
                Log.e("amount",str_amount);
                startActivityForResult(dropInRequest.getIntent(UpgradePremiumMemberShipActivity.this), 333);
            }

            AppDelegate.hideProgressDialog(UpgradePremiumMemberShipActivity.this);
        }
    }

    private class GetJsonObjectFromPostUrl extends AsyncTask<String, Void, String>
    {
        @Override
        protected void onPreExecute()
        {
            super.onPreExecute();
            AppDelegate.showProgressDialog(UpgradePremiumMemberShipActivity.this);
        }

        @Override
        protected String doInBackground(String... params)
        {
            HashMap<String, String> map = new HashMap<>();
            map.put("nonce", params[0]);
            // TODO: change amount
            map.put("amt", str_amount);
            Log.e("amt",str_amount);
            return performPostCall("https://craftedeals.com.au/check_error.php", map);
        }

        @Override
        protected void onPostExecute(String string)
        {
            super.onPostExecute(string);
            Log.e("response", string);
            try
            {
                JSONObject jsonObject = new JSONObject(string);
                if (jsonObject.optInt("status") != 0)
                {
                    UpgradePremiumFragment.str_transaction_id = jsonObject.optString("result");
                    UpgradePremiumFragment.mHandler.sendEmptyMessage(5);
                }
                else
                {
                    Toast.makeText(UpgradePremiumMemberShipActivity.this, jsonObject.optString("result"), Toast.LENGTH_SHORT).show();

                    AlertDialog.Builder builder;
                    builder = new AlertDialog.Builder(UpgradePremiumMemberShipActivity.this);

                    builder.setTitle("Error in transaction..")
                            .setMessage("Please check your card details.")
                            .setPositiveButton("Retry", new DialogInterface.OnClickListener()
                            {
                                public void onClick(DialogInterface dialog, int which)
                                {
                                    dialog.dismiss();
                                    DropInRequest dropInRequest = new DropInRequest()
                                            .clientToken(brainTreeToken);
                                    //Toast.makeText(UpgradePremiumMemberShipActivity.this, "Hello", Toast.LENGTH_SHORT).show();
                                    // TODO: change amount
                                    dropInRequest.amount(str_amount);
                                    Log.e("amount",str_amount);
                                    startActivityForResult(dropInRequest.getIntent(UpgradePremiumMemberShipActivity.this), 333);
                                }
                            })
                            .setIcon(R.mipmap.ic_launcher)
                            .show();
                }
            }
            catch (JSONException e)
            {
                e.printStackTrace();
            }
            AppDelegate.hideProgressDialog(UpgradePremiumMemberShipActivity.this);
        }
    }

    public static JSONObject getJSONObjectFromURL(String urlString) throws IOException, JSONException
    {
        HttpURLConnection urlConnection;
        URL url = new URL(urlString);
        urlConnection = (HttpURLConnection) url.openConnection();
        urlConnection.setRequestMethod("GET");
        urlConnection.setReadTimeout(10000 /* milliseconds */);
        urlConnection.setConnectTimeout(15000 /* milliseconds */);
        urlConnection.setDoOutput(true);
        urlConnection.connect();

        BufferedReader br = new BufferedReader(new InputStreamReader(url.openStream()));
        StringBuilder sb = new StringBuilder();

        String line;
        while ((line = br.readLine()) != null)
        {
            sb.append(line + "\n");
        }
        br.close();

        String jsonString = sb.toString();
        System.out.println("JSON: " + jsonString);

        return new JSONObject(jsonString);
    }

    public String performPostCall(String requestURL, HashMap<String, String> postDataParams)
    {
        URL url;
        String response = "";
        try
        {
            url = new URL(requestURL);

            HttpURLConnection conn = (HttpURLConnection) url.openConnection();
            conn.setReadTimeout(15000);
            conn.setConnectTimeout(15000);
            conn.setRequestMethod("POST");
            conn.setDoInput(true);
            conn.setDoOutput(true);

            OutputStream os = conn.getOutputStream();
            BufferedWriter writer = new BufferedWriter(
                    new OutputStreamWriter(os, "UTF-8"));
            writer.write(getPostDataString(postDataParams));

            writer.flush();
            writer.close();
            os.close();
            int responseCode = conn.getResponseCode();

            if (responseCode == HttpsURLConnection.HTTP_OK)
            {
                String line;
                BufferedReader br = new BufferedReader(new InputStreamReader(conn.getInputStream()));
                while ((line = br.readLine()) != null)
                {
                    response += line;
                }
            }
            else
            {
                response = "";

            }
        }
        catch (Exception e)
        {
            e.printStackTrace();
        }

        return response;
    }

    private String getPostDataString(HashMap<String, String> params) throws UnsupportedEncodingException
    {
        StringBuilder result = new StringBuilder();
        boolean first = true;
        for (Map.Entry<String, String> entry : params.entrySet())
        {
            if (first)
            {
                first = false;
            }
            else
            {
                result.append("&");
            }

            result.append(URLEncoder.encode(entry.getKey(), "UTF-8"));
            result.append("=");
            result.append(URLEncoder.encode(entry.getValue(), "UTF-8"));
        }

        return result.toString();
    }

    private void startPayPalService()
    {
        Intent intent = new Intent(this, PayPalService.class);
        intent.putExtra(PayPalService.EXTRA_PAYPAL_CONFIGURATION, AppDelegate.config);
        startService(intent);
    }

    public String TAG = "paypal";

    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data)
    {
        if (requestCode == 333)
        {
            if (resultCode == Activity.RESULT_OK)
            {
                DropInResult result = data.getParcelableExtra(DropInResult.EXTRA_DROP_IN_RESULT);
                // use the result to update your UI and send the payment method nonce to your server
                Log.e("result", result.getPaymentMethodNonce().getNonce());


                new GetJsonObjectFromPostUrl().execute(result.getPaymentMethodNonce().getNonce());
            }
            else if (resultCode == Activity.RESULT_CANCELED)
            {
                Log.e("result", "Cancelled");
            }
            else
            {
                // handle errors here, an exception may be available in
                Exception error = (Exception) data.getSerializableExtra(DropInActivity.EXTRA_ERROR);
                Log.e("result", error.getMessage());
            }
        }

        else if (requestCode == AppDelegate.REQUEST_CODE_PAYMENT)
        {
            if (resultCode == Activity.RESULT_OK)
            {
                PaymentConfirmation confirm =
                        data.getParcelableExtra(PaymentActivity.EXTRA_RESULT_CONFIRMATION);
                if (confirm != null)
                {
                    try
                    {
                        Log.i(TAG, confirm.toJSONObject().toString(4));
                        Log.i(TAG, confirm.getPayment().toJSONObject().toString(4));
                        /**
                         *  TODO: send 'confirm' (and possibly confirm.getPayment() to your server for verification
                         * or consent completion.
                         * See https://developer.paypal.com/webapps/developer/docs/integration/mobile/verify-mobile-payment/
                         * for more details.
                         *
                         * For sample mobile backend interactions, see
                         * https://github.com/paypal/rest-api-sdk-python/tree/master/samples/mobile_backend
                         */
                        JSONObject jsonObject = new JSONObject(confirm.toJSONObject().toString(4));
                        AppDelegate.trackUpgradeAccount(new Prefs(this).getUserdata().name, new Prefs(this).getUserdata().id, "UPGRADE MEMBERSHIP ACTIVITY");
                        UpgradePremiumFragment.str_transaction_id = jsonObject.getJSONObject("response").getString("id");
                        if (UpgradePremiumFragment.mHandler != null)
                        {
                            UpgradePremiumFragment.mHandler.sendEmptyMessage(5);
                        }

                        AppDelegate.showToast(UpgradePremiumMemberShipActivity.this, "PaymentConfirmation info received from PayPal, T ID is = " + jsonObject.getJSONObject("response").getString("id"));
                    }
                    catch (JSONException e)
                    {
                        AppDelegate.LogE(e);
                        Log.e(TAG, "an extremely unlikely failure occurred: ", e);
                    }
                }
            }
            else if (resultCode == Activity.RESULT_CANCELED)
            {
                Log.i(TAG, "The user canceled.");
            }
            else if (resultCode == PaymentActivity.RESULT_EXTRAS_INVALID)
            {
                Log.i(
                        TAG,
                        "An invalid Payment or PayPalConfiguration was submitted. Please see the docs.");
            }
        }
        else if (requestCode == AppDelegate.REQUEST_CODE_FUTURE_PAYMENT)
        {
            if (resultCode == Activity.RESULT_OK)
            {
                PayPalAuthorization auth =
                        data.getParcelableExtra(PayPalFuturePaymentActivity.EXTRA_RESULT_AUTHORIZATION);
                if (auth != null)
                {
                    try
                    {
                        Log.i("FuturePaymentExample", auth.toJSONObject().toString(4));

                        String authorization_code = auth.getAuthorizationCode();
                        Log.i("FuturePaymentExample", authorization_code);

                        sendAuthorizationToServer(auth);
                        AppDelegate.showToast(UpgradePremiumMemberShipActivity.this, getResources().getString(R.string.future_payment_code));

                    }
                    catch (JSONException e)
                    {
                        Log.e("FuturePaymentExample", "an extremely unlikely failure occurred: ", e);
                    }
                }
            }
            else if (resultCode == Activity.RESULT_CANCELED)
            {
                Log.i("FuturePaymentExample", "The user canceled.");
            }
            else if (resultCode == PayPalFuturePaymentActivity.RESULT_EXTRAS_INVALID)
            {
                Log.i(
                        "FuturePaymentExample",
                        "Probably the attempt to previously start the PayPalService had an invalid PayPalConfiguration. Please see the docs.");
            }
        }
        else if (requestCode == AppDelegate.REQUEST_CODE_PROFILE_SHARING)
        {
            if (resultCode == Activity.RESULT_OK)
            {
                PayPalAuthorization auth =
                        data.getParcelableExtra(PayPalProfileSharingActivity.EXTRA_RESULT_AUTHORIZATION);
                if (auth != null)
                {
                    try
                    {
                        Log.i("ProfileSharingExample", auth.toJSONObject().toString(4));

                        String authorization_code = auth.getAuthorizationCode();
                        Log.i("ProfileSharingExample", authorization_code);

                        sendAuthorizationToServer(auth);
                        AppDelegate.showToast(UpgradePremiumMemberShipActivity.this, getResources().getString(R.string.profile_sharing_code));

                    }
                    catch (JSONException e)
                    {
                        Log.e("ProfileSharingExample", "an extremely unlikely failure occurred: ", e);
                    }
                }
            }
            else if (resultCode == Activity.RESULT_CANCELED)
            {
                Log.i("ProfileSharingExample", "The user canceled.");
            }
            else if (resultCode == PayPalFuturePaymentActivity.RESULT_EXTRAS_INVALID)
            {
                Log.i(
                        "ProfileSharingExample",
                        "Probably the attempt to previously start the PayPalService had an invalid PayPalConfiguration. Please see the docs.");
            }
        }
    }

    private void sendAuthorizationToServer(PayPalAuthorization authorization)
    {
        /**
         * TODO: Send the authorization response to your server, where it can
         * exchange the authorization code for OAuth access and refresh tokens.
         *
         * Your server must then store these tokens, so that your server code
         * can execute payments for this user in the future.
         *
         * A more complete example that includes the required app-server to
         * PayPal-server integration is available from
         * https://github.com/paypal/rest-api-sdk-python/tree/master/samples/mobile_backend
         */

    }
}
