package com.craftedeals.activities;

import android.app.Activity;
import android.graphics.drawable.Drawable;
import android.os.Build;
import android.os.Bundle;
import android.os.Handler;
import android.os.Message;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.DefaultItemAnimator;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.View;
import android.view.Window;
import android.view.WindowManager;

import com.craftedeals.AppDelegate;
import com.craftedeals.Async.PostAsync;
import com.craftedeals.Models.NotificationModel;
import com.craftedeals.Models.PostAysnc_Model;
import com.craftedeals.R;
import com.craftedeals.Utils.Prefs;
import com.craftedeals.adapters.NotificationAdapter;
import com.craftedeals.constants.ServerRequestConstants;
import com.craftedeals.constants.Tags;
import com.craftedeals.interfaces.OnReciveServerResponse;
import com.craftedeals.parser.JSONParser;
import com.nostra13.universalimageloader.core.DisplayImageOptions;
import com.nostra13.universalimageloader.core.ImageLoaderConfiguration;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;


/**
 * Created by Heena on 05-Dec-16.
 */

public class NotificationActivity extends AppCompatActivity implements View.OnClickListener, OnReciveServerResponse {

    public com.nostra13.universalimageloader.core.ImageLoader imageLoader = com.nostra13.universalimageloader.core.ImageLoader.getInstance();
    public DisplayImageOptions options = new DisplayImageOptions.Builder().cacheInMemory(true)
            .cacheOnDisc(true).resetViewBeforeLoading(true).
                    build();
    public static Activity mActivity;
    private Prefs prefs;
    public static Handler mHandler;
    private RecyclerView recycler_notification_list;
    int i = 0;
    ArrayList<NotificationModel> notificationModelArrayList = new ArrayList<>();
    private NotificationAdapter mAdapter;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.LOLLIPOP) {
            Window window = this.getWindow();
            Drawable background = this.getResources().getDrawable(R.drawable.topbg);
            window.addFlags(WindowManager.LayoutParams.FLAG_DRAWS_SYSTEM_BAR_BACKGROUNDS);
            window.setStatusBarColor(getResources().getColor(android.R.color.transparent));
            window.setNavigationBarColor(getResources().getColor(android.R.color.transparent));
            window.setBackgroundDrawable(background);
        }
        setContentView(R.layout.notification);
        imageLoader.init(ImageLoaderConfiguration.createDefault(this));
        mActivity = this;
        prefs = new Prefs(this);
        setHandler();
        initView();
//      mHandler.sendEmptyMessage(5);
    }

    @Override
    protected void onResume() {
        super.onResume();
        mHandler.sendEmptyMessage(5);
    }

    private void show_notification(ArrayList<NotificationModel> notificationModelArrayList) {
        mAdapter = new NotificationAdapter(this, notificationModelArrayList);
        RecyclerView.LayoutManager mLayoutManager = new LinearLayoutManager(this);
        recycler_notification_list.setLayoutManager(mLayoutManager);
        recycler_notification_list.setItemAnimator(new DefaultItemAnimator());
        recycler_notification_list.setAdapter(mAdapter);
    }

    private void initView() {
        recycler_notification_list = (RecyclerView) findViewById(R.id.recycler_notification_list);
        findViewById(R.id.img_c_back).setOnClickListener(this);
    }

    private void setHandler() {
        mHandler = new Handler() {
            @Override
            public void dispatchMessage(Message msg) {
                super.dispatchMessage(msg);
                switch (msg.what) {
                    case 10:
                        AppDelegate.showProgressDialog(NotificationActivity.this);
                        break;
                    case 11:
                        AppDelegate.hideProgressDialog(NotificationActivity.this);
                        break;
                    case 1:
                        break;
                    case 2:
                        break;
                    case 3:

                        break;
                    case 5:
                        execute_getNotificationApi();
//                        notificationModelArrayList.clear();
//                        getNotification();
//                        if (notificationModelArrayList != null && notificationModelArrayList.size() > 0)
//                            show_notification(notificationModelArrayList);
//                        else {
//                            notificationModelArrayList = new ArrayList<>();
//                            show_notification(notificationModelArrayList);
//                            AppDelegate.showToast(NotificationActivity.this, getString(R.string.notification_not_available));
//                        }
                        break;
                    case 6:
                        break;
                    case 7:
                    case 8:
                        break;
                }
            }
        };
    }

//    private void getNotification() {
//        try {
//            RealmResults<RealmNotificationModel> result = RealmController.with(getApplication()).getNotifications();
//            RealmNotificationModel data = new RealmNotificationModel();
//            AppDelegate.LogT(" notif data size==" + result.size());
//            for (int i = 0; i < result.size(); i++) {
//                AppDelegate.LogT(" notif data data==" + data);
//                data = result.get(i);
//                setdata(data);
//            }
//        } catch (Exception e) {
//            AppDelegate.LogE(e);
//        }
//    }

    //    private void setdata(RealmNotificationModel data) {
//        NotificationModel notificationModel = new NotificationModel();
//        notificationModel.setId(data.getId());
//        notificationModel.setDeal_title(data.getDeal_title());
//        notificationModel.setDeal_id(data.getDeal_id());
//        notificationModel.setVendor_id(data.getVendor_id());
//        notificationModel.setVendor_name(data.getVendor_name());
//        notificationModel.setMessage(data.getMessage());
//        notificationModel.setDeal_image(data.getDeal_image());
//        notificationModel.setDeal_type(data.getDeal_type());
//        notificationModel.setVendor_image(data.getVendor_image());
//        notificationModel.setBusiness_name(data.getBusiness_name());
//        notificationModel.setNotification_type(data.getNotification_type());
//        notificationModel.setPurchase_id((data.getPurchase_id()));
//        notificationModel.setMembership_id(data.getMembership_id());
//        notificationModel.setMembership_name(data.getMembership_name());
//        notificationModel.setMembership_amount(data.getMembership_amount());
//        notificationModel.setMonth_of_membership(data.getMonth_of_membership());
//        notificationModel.setMembership_description(data.getMembership_description());
//        notificationModel.setMembership_status(data.getMembership_status());
//        notificationModel.setUser_id(data.getUser_id());
//        notificationModel.setPayment_id(data.getPayment_id());
//        AppDelegate.LogT(" notif data==" + notificationModel);
//        notificationModelArrayList.add(notificationModel);
//        AppDelegate.LogT("notificationModelArrayList.size");
//    }
//
    @Override
    public void onClick(View v) {
        switch (v.getId()) {
            case R.id.img_c_back:
                onBackPressed();
                break;
        }
    }

    private void execute_getNotificationApi() {
        try {
            if (AppDelegate.haveNetworkConnection(this, true)) {
                ArrayList<PostAysnc_Model> mPostArrayList = new ArrayList<PostAysnc_Model>();
                PostAsync mPostAsyncObj;
                AppDelegate.getInstance(this).setPostParamsSecond(mPostArrayList, Tags.API_KEY, Tags.API_KEY_VALUE);
                AppDelegate.getInstance(this).setPostParamsSecond(mPostArrayList, Tags.user_id, new Prefs(this).getUserdata().id);
                mPostAsyncObj = new PostAsync(this, this, ServerRequestConstants.GET_NOTIFICATION,
                        mPostArrayList, null);
                mHandler.sendEmptyMessage(10);
                mPostAsyncObj.execute();
            }
        } catch (Exception e) {
            AppDelegate.LogE(e);
            AppDelegate.showToast(this, getString(R.string.try_again));
        }
    }

    @Override
    public void setOnReciveResult(String apiName, String result) {
        if (mHandler == null)
            return;
        mHandler.sendEmptyMessage(11);
        if (!AppDelegate.isValidString(result)) {
            AppDelegate.showToast(this, getString(R.string.time_out));
            return;
        }
        if (apiName.equalsIgnoreCase(ServerRequestConstants.GET_NOTIFICATION)) {
            recycler_notification_list.setAdapter(null);
            parsegetNotification(result);
        }
    }

    private void parsegetNotification(String result) {
        try {
            notificationModelArrayList.clear();

            JSONObject jsonObject = new JSONObject(result);
            if (jsonObject.getInt(Tags.status) == 1) {
                // AppDelegate.showToast(this, jsonObject.getString(Tags.message));
                if (jsonObject.has(Tags.response)) {
                    JSONArray jsonArray = jsonObject.getJSONArray(Tags.response);

                    for (int i = 0; i < jsonArray.length(); i++) {
                        JSONObject object1 = jsonArray.getJSONObject(i);
                        NotificationModel notificationModel = new NotificationModel();
                        notificationModel.setId(JSONParser.getInt(object1, Tags.id));
                        notificationModel.setUser_id(JSONParser.getInt(object1, Tags.user_id));
                        notificationModel.created = JSONParser.getString(object1, Tags.created);
                        JSONObject object = object1.getJSONObject(Tags.massage);

                        notificationModel.setDeal_title(JSONParser.getString(object, Tags.deal_title));
                        notificationModel.setDeal_id(JSONParser.getString(object, Tags.deal_id));
                        notificationModel.setVendor_id(JSONParser.getString(object, Tags.vendor_id));
                        notificationModel.setVendor_name(JSONParser.getString(object, Tags.vendor_name));
                        notificationModel.setMessage(JSONParser.getString(object, Tags.message));
                        notificationModel.setDeal_image(JSONParser.getString(object, Tags.deal_image));
                        notificationModel.setDeal_type(JSONParser.getString(object, Tags.deal_type));
                        notificationModel.setVendor_image(JSONParser.getString(object, Tags.vendor_image));
                        notificationModel.setBusiness_name(JSONParser.getString(object, Tags.business_name));
                        notificationModel.setNotification_type(JSONParser.getInt(object, Tags.notification_type));
                        notificationModel.setPurchase_id(JSONParser.getInt(object, Tags.purchase_id));
                        notificationModel.setMembership_id(JSONParser.getString(object, Tags.membership_id));
                        notificationModel.setMembership_name(JSONParser.getString(object, Tags.membership_name));
                        notificationModel.setMembership_amount(JSONParser.getString(object, Tags.membership_amount));
                        notificationModel.setMonth_of_membership(JSONParser.getString(object, Tags.month_of_membership));
                        notificationModel.setMembership_description(JSONParser.getString(object, Tags.membership_discription));
                        notificationModel.setMembership_status(JSONParser.getString(object, Tags.membership_status));

                        notificationModel.setPayment_id(JSONParser.getInt(object, Tags.payment_id));
                        AppDelegate.LogT(" notif data==" + notificationModel);
                        notificationModelArrayList.add(notificationModel);
                        AppDelegate.LogT("notificationModelArrayList.size");
                    }
                    show_notification(notificationModelArrayList);
                } else {
                    if (jsonObject.has(Tags.message))
                        AppDelegate.showToast(this, jsonObject.getString(Tags.message));
                }
            } else {
                AppDelegate.showToast(this, jsonObject.getString(Tags.message));
            }
        } catch (JSONException e) {
            e.printStackTrace();
        }
    }

    @Override
    protected void onDestroy() {
        super.onDestroy();
        mActivity = null;
        mHandler = null;
    }
}
