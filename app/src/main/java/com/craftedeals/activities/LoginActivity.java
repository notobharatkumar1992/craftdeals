package com.craftedeals.activities;

import android.Manifest;
import android.app.Activity;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.graphics.Typeface;
import android.graphics.drawable.Drawable;
import android.os.Build;
import android.os.Bundle;
import android.os.Handler;
import android.os.Message;
import android.support.annotation.NonNull;
import android.support.v4.app.ActivityCompat;
import android.support.v4.app.ActivityOptionsCompat;
import android.support.v4.content.ContextCompat;
import android.support.v4.util.Pair;
import android.support.v7.app.AppCompatActivity;
import android.util.Log;
import android.view.View;
import android.view.Window;
import android.view.WindowManager;
import android.widget.EditText;

import com.craftedeals.AppDelegate;
import com.craftedeals.Async.PostAsync;
import com.craftedeals.Models.CityModel;
import com.craftedeals.Models.FbDetails;
import com.craftedeals.Models.Fb_detail_GetSet;
import com.craftedeals.Models.PostAysnc_Model;
import com.craftedeals.Models.UserDataModel;
import com.craftedeals.R;
import com.craftedeals.Utils.Prefs;
import com.craftedeals.Utils.TransitionHelper;
import com.craftedeals.constants.ServerRequestConstants;
import com.craftedeals.constants.Tags;
import com.craftedeals.interfaces.OnReciveServerResponse;
import com.craftedeals.parser.JSONParser;
import com.facebook.AccessToken;
import com.facebook.CallbackManager;
import com.facebook.FacebookAuthorizationException;
import com.facebook.FacebookCallback;
import com.facebook.FacebookException;
import com.facebook.FacebookSdk;
import com.facebook.GraphRequest;
import com.facebook.GraphResponse;
import com.facebook.HttpMethod;
import com.facebook.appevents.AppEventsLogger;
import com.facebook.login.LoginManager;
import com.facebook.login.LoginResult;
import com.nostra13.universalimageloader.core.DisplayImageOptions;
import com.nostra13.universalimageloader.core.ImageLoaderConfiguration;

import org.json.JSONObject;

import java.io.File;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.net.HttpURLConnection;
import java.net.MalformedURLException;
import java.net.URL;
import java.util.ArrayList;
import java.util.Arrays;

import carbon.widget.ImageView;

public class LoginActivity extends AppCompatActivity implements View.OnClickListener, OnReciveServerResponse
{

    public static Activity mActivity;
    private EditText et_username, et_password;
    private ImageView img_c_remember_me;
    public Handler mHandler;
    private Prefs prefs;
    public CallbackManager callbackManager;
    private Fb_detail_GetSet fbUserData;
    private String fb_LoginToken;
    private boolean isCalledOnce = false;
    private Bundle bundle;
    int login_from = 0;
    final int MY_PERMISSIONS_REQUEST_LOCATION = 1578;

    @Override
    protected void onCreate(Bundle savedInstanceState)
    {
        super.onCreate(savedInstanceState);
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.LOLLIPOP)
        {
            Window window = this.getWindow();
            Drawable background = this.getResources().getDrawable(R.drawable.spalsh);
            window.addFlags(WindowManager.LayoutParams.FLAG_DRAWS_SYSTEM_BAR_BACKGROUNDS);
            window.setStatusBarColor(getResources().getColor(android.R.color.transparent));
            window.setNavigationBarColor(getResources().getColor(android.R.color.transparent));
            window.setBackgroundDrawable(background);
        }
        imageLoader.init(ImageLoaderConfiguration.createDefault(this));
        setContentView(R.layout.login);
        mActivity = this;
        prefs = new Prefs(this);
        initView();
        setHandler();
        img_c_remember_me.setSelected(true);
        mHandler.sendEmptyMessage(1);

        locationPermission();
    }

    //
    private void initView()
    {
        et_username = (EditText) findViewById(R.id.et_username);
        et_username.setTypeface(Typeface.createFromAsset(getAssets(), getString(R.string.font_regular)));
        et_password = (EditText) findViewById(R.id.et_password);
        et_password.setTypeface(Typeface.createFromAsset(getAssets(), getString(R.string.font_regular)));
        img_c_remember_me = (ImageView) findViewById(R.id.img_c_remember_me);
        img_c_remember_me.setOnClickListener(this);
        findViewById(R.id.txt_c_forgot_password).setOnClickListener(this);
        findViewById(R.id.txt_c_login).setOnClickListener(this);
        findViewById(R.id.txt_c_login_with_facebook).setOnClickListener(this);
        findViewById(R.id.txt_c_create_account).setOnClickListener(this);
        if (getIntent() != null && AppDelegate.isValidString(getIntent().getIntExtra(Tags.login_from, 0) + ""))
        {
            login_from = getIntent().getIntExtra(Tags.login_from, 0);
        }
    }

    private void setHandler()
    {
        mHandler = new Handler()
        {
            @Override
            public void dispatchMessage(Message msg)
            {
                super.dispatchMessage(msg);
                switch (msg.what)
                {
                    case 10:
                        AppDelegate.showProgressDialog(LoginActivity.this);
                        break;
                    case 11:
                        AppDelegate.hideProgressDialog(LoginActivity.this);
                        break;
                    case 1:
                        String emailid = new Prefs(LoginActivity.this).getStringValuefromTemp(Tags.EMAIL, "");
                        if (AppDelegate.isValidString(emailid))
                        {
                            AppDelegate.Log("emailid", emailid + "" + "hellooo");
                            img_c_remember_me.setSelected(true);
                            et_username.setText("" + new Prefs(LoginActivity.this).getStringValuefromTemp(Tags.EMAIL, ""));
                            et_password.setText("" + new Prefs(LoginActivity.this).getStringValuefromTemp(Tags.PASSWORD, ""));
                        }
                        else
                        {
                            AppDelegate.Log("emailid", emailid + "" + "byee");
                            img_c_remember_me.setSelected(true);
                            et_username.setText("");
                            et_password.setText("");
                        }
                        break;
                    case 2:
                        if (img_c_remember_me.isSelected())
                        {
                            et_username.setText("");
                            et_password.setText("");
                            new Prefs(LoginActivity.this).putStringValueinTemp(Tags.EMAIL, "");
                            new Prefs(LoginActivity.this).putStringValueinTemp(Tags.PASSWORD, "");
                            img_c_remember_me.setSelected(false);
                        }
                        else
                        {
                            new Prefs(LoginActivity.this).putStringValueinTemp(Tags.EMAIL, et_username.getText().toString());
                            new Prefs(LoginActivity.this).putStringValueinTemp(Tags.PASSWORD, et_password.getText().toString());
                            img_c_remember_me.setSelected(true);
                        }
                        break;
                    case 3:
                        executeLogin();
                        break;
                }
            }
        };
    }

    @Override
    protected void onDestroy()
    {
        super.onDestroy();
        if (FacebookSdk.isInitialized())
        {
            AppDelegate.LogT("INITIALIZED facebook sdk");
            disconnectFromFacebook();
        }
        mActivity = null;
    }

    @Override
    public void setOnReciveResult(String apiName, String result)
    {
        mHandler.sendEmptyMessage(11);
        if (!AppDelegate.isValidString(result))
        {
            AppDelegate.showToast(this, getResources().getString(R.string.time_out));
            return;
        }
        else if (apiName.equalsIgnoreCase(ServerRequestConstants.LOGIN))
        {
            parseLoginResponse(result);
        }
        else if (apiName.equalsIgnoreCase(ServerRequestConstants.CHK_FACEBOOK))
        {
            parseCheckFbResponse(result);
        }
    }

    com.nostra13.universalimageloader.core.ImageLoader imageLoader = com.nostra13.universalimageloader.core.ImageLoader.getInstance();
    DisplayImageOptions options = new DisplayImageOptions.Builder().cacheInMemory(true)
            .cacheOnDisc(true).resetViewBeforeLoading(true).
                    build();

    private void parseCheckFbResponse(String result)
    {
        try
        {
            JSONObject jsonObject = new JSONObject(result);
            if (jsonObject.getString(Tags.status).equalsIgnoreCase("2"))
            {
                if (AppDelegate.isValidString(JSONParser.getString(jsonObject, Tags.message)))
                {
                    AppDelegate.showToast(LoginActivity.this, jsonObject.getString(Tags.message));
                }
            }
            else if (jsonObject.getString(Tags.status).equalsIgnoreCase("1"))
            {
                if (AppDelegate.isValidString(JSONParser.getString(jsonObject, Tags.message)))
                {
                    AppDelegate.showToast(LoginActivity.this, jsonObject.getString(Tags.message));
                }
                JSONObject object = jsonObject.getJSONObject(Tags.DATA);
                UserDataModel userDataModel = new UserDataModel();
                userDataModel.id = JSONParser.getInt(object, Tags.id);
                userDataModel.name = JSONParser.getString(object, Tags.name);
                userDataModel.first_names = JSONParser.getString(object, Tags.name);
                userDataModel.last_names = JSONParser.getString(object, Tags.last_name);
                userDataModel.email = JSONParser.getString(object, Tags.email);
                userDataModel.contact_number = JSONParser.getString(object, Tags.contact_number);
                userDataModel.membership_id = JSONParser.getInt(object, Tags.membership_id);
                userDataModel.image = JSONParser.getString(object, Tags.image);
                userDataModel.device_id = JSONParser.getString(object, Tags.device_id);
                userDataModel.device_type = JSONParser.getInt(object, Tags.device_type);
                userDataModel.created = JSONParser.getString(object, Tags.created);
                userDataModel.address = JSONParser.getString(object, Tags.address);
                userDataModel.social_id = JSONParser.getString(object, Tags.social_id);
                userDataModel.fav_brewery = JSONParser.getString(object, Tags.brewery_type_id);
                if (object.has(Tags.user_range))
                {
                    userDataModel.user_range = JSONParser.getInt(object, Tags.user_range);
                }
                if (object.has(Tags.membership_start_date) && object.has(Tags.membership_end_date))
                {
                    userDataModel.membership_start_date = JSONParser.getString(object, Tags.membership_start_date);
                    userDataModel.membership_end_date = JSONParser.getString(object, Tags.membership_end_date);
                }
                if (AppDelegate.isValidString(object.getString(Tags.state)))
                {
                    JSONObject state = object.getJSONObject(Tags.state);
                    userDataModel.province = JSONParser.getString(state, Tags.region_name);
                    userDataModel.state_id = JSONParser.getInt(state, Tags.id) + "";
                }
                if (AppDelegate.isValidString(object.getString(Tags.city)))
                {
                    JSONObject city = object.getJSONObject(Tags.city);
                    userDataModel.city = JSONParser.getString(city, Tags.name);
                    userDataModel.city_id = JSONParser.getInt(city, Tags.id) + "";
                    CityModel cityModel = new CityModel();
                    cityModel.lat = JSONParser.getString(city, Tags.latitude);
                    cityModel.lng = JSONParser.getString(city, Tags.longitude);
                    cityModel.location_type = Tags.other;
                    cityModel.id = JSONParser.getInt(city, Tags.id) + "";
                    cityModel.name = JSONParser.getString(city, Tags.name);
//                    prefs.setCityData(cityModel);
                }
//                if (AppDelegate.isValidString(object.getString(Tags.beer_type))) {
//                    JSONObject beer = object.getJSONObject(Tags.beer_type);
//                    userDataModel.fav_beer = JSONParser.getString(beer, Tags.name);
//                    userDataModel.beer_id = JSONParser.getInt(beer, Tags.id);
//                }
//                if (AppDelegate.isValidString(object.getString(Tags.brewery_type))) {
//                    JSONObject brewery = object.getJSONObject(Tags.brewery_type);
//                    userDataModel.fav_brewery = JSONParser.getString(brewery, Tags.name);
//                    userDataModel.brewery_id = JSONParser.getInt(brewery, Tags.id);
//                }
                prefs.setUserData(userDataModel);
                if (login_from == AppDelegate.LOGIN_FROM_VENDOR_PROFILE)
                {
                    finish();
                }
                else
                {
                    if (DashBoardActivity.mainActivity != null)
                    {
                        DashBoardActivity.mainActivity.finish();
                    }
                    startActivity(new Intent(LoginActivity.this, DashBoardActivity.class));
                    finish();
                }

            }
            else
            {
                if (AppDelegate.haveNetworkConnection(LoginActivity.this, true))
                {
                    AppDelegate.LogT("FaceBook data ==>" + fbUserData.profile_pic);
//                  Intent intent = new Intent(LoginActivity.this, MemberShipActivity.class);
                    Intent intent = new Intent(LoginActivity.this, FacebookGetDataActivity.class);
                    Bundle bundle = new Bundle();
                    bundle.putInt(Tags.TYPE, AppDelegate.DATA_TYPE_FB);
                    bundle.putInt(Tags.login_from, login_from);
                    bundle.putParcelable(Tags.user, fbUserData);
                    intent.putExtras(bundle);
                    startActivity(intent);
                }
            }
        }
        catch (Exception e)
        {
            AppDelegate.LogE(e);
        }
    }

    public String downloadImage(String urlString)
    {
        File file = new File(SignUpActivity.getNewFile(LoginActivity.this));
        String filepath = null;
        try
        {
            URL url = new URL(urlString + "");
            HttpURLConnection urlConnection = (HttpURLConnection) url.openConnection();
            urlConnection.setRequestMethod("GET");
            urlConnection.setDoOutput(true);
            urlConnection.connect();
            FileOutputStream fileOutput = new FileOutputStream(file);
            InputStream inputStream = urlConnection.getInputStream();
            int totalSize = urlConnection.getContentLength();
            int downloadedSize = 0;
            byte[] buffer = new byte[1024];
            int bufferLength = 0;
            while ((bufferLength = inputStream.read(buffer)) > 0)
            {
                fileOutput.write(buffer, 0, bufferLength);
                downloadedSize += bufferLength;
                Log.i("Progress:", "downloadedSize:" + downloadedSize + "totalSize:" + totalSize);
            }
            fileOutput.close();
            if (downloadedSize == totalSize)
            {
                filepath = file.getPath();
            }
        }
        catch (MalformedURLException e)
        {
            AppDelegate.LogE(e);
        }
        catch (IOException e)
        {
            filepath = null;
            AppDelegate.LogE(e);
        }
        Log.i("filepath:", " " + filepath);
        return filepath;
    }

    private void parseLoginResponse(String result)
    {
        try
        {
            JSONObject jsonObject = new JSONObject(result);
            if (jsonObject.getString(Tags.status).equalsIgnoreCase("1"))
            {
                AppDelegate.showToast(LoginActivity.this, jsonObject.getString(Tags.message));
                JSONObject object = jsonObject.getJSONObject(Tags.response);
                UserDataModel userDataModel = new UserDataModel();
                userDataModel.id = JSONParser.getInt(object, Tags.id);
                userDataModel.name = JSONParser.getString(object, Tags.name);
                userDataModel.first_names = JSONParser.getString(object, Tags.name);
                userDataModel.last_names = JSONParser.getString(object, Tags.last_name);
                userDataModel.email = JSONParser.getString(object, Tags.email);
                userDataModel.contact_number = JSONParser.getString(object, Tags.contact_number);
                userDataModel.membership_id = JSONParser.getInt(object, Tags.membership_id);
                userDataModel.image = JSONParser.getString(object, Tags.image);
                userDataModel.device_id = JSONParser.getString(object, Tags.device_id);
                userDataModel.device_type = JSONParser.getInt(object, Tags.device_type);
                userDataModel.created = JSONParser.getString(object, Tags.created);
                userDataModel.social_id = JSONParser.getString(object, Tags.social_id);
                userDataModel.fav_brewery = JSONParser.getString(object, Tags.brewery_type_id);
                if (object.has(Tags.user_range))
                {
                    userDataModel.user_range = JSONParser.getInt(object, Tags.user_range);
                }
                if (object.has(Tags.membership_start_date) && object.has(Tags.membership_end_date))
                {
                    userDataModel.membership_start_date = JSONParser.getString(object, Tags.membership_start_date);
                    userDataModel.membership_end_date = JSONParser.getString(object, Tags.membership_end_date);
                }
                userDataModel.address = JSONParser.getString(object, Tags.address);
                userDataModel.state_id = JSONParser.getString(object, Tags.state_id);
                userDataModel.city_id = JSONParser.getString(object, Tags.city_id);
                if ((object.has(Tags.state)) && AppDelegate.isValidString(object.getString(Tags.state)))
                {
                    JSONObject state = object.getJSONObject(Tags.state);
                    userDataModel.province = JSONParser.getString(state, Tags.region_name);
                    userDataModel.state_id = JSONParser.getInt(state, Tags.id) + "";
                }
                if (object.has(Tags.city) && AppDelegate.isValidString(object.getString(Tags.city)))
                {
                    JSONObject city = object.getJSONObject(Tags.city);
                    userDataModel.city = JSONParser.getString(city, Tags.name);
                    userDataModel.city_id = JSONParser.getInt(city, Tags.id) + "";
                    if (prefs.getCitydata() != null)
                    {
                    }
                    else
                    {
                        CityModel cityModel = new CityModel();
                        cityModel.lat = JSONParser.getString(city, Tags.latitude);
                        cityModel.lng = JSONParser.getString(city, Tags.longitude);
                        cityModel.location_type = Tags.other;
                        cityModel.id = JSONParser.getInt(city, Tags.id) + "";
                        cityModel.name = JSONParser.getString(city, Tags.name);
//                        prefs.setCityData(cityModel);
                    }
                }
//                if (object.has(Tags.beer_type) && AppDelegate.isValidString(object.getString(Tags.beer_type))) {
//                    JSONObject beer = object.getJSONObject(Tags.beer_type);
//                    userDataModel.fav_beer = JSONParser.getString(beer, Tags.name);
//                    userDataModel.beer_id = JSONParser.getInt(beer, Tags.id);
//                }
//                if (object.has(Tags.brewery_type) && AppDelegate.isValidString(object.getString(Tags.brewery_type))) {
//                    JSONObject brewery = object.getJSONObject(Tags.brewery_type);
//                    userDataModel.fav_brewery = JSONParser.getString(brewery, Tags.name);
//                    userDataModel.brewery_id = JSONParser.getInt(brewery, Tags.id);
//                }
                prefs.setUserData(userDataModel);
                if (login_from == AppDelegate.LOGIN_FROM_VENDOR_PROFILE)
                {
                    finish();
                }
                else
                {
                    startActivity(new Intent(LoginActivity.this, DashBoardActivity.class));
                    finish();
                }

            }
            else
            {
                AppDelegate.showAlert(LoginActivity.this, jsonObject.getString(Tags.message));
            }
        }
        catch (Exception e)
        {
            AppDelegate.LogE(e);
        }
    }

    public void disconnectFromFacebook()
    {
        if (AccessToken.getCurrentAccessToken() == null)
        {
            return; // already logged out
        }
        new GraphRequest(AccessToken.getCurrentAccessToken(), "/me/permissions/", null, HttpMethod.DELETE, new GraphRequest
                .Callback()
        {
            @Override
            public void onCompleted(GraphResponse graphResponse)
            {
                LoginManager.getInstance().logOut();
                AppDelegate.LogT("Logout from facebook");
            }
        }).executeAsync();
    }

    @Override
    public void onClick(View v)
    {
        switch (v.getId())
        {
            case R.id.txt_c_forgot_password:
            {
//                startActivity(new Intent(LoginActivity.this, ForgotPasswordActivity.class));

                Intent intent = new Intent(LoginActivity.this, ForgotPasswordActivity.class);
                final Pair<View, String>[] pairs = TransitionHelper.createSafeTransitionParticipants(LoginActivity.this, false, new Pair<>(findViewById(R.id.txt_c_forgot_password), "forgot_password"));
                ActivityOptionsCompat transitionActivityOptions = ActivityOptionsCompat.makeSceneTransitionAnimation(LoginActivity.this, pairs);
                startActivity(intent, transitionActivityOptions.toBundle());
                break;
            }
            case R.id.txt_c_login:
                new Prefs(LoginActivity.this).putStringValueinTemp(Tags.social_id, "");
                if (img_c_remember_me.isSelected())
                {
                    new Prefs(this).putStringValueinTemp(Tags.EMAIL, et_username.getText().toString());
                    new Prefs(this).putStringValueinTemp(Tags.PASSWORD, et_password.getText().toString());
                }
                else
                {
                    new Prefs(this).clearSharedPreference();
                    new Prefs(this).putStringValueinTemp(Tags.EMAIL, "");
                    new Prefs(this).putStringValueinTemp(Tags.PASSWORD, "");
                }
                checkValidity();
                break;

            case R.id.txt_c_login_with_facebook:
                if (AppDelegate.haveNetworkConnection(this, false))
                {
//                    img_c_remember_me.setSelected(true);
//
                    new Prefs(this).putStringValueinTemp(Tags.EMAIL, "");
                    new Prefs(this).putStringValueinTemp(Tags.PASSWORD, "");
                    openFacebook();
                }
                else
                {
                    AppDelegate.showToast(this, getResources().getString(R.string.not_connected));
                }
                break;

            case R.id.txt_c_create_account:
            {
                final Pair<View, String>[] pairs = TransitionHelper.createSafeTransitionParticipants(LoginActivity.this, false, new Pair<>(findViewById(R.id.txt_c_create_account), "create_account"));
                ActivityOptionsCompat transitionActivityOptions = ActivityOptionsCompat.makeSceneTransitionAnimation(LoginActivity.this, pairs);
                // startActivity(new Intent(LoginActivity.this, SignUpActivity.class), transitionActivityOptions.toBundle());
                Intent intent = new Intent(this, SignUpActivity.class);
                intent.putExtra(Tags.login_from, login_from);
                startActivity(intent, transitionActivityOptions.toBundle());
                break;
            }
            case R.id.img_c_remember_me:
                mHandler.sendEmptyMessage(2);
                break;

            case R.id.et_username:
                AppDelegate.LogT("et_username clicked");
                break;
        }
    }


    private void checkValidity()
    {
        if (!AppDelegate.CheckEmail(et_username.getText().toString().trim()))
        {
            AppDelegate.showToast(this, getResources().getString(R.string.foremail));
        }
        else if (!AppDelegate.isValidString(et_password.getText().toString()))
        {
            AppDelegate.showToast(this, getResources().getString(R.string.forpass));
        }
        else if (!AppDelegate.isLegalPassword(et_password.getText().toString()))
        {
            AppDelegate.showToast(this, getResources().getString(R.string.forlength));
        }/* else if (!AppDelegate.password_validation(this, password.getText().toString())) {
        }*/
        else if (AppDelegate.haveNetworkConnection(this, false))
        {
            if (img_c_remember_me.isSelected())
            {
                AppDelegate.Log("toggle _if", img_c_remember_me.isSelected() + "");
                prefs.putBooleanValue(Tags.remember, true);
                prefs.putStringValue(Tags.EMAIL, et_username.getText().toString().trim());
                prefs.putStringValue(Tags.PASSWORD, et_password.getText().toString());
            }
            executeLogin();
        }
        else
        {
            AppDelegate.showToast(this, getResources().getString(R.string.not_connected));
        }
    }

    private void executeLogin()
    {
        try
        {
            if (AppDelegate.haveNetworkConnection(this, true))
            {
                ArrayList<PostAysnc_Model> mPostArrayList = new ArrayList<PostAysnc_Model>();
                PostAsync mPostAsyncObj;
                AppDelegate.getInstance(this).setPostParamsSecond(mPostArrayList, Tags.API_KEY, Tags.API_KEY_VALUE);
                AppDelegate.getInstance(this).setPostParamsSecond(mPostArrayList, Tags.email, et_username.getText().toString().trim());
                AppDelegate.getInstance(this).setPostParamsSecond(mPostArrayList, Tags.password, et_password.getText().toString());
                AppDelegate.getInstance(this).setPostParamsSecond(mPostArrayList, Tags.device_id, prefs.getGCMtokenfromTemp());
                AppDelegate.getInstance(this).setPostParamsSecond(mPostArrayList, Tags.device_type, AppDelegate.DEVICE_TYPE, ServerRequestConstants.Key_PostintValue);
                AppDelegate.getInstance(this).setPostParamsSecond(mPostArrayList, Tags.role, AppDelegate.BUYER, ServerRequestConstants.Key_PostintValue);
                mPostAsyncObj = new PostAsync(this, this, ServerRequestConstants.LOGIN, mPostArrayList, null);
                mHandler.sendEmptyMessage(10);
                mPostAsyncObj.execute();
            }
        }
        catch (Exception e)
        {
            AppDelegate.LogE(e);
            AppDelegate.showToast(this, getResources().getString(R.string.try_again));
        }
    }

    public void locationPermission()
    {
        if (ContextCompat.checkSelfPermission(this,
                Manifest.permission.ACCESS_FINE_LOCATION) != PackageManager.PERMISSION_GRANTED)
        {
            if (ActivityCompat.shouldShowRequestPermissionRationale(this, Manifest.permission.ACCESS_FINE_LOCATION))
            {
                // Show an explanation to the user *asynchronously* -- don't block
                // this thread waiting for the user's response! After the user
                // sees the explanation, try again to request the permission.
                ActivityCompat.requestPermissions(this, new String[]{Manifest.permission.ACCESS_FINE_LOCATION}, MY_PERMISSIONS_REQUEST_LOCATION);
            }
            else
            {
                ActivityCompat.requestPermissions(this, new String[]{Manifest.permission.ACCESS_FINE_LOCATION}, MY_PERMISSIONS_REQUEST_LOCATION);
            }
        }
    }

    @Override
    public void onRequestPermissionsResult(int requestCode, @NonNull String permissions[], @NonNull int[] grantResults)
    {
        switch (requestCode)
        {
            case MY_PERMISSIONS_REQUEST_LOCATION:
            {
                // If request is cancelled, the result arrays are empty.
                if (grantResults.length > 0 && grantResults[0] == PackageManager.PERMISSION_GRANTED)
                {
                    // permission was granted, yay! Do the
                    // contacts-related task you need to do.
                }
                else
                {
                    // permission denied, boo! Disable the
                    // functionality that depends on this permission.
                }
            }
        }
    }

    public void openFacebook()
    {
        FacebookSdk.sdkInitialize(this);
        AppEventsLogger.activateApp(this);
        callbackManager = CallbackManager.Factory.create();
        LoginManager.getInstance().logInWithReadPermissions(this, Arrays.asList("public_profile", "user_friends", "user_birthday", "email"));
        LoginManager.getInstance().registerCallback(callbackManager,
                new FacebookCallback<LoginResult>()
                {
                    @Override
                    public void onSuccess(final LoginResult loginResult)
                    {
                        AppDelegate.LogFB("login success" + loginResult.getAccessToken() + "");
                        AppDelegate.LogT("onSuccess = " + loginResult.getAccessToken() + "");
                        AppDelegate.showProgressDialog(LoginActivity.this);
                        fb_LoginToken = loginResult.getAccessToken().toString();
                        GraphRequest request = GraphRequest.newMeRequest(
                                loginResult.getAccessToken(),
                                new GraphRequest.GraphJSONObjectCallback()
                                {
                                    @Override
                                    public void onCompleted(JSONObject object, GraphResponse response)
                                    {
                                        // Application code
                                        if (response != null)
                                        {
                                            fbUserData = new Fb_detail_GetSet();
                                            fbUserData = new FbDetails().getFacebookDetail(response.getJSONObject().toString());
                                            AppDelegate.LogT("Facebook details==" + fbUserData + "");
                                            AppDelegate.hideProgressDialog(LoginActivity.this);
                                            new Prefs(LoginActivity.this).putStringValueinTemp(Tags.social_id, fbUserData.getSocial_id());
                                            callAsyncFacebookVerify(fbUserData);
                                        }
                                    }
                                });
                        Bundle parameters = new Bundle();
                        parameters.putString("fields", "first_name,last_name,email,id,name,gender,birthday,picture.type(large)");
                        request.setParameters(parameters);
                        request.executeAsync();
                    }

                    @Override
                    public void onCancel()
                    {
                        AppDelegate.LogFB("login cancel");

                        if (AccessToken.getCurrentAccessToken() != null)
                        {
                            LoginManager.getInstance().logOut();
                        }
                        if (!isCalledOnce)
                        {
                            isCalledOnce = true;
                            openFacebook();
                        }
                    }

                    @Override
                    public void onError(FacebookException exception)
                    {
                        AppDelegate.LogFB("login error = " + exception.getMessage());
                        if (exception.getMessage().contains("CONNECTION_FAILURE"))
                        {
                            AppDelegate.hideProgressDialog(LoginActivity.this);
                        }
                        else if (exception instanceof FacebookAuthorizationException)
                        {
                            if (AccessToken.getCurrentAccessToken() != null)
                            {
                                LoginManager.getInstance().logOut();
                                if (!isCalledOnce)
                                {
                                    isCalledOnce = true;
                                    openFacebook();
                                }
                            }
                        }
                    }
                });
    }

    private void callAsyncFacebookVerify(Fb_detail_GetSet fbUserData)
    {
        if (fbUserData == null)
        {
            AppDelegate.showAlert(this, getResources().getString(R.string.facebook_data_incorrect));
        }
        else if (AppDelegate.haveNetworkConnection(this, false))
        {
            try
            {
                ArrayList<PostAysnc_Model> mPostArrayList = new ArrayList<PostAysnc_Model>();
                AppDelegate.getInstance(this).setPostParamsSecond(mPostArrayList, Tags.API_KEY, Tags.API_KEY_VALUE);
                AppDelegate.getInstance(this).setPostParamsSecond(mPostArrayList, Tags.device_id, prefs.getGCMtokenfromTemp());
                AppDelegate.getInstance(this).setPostParamsSecond(mPostArrayList, Tags.device_type, AppDelegate.DEVICE_TYPE, ServerRequestConstants.Key_PostintValue);
                AppDelegate.getInstance(this).setPostParamsSecond(mPostArrayList, Tags.social_id, fbUserData.getSocial_id());
                PostAsync mPostAsyncObj = new PostAsync(this, this, ServerRequestConstants.CHK_FACEBOOK,
                        mPostArrayList, null);
                mHandler.sendEmptyMessage(10);
                mPostAsyncObj.execute();
            }
            catch (Exception e)
            {
                AppDelegate.LogE(e);
            }
        }
        else
        {
            AppDelegate.showToast(this, getResources().getString(R.string.try_again));
        }
    }

    @Override
    public void onActivityResult(int requestCode, int resultCode, Intent data)
    {
        super.onActivityResult(requestCode, resultCode, data);
        AppDelegate.LogT("LoginActivity onActivityResult called");
        callbackManager.onActivityResult(requestCode, resultCode, data);
    }

}
