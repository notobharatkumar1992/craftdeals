package com.craftedeals.activities;

import android.content.Intent;
import android.graphics.drawable.Drawable;
import android.os.Build;
import android.os.Bundle;
import android.os.Handler;
import android.os.Message;
import android.support.v4.app.Fragment;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.DefaultItemAnimator;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.View;
import android.view.Window;
import android.view.WindowManager;

import com.craftedeals.AppDelegate;
import com.craftedeals.Async.PostAsync;
import com.craftedeals.Models.CategoryModel;
import com.craftedeals.Models.HeroDealsModel;
import com.craftedeals.Models.PostAysnc_Model;
import com.craftedeals.R;
import com.craftedeals.Utils.Prefs;
import com.craftedeals.adapters.MyHeroDealsAdapter;
import com.craftedeals.constants.ServerRequestConstants;
import com.craftedeals.constants.Tags;
import com.craftedeals.interfaces.OnReciveServerResponse;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;

import carbon.widget.TextView;

/**
 * Created by Heena on 07-Oct-16.
 */
public class MyHeroDealsActivity extends AppCompatActivity implements View.OnClickListener, OnReciveServerResponse {
    private Prefs prefs;
    private TextView txt_c_title, txt_c_all, txt_c_active, txt_c_viewdeals;
    public Handler mHandler;
    public static final int LABEL_ACTIVE = 1, LABEL_ALL = 2;
    private RecyclerView recycler_deals_list;

    private HeroDealsModel heroDealsModel;
    private ArrayList<HeroDealsModel> heroDealsModelArrayList = new ArrayList<>();

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.LOLLIPOP) {
            Window window = this.getWindow();
            Drawable background = this.getResources().getDrawable(R.drawable.topbg);
            window.addFlags(WindowManager.LayoutParams.FLAG_DRAWS_SYSTEM_BAR_BACKGROUNDS);
            window.setStatusBarColor(getResources().getColor(android.R.color.transparent));
            window.setNavigationBarColor(getResources().getColor(android.R.color.transparent));
            window.setBackgroundDrawable(background);
        }
        setContentView(R.layout.my_purchase);
        prefs = new Prefs(this);
        initView();
        setHandler();
        mHandler.sendEmptyMessage(3);
    }

    private void initView() {
        findViewById(R.id.img_back).setOnClickListener(this);
        txt_c_title = (TextView) findViewById(R.id.txt_c_title);
        findViewById(R.id.img_search).setOnClickListener(this);
        txt_c_active = (TextView) findViewById(R.id.txt_c_active);
        txt_c_active.setOnClickListener(this);
        txt_c_all = (TextView) findViewById(R.id.txt_c_all);
        txt_c_all.setOnClickListener(this);
        recycler_deals_list = (RecyclerView) findViewById(R.id.recycler_deals_list);
        setselection(LABEL_ACTIVE);
        findViewById(R.id.ll_options).setVisibility(View.GONE);
        txt_c_title.setText("My Hero Deals");
    }


    void setselection(int position) {
        txt_c_active.setSelected(false);
        txt_c_all.setSelected(false);
        switch (position) {
            case LABEL_ACTIVE:
                txt_c_active.setSelected(true);
                break;
            case LABEL_ALL:
                txt_c_all.setSelected(true);
                break;
        }
    }


    private void displayView(int position) {
        AppDelegate.hideKeyBoard(this);
        setselection(position);
        Fragment fragment = null;
        switch (position) {
            case LABEL_ACTIVE:
                mHandler.sendEmptyMessage(4);
                break;
            case LABEL_ALL:
                mHandler.sendEmptyMessage(5);
                break;
            default:
                break;
        }
    }

    private void show_ActiveDealsList(ArrayList<HeroDealsModel> heroDealsModelArrayList) {
        MyHeroDealsAdapter mAdapter = new MyHeroDealsAdapter(this, heroDealsModelArrayList);
        RecyclerView.LayoutManager mLayoutManager = new LinearLayoutManager(this);
        recycler_deals_list.setLayoutManager(mLayoutManager);
        recycler_deals_list.setItemAnimator(new DefaultItemAnimator());
        recycler_deals_list.setAdapter(mAdapter);
    }


    private void setHandler() {
        mHandler = new Handler() {
            @Override
            public void dispatchMessage(Message msg) {
                super.dispatchMessage(msg);
                switch (msg.what) {
                    case 10:
                        AppDelegate.showProgressDialog(MyHeroDealsActivity.this);
                        break;
                    case 11:
                        AppDelegate.hideProgressDialog(MyHeroDealsActivity.this);
                        break;
                    case 4:
                        if (heroDealsModelArrayList != null)
                            show_ActiveDealsList(heroDealsModelArrayList);
                        break;
                    case 5:
                        break;
                    case 3:
                        execute_MyHeroDealsAPI();
                        break;
                }
            }
        };
    }


    @Override
    public void onClick(View v) {
        switch (v.getId()) {
            case R.id.img_back:
                onBackPressed();
                break;
            case R.id.img_search:
                startActivity(new Intent(this, SearchActivity.class));
                break;
            case R.id.txt_c_active:
                displayView(LABEL_ACTIVE);
                break;
            case R.id.txt_c_all:
                displayView(LABEL_ALL);
                break;
            case R.id.list_container:
                break;
            case R.id.map_container:
                break;
            case R.id.txt_c_viewdeals:
                break;
        }
    }

    @Override
    public void onBackPressed() {
        super.onBackPressed();
        super.onBackPressed();
    }


    @Override
    public void setOnReciveResult(String apiName, String result) {
        if (mHandler == null) {
            return;
        }
        mHandler.sendEmptyMessage(11);
        if (!AppDelegate.isValidString(result)) {
            AppDelegate.showToast(MyHeroDealsActivity.this, getResources().getString(R.string.time_out));
            recycler_deals_list.setAdapter(null);
            return;
        }
        if (apiName.equals(ServerRequestConstants.MY_HERODEALS)) {
            recycler_deals_list.setAdapter(null);
            parseMyHeroDeals(result);
        }
    }

    private void execute_MyHeroDealsAPI() {
        try {
            if (AppDelegate.haveNetworkConnection(this, true)) {
                ArrayList<PostAysnc_Model> mPostArrayList = new ArrayList<PostAysnc_Model>();
                AppDelegate.getInstance(this).setPostParamsSecond(mPostArrayList, Tags.API_KEY, Tags.API_KEY_VALUE);
                if (new Prefs(this).getUserdata() != null)
                    AppDelegate.getInstance(this).setPostParamsSecond(mPostArrayList, Tags.userId, (new Prefs(this).getUserdata().id), ServerRequestConstants.Key_PostintValue);
                if (new Prefs(this).getCitydata() != null) {
                    AppDelegate.getInstance(this).setPostParamsSecond(mPostArrayList, Tags.LAT, new Prefs(this).getCitydata().lat);
                    AppDelegate.getInstance(this).setPostParamsSecond(mPostArrayList, Tags.LONG, new Prefs(this).getCitydata().lng);
                }
                PostAsync mPostAsyncObj;
                mPostAsyncObj = new PostAsync(this, this, ServerRequestConstants.MY_HERODEALS,
                        mPostArrayList, null);
                mHandler.sendEmptyMessage(10);
                mPostAsyncObj.execute();
            }
        } catch (Exception e) {
            AppDelegate.showToast(this, getResources().getString(R.string.try_again));
            AppDelegate.LogE(e);
        }
    }

    private void parseMyHeroDeals(String result) {
        try {
            JSONObject jsonobj = new JSONObject(result);
            if (jsonobj.getInt(Tags.status) == 1 && jsonobj.getInt(Tags.dataFlow) == 1) {
                CategoryModel category = new CategoryModel();
                ArrayList<HeroDealsModel> heroDealsModelArrayList = new ArrayList<>();
                JSONArray array = jsonobj.getJSONArray(Tags.response);
                for (int i = 0; i < array.length(); i++) {
                    heroDealsModel = new HeroDealsModel();
                    JSONObject obj = array.getJSONObject(i);
                    heroDealsModel.coupon_id = obj.getInt(Tags.coupon_id);
                    heroDealsModel.deal_id = obj.getInt(Tags.deal_id);

                    // heroDealsModel.name = obj.getString(Tags.name);
                    heroDealsModel.deal_status = obj.getInt(Tags.type);
                    heroDealsModel.distance = obj.optString(Tags.distance);

                    JSONObject heroobj = obj.getJSONObject(Tags.herodeal);


                    heroDealsModel.id = heroobj.getInt(Tags.id);
                    heroDealsModel.user_id = heroobj.getInt(Tags.user_id);
//                    heroDealsModel.user_id = obj.getInt(Tags.user_id);
                    heroDealsModel.herodeal_title = heroobj.getString(Tags.name);
                    heroDealsModel.deal_status = obj.getInt(Tags.type);
                    heroDealsModel.image = heroobj.getString(Tags.image);
                    heroDealsModel.deal_category_id = heroobj.getInt(Tags.deal_category_id);
                    heroDealsModel.sub_category_id = heroobj.getInt(Tags.sub_category_id);
                    heroDealsModel.description = heroobj.getString(Tags.description);
                    heroDealsModel.value_upto = heroobj.getInt(Tags.value_upto);
                    heroDealsModel.discount_offer = heroobj.getString(Tags.discount_offer);
                    heroDealsModel.starting_date = heroobj.getString(Tags.starting_date);
                    heroDealsModel.expiry_date = heroobj.getString(Tags.expiry_date);
                    heroDealsModel.created = heroobj.getString(Tags.created);
                    heroDealsModel.modified = heroobj.getString(Tags.modified);
                    heroDealsModel.trems_and_conditions = heroobj.getString(Tags.trems_and_conditions);
                    heroDealsModel.total_deal = heroobj.getInt(Tags.total_deal);
                    if (AppDelegate.isValidString(heroobj.getString(Tags.user))) {
                        JSONObject vendor = heroobj.getJSONObject(Tags.user);
                        heroDealsModel.vendor_name = vendor.getString(Tags.name);
                        if (vendor.has(Tags.busines)) {
                            if (AppDelegate.isValidString(vendor.getString(Tags.busines))) {
                                heroDealsModel.company_name = vendor.getJSONObject(Tags.busines).getString(Tags.name);
                                heroDealsModel.company_image = vendor.getJSONObject(Tags.busines).getString(Tags.image);
                                heroDealsModel.latitude = vendor.getJSONObject(Tags.busines).getString(Tags.latitude);
                                heroDealsModel.longitude = vendor.getJSONObject(Tags.busines).getString(Tags.longitude);
                                heroDealsModel.company_address = vendor.getJSONObject(Tags.busines).getString(Tags.location_address);
                                heroDealsModel.company_opening_hours = vendor.getJSONObject(Tags.busines).getString(Tags.open_hours);
                                heroDealsModel.company_contact_number = vendor.getJSONObject(Tags.busines).getString(Tags.contact_number);
                                heroDealsModel.cityName = vendor.getJSONObject(Tags.busines).optString(Tags.cityName);
                            }
                        }
                    }
                    JSONObject object = heroobj.getJSONObject(Tags.deal_category);
                    category = new CategoryModel();
                    category.id = object.getInt(Tags.id);
                    category.title = object.getString(Tags.title);
                    category.description = object.getString(Tags.description);
                    category.image = object.getString(Tags.image);
                    heroDealsModel.category = category.title;
                    heroDealsModelArrayList.add(heroDealsModel);
                    this.heroDealsModelArrayList = heroDealsModelArrayList;
                }
                AppDelegate.LogT("heroDealsModelArrayList==" + heroDealsModelArrayList);
                mHandler.sendEmptyMessage(4);
            } else {
                AppDelegate.showToast(this, jsonobj.getString(Tags.message));
            }
        } catch (JSONException e) {
            AppDelegate.LogE(e);
        }
    }

}
