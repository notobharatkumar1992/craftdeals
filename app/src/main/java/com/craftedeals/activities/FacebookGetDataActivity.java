package com.craftedeals.activities;

import android.app.DatePickerDialog;
import android.app.Dialog;
import android.content.Intent;
import android.graphics.Color;
import android.graphics.Typeface;
import android.graphics.drawable.ColorDrawable;
import android.graphics.drawable.Drawable;
import android.os.Build;
import android.os.Bundle;
import android.os.Handler;
import android.os.Message;
import android.support.annotation.Nullable;
import android.support.v7.app.AppCompatActivity;
import android.view.KeyEvent;
import android.view.View;
import android.view.Window;
import android.view.WindowManager;
import android.view.inputmethod.EditorInfo;
import android.view.inputmethod.InputMethodManager;
import android.widget.AdapterView;
import android.widget.DatePicker;
import android.widget.EditText;
import android.widget.ListView;

import com.craftedeals.AppDelegate;
import com.craftedeals.Async.PostAsync;
import com.craftedeals.Models.FavBeveragesModel;
import com.craftedeals.Models.Fb_detail_GetSet;
import com.craftedeals.Models.PostAysnc_Model;
import com.craftedeals.Models.UserDataModel;
import com.craftedeals.R;
import com.craftedeals.Utils.Prefs;
import com.craftedeals.adapters.AlertAdapter;
import com.craftedeals.constants.ServerRequestConstants;
import com.craftedeals.constants.Tags;
import com.craftedeals.interfaces.OnReciveServerResponse;

import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.Calendar;

import carbon.widget.TextView;

/**
 * Created by Bharat on 06-Dec-16.
 */

public class FacebookGetDataActivity extends AppCompatActivity implements View.OnClickListener, OnReciveServerResponse {

    public Handler mHandler;
    public EditText et_email;
    public TextView txt_c_dob, txt_c_heard_about_us, txt_c_dialog_title;
    private int mYear, mMonth, mDay;
    public String last_selected_date = null;
    ArrayList<FavBeveragesModel> heard_about_us_array = new ArrayList<>();
    private AlertAdapter heard_about_us_Adapter;
    private Dialog dialogue;
    private ListView list_view;
    public int type = 0, login_from = 0;
    public Fb_detail_GetSet fbUserData;
    public UserDataModel userDataModel;

    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.LOLLIPOP) {
            Window window = this.getWindow();
            Drawable background = this.getResources().getDrawable(R.drawable.topbg);
            window.addFlags(WindowManager.LayoutParams.FLAG_DRAWS_SYSTEM_BAR_BACKGROUNDS);
            window.setStatusBarColor(getResources().getColor(android.R.color.transparent));
            window.setNavigationBarColor(getResources().getColor(android.R.color.transparent));
            window.setBackgroundDrawable(background);
        }
        setContentView(R.layout.facebook_get_data);
        setheard_about_us();
        type = getIntent().getExtras().getInt(Tags.TYPE);
        login_from = getIntent().getIntExtra(Tags.login_from, 0);
        fbUserData = getIntent().getExtras().getParcelable(Tags.user);
        initView();
        setHandler();
    }

    private void setheard_about_us() {
        FavBeveragesModel favBeveragesModel = new FavBeveragesModel();
        favBeveragesModel.name = getString(R.string.Facebook);
        heard_about_us_array.add(favBeveragesModel);
        favBeveragesModel = new FavBeveragesModel();
        favBeveragesModel.name = getString(R.string.Google);
        heard_about_us_array.add(favBeveragesModel);
        favBeveragesModel = new FavBeveragesModel();
        favBeveragesModel.name = getString(R.string.Instagram);
        heard_about_us_array.add(favBeveragesModel);
        favBeveragesModel = new FavBeveragesModel();
        favBeveragesModel.name = getString(R.string.WordofMouth);
        heard_about_us_array.add(favBeveragesModel);
        favBeveragesModel = new FavBeveragesModel();
        favBeveragesModel.name = getString(R.string.FrothMagazine);
        heard_about_us_array.add(favBeveragesModel);
        favBeveragesModel = new FavBeveragesModel();
        favBeveragesModel.name = getString(R.string.OtherPublication);
        heard_about_us_array.add(favBeveragesModel);
    }

    private void initView() {
        findViewById(R.id.img_c_back).setOnClickListener(this);
        findViewById(R.id.txt_c_submit).setOnClickListener(this);
        et_email = (EditText) findViewById(R.id.et_email);
        et_email.setTypeface(Typeface.createFromAsset(getAssets(), getString(R.string.font_regular)));
        txt_c_dob = (TextView) findViewById(R.id.txt_c_dob);
        txt_c_heard_about_us = (TextView) findViewById(R.id.txt_c_heard_about_us);
        txt_c_heard_about_us.setOnClickListener(this);

        et_email.setOnEditorActionListener(new android.widget.TextView.OnEditorActionListener() {
            @Override
            public boolean onEditorAction(android.widget.TextView v, int actionId, KeyEvent event) {
                if (actionId == EditorInfo.IME_ACTION_NEXT) {
                    // Creating uri to be opened
                    AppDelegate.hideKeyBoard(FacebookGetDataActivity.this);
//                    if (!AppDelegate.isValidString(txt_c_dob.getText().toString()))
                    if (!AppDelegate.isValidString(fbUserData.bitrhday)) {
                        txt_c_dob.performClick();
                    } else {
                        txt_c_heard_about_us.performClick();
                    }

                }
                return false;
            }
        });
        if (fbUserData != null) {
            if (AppDelegate.isValidString(fbUserData.emailid)) {
                et_email.setText(fbUserData.emailid + "");
                et_email.setEnabled(false);
            }
            if (AppDelegate.isValidString(fbUserData.bitrhday)) {
                txt_c_dob.setText(fbUserData.bitrhday);
            } else {
                txt_c_dob.setOnClickListener(this);
            }
        }
    }

    private void setHandler() {
        mHandler = new Handler() {
            @Override
            public void dispatchMessage(Message msg) {
                super.dispatchMessage(msg);
                switch (msg.what) {
                    case 10:
                        AppDelegate.showProgressDialog(FacebookGetDataActivity.this);
                        break;
                    case 11:
                        AppDelegate.hideProgressDialog(FacebookGetDataActivity.this);
                        break;
                }
            }
        };
    }

    private void showDateDialog() {
        Calendar calendar = Calendar.getInstance();
        if (AppDelegate.isValidString(last_selected_date)) {

            String parts[] = last_selected_date.split("/");
            int day = Integer.parseInt(parts[0]);
            int month = Integer.parseInt(parts[1]);
            int year = Integer.parseInt(parts[2]);

            calendar.set(Calendar.YEAR, year);
            calendar.set(Calendar.MONTH, month);
            calendar.add(Calendar.MONTH, -1);
            calendar.set(Calendar.DAY_OF_MONTH, day);
            mYear = calendar.get(Calendar.YEAR);
            mMonth = calendar.get(Calendar.MONTH);
            mDay = calendar.get(Calendar.DAY_OF_MONTH);
        } else {
            calendar = Calendar.getInstance();
            mYear = calendar.get(Calendar.YEAR);
            mMonth = calendar.get(Calendar.MONTH);
            mDay = calendar.get(Calendar.DAY_OF_MONTH);

        }
        DatePickerDialog dpd = new DatePickerDialog(this,
                new DatePickerDialog.OnDateSetListener() {
                    @Override
                    public void onDateSet(DatePicker view, int year, int monthOfYear, int dayOfMonth) {
                        // txt_c_dob.setText((monthOfYear + 1) + "-" + dayOfMonth + "-" + year);
                        txt_c_dob.setText(dayOfMonth + "/" + (monthOfYear + 1) + "/" + year);
                        last_selected_date = txt_c_dob.getText().toString();
                        //  if (!AppDelegate.isValidString(txt_c_fav_beer.getText().toString()))
                        new Handler().postDelayed(new Runnable() {
                            @Override
                            public void run() {
                                txt_c_heard_about_us.performClick();
                            }
                        }, 300);
                    }
                }, mYear, mMonth, mDay);
        Calendar c = Calendar.getInstance();
        c.get(Calendar.YEAR);
        c.add(Calendar.YEAR, -18);
        c.set(Calendar.YEAR, c.get(Calendar.YEAR));
        c.set(Calendar.MONTH, c.get(Calendar.MONTH));
        c.set(Calendar.DAY_OF_MONTH, c.get(Calendar.DAY_OF_MONTH));
        dpd.getDatePicker().setMaxDate(c.getTimeInMillis()/*c.getTimeInMillis()*//*System.currentTimeMillis() - 10000*/);
        dpd.show();
    }

    @Override
    public void onClick(View v) {
        switch (v.getId()) {
            case R.id.txt_c_submit:
                check_validation();
                break;
            case R.id.img_c_back:
                onBackPressed();
                break;
            case R.id.txt_c_dob:
                showDateDialog();
                break;
            case R.id.txt_c_heard_about_us:
                heard_about_us_Dialog(heard_about_us_array);
                break;
        }
    }

    public void dialog() {
        dialogue = new Dialog(this);
        dialogue.requestWindowFeature(Window.FEATURE_NO_TITLE);
        dialogue.setContentView(R.layout.dialog);
        dialogue.getWindow().setBackgroundDrawable(new ColorDrawable(Color.TRANSPARENT));
        txt_c_dialog_title = (TextView) dialogue.findViewById(R.id.txt_c_dialog_title);
        list_view = (ListView) dialogue.findViewById(R.id.dialog_list);
    }

    public void heard_about_us_Dialog(final ArrayList<FavBeveragesModel> heard_about_us_array) {
        dialog();
        txt_c_dialog_title.setText(getString(R.string.heard_about_us));
        heard_about_us_Adapter = new AlertAdapter(this, heard_about_us_array);
        list_view.setAdapter(heard_about_us_Adapter);
        list_view.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView<?> parent, View view, int position, long id) {
                txt_c_heard_about_us.setText(heard_about_us_array.get(position).name);
                if (dialogue != null && dialogue.isShowing()) {
                    dialogue.dismiss();
                    // if (!AppDelegate.isValidString(txt_c_fav_brewery.getText().toString()))

                }
            }
        });
        if (dialogue != null && dialogue.isShowing()) {
            dialogue.dismiss();
            dialogue.show();
        } else {
            if (dialogue != null)
                dialogue.show();
        }
    }

    private void check_validation() {
        if (!AppDelegate.CheckEmail(et_email.getText().toString())) {
            AppDelegate.showToast(this, getResources().getString(R.string.foremail));
        } else if (!AppDelegate.isValidString(txt_c_dob.getText().toString())) {
            AppDelegate.showToast(this, getResources().getString(R.string.forbirth));
        } else if (!AppDelegate.isValidString(txt_c_heard_about_us.getText().toString())) {
            AppDelegate.showToast(this, getResources().getString(R.string.for_heard_about_us));
        } else if (AppDelegate.haveNetworkConnection(this, false)) {
            fbUserData.bitrhday = txt_c_dob.getText().toString();
            fbUserData.heard_about_us = txt_c_heard_about_us.getText().toString();
            fbUserData.emailid = et_email.getText().toString();
            Intent intent = new Intent(FacebookGetDataActivity.this, MemberShipActivity.class);
            Bundle bundle = new Bundle();
            bundle.putInt(Tags.TYPE, AppDelegate.DATA_TYPE_FB);
            bundle.putInt(Tags.login_from, login_from);
            bundle.putParcelable(Tags.user, fbUserData);
            intent.putExtras(bundle);
            startActivity(intent);
            finish();
        } else {
            AppDelegate.showToast(this, getResources().getString(R.string.not_connected));
        }
    }

    private void execute_changePasswrodAsync() {

    }

    @Override
    public void setOnReciveResult(String apiName, String result) {
        mHandler.sendEmptyMessage(11);
        if (!AppDelegate.isValidString(result)) {
            AppDelegate.showToast(this, getResources().getString(R.string.time_out));
            return;
        }
        if (apiName.equalsIgnoreCase(ServerRequestConstants.CHANGE_PASSWORD)) {
//            parseChangePassword(result);
        }
    }
}
