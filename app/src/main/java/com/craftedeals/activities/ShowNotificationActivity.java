package com.craftedeals.activities;

import android.graphics.drawable.Drawable;
import android.os.Build;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.view.View;
import android.view.Window;
import android.view.WindowManager;

import com.craftedeals.AppDelegate;
import com.craftedeals.Models.HeroDealsModel;
import com.craftedeals.Models.NotificationModel;
import com.craftedeals.R;
import com.craftedeals.constants.Tags;

import carbon.widget.TextView;


/**
 * Created by Heena on 07-Oct-16.
 */
public class ShowNotificationActivity extends AppCompatActivity implements View.OnClickListener {

    TextView txt_c_viewdetail, txt_c_details;
    private int deal_id;
    public String str_from;
    private HeroDealsModel heroDealsModel;
    private NotificationModel notificationModel;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.LOLLIPOP) {
            Window window = this.getWindow();
            Drawable background = this.getResources().getDrawable(R.drawable.topbg);
            window.addFlags(WindowManager.LayoutParams.FLAG_DRAWS_SYSTEM_BAR_BACKGROUNDS);
            window.setStatusBarColor(getResources().getColor(android.R.color.transparent));
            window.setNavigationBarColor(getResources().getColor(android.R.color.transparent));
            window.setBackgroundDrawable(background);
        }
        setContentView(R.layout.show_notification);
        initView();
    }

    private void initView() {
        txt_c_details = (TextView) findViewById(R.id.txt_c_details);
        txt_c_viewdetail = (TextView) findViewById(R.id.txt_c_viewdetail);
        txt_c_viewdetail.setOnClickListener(this);
        deal_id = getIntent().getIntExtra(Tags.deal_id, 0);
        str_from = getIntent().getStringExtra(Tags.FROM);
        if (!AppDelegate.isValidString(str_from) && getIntent().getExtras() != null && getIntent().getExtras().getParcelable(Tags.notification) != null) {
            notificationModel = getIntent().getExtras().getParcelable(Tags.notification);
            txt_c_details.setText(notificationModel.membership_description);
        }
    }

    @Override
    protected void onDestroy() {
        super.onDestroy();
    }

    @Override
    public void onClick(View v) {
        switch (v.getId()) {
            case R.id.txt_c_viewdetail:
                break;
        }
    }
}
