package com.craftedeals.activities;

import android.app.Activity;
import android.content.Intent;
import android.graphics.drawable.Drawable;
import android.os.Build;
import android.os.Bundle;
import android.os.Handler;
import android.os.Message;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.DefaultItemAnimator;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.View;
import android.view.Window;
import android.view.WindowManager;
import android.widget.LinearLayout;

import com.craftedeals.AppDelegate;
import com.craftedeals.Async.PostAsync;
import com.craftedeals.Models.HeroDealsModel;
import com.craftedeals.Models.ItemsModel;
import com.craftedeals.Models.PostAysnc_Model;
import com.craftedeals.R;
import com.craftedeals.Utils.Prefs;
import com.craftedeals.adapters.OrderSummaryAdapter;
import com.craftedeals.constants.ServerRequestConstants;
import com.craftedeals.constants.Tags;
import com.craftedeals.interfaces.OnItemClick;
import com.craftedeals.interfaces.OnReciveServerResponse;
import com.paypal.android.sdk.payments.PayPalAuthorization;
import com.paypal.android.sdk.payments.PayPalFuturePaymentActivity;
import com.paypal.android.sdk.payments.PayPalPayment;
import com.paypal.android.sdk.payments.PayPalProfileSharingActivity;
import com.paypal.android.sdk.payments.PayPalService;
import com.paypal.android.sdk.payments.PaymentActivity;
import com.paypal.android.sdk.payments.PaymentConfirmation;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.math.BigDecimal;
import java.util.ArrayList;

import carbon.widget.TextView;

import static com.craftedeals.activities.DealsActivity.DEAL_ADHOC;

/**
 * Created by Heena on 10-Oct-16.
 */
public class OrderSummaryActivity extends AppCompatActivity implements View.OnClickListener, OnReciveServerResponse, OnItemClick {

    private Handler mHandler;
    private LinearLayout ll_adhoc_deals;
    public static final int ADHOC = 1, HERO = 2;
    private HeroDealsModel heroDealsModel;
    private ArrayList<HeroDealsModel> heroDealsModelArrayList;
    private RecyclerView recycler_order;
    private ArrayList<ItemsModel> itemsModelArrayList;
    private String total_amount_string = "0", adhoc_deal_id = "0", image = "", deal_name;
    private TextView txt_c_total_amount, txt_c_proceed;
    private ArrayList<ItemsModel> itemsArrayList = new ArrayList<>();

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.LOLLIPOP) {
            Window window = this.getWindow();
            Drawable background = this.getResources().getDrawable(R.drawable.topbg);
            window.addFlags(WindowManager.LayoutParams.FLAG_DRAWS_SYSTEM_BAR_BACKGROUNDS);
            window.setStatusBarColor(getResources().getColor(android.R.color.transparent));
            window.setNavigationBarColor(getResources().getColor(android.R.color.transparent));
            window.setBackgroundDrawable(background);
        }
        setContentView(R.layout.order_summery);
        try {
            itemsModelArrayList = getIntent().getParcelableArrayListExtra(Tags.items);
            total_amount_string = getIntent().getStringExtra(Tags.amount);
            adhoc_deal_id = getIntent().getStringExtra(Tags.adhoc_deal_id);
            image = getIntent().getStringExtra(Tags.image);
            deal_name = getIntent().getStringExtra(Tags.deal);
            AppDelegate.LogT("adhoc_deal_id=" + adhoc_deal_id);
        } catch (Exception e) {
            AppDelegate.LogE(e);
        }
        initView();
        setHandler();
        startPayPalService();
        mHandler.sendEmptyMessage(3);
    }

    private void setHandler() {
        mHandler = new Handler() {
            @Override
            public void dispatchMessage(Message msg) {
                super.dispatchMessage(msg);
                switch (msg.what) {
                    case 10:
                        AppDelegate.showProgressDialog(OrderSummaryActivity.this);
                        break;
                    case 11:
                        AppDelegate.hideProgressDialog(OrderSummaryActivity.this);
                        break;
                    case 1:
                        break;
                    case 2:
                        break;
                    case 3:
                        show_order();
                        break;
                }
            }
        };
    }

    private void show_order() {
        txt_c_total_amount.setText("Total Amount: $ " + total_amount_string + "");
        if (itemsModelArrayList != null && itemsModelArrayList.size() > 0) {
            AppDelegate.LogT("send to confirm items=" + itemsModelArrayList);
            for (ItemsModel itemsModel : itemsModelArrayList) {
                if (itemsModel.item_count.equalsIgnoreCase("0")) {
                } else {
                    itemsArrayList.add(itemsModel);
                }
            }

            OrderSummaryAdapter mAdapter = new OrderSummaryAdapter(this, itemsArrayList, this, image);
            RecyclerView.LayoutManager mLayoutManager = new LinearLayoutManager(this);
            recycler_order.setLayoutManager(mLayoutManager);
            recycler_order.setItemAnimator(new DefaultItemAnimator());
            recycler_order.setAdapter(mAdapter);
        }
    }

    private void initView() {
        findViewById(R.id.img_c_back).setOnClickListener(this);
        recycler_order = (RecyclerView) findViewById(R.id.recycler_order);
        txt_c_proceed = (TextView) findViewById(R.id.txt_c_proceed);
        txt_c_proceed.setOnClickListener(this);
        txt_c_total_amount = (TextView) findViewById(R.id.txt_c_total_amount);
    }

    @Override
    public void onClick(View v) {
        switch (v.getId()) {
            case R.id.img_c_back:
                finish();
                break;
            case R.id.txt_c_proceed:
                onBuyPressed(total_amount_string, total_amount_string);
                break;
        }
    }


    @Override
    public void setOnReciveResult(String apiName, String result) {
        mHandler.sendEmptyMessage(11);
        if (!AppDelegate.isValidString(result)) {
            AppDelegate.showToast(OrderSummaryActivity.this, getResources().getString(R.string.time_out));
            return;
        }
        if (apiName.equals(ServerRequestConstants.CONFIRM_ORDER)) {
            parseConfirmOrder(result);
        }
    }

    private void parseConfirmOrder(String result) {
        try {
            JSONObject jsonobj = new JSONObject(result);
            if (jsonobj.getInt(Tags.status) == 1) {
                Intent intent = new Intent(this, CongratulationActivity.class);
                intent.putExtra(Tags.deal, deal_name);
                intent.putExtra(Tags.deal_type, DEAL_ADHOC);
                intent.putExtra(Tags.types, CongratulationActivity.DEAL_BOUGHT);
                startActivity(intent);
//                AppDelegate.showToast(OrderSummaryActivity.this, jsonobj.getString(Tags.message));
                onBackPressed();
                if (AdHocDealDetailActivity.mActivity != null) {
                    AdHocDealDetailActivity.mActivity.finish();
                }
            } else {
                AppDelegate.showToast(OrderSummaryActivity.this, jsonobj.getString(Tags.message));
            }
        } catch (JSONException e) {
            AppDelegate.LogE(e);
        }
    }

    @Override
    public void setOnItemClickListener(String name, int position, ArrayList<ItemsModel> itemsModelArrayList) {
        this.itemsArrayList = itemsModelArrayList;
        total_amount_string = "0";
        if (name.equalsIgnoreCase(Tags.clicks)) {
            if (itemsModelArrayList.size() > 0) {
                for (ItemsModel itemsModel : itemsModelArrayList) {
                    if (itemsModel.item_count.equalsIgnoreCase("0")) {
                        total_amount_string = String.valueOf(Integer.parseInt(itemsModel.item_count) * itemsModel.price + Integer.parseInt(total_amount_string));
                        txt_c_total_amount.setText("Total Amount: $ " + total_amount_string + "");
                    } else {
                        total_amount_string = String.valueOf(Integer.parseInt(itemsModel.item_count) * itemsModel.price + Integer.parseInt(total_amount_string));
                        txt_c_total_amount.setText("Total Amount: $ " + total_amount_string + "");
                    }
                }
            } else {
                total_amount_string = "0";
                txt_c_total_amount.setText("Total Amount: $ " + "0" + "");
                onBackPressed();
            }
        }
    }

    /*paypal integration code Start*/
    public void onBuyPressed(String str_amount, String str_item_name) {
        /*
         * PAYMENT_INTENT_SALE will cause the payment to complete immediately.
         * Change PAYMENT_INTENT_SALE to
         *   - PAYMENT_INTENT_AUTHORIZE to only authorize payment and capture funds later.
         *   - PAYMENT_INTENT_ORDER to create a payment for authorization and capture
         *     later via calls from your server.
         *
         * Also, to include additional payment details and an item list, see getStuffToBuy() below.
         */
        PayPalPayment thingToBuy = new PayPalPayment(new BigDecimal(str_amount), "USD", str_item_name, PayPalPayment.PAYMENT_INTENT_SALE);

        /*
         * See getStuffToBuy(..) for examples of some available payment options.
         */
        Intent intent = new Intent(OrderSummaryActivity.this, PaymentActivity.class);

        // send the same configuration for restart resiliency
        intent.putExtra(PayPalService.EXTRA_PAYPAL_CONFIGURATION, AppDelegate.config);
        intent.putExtra(PaymentActivity.EXTRA_PAYMENT, thingToBuy);
        startActivityForResult(intent, AppDelegate.REQUEST_CODE_PAYMENT);
    }

//    private PayPalPayment getThingToBuy(String paymentIntent) {
//        return new PayPalPayment(new BigDecimal("0.01"), "USD", "sample item",
//                paymentIntent);
//    }

    private void startPayPalService() {
        Intent intent = new Intent(this, PayPalService.class);
        intent.putExtra(PayPalService.EXTRA_PAYPAL_CONFIGURATION, AppDelegate.config);
        startService(intent);
    }

    public String TAG = "paypal";

    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        if (requestCode == AppDelegate.REQUEST_CODE_PAYMENT) {
            if (resultCode == Activity.RESULT_OK) {
                PaymentConfirmation confirm =
                        data.getParcelableExtra(PaymentActivity.EXTRA_RESULT_CONFIRMATION);
                if (confirm != null) {
                    try {
                        Log.i(TAG, confirm.toJSONObject().toString(4));
                        Log.i(TAG, confirm.getPayment().toJSONObject().toString(4));
                        /**
                         *  TODO: send 'confirm' (and possibly confirm.getPayment() to your server for verification
                         * or consent completion.
                         * See https://developer.paypal.com/webapps/developer/docs/integration/mobile/verify-mobile-payment/
                         * for more details.
                         *
                         * For sample mobile backend interactions, see
                         * https://github.com/paypal/rest-api-sdk-python/tree/master/samples/mobile_backend
                         */
                        JSONObject jsonObject = new JSONObject(confirm.toJSONObject().toString(4));
                        jsonObject.getJSONObject("response").getString("id");
                        AppDelegate.trackPurchaseDeal(deal_name, "ORDER SUMMERY");
                        callAsyncBuy(jsonObject.getJSONObject("response").getString("id"));

                        AppDelegate.showToast(OrderSummaryActivity.this, "Payment info received from PayPal, Transaction ID is = " + jsonObject.getJSONObject("response").getString("id"));

                    } catch (JSONException e) {
                        Log.e(TAG, "an extremely unlikely failure occurred: ", e);
                    }
                }
            } else if (resultCode == Activity.RESULT_CANCELED) {
                Log.i(TAG, "The user canceled.");
            } else if (resultCode == PaymentActivity.RESULT_EXTRAS_INVALID) {
                Log.i(
                        TAG,
                        "An invalid Payment or PayPalConfiguration was submitted. Please see the docs.");
            }
        } else if (requestCode == AppDelegate.REQUEST_CODE_FUTURE_PAYMENT) {
            if (resultCode == Activity.RESULT_OK) {
                PayPalAuthorization auth =
                        data.getParcelableExtra(PayPalFuturePaymentActivity.EXTRA_RESULT_AUTHORIZATION);
                if (auth != null) {
                    try {
                        Log.i("FuturePaymentExample", auth.toJSONObject().toString(4));

                        String authorization_code = auth.getAuthorizationCode();
                        Log.i("FuturePaymentExample", authorization_code);

                        sendAuthorizationToServer(auth);
                        AppDelegate.showToast(OrderSummaryActivity.this, "Future Payment code received from PayPal");

                    } catch (JSONException e) {
                        Log.e("FuturePaymentExample", "an extremely unlikely failure occurred: ", e);
                    }
                }
            } else if (resultCode == Activity.RESULT_CANCELED) {
                Log.i("FuturePaymentExample", "The user canceled.");
            } else if (resultCode == PayPalFuturePaymentActivity.RESULT_EXTRAS_INVALID) {
                Log.i(
                        "FuturePaymentExample",
                        "Probably the attempt to previously start the PayPalService had an invalid PayPalConfiguration. Please see the docs.");
            }
        } else if (requestCode == AppDelegate.REQUEST_CODE_PROFILE_SHARING) {
            if (resultCode == Activity.RESULT_OK) {
                PayPalAuthorization auth =
                        data.getParcelableExtra(PayPalProfileSharingActivity.EXTRA_RESULT_AUTHORIZATION);
                if (auth != null) {
                    try {
                        Log.i("ProfileSharingExample", auth.toJSONObject().toString(4));

                        String authorization_code = auth.getAuthorizationCode();
                        Log.i("ProfileSharingExample", authorization_code);

                        sendAuthorizationToServer(auth);
                        AppDelegate.showToast(OrderSummaryActivity.this, "Profile Sharing code received from PayPal");

                    } catch (JSONException e) {
                        Log.e("ProfileSharingExample", "an extremely unlikely failure occurred: ", e);
                    }
                }
            } else if (resultCode == Activity.RESULT_CANCELED) {
                Log.i("ProfileSharingExample", "The user canceled.");
            } else if (resultCode == PayPalFuturePaymentActivity.RESULT_EXTRAS_INVALID) {
                Log.i(
                        "ProfileSharingExample",
                        "Probably the attempt to previously start the PayPalService had an invalid PayPalConfiguration. Please see the docs.");
            }
        }
    }

    private void callAsyncBuy(String paypal_id) {
        JSONArray jsonArray = new JSONArray();

        for (int i = 0; i < itemsArrayList.size(); i++) {
            if (itemsArrayList.get(i).item_count.equalsIgnoreCase("0")) {

            } else {
                try {
                    JSONObject jsonObject = new JSONObject();
                    jsonObject.put("id", itemsArrayList.get(i).id + "");
                    jsonObject.put("quantity", itemsArrayList.get(i).item_count + "");
                    jsonObject.put("price", itemsArrayList.get(i).price + "");
                    jsonArray.put(jsonObject);
                } catch (JSONException e) {
                    e.printStackTrace();
                }
            }
        }
        try {
            if (AppDelegate.haveNetworkConnection(this, true)) {
                ArrayList<PostAysnc_Model> mPostArrayList = new ArrayList<PostAysnc_Model>();
                AppDelegate.getInstance(this).setPostParamsSecond(mPostArrayList, Tags.API_KEY, Tags.API_KEY_VALUE);
                if (new Prefs(this).getUserdata() != null) {
                    AppDelegate.getInstance(this).setPostParamsSecond(mPostArrayList, Tags.user_id, (new Prefs(this).getUserdata().id), ServerRequestConstants.Key_PostintValue);
                    AppDelegate.getInstance(this).setPostParamsSecond(mPostArrayList, Tags.adhoc_deal_id, adhoc_deal_id);
                    AppDelegate.getInstance(this).setPostParamsSecond(mPostArrayList, Tags.total_price, total_amount_string);
                    AppDelegate.getInstance(this).setPostParamsSecond(mPostArrayList, Tags.paypal_id, paypal_id);
                    AppDelegate.getInstance(this).setPostParamsSecond(mPostArrayList, Tags.items, jsonArray.toString());
                }
                PostAsync mPostAsyncObj;
                mPostAsyncObj = new PostAsync(this, this, ServerRequestConstants.CONFIRM_ORDER,
                        mPostArrayList, null);
                mHandler.sendEmptyMessage(10);
                mPostAsyncObj.execute();
            }
        } catch (Exception e) {
            AppDelegate.showToast(this, getString(R.string.try_again));
            AppDelegate.LogE(e);
        }
    }

    private void sendAuthorizationToServer(PayPalAuthorization authorization) {
        /**
         * TODO: Send the authorization response to your server, where it can
         * exchange the authorization code for OAuth access and refresh tokens.
         *
         * Your server must then store these tokens, so that your server code
         * can execute payments for this user in the future.
         *
         * A more complete example that includes the required app-server to
         * PayPal-server integration is available from
         * https://github.com/paypal/rest-api-sdk-python/tree/master/samples/mobile_backend
         */

    }
    /*paypal integration code Exit*/
}
