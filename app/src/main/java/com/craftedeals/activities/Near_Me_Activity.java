package com.craftedeals.activities;

import android.content.Context;
import android.content.IntentSender;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.graphics.drawable.Animatable;
import android.graphics.drawable.AnimationDrawable;
import android.graphics.drawable.Drawable;
import android.location.Location;
import android.os.Build;
import android.os.Bundle;
import android.os.Handler;
import android.os.Looper;
import android.os.Message;
import android.support.annotation.Nullable;
import android.support.v7.app.AppCompatActivity;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.Window;
import android.view.WindowManager;
import android.widget.RelativeLayout;

import com.craftedeals.AppDelegate;
import com.craftedeals.Async.LocationAddress;
import com.craftedeals.Models.CityModel;
import com.craftedeals.Models.Person;
import com.craftedeals.R;
import com.craftedeals.Utils.CircleImageView;
import com.craftedeals.Utils.Prefs;
import com.craftedeals.Utils.WorkaroundMapFragment;
import com.craftedeals.constants.Tags;
import com.google.android.gms.common.ConnectionResult;
import com.google.android.gms.common.api.GoogleApiClient;
import com.google.android.gms.common.api.PendingResult;
import com.google.android.gms.common.api.ResultCallback;
import com.google.android.gms.common.api.Status;
import com.google.android.gms.location.LocationListener;
import com.google.android.gms.location.LocationRequest;
import com.google.android.gms.location.LocationServices;
import com.google.android.gms.location.LocationSettingsRequest;
import com.google.android.gms.location.LocationSettingsResult;
import com.google.android.gms.location.LocationSettingsStates;
import com.google.android.gms.location.LocationSettingsStatusCodes;
import com.google.android.gms.maps.CameraUpdateFactory;
import com.google.android.gms.maps.GoogleMap;
import com.google.android.gms.maps.OnMapReadyCallback;
import com.google.android.gms.maps.model.BitmapDescriptorFactory;
import com.google.android.gms.maps.model.CameraPosition;
import com.google.android.gms.maps.model.LatLng;
import com.google.android.gms.maps.model.LatLngBounds;
import com.google.android.gms.maps.model.Marker;
import com.google.android.gms.maps.model.MarkerOptions;
import com.nostra13.universalimageloader.core.DisplayImageOptions;
import com.nostra13.universalimageloader.core.ImageLoaderConfiguration;
import com.nostra13.universalimageloader.core.assist.FailReason;
import com.nostra13.universalimageloader.core.listener.ImageLoadingListener;

import org.apache.commons.lang3.text.WordUtils;

import java.io.IOException;
import java.io.InputStream;
import java.net.URL;

import carbon.widget.ImageView;
import carbon.widget.LinearLayout;
import carbon.widget.TextView;

import static com.craftedeals.activities.DealsActivity.DEAL_HERO;

/**
 * Created by Bharat on 07/29/2016.
 */
public class Near_Me_Activity extends AppCompatActivity implements GoogleApiClient.ConnectionCallbacks, GoogleApiClient.OnConnectionFailedListener,
        LocationListener, WorkaroundMapFragment.OnTouchListener, View.OnClickListener {

    private GoogleMap map_business;
    private String name = "", country = "", state = "", city = "", image = "", company_name = "";
    private Handler mHandler;
    private TextView txt_c_title;
    public com.nostra13.universalimageloader.core.ImageLoader imageLoader = com.nostra13.universalimageloader.core.ImageLoader.getInstance();
    public DisplayImageOptions options = new DisplayImageOptions.Builder().cacheInMemory(true)
            .cacheOnDisc(true).resetViewBeforeLoading(true).
                    build();
    ImageView img_c_loading1;
    CircleImageView cimg_user;
    Person person;
    private Marker customMarker;
    private final static int CONNECTION_FAILURE_RESOLUTION_REQUEST = 9000;
    private GoogleApiClient mGoogleApiClient;
    private LocationRequest mLocationRequest;
    private double currentLatitude = 0, currentLongitude = 0;
    WorkaroundMapFragment map;
    TextView txt_c_address;
    LinearLayout ll_c_;
    CityModel cityModel;

    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.LOLLIPOP) {
            Window window = this.getWindow();
            Drawable background = this.getResources().getDrawable(R.drawable.topbg);
            window.addFlags(WindowManager.LayoutParams.FLAG_DRAWS_SYSTEM_BAR_BACKGROUNDS);
            window.setStatusBarColor(getResources().getColor(android.R.color.transparent));
            window.setNavigationBarColor(getResources().getColor(android.R.color.transparent));
            window.setBackgroundDrawable(background);
        }
        setContentView(R.layout.near_me);
        initGPS();
        showGPSalert();
        imageLoader.init(ImageLoaderConfiguration.createDefault(this));
        setHandler();
        initView();
    }

    private void showGPSalert() {
        if (mGoogleApiClient == null) {
            mGoogleApiClient = new GoogleApiClient.Builder(Near_Me_Activity.this)
                    .addApi(LocationServices.API)
                    .addConnectionCallbacks(this)
                    .addOnConnectionFailedListener(this).build();
            mGoogleApiClient.connect();
        }
        LocationRequest locationRequest = LocationRequest.create();
        locationRequest.setPriority(LocationRequest.PRIORITY_HIGH_ACCURACY);
        locationRequest.setInterval(30 * 1000);
        locationRequest.setFastestInterval(5 * 1000);
        LocationSettingsRequest.Builder builder = new LocationSettingsRequest.Builder()
                .addLocationRequest(locationRequest);
        //**************************
        builder.setAlwaysShow(true); //this is the key ingredient
        //**************************
        PendingResult<LocationSettingsResult> result =
                LocationServices.SettingsApi.checkLocationSettings(mGoogleApiClient, builder.build());
        result.setResultCallback(new ResultCallback<LocationSettingsResult>() {
            @Override
            public void onResult(LocationSettingsResult result) {
                final Status status = result.getStatus();
                final LocationSettingsStates state = result.getLocationSettingsStates();
                switch (status.getStatusCode()) {
                    case LocationSettingsStatusCodes.SUCCESS:
                        // All location settings are satisfied. The client can initialize location
                        // requests here.
                        break;
                    case LocationSettingsStatusCodes.RESOLUTION_REQUIRED:
                        // Location settings are not satisfied. But could be fixed by showing the user
                        // a dialog.
                        try {
                            // Show the dialog by calling startResolutionForResult(),
                            // and check the result in onActivityResult().
                            status.startResolutionForResult(
                                    Near_Me_Activity.this, 1000);
                        } catch (IntentSender.SendIntentException e) {
                            // Ignore the error.
                        }
                        break;
                    case LocationSettingsStatusCodes.SETTINGS_CHANGE_UNAVAILABLE:
                        // Location settings are not satisfied. However, we have no way to fix the
                        // settings so we won't show the dialog.
                        break;
                }
            }
        });
    }

    private void initGPS() {
        mGoogleApiClient = new GoogleApiClient.Builder(this)
                .addConnectionCallbacks(this)
                .addOnConnectionFailedListener(this)
                .addApi(LocationServices.API)
                .build();
        AppDelegate.LogT("mGoogleApiClient Initialited");
        mLocationRequest = LocationRequest.create()
                .setPriority(LocationRequest.PRIORITY_HIGH_ACCURACY)
                .setInterval(500)        // 10 seconds, in milliseconds
                .setFastestInterval(500); // 10 second, in milliseconds
        AppDelegate.LogT("mLocationRequest Initialited");
    }

    @Override
    protected void onPause() {
        super.onPause();
        try {
            if (mGoogleApiClient.isConnected()) {
                LocationServices.FusedLocationApi.removeLocationUpdates(mGoogleApiClient, Near_Me_Activity.this);
                mGoogleApiClient.disconnect();
            }
        } catch (Exception e) {
            AppDelegate.LogE(e);
        }
    }

    private void initView() {
        txt_c_title = (TextView) findViewById(R.id.txt_c_title);
        txt_c_address = (TextView) findViewById(R.id.txt_c_address);
        img_c_loading1 = (ImageView) findViewById(R.id.img_c_loading1);
        txt_c_title.setText(getString(R.string.select_location));
        findViewById(R.id.img_c_back).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                onBackPressed();
            }
        });
        ll_c_ = (LinearLayout) findViewById(R.id.ll_c_);
        if (new Prefs(this).getCitydata() != null) {
            currentLatitude = Double.parseDouble(new Prefs(this).getCitydata().lat);
            currentLongitude = Double.parseDouble(new Prefs(this).getCitydata().lng);
        }
        map = (WorkaroundMapFragment) getSupportFragmentManager().findFragmentById(R.id.fragment_map);
        /*((WorkaroundMapFragment) getSupportFragmentManager().findFragmentById(R.id.fragment_map))*/
        map.getMapAsync(new OnMapReadyCallback() {
            @Override
            public void onMapReady(GoogleMap googleMap) {
                map_business = googleMap;
                showMap();
            }
        });

    }

    private String place_name = "", place_address = "";

    public void setResultFromGeoCoderApi(Bundle bundle) {
        mHandler.sendEmptyMessage(11);
        if (bundle.getString(Tags.PLACE_NAME) != null) {
            place_name = bundle.getString(Tags.PLACE_NAME);
            place_address = bundle.getString(Tags.PLACE_ADD);
            showMap();
        } else {
            AppDelegate.showToast(Near_Me_Activity.this, getResources().getString(R.string.no_location_available));
        }
    }

    public void setLatLngAndFindAddress(final LatLng latLng, long countDownTime) {
        LatLng arg0 = AppDelegate.getRoundedLatLng(latLng);
        LocationAddress.getAddressFromLocation(arg0.latitude, arg0.longitude, this, mHandler);
    }

    private void showMap() {
        if (map_business == null) {
            return;
        }
        map.setListener(this);
        map_business.setMyLocationEnabled(true);
        map_business.getUiSettings().setMapToolbarEnabled(false);
        map_business.getUiSettings().setZoomControlsEnabled(false);
        map_business.getUiSettings().setCompassEnabled(false);
        map_business.setMapType(GoogleMap.MAP_TYPE_NORMAL);
        map_business.clear();
        mHandler.sendEmptyMessage(10);
        ll_c_.setOnClickListener(this);
        new Handler(this.getMainLooper()).post(new Runnable() {
            @Override
            public void run() {
                AppDelegate.setStictModePermission();

                CameraPosition cameraPosition = new CameraPosition.Builder().target(new LatLng(currentLatitude, currentLongitude))
                        .zoom(5).build();
                map_business.moveCamera(CameraUpdateFactory.newCameraPosition(cameraPosition));
                setloc(currentLatitude, currentLongitude);
            } // This is your code
        });
        try {
            if (map_business != null) {
                LatLngBounds AUSTRALIA = new LatLngBounds(new LatLng(-44, 113), new LatLng(-10, 154));
                map_business.moveCamera(CameraUpdateFactory.newLatLngBounds(AUSTRALIA, 0));
            }
        } catch (Exception e) {
            AppDelegate.LogE(e);
        }
        map_business.setOnCameraChangeListener(new GoogleMap.OnCameraChangeListener() {
            @Override
            public void onCameraChange(CameraPosition cameraPosition) {
                AppDelegate.LogT("centerLat" + cameraPosition.target.latitude + "");
                AppDelegate.LogT("centerLong" + cameraPosition.target.longitude + "");
                currentLatitude = cameraPosition.target.latitude;
                currentLongitude = cameraPosition.target.longitude;
                setloc(currentLatitude, currentLongitude);
            }
        });
    }

    private void setHandler() {
        mHandler = new Handler() {
            @Override
            public void dispatchMessage(Message msg) {
                super.dispatchMessage(msg);
                switch (msg.what) {
                    case 10:
                        AppDelegate.showProgressDialog(Near_Me_Activity.this);
                        break;
                    case 11:
                        AppDelegate.hideProgressDialog(Near_Me_Activity.this);
                        break;
                    case 2:
                        setAddressFromGEOcoder(msg.getData());
                        break;
                }
            }
        };
    }

    private void setAddressFromGEOcoder(Bundle data) {
        cityModel = null;
        AppDelegate.LogT("setAddressFromGEOcoder called== " + data);
        if (data != null)
            if (!data.getString(Tags.country).equalsIgnoreCase("Australia")) {
                AppDelegate.showToast(this, "Location should be from Australia");
            } else {
                cityModel = new CityModel();
                cityModel.location_type = Tags.current_location;
                cityModel.name = data.getString(Tags.city_param);
                cityModel.lat = String.valueOf(data.getDouble(Tags.LAT)) + "";
                cityModel.lng = String.valueOf(data.getDouble(Tags.LNG)) + "";

            }
        mHandler.sendEmptyMessage(11);
        img_c_loading1.setVisibility(View.GONE);
        txt_c_address.setText(data.getString(Tags.address));
        if (AppDelegate.isValidString(data.getString(Tags.address)))
            ll_c_.setVisibility(View.VISIBLE);

    }

    @Override
    public void onConnected(Bundle bundle) {
        Location location = LocationServices.FusedLocationApi.getLastLocation(mGoogleApiClient);
        AppDelegate.LogT("onConnected Initialited");
        if (location == null) {
            try {
                AppDelegate.LogT("onConnected Initialited== null");
                LocationServices.FusedLocationApi.requestLocationUpdates(mGoogleApiClient, mLocationRequest, Near_Me_Activity.this);
            } catch (Exception e) {
                AppDelegate.LogE(e);
            }
        } else {
            currentLatitude = location.getLatitude();
            currentLongitude = location.getLongitude();
            if (new Prefs(this).getCitydata() == null)
                setloc(currentLatitude, currentLongitude);
            AppDelegate.LogT("latLng = " + currentLatitude + ", " + currentLongitude);
            new Prefs(this).putStringValue(Tags.LAT, String.valueOf(currentLatitude));
            new Prefs(this).putStringValue(Tags.LNG, String.valueOf(currentLongitude));

        }
    }

    private void setloc(double latitude, double longitude) {
        mHandler.sendEmptyMessage(10);
        ll_c_.setVisibility(View.GONE);
        img_c_loading1.setVisibility(View.VISIBLE);
        AnimationDrawable frameAnimation = (AnimationDrawable) img_c_loading1.getDrawable();
        frameAnimation.setCallback(img_c_loading1);
        frameAnimation.setVisible(true, true);
        frameAnimation.start();
        LocationAddress.getAddressFromLocation(latitude, longitude, this, mHandler);
    }

    @Override
    public void onConnectionSuspended(int i) {
    }

    @Override
    public void onConnectionFailed(ConnectionResult connectionResult) {
        if (connectionResult.hasResolution()) {
            try {
                AppDelegate.LogT("onConnectionFailed Initialited" + connectionResult);
                connectionResult.startResolutionForResult(this, CONNECTION_FAILURE_RESOLUTION_REQUEST);

            } catch (IntentSender.SendIntentException e) {
                AppDelegate.LogE(e);
            }
        } else {
            Log.e("Error", "Location services connection failed with code " + connectionResult.getErrorCode());
        }
    }

    @Override
    protected void onResume() {
        super.onResume();
        AppDelegate.hideKeyBoard(this);
        mGoogleApiClient.connect();

    }

    @Override
    public void onLocationChanged(Location location) {
        currentLatitude = location.getLatitude();
        currentLongitude = location.getLongitude();
        AppDelegate.LogGP("onLocationChanged latLng = " + currentLatitude + ", " + currentLongitude);

        new Prefs(this).putStringValue(Tags.LAT, String.valueOf(currentLatitude));
        new Prefs(this).putStringValue(Tags.LNG, String.valueOf(currentLongitude));
        try {
            if (new Prefs(this).getCitydata() == null) {
                mHandler.sendEmptyMessage(10);
                LocationAddress.getAddressFromLocation(currentLatitude, currentLongitude, this, mHandler);
            }
//}
            if (mGoogleApiClient.isConnected()) {
                LocationServices.FusedLocationApi.removeLocationUpdates(mGoogleApiClient, Near_Me_Activity.this);
                mGoogleApiClient.disconnect();
                AppDelegate.LogGP("Fused Location api disconnect called");
            }
        } catch (Exception e) {
            AppDelegate.LogE(e);
        }
    }

    @Override
    public void onTouch() {
        AppDelegate.LogT("on touch called");
//        setloc(currentLatitude, currentLongitude);
    }

    @Override
    public void onClick(View v) {
        switch (v.getId()) {
            case R.id.ll_c_:
                if (cityModel != null) {
                    if (AppDelegate.isValidString(cityModel.name)) {
                        new Prefs(this).setCityData(cityModel);
                        AppDelegate.showToast(this, "Location selected");
                        DashBoardActivity.mHandler.sendEmptyMessage(4);
                        onBackPressed();
                    }
                }

                break;
        }

    }
}
