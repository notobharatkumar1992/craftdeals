package com.craftedeals.activities;

import android.graphics.Typeface;
import android.graphics.drawable.Drawable;
import android.os.Build;
import android.os.Bundle;
import android.os.Handler;
import android.os.Message;
import android.support.annotation.Nullable;
import android.support.v7.app.AppCompatActivity;
import android.view.View;
import android.view.Window;
import android.view.WindowManager;
import android.widget.EditText;

import com.craftedeals.AppDelegate;
import com.craftedeals.Async.PostAsync;
import com.craftedeals.Models.PostAysnc_Model;
import com.craftedeals.R;
import com.craftedeals.Utils.Prefs;
import com.craftedeals.constants.ServerRequestConstants;
import com.craftedeals.constants.Tags;
import com.craftedeals.interfaces.OnReciveServerResponse;

import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;

/**
 * Created by Bharat on 06-Dec-16.
 */

public class ChangePasswordActivity extends AppCompatActivity implements View.OnClickListener, OnReciveServerResponse {

    public Handler mHandler;
    public EditText et_old_password, et_new_password, et_confirm_password;

    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.LOLLIPOP) {
            Window window = this.getWindow();
            Drawable background = this.getResources().getDrawable(R.drawable.topbg);
            window.addFlags(WindowManager.LayoutParams.FLAG_DRAWS_SYSTEM_BAR_BACKGROUNDS);
            window.setStatusBarColor(getResources().getColor(android.R.color.transparent));
            window.setNavigationBarColor(getResources().getColor(android.R.color.transparent));
            window.setBackgroundDrawable(background);
        }
        setContentView(R.layout.change_password_activity);
        initView();
        setHandler();
    }

    private void initView() {
        findViewById(R.id.img_c_back).setOnClickListener(this);
        findViewById(R.id.txt_c_submit).setOnClickListener(this);
        et_old_password = (EditText) findViewById(R.id.et_old_password);
        et_old_password.setTypeface(Typeface.createFromAsset(getAssets(), getString(R.string.font_regular)));
        et_new_password = (EditText) findViewById(R.id.et_new_password);
        et_new_password.setTypeface(Typeface.createFromAsset(getAssets(), getString(R.string.font_regular)));
        et_confirm_password = (EditText) findViewById(R.id.et_confirm_password);
        et_confirm_password.setTypeface(Typeface.createFromAsset(getAssets(), getString(R.string.font_regular)));
    }

    private void setHandler() {
        mHandler = new Handler() {
            @Override
            public void dispatchMessage(Message msg) {
                super.dispatchMessage(msg);
                switch (msg.what) {
                    case 10:
                        AppDelegate.showProgressDialog(ChangePasswordActivity.this);
                        break;
                    case 11:
                        AppDelegate.hideProgressDialog(ChangePasswordActivity.this);
                        break;
                }
            }
        };
    }

    @Override
    public void onClick(View v) {
        switch (v.getId()) {
            case R.id.txt_c_submit:
                check_validation();
                break;
            case R.id.img_c_back:
                onBackPressed();
                break;
        }
    }

    private void check_validation() {
        if (!AppDelegate.isValidString(et_old_password.getText().toString())) {
            AppDelegate.showToast(this, getResources().getString(R.string.enter_old_password));
        } else if (!AppDelegate.isLegalPassword(et_old_password.getText().toString())) {
            AppDelegate.showToast(this, getResources().getString(R.string.forlength));
        } else if (!AppDelegate.isLegalPassword(et_new_password.getText().toString())) {
            AppDelegate.showToast(this, getResources().getString(R.string.forlength));
        } else if (et_new_password.getText().toString().equalsIgnoreCase(et_old_password.getText().toString())) {
            AppDelegate.showToast(this, getResources().getString(R.string.forchange_pass));
        } else if (!AppDelegate.isValidString(et_confirm_password.getText().toString())) {
            AppDelegate.showToast(this, getResources().getString(R.string.forconpass));
        } else if (!et_confirm_password.getText().toString().equalsIgnoreCase(et_new_password.getText().toString())) {
            AppDelegate.showToast(this, getResources().getString(R.string.formatch));
        } else if (AppDelegate.haveNetworkConnection(this, false)) {
            execute_changePasswrodAsync();
        } else {
            AppDelegate.showToast(this, getResources().getString(R.string.not_connected));
        }
    }

    private void execute_changePasswrodAsync() {
        try {
            if (AppDelegate.haveNetworkConnection(this, true)) {
                ArrayList<PostAysnc_Model> mPostArrayList = new ArrayList<PostAysnc_Model>();
                PostAsync mPostAsyncObj;
                AppDelegate.getInstance(this).setPostParamsSecond(mPostArrayList, Tags.API_KEY, Tags.API_KEY_VALUE);
                AppDelegate.getInstance(this).setPostParamsSecond(mPostArrayList, Tags.user_id, new Prefs(this).getUserdata().id);
                AppDelegate.getInstance(this).setPostParamsSecond(mPostArrayList, Tags.old_password, et_old_password.getText().toString());
                AppDelegate.getInstance(this).setPostParamsSecond(mPostArrayList, Tags.new_password, et_new_password.getText().toString());

                AppDelegate.getInstance(this).setPostParamsSecond(mPostArrayList, Tags.device_type, 2, ServerRequestConstants.Key_PostintValue);
                AppDelegate.getInstance(this).setPostParamsSecond(mPostArrayList, Tags.role, AppDelegate.BUYER, ServerRequestConstants.Key_PostintValue);
                mPostAsyncObj = new PostAsync(this, this, ServerRequestConstants.CHANGE_PASSWORD,
                        mPostArrayList, null);
                mHandler.sendEmptyMessage(10);
                mPostAsyncObj.execute();
            }
        } catch (Exception e) {
            AppDelegate.LogE(e);
            AppDelegate.showToast(this,getResources().getString(R.string.try_again));
        }
    }

    @Override
    public void setOnReciveResult(String apiName, String result) {
        mHandler.sendEmptyMessage(11);
        if (!AppDelegate.isValidString(result)) {
            AppDelegate.showToast(this, getResources().getString(R.string.time_out));
            return;
        }
        if (apiName.equalsIgnoreCase(ServerRequestConstants.CHANGE_PASSWORD)) {
            parseChangePassword(result);
        }
    }

    private void parseChangePassword(String result) {
        try {
            JSONObject jsonObject = new JSONObject(result);
            if (jsonObject.getInt(Tags.status) == 1) {
                AppDelegate.showToast(this, jsonObject.getString(Tags.message));
                new Prefs(ChangePasswordActivity.this).putStringValueinTemp(Tags.PASSWORD, et_new_password.getText().toString());
                finish();
            } else {
                AppDelegate.showToast(this, jsonObject.getString(Tags.message));
            }
        } catch (JSONException e) {
           AppDelegate.LogE(e);
        }
    }
}
