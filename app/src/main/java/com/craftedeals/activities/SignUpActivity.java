package com.craftedeals.activities;

import android.app.Activity;
import android.app.DatePickerDialog;
import android.app.Dialog;
import android.content.Context;
import android.content.Intent;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.graphics.Color;
import android.graphics.Matrix;
import android.graphics.Typeface;
import android.graphics.drawable.ColorDrawable;
import android.graphics.drawable.Drawable;
import android.media.ExifInterface;
import android.net.Uri;
import android.os.AsyncTask;
import android.os.Build;
import android.os.Bundle;
import android.os.Environment;
import android.os.Handler;
import android.os.Message;
import android.provider.MediaStore;
import android.support.annotation.NonNull;
import android.support.v4.app.FragmentActivity;
import android.support.v7.app.AlertDialog;
import android.support.v7.app.AppCompatActivity;
import android.text.Editable;
import android.text.TextWatcher;
import android.util.Log;
import android.view.KeyEvent;
import android.view.View;
import android.view.Window;
import android.view.WindowManager;
import android.view.inputmethod.EditorInfo;
import android.view.inputmethod.InputMethodManager;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.DatePicker;
import android.widget.EditText;
import android.widget.ListView;
import android.widget.Toast;

import com.craftedeals.AppDelegate;
import com.craftedeals.Async.PostAsync;
import com.craftedeals.Models.FavBeveragesModel;
import com.craftedeals.Models.PostAysnc_Model;
import com.craftedeals.Models.UserDataModel;
import com.craftedeals.R;
import com.craftedeals.Utils.CircleImageView;
import com.craftedeals.Utils.Prefs;
import com.craftedeals.adapters.AlertAdapter;
import com.craftedeals.adapters.CityAdapterSearch;
import com.craftedeals.constants.ServerRequestConstants;
import com.craftedeals.constants.Tags;
import com.craftedeals.interfaces.OnPictureResult;
import com.craftedeals.interfaces.OnReciveServerResponse;
import com.craftedeals.parser.JSONParser;
import com.yalantis.ucrop.UCrop;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.io.File;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.util.ArrayList;
import java.util.Calendar;

import carbon.widget.TextView;

public class SignUpActivity extends AppCompatActivity implements View.OnClickListener, OnReciveServerResponse, OnPictureResult
{

    public static Activity mActivity;
    private EditText et_fav_brewery, et_username, et_lastname, et_password, et_email, et_confirm_password, et_contact_no, et_address, et_search;
    public Handler mHandler;
    private Prefs prefs;
    ArrayList<FavBeveragesModel> favBeerArraylist = new ArrayList<>();
    ArrayList<FavBeveragesModel> favBreweryArrayList = new ArrayList<>();
    ArrayList<FavBeveragesModel> cityArray = new ArrayList<>();
    ArrayList<FavBeveragesModel> provinceArray = new ArrayList<>();
    private CircleImageView cimg_user;
    private FavBeveragesModel favBeerModel, favBreweryModel;
    // Spinner spinner_favbrewery, spinner_favbeer, spinner_province, spinner_city;
    int login_from = 0;
    private int mYear, mMonth, mDay;
    TextView txt_c_dob, txt_c_heard_about_us;
    private Dialog dialogue, dialog;
    private AlertAdapter breweryAdapter, beerAdapter, heard_about_us_Adapter;
    CityAdapterSearch provinceAdapter, cityAdpter;
    private TextView txt_c_dialog_title, txt_c_province, txt_c_city, txt_c_fav_beer, txt_c_search_dialog_title;
    private ListView list_view;
    public int province_id = 0, city_id = 0, beer_id = 0, brewery_id = 0;
    public String last_selected_date = null;
    ArrayList<FavBeveragesModel> heard_about_us_array = new ArrayList<>();

    @Override
    protected void onCreate(Bundle savedInstanceState)
    {
        super.onCreate(savedInstanceState);
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.LOLLIPOP)
        {
            Window window = this.getWindow();
            Drawable background = this.getResources().getDrawable(R.drawable.topbg);
            window.addFlags(WindowManager.LayoutParams.FLAG_DRAWS_SYSTEM_BAR_BACKGROUNDS);
            window.setStatusBarColor(getResources().getColor(android.R.color.transparent));
            window.setNavigationBarColor(getResources().getColor(android.R.color.transparent));
            window.setBackgroundDrawable(background);
        }
        setContentView(R.layout.sign_up);
        setheard_about_us();
        mActivity = this;
        prefs = new Prefs(this);
        setHandler();
//        mHandler.sendEmptyMessage(1);
        initView();
        mHandler.sendEmptyMessage(7);
    }

    private void setheard_about_us()
    {
        FavBeveragesModel favBeveragesModel = new FavBeveragesModel();
        favBeveragesModel.name = getString(R.string.Facebook);
        heard_about_us_array.add(favBeveragesModel);
        favBeveragesModel = new FavBeveragesModel();
        favBeveragesModel.name = getString(R.string.Google);
        heard_about_us_array.add(favBeveragesModel);
        favBeveragesModel = new FavBeveragesModel();
        favBeveragesModel.name = getString(R.string.Instagram);
        heard_about_us_array.add(favBeveragesModel);
        favBeveragesModel = new FavBeveragesModel();
        favBeveragesModel.name = getString(R.string.WordofMouth);
        heard_about_us_array.add(favBeveragesModel);
        favBeveragesModel = new FavBeveragesModel();
        favBeveragesModel.name = getString(R.string.FrothMagazine);
        heard_about_us_array.add(favBeveragesModel);
        favBeveragesModel = new FavBeveragesModel();
        favBeveragesModel.name = getString(R.string.OtherPublication);
        heard_about_us_array.add(favBeveragesModel);
    }

    @Override
    protected void onDestroy()
    {
        super.onDestroy();
        mActivity = null;
    }

    private void showDateDialog()
    {
        Calendar calendar = Calendar.getInstance();
        if (AppDelegate.isValidString(last_selected_date))
        {

            String parts[] = last_selected_date.split("/");
            int day = Integer.parseInt(parts[0]);
            int month = Integer.parseInt(parts[1]);
            int year = Integer.parseInt(parts[2]);

            calendar.set(Calendar.YEAR, year);
            calendar.set(Calendar.MONTH, month);
            calendar.add(Calendar.MONTH, -1);
            calendar.set(Calendar.DAY_OF_MONTH, day);
            mYear = calendar.get(Calendar.YEAR);
            mMonth = calendar.get(Calendar.MONTH);
            mDay = calendar.get(Calendar.DAY_OF_MONTH);
        }
        else
        {
            calendar = Calendar.getInstance();
            mYear = calendar.get(Calendar.YEAR);
            mMonth = calendar.get(Calendar.MONTH);
            mDay = calendar.get(Calendar.DAY_OF_MONTH);

        }
        DatePickerDialog dpd = new DatePickerDialog(this,
                new DatePickerDialog.OnDateSetListener()
                {
                    @Override
                    public void onDateSet(DatePicker view, int year, int monthOfYear, int dayOfMonth)
                    {
                        // txt_c_dob.setText((monthOfYear + 1) + "-" + dayOfMonth + "-" + year);
                        txt_c_dob.setText(dayOfMonth + "/" + (monthOfYear + 1) + "/" + year);
                        last_selected_date = txt_c_dob.getText().toString();
                        txt_c_dob.setError(null);
                        txt_c_dob.clearFocus();
                        //  if (!AppDelegate.isValidString(txt_c_fav_beer.getText().toString()))
                        new Handler().postDelayed(new Runnable()
                        {
                            @Override
                            public void run()
                            {
                                txt_c_province.performClick();
                            }
                        }, 300);
                    }
                }, mYear, mMonth, mDay);
        Calendar c = Calendar.getInstance();
        c.get(Calendar.YEAR);
        c.add(Calendar.YEAR, -18);
        c.set(Calendar.YEAR, c.get(Calendar.YEAR));
        c.set(Calendar.MONTH, c.get(Calendar.MONTH));
        c.set(Calendar.DAY_OF_MONTH, c.get(Calendar.DAY_OF_MONTH));
        dpd.getDatePicker().setMaxDate(c.getTimeInMillis()/*c.getTimeInMillis()*//*System.currentTimeMillis() - 10000*/);
        dpd.show();
    }

    private void initView()
    {
        findViewById(R.id.rl_user_img).setVisibility(View.GONE);
        cimg_user = (CircleImageView) findViewById(R.id.cimg_user);
        cimg_user.setOnClickListener(this);
        et_fav_brewery = (EditText) findViewById(R.id.et_fav_brewery);
        et_fav_brewery.setTypeface(Typeface.createFromAsset(getAssets(), getString(R.string.font_regular)));
        et_username = (EditText) findViewById(R.id.et_username);
        et_username.setTypeface(Typeface.createFromAsset(getAssets(), getString(R.string.font_regular)));
        et_lastname = (EditText) findViewById(R.id.et_lastname);
        et_lastname.setTypeface(Typeface.createFromAsset(getAssets(), getString(R.string.font_regular)));
        et_password = (EditText) findViewById(R.id.et_password);
        et_password.setTypeface(Typeface.createFromAsset(getAssets(), getString(R.string.font_regular)));
        et_email = (EditText) findViewById(R.id.et_email);
        et_email.setTypeface(Typeface.createFromAsset(getAssets(), getString(R.string.font_regular)));
        et_confirm_password = (EditText) findViewById(R.id.et_confirm_password);
        et_confirm_password.setTypeface(Typeface.createFromAsset(getAssets(), getString(R.string.font_regular)));
        findViewById(R.id.txt_c_sign_up).setOnClickListener(this);
        findViewById(R.id.img_back).setOnClickListener(this);
        txt_c_fav_beer = (TextView) findViewById(R.id.txt_c_fav_beer);
        txt_c_city = (TextView) findViewById(R.id.txt_c_city);
        txt_c_province = (TextView) findViewById(R.id.txt_c_province);
        txt_c_dob = (TextView) findViewById(R.id.txt_c_dob);
        txt_c_heard_about_us = (TextView) findViewById(R.id.txt_c_heard_about_us);
        txt_c_heard_about_us.setOnClickListener(this);
        txt_c_fav_beer.setOnClickListener(this);
        txt_c_city.setOnClickListener(this);
        txt_c_province.setOnClickListener(this);
        txt_c_dob.setOnClickListener(this);
        et_contact_no = (EditText) findViewById(R.id.et_contact_no);
        et_contact_no.setTypeface(Typeface.createFromAsset(getAssets(), getString(R.string.font_regular)));
        et_address = (EditText) findViewById(R.id.et_address);
        et_address.setTypeface(Typeface.createFromAsset(getAssets(), getString(R.string.font_regular)));
        et_fav_brewery.setOnEditorActionListener(new android.widget.TextView.OnEditorActionListener()
        {
            @Override
            public boolean onEditorAction(android.widget.TextView v, int actionId, KeyEvent event)
            {
                if (actionId == EditorInfo.IME_ACTION_NEXT)
                {
                    // Creating uri to be opened
                    AppDelegate.hideKeyBoard(SignUpActivity.this);
//                    if (!AppDelegate.isValidString(txt_c_dob.getText().toString()))
//                    txt_c_dob.performClick();
                }
                return false;
            }
        });
        et_confirm_password.setOnEditorActionListener(new android.widget.TextView.OnEditorActionListener()
        {
            @Override
            public boolean onEditorAction(android.widget.TextView v, int actionId, KeyEvent event)
            {
                if (actionId == EditorInfo.IME_ACTION_DONE)
                {
                    // Creating uri to be opened
                    et_confirm_password.setCursorVisible(false);
                }
                return false;
            }
        });
        if (getIntent() != null && AppDelegate.isValidString(getIntent().getIntExtra(Tags.login_from, 0) + ""))
        {
            login_from = getIntent().getIntExtra(Tags.login_from, 0);
        }
    }

    private void setHandler()
    {
        mHandler = new Handler()
        {
            @Override
            public void dispatchMessage(Message msg)
            {
                super.dispatchMessage(msg);
                switch (msg.what)
                {
                    case 10:
                        AppDelegate.showProgressDialog(SignUpActivity.this);
                        break;
                    case 11:
                        AppDelegate.hideProgressDialog(SignUpActivity.this);
                        break;
                    case 1:
                        execute_getBeveragesApi();
                        break;
                    case 2:
                        // beerDialog(favBeerArraylist);
                        break;
                    case 3:
                        //  breweryDialog(favBreweryArrayList);
                        break;
                    case 5:
                        provinceDialog(provinceArray);
                        break;
                    case 6:
                        AppDelegate.LogT("city array ==>" + cityArray);
                        cityDialog(cityArray);
                        break;
                    case 7:

                }
            }
        };
    }

    private void execute_getBeveragesApi()
    {
        try
        {
            if (AppDelegate.haveNetworkConnection(this, true))
            {
                ArrayList<PostAysnc_Model> mPostArrayList = new ArrayList<PostAysnc_Model>();
                PostAsync mPostAsyncObj;
                AppDelegate.getInstance(this).setPostParamsSecond(mPostArrayList, Tags.API_KEY, Tags.API_KEY_VALUE);

                mPostAsyncObj = new PostAsync(this, this, ServerRequestConstants.GET_BEVERAGES,
                        mPostArrayList, null);
                mHandler.sendEmptyMessage(10);
                mPostAsyncObj.execute();
            }
        }
        catch (Exception e)
        {
            AppDelegate.LogE(e);
            // AppDelegate.showToast(this, "Please try again.");
        }
    }


    @Override
    public void onClick(View v)
    {
        switch (v.getId())
        {
            case R.id.cimg_user:
                showImageSelectorList();
                break;
            case R.id.txt_c_sign_up:
                chk_validation();
                break;
            case R.id.img_back:
                onBackPressed();
                break;
            case R.id.txt_c_dob:
                showDateDialog();
                break;

            case R.id.txt_c_fav_beer:
                if (favBeerArraylist.size() == 0)
                {
                    execute_getBeveragesApi();
                }
                else
                {
//                    beerDialog(favBeerArraylist);
                }
                break;
            case R.id.txt_c_province:
                if (provinceArray.size() == 0)
                {
                    execute_getProvince();
                }
                else
                {
                    provinceDialog(provinceArray);
                }
                break;
            case R.id.txt_c_city:
                AppDelegate.LogT("city clicked");
                if (province_id == 0)
                {
                    AppDelegate.showToast(this, getString(R.string.please_select_state));
                }
                else
                {
                    if (cityArray.size() == 0)
                    {
                        execute_getCityList();
                    }
                    else
                    {
                        cityDialog(cityArray);
                    }
                }
                break;
            case R.id.txt_c_heard_about_us:
                AppDelegate.LogT("city clicked");
                if (heard_about_us_array != null && heard_about_us_array.size() > 0)
                {
                    heard_about_us_Dialog(heard_about_us_array);
                }
                break;
        }
    }

    private void chk_validation()
    {
        /*if (capturedFile == null) {
            AppDelegate.showToast(this, getResources().getString(R.string.forimage));
        } else */
        if (!AppDelegate.isValidString(et_username.getText().toString()))
        {
            et_username.setError(getResources().getString(R.string.forname));
            et_username.requestFocus();
//            AppDelegate.showToast(this, getResources().getString(R.string.forname));
        }
        else if (!AppDelegate.isValidString(et_lastname.getText().toString()))
        {
            et_lastname.setError(getResources().getString(R.string.forlastname));
            et_lastname.requestFocus();
//            AppDelegate.showToast(this, getResources().getString(R.string.forlastname));
        }
        else if (!AppDelegate.CheckEmail(et_email.getText().toString()))
        {
            et_email.setError(getResources().getString(R.string.foremail));
            et_email.requestFocus();
//            AppDelegate.showToast(this, getResources().getString(R.string.foremail));
//        } else if (!AppDelegate.isValidString(et_contact_no.getText().toString())) {
//            et_contact_no.setError(getResources().getString(R.string.forcontact));
//            et_contact_no.requestFocus();
//            AppDelegate.showToast(this, getResources().getString(R.string.forcontact));
        }
        else if ((et_contact_no.getText().toString().length() < 10))
        {
            et_contact_no.setError(getResources().getString(R.string.foracontact_length));
            et_contact_no.requestFocus();
        }
        else if (!AppDelegate.isValidString(et_fav_brewery.getText().toString()))
        {
            et_fav_brewery.setError(getResources().getString(R.string.Select_favBrewery));
            et_fav_brewery.requestFocus();
//            AppDelegate.showToast(this, getResources().getString(R.string.Select_favBrewery));
        }
        else if (!AppDelegate.isValidString(txt_c_dob.getText().toString()))
        {
            txt_c_dob.setError(getResources().getString(R.string.forbirth));
            txt_c_dob.requestFocus();
//            AppDelegate.showToast(this, getResources().getString(R.string.forbirth));
        }/* else if (!AppDelegate.isValidString(txt_c_fav_beer.getText().toString())) {
            AppDelegate.showToast(this, getResources().getString(R.string.select_favBeer));
        }*/
        else if (!AppDelegate.isValidString(txt_c_province.getText().toString()))
        {

            txt_c_province.setError(getResources().getString(R.string.forprovince));
            txt_c_province.requestFocus();
//            AppDelegate.showToast(this, getResources().getString(R.string.forprovince));
        }
        else if (!AppDelegate.isValidString(txt_c_city.getText().toString()))
        {
            txt_c_city.setError(getResources().getString(R.string.forcity));
            txt_c_city.requestFocus();
//            AppDelegate.showToast(this, getResources().getString(R.string.forcity));
        }
        else if (!AppDelegate.isValidString(txt_c_heard_about_us.getText().toString()))
        {
            txt_c_heard_about_us.setError(getResources().getString(R.string.for_heard_about_us));
            txt_c_heard_about_us.requestFocus();
//            AppDelegate.showToast(this, getResources().getString(R.string.for_heard_about_us));
        } /*else if (!AppDelegate.isValidString(et_address.getText().toString())) {
            AppDelegate.showToast(this, getResources().getString(R.string.foraddress));
        }*/
        else if (!AppDelegate.isValidString(et_password.getText().toString()))
        {
            et_password.setError(getResources().getString(R.string.forpass));
            et_password.requestFocus();
//            AppDelegate.showToast(this, getResources().getString(R.string.forpass));
        }
        else if (!AppDelegate.isLegalPassword(et_password.getText().toString()))
        {
            et_password.setError(getResources().getString(R.string.forlength));
            et_password.requestFocus();
//            AppDelegate.showToast(this, getResources().getString(R.string.forlength));
        }
        else if (!AppDelegate.isValidString(et_confirm_password.getText().toString()))
        {
            et_confirm_password.setError(getResources().getString(R.string.forconpass));
            et_confirm_password.requestFocus();
//            AppDelegate.showToast(this, getResources().getString(R.string.forconpass));
        }
        else if (!et_confirm_password.getText().toString().equalsIgnoreCase(et_password.getText().toString()))
        {
            et_confirm_password.setError(getResources().getString(R.string.formatch));
            et_confirm_password.requestFocus();
//            AppDelegate.showToast(this, getResources().getString(R.string.formatch));
        }
        else if (AppDelegate.haveNetworkConnection(this, false))
        {
            UserDataModel userDataModel = new UserDataModel();
            userDataModel.name = et_username.getText().toString();
            userDataModel.last_names = et_lastname.getText().toString();
            userDataModel.password = et_password.getText().toString();
            userDataModel.email = et_email.getText().toString();
            if (capturedFile != null)
            {
                userDataModel.file_path = capturedFile.getAbsolutePath();
            }
//            userDataModel.beer_id = beer_id;
            userDataModel.fav_brewery = et_fav_brewery.getText().toString();
            userDataModel.address = et_address.getText().toString();
            userDataModel.contact_number = et_contact_no.getText().toString();
            userDataModel.dob = txt_c_dob.getText().toString();
            userDataModel.city_id = city_id + "";
            userDataModel.state_id = province_id + "";
            userDataModel.heard_about_us = txt_c_heard_about_us.getText().toString();
            Intent intent = new Intent(SignUpActivity.this, MemberShipActivity.class);
            Bundle bundle = new Bundle();
            bundle.putInt(Tags.TYPE, AppDelegate.DATA_TYPE_SIGNUP);
            bundle.putInt(Tags.login_from, login_from);
            bundle.putParcelable(Tags.user, userDataModel);
            intent.putExtras(bundle);
            startActivity(intent);
        }
        else
        {
            AppDelegate.showToast(this, getResources().getString(R.string.not_connected));
        }
    }

    @Override
    public void setOnReciveResult(String apiName, String result)
    {
        if (mActivity == null)
        {
            return;
        }
        mHandler.sendEmptyMessage(11);
        if (!AppDelegate.isValidString(result))
        {
            AppDelegate.showToast(this, getString(R.string.time_out));
            return;
        }
        if (apiName.equalsIgnoreCase(ServerRequestConstants.SIGN_UP))
        {
            parseSignUp(result);
        }
        else if (apiName.equalsIgnoreCase(ServerRequestConstants.GET_BEVERAGES))
        {
            parseBeverages(result);
        }
        else if (apiName.equalsIgnoreCase(ServerRequestConstants.GET_PROVINCE))
        {
            parseProvince(result);
        }
        else if (apiName.equalsIgnoreCase(ServerRequestConstants.GET_CITY))
        {
            parseCity(result);
        }
    }

    private void parseProvince(String result)
    {
        try
        {
            JSONObject jsonObject = new JSONObject(result);
            if (jsonObject.getString(Tags.status).equalsIgnoreCase("1"))
            {
                JSONArray jsonArray = jsonObject.getJSONArray(Tags.response);
                FavBeveragesModel cityModel = new FavBeveragesModel();
                for (int i = 0; i < jsonArray.length(); i++)
                {
                    JSONObject object = jsonArray.getJSONObject(i);
                    cityModel = new FavBeveragesModel();
                    cityModel.id = (JSONParser.getInt(object, Tags.id));
                    cityModel.name = JSONParser.getString(object, Tags.region_name);
                    provinceArray.add(cityModel);
                }
                mHandler.sendEmptyMessage(5);
            }
            else
            {
                AppDelegate.showToast(SignUpActivity.this, jsonObject.getString(Tags.message));
            }
        }
        catch (Exception e)
        {
            AppDelegate.LogE(e);
            AppDelegate.showToast(SignUpActivity.this, getString(R.string.try_again));
        }
    }

    private void parseCity(String result)
    {
        try
        {
            JSONObject jsonObject = new JSONObject(result);
            if (jsonObject.getString(Tags.status).equalsIgnoreCase("1"))
            {
                JSONArray jsonArray = jsonObject.getJSONArray(Tags.response);
                FavBeveragesModel cityModel = new FavBeveragesModel();
                for (int i = 0; i < jsonArray.length(); i++)
                {
                    JSONObject object = jsonArray.getJSONObject(i);
                    cityModel = new FavBeveragesModel();
                    cityModel.id = (JSONParser.getInt(object, Tags.id));
                    cityModel.name = JSONParser.getString(object, Tags.name);
                    cityArray.add(cityModel);
                }
                mHandler.sendEmptyMessage(6);
            }
            else
            {
                AppDelegate.showToast(SignUpActivity.this, jsonObject.getString(Tags.message));
            }
        }
        catch (Exception e)
        {
            AppDelegate.LogE(e);
            AppDelegate.showToast(SignUpActivity.this, getString(R.string.try_again));
        }
    }

    private void execute_getProvince()
    {
        try
        {
            if (AppDelegate.haveNetworkConnection(SignUpActivity.this, true))
            {
                ArrayList<PostAysnc_Model> mPostArrayList = new ArrayList<PostAysnc_Model>();
                AppDelegate.getInstance(SignUpActivity.this).setPostParamsSecond(mPostArrayList, Tags.API_KEY, Tags.API_KEY_VALUE);
                AppDelegate.getInstance(SignUpActivity.this).setPostParamsSecond(mPostArrayList, Tags.country_id, 1, ServerRequestConstants.Key_PostintValue);

                PostAsync mPostAsyncObj;
                mPostAsyncObj = new PostAsync(SignUpActivity.this, this, ServerRequestConstants.GET_PROVINCE, mPostArrayList, null);
                AppDelegate.showProgressDialog(SignUpActivity.this);
                mPostAsyncObj.execute();
            }
        }
        catch (Exception e)
        {
            AppDelegate.showToast(SignUpActivity.this, getString(R.string.try_again));
        }
    }

    private void execute_getCityList()
    {
        try
        {
            if (AppDelegate.haveNetworkConnection(SignUpActivity.this, true))
            {
                ArrayList<PostAysnc_Model> mPostArrayList = new ArrayList<PostAysnc_Model>();
                AppDelegate.getInstance(SignUpActivity.this).setPostParamsSecond(mPostArrayList, Tags.API_KEY, Tags.API_KEY_VALUE);
                AppDelegate.getInstance(SignUpActivity.this).setPostParamsSecond(mPostArrayList, Tags.state_id, province_id);
                PostAsync mPostAsyncObj;
                mPostAsyncObj = new PostAsync(SignUpActivity.this, this, ServerRequestConstants.GET_CITY, mPostArrayList, null);
                AppDelegate.showProgressDialog(SignUpActivity.this);
                mPostAsyncObj.execute();
            }
        }
        catch (Exception e)
        {
            AppDelegate.showToast(SignUpActivity.this, getString(R.string.try_again));
            AppDelegate.LogE(e);
        }
    }

    private void parseBeverages(String result)
    {
        try
        {
            JSONObject jsonObject = new JSONObject(result);
            if (jsonObject.getInt(Tags.status) == 1 && jsonObject.getInt(Tags.dataFlow) == 1)
            {
                JSONObject array = jsonObject.getJSONObject(Tags.response);
                JSONArray beerArray = array.getJSONArray(Tags.Beer);
                JSONArray breweryArray = array.getJSONArray(Tags.Brewery);
                for (int i = 0; i < beerArray.length(); i++)
                {
                    jsonObject = beerArray.getJSONObject(i);
                    favBeerModel = new FavBeveragesModel();
                    favBeerModel.id = JSONParser.getInt(jsonObject, Tags.id);
                    favBeerModel.name = JSONParser.getString(jsonObject, Tags.name);
                    favBeerModel.created = JSONParser.getString(jsonObject, Tags.created);
                    favBeerArraylist.add(favBeerModel);
                }
                for (int i = 0; i < breweryArray.length(); i++)
                {
                    jsonObject = breweryArray.getJSONObject(i);
                    favBreweryModel = new FavBeveragesModel();
                    favBreweryModel.id = JSONParser.getInt(jsonObject, Tags.id);
                    favBreweryModel.name = JSONParser.getString(jsonObject, Tags.name);
                    favBreweryModel.created = JSONParser.getString(jsonObject, Tags.created);
                    favBreweryArrayList.add(favBreweryModel);
                }
                mHandler.sendEmptyMessage(2);
            }
            else
            {
                AppDelegate.showToast(this, jsonObject.getString(Tags.message));
            }
        }
        catch (JSONException e)
        {
            AppDelegate.LogE(e);
        }
    }

    private void parseSignUp(String result)
    {
        try
        {
            JSONObject jsonObject = new JSONObject(result);
            if (jsonObject.getInt(Tags.status) == 1 && jsonObject.getInt(Tags.dataFlow) == 0)
            {
                AppDelegate.showToast(this, jsonObject.getString(Tags.message));
                finish();
            }
            else
            {
                AppDelegate.showToast(this, jsonObject.getString(Tags.message));
            }
        }
        catch (JSONException e)
        {
            AppDelegate.LogE(e);
        }

    }


    /* image selection*/
    public static File capturedFile;
    public static Uri imageURI = null;

    public void showImageSelectorList()
    {
        AppDelegate.hideKeyBoard(SignUpActivity.this);
        AlertDialog.Builder builder = new AlertDialog.Builder(SignUpActivity.this);
        ListView modeList = new ListView(SignUpActivity.this);
        String[] stringArray = new String[]{"  Camera", "  Gallery", "  Cancel"};
        ArrayAdapter<String> modeAdapter = new ArrayAdapter<>(SignUpActivity.this, R.layout.spinner_simple_list_item, stringArray);
        modeList.setAdapter(modeAdapter);
        builder.setView(modeList);
        final Dialog dialog = builder.create();
        modeList.setOnItemClickListener(new AdapterView.OnItemClickListener()
        {
            @Override
            public void onItemClick(AdapterView<?> adapterView, View view, int i, long l)
            {
                switch (i)
                {
                    case 0:
                        dialog.dismiss();
                        new OpenCamera().execute();
                        break;
                    case 1:
                        dialog.dismiss();
                        openGallery();
                        break;
                    case 2:
                        dialog.dismiss();
                        break;
                }
            }
        });
        dialog.show();
    }

    @Override
    public void setOnReceivePictureResult(String apiName, Uri picUri)
    {
        if (apiName.equalsIgnoreCase(Tags.PICTURE))
        {
            try
            {
                Bitmap OriginalPhoto = MediaStore.Images.Media.getBitmap(getContentResolver(), picUri);
                OriginalPhoto = Bitmap.createScaledBitmap(OriginalPhoto, 240, 240, true);
                cimg_user.setImageBitmap(OriginalPhoto);
            }
            catch (IOException e)
            {
                AppDelegate.LogE(e);
            }
        }
    }

    class OpenCamera extends AsyncTask<Void, Void, Void>
    {

        @Override
        protected void onPreExecute()
        {
            super.onPreExecute();
            mHandler.sendEmptyMessage(10);
        }

        @Override
        protected Void doInBackground(Void... params)
        {
            Intent intent = new Intent("android.media.action.IMAGE_CAPTURE");
            String str_file_path = getNewFile(SignUpActivity.this);
            if (str_file_path == null ? true : str_file_path.length() > 0 ? false : true)
            {
                AppDelegate.showToast(SignUpActivity.this, getString(R.string.file_not_created));
                return null;
            }
            imageURI = Uri.fromFile(capturedFile);
            intent.putExtra(MediaStore.EXTRA_OUTPUT, imageURI);
            startActivityForResult(intent, AppDelegate.CAPTURE_IMAGE_FULLSIZE_ACTIVITY_REQUEST_CODE);
            return null;
        }

        @Override
        protected void onPostExecute(Void aVoid)
        {
            super.onPostExecute(aVoid);
            mHandler.sendEmptyMessage(11);
        }
    }

    public static String getNewFile(Context mContext)
    {
        File directoryFile;
        if (AppDelegate.isSDcardAvailable())
        {
            directoryFile = new File(Environment.getExternalStorageDirectory()
                    + "/" + mContext.getString(R.string.app_name));
        }
        else
        {
            directoryFile = mContext.getDir(mContext.getString(R.string.app_name), Context.MODE_PRIVATE);
        }
        if (directoryFile.exists() && directoryFile.isDirectory()
                || directoryFile.mkdirs())
        {
            capturedFile = new File(directoryFile, "Image_" + System.currentTimeMillis()
                    + ".png");
            try
            {
                if (capturedFile.createNewFile())
                {
                    AppDelegate.LogT("File created = " + capturedFile.getAbsolutePath());
                    return capturedFile.getAbsolutePath();
                }
            }
            catch (IOException e)
            {
                AppDelegate.LogE(e);
            }
        }
        AppDelegate.LogE("no file created.");
        return null;
    }

    public void openGallery()
    {
        Intent intent = new Intent();
        intent.setType("image/*");
        intent.setAction(Intent.ACTION_GET_CONTENT);
        intent.addCategory(Intent.CATEGORY_OPENABLE);
        startActivityForResult(Intent.createChooser(intent, "Select Picture"), AppDelegate.SELECT_PICTURE);
    }


    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data)
    {
        super.onActivityResult(requestCode, resultCode, data);
        AppDelegate.LogT("onActivityResult MainActivity");
        if (resultCode == Activity.RESULT_OK)
        {
            if (requestCode == AppDelegate.SELECT_PICTURE)
            {
                final Uri selectedUri = data.getData();
                if (selectedUri != null)
                {
                    startCropActivity(SignUpActivity.this, data.getData());
                }
                else
                {
                    Toast.makeText(SignUpActivity.this, R.string.toast_cannot_retrieve_selected_image, Toast.LENGTH_SHORT).show();
                }
            }
            else if (requestCode == UCrop.REQUEST_CROP)
            {
                handleCropResult(data);
            }
            else if (requestCode == AppDelegate.CAPTURE_IMAGE_FULLSIZE_ACTIVITY_REQUEST_CODE)
            {
                if (imageURI != null)
                {
                    startCropActivity(SignUpActivity.this, imageURI);
                }
                else
                {
                    Toast.makeText(this, R.string.toast_cannot_retrieve_selected_image, Toast.LENGTH_SHORT).show();
                }
            }
        }
    }

    private void handleCropResult(@NonNull Intent result)
    {
        final Uri resultUri = UCrop.getOutput(result);
        if (resultUri != null)
        {
            setOnReceivePictureResult(Tags.PICTURE, resultUri);
        }
        else
        {
            Toast.makeText(this, R.string.toast_cannot_retrieve_cropped_image, Toast.LENGTH_SHORT).show();
        }
    }


    @SuppressWarnings("ThrowableResultOfMethodCallIgnored")
    private void handleCropError(@NonNull Intent result)
    {
        final Throwable cropError = UCrop.getError(result);
        if (cropError != null)
        {
            Toast.makeText(this, cropError.getMessage(), Toast.LENGTH_LONG).show();
        }
        else
        {
            Toast.makeText(this, R.string.toast_unexpected_error, Toast.LENGTH_SHORT).show();
        }
    }

    private static final String SAMPLE_CROPPED_IMAGE_NAME = "SampleCropImage";

    public static void startCropActivity(FragmentActivity mActivity, Uri uri)
    {
        String destinationFileName = SAMPLE_CROPPED_IMAGE_NAME;
        destinationFileName += ".png";
        capturedFile = new File(mActivity.getCacheDir(), destinationFileName);
        UCrop uCrop = UCrop.of(uri, Uri.fromFile(capturedFile));
        uCrop = basisConfig(uCrop);
        uCrop = advancedConfig(uCrop);
        uCrop.start(mActivity);
    }

//    public static boolean imageUriIsValid(FragmentActivity mActivity, Uri contentUri) {
//        ContentResolver cr = mActivity.getContentResolver();
//        String[] projection = {MediaStore.MediaColumns.DATA};
//        Cursor cur = cr.query(contentUri, projection, null, null, null);
//        if (cur != null) {
//            if (cur.moveToFirst()) {
//                String filePath = cur.getString(0);
//                if (new File(filePath).exists()) {
//                    // do something if it exists
//                    cur.close();
//                    return true;
//                } else {
//                    // File was not found
//                    cur.close();
//                    AppDelegate.showToast(mActivity, "File not found please try again later.");
//                    return false;
//                }
//            } else {
//                // Uri was ok but no entry found.
//                AppDelegate.showToast(mActivity, "No image found please try again later.");
//                return false;
//            }
//        } else {
//            // content Uri was invalid or some other error occurred
//            AppDelegate.showToast(mActivity, "Image path was invalid some exception occured at your device.");
//            return false;
//        }
//    }

    /**
     * In most cases you need only to set crop aspect ration and max size for resulting image.
     *
     * @param uCrop - ucrop builder instance
     * @return - ucrop builder instance
     */
    public static UCrop basisConfig(@NonNull UCrop uCrop)
    {
        uCrop = uCrop.withAspectRatio(1, 1);
        return uCrop;
    }

    /**
     * Sometimes you want to adjust more options, it's done via {@link UCrop.Options} class.
     *
     * @param uCrop - ucrop builder instance
     * @return - ucrop builder instance
     */
    public static UCrop advancedConfig(@NonNull UCrop uCrop)
    {
        UCrop.Options options = new UCrop.Options();

        options.setCompressionFormat(Bitmap.CompressFormat.PNG);
        options.setCompressionQuality(00);

//        if (SellItemFragment.onPictureResult != null) {
//            options.setHideBottomControls(false);
//            options.setFreeStyleCropEnabled(false);
//        } else {
        options.setHideBottomControls(true);
        options.setFreeStyleCropEnabled(false);
//        }


        return uCrop.withOptions(options);
    }


    public static Bitmap rotateImage(Bitmap source, float angle)
    {
        Matrix matrix = new Matrix();
        matrix.postRotate(angle);
        return Bitmap.createBitmap(source, 0, 0, source.getWidth(), source.getHeight(), matrix, true);
    }

    public void writeImageFile(Bitmap bitmap, String filePath)
    {
        if (AppDelegate.isValidString(filePath))
        {
            capturedFile = new File(filePath);
        }
        else
        {
            return;
        }
        FileOutputStream fOut = null;
        try
        {
            fOut = new FileOutputStream(capturedFile);
        }
        catch (FileNotFoundException e)
        {
            AppDelegate.LogE(e);
        }
        bitmap.compress(Bitmap.CompressFormat.PNG, 85, fOut);
        try
        {
            fOut.flush();
        }
        catch (IOException e)
        {
            AppDelegate.LogE(e);
        }
        try
        {
            fOut.close();
        }
        catch (IOException e)
        {
            AppDelegate.LogE(e);
        }
    }

    private Bitmap getBitmap(String path)
    {
        Uri uri = Uri.fromFile(new File(path));
        InputStream in = null;
        try
        {
            final int IMAGE_MAX_SIZE = 1200000; // 1.2MP
            in = getContentResolver().openInputStream(uri);

            // Decode image size
            BitmapFactory.Options o = new BitmapFactory.Options();
            o.inJustDecodeBounds = true;
            BitmapFactory.decodeStream(in, null, o);
            in.close();


            int scale = 1;
            while ((o.outWidth * o.outHeight) * (1 / Math.pow(scale, 2)) >
                    IMAGE_MAX_SIZE)
            {
                scale++;
            }
            Log.d("", "scale = " + scale + ", orig-width: " + o.outWidth + ", orig-height: " + o.outHeight);

            Bitmap b = null;
            in = getContentResolver().openInputStream(uri);
            if (scale > 1)
            {
                scale--;
                // scale to max possible inSampleSize that still yields an image
                // larger than target
                o = new BitmapFactory.Options();
                o.inSampleSize = scale;
                b = BitmapFactory.decodeStream(in, null, o);

                // resize to desired dimensions
                int height = b.getHeight();
                int width = b.getWidth();
                Log.d("", "1th scale operation dimenions - width: " + width + ", height: " + height);

                double y = Math.sqrt(IMAGE_MAX_SIZE
                        / (((double) width) / height));
                double x = (y / height) * width;

                Bitmap scaledBitmap = Bitmap.createScaledBitmap(b, (int) x,
                        (int) y, true);
                b.recycle();
                b = scaledBitmap;

                System.gc();
            }
            else
            {
                b = BitmapFactory.decodeStream(in);
            }
            in.close();

            Log.d("", "bitmap size - width: " + b.getWidth() + ", height: " +
                    b.getHeight());
            if (b != null)
            {
                ExifInterface ei = null;
                try
                {
                    ei = new ExifInterface(path);
                }
                catch (IOException e)
                {
                    e.printStackTrace();
                }
                int orientation = ei.getAttributeInt(ExifInterface.TAG_ORIENTATION,
                        ExifInterface.ORIENTATION_UNDEFINED);
                switch (orientation)
                {
                    case ExifInterface.ORIENTATION_ROTATE_90:
                        rotateImage(b, 90);
                        break;
                    case ExifInterface.ORIENTATION_ROTATE_180:
                        rotateImage(b, 180);
                        break;
                    case ExifInterface.ORIENTATION_ROTATE_270:
                        rotateImage(b, 270);
                        break;
                    case ExifInterface.ORIENTATION_NORMAL:
                    default:
                        break;
                }
                writeImageFile(b, path);
            }
            return b;
        }
        catch (IOException e)
        {
            Log.e("", e.getMessage(), e);
            return null;
        }
    }

    public void dialog()
    {
        dialogue = new Dialog(this);
        dialogue.requestWindowFeature(Window.FEATURE_NO_TITLE);
        dialogue.setContentView(R.layout.dialog);
        dialogue.getWindow().setBackgroundDrawable(new ColorDrawable(Color.TRANSPARENT));
        txt_c_dialog_title = (TextView) dialogue.findViewById(R.id.txt_c_dialog_title);
        list_view = (ListView) dialogue.findViewById(R.id.dialog_list);
    }


//    public void breweryDialog(final ArrayList<FavBeveragesModel> breweryArray) {
//        dialog();
//        txt_c_dialog_title.setText("Select Brewery");
//        breweryAdapter = new AlertAdapter(this, breweryArray);
//        list_view.setAdapter(breweryAdapter);
//        list_view.setOnItemClickListener(new AdapterView.OnItemClickListener() {
//            @Override
//            public void onItemClick(AdapterView<?> parent, View view, int position, long id) {
//                txt_c_fav_brewery.setText(breweryArray.get(position).name);
//                brewery_id = breweryArray.get(position).id;
//                if (dialogue != null && dialogue.isShowing()) {
//                    dialogue.dismiss();
//                    // if (!AppDelegate.isValidString(txt_c_province.getText().toString()))
//                    new Handler().postDelayed(new Runnable() {
//                        @Override
//                        public void run() {
//                            txt_c_province.performClick();
//                        }
//                    }, 300);
//                }
//            }
//        });
//        if (dialogue != null && dialogue.isShowing()) {
//            dialogue.dismiss();
//            dialogue.show();
//        } else {
//            if (dialogue != null)
//                dialogue.show();
//        }
//    }

//    public void beerDialog(final ArrayList<FavBeveragesModel> beerArray) {
//        dialog();
//        txt_c_dialog_title.setText("Select Beer");
//        beerAdapter = new AlertAdapter(this, beerArray);
//        list_view.setAdapter(beerAdapter);
//        list_view.setOnItemClickListener(new AdapterView.OnItemClickListener() {
//            @Override
//            public void onItemClick(AdapterView<?> parent, View view, int position, long id) {
//                txt_c_fav_beer.setText(beerArray.get(position).name);
//                beer_id = beerArray.get(position).id;
//                if (dialogue != null && dialogue.isShowing()) {
//                    dialogue.dismiss();
//                    // if (!AppDelegate.isValidString(txt_c_fav_brewery.getText().toString()))
//                    new Handler().postDelayed(new Runnable() {
//                        @Override
//                        public void run() {
//                            txt_c_fav_brewery.performClick();
//                        }
//                    }, 300);
//
//                }
//            }
//        });
//        if (dialogue != null && dialogue.isShowing()) {
//            dialogue.dismiss();
//            dialogue.show();
//        } else {
//            if (dialogue != null)
//                dialogue.show();
//        }
//    }

    public void heard_about_us_Dialog(final ArrayList<FavBeveragesModel> heard_about_us_array)
    {
        dialog();
        txt_c_dialog_title.setText(getString(R.string.heard_about_us));
        heard_about_us_Adapter = new AlertAdapter(this, heard_about_us_array);
        list_view.setAdapter(heard_about_us_Adapter);
        list_view.setOnItemClickListener(new AdapterView.OnItemClickListener()
        {
            @Override
            public void onItemClick(AdapterView<?> parent, View view, int position, long id)
            {
                txt_c_heard_about_us.setText(heard_about_us_array.get(position).name);
                txt_c_heard_about_us.setError(null);
                txt_c_heard_about_us.clearFocus();
                if (dialogue != null && dialogue.isShowing())
                {
                    dialogue.dismiss();
                    // if (!AppDelegate.isValidString(txt_c_fav_brewery.getText().toString()))
                    new Handler().postDelayed(new Runnable()
                    {
                        @Override
                        public void run()
                        {
                            et_password.post(
                                    new Runnable()
                                    {
                                        public void run()
                                        {
                                            InputMethodManager inputMethodManager = (InputMethodManager) getSystemService(INPUT_METHOD_SERVICE);
                                            inputMethodManager.toggleSoftInputFromWindow(et_password.getApplicationWindowToken(), InputMethodManager.SHOW_FORCED, 0);
                                            et_password.requestFocus();
                                        }
                                    });
                        }
                    }, 300);

                }
            }
        });
        if (dialogue != null && dialogue.isShowing())
        {
            dialogue.dismiss();
            dialogue.show();
        }
        else
        {
            if (dialogue != null)
            {
                dialogue.show();
            }
        }
    }

    public void dialogsearch()
    {
        dialogue = new Dialog(this);
        dialogue.requestWindowFeature(Window.FEATURE_NO_TITLE);
        dialogue.setContentView(R.layout.dialog_search);
        dialogue.getWindow().setBackgroundDrawable(new ColorDrawable(Color.TRANSPARENT));
        et_search = (EditText) dialogue.findViewById(R.id.et_search);
        txt_c_search_dialog_title = (TextView) dialogue.findViewById(R.id.txt_c_search_dialog_title);
        et_search.setTypeface(Typeface.createFromAsset(getAssets(), getString(R.string.font_regular)));
        list_view = (ListView) dialogue.findViewById(R.id.dialog_list);
    }

    private void provinceDialog(final ArrayList<FavBeveragesModel> provinceArray)
    {
        dialogsearch();
        txt_c_search_dialog_title.setText(getString(R.string.select_province));
        provinceAdapter = new CityAdapterSearch(this, provinceArray);
        list_view.setAdapter(provinceAdapter);
        list_view.setOnItemClickListener(new AdapterView.OnItemClickListener()
        {
            @Override
            public void onItemClick(AdapterView<?> parent, View view, int position, long id)
            {
                txt_c_province.setText(provinceArray.get(position).name);
                AppDelegate.LogT("onItemClick = " + provinceAdapter.getItem(position).id + "");
                for (FavBeveragesModel cityItems : provinceArray)
                {
                    if (cityItems.name.equalsIgnoreCase(provinceAdapter.getItem(position).name + ""))
                    {
                        txt_c_province.setText(cityItems.name);
                        province_id = cityItems.id;
                        txt_c_province.setError(null);
                        txt_c_province.clearFocus();
                        txt_c_city.setText("");
                        et_address.setText("");
                        cityArray.clear();
                        if (SignUpActivity.this != null && dialogue != null && dialogue.isShowing())
                        {
                            dialogue.dismiss();
                            //if (!AppDelegate.isValidString(txt_c_city.getText().toString()))
                            new Handler().postDelayed(new Runnable()
                            {
                                @Override
                                public void run()
                                {
                                    txt_c_city.performClick();
                                }
                            }, 300);
                        }
                        break;
                    }
                }
            }
        });
        et_search.addTextChangedListener(new TextWatcher()
        {
            @Override
            public void beforeTextChanged(CharSequence s, int start, int count, int after)
            {

            }

            @Override
            public void onTextChanged(CharSequence s, int start, int before, int count)
            {

            }

            @Override
            public void afterTextChanged(Editable s)
            {
                provinceAdapter.filter(et_search.getText().toString());
            }
        });
        if (dialogue != null && dialogue.isShowing())
        {
            dialogue.dismiss();
            dialogue.show();
        }
        else
        {
            if (dialogue != null)
            {
                dialogue.show();
            }
        }
    }

    private void cityDialog(final ArrayList<FavBeveragesModel> cityeArray)
    {
        dialogsearch();
        txt_c_search_dialog_title.setText(getString(R.string.select_city));
        cityAdpter = new CityAdapterSearch(this, cityeArray);
        list_view.setAdapter(cityAdpter);
        list_view.setOnItemClickListener(new AdapterView.OnItemClickListener()
        {
            @Override
            public void onItemClick(AdapterView<?> parent, View view, int position, long id)
            {
                AppDelegate.LogT("onItemClick = " + cityAdpter.getItem(position).id + "");
                for (FavBeveragesModel cityItems : cityeArray)
                {
                    if (cityItems.name.equalsIgnoreCase(cityAdpter.getItem(position).name + ""))
                    {
                        txt_c_city.setText(cityItems.name);
                        txt_c_city.setError(null);
                        txt_c_city.clearFocus();
                        city_id = cityItems.id;
                        et_address.setText("");
                        if (SignUpActivity.this != null && dialogue != null && dialogue.isShowing())
                        {
                            dialogue.dismiss();
                            txt_c_heard_about_us.performClick();
                            // if (!AppDelegate.isValidString(et_address.getText().toString())) {
//                            et_address.post(
//                                    new Runnable() {
//                                        public void run() {
//                                            InputMethodManager inputMethodManager = (InputMethodManager) getSystemService(INPUT_METHOD_SERVICE);
//                                            inputMethodManager.toggleSoftInputFromWindow(et_address.getApplicationWindowToken(), InputMethodManager.SHOW_FORCED, 0);
//                                            et_address.requestFocus();
//                                        }
//                                    });
                        }
                        //  }
                        break;
                    }
                }
            }
        });
        et_search.addTextChangedListener(new TextWatcher()
        {
            @Override
            public void beforeTextChanged(CharSequence s, int start, int count, int after)
            {

            }

            @Override
            public void onTextChanged(CharSequence s, int start, int before, int count)
            {

            }

            @Override
            public void afterTextChanged(Editable s)
            {
                cityAdpter.filter(et_search.getText().toString());
            }
        });
        if (dialogue != null && dialogue.isShowing())
        {
            dialogue.dismiss();
            dialogue.show();
        }
        else
        {
            if (dialogue != null)
            {
                dialogue.show();
            }
        }
    }


}
