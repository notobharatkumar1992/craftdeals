package com.craftedeals.activities;

import android.content.IntentSender;
import android.graphics.Typeface;
import android.graphics.drawable.Drawable;
import android.location.Location;
import android.location.LocationManager;
import android.os.Build;
import android.os.Bundle;
import android.os.Handler;
import android.os.Message;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.text.Editable;
import android.text.TextWatcher;
import android.util.Log;
import android.view.View;
import android.view.Window;
import android.view.WindowManager;
import android.widget.EditText;

import com.craftedeals.AppDelegate;
import com.craftedeals.Async.LocationAddress;
import com.craftedeals.Async.PostAsync;
import com.craftedeals.Models.CityModel;
import com.craftedeals.Models.PostAysnc_Model;
import com.craftedeals.R;
import com.craftedeals.Utils.Prefs;
import com.craftedeals.adapters.CityAdapter;
import com.craftedeals.constants.ServerRequestConstants;
import com.craftedeals.constants.Tags;
import com.craftedeals.interfaces.OnListCityItemClick;
import com.craftedeals.interfaces.OnReciveServerResponse;
import com.craftedeals.parser.JSONParser;
import com.github.aakira.expandablelayout.ExpandableLayout;
import com.google.android.gms.common.ConnectionResult;
import com.google.android.gms.common.api.CommonStatusCodes;
import com.google.android.gms.common.api.GoogleApiClient;
import com.google.android.gms.common.api.PendingResult;
import com.google.android.gms.common.api.ResultCallback;
import com.google.android.gms.common.api.Status;
import com.google.android.gms.location.LocationRequest;
import com.google.android.gms.location.LocationServices;
import com.google.android.gms.location.LocationSettingsRequest;
import com.google.android.gms.location.LocationSettingsResult;
import com.google.android.gms.location.LocationSettingsStates;
import com.google.android.gms.location.LocationSettingsStatusCodes;

import org.json.JSONArray;
import org.json.JSONObject;

import java.util.ArrayList;

import carbon.widget.TextView;


/**
 * Created by Heena on 24-Nov-16.
 */
public class SelectLocationActivity extends AppCompatActivity implements View.OnClickListener, OnReciveServerResponse, OnListCityItemClick, GoogleApiClient.ConnectionCallbacks, GoogleApiClient.OnConnectionFailedListener,
        com.google.android.gms.location.LocationListener {

    public TextView txt_c_no_data;
    public EditText et_search;
    public RecyclerView recycler_deals_list;
    public Handler mHandler;
    private double currentLatitude = 0, currentLongitude = 0;
    public CityAdapter cityAdapter;
    public ArrayList<CityModel> cityModels = new ArrayList<>();
    Prefs prefs;
    private final static int CONNECTION_FAILURE_RESOLUTION_REQUEST = 9000;
    private GoogleApiClient mGoogleApiClient;
    private LocationRequest mLocationRequest;

    public LocationManager locationManager;
    public boolean isClicked = false;

    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.LOLLIPOP) {
            Window window = this.getWindow();
            Drawable background = this.getResources().getDrawable(R.drawable.topbg);
            window.addFlags(WindowManager.LayoutParams.FLAG_DRAWS_SYSTEM_BAR_BACKGROUNDS);
            window.setStatusBarColor(getResources().getColor(android.R.color.transparent));
            window.setNavigationBarColor(getResources().getColor(android.R.color.transparent));
            window.setBackgroundDrawable(background);
        }
        setContentView(R.layout.select_location);
        initView();
        prefs = new Prefs(this);
        initGPS();
        setHandler();
        execute_getCityList();
    }

    private void initGPS() {
        locationManager = (LocationManager) getSystemService(LOCATION_SERVICE);
        mGoogleApiClient = new GoogleApiClient.Builder(this)
                .addConnectionCallbacks(this)
                .addOnConnectionFailedListener(this)
                .addApi(LocationServices.API)
                .build();
        AppDelegate.LogT("mGoogleApiClient Initialited");
        mLocationRequest = LocationRequest.create()
                .setPriority(LocationRequest.PRIORITY_HIGH_ACCURACY)
                .setInterval(500)        // 10 seconds, in milliseconds
                .setFastestInterval(500); // 10 second, in milliseconds
        AppDelegate.LogT("mLocationRequest Initialited");
    }

    private void setHandler() {
        mHandler = new Handler() {
            @Override
            public void dispatchMessage(Message msg) {
                super.dispatchMessage(msg);
                switch (msg.what) {
                    case 10:
                        AppDelegate.showProgressDialog(SelectLocationActivity.this);
                        break;
                    case 11:
                        AppDelegate.hideProgressDialog(SelectLocationActivity.this);
                        break;
                    case 1:
                        if (cityModels != null && cityModels.size() > 0) {
                            AppDelegate.LogT("cityModels=");
                            txt_c_no_data.setVisibility(View.GONE);
                            cityAdapter = new CityAdapter(SelectLocationActivity.this, cityModels, SelectLocationActivity.this);
                            recycler_deals_list.setVisibility(View.VISIBLE);
                            recycler_deals_list.setAdapter(cityAdapter);
                            cityAdapter.notifyDataSetChanged();
                            recycler_deals_list.invalidate();
                            cityAdapter.filterSting(et_search.getText().toString());

                        } else {
                            txt_c_no_data.setVisibility(View.VISIBLE);
                            recycler_deals_list.setVisibility(View.GONE);
                        }
                        break;
                    case 2:

                        setAddressFromGEOcoder(msg.getData());
                        break;
                    case 3:
                        break;
                }
            }
        };
    }

    private void setloc(double latitude, double longitude) {
        mHandler.sendEmptyMessage(10);
        LocationAddress.getAddressFromLocation(latitude, longitude, this, mHandler);
    }

    private void setAddressFromGEOcoder(Bundle data) {
        if (data == null) {
            AppDelegate.showToast(SelectLocationActivity.this, getString(R.string.location_failed_select));
        } else {
            CityModel cityModel;
            if (new Prefs(this).getCitydata() != null)
                cityModel = new Prefs(this).getCitydata();
            else
                cityModel = new CityModel();
            cityModel.location_type = Tags.current_location;
            cityModel.name = data.getString(Tags.city_param);
            cityModel.lat = String.valueOf(data.getDouble(Tags.LAT)) + "";
            cityModel.lng = String.valueOf(data.getDouble(Tags.LNG)) + "";
            new Prefs(this).setCityData(cityModel);
            mHandler.sendEmptyMessage(11);
            DashBoardActivity.mHandler.sendEmptyMessage(4);
            onBackPressed();
        }
    }

    private void initView() {
        findViewById(R.id.img_c_back).setOnClickListener(this);
        et_search = (EditText) findViewById(R.id.et_search);
        et_search.addTextChangedListener(new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence s, int start, int count, int after) {
            }

            @Override
            public void onTextChanged(CharSequence s, int start, int before, int count) {
            }

            @Override
            public void afterTextChanged(Editable s) {
                mHandler.sendEmptyMessage(1);
            }
        });
        et_search.setTypeface(Typeface.createFromAsset(getAssets(), getString(R.string.font_regular)));
        recycler_deals_list = (RecyclerView) findViewById(R.id.recycler_deals_list);
        recycler_deals_list.setLayoutManager(new LinearLayoutManager(this));
        txt_c_no_data = (TextView) findViewById(R.id.txt_c_no_data);
        findViewById(R.id.txt_c_location).setOnClickListener(this);
    }

    @Override
    public void onClick(View v) {
        switch (v.getId()) {
            case R.id.img_c_back:
                onBackPressed();
                break;
            case R.id.txt_c_location:
                isClicked = true;
                showGPSalert();
                break;
        }
    }


    private void showGPSalert() {
        if (mGoogleApiClient == null) {
            mGoogleApiClient = new GoogleApiClient.Builder(SelectLocationActivity.this)
                    .addApi(LocationServices.API)
                    .addConnectionCallbacks(this)
                    .addOnConnectionFailedListener(this).build();
            mGoogleApiClient.connect();
        }

        LocationSettingsRequest.Builder builder = new LocationSettingsRequest.Builder().addLocationRequest(mLocationRequest);
        //**************
        builder.setAlwaysShow(true); //this is the key ingredient
        //**************************
        PendingResult<LocationSettingsResult> result =
                LocationServices.SettingsApi.checkLocationSettings(mGoogleApiClient, builder.build());
        result.setResultCallback(new ResultCallback<LocationSettingsResult>() {
            @Override
            public void onResult(LocationSettingsResult result) {
                final Status status = result.getStatus();
                final LocationSettingsStates state = result.getLocationSettingsStates();
                AppDelegate.LogGP("status.getStatusCode() => " + status.getStatusCode());
                switch (status.getStatusCode()) {
                    case LocationSettingsStatusCodes.SUCCESS:
                        // All location settings are satisfied. The client can initialize location
                        // requests here.
                        AppDelegate.LogGP("GPS SUCCESS ");

                        Location location = LocationServices.FusedLocationApi.getLastLocation(mGoogleApiClient);
                        AppDelegate.LogT("onConnected Initialited");
                        if (location == null) {
                            try {
                                AppDelegate.LogT("onConnected Initialited== null");
                                LocationServices.FusedLocationApi.requestLocationUpdates(mGoogleApiClient, mLocationRequest, SelectLocationActivity.this);
                            } catch (Exception e) {
                                AppDelegate.LogE(e);
                            }
                        } else {
                            currentLatitude = location.getLatitude();
                            currentLongitude = location.getLongitude();
                            setloc(currentLatitude, currentLongitude);
                            AppDelegate.LogT("latLng = " + currentLatitude + ", " + currentLongitude);
                        }

                        break;
                    case LocationSettingsStatusCodes.RESOLUTION_REQUIRED:
                        // Location settings are not satisfied. But could be fixed by showing the user
                        // a dialog.
                        AppDelegate.LogGP("GPS RESOLUTION_REQUIRED ");
                        try {
                            // Show the dialog by calling startResolutionForResult(),
                            // and check the result in onActivityResult().
                            status.startResolutionForResult(
                                    SelectLocationActivity.this, 1000);
                        } catch (IntentSender.SendIntentException e) {
                            // Ignore the error.
                        }
                        break;
                    case LocationSettingsStatusCodes.SETTINGS_CHANGE_UNAVAILABLE:
                        // Location settings are not satisfied. However, we have no way to fix the
                        // settings so we won't show the dialog.
                        AppDelegate.LogGP("GPS SETTINGS_CHANGE_UNAVAILABLE ");
                        break;

                    case CommonStatusCodes.CANCELED:
                        AppDelegate.LogGP("GPS CANCELED ");
                        break;
                }
            }
        });
    }

    private void execute_getCityList() {
        try {
            if (AppDelegate.haveNetworkConnection(SelectLocationActivity.this, true)) {
                ArrayList<PostAysnc_Model> mPostArrayList = new ArrayList<PostAysnc_Model>();
                AppDelegate.getInstance(SelectLocationActivity.this).setPostParamsSecond(mPostArrayList, Tags.API_KEY, Tags.API_KEY_VALUE);
                PostAsync mPostAsyncObj;
                mPostAsyncObj = new PostAsync(SelectLocationActivity.this, this, ServerRequestConstants.CITY_LIST, mPostArrayList, null);
                AppDelegate.showProgressDialog(SelectLocationActivity.this);
                mPostAsyncObj.execute();
            }
        } catch (Exception e) {
            AppDelegate.showAlert(SelectLocationActivity.this, getString(R.string.try_again), "Alert!!!");
        }
    }

    @Override
    public void setOnReciveResult(String apiName, String result) {
        mHandler.sendEmptyMessage(11);
        if (!AppDelegate.isValidString(result)) {
            AppDelegate.showToast(SelectLocationActivity.this, getString(R.string.time_out));
        } else if (apiName.equalsIgnoreCase(ServerRequestConstants.CITY_LIST)) {
            parseCityList(result);
        }
    }

    private void parseCityList(String result) {
        try {
            JSONObject jsonObject = new JSONObject(result);
            if (jsonObject.getString(Tags.status).equalsIgnoreCase("1")) {
                JSONArray jsonArray = jsonObject.getJSONArray(Tags.response);
                for (int i = 0; i < jsonArray.length(); i++) {
                    JSONObject object = jsonArray.getJSONObject(i);
                    CityModel cityModel = new CityModel();
                    cityModel.id = String.valueOf(JSONParser.getInt(object, Tags.sta_id));
                    cityModel.name = JSONParser.getString(object, Tags.name);
                    cityModel.lat = String.valueOf(JSONParser.getDouble(object, Tags.latitude));
                    cityModel.lng = String.valueOf(JSONParser.getDouble(object, Tags.longitude));
                    cityModel.location_type = Tags.other;
                    cityModels.add(cityModel);
                }
                mHandler.sendEmptyMessage(1);
            } else {
                AppDelegate.showAlert(SelectLocationActivity.this, jsonObject.getString(Tags.message));
            }
        } catch (Exception e) {
            AppDelegate.LogE(e);
            AppDelegate.showToast(SelectLocationActivity.this, getString(R.string.try_again));
        }
    }

    @Override
    public void setOnListItemClickListener(String name, int position, CityModel cityModel) {
        if (name.equalsIgnoreCase(Tags.city_name)) {
            AppDelegate.LogT("cityModel==" + cityModel);
            new Prefs(this).setCityData(cityModel);
            DashBoardActivity.mHandler.sendEmptyMessage(4);
            finish();
        }
    }

    @Override
    public void onConnected(@Nullable Bundle bundle) {
        Location location = LocationServices.FusedLocationApi.getLastLocation(mGoogleApiClient);
        AppDelegate.LogT("onConnected Initialited");
        if (location == null) {
            try {
                AppDelegate.LogT("onConnected Initialited== null");
                LocationServices.FusedLocationApi.requestLocationUpdates(mGoogleApiClient, mLocationRequest, SelectLocationActivity.this);
            } catch (Exception e) {
                AppDelegate.LogE(e);
            }
        } else {
            currentLatitude = location.getLatitude();
            currentLongitude = location.getLongitude();

            AppDelegate.LogT("latLng = " + currentLatitude + ", " + currentLongitude);
        }
    }

    @Override
    public void onConnectionSuspended(int i) {

    }

    @Override
    public void onConnectionFailed(@NonNull ConnectionResult connectionResult) {
        if (connectionResult.hasResolution()) {
            try {
                AppDelegate.LogT("onConnectionFailed Initialited" + connectionResult);
                connectionResult.startResolutionForResult(this, CONNECTION_FAILURE_RESOLUTION_REQUEST);

            } catch (IntentSender.SendIntentException e) {
                AppDelegate.LogE(e);
            }
        } else {
            Log.e("Error", "Location services connection failed with code " + connectionResult.getErrorCode());
        }
    }

    @Override
    protected void onResume() {
        super.onResume();
        AppDelegate.hideKeyBoard(this);
        mGoogleApiClient.connect();
    }

    @Override
    public void onLocationChanged(Location location) {
        if (location != null && location.getLatitude() != 0.0 && location.getLongitude() != 0.0) {
            currentLatitude = location.getLatitude();
            currentLongitude = location.getLongitude();
            try {
                if (isClicked) {
                    setloc(currentLatitude, currentLongitude);
                    if (mGoogleApiClient.isConnected()) {
                        LocationServices.FusedLocationApi.removeLocationUpdates(mGoogleApiClient, SelectLocationActivity.this);
                        mGoogleApiClient.disconnect();
                        AppDelegate.LogGP("Fused Location api disconnect called");
                    }
                }
            } catch (Exception e) {
                AppDelegate.LogE(e);
            }
        }
    }
}
