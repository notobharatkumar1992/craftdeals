package com.craftedeals.activities;

import android.content.Intent;
import android.graphics.drawable.Drawable;
import android.os.Build;
import android.os.Bundle;
import android.os.Handler;
import android.os.Message;
import android.support.v4.app.Fragment;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.DefaultItemAnimator;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.View;
import android.view.Window;
import android.view.WindowManager;
import android.widget.LinearLayout;

import com.craftedeals.AppDelegate;
import com.craftedeals.Async.PostAsync;
import com.craftedeals.Models.AdHocDealsModel;
import com.craftedeals.Models.CategoryModel;
import com.craftedeals.Models.PostAysnc_Model;
import com.craftedeals.R;
import com.craftedeals.Utils.Prefs;
import com.craftedeals.adapters.MyFavouriteAdapter;
import com.craftedeals.constants.ServerRequestConstants;
import com.craftedeals.constants.Tags;
import com.craftedeals.interfaces.OnListItemClickListener;
import com.craftedeals.interfaces.OnReciveServerResponse;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;

import carbon.widget.TextView;

/**
 * Created by Heena on 07-Oct-16.
 */
public class MyFavouriteDealsActivity extends AppCompatActivity implements View.OnClickListener, OnReciveServerResponse, OnListItemClickListener {

    private Prefs prefs;
    private TextView txt_c_title, txt_c_all, txt_c_active;
    public Handler mHandler;
    public static final int LABEL_ACTIVE = 1, LABEL_ALL = 2;
    private RecyclerView recycler_deals_list;
    LinearLayout ll_options;
    private ArrayList<AdHocDealsModel> adHocDealsModelArrayList = new ArrayList<>();
    private AdHocDealsModel adHocDealModel;
    boolean ifExists = false;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.LOLLIPOP) {
            Window window = this.getWindow();
            Drawable background = this.getResources().getDrawable(R.drawable.topbg);
            window.addFlags(WindowManager.LayoutParams.FLAG_DRAWS_SYSTEM_BAR_BACKGROUNDS);
            window.setStatusBarColor(getResources().getColor(android.R.color.transparent));
            window.setNavigationBarColor(getResources().getColor(android.R.color.transparent));
            window.setBackgroundDrawable(background);
        }
        setContentView(R.layout.my_purchase);
        prefs = new Prefs(this);
        initView();
        setHandler();
        mHandler.sendEmptyMessage(3);
    }

    private void initView() {
        findViewById(R.id.img_back).setOnClickListener(this);
        txt_c_title = (TextView) findViewById(R.id.txt_c_title);
        findViewById(R.id.img_search).setOnClickListener(this);
        txt_c_active = (TextView) findViewById(R.id.txt_c_active);
        txt_c_active.setOnClickListener(this);
        txt_c_all = (TextView) findViewById(R.id.txt_c_all);
        txt_c_all.setOnClickListener(this);
        recycler_deals_list = (RecyclerView) findViewById(R.id.recycler_deals_list);
        ll_options = (LinearLayout) findViewById(R.id.ll_options);
        ll_options.setVisibility(View.GONE);
        setselection(LABEL_ACTIVE);
        txt_c_title.setText("My Favourites");
    }

    void setselection(int position) {
        txt_c_active.setSelected(false);
        txt_c_all.setSelected(false);
        switch (position) {
            case LABEL_ACTIVE:
                txt_c_active.setSelected(true);
                break;
            case LABEL_ALL:
                txt_c_all.setSelected(true);
                break;
        }
    }


    private void displayView(int position) {
        AppDelegate.hideKeyBoard(this);
        setselection(position);
        Fragment fragment = null;
        switch (position) {
            case LABEL_ACTIVE:
                mHandler.sendEmptyMessage(4);
                break;
            case LABEL_ALL:
                mHandler.sendEmptyMessage(5);
                break;
            default:
                break;
        }
    }


    private void show_AllDealsList(ArrayList<AdHocDealsModel> heroDealsModelArrayList) {
        AppDelegate.LogT("show_AllDealsList");
        MyFavouriteAdapter mAdapter = new MyFavouriteAdapter(this, heroDealsModelArrayList, this);
        RecyclerView.LayoutManager mLayoutManager = new LinearLayoutManager(this);
        recycler_deals_list.setLayoutManager(mLayoutManager);
        recycler_deals_list.setItemAnimator(new DefaultItemAnimator());
        recycler_deals_list.setAdapter(mAdapter);

        if (heroDealsModelArrayList != null && heroDealsModelArrayList.size() > 0) {
            findViewById(R.id.ll_no_item).setVisibility(View.GONE);
            recycler_deals_list.setVisibility(View.VISIBLE);
        } else {
            findViewById(R.id.ll_no_item).setVisibility(View.VISIBLE);
            recycler_deals_list.setVisibility(View.GONE);
        }
    }

    private void setHandler() {
        mHandler = new Handler() {
            @Override
            public void dispatchMessage(Message msg) {
                super.dispatchMessage(msg);
                switch (msg.what) {
                    case 10:
                        AppDelegate.showProgressDialog(MyFavouriteDealsActivity.this);
                        break;
                    case 11:
                        AppDelegate.hideProgressDialog(MyFavouriteDealsActivity.this);
                        break;
                    case 4:
                        if (adHocDealsModelArrayList != null && adHocDealsModelArrayList.size() > 0) {
                            findViewById(R.id.ll_no_item).setVisibility(View.GONE);
                        } else {
                            findViewById(R.id.ll_no_item).setVisibility(View.VISIBLE);
                        }
                        break;
                    case 5:
                        if (adHocDealsModelArrayList != null)
                            show_AllDealsList(adHocDealsModelArrayList);
                        break;
                    case 3:
                        execute_MyFavouriteDealsAPI();
                        break;
                    case 9:
                        if (adHocDealsModelArrayList != null && adHocDealsModelArrayList.size() > 0) {
                            findViewById(R.id.ll_no_item).setVisibility(View.GONE);
                        } else {
                            findViewById(R.id.ll_no_item).setVisibility(View.VISIBLE);
                        }
                        break;

                }
            }
        };
    }

    @Override
    protected void onResume() {
        super.onResume();
        mHandler.sendEmptyMessage(9);
    }

    @Override
    public void onClick(View v) {
        switch (v.getId()) {
            case R.id.img_back:
                onBackPressed();
                break;
            case R.id.img_search:
                startActivity(new Intent(this, SearchActivity.class));
                break;
            case R.id.txt_c_active:
                displayView(LABEL_ACTIVE);
                break;
            case R.id.txt_c_all:
                displayView(LABEL_ALL);
                break;
            case R.id.list_container:
                break;
            case R.id.map_container:
                break;
            case R.id.txt_c_viewdeals:
                break;
        }
    }

    @Override
    public void onBackPressed() {
        super.onBackPressed();
        super.onBackPressed();
    }


    @Override
    public void setOnReciveResult(String apiName, String result) {
        mHandler.sendEmptyMessage(11);
        if (!AppDelegate.isValidString(result)) {
            AppDelegate.showToast(MyFavouriteDealsActivity.this, getResources().getString(R.string.time_out));
            recycler_deals_list.setAdapter(null);
            return;
        }
        if (apiName.equals(ServerRequestConstants.MY_FAVOURITE_DEALS)) {
            recycler_deals_list.setAdapter(null);
            parseAdHocDeals(result);
        }
    }

    private void execute_MyFavouriteDealsAPI() {
        try {
            if (AppDelegate.haveNetworkConnection(this, true)) {
                ArrayList<PostAysnc_Model> mPostArrayList = new ArrayList<PostAysnc_Model>();
                AppDelegate.getInstance(this).setPostParamsSecond(mPostArrayList, Tags.API_KEY, Tags.API_KEY_VALUE);
                if (new Prefs(this).getCitydata() != null) {
                    AppDelegate.getInstance(this).setPostParamsSecond(mPostArrayList, Tags.LAT, new Prefs(this).getCitydata().lat);
                    AppDelegate.getInstance(this).setPostParamsSecond(mPostArrayList, Tags.LONG, new Prefs(this).getCitydata().lng);
                }
                if (new Prefs(this).getUserdata() != null)
                    AppDelegate.getInstance(this).setPostParamsSecond(mPostArrayList, Tags.userId, (new Prefs(this).getUserdata().id), ServerRequestConstants.Key_PostintValue);
                PostAsync mPostAsyncObj;
                mPostAsyncObj = new PostAsync(this, this, ServerRequestConstants.MY_FAVOURITE_DEALS,
                        mPostArrayList, null);
                mHandler.sendEmptyMessage(10);
                mPostAsyncObj.execute();
            }
        } catch (Exception e) {
            AppDelegate.showToast(this, getResources().getString(R.string.try_again));
            AppDelegate.LogE(e);
        }
    }

    private void parseAdHocDeals(String result) {
        try {
            JSONObject jsonobj = new JSONObject(result);
            if (jsonobj.getInt(Tags.status) == 1 && jsonobj.getInt(Tags.dataFlow) == 1) {
                CategoryModel category = new CategoryModel();
                JSONArray array = jsonobj.getJSONArray(Tags.response);
                adHocDealsModelArrayList.clear();
                for (int i = 0; i < array.length(); i++) {
                    adHocDealModel = new AdHocDealsModel();
                    JSONObject object = array.getJSONObject(i);
                    adHocDealModel.distance = object.optString(Tags.distance);
                    adHocDealModel.id = object.getInt(Tags.id);
                    adHocDealModel.user_id = object.getInt(Tags.user_id);
                    adHocDealModel.id = object.getInt(Tags.adhocdeal_id);
                    adHocDealModel.distance = object.optString(Tags.distance);
                    JSONObject heroobj = object.getJSONObject("adhocdeal");
                    adHocDealModel.user_id = heroobj.getInt(Tags.user_id);
                    adHocDealModel.name = heroobj.getString(Tags.name);
                    adHocDealModel.deal_status = heroobj.getInt(Tags.deal_type);
                    adHocDealModel.use_the_offer = heroobj.getString(Tags.use_the_offer);
                    adHocDealModel.things_to_remebers = heroobj.getString(Tags.things_to_remebers);
                    adHocDealModel.deal_category_id = heroobj.getInt(Tags.deal_category_id);
                    adHocDealModel.sub_category_id = heroobj.getInt(Tags.sub_category_id);
                    adHocDealModel.facilities = heroobj.getString(Tags.facilities);
                    adHocDealModel.discount_cost = heroobj.getInt(Tags.discount_cost);
                    adHocDealModel.item_detail = heroobj.getString(Tags.item_detail);
                    adHocDealModel.deal_type = heroobj.getInt(Tags.deal_type);
                    adHocDealModel.starting_date = heroobj.getString(Tags.starting_date);
                    adHocDealModel.expiry_date = heroobj.getString(Tags.expiry_date);
                    adHocDealModel.price = heroobj.getInt(Tags.price);
                    adHocDealModel.total_deal = heroobj.getInt(Tags.total_deal);
                    adHocDealModel.volume_of_deal = heroobj.getInt(Tags.volume_of_deal);
                    adHocDealModel.timing_to = heroobj.getString(Tags.timing_to);
                    adHocDealModel.timing_from = heroobj.getString(Tags.timing_from);
                    adHocDealModel.valid_on = heroobj.getString(Tags.valid_on);
                    adHocDealModel.vendor_delete_deal = heroobj.getInt(Tags.vendor_delete_deal);
                    adHocDealModel.favourite = heroobj.getInt(Tags.favourite);
                    adHocDealModel.liked = 1;
                    if (AppDelegate.isValidString(heroobj.getString(Tags.user))) {
                        JSONObject vendor = heroobj.getJSONObject(Tags.user);
                        adHocDealModel.vendor_name = vendor.getString(Tags.name);
                        if (vendor.has(Tags.busines) && AppDelegate.isValidString(vendor.getString(Tags.busines))) {
                            //  JSONObject business = vendor.getJSONObject(Tags.busines);
                            adHocDealModel.company_name = vendor.getJSONObject(Tags.busines).getString(Tags.name);
                            adHocDealModel.latitude = vendor.getJSONObject(Tags.busines).getString(Tags.latitude);
                            adHocDealModel.longitude = vendor.getJSONObject(Tags.busines).getString(Tags.longitude);
                            adHocDealModel.company_address = vendor.getJSONObject(Tags.busines).getString(Tags.location_address);
                            adHocDealModel.cityName = vendor.getJSONObject(Tags.busines).optString(Tags.cityName);
                        }
                    }
                    try {
                        if (AppDelegate.isValidString(heroobj.getString(Tags.images))) {
                            JSONArray img = heroobj.getJSONArray(Tags.images);
                            for (int k = 0; k < img.length(); k++) {
                                JSONObject json = img.getJSONObject(k);
                                adHocDealModel.image = json.getString(Tags.image);
//                            adHocDealModel.adhocdeal_id = json.getInt(Tags.adhocdeal_id);
                            }
                        }
                    } catch (JSONException e) {
                        AppDelegate.LogE(e);
                    }
                    if (heroobj.has(Tags.image))
                        adHocDealModel.image = heroobj.getString(Tags.image);
                    JSONObject obj = heroobj.getJSONObject(Tags.deal_category);
                    category = new CategoryModel();
                    category.id = obj.getInt(Tags.id);
                    category.title = obj.getString(Tags.title);
                    category.description = obj.getString(Tags.description);
                    category.image = obj.getString(Tags.image);
                    adHocDealModel.category = category.title;
                    if (adHocDealsModelArrayList != null && adHocDealsModelArrayList.size() > 0) {
                        for (int l = 0; l < adHocDealsModelArrayList.size(); l++) {
                            if (adHocDealModel.id != adHocDealsModelArrayList.get(l).id) {
                                ifExists = false;
                            } else {
                                ifExists = true;
                                break;
                            }
                        }
                        if (!ifExists)
                            adHocDealsModelArrayList.add(adHocDealModel);

                    } else {
                        adHocDealsModelArrayList.add(adHocDealModel);
                    }

                    show_AllDealsList(adHocDealsModelArrayList);
                    //  mHandler.sendEmptyMessage(4);
                }
                AppDelegate.LogT("adHocDealModelArrayList==" + adHocDealsModelArrayList);
            } else {
//                AppDelegate.showToast(this,"You don't have any favourite." /*jsonobj.getString(Tags.message)*/);
                mHandler.sendEmptyMessage(4);
            }
        } catch (JSONException e) {
            AppDelegate.LogE(e);
        }
    }

    @Override
    public void setOnListItemClickListener(String name, int position) {
        if (name.equalsIgnoreCase("check")) {
            recycler_deals_list.setVisibility(View.GONE);
            findViewById(R.id.ll_no_item).setVisibility(View.VISIBLE);
        }
    }

    @Override
    public void setOnListItemClickListener(String name, int position, boolean like_dislike) {

    }
}
