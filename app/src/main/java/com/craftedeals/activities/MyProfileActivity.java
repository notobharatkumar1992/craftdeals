package com.craftedeals.activities;

import android.content.Intent;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.graphics.drawable.AnimationDrawable;
import android.graphics.drawable.Drawable;
import android.os.Build;
import android.os.Bundle;
import android.os.Handler;
import android.os.Message;
import android.support.v7.app.AppCompatActivity;
import android.view.View;
import android.view.Window;
import android.view.WindowManager;

import com.craftedeals.AppDelegate;
import com.craftedeals.Async.PostAsync;
import com.craftedeals.Models.PostAysnc_Model;
import com.craftedeals.Models.UserDataModel;
import com.craftedeals.R;
import com.craftedeals.Utils.CircleImageView;
import com.craftedeals.Utils.Prefs;
import com.craftedeals.constants.ServerRequestConstants;
import com.craftedeals.constants.Tags;
import com.craftedeals.interfaces.OnReciveServerResponse;
import com.craftedeals.parser.JSONParser;
import com.nostra13.universalimageloader.core.DisplayImageOptions;
import com.nostra13.universalimageloader.core.ImageLoaderConfiguration;
import com.nostra13.universalimageloader.core.assist.FailReason;
import com.nostra13.universalimageloader.core.listener.ImageLoadingListener;

import org.apache.commons.lang3.text.WordUtils;
import org.json.JSONObject;

import java.util.ArrayList;

import carbon.widget.ImageView;
import carbon.widget.TextView;

/**
 * Created by Heena on 24-Nov-16.
 */

public class MyProfileActivity extends AppCompatActivity implements View.OnClickListener, OnReciveServerResponse {
    private Prefs prefs;
    public Handler mHandler;
    ImageView img_user_bg, img_c_loading1;
    CircleImageView cimg_user;
    TextView txt_c_name, txt_c_address, txt_c_total_purchase, txt_c_total_redeemed, txt_c_favourite, txt_c_email, txt_c_phone, txt_c_fav_brewery, txt_c_beer;
    public com.nostra13.universalimageloader.core.ImageLoader imageLoader = com.nostra13.universalimageloader.core.ImageLoader.getInstance();
    public DisplayImageOptions options = new DisplayImageOptions.Builder().cacheInMemory(true)
            .cacheOnDisc(true).resetViewBeforeLoading(true).
                    build();
    private UserDataModel userDataModel;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.LOLLIPOP) {
            Window window = this.getWindow();
            Drawable background = this.getResources().getDrawable(R.drawable.topbg);
            window.addFlags(WindowManager.LayoutParams.FLAG_DRAWS_SYSTEM_BAR_BACKGROUNDS);
            window.setStatusBarColor(getResources().getColor(android.R.color.transparent));
            window.setNavigationBarColor(getResources().getColor(android.R.color.transparent));
            window.setBackgroundDrawable(background);
        }
        setContentView(R.layout.profile_activity);
        userDataModel = new Prefs(this).getUserdata();
        imageLoader.init(ImageLoaderConfiguration.createDefault(this));
        prefs = new Prefs(this);
        initView();
        setHandler();
        mHandler.sendEmptyMessage(5);
    }

    private void execute_getMyProfile() {
        try {
            if (AppDelegate.haveNetworkConnection(MyProfileActivity.this, true)) {
                ArrayList<PostAysnc_Model> mPostArrayList = new ArrayList<PostAysnc_Model>();
                AppDelegate.getInstance(MyProfileActivity.this).setPostParamsSecond(mPostArrayList, Tags.API_KEY, Tags.API_KEY_VALUE);
                AppDelegate.getInstance(MyProfileActivity.this).setPostParamsSecond(mPostArrayList, Tags.user_id, new Prefs(this).getUserdata().id, ServerRequestConstants.Key_PostintValue);

                PostAsync mPostAsyncObj;
                mPostAsyncObj = new PostAsync(MyProfileActivity.this, this, ServerRequestConstants.GET_MYPROFILE, mPostArrayList, null);
                AppDelegate.showProgressDialog(MyProfileActivity.this);
                mPostAsyncObj.execute();
            }
        } catch (Exception e) {
            AppDelegate.showToast(MyProfileActivity.this, getResources().getString(R.string.try_again));
        }
    }

    private void setHandler() {
        mHandler = new Handler() {
            @Override
            public void dispatchMessage(Message msg) {
                super.dispatchMessage(msg);
                switch (msg.what) {
                    case 10:
                        AppDelegate.showProgressDialog(MyProfileActivity.this);
                        break;
                    case 11:
                        AppDelegate.hideProgressDialog(MyProfileActivity.this);
                        break;
                    case 4:
                        setValues();
                        break;
                    case 5:
                        execute_getMyProfile();
                        break;
                    case 3:
                        break;
                }
            }
        };
    }

    void setValues() {
        AppDelegate.LogT("setValues called");
        if (userDataModel != null && AppDelegate.isValidString(userDataModel.image)) {
            img_c_loading1.setVisibility(View.VISIBLE);
            AnimationDrawable frameAnimation = (AnimationDrawable) img_c_loading1.getDrawable();
            frameAnimation.setCallback(img_c_loading1);
            frameAnimation.setVisible(true, true);
            frameAnimation.start();

            imageLoader.loadImage(new Prefs(this).getUserdata().image, options, new ImageLoadingListener() {
                @Override
                public void onLoadingStarted(String imageUri, View view) {
                }

                @Override
                public void onLoadingFailed(String imageUri, View view, FailReason failReason) {
                    img_c_loading1.setVisibility(View.GONE);
                    cimg_user.setImageDrawable(getResources().getDrawable(R.drawable.logo));
                }

                @Override
                public void onLoadingComplete(String imageUri, View view, Bitmap bitmap) {
                    cimg_user.setImageBitmap(bitmap);
                    img_c_loading1.setVisibility(View.GONE);
                    try {
                        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.JELLY_BEAN_MR1) {
                            img_user_bg.setImageBitmap(AppDelegate.blurRenderScript(MyProfileActivity.this, bitmap));
                        }
                    } catch (Exception e) {
                        AppDelegate.LogE(e);
                    }
                }

                @Override
                public void onLoadingCancelled(String imageUri, View view) {
                    img_c_loading1.setVisibility(View.GONE);
                }
            });
        } else {
            cimg_user.setImageDrawable(getResources().getDrawable(R.drawable.logo));
            try {
                if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.JELLY_BEAN_MR1) {
                    img_user_bg.setImageBitmap(AppDelegate.blurRenderScript(MyProfileActivity.this, BitmapFactory.decodeResource(getResources(), R.drawable.logo)));
                }
            } catch (Exception e) {
                AppDelegate.LogE(e);
            }
        }
        if (userDataModel != null) {
            txt_c_name.setText(WordUtils.capitalize(userDataModel.name) + " " + (AppDelegate.isValidString(userDataModel.last_names) ? userDataModel.last_names : ""));
            if (AppDelegate.isValidString(userDataModel.province) && AppDelegate.isValidString(userDataModel.city))
                txt_c_address.setText(userDataModel.city + ", " + userDataModel.province);
            txt_c_total_purchase.setText(userDataModel.total_purchase + "");
            txt_c_total_redeemed.setText(userDataModel.total_redeemed + "");
            txt_c_favourite.setText(userDataModel.favourite + "");
            txt_c_email.setText(userDataModel.email);
            txt_c_phone.setText(userDataModel.contact_number);
            txt_c_fav_brewery.setText(userDataModel.fav_brewery);
            txt_c_beer.setText(userDataModel.fav_beer);
        }
    }

    @Override
    protected void onResume() {
        super.onResume();
        userDataModel = new Prefs(this).getUserdata();
        mHandler.sendEmptyMessage(4);

    }

    private void initView() {
        findViewById(R.id.img_c_back).setOnClickListener(this);
        findViewById(R.id.img_c_edit).setOnClickListener(this);
        img_user_bg = (ImageView) findViewById(R.id.img_user_bg);
        img_c_loading1 = (ImageView) findViewById(R.id.img_c_loading1);
        cimg_user = (CircleImageView) findViewById(R.id.cimg_user);
        txt_c_name = (TextView) findViewById(R.id.txt_c_name);
        txt_c_address = (TextView) findViewById(R.id.txt_c_address);
        txt_c_total_purchase = (TextView) findViewById(R.id.txt_c_total_purchase);
        txt_c_total_redeemed = (TextView) findViewById(R.id.txt_c_total_redeemed);
        txt_c_favourite = (TextView) findViewById(R.id.txt_c_favourite);
        txt_c_email = (TextView) findViewById(R.id.txt_c_email);
        txt_c_phone = (TextView) findViewById(R.id.txt_c_phone);
        txt_c_fav_brewery = (TextView) findViewById(R.id.txt_c_fav_brewery);
        txt_c_beer = (TextView) findViewById(R.id.txt_c_beer);
    }


    @Override
    public void onClick(View v) {
        switch (v.getId()) {
            case R.id.img_c_back:
                finish();
                break;
            case R.id.img_c_edit:
                startActivity(new Intent(this, EditProfileActivity.class));
                break;
        }
    }

    @Override
    public void setOnReciveResult(String apiName, String result) {
        mHandler.sendEmptyMessage(11);
        if (!AppDelegate.isValidString(result)) {
            AppDelegate.showToast(this, getResources().getString(R.string.time_out));
            return;
        }
        if (apiName.equalsIgnoreCase(ServerRequestConstants.GET_MYPROFILE)) {
            parseProfile(result);
        }
    }

    private void parseProfile(String result) {
        try {
            JSONObject jsonObject = new JSONObject(result);
            if (jsonObject.getString(Tags.status).equalsIgnoreCase("1")) {
                //  AppDelegate.showToast(MyProfileActivity.this, jsonObject.getString(Tags.message));
                JSONObject object = jsonObject.getJSONObject(Tags.response);
                userDataModel = new UserDataModel();
                userDataModel.id = JSONParser.getInt(object, Tags.id);
                userDataModel.name = JSONParser.getString(object, Tags.name);
                userDataModel.first_names = JSONParser.getString(object, Tags.name);
                userDataModel.last_names = JSONParser.getString(object, Tags.last_name);
                userDataModel.email = JSONParser.getString(object, Tags.email);
                userDataModel.contact_number = JSONParser.getString(object, Tags.contact_number);
                userDataModel.membership_id = JSONParser.getInt(object, Tags.membership_id);
                userDataModel.image = JSONParser.getString(object, Tags.image);
                userDataModel.device_id = JSONParser.getString(object, Tags.device_id);
                userDataModel.device_type = JSONParser.getInt(object, Tags.device_type);
                userDataModel.created = JSONParser.getString(object, Tags.created);
                userDataModel.address = JSONParser.getString(object, Tags.address);
                userDataModel.total_purchase = JSONParser.getInt(object, Tags.total_purchase);
                userDataModel.total_redeemed = JSONParser.getInt(object, Tags.total_redeemed);
                userDataModel.favourite = JSONParser.getInt(object, Tags.favourite);
                userDataModel.social_id = JSONParser.getString(object, Tags.social_id);
                userDataModel.fav_brewery = JSONParser.getString(object, Tags.brewery_type_id);
                if (object.has(Tags.user_range)) {
                    userDataModel.user_range = JSONParser.getInt(object, Tags.user_range);
                }
                if (object.has(Tags.heard_about_us)) {
                    userDataModel.heard_about_us = JSONParser.getString(object, Tags.heard_about_us);
                }
                if (object.has(Tags.membership_start_date) && object.has(Tags.membership_end_date)) {
                    userDataModel.membership_start_date = JSONParser.getString(object, Tags.membership_start_date);
                    userDataModel.membership_end_date = JSONParser.getString(object, Tags.membership_end_date);
                }
                userDataModel.dob = JSONParser.getString(object, Tags.dob);
                if (AppDelegate.isValidString(object.getString(Tags.state))) {
                    JSONObject state = object.getJSONObject(Tags.state);
                    userDataModel.province = JSONParser.getString(state, Tags.region_name);
                    userDataModel.state_id = JSONParser.getInt(state, Tags.id) + "";
                }
                if (AppDelegate.isValidString(object.getString(Tags.city))) {
                    JSONObject city = object.getJSONObject(Tags.city);
                    userDataModel.city = JSONParser.getString(city, Tags.name);
                    userDataModel.city_id = JSONParser.getInt(city, Tags.id) + "";
                }
//                if (AppDelegate.isValidString(object.getString(Tags.beer_type))) {
//                    JSONObject beer = object.getJSONObject(Tags.beer_type);
//                    userDataModel.fav_beer = JSONParser.getString(beer, Tags.name);
//                    userDataModel.beer_id = JSONParser.getInt(beer, Tags.id);
//                }
//                if (AppDelegate.isValidString(object.getString(Tags.brewery_type))) {
//                    JSONObject brewery = object.getJSONObject(Tags.brewery_type);
//                    userDataModel.fav_brewery = JSONParser.getString(brewery, Tags.name);
//                    userDataModel.brewery_id = JSONParser.getInt(brewery, Tags.id);
//                }

                new Prefs(this).setUserData(userDataModel);
                mHandler.sendEmptyMessage(4);
            } else {
                AppDelegate.showAlert(MyProfileActivity.this, jsonObject.getString(Tags.message));
            }
        } catch (Exception e) {
            AppDelegate.LogE(e);
        }
    }

}
