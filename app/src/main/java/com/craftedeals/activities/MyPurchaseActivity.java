package com.craftedeals.activities;

import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.graphics.drawable.Drawable;
import android.os.Build;
import android.os.Bundle;
import android.os.Handler;
import android.os.Message;
import android.support.v4.app.Fragment;
import android.support.v7.app.AlertDialog;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.DefaultItemAnimator;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.View;
import android.view.Window;
import android.view.WindowManager;
import android.widget.LinearLayout;

import com.craftedeals.AppDelegate;
import com.craftedeals.Async.PostAsync;
import com.craftedeals.Models.AdHocDealsModel;
import com.craftedeals.Models.ItemsModel;
import com.craftedeals.Models.PostAysnc_Model;
import com.craftedeals.R;
import com.craftedeals.Utils.Prefs;
import com.craftedeals.adapters.MyPurchaseAdapter;
import com.craftedeals.constants.ServerRequestConstants;
import com.craftedeals.constants.Tags;
import com.craftedeals.interfaces.OnListItemClickListener;
import com.craftedeals.interfaces.OnReciveServerResponse;
import com.craftedeals.parser.JSONParser;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;

import carbon.widget.TextView;

import static com.craftedeals.AppDelegate.DISLIKE;
import static com.craftedeals.AppDelegate.LIKE;
import static com.craftedeals.activities.DealsActivity.DEAL_ADHOC;

/**
 * Created by Heena on 07-Oct-16.
 */
public class MyPurchaseActivity extends AppCompatActivity implements View.OnClickListener, OnReciveServerResponse, OnListItemClickListener {
    private Prefs prefs;
    private TextView txt_c_title, txt_c_all, txt_c_active, txt_c_viewdeals;
    public static Handler mHandler;
    public static final int LABEL_ACTIVE = 1, LABEL_ALL = 2;
    private RecyclerView recycler_deals_list;

    //    private MyPurchaseModel myPurchaseModel, cart_itemModel;

    LinearLayout ll_options;
    private MyPurchaseAdapter mAdapter;
    private int position = 0, selected_list = LABEL_ALL;

    public ArrayList<AdHocDealsModel> arrayAdhocDeals = new ArrayList<>();
    private boolean like_dislike = false;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.LOLLIPOP) {
            Window window = this.getWindow();
            Drawable background = this.getResources().getDrawable(R.drawable.topbg);
            window.addFlags(WindowManager.LayoutParams.FLAG_DRAWS_SYSTEM_BAR_BACKGROUNDS);
            window.setStatusBarColor(getResources().getColor(android.R.color.transparent));
            window.setNavigationBarColor(getResources().getColor(android.R.color.transparent));
            window.setBackgroundDrawable(background);
        }
        setContentView(R.layout.my_purchase);
        prefs = new Prefs(this);
        initView();
        setHandler();
        mHandler.sendEmptyMessage(3);
    }

    private void initView() {
        findViewById(R.id.img_back).setOnClickListener(this);
        txt_c_title = (TextView) findViewById(R.id.txt_c_title);
        findViewById(R.id.img_search).setOnClickListener(this);
        txt_c_active = (TextView) findViewById(R.id.txt_c_active);
        txt_c_active.setOnClickListener(this);
        txt_c_all = (TextView) findViewById(R.id.txt_c_all);
        txt_c_all.setOnClickListener(this);
        recycler_deals_list = (RecyclerView) findViewById(R.id.recycler_deals_list);
        recycler_deals_list.setLayoutManager(new LinearLayoutManager(this));
        recycler_deals_list.setHasFixedSize(true);
        recycler_deals_list.setItemAnimator(new DefaultItemAnimator());
        ll_options = (LinearLayout) findViewById(R.id.ll_options);
        txt_c_title.setText("My Purchase");
        setselection(LABEL_ALL);
    }


    private void setselection(int position) {
        txt_c_active.setSelected(false);
        txt_c_all.setSelected(false);
        switch (position) {
            case LABEL_ACTIVE:
                txt_c_active.setSelected(true);
                break;
            case LABEL_ALL:
                txt_c_all.setSelected(true);
                break;
        }
    }

    private void displayView(int position) {
        AppDelegate.hideKeyBoard(this);
        setselection(position);
        Fragment fragment = null;
        switch (position) {
            case LABEL_ACTIVE:
                mHandler.sendEmptyMessage(4);
                break;
            case LABEL_ALL:
                mHandler.sendEmptyMessage(5);
                break;
            default:
                break;
        }
    }

    private void setHandler() {
        mHandler = new Handler() {
            @Override
            public void dispatchMessage(Message msg) {
                super.dispatchMessage(msg);
                switch (msg.what) {
                    case 10:
                        AppDelegate.showProgressDialog(MyPurchaseActivity.this);
                        break;
                    case 11:
                        AppDelegate.hideProgressDialog(MyPurchaseActivity.this);
                        break;
                    case 1:
                        if (selected_list == LABEL_ALL) {
                            showMypurchaselist(arrayAdhocDeals, LABEL_ALL);
                        } else {
                            showMypurchaselist(arrayAdhocDeals, LABEL_ACTIVE);
                        }
                        break;
                    case 2:
                        break;
                    case 3:
                        execute_MyPurchaseDealsAPI();
                        break;

                }
            }
        };
    }

    private void execute_MyPurchaseDealsAPI() {
        try {
            if (AppDelegate.haveNetworkConnection(this, true)) {
                ArrayList<PostAysnc_Model> mPostArrayList = new ArrayList<PostAysnc_Model>();
                AppDelegate.getInstance(this).setPostParamsSecond(mPostArrayList, Tags.API_KEY, Tags.API_KEY_VALUE);
                if (new Prefs(this).getUserdata() != null)
                    AppDelegate.getInstance(this).setPostParamsSecond(mPostArrayList, Tags.user_id, (new Prefs(this).getUserdata().id), ServerRequestConstants.Key_PostintValue);
                if (new Prefs(this).getCitydata() != null) {
                    AppDelegate.getInstance(this).setPostParamsSecond(mPostArrayList, Tags.LAT, new Prefs(this).getCitydata().lat);
                    AppDelegate.getInstance(this).setPostParamsSecond(mPostArrayList, Tags.LONG, new Prefs(this).getCitydata().lng);
                }
                PostAsync mPostAsyncObj;
                mPostAsyncObj = new PostAsync(this, this, ServerRequestConstants.MY_PURCHASE,
                        mPostArrayList, null);
                mHandler.sendEmptyMessage(10);
                mPostAsyncObj.execute();
            }
        } catch (Exception e) {
            AppDelegate.showToast(this, getResources().getString(R.string.try_again));
            AppDelegate.LogE(e);
        }
    }


    @Override
    public void onClick(View v) {
        switch (v.getId()) {
            case R.id.img_back:
                onBackPressed();
                break;
            case R.id.img_search:
                startActivity(new Intent(this, SearchActivity.class));
                break;
            case R.id.txt_c_active:
                selected_list = LABEL_ACTIVE;
                displayView(LABEL_ACTIVE);
                showMypurchaselist(arrayAdhocDeals, LABEL_ACTIVE);
                break;
            case R.id.txt_c_all:
                selected_list = LABEL_ALL;
                displayView(LABEL_ALL);
                showMypurchaselist(arrayAdhocDeals, LABEL_ALL);
                break;
            case R.id.list_container:
                break;
            case R.id.map_container:
                break;
            case R.id.txt_c_viewdeals:
                break;
        }
    }

    @Override
    public void onBackPressed() {
        super.onBackPressed();
        super.onBackPressed();
    }

    @Override
    protected void onDestroy() {
        super.onDestroy();
        mHandler = null;
    }

    @Override
    public void setOnReciveResult(String apiName, String result) {
        if (mHandler == null) {
            return;
        }
        mHandler.sendEmptyMessage(11);
        if (!AppDelegate.isValidString(result)) {
            AppDelegate.showToast(MyPurchaseActivity.this, getResources().getString(R.string.time_out));
            recycler_deals_list.setAdapter(null);
            return;
        }
        if (apiName.equals(ServerRequestConstants.MY_PURCHASE)) {
            recycler_deals_list.setAdapter(null);
            parseMyPurchase(result);
        } else if (apiName.equals(ServerRequestConstants.REDEEM_ADHOC)) {
            parseRedeem(result);
        } else if (apiName.equalsIgnoreCase(ServerRequestConstants.ADHOC_LIKE)) {
            parseAdHocLikes(result);
        }
    }

    private void parseAdHocLikes(String result) {
        try {
            JSONObject jsonobj = new JSONObject(result);
            if (jsonobj.getInt(Tags.status) == 1 && jsonobj.getInt(Tags.dataFlow) == 1) {
                AppDelegate.showToast(MyPurchaseActivity.this, jsonobj.getString(Tags.response));
//                arrayAdhocDeals.get(position).liked = arrayAdhocDeals.get(position).liked == DISLIKE ? LIKE : DISLIKE;
                for (int i = 0; i < arrayAdhocDeals.size(); i++) {
                    if (arrayAdhocDeals.get(position).adhocdeal_id == arrayAdhocDeals.get(i).adhocdeal_id) {
                        AppDelegate.LogT(position + ", adhoc id => " + arrayAdhocDeals.get(position).adhocdeal_id + " = " + arrayAdhocDeals.get(i).adhocdeal_id + ", before = " + arrayAdhocDeals.get(position).liked);
                        arrayAdhocDeals.get(i).liked = arrayAdhocDeals.get(i).liked == DISLIKE ? LIKE : DISLIKE;
                        AppDelegate.LogT(position + ", adhoc id => " + arrayAdhocDeals.get(position).adhocdeal_id + " = " + arrayAdhocDeals.get(i).adhocdeal_id + ", after = " + arrayAdhocDeals.get(position).liked);
                    }
                }
                mHandler.sendEmptyMessage(1);
            } else {
                AppDelegate.showToast(MyPurchaseActivity.this, jsonobj.getString(Tags.message));
            }
        } catch (JSONException e) {
            AppDelegate.LogE(e);
        }
    }

    private void parseRedeem(String result) {
        try {
            JSONObject jsonobj = new JSONObject(result);
            if (jsonobj.getInt(Tags.status) == 1 && jsonobj.getInt(Tags.dataFlow) == 1) {
                AppDelegate.showToast(this, jsonobj.getString(Tags.message));
                arrayAdhocDeals.get(position).coupon_status = 1;
                mHandler.sendEmptyMessage(1);
                AppDelegate.trackRedeemDeal(arrayAdhocDeals.get(position).name, DEAL_ADHOC, "My Purchase");
                Intent intent = new Intent(this, CongratulationActivity.class);
                intent.putExtra(Tags.deal, arrayAdhocDeals.get(position).company_name);
                intent.putExtra(Tags.deal_type, DEAL_ADHOC);
                intent.putExtra(Tags.types, CongratulationActivity.DEAL_REDEEMED);
                startActivity(intent);
            } else {
                AppDelegate.showAlert(this, jsonobj.getString(Tags.message));
            }
        } catch (JSONException e) {
            AppDelegate.LogE(e);
        }
    }

    private void parseMyPurchase(String result) {
        try {
            JSONObject jsonobj = new JSONObject(result);
            if (jsonobj.getInt(Tags.status) == 1) {
                JSONArray array = jsonobj.getJSONArray(Tags.data);
                AppDelegate.LogT("dat is==" + array);
                arrayAdhocDeals.clear();
                for (int i = 0; i < array.length(); i++) {
                    JSONObject object = array.getJSONObject(i);
                    AdHocDealsModel adHocDealModel = new AdHocDealsModel();
                    adHocDealModel.purchase_id = JSONParser.getString(object, Tags.id);
                    adHocDealModel.payment_id = JSONParser.getString(object, Tags.payment_id);
                    adHocDealModel.coupon_id = JSONParser.getString(object, Tags.coupon_id);
                    adHocDealModel.coupon_name = JSONParser.getString(object.getJSONObject("coupon"), Tags.name);
                    adHocDealModel.coupon_status = JSONParser.getInt(object.getJSONObject("coupon"), Tags.status);
                    adHocDealModel.coupon_created = JSONParser.getString(object.getJSONObject("coupon"), Tags.created);
                    adHocDealModel.coupon_modified = JSONParser.getString(object.getJSONObject("coupon"), Tags.modified);
                    AppDelegate.LogT("adHocDealModel.coupon_created==" + adHocDealModel.coupon_created);
                    AppDelegate.LogT("adHocDealModel.coupon_modified==" + adHocDealModel.coupon_modified);
                    adHocDealModel.total_amount = JSONParser.getString(object, Tags.total_amount);
                    AppDelegate.LogT("adHocDealModel.total_amount==" + adHocDealModel.total_amount);
                    JSONArray cart_items_array = object.getJSONArray(Tags.cart_items);

                    JSONObject heroobj = object.getJSONObject("adhocdeal");

                    adHocDealModel.distance = heroobj.optString(Tags.distance);

                    adHocDealModel.adhocdeal_id = heroobj.getInt(Tags.id);
                    adHocDealModel.user_id = heroobj.getInt(Tags.user_id);
                    adHocDealModel.name = heroobj.getString(Tags.name);
                    adHocDealModel.use_the_offer = heroobj.getString(Tags.use_the_offer);
                    adHocDealModel.things_to_remebers = heroobj.getString(Tags.things_to_remebers);
                    adHocDealModel.deal_category_id = heroobj.getInt(Tags.deal_category_id);
                    adHocDealModel.sub_category_id = heroobj.getInt(Tags.sub_category_id);
                    adHocDealModel.facilities = heroobj.getString(Tags.facilities);
                    adHocDealModel.item_detail = heroobj.getString(Tags.item_detail);
                    adHocDealModel.deal_type = heroobj.getInt(Tags.deal_type);
                    adHocDealModel.starting_date = heroobj.getString(Tags.starting_date);
                    adHocDealModel.expiry_date = heroobj.getString(Tags.expiry_date);
                    adHocDealModel.price = heroobj.getInt(Tags.price);
                    adHocDealModel.discount_cost = heroobj.getInt(Tags.discount_cost);
                    adHocDealModel.total_deal = heroobj.getInt(Tags.total_deal);
                    adHocDealModel.volume_of_deal = heroobj.getInt(Tags.volume_of_deal);
                    adHocDealModel.timing_to = heroobj.getString(Tags.timing_to);
                    adHocDealModel.timing_from = heroobj.getString(Tags.timing_from);
                    adHocDealModel.valid_on = heroobj.getString(Tags.valid_on);
                    adHocDealModel.vendor_delete_deal = heroobj.getInt(Tags.vendor_delete_deal);
                    adHocDealModel.favourite = heroobj.getInt(Tags.favourite);
                    if (heroobj.has(Tags.liked))
                        adHocDealModel.liked = heroobj.getInt(Tags.liked);
                    if (heroobj.has(Tags.expired))
                        adHocDealModel.expired = heroobj.getInt(Tags.expired);
                    if (AppDelegate.isValidString(heroobj.getString(Tags.user))) {
                        JSONObject vendor = heroobj.getJSONObject(Tags.user);
                        adHocDealModel.vendor_name = vendor.getString(Tags.name);
                        if (vendor.has(Tags.busines) && AppDelegate.isValidString(vendor.getString(Tags.busines))) {
                            adHocDealModel.latitude = vendor.getJSONObject(Tags.busines).getString(Tags.latitude);
                            adHocDealModel.longitude = vendor.getJSONObject(Tags.busines).getString(Tags.longitude);
                            adHocDealModel.about_me = vendor.getJSONObject(Tags.busines).getString(Tags.about_business);
                            adHocDealModel.company_address = vendor.getJSONObject(Tags.busines).getString(Tags.location_address);
                            adHocDealModel.company_name = vendor.getJSONObject(Tags.busines).getString(Tags.name);
                            adHocDealModel.company_image = vendor.getJSONObject(Tags.busines).getString(Tags.image);
                            adHocDealModel.company_opening_hours = vendor.getJSONObject(Tags.busines).getString(Tags.open_hours);
                            adHocDealModel.company_contact_number = vendor.getJSONObject(Tags.busines).getString(Tags.contact_number);
//                            adHocDealModel.cityName = vendor.getJSONObject(Tags.busines).getString(Tags.cityName);
                        }
                    }
                    try {
                        if (AppDelegate.isValidString(heroobj.getString(Tags.images))) {
                            JSONArray img = heroobj.getJSONArray(Tags.images);
                            for (int k = 0; k < img.length(); k++) {
                                JSONObject json = img.getJSONObject(k);
                                adHocDealModel.image = json.getString(Tags.image);
                            }
                        }
                    } catch (JSONException e) {
                        AppDelegate.LogE(e);
                    }
                    if (heroobj.has(Tags.image))
                        adHocDealModel.image = heroobj.getString(Tags.image);
                    ArrayList<ItemsModel> arrayItems = new ArrayList<>();
                    for (int k = 0; k < cart_items_array.length(); k++) {
                        JSONObject cartObject = cart_items_array.getJSONObject(k);
                        JSONObject cartItemobj = cartObject.getJSONObject("item");
                        ItemsModel itemsModel = new ItemsModel();
                        itemsModel.id = JSONParser.getInt(cartItemobj, Tags.id);
                        itemsModel.adhocdeal_id = JSONParser.getInt(cartItemobj, Tags.adhocdeal_id);
                        itemsModel.name = JSONParser.getString(cartItemobj, Tags.name);
                        itemsModel.actual_cost = JSONParser.getInt(cartItemobj, Tags.actual_cost);
                        itemsModel.price = JSONParser.getInt(cartItemobj, Tags.price);
                        itemsModel.discount_cost = JSONParser.getInt(cartItemobj, Tags.discount_cost);
                        itemsModel.details = JSONParser.getString(cartItemobj, Tags.details);
                        itemsModel.created = JSONParser.getString(cartItemobj, Tags.created);

                        itemsModel.total_amount = JSONParser.getString(cartObject, Tags.total_amount);
                        itemsModel.no_of_item = JSONParser.getString(cartObject, Tags.no_of_item);

                        arrayItems.add(itemsModel);
                    }


                    adHocDealModel.arrayItems = arrayItems;

                    arrayAdhocDeals.add(adHocDealModel);
                }
                setselection(LABEL_ALL);
                showMypurchaselist(arrayAdhocDeals, LABEL_ALL);
            } else {
                AppDelegate.showToast(this, jsonobj.getString(Tags.message));
            }
        } catch (JSONException e) {
            AppDelegate.LogE(e);
        }
    }

    private void showMypurchaselist(ArrayList<AdHocDealsModel> cart_itemsArrayList, int value) {
        AppDelegate.LogT("cart_itemsArrayList==" + cart_itemsArrayList);
        LinearLayoutManager mLinearLayoutManager = new LinearLayoutManager(this);
        mLinearLayoutManager.setOrientation(LinearLayoutManager.VERTICAL);
        mAdapter = new MyPurchaseAdapter(this, cart_itemsArrayList, this, value);
//        recycler_deals_list.setLayoutManager(mLinearLayoutManager);
//        recycler_deals_list.setHasFixedSize(true);
//        recycler_deals_list.setItemAnimator(new DefaultItemAnimator());
        recycler_deals_list.setAdapter(mAdapter);
    }

    @Override
    public void setOnListItemClickListener(String name, int position) {
        AppDelegate.LogT("name = " + name + ", pso => " + position);
        if (name.equalsIgnoreCase(Tags.redeem)) {
            showAlert(this, getString(R.string.alert_adhoc), getString(R.string.alert_adhoc_message), position);
        } else if (name.equalsIgnoreCase(Tags.favourite)) {
            for (int i = 0; i < arrayAdhocDeals.size(); i++) {
                if (Integer.parseInt(arrayAdhocDeals.get(i).coupon_id) == position) {
                    AppDelegate.LogT("fav position=> " + arrayAdhocDeals.get(i).adhocdeal_id + " = " + position);
                    this.position = i;
                    if (new Prefs(MyPurchaseActivity.this).getUserdata() != null) {
                        if (arrayAdhocDeals.get(i).liked == DISLIKE) {
                            execute_favouriteApi(DISLIKE, arrayAdhocDeals.get(i).adhocdeal_id, new Prefs(MyPurchaseActivity.this).getUserdata().id);
                        } else {
                            execute_favouriteApi(LIKE, arrayAdhocDeals.get(i).adhocdeal_id, new Prefs(MyPurchaseActivity.this).getUserdata().id);
                        }
                    } else {
                        AppDelegate.showToast(MyPurchaseActivity.this, getString(R.string.login_first));
                    }
                }
            }
//            if (arrayAdhocDeals != null && arrayAdhocDeals.size() > position)
//                executeRedeemApi(position);
        }
    }

    public void showAlert(Context mContext, String Title, String Message, final int position) {
        try {
            AlertDialog.Builder alert = new AlertDialog.Builder(mContext);
            alert.setCancelable(false);
            alert.setTitle(Title);
            alert.setMessage(Message);
            alert.setPositiveButton(
                    Tags.OK,
                    new DialogInterface.OnClickListener() {

                        @Override
                        public void onClick(DialogInterface dialog, int which) {
                            for (int i = 0; i < arrayAdhocDeals.size(); i++) {
                                if (Integer.parseInt(arrayAdhocDeals.get(i).coupon_id) == position) {
                                    MyPurchaseActivity.this.position = i;
                                    executeRedeemApi(i);
                                }
                            }
                            dialog.dismiss();
                        }
                    }).setNegativeButton(Tags.CANCEL, new DialogInterface.OnClickListener() {
                @Override
                public void onClick(DialogInterface dialog, int which) {
                    dialog.dismiss();
                }
            });
            alert.show();
        } catch (Exception e) {
            AppDelegate.LogE(e);
        }
    }

    private void executeRedeemApi(int position) {
        try {
            if (AppDelegate.haveNetworkConnection(this, true)) {
                ArrayList<PostAysnc_Model> mPostArrayList = new ArrayList<PostAysnc_Model>();
                AppDelegate.getInstance(this).setPostParamsSecond(mPostArrayList, Tags.API_KEY, Tags.API_KEY_VALUE);
                if (new Prefs(this).getUserdata() != null)
                    AppDelegate.getInstance(this).setPostParamsSecond(mPostArrayList, Tags.user_id, (new Prefs(this).getUserdata().id), ServerRequestConstants.Key_PostintValue);
                AppDelegate.getInstance(this).setPostParamsSecond(mPostArrayList, Tags.coupon_id, arrayAdhocDeals.get(position).coupon_id);
                AppDelegate.getInstance(this).setPostParamsSecond(mPostArrayList, Tags.deal_id, arrayAdhocDeals.get(position).adhocdeal_id);

                PostAsync mPostAsyncObj;
                mPostAsyncObj = new PostAsync(this, this, ServerRequestConstants.REDEEM_ADHOC,
                        mPostArrayList, null);
                mHandler.sendEmptyMessage(10);
                mPostAsyncObj.execute();
            }
        } catch (Exception e) {
            AppDelegate.showToast(this, getResources().getString(R.string.try_again));
            AppDelegate.LogE(e);
        }
    }

    @Override
    public void setOnListItemClickListener(String name, int position, boolean like_dislike) {
        this.like_dislike = like_dislike;
        if (name.equalsIgnoreCase(Tags.favourite)) {
            for (int i = 0; i < arrayAdhocDeals.size(); i++) {
                if (Integer.parseInt(arrayAdhocDeals.get(i).coupon_id) == position) {
                    AppDelegate.LogT("fav position=> " + arrayAdhocDeals.get(i).adhocdeal_id + " = " + position);
                    this.position = i;
                    if (new Prefs(MyPurchaseActivity.this).getUserdata() != null) {
                        if (arrayAdhocDeals.get(i).liked == DISLIKE) {
                            execute_favouriteApi(LIKE, arrayAdhocDeals.get(i).adhocdeal_id, new Prefs(MyPurchaseActivity.this).getUserdata().id);
                        } else {
                            execute_favouriteApi(DISLIKE, arrayAdhocDeals.get(i).adhocdeal_id, new Prefs(MyPurchaseActivity.this).getUserdata().id);
                        }
                    } else {
                        AppDelegate.showToast(MyPurchaseActivity.this, getString(R.string.login_first));
                    }
                }
            }
//            if (arrayAdhocDeals != null && arrayAdhocDeals.size() > position)
//                executeRedeemApi(position);
        }
    }

    private void execute_favouriteApi(int like, int id, int user_id) {
        try {
            if (AppDelegate.haveNetworkConnection(MyPurchaseActivity.this, true)) {
                ArrayList<PostAysnc_Model> mPostArrayList = new ArrayList<PostAysnc_Model>();
                AppDelegate.getInstance(MyPurchaseActivity.this).setPostParamsSecond(mPostArrayList, Tags.API_KEY, Tags.API_KEY_VALUE);
                AppDelegate.getInstance(MyPurchaseActivity.this).setPostParamsSecond(mPostArrayList, Tags.deal_id, id);
                AppDelegate.getInstance(MyPurchaseActivity.this).setPostParamsSecond(mPostArrayList, Tags.user_id, user_id);
                AppDelegate.getInstance(MyPurchaseActivity.this).setPostParamsSecond(mPostArrayList, Tags.status, like, ServerRequestConstants.Key_PostintValue);
                PostAsync mPostAsyncObj;
                mPostAsyncObj = new PostAsync(MyPurchaseActivity.this, MyPurchaseActivity.this, ServerRequestConstants.ADHOC_LIKE,
                        mPostArrayList, null);
                AppDelegate.showProgressDialog(MyPurchaseActivity.this);
                mPostAsyncObj.execute();
            }
        } catch (Exception e) {
            AppDelegate.showToast(MyPurchaseActivity.this, getResources().getString(R.string.try_again));
            AppDelegate.LogE(e);
        }
    }

}
