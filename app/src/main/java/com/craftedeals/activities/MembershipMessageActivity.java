package com.craftedeals.activities;

import android.content.Intent;
import android.graphics.drawable.Drawable;
import android.os.Build;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v7.app.AppCompatActivity;
import android.view.View;
import android.view.Window;
import android.view.WindowManager;

import com.craftedeals.AppDelegate;
import com.craftedeals.R;
import com.craftedeals.constants.Tags;

import carbon.widget.TextView;

import static com.craftedeals.activities.DealsActivity.DEAL_ADHOC;

/**
 * Created by Bharat on 27-Dec-16.
 */
public class MembershipMessageActivity extends AppCompatActivity {

    private TextView txt_c_msg, txt_steps_1, txt_steps_2, txt_steps_3, txt_steps_4, txt_c_step;
    public static final int DEAL_BOUGHT = 1, DEAL_REDEEMED = 2;
    private int type = 0, deal_type = DEAL_ADHOC;
    public String str_deal_name = "Deal";
    private int login_from = 0;
    private int membership_id = 0;
    private String membership_amount = "";

    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.LOLLIPOP) {
            Window window = this.getWindow();
            Drawable background = this.getResources().getDrawable(R.drawable.topbg);
            window.addFlags(WindowManager.LayoutParams.FLAG_DRAWS_SYSTEM_BAR_BACKGROUNDS);
            window.setStatusBarColor(getResources().getColor(android.R.color.transparent));
            window.setNavigationBarColor(getResources().getColor(android.R.color.transparent));
            window.setBackgroundDrawable(background);
        }
        setContentView(R.layout.membership_message_activity);
        login_from = getIntent().getIntExtra(Tags.login_from, 0);
        membership_id = getIntent().getIntExtra(Tags.membership_id, 0);
        membership_amount = getIntent().getStringExtra(Tags.membership_amount);
        txt_c_msg = (TextView) findViewById(R.id.txt_c_msg);
        if (membership_id != 1) {
            txt_c_msg.setText(getString(R.string.welcome_on_board) + " " + getString(R.string.paid_msg));
        } else {
            txt_c_msg.setText(getString(R.string.welcome_on_board) + " " + "Browse and purchase great one off Tap-A-Deals offered by our venues .To save $1000 plus upgrade to our Hero Deals for $" + membership_amount + " With Craftedeals you will DISCOVER | EXPERIENCE | SAVE");
        }
        findViewById(R.id.txt_c_ok).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (login_from == AppDelegate.LOGIN_FROM_VENDOR_PROFILE) {
                    finish();
                } else if (DashBoardActivity.mainActivity != null) {
                    finish();
                } else {
                    startActivity(new Intent(MembershipMessageActivity.this, DashBoardActivity.class));
                    finish();
                }
            }
        });
    }

    @Override
    public void onBackPressed() {
        if (login_from == AppDelegate.LOGIN_FROM_VENDOR_PROFILE) {
            finish();
        } else if (DashBoardActivity.mainActivity != null) {
            finish();
        } else {
            startActivity(new Intent(MembershipMessageActivity.this, DashBoardActivity.class));
        }
    }

    @Override
    protected void finalize() throws Throwable {
        super.finalize();
    }
}