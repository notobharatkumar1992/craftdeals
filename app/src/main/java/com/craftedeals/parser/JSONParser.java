package com.craftedeals.parser;


import com.craftedeals.AppDelegate;

import org.json.JSONObject;

/**
 * Created by bharat on 14/1/16.
 */
public class JSONParser {

    public static String getString(JSONObject jsonObject, String str_tag) {
        String str_value = "";
        try {
            str_value = jsonObject.opt(str_tag) != null ? jsonObject.getString(str_tag) : "";
        } catch (Exception e) {
            AppDelegate.LogE(e);
        }
        return str_value;
    }

    public static int getInt(JSONObject jsonObject, String str_tag) {
        int int_value = -1;
        try {
            int_value = Integer.parseInt(jsonObject.opt(str_tag) != null ? (!jsonObject.getString(str_tag).equalsIgnoreCase("null") ? jsonObject.getString(str_tag) : "-1") : "-1");
        } catch (Exception e) {
            AppDelegate.LogE(e);
        }
        return int_value;
    }

    public static boolean getBoolean(JSONObject jsonObject, String str_tag) {
        boolean value = false;
        try {
            String str_value = jsonObject.opt(str_tag) != null ? jsonObject.getString(str_tag) : "false";
            value = str_value.equalsIgnoreCase("true") ? true : false;
        } catch (Exception e) {
            AppDelegate.LogE(e);
        }
        return value;
    }

    public static double getDouble(JSONObject jsonObject, String str_tag) {
        double value = 0;
        try {
            value = Double.valueOf(jsonObject.opt(str_tag) != null ? (!jsonObject.getString(str_tag).equalsIgnoreCase("null") ? jsonObject.getString(str_tag) : "0") : "0");
        } catch (Exception e) {
            AppDelegate.LogE(e);
        }
        return value;
    }
}
