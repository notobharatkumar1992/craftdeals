package com.craftedeals.Models;

import android.os.Parcel;
import android.os.Parcelable;

/**
 * Created by Bharat on 23-Nov-16.
 */

public class MembershipModel implements Parcelable
{

    public String membership_id, name, amount, month_of_membership, description, status, created;

    public MembershipModel(Parcel in)
    {
        membership_id = in.readString();
        name = in.readString();
        amount = in.readString();
        month_of_membership = in.readString();
        description = in.readString();
        status = in.readString();
        created = in.readString();
    }

    public MembershipModel()
    {

    }

    @Override
    public String toString()
    {
        return "MembershipModel{" +
                "membership_id='" + membership_id + '\'' +
                ", name='" + name + '\'' +
                ", amount='" + amount + '\'' +
                ", month_of_membership='" + month_of_membership + '\'' +
                ", description='" + description + '\'' +
                ", status='" + status + '\'' +
                ", created='" + created + '\'' +
                '}';
    }

    public static final Creator<MembershipModel> CREATOR = new Creator<MembershipModel>()
    {
        @Override
        public MembershipModel createFromParcel(Parcel in)
        {
            return new MembershipModel(in);
        }

        @Override
        public MembershipModel[] newArray(int size)
        {
            return new MembershipModel[size];
        }
    };

    @Override
    public int describeContents()
    {
        return 0;
    }

    @Override
    public void writeToParcel(Parcel dest, int flags)
    {
        dest.writeString(membership_id);
        dest.writeString(name);
        dest.writeString(amount);
        dest.writeString(month_of_membership);
        dest.writeString(description);
        dest.writeString(status);
        dest.writeString(created);
    }
}
