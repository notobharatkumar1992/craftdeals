package com.craftedeals.Models;

import android.os.Parcel;
import android.os.Parcelable;

/**
 * Created by parul on 15/1/16.
 */
public class UserDataModel implements Parcelable {
    public int id, postal_code, category_id, sub_category_id, membership_id, login_type, device_type, is_verified, is_login, is_view, beer_id, brewery_id;
    public String userId, name, first_names, last_names, email, contact_number, address, country_id, state_id, city_id, dob, role, image, reset_password, social_id, verification_code, device_id, created, password, file_path;
    public String province, city,heard_about_us;
    public int total_purchase, total_redeemed, favourite,user_range;
    public String fav_brewery, fav_beer,membership_end_date,membership_start_date;

    public UserDataModel() {
    }

    @Override
    public String toString() {
        return "UserDataModel{" +
                "id=" + id +
                ", postal_code=" + postal_code +
                ", category_id=" + category_id +
                ", sub_category_id=" + sub_category_id +
                ", membership_id=" + membership_id +
                ", login_type=" + login_type +
                ", device_type=" + device_type +
                ", is_verified=" + is_verified +
                ", is_login=" + is_login +
                ", is_view=" + is_view +
                ", beer_id=" + beer_id +
                ", brewery_id=" + brewery_id +
                ", userId='" + userId + '\'' +
                ", name='" + name + '\'' +
                ", first_names='" + first_names + '\'' +
                ", last_names='" + last_names + '\'' +
                ", email='" + email + '\'' +
                ", contact_number='" + contact_number + '\'' +
                ", address='" + address + '\'' +
                ", country_id='" + country_id + '\'' +
                ", state_id='" + state_id + '\'' +
                ", city_id='" + city_id + '\'' +
                ", dob='" + dob + '\'' +
                ", role='" + role + '\'' +
                ", image='" + image + '\'' +
                ", reset_password='" + reset_password + '\'' +
                ", social_id='" + social_id + '\'' +
                ", verification_code='" + verification_code + '\'' +
                ", device_id='" + device_id + '\'' +
                ", created='" + created + '\'' +
                ", password='" + password + '\'' +
                ", file_path='" + file_path + '\'' +
                ", province='" + province + '\'' +
                ", city='" + city + '\'' +
                ", heard_about_us='" + heard_about_us + '\'' +
                ", total_purchase=" + total_purchase +
                ", total_redeemed=" + total_redeemed +
                ", favourite=" + favourite +
                ", user_range=" + user_range +
                ", fav_brewery='" + fav_brewery + '\'' +
                ", fav_beer='" + fav_beer + '\'' +
                ", membership_end_date='" + membership_end_date + '\'' +
                ", membership_start_date='" + membership_start_date + '\'' +
                '}';
    }

    protected UserDataModel(Parcel in) {
        id = in.readInt();
        postal_code = in.readInt();
        category_id = in.readInt();
        sub_category_id = in.readInt();
        membership_id = in.readInt();
        login_type = in.readInt();
        device_type = in.readInt();
        is_verified = in.readInt();
        is_login = in.readInt();
        is_view = in.readInt();
        beer_id = in.readInt();
        brewery_id = in.readInt();
        userId = in.readString();
        name = in.readString();
        first_names = in.readString();
        last_names = in.readString();
        email = in.readString();
        contact_number = in.readString();
        address = in.readString();
        country_id = in.readString();
        state_id = in.readString();
        city_id = in.readString();
        dob = in.readString();
        role = in.readString();
        image = in.readString();
        reset_password = in.readString();
        social_id = in.readString();
        verification_code = in.readString();
        device_id = in.readString();
        created = in.readString();
        password = in.readString();
        file_path = in.readString();
        province = in.readString();
        city = in.readString();
        heard_about_us = in.readString();
        total_purchase = in.readInt();
        total_redeemed = in.readInt();
        favourite = in.readInt();
        user_range = in.readInt();
        fav_brewery = in.readString();
        fav_beer = in.readString();
        membership_end_date = in.readString();
        membership_start_date = in.readString();
    }

    @Override
    public void writeToParcel(Parcel dest, int flags) {
        dest.writeInt(id);
        dest.writeInt(postal_code);
        dest.writeInt(category_id);
        dest.writeInt(sub_category_id);
        dest.writeInt(membership_id);
        dest.writeInt(login_type);
        dest.writeInt(device_type);
        dest.writeInt(is_verified);
        dest.writeInt(is_login);
        dest.writeInt(is_view);
        dest.writeInt(beer_id);
        dest.writeInt(brewery_id);
        dest.writeString(userId);
        dest.writeString(name);
        dest.writeString(first_names);
        dest.writeString(last_names);
        dest.writeString(email);
        dest.writeString(contact_number);
        dest.writeString(address);
        dest.writeString(country_id);
        dest.writeString(state_id);
        dest.writeString(city_id);
        dest.writeString(dob);
        dest.writeString(role);
        dest.writeString(image);
        dest.writeString(reset_password);
        dest.writeString(social_id);
        dest.writeString(verification_code);
        dest.writeString(device_id);
        dest.writeString(created);
        dest.writeString(password);
        dest.writeString(file_path);
        dest.writeString(province);
        dest.writeString(city);
        dest.writeString(heard_about_us);
        dest.writeInt(total_purchase);
        dest.writeInt(total_redeemed);
        dest.writeInt(favourite);
        dest.writeInt(user_range);
        dest.writeString(fav_brewery);
        dest.writeString(fav_beer);
        dest.writeString(membership_end_date);
        dest.writeString(membership_start_date);
    }

    @Override
    public int describeContents() {
        return 0;
    }

    public static final Creator<UserDataModel> CREATOR = new Creator<UserDataModel>() {
        @Override
        public UserDataModel createFromParcel(Parcel in) {
            return new UserDataModel(in);
        }

        @Override
        public UserDataModel[] newArray(int size) {
            return new UserDataModel[size];
        }
    };
}
