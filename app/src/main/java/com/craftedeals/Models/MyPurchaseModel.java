package com.craftedeals.Models;

import android.os.Parcel;
import android.os.Parcelable;

import java.util.ArrayList;

/**
 * Created by Heena on 07-Dec-16.
 */

public class MyPurchaseModel implements Parcelable {

    public int status, id, user_id, adhocdeal_id, payment_id, coupon_id, cart_id, no_of_item, total_amount;

    public ArrayList<MyPurchaseModel> cart_itemsArrayList = new ArrayList<>();

    public String deal_name, redeem_status;
    public ItemsModel itemsModel;
    public AdHocDealsModel adHocDealsModel;

    public MyPurchaseModel() {
    }

    @Override
    public String toString() {
        return "MyPurchaseModel{" +
                "status=" + status +
                ", id=" + id +
                ", user_id=" + user_id +
                ", adhocdeal_id=" + adhocdeal_id +
                ", payment_id=" + payment_id +
                ", coupon_id=" + coupon_id +
                ", cart_id=" + cart_id +
                ", no_of_item=" + no_of_item +
                ", total_amount=" + total_amount +
                ", cart_itemsArrayList=" + cart_itemsArrayList +
                ", deal_name='" + deal_name + '\'' +
                ", redeem_status='" + redeem_status + '\'' +
                ", itemsModel=" + itemsModel +
                ", adHocDealsModel=" + adHocDealsModel +
                '}';
    }

    protected MyPurchaseModel(Parcel in) {
        status = in.readInt();
        id = in.readInt();
        user_id = in.readInt();
        adhocdeal_id = in.readInt();
        payment_id = in.readInt();
        coupon_id = in.readInt();
        cart_id = in.readInt();
        no_of_item = in.readInt();
        total_amount = in.readInt();
        cart_itemsArrayList = in.createTypedArrayList(MyPurchaseModel.CREATOR);
        deal_name = in.readString();
        redeem_status = in.readString();
        itemsModel = in.readParcelable(ItemsModel.class.getClassLoader());
        adHocDealsModel = in.readParcelable(AdHocDealsModel.class.getClassLoader());
    }

    @Override
    public void writeToParcel(Parcel dest, int flags) {
        dest.writeInt(status);
        dest.writeInt(id);
        dest.writeInt(user_id);
        dest.writeInt(adhocdeal_id);
        dest.writeInt(payment_id);
        dest.writeInt(coupon_id);
        dest.writeInt(cart_id);
        dest.writeInt(no_of_item);
        dest.writeInt(total_amount);
        dest.writeTypedList(cart_itemsArrayList);
        dest.writeString(deal_name);
        dest.writeString(redeem_status);
        dest.writeParcelable(itemsModel, flags);
        dest.writeParcelable(adHocDealsModel, flags);
    }

    @Override
    public int describeContents() {
        return 0;
    }

    public static final Creator<MyPurchaseModel> CREATOR = new Creator<MyPurchaseModel>() {
        @Override
        public MyPurchaseModel createFromParcel(Parcel in) {
            return new MyPurchaseModel(in);
        }

        @Override
        public MyPurchaseModel[] newArray(int size) {
            return new MyPurchaseModel[size];
        }
    };
}
