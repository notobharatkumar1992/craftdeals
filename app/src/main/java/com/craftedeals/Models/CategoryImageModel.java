package com.craftedeals.Models;

import android.graphics.Bitmap;

/**
 * Created by Bharat on 05-Apr-17.
 */

public class CategoryImageModel {

    public String category_id, marker_icon;
    public Bitmap bitmap;

    public CategoryImageModel(String category_id, String marker_icon, Bitmap loadedImage) {
        this.category_id = category_id;
        this.marker_icon = marker_icon;
        this.bitmap = loadedImage;
    }
}
