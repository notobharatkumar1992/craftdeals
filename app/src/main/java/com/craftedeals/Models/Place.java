package com.craftedeals.Models;

import android.os.Parcel;
import android.os.Parcelable;

import org.json.JSONException;
import org.json.JSONObject;

import java.util.logging.Level;
import java.util.logging.Logger;

public class Place implements Parcelable {

    public String id;
    public String user_id;
    public String icon;
    public String name;
    public String add;
    public String distance;
    public String isFav = "0";
    public String isTo = "0";
    public String vicinity;
    public Double latitude;
    public Double longitude;


    public String formatted_address;
    public String formatted_phone_number;
    public String reference;

    public String postal_code;
    public String city;
    public String country;
    public String address_line_2;

    public boolean is_primary_location = false;

    // public String fav_type = "0";

    public static final Creator<Place> CREATOR = new Creator<Place>() {
        @Override
        public Place createFromParcel(Parcel in) {
            return new Place(in);
        }

        @Override
        public Place[] newArray(int size) {
            return new Place[size];
        }
    };

    public static Place jsonToPontoReferencia(JSONObject pontoReferencia) {
        try {
            Place result = new Place();
            JSONObject geometry = (JSONObject) pontoReferencia.get("geometry");
            JSONObject location = (JSONObject) geometry.get("location");
            result.setLatitude((Double) location.get("lat"));
            result.setLongitude((Double) location.get("lng"));
            result.setIcon(pontoReferencia.getString("icon"));
            result.setName(pontoReferencia.getString("name"));
            result.setAddress(pontoReferencia.getString("vicinity"));
            result.setVicinity(pontoReferencia.getString("vicinity"));
            result.setId(pontoReferencia.getString("place_id"));
            return result;
        } catch (JSONException ex) {
            Logger.getLogger(Place.class.getName()).log(Level.SEVERE, null, ex);
        }
        return null;
    }

    public Place() {

    }

    /**
     * @return the isTo
     */
    public String getisTo() {
        return isTo;
    }

    /**
     * @param isTo the isTo to set
     */
    public void setisTo(String isTo) {
        this.isTo = isTo;
    }

    /**
     * @return the distance
     */
    public String getDistance() {
        return distance;
    }

    /**
     * @param distance the distance to set
     */
    public void setDistance(String distance) {
        this.distance = distance;
    }

    /**
     * @return the isFav
     */
    public String getIsFav() {
        return isFav;
    }

    /**
     * @param isFav the isFav to set
     */
    public void setIsFav(String isFav) {
        this.isFav = isFav;
    }

    public String getAddress() {
        return add;
    }

    public void setAddress(String add) {
        this.add = add;
    }

    public String getFormatted_address() {
        return formatted_address;
    }

    public void setFormatted_address(String formatted_address) {
        this.formatted_address = formatted_address;
    }

    public String getFormatted_phone_number() {
        return formatted_phone_number;
    }

    public void setFormatted_phone_number(String formatted_phone_number) {
        this.formatted_phone_number = formatted_phone_number;
    }

    public String getReference() {
        return reference;
    }

    public void setReference(String reference) {
        this.reference = reference;
    }

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public String getuser_id() {
        return user_id;
    }

    public void setuser_id(String user_id) {
        this.user_id = user_id;
    }

    public String getIcon() {
        return icon;
    }

    public void setIcon(String icon) {
        this.icon = icon;
    }

    public Double getLatitude() {
        return latitude;
    }

    public void setLatitude(Double latitude) {
        this.latitude = latitude;
    }

    public Double getLongitude() {
        return longitude;
    }

    public void setLongitude(Double longitude) {
        this.longitude = longitude;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getVicinity() {
        return vicinity;
    }

    // public String getsfav_types() {
    // return fav_type;
    // }
    //
    // public void setfav_stype(String fav_type) {
    // this.fav_type = fav_type;
    // }

    public Place(Parcel in) {
        id = in.readString();
        user_id = in.readString();
        icon = in.readString();
        name = in.readString();
        add = in.readString();
        distance = in.readString();
        isFav = in.readString();

        isTo = in.readString();
        // latitude = in.readDouble();
        //longitude = in.readDouble();
        vicinity = in.readString();
        formatted_address = in.readString();
        formatted_address = in.readString();
        reference = in.readString();
        postal_code = in.readString();
        city = in.readString();
        country = in.readString();
        address_line_2 = in.readString();

    }

    public void setVicinity(String vicinity) {
        this.vicinity = vicinity;
    }

    @Override
    public String toString() {
        return "Place{" + "id=" + id + ", icon=" + icon + ", name=" + name
                + ", latitude=" + latitude + ", longitude=" + longitude + '}';
    }

    @Override
    public int describeContents() {
        return 0;
    }

    @Override
    public void writeToParcel(Parcel dest, int flags) {
        dest.writeString(id);
        dest.writeString(user_id);
        dest.writeString(icon);
        dest.writeString(name);
        dest.writeString(add);
        dest.writeString(distance);
        dest.writeString(isFav);

        dest.writeString(isTo);
//        dest.writeDouble(latitude);
        //   dest.writeDouble(longitude);
        dest.writeString(vicinity);
        dest.writeString(formatted_address);
        dest.writeString(formatted_phone_number);
        dest.writeString(reference);
        dest.writeString(postal_code);
        dest.writeString(city);
        dest.writeString(country);
        dest.writeString(address_line_2);
    }
}