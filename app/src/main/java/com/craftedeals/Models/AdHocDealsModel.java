package com.craftedeals.Models;

import android.os.Parcel;
import android.os.Parcelable;

import java.util.ArrayList;

/**
 * Created by Heena on 20-Oct-16.
 */
public class AdHocDealsModel implements Parcelable {

    public int status, dataFlow, id, user_id, deal_category_id, sub_category_id, deal_type, price, volume_of_deal, vendor_delete_deal, total_deal, favourite, adhocdeal_id, image_id, liked;
    public String name, use_the_offer, things_to_remebers, facilities, item_detail, starting_date, expiry_date, timing_to, timing_from, valid_on, title, image, alt, thumb;
    public String vendor_name, latitude, longitude, category, about_me, address2, address1, company_image, company_thumb_image, cityName;
    public int deal_status;
    public int deal_price_real, deal_price_offer, discount_cost;
    public String company_address, company_name, company_contact_number, company_opening_hours;
    public int expired;
    public ArrayList<ItemsModel> arrayItems;
    public String purchase_id, payment_id, coupon_id, total_amount, coupon_name, coupon_created, coupon_modified;
    public int coupon_status;
    public String markerImage, distance, url;

    public AdHocDealsModel() {
    }


    protected AdHocDealsModel(Parcel in) {
        status = in.readInt();
        dataFlow = in.readInt();
        id = in.readInt();
        user_id = in.readInt();
        deal_category_id = in.readInt();
        sub_category_id = in.readInt();
        deal_type = in.readInt();
        price = in.readInt();
        volume_of_deal = in.readInt();
        vendor_delete_deal = in.readInt();
        total_deal = in.readInt();
        favourite = in.readInt();
        adhocdeal_id = in.readInt();
        image_id = in.readInt();
        liked = in.readInt();
        name = in.readString();
        use_the_offer = in.readString();
        things_to_remebers = in.readString();
        facilities = in.readString();
        item_detail = in.readString();
        starting_date = in.readString();
        expiry_date = in.readString();
        timing_to = in.readString();
        timing_from = in.readString();
        valid_on = in.readString();
        title = in.readString();
        image = in.readString();
        alt = in.readString();
        thumb = in.readString();
        vendor_name = in.readString();
        latitude = in.readString();
        longitude = in.readString();
        category = in.readString();
        about_me = in.readString();
        address2 = in.readString();
        address1 = in.readString();
        company_image = in.readString();
        company_thumb_image = in.readString();
        cityName = in.readString();
        deal_status = in.readInt();
        deal_price_real = in.readInt();
        deal_price_offer = in.readInt();
        discount_cost = in.readInt();
        company_address = in.readString();
        company_name = in.readString();
        company_contact_number = in.readString();
        company_opening_hours = in.readString();
        expired = in.readInt();
        arrayItems = in.createTypedArrayList(ItemsModel.CREATOR);
        purchase_id = in.readString();
        payment_id = in.readString();
        coupon_id = in.readString();
        total_amount = in.readString();
        coupon_name = in.readString();
        coupon_created = in.readString();
        coupon_modified = in.readString();
        coupon_status = in.readInt();
        markerImage = in.readString();
        distance = in.readString();
        url = in.readString();
    }

    @Override
    public void writeToParcel(Parcel dest, int flags) {
        dest.writeInt(status);
        dest.writeInt(dataFlow);
        dest.writeInt(id);
        dest.writeInt(user_id);
        dest.writeInt(deal_category_id);
        dest.writeInt(sub_category_id);
        dest.writeInt(deal_type);
        dest.writeInt(price);
        dest.writeInt(volume_of_deal);
        dest.writeInt(vendor_delete_deal);
        dest.writeInt(total_deal);
        dest.writeInt(favourite);
        dest.writeInt(adhocdeal_id);
        dest.writeInt(image_id);
        dest.writeInt(liked);
        dest.writeString(name);
        dest.writeString(use_the_offer);
        dest.writeString(things_to_remebers);
        dest.writeString(facilities);
        dest.writeString(item_detail);
        dest.writeString(starting_date);
        dest.writeString(expiry_date);
        dest.writeString(timing_to);
        dest.writeString(timing_from);
        dest.writeString(valid_on);
        dest.writeString(title);
        dest.writeString(image);
        dest.writeString(alt);
        dest.writeString(thumb);
        dest.writeString(vendor_name);
        dest.writeString(latitude);
        dest.writeString(longitude);
        dest.writeString(category);
        dest.writeString(about_me);
        dest.writeString(address2);
        dest.writeString(address1);
        dest.writeString(company_image);
        dest.writeString(company_thumb_image);
        dest.writeString(cityName);
        dest.writeInt(deal_status);
        dest.writeInt(deal_price_real);
        dest.writeInt(deal_price_offer);
        dest.writeInt(discount_cost);
        dest.writeString(company_address);
        dest.writeString(company_name);
        dest.writeString(company_contact_number);
        dest.writeString(company_opening_hours);
        dest.writeInt(expired);
        dest.writeTypedList(arrayItems);
        dest.writeString(purchase_id);
        dest.writeString(payment_id);
        dest.writeString(coupon_id);
        dest.writeString(total_amount);
        dest.writeString(coupon_name);
        dest.writeString(coupon_created);
        dest.writeString(coupon_modified);
        dest.writeInt(coupon_status);
        dest.writeString(markerImage);
        dest.writeString(distance);
        dest.writeString(url);
    }

    @Override
    public int describeContents() {
        return 0;
    }

    public static final Creator<AdHocDealsModel> CREATOR = new Creator<AdHocDealsModel>() {
        @Override
        public AdHocDealsModel createFromParcel(Parcel in) {
            return new AdHocDealsModel(in);
        }

        @Override
        public AdHocDealsModel[] newArray(int size) {
            return new AdHocDealsModel[size];
        }
    };
}
