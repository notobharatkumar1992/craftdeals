package com.craftedeals.Models;

import android.os.Parcel;
import android.os.Parcelable;

/**
 * Created by Heena on 25-Nov-16.
 */

public class FavBeveragesModel implements Parcelable {
    public int id;
    public String name, created;

    protected FavBeveragesModel(Parcel in) {
        id = in.readInt();
        name = in.readString();
        created = in.readString();
    }

    @Override
    public void writeToParcel(Parcel dest, int flags) {
        dest.writeInt(id);
        dest.writeString(name);
        dest.writeString(created);
    }

    @Override
    public int describeContents() {
        return 0;
    }

    public static final Creator<FavBeveragesModel> CREATOR = new Creator<FavBeveragesModel>() {
        @Override
        public FavBeveragesModel createFromParcel(Parcel in) {
            return new FavBeveragesModel(in);
        }

        @Override
        public FavBeveragesModel[] newArray(int size) {
            return new FavBeveragesModel[size];
        }
    };

    @Override
    public String toString() {
        return "FavBeveragesModel{" +
                "id=" + id +
                ", name='" + name + '\'' +
                ", created='" + created + '\'' +
                '}';
    }

    public FavBeveragesModel() {
    }
}
