package com.craftedeals.Models;

import android.os.Parcel;
import android.os.Parcelable;

/**
 * Created by Heena on 12-Oct-16.
 */
public class HeroDealsModel implements Parcelable {
    public int coupon_id, deal_status, id, user_id, deal_category_id, sub_category_id, value_upto, total_deal, status;
    public String company_contact_number, company_opening_hours, company_name, company_image, company_address, name, description, discount_offer, starting_date, expiry_date, trems_and_conditions, vendor_name, image, thumb, herodeal_title, category, longitude, latitude, created, modified;
    public int deal_id;
    public int redeem;
    public String markerImage, distance, cityName, url;

    public HeroDealsModel() {
    }


    protected HeroDealsModel(Parcel in) {
        coupon_id = in.readInt();
        deal_status = in.readInt();
        id = in.readInt();
        user_id = in.readInt();
        deal_category_id = in.readInt();
        sub_category_id = in.readInt();
        value_upto = in.readInt();
        total_deal = in.readInt();
        status = in.readInt();
        company_contact_number = in.readString();
        company_opening_hours = in.readString();
        company_name = in.readString();
        company_image = in.readString();
        company_address = in.readString();
        name = in.readString();
        description = in.readString();
        discount_offer = in.readString();
        starting_date = in.readString();
        expiry_date = in.readString();
        trems_and_conditions = in.readString();
        vendor_name = in.readString();
        image = in.readString();
        thumb = in.readString();
        herodeal_title = in.readString();
        category = in.readString();
        longitude = in.readString();
        latitude = in.readString();
        created = in.readString();
        modified = in.readString();
        deal_id = in.readInt();
        redeem = in.readInt();
        markerImage = in.readString();
        distance = in.readString();
        cityName = in.readString();
        url = in.readString();
    }

    @Override
    public void writeToParcel(Parcel dest, int flags) {
        dest.writeInt(coupon_id);
        dest.writeInt(deal_status);
        dest.writeInt(id);
        dest.writeInt(user_id);
        dest.writeInt(deal_category_id);
        dest.writeInt(sub_category_id);
        dest.writeInt(value_upto);
        dest.writeInt(total_deal);
        dest.writeInt(status);
        dest.writeString(company_contact_number);
        dest.writeString(company_opening_hours);
        dest.writeString(company_name);
        dest.writeString(company_image);
        dest.writeString(company_address);
        dest.writeString(name);
        dest.writeString(description);
        dest.writeString(discount_offer);
        dest.writeString(starting_date);
        dest.writeString(expiry_date);
        dest.writeString(trems_and_conditions);
        dest.writeString(vendor_name);
        dest.writeString(image);
        dest.writeString(thumb);
        dest.writeString(herodeal_title);
        dest.writeString(category);
        dest.writeString(longitude);
        dest.writeString(latitude);
        dest.writeString(created);
        dest.writeString(modified);
        dest.writeInt(deal_id);
        dest.writeInt(redeem);
        dest.writeString(markerImage);
        dest.writeString(distance);
        dest.writeString(cityName);
        dest.writeString(url);
    }

    @Override
    public int describeContents() {
        return 0;
    }

    public static final Creator<HeroDealsModel> CREATOR = new Creator<HeroDealsModel>() {
        @Override
        public HeroDealsModel createFromParcel(Parcel in) {
            return new HeroDealsModel(in);
        }

        @Override
        public HeroDealsModel[] newArray(int size) {
            return new HeroDealsModel[size];
        }
    };
}
