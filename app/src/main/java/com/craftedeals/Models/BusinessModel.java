package com.craftedeals.Models;

import android.os.Parcel;
import android.os.Parcelable;

/**
 * Created by Heena on 17-Oct-16.
 */
public class BusinessModel implements Parcelable {
    public int status, dataFlow, business_id, user_id;
    public String name, website_url, about_business, postal_code, contact_name, contact_number, image, email, address1, address2, city, state, country, location_address, longitude, latitude, open_hours, thumb;

    public BusinessModel() {
    }

    protected BusinessModel(Parcel in) {
        status = in.readInt();
        dataFlow = in.readInt();
        business_id = in.readInt();
        user_id = in.readInt();
        postal_code = in.readString();
        name = in.readString();
        website_url = in.readString();
        about_business = in.readString();
        contact_name = in.readString();
        contact_number = in.readString();
        image = in.readString();
        email = in.readString();
        address1 = in.readString();
        address2 = in.readString();
        city = in.readString();
        state = in.readString();
        country = in.readString();
        location_address = in.readString();
        longitude = in.readString();
        latitude = in.readString();
        open_hours = in.readString();
        thumb = in.readString();
    }

    public static final Creator<BusinessModel> CREATOR = new Creator<BusinessModel>() {
        @Override
        public BusinessModel createFromParcel(Parcel in) {
            return new BusinessModel(in);
        }

        @Override
        public BusinessModel[] newArray(int size) {
            return new BusinessModel[size];
        }
    };

    @Override
    public int describeContents() {
        return 0;
    }

    @Override
    public void writeToParcel(Parcel dest, int flags) {
        dest.writeInt(status);
        dest.writeInt(dataFlow);
        dest.writeInt(business_id);
        dest.writeInt(user_id);
        dest.writeString(postal_code);
        dest.writeString(name);
        dest.writeString(website_url);
        dest.writeString(about_business);
        dest.writeString(contact_name);
        dest.writeString(contact_number);
        dest.writeString(image);
        dest.writeString(email);
        dest.writeString(address1);
        dest.writeString(address2);
        dest.writeString(city);
        dest.writeString(state);
        dest.writeString(country);
        dest.writeString(location_address);
        dest.writeString(longitude);
        dest.writeString(latitude);
        dest.writeString(open_hours);
        dest.writeString(thumb);
    }
}
