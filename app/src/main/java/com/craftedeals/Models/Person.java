/*
 * Copyright 2013 Google Inc.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package com.craftedeals.Models;

import android.graphics.drawable.Drawable;
import android.os.Parcel;
import android.os.Parcelable;

import com.google.android.gms.maps.model.LatLng;


public class Person implements Parcelable {
    public final int id;
    public final String name;
    public final LatLng latlong;
    public HeroDealsModel heroDealsModel;
    public AdHocDealsModel adHocDealsModel;
    public int type;

    public Person(LatLng latlong, int id, String name, HeroDealsModel heroDealsModel, AdHocDealsModel adHocDealsModel, int type) {
        this.id = id;
        this.name = name;
        this.latlong = latlong;
        this.heroDealsModel = heroDealsModel;
        this.adHocDealsModel = adHocDealsModel;
        this.type = type;

    }


    protected Person(Parcel in) {
        id = in.readInt();
        name = in.readString();
        latlong = in.readParcelable(LatLng.class.getClassLoader());
        heroDealsModel = in.readParcelable(HeroDealsModel.class.getClassLoader());
        adHocDealsModel = in.readParcelable(AdHocDealsModel.class.getClassLoader());
        type = in.readInt();
    }

    @Override
    public String toString() {
        return "Person{" +
                "id=" + id +
                ", name='" + name + '\'' +
                ", latlong=" + latlong +
                ", heroDealsModel=" + heroDealsModel +
                ", adHocDealsModel=" + adHocDealsModel +
                ", type=" + type +
                '}';
    }

    @Override
    public void writeToParcel(Parcel dest, int flags) {
        dest.writeInt(id);
        dest.writeString(name);
        dest.writeParcelable(latlong, flags);
        dest.writeParcelable(heroDealsModel, flags);
        dest.writeParcelable(adHocDealsModel, flags);
        dest.writeInt(type);
    }

    @Override
    public int describeContents() {
        return 0;
    }

    public static final Creator<Person> CREATOR = new Creator<Person>() {
        @Override
        public Person createFromParcel(Parcel in) {
            return new Person(in);
        }

        @Override
        public Person[] newArray(int size) {
            return new Person[size];
        }
    };
}
