package com.craftedeals.Models;

import android.os.Parcel;
import android.os.Parcelable;

/**
 * Created by Heena on 07-Oct-16.
 */
public class DealsModel implements Parcelable {
int status,dataFlow;

    public DealsModel() {
    }

    protected DealsModel(Parcel in) {
        status = in.readInt();
        dataFlow = in.readInt();
    }

    @Override
    public void writeToParcel(Parcel dest, int flags) {
        dest.writeInt(status);
        dest.writeInt(dataFlow);
    }

    @Override
    public int describeContents() {
        return 0;
    }

    public static final Creator<DealsModel> CREATOR = new Creator<DealsModel>() {
        @Override
        public DealsModel createFromParcel(Parcel in) {
            return new DealsModel(in);
        }

        @Override
        public DealsModel[] newArray(int size) {
            return new DealsModel[size];
        }
    };
}
