package com.craftedeals.Models;

import android.os.Parcel;
import android.os.Parcelable;

/**
 * Created by Heena on 31-Jan-17.
 */

public class OpenHoursModel implements Parcelable {
    public  String from, day,to;

    public OpenHoursModel() {
    }

    protected OpenHoursModel(Parcel in) {
        from = in.readString();
        day = in.readString();
        to = in.readString();
    }

    @Override
    public void writeToParcel(Parcel dest, int flags) {
        dest.writeString(from);
        dest.writeString(day);
        dest.writeString(to);
    }

    @Override
    public int describeContents() {
        return 0;
    }

    public static final Creator<OpenHoursModel> CREATOR = new Creator<OpenHoursModel>() {
        @Override
        public OpenHoursModel createFromParcel(Parcel in) {
            return new OpenHoursModel(in);
        }

        @Override
        public OpenHoursModel[] newArray(int size) {
            return new OpenHoursModel[size];
        }
    };
}
