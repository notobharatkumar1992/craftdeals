package com.craftedeals.Models;

import android.os.Parcel;
import android.os.Parcelable;

/**
 * Created by Heena on 12-Oct-16.
 */
public class CategoryModel implements Parcelable {
    public int id, status, checked;
    public String title, description, image, thumb, created, marker_icon;

    public CategoryModel() {
    }

    protected CategoryModel(Parcel in) {
        id = in.readInt();
        status = in.readInt();
        checked = in.readInt();
        title = in.readString();
        description = in.readString();
        image = in.readString();
        thumb = in.readString();
        created = in.readString();
        marker_icon = in.readString();
    }

    @Override
    public void writeToParcel(Parcel dest, int flags) {
        dest.writeInt(id);
        dest.writeInt(status);
        dest.writeInt(checked);
        dest.writeString(title);
        dest.writeString(description);
        dest.writeString(image);
        dest.writeString(thumb);
        dest.writeString(created);
        dest.writeString(marker_icon);
    }

    @Override
    public int describeContents() {
        return 0;
    }

    public static final Creator<CategoryModel> CREATOR = new Creator<CategoryModel>() {
        @Override
        public CategoryModel createFromParcel(Parcel in) {
            return new CategoryModel(in);
        }

        @Override
        public CategoryModel[] newArray(int size) {
            return new CategoryModel[size];
        }
    };
}
