package com.craftedeals.Models;

import android.os.Parcel;
import android.os.Parcelable;

/**
 * Created by Heena on 05-Dec-16.
 */

public class NotificationModel implements Parcelable {
    public int status, reading_status, notification_type, purchase_id, user_id, payment_id;
    public String message, vendor_name, vendor_image, deal_image, deal_type, vendor_id, deal_id, membership_id, membership_name, membership_amount, month_of_membership, membership_description, membership_status;
    public String deal_title, business_name, created;
    public int id;

    public int getStatus() {
        return status;
    }

    public void setStatus(int status) {
        this.status = status;
    }

    public int getReading_status() {
        return reading_status;
    }

    public void setReading_status(int reading_status) {
        this.reading_status = reading_status;
    }

    public int getNotification_type() {
        return notification_type;
    }

    public void setNotification_type(int notification_type) {
        this.notification_type = notification_type;
    }

    public int getPurchase_id() {
        return purchase_id;
    }

    public void setPurchase_id(int purchase_id) {
        this.purchase_id = purchase_id;
    }

    public int getUser_id() {
        return user_id;
    }

    public void setUser_id(int user_id) {
        this.user_id = user_id;
    }

    public int getPayment_id() {
        return payment_id;
    }

    public void setPayment_id(int payment_id) {
        this.payment_id = payment_id;
    }

    public String getMessage() {
        return message;
    }

    public void setMessage(String message) {
        this.message = message;
    }

    public String getVendor_name() {
        return vendor_name;
    }

    public void setVendor_name(String vendor_name) {
        this.vendor_name = vendor_name;
    }

    public String getVendor_image() {
        return vendor_image;
    }

    public void setVendor_image(String vendor_image) {
        this.vendor_image = vendor_image;
    }

    public String getDeal_image() {
        return deal_image;
    }

    public void setDeal_image(String deal_image) {
        this.deal_image = deal_image;
    }

    public String getDeal_type() {
        return deal_type;
    }

    public void setDeal_type(String deal_type) {
        this.deal_type = deal_type;
    }

    public String getVendor_id() {
        return vendor_id;
    }

    public void setVendor_id(String vendor_id) {
        this.vendor_id = vendor_id;
    }

    public String getDeal_id() {
        return deal_id;
    }

    public void setDeal_id(String deal_id) {
        this.deal_id = deal_id;
    }

    public String getMembership_id() {
        return membership_id;
    }

    public void setMembership_id(String membership_id) {
        this.membership_id = membership_id;
    }

    public String getMembership_name() {
        return membership_name;
    }

    public void setMembership_name(String membership_name) {
        this.membership_name = membership_name;
    }

    public String getMembership_amount() {
        return membership_amount;
    }

    public void setMembership_amount(String membership_amount) {
        this.membership_amount = membership_amount;
    }

    public String getMonth_of_membership() {
        return month_of_membership;
    }

    public void setMonth_of_membership(String month_of_membership) {
        this.month_of_membership = month_of_membership;
    }

    public String getMembership_description() {
        return membership_description;
    }

    public void setMembership_description(String membership_description) {
        this.membership_description = membership_description;
    }

    public String getMembership_status() {
        return membership_status;
    }

    public void setMembership_status(String membership_status) {
        this.membership_status = membership_status;
    }

    public String getDeal_title() {
        return deal_title;
    }

    public void setDeal_title(String deal_title) {
        this.deal_title = deal_title;
    }

    public String getBusiness_name() {
        return business_name;
    }

    public void setBusiness_name(String business_name) {
        this.business_name = business_name;
    }

    public long getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    @Override
    public String toString() {
        return "NotificationModel{" +
                "status=" + status +
                ", reading_status=" + reading_status +
                ", notification_type=" + notification_type +
                ", purchase_id=" + purchase_id +
                ", user_id=" + user_id +
                ", payment_id=" + payment_id +
                ", message='" + message + '\'' +
                ", vendor_name='" + vendor_name + '\'' +
                ", vendor_image='" + vendor_image + '\'' +
                ", deal_image='" + deal_image + '\'' +
                ", deal_type='" + deal_type + '\'' +
                ", vendor_id='" + vendor_id + '\'' +
                ", deal_id='" + deal_id + '\'' +
                ", membership_id='" + membership_id + '\'' +
                ", membership_name='" + membership_name + '\'' +
                ", membership_amount='" + membership_amount + '\'' +
                ", month_of_membership='" + month_of_membership + '\'' +
                ", membership_description='" + membership_description + '\'' +
                ", membership_status='" + membership_status + '\'' +
                ", deal_title='" + deal_title + '\'' +
                ", business_name='" + business_name + '\'' +
                ", created='" + created + '\'' +
                ", id=" + id +
                '}';
    }

    protected NotificationModel(Parcel in) {
        status = in.readInt();
        reading_status = in.readInt();
        notification_type = in.readInt();
        purchase_id = in.readInt();
        user_id = in.readInt();
        payment_id = in.readInt();
        message = in.readString();
        vendor_name = in.readString();
        vendor_image = in.readString();
        deal_image = in.readString();
        deal_type = in.readString();
        vendor_id = in.readString();
        deal_id = in.readString();
        membership_id = in.readString();
        membership_name = in.readString();
        membership_amount = in.readString();
        month_of_membership = in.readString();
        membership_description = in.readString();
        membership_status = in.readString();
        deal_title = in.readString();
        business_name = in.readString();
        created = in.readString();
        id = in.readInt();
    }

    @Override
    public void writeToParcel(Parcel dest, int flags) {
        dest.writeInt(status);
        dest.writeInt(reading_status);
        dest.writeInt(notification_type);
        dest.writeInt(purchase_id);
        dest.writeInt(user_id);
        dest.writeInt(payment_id);
        dest.writeString(message);
        dest.writeString(vendor_name);
        dest.writeString(vendor_image);
        dest.writeString(deal_image);
        dest.writeString(deal_type);
        dest.writeString(vendor_id);
        dest.writeString(deal_id);
        dest.writeString(membership_id);
        dest.writeString(membership_name);
        dest.writeString(membership_amount);
        dest.writeString(month_of_membership);
        dest.writeString(membership_description);
        dest.writeString(membership_status);
        dest.writeString(deal_title);
        dest.writeString(business_name);
        dest.writeString(created);
        dest.writeInt(id);
    }

    @Override
    public int describeContents() {
        return 0;
    }

    public static final Creator<NotificationModel> CREATOR = new Creator<NotificationModel>() {
        @Override
        public NotificationModel createFromParcel(Parcel in) {
            return new NotificationModel(in);
        }

        @Override
        public NotificationModel[] newArray(int size) {
            return new NotificationModel[size];
        }
    };


    public NotificationModel() {
    }


}
