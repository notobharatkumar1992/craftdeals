package com.craftedeals.Models;

import android.os.Parcel;
import android.os.Parcelable;

/**
 * Created by Heena on 01-Dec-16.
 */

public class ItemsModel implements Parcelable {

    public int id, adhocdeal_id, actual_cost, price, discount_cost,available_deal,validation_type;
    public String name, details, created, item_count;

    public String total_amount, no_of_item;

    public ItemsModel() {
    }

    @Override
    public String toString() {
        return "ItemsModel{" +
                "id=" + id +
                ", adhocdeal_id=" + adhocdeal_id +
                ", actual_cost=" + actual_cost +
                ", price=" + price +
                ", discount_cost=" + discount_cost +
                ", available_deal=" + available_deal +
                ", validation_type=" + validation_type +
                ", name='" + name + '\'' +
                ", details='" + details + '\'' +
                ", created='" + created + '\'' +
                ", item_count='" + item_count + '\'' +
                ", total_amount='" + total_amount + '\'' +
                ", no_of_item='" + no_of_item + '\'' +
                '}';
    }

    protected ItemsModel(Parcel in) {
        id = in.readInt();
        adhocdeal_id = in.readInt();
        actual_cost = in.readInt();
        price = in.readInt();
        discount_cost = in.readInt();
        available_deal = in.readInt();
        validation_type = in.readInt();
        name = in.readString();
        details = in.readString();
        created = in.readString();
        item_count = in.readString();
        total_amount = in.readString();
        no_of_item = in.readString();
    }

    @Override
    public void writeToParcel(Parcel dest, int flags) {
        dest.writeInt(id);
        dest.writeInt(adhocdeal_id);
        dest.writeInt(actual_cost);
        dest.writeInt(price);
        dest.writeInt(discount_cost);
        dest.writeInt(available_deal);
        dest.writeInt(validation_type);
        dest.writeString(name);
        dest.writeString(details);
        dest.writeString(created);
        dest.writeString(item_count);
        dest.writeString(total_amount);
        dest.writeString(no_of_item);
    }

    @Override
    public int describeContents() {
        return 0;
    }

    public static final Creator<ItemsModel> CREATOR = new Creator<ItemsModel>() {
        @Override
        public ItemsModel createFromParcel(Parcel in) {
            return new ItemsModel(in);
        }

        @Override
        public ItemsModel[] newArray(int size) {
            return new ItemsModel[size];
        }
    };
}
