package com.craftedeals.Models;

import android.os.Parcel;
import android.os.Parcelable;

/**
 * Created by admin on 28-07-2016.
 */
public class Fb_detail_GetSet implements Parcelable {
    public String social_id, gid, file_path;
    public String firstname, last_name, emailid, gender, bitrhday, name, profile_pic, city, heard_about_us;

    @Override
    public String toString() {
        return "Fb_detail_GetSet{" +
                "social_id='" + social_id + '\'' +
                ", gid='" + gid + '\'' +
                ", file_path='" + file_path + '\'' +
                ", firstname='" + firstname + '\'' +
                ", last_name='" + last_name + '\'' +
                ", emailid='" + emailid + '\'' +
                ", gender='" + gender + '\'' +
                ", bitrhday='" + bitrhday + '\'' +
                ", name='" + name + '\'' +
                ", profile_pic='" + profile_pic + '\'' +
                ", city='" + city + '\'' +
                ", heard_about_us='" + heard_about_us + '\'' +
                '}';
    }

    protected Fb_detail_GetSet(Parcel in) {
        social_id = in.readString();
        gid = in.readString();
        file_path = in.readString();
        firstname = in.readString();
        last_name = in.readString();
        emailid = in.readString();
        gender = in.readString();
        bitrhday = in.readString();
        name = in.readString();
        profile_pic = in.readString();
        city = in.readString();
        heard_about_us = in.readString();
    }

    @Override
    public void writeToParcel(Parcel dest, int flags) {
        dest.writeString(social_id);
        dest.writeString(gid);
        dest.writeString(file_path);
        dest.writeString(firstname);
        dest.writeString(last_name);
        dest.writeString(emailid);
        dest.writeString(gender);
        dest.writeString(bitrhday);
        dest.writeString(name);
        dest.writeString(profile_pic);
        dest.writeString(city);
        dest.writeString(heard_about_us);
    }

    @Override
    public int describeContents() {
        return 0;
    }

    public static final Creator<Fb_detail_GetSet> CREATOR = new Creator<Fb_detail_GetSet>() {
        @Override
        public Fb_detail_GetSet createFromParcel(Parcel in) {
            return new Fb_detail_GetSet(in);
        }

        @Override
        public Fb_detail_GetSet[] newArray(int size) {
            return new Fb_detail_GetSet[size];
        }
    };

    public String getProfile_pic() {
        return profile_pic;
    }

    public String getCity() {
        return city;
    }

    public void setCity(String city) {
        this.city = city;
    }

    public String getFile_path() {
        return file_path;
    }

    public void setFile_path(String file_path) {
        this.file_path = file_path;
    }

    public String getHeard_about_us() {
        return heard_about_us;
    }

    public void setHeard_about_us(String heard_about_us) {
        this.heard_about_us = heard_about_us;
    }

    public void setProfile_pic(String profile_pic) {
        this.profile_pic = profile_pic;
    }

    public Fb_detail_GetSet() {
    }


    public String getSocial_id() {
        return social_id;
    }

    public void setSocial_id(String social_id) {
        this.social_id = social_id;
    }

    public String getGid() {
        return gid;
    }

    public void setGid(String gid) {
        this.gid = gid;
    }

    public String getFirstname() {
        return firstname;
    }

    public void setFirstname(String firstname) {
        this.firstname = firstname;
    }

    public String getLast_name() {
        return last_name;
    }

    public void setLast_name(String last_name) {
        this.last_name = last_name;
    }

    public String getEmailid() {
        return emailid;
    }

    public void setEmailid(String emailid) {
        this.emailid = emailid;
    }

    public String getGender() {
        return gender;
    }

    public void setGender(String gender) {
        this.gender = gender;
    }

    public String getBitrhday() {
        return bitrhday;
    }

    public void setBitrhday(String bitrhday) {
        this.bitrhday = bitrhday;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }


}
