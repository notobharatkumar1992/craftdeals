package com.craftedeals;

import android.annotation.SuppressLint;
import android.app.Activity;
import android.app.Application;
import android.app.Dialog;
import android.app.ProgressDialog;
import android.content.ActivityNotFoundException;
import android.content.ContentValues;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.pm.PackageInfo;
import android.content.pm.PackageManager;
import android.content.pm.Signature;
import android.content.res.Resources;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.graphics.Canvas;
import android.graphics.Matrix;
import android.graphics.Paint;
import android.graphics.PorterDuff;
import android.graphics.PorterDuffXfermode;
import android.graphics.Rect;
import android.graphics.RectF;
import android.graphics.drawable.AnimationDrawable;
import android.graphics.drawable.ColorDrawable;
import android.graphics.drawable.Drawable;
import android.net.ConnectivityManager;
import android.net.NetworkInfo;
import android.net.Uri;
import android.os.Bundle;
import android.os.Environment;
import android.os.Handler;
import android.os.StrictMode;
import android.provider.MediaStore;
import android.renderscript.Allocation;
import android.renderscript.Element;
import android.renderscript.RenderScript;
import android.renderscript.ScriptIntrinsicBlur;
import android.support.multidex.MultiDex;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentActivity;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentTransaction;
import android.support.v7.app.AlertDialog;
import android.telephony.TelephonyManager;
import android.util.Base64;
import android.util.DisplayMetrics;
import android.util.Log;
import android.util.TypedValue;
import android.view.View;
import android.view.ViewGroup;
import android.view.Window;
import android.view.inputmethod.InputMethodManager;
import android.widget.EditText;
import android.widget.GridView;
import android.widget.ImageView;
import android.widget.ListAdapter;
import android.widget.ListView;
import android.widget.RelativeLayout;
import android.widget.Toast;

import com.craftedeals.Models.CategoryImageModel;
import com.craftedeals.Models.PostAysnc_Model;
import com.craftedeals.Utils.Prefs;
import com.craftedeals.Utils.ScalingUtilities;
import com.craftedeals.constants.ServerRequestConstants;
import com.craftedeals.constants.Tags;
import com.craftedeals.interfaces.OnDialogClickListener;
import com.craftedeals.parser.JSONParser;
import com.crashlytics.android.Crashlytics;
import com.google.android.gms.analytics.GoogleAnalytics;
import com.google.android.gms.analytics.HitBuilders;
import com.google.android.gms.analytics.Tracker;
import com.google.android.gms.maps.CameraUpdateFactory;
import com.google.android.gms.maps.GoogleMap;
import com.google.android.gms.maps.MapFragment;
import com.google.android.gms.maps.SupportMapFragment;
import com.google.android.gms.maps.model.BitmapDescriptorFactory;
import com.google.android.gms.maps.model.CameraPosition;
import com.google.android.gms.maps.model.LatLng;
import com.google.android.gms.maps.model.MarkerOptions;
import com.paypal.android.sdk.payments.PayPalConfiguration;

import org.apache.http.params.BasicHttpParams;
import org.apache.http.params.HttpConnectionParams;
import org.apache.http.params.HttpParams;
import org.json.JSONObject;

import java.io.BufferedReader;
import java.io.ByteArrayOutputStream;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.io.UnsupportedEncodingException;
import java.security.MessageDigest;
import java.security.NoSuchAlgorithmException;
import java.text.DateFormat;
import java.text.DecimalFormat;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.TimeZone;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

import io.fabric.sdk.android.Fabric;

import static com.craftedeals.activities.DealsActivity.DEAL_HERO;

public class AppDelegate extends Application
{
    public static ArrayList<CategoryImageModel> arrayBitmap = new ArrayList<>();
    public static final int LOGIN_FROM_VENDOR_PROFILE = 2;
    public static final int DEVICE_TYPE = 2;

    public static final int DATA_TYPE_FB = 1, DATA_TYPE_SIGNUP = 2, DATA_TYPE_PROFILE = 3;

    // Note: Your consumer key and secret should be obfuscated in your source code before shipping.
    private static final String TWITTER_KEY = "hCbYWLiVvDWK6181hQreXzrk8";
    private static final String TWITTER_SECRET = "1OtsU61sHn62Xdbo4yz2RMmq22z3hudVycjwxy4fRQpnyz5kWe";


    // Socket server url
    //    public static final String CHAT_SERVER_URL = "http://chat.socket.io";

    // For you tube
    public static final String DEVELOPER_KEY = "AIzaSyAT7e3Bw_myM8l7Qit9eF7gR4Yu_8-hPu0";

    public static final int PRODUCT_MY_PROFILE = 1, PRODUCT_MY_LISTING = 2, PRODUCT_DASHBOARD = 3, PRODUCT_NOTIFICATION = 4, PRODUCT_CHAT = 5, PRODUCT_MAKEOFFER = 5;
    public static final int FROM_JOBS = 1, FROM_INTERNSHIP = 2;

    public static final float MAP_ZOOM = 17, TILT = 25;
    public static final int CAPTURE_IMAGE_FULLSIZE_ACTIVITY_REQUEST_CODE = 1777;
    public static final int IMAGE_MAX_SIZE = 2048;
    public static final int SELECT_PICTURE = 222;
    public static final int PIC_CROP = 333;
    public static boolean CHAT_PAGE_RESUME = false;
    public static int frame_layout_height = 0;
    public static ProgressDialog mProgressDialog;
    public static boolean zeroResult = false;
    public static MarkerOptions markerOptions;
    public static DisplayMetrics metrics;

    // by me h
    public static String API_KEY_4 = "AIzaSyBrBqsVSkmZ_OIuI-I-Vd8mJCZ297j8tXo";
    // by me
    public static String API_KEY_1 = "AIzaSyBue6xSS-BTr8upwyyi6ae3NWcE22OeFSw";
    // created by pritam sir
    public static String API_KEY_2 = "AIzaSyC7hYR1hv6sEn5sq05jBeSn4mE7jrjqczk";
    // by me and REQUEST_DENIED for place search
    public static String API_KEY_3 = "AIzaSyCbLy-PxiSQjd5aAGgxBkULK1B9Dk3dGK8";
    // from my email for distance matrix admin side
    public static String API_ACCESS_KEY = "AIzaSyAkpr9y0YAkyQorrjCJ2f-WLEL8E5O7DUs";
    public static String[] fbPermissions = {"public_profile", "email", "user_birthday"};
    public static Bitmap bitmapMask;
    public static Bitmap bitmapCache;
    public static AppDelegate mInstance;
    public static AlertDialog.Builder mAlert;
    public static DisplayMetrics displayMetrics;
    public static final int LIKE = 1, DISLIKE = 2;
    public static Prefs prefs;
    public static final int BUYER = 2, RETAILER = 3;
    private static Dialog builder;
    private static Tracker mTracker;
    ///////////////////////
    /**
     * - Set to PayPalConfiguration.ENVIRONMENT_PRODUCTION to move real money.
     * <p>
     * - Set to PayPalConfiguration.ENVIRONMENT_SANDBOX to use your test credentials
     * from https://developer.paypal.com
     * <p>
     * - Set to PayPalConfiguration.ENVIRONMENT_NO_NETWORK to kick the tires
     * without communicating to PayPal's servers.
     */
    //public static final String CONFIG_ENVIRONMENT = PayPalConfiguration.ENVIRONMENT_NO_NETWORK;
    public static final String CONFIG_ENVIRONMENT = PayPalConfiguration.ENVIRONMENT_SANDBOX;

    // note that these credentials will differ between live & sandbox environments.
//    private static final String CONFIG_CLIENT_ID = "credentials from developer.paypal.com";
    // public static final String CONFIG_CLIENT_ID = "ATtVObM1flYJzsSHK-6lQ5IiRbkMFCNjEJ2OtEa3e2CCZtNAleeKFbvK0oKF4VLiqQo0FhWCOQLqfzyv";
    public static final String CONFIG_CLIENT_ID = "Aelf3dve_uNP6VIIwhTRsL4PB1FL4BLw9KcAEmL5oXqkGWomtxLxL5tRDak-4euch_1HecL6Mfa-DdIH";
    //paypal new live api key
    //public static final String CONFIG_CLIENT_ID = "AWm8CM0fVf8x8E7rzFI4M7gy_-bG2yYQw3aDwcjDT0EwzbXFxhyACsnfhH2XCvVhBdix0AvK6wchNeD5";


    public static final int REQUEST_CODE_PAYMENT = 1;
    public static final int REQUEST_CODE_FUTURE_PAYMENT = 2;
    public static final int REQUEST_CODE_PROFILE_SHARING = 3;

    public static PayPalConfiguration config = new PayPalConfiguration()
            .environment(CONFIG_ENVIRONMENT)
            .clientId(CONFIG_CLIENT_ID)
            // The following are only used in PayPalFuturePaymentActivity.
            .merchantName("Craftdeal")
            .merchantPrivacyPolicyUri(Uri.parse("https://www.example.com/privacy"))
            .merchantUserAgreementUri(Uri.parse("https://www.example.com/legal"));
////////////////////////////

    public static boolean CheckEmail(String email)
    {
        String EMAIL_PATTERN = "^[_A-Za-z0-9-\\+]+(\\.[_A-Za-z0-9-]+)*@"
                + "[A-Za-z0-9]+(\\.[A-Za-z0-9]+)*(\\.[A-Za-z]{2,})$";

        Pattern pattern = Pattern.compile(EMAIL_PATTERN);
        Matcher matcher = pattern.matcher(email);
        return matcher.matches();
    }

    public static synchronized Prefs getPrefsInstance(Context mContext)
    {
        if (prefs == null)
        {
            prefs = new Prefs(mContext);
        }
        return prefs;
    }

    public static boolean haveNetworkConnection(Context mContext)
    {
        return haveNetworkConnection(mContext, true);
    }

    public static String distance(double lat1, double lon1, double lat2, double lon2)
    {
        double theta = lon1 - lon2;
        double dist = Math.sin(deg2rad(lat1))
                * Math.sin(deg2rad(lat2))
                + Math.cos(deg2rad(lat1))
                * Math.cos(deg2rad(lat2))
                * Math.cos(deg2rad(theta));
        dist = Math.acos(dist);
        dist = rad2deg(dist);
        dist = dist * 60 * 1.1515;
        DecimalFormat df = new DecimalFormat("0.##");
        return (df.format(dist));
    }

    public static double deg2rad(double deg)
    {
        return (deg * Math.PI / 180.0);
    }

    public static double rad2deg(double rad)
    {
        return (rad * 180.0 / Math.PI);
    }

    public static boolean haveNetworkConnection(Context mContext, boolean showAlert)
    {
        boolean isConnected = false;
        boolean haveConnectedWifi = false;
        boolean haveConnectedMobile = false;

        if (mContext == null)
        {
            return false;
        }
        else
        {
            ConnectivityManager cm = (ConnectivityManager) mContext
                    .getSystemService(Context.CONNECTIVITY_SERVICE);
            NetworkInfo[] netInfo = cm.getAllNetworkInfo();
            for (NetworkInfo ni : netInfo)
            {
                if (ni.getTypeName().equalsIgnoreCase("WIFI"))
                {
                    if (ni.isConnected())
                    {
                        haveConnectedWifi = true;
                    }
                }
                if (ni.getTypeName().equalsIgnoreCase("MOBILE"))
                {
                    if (ni.isConnected())
                    {
                        haveConnectedMobile = true;
                    }
                }
            }
            isConnected = haveConnectedWifi || haveConnectedMobile;
            if (isConnected)
            {
                return isConnected;
            }
            else
            {
                if (showAlert)
                {
                    AppDelegate.showToast(mContext, mContext.getResources().getString(R.string.active_internet_connection));
                }
            }
            return isConnected;
        }
    }

    public static void showToast(Context mContext, String Message)
    {
        try
        {
            if (mContext != null)
            {
                Toast.makeText(mContext, Message, Toast.LENGTH_SHORT).show();
            }
            else
            {
                Log.e("tag", "context is null at showing toast.");
            }
        }
        catch (Exception e)
        {
            Log.e("tag", "context is null at showing toast.", e);
        }
    }

    public static void showToast(Context mContext, String Message, int type)
    {
        try
        {
            if (mContext != null)
            {
                if (type == 0)
                {
                    Toast.makeText(mContext, Message, Toast.LENGTH_SHORT).show();
                }
                else
                {
                    Toast.makeText(mContext, Message, Toast.LENGTH_LONG).show();
                }
            }
            else
            {
                Log.e("tag", "context is null at showing toast.");
            }
        }
        catch (Exception e)
        {
            Log.e("tag", "context is null at showing toast.", e);
        }
    }


    public static void showProgressDialog(Activity mContext)
    {
        showProgressDialog(mContext, "", "Loading...");
    }

    public static void showProgressDialog(Activity mContext, String mTitle,
                                          String mMessage)
    {
        AppDelegate.hideKeyBoard(mContext);
        try
        {
            if (mContext != null)
            {
                if (mProgressDialog != null && mProgressDialog.isShowing())
                {
                    return;
                }
                mProgressDialog = new ProgressDialog(mContext);
                mProgressDialog.setCancelable(false);
                mProgressDialog.setTitle(mTitle);
                mProgressDialog.setMessage(mMessage);
                if (mProgressDialog != null && mProgressDialog.isShowing())
                {
                    mProgressDialog.dismiss();
                    mProgressDialog.show();
                }
                else
                {
                    mProgressDialog.show();
                }
            }
            else
            {
                Log.d("tag", "showProgressDialog instense is null");
            }
        }
        catch (Exception e)
        {
            AppDelegate.LogE(e);
        }

    }

    public static void showProgressDialog2(Activity mContext)
    {
        AppDelegate.hideKeyBoard(mContext);
        try
        {
            if (mContext != null)
            {
                if (mProgressDialog != null && mProgressDialog.isShowing())
                {
                    return;
                }
                mProgressDialog = new ProgressDialog(mContext);
                mProgressDialog.setCancelable(false);
                if (mProgressDialog != null && mProgressDialog.isShowing())
                {
                    mProgressDialog.dismiss();
                    mProgressDialog.show();
                }
                else
                {
                    mProgressDialog.show();
                }
            }
            else
            {
                Log.d("tag", "showProgressDialog instense is null");
            }
        }
        catch (Exception e)
        {
            AppDelegate.LogE(e);
        }

    }

    public static void hideProgressDialog(Context mContext)
    {
        try
        {
            if (mContext != null)
            {
                if (mProgressDialog != null && mProgressDialog.isShowing())
                {
                    mProgressDialog.dismiss();
                }
                else
                {
                    mProgressDialog = new ProgressDialog(mContext);
                    mProgressDialog.dismiss();
                }
            }
            else
            {
                Log.d("tag", " hide instense is null");
            }
        }
        catch (Exception e)
        {
            AppDelegate.LogE(e);
        }
    }

    public static void showFragment(FragmentActivity mContext,
                                    Fragment mFragment)
    {

        try
        {
            FragmentTransaction mFragmentTransaction = mContext
                    .getSupportFragmentManager()
                    .beginTransaction();
            mFragmentTransaction.replace(R.id.main_content, mFragment)
                    .addToBackStack(null).commitAllowingStateLoss();
            hideKeyBoard(mContext);
        }
        catch (Exception e)
        {
            AppDelegate.LogE(e);
        }
    }

    public static void showFragment(FragmentActivity mContext,
                                    Fragment mFragment, int fragmentId, Bundle mBundle, String TAG)
    {

        if (mBundle != null && mContext != null)
        {
            mFragment.setArguments(mBundle);
        }
        try
        {
            FragmentTransaction mFragmentTransaction = mContext
                    .getSupportFragmentManager()
                    .beginTransaction();


            mFragmentTransaction.replace(fragmentId, mFragment, TAG)
                    .addToBackStack(TAG).commitAllowingStateLoss();
            hideKeyBoard(mContext);
        }
        catch (Exception e)
        {
            AppDelegate.LogE(e);
        }
    }

    /**
     * Method to Hide Soft Input Keyboard
     *
     * @param mContext
     * @param view
     */

    public static void HideKeyboardMain(Activity mContext, View view)
    {
        try
        {
            InputMethodManager imm = (InputMethodManager) mContext
                    .getSystemService(Context.INPUT_METHOD_SERVICE);
            // R.id.search_img
            imm.hideSoftInputFromWindow(view.getWindowToken(), 0);
        }
        catch (Exception e)
        {
            //Utility.debug(1, TAG, "Exception in executing HideKeyboardMain()");
            AppDelegate.LogE(e);
        }
    }

    public static void openKeyboard(Activity mActivity, View view)
    {
        try
        {
            final InputMethodManager imm = (InputMethodManager) mActivity
                    .getSystemService(Context.INPUT_METHOD_SERVICE);
            //imm.showSoftInput(view, InputMethodManager.SHOW_IMPLICIT);
            imm.showSoftInput(view, InputMethodManager.SHOW_FORCED);
        }
        catch (Exception e)
        {
            AppDelegate.LogE(e);
        }
    }

    /**
     * @param TAG
     * @param Message
     * @param LogType
     */
    public static void Log(String TAG, String Message, int LogType)
    {
        switch (LogType)
        {
            // Case 1- To Show Message as Debug
            case 1:
                Log.d(TAG, Message);
                break;
            // Case 2- To Show Message as Error
            case 2:
                Log.e(TAG, Message);
                break;
            // Case 3- To Show Message as Information
            case 3:
                Log.i(TAG, Message);
                break;
            // Case 4- To Show Message as Verbose
            case 4:
                Log.v(TAG, Message);
                break;
            // Case 5- To Show Message as Assert
            case 5:
                Log.w(TAG, Message);
                break;
            // Case Default- To Show Message as System Print
            default:
                System.out.println(Message);
                break;
        }
    }

    public static void Log(String TAG, String Message)
    {
        AppDelegate.Log(TAG, Message, 1);
    }

    /* Function to show log for error message */
    public static void LogD(String Message)
    {
        AppDelegate.Log(Tags.DATE, "" + Message, 1);
    }

    /* Function to show log for error message */
    public static void LogDB(String Message)
    {
        AppDelegate.Log(Tags.DATABASE, "" + Message, 1);
    }

    /* Function to show log for error message */
    public static void LogE(Exception e)
    {
        if (e != null)
        {
            AppDelegate.LogE(e.getMessage());
            e.printStackTrace();
        }
        else
        {
            AppDelegate.Log(Tags.ERROR, "exception object is also null.", 2);
        }
    }

    /* Function to show log for error message */
    public static void LogE(OutOfMemoryError e)
    {
        if (e != null)
        {
            AppDelegate.LogE(e.getMessage());
            e.printStackTrace();
        }
        else
        {
            AppDelegate.Log(Tags.ERROR, "exception object is also null.", 2);
        }
    }

    /* Function to show log for error message */
    public static void LogE(String message, Exception exception)
    {
        if (exception != null)
        {
            AppDelegate.LogE("from = " + message + " => "
                    + exception.getMessage());
            exception.printStackTrace();
        }
        else
        {
            AppDelegate.Log(Tags.ERROR, "exception object is also null. at "
                    + message, 2);
        }
    }

    /**
     * Funtion to log with tag RESULT, you need to just pass the message.
     *
     * @String Message = pass your message that you want to log.
     */
    public static void LogR(String Message)
    {
        AppDelegate.Log(Tags.RESULT, "" + Message, 1);
    }

    /**
     * Funtion to log with tag RESULT, you need to just pass the message.
     *
     * @String Message = pass your message that you want to log.
     */
    public static void LogI(String Message)
    {
        AppDelegate.Log(Tags.INTERNET, "" + Message, 1);
    }

    /**
     * Funtion to log with tag ERROR, you need to just pass the message. This
     * method is used to exeception .
     *
     * @String Message = pass your message that you want to log.
     */
    public static void LogE(String Message)
    {
        AppDelegate.Log(Tags.ERROR, "" + Message, 2);
    }

    /**
     * Funtion to log with tag your name, you need to just pass the message.
     * This method is used to log url of your api calling.
     *
     * @String Message = pass your message that you want to log.
     */
    public static void LogB(String Message)
    {
        AppDelegate.Log(Tags.BHARAT, "" + Message, 1);
    }

    /**
     * Funtion to log with tag URL, you need to just pass the message. This
     * method is used to log url of your api calling.
     *
     * @String Message = pass your message that you want to log.
     */
    public static void LogU(String Message)
    {
        AppDelegate.Log(Tags.URL, "" + Message, 1);
    }

    /**
     * Funtion to log with tag URL_API, you need to just pass the message. This
     * method is used to log url of your api calling.
     *
     * @String Message = pass your message that you want to log.
     */
    public static void LogUA(String Message)
    {
        AppDelegate.Log(Tags.URL_API, "" + Message, 1);
    }

    /**
     * Funtion to log with tag URL_POST, you need to just pass the message. This
     * method is used to log post param of your api calling.
     *
     * @String Message = pass your message that you want to log.
     */
    public static void LogUP(String Message)
    {
        AppDelegate.Log(Tags.URL_POST, "" + Message, 1);
    }

    /**
     * Funtion to log with tag URL_RESPONSE, you need to just pass the message.
     * This method is used to log response of your api calling.
     *
     * @String Message = pass your message that you want to log.
     */
    public static void LogUR(String Message)
    {
        AppDelegate.Log(Tags.URL_RESPONSE, "URL_RESPONSE " + Message, 1);
    }

    /**
     * Funtion to log with tag TEST, you need to just pass the message.
     *
     * @String Message = pass your message that you want to log.
     */
    public static void LogT(String Message)
    {
        AppDelegate.Log(Tags.TEST, "" + Message, 1);
    }

    /**
     * Funtion to log with tag TEST, you need to just pass the message.
     *
     * @String Message = pass your message that you want to log.
     */
    public static void LogPN(String Message)
    {
        AppDelegate.Log(Tags.PUBNUB, "" + Message, 1);
    }

    /**
     * Funtion to log with tag TEST, you need to just pass the message.
     *
     * @String Message = pass your message that you want to log.
     */
    public static void LogCh(String Message)
    {
        AppDelegate.Log("check", "" + Message, 1);
    }

    public static void LogTR(String Message)
    {
        AppDelegate.Log(Tags.TRACKER, "" + Message, 1);
    }

    /**
     * Funtion to log with tag TEST, you need to just pass the message.
     *
     * @String Message = pass your message that you want to log.
     */
    public static void LogF(String Message)
    {
        AppDelegate.Log(Tags.FORMATED, "" + Message, 1);
    }

    /**
     * Funtion to log with tag TEST, you need to just pass the message.
     *
     * @String Message = pass your message that you want to log.
     */
    public static void LogFB(String Message)
    {
        AppDelegate.Log(Tags.FACEBOOK, "" + Message, 1);
    }

    /**
     * Funtion to log with tag TEST, you need to just pass the message.
     *
     * @String Message = pass your message that you want to log.
     */
    public static void LogS(String Message)
    {
        AppDelegate.Log(Tags.SERVICE, "" + Message, 1);
    }

    /**
     * Funtion to log with tag TEST, you need to just pass the message.
     *
     * @Message = pass your message that you want to log.
     * @int type = you need to pass int value to print in different color. 0 =
     * default color; 1 = fro print in exception style in red color; 2 =
     * info style in orange color;
     */
    public static void LogT(String Message, int type)
    {
        AppDelegate.Log(Tags.TEST, "" + Message, type);
    }

    /**
     * Funtion to log with tag PREF, you need to just pass the message.
     *
     * @String Message = pass your message that you want to log.
     */
    public static void LogP(String Message)
    {
        AppDelegate.Log(Tags.PREF, "" + Message, 1);
    }

    /**
     * Funtion to log with tag PREF, you need to just pass the message.
     *
     * @String Message = pass your message that you want to log.
     */
    public static void LogPU(String Message)
    {
        AppDelegate.Log(Tags.PUSHER, "" + Message, 1);
    }

    /**
     * Funtion to log with tag GCM, you need to just pass the message.
     *
     * @String Message = pass your message that you want to log.
     */
    public static void LogGC(String Message)
    {
        AppDelegate.Log(Tags.GCM, "" + Message, 1);
    }

    /**
     * Funtion to log with tag Chat, you need to just pass the message.
     *
     * @String Message = pass your message that you want to log.
     */
    public static void LogC(String Message)
    {
        AppDelegate.Log(Tags.CHAT, "" + Message, 1);
    }


    /**
     * Funtion to log with tag GPS, you need to just pass the message.
     *
     * @String Message = pass your message that you want to log.
     */
    public static void LogGP(String Message)
    {
        AppDelegate.Log(Tags.GPS, "" + Message, 1);
    }

    public static void showAlertAccordingResponse(Context mContext, String response)
    {
        if (mContext == null)
        {
            return;
        }
        String str_errorMessage = "", str_errorTitle;
        try
        {
            if (response != null && response.length() > 0)
            {
                JSONObject jsonObject = new JSONObject(response);
                str_errorTitle = JSONParser.getString(jsonObject, "error");
                str_errorMessage = JSONParser.getString(jsonObject, "message");
                if (str_errorMessage.equalsIgnoreCase("No message available"))
                {
                    str_errorMessage = "Sorry we are unable to process your request. Please try again later.";
                }
                if (str_errorMessage != null && str_errorMessage.length() > 0)
                {
                    AppDelegate.showAlert(mContext, str_errorTitle, str_errorMessage, "Close");
                }
                else
                {
                    AppDelegate.showAlert(mContext, "Internet server error, Please try again later.");
                }
            }
        }
        catch (Exception e)
        {
            AppDelegate.showAlert(mContext, "Internet server error, Please try again later.");
            AppDelegate.LogE(e);
        }

    }

    public static void timeoutalert(Context context)
    {
        try
        {
            AlertDialog.Builder alertDialog = new AlertDialog.Builder(context);

            alertDialog.setTitle("Error");
            alertDialog.setMessage("Connection TimeOut");
            alertDialog.setCancelable(false);

            alertDialog.setNeutralButton("OK",
                    new DialogInterface.OnClickListener()
                    {
                        public void onClick(DialogInterface dialog, int which)
                        {
                            dialog.cancel();
                        }
                    });

            alertDialog.show();
        }
        catch (Exception e)
        {
            AppDelegate.LogE(e);
        }
    }

    public static boolean checkEmptyString(String xyz)
    {
        if (xyz == null || xyz.trim().equalsIgnoreCase(""))
        {
            return true;
        }
        else
        {
            return false;
        }
    }

    /**
     * Function to Transaction between Fragments
     */
    public static void loadFragment(FragmentManager fragmentManager,
                                    Fragment fragment)
    {
        try
        {

            if (fragmentManager != null && fragment != null)
            {
                fragmentManager
                        .beginTransaction()
//                        .setCustomAnimations(R.anim.left_in, R.anim.left_out,
//                                R.anim.right_in, R.anim.right_out_1)
                        .replace(R.id.main_content, fragment)
                        .addToBackStack(null).commitAllowingStateLoss();
            }
        }
        catch (Exception e)
        {
            AppDelegate.LogE(e);
        }
    }

    /**
     * Function to Transaction between Fragments
     */
    public static void addFragment(FragmentManager fragmentManager, Fragment fragment)
    {
        AppDelegate.addFragment(fragmentManager, fragment, 1);
    }

    /**
     * Function to Transaction between Fragments
     */
    public static void addFragment(FragmentManager fragmentManager, Fragment fragment, int value)
    {
        try
        {
            Bundle bundle = new Bundle();
            bundle.putString(Tags.FROM, value == 0 ? Tags.LOGIN : Tags.HOME);
            if (fragmentManager != null && fragment != null)
            {
                fragment.setArguments(bundle);
                fragmentManager
                        .beginTransaction()
//                        .setCustomAnimations(R.anim.left_in, R.anim.left_out,
//                                R.anim.right_in, R.anim.right_out_1)
                        .add(R.id.main_content, fragment)
                        .addToBackStack(null).commitAllowingStateLoss();
            }
        }
        catch (Exception e)
        {
            AppDelegate.LogE(e);
        }
    }

    /**
     * Function to Transaction between Fragments
     */
    public static void showFragmentAnimation(FragmentManager fragmentManager,
                                             Fragment fragment)
    {
        try
        {
            if (fragmentManager != null && fragment != null)
            {
                fragmentManager
                        .beginTransaction()
                        .setCustomAnimations(R.anim.left_in, R.anim.left_out,
                                R.anim.right_in, R.anim.right_out_1)
                        .replace(R.id.main_content, fragment)
                        .addToBackStack(null).commit();
            }
        }
        catch (Exception e)
        {
            AppDelegate.LogE(e);
        }
    }

    public static void showFragmentAnimation(FragmentActivity mContext,
                                             Fragment mFragment, int fragmentId, Bundle mBundle, String TAG)
    {

        if (mBundle != null && mContext != null)
        {
            mFragment.setArguments(mBundle);
        }
        try
        {
            if (mContext != null && mFragment != null)
            {
                mContext.getSupportFragmentManager()
                        .beginTransaction()
                        .setCustomAnimations(R.anim.left_in, R.anim.left_out,
                                R.anim.right_in, R.anim.right_out_1)
                        .replace(fragmentId, mFragment)
                        .addToBackStack(null).commit();
            }
            hideKeyBoard(mContext);
        }
        catch (Exception e)
        {
            AppDelegate.LogE(e);
        }
    }

    public static void showFragmentAnimationOppose(FragmentActivity mContext, Fragment mFragment, int fragmentId, Bundle mBundle, String TAG)
    {

        if (mBundle != null && mContext != null)
        {
            mFragment.setArguments(mBundle);
        }
        try
        {
            if (mContext != null && mFragment != null)
            {
                mContext.getSupportFragmentManager()
                        .beginTransaction()
                        .setCustomAnimations(R.anim.right_in, R.anim.right_out_1, R.anim.left_in, R.anim.left_out)
                        .replace(fragmentId, mFragment)
                        .addToBackStack(null).commit();
            }
            hideKeyBoard(mContext);
        }
        catch (Exception e)
        {
            AppDelegate.LogE(e);
        }
    }

    public static void showFragmentAnimationBottomIn(FragmentActivity mContext, Fragment mFragment, int fragmentId, Bundle mBundle, String TAG)
    {
        if (mBundle != null && mContext != null)
        {
            mFragment.setArguments(mBundle);
        }
        try
        {
            if (mContext != null && mFragment != null)
            {
                mContext.getSupportFragmentManager()
                        .beginTransaction()
                        .setCustomAnimations(R.anim.bottom_in, R.anim.left_out, R.anim.right_in, R.anim.right_out_1)
                        .replace(fragmentId, mFragment)
                        .addToBackStack(null).commit();
            }
            hideKeyBoard(mContext);
        }
        catch (Exception e)
        {
            AppDelegate.LogE(e);
        }
    }

    public static void showFragmentAnimation(FragmentActivity mContext, Fragment mFragment, Bundle mBundle, String TAG)
    {
        if (mBundle != null && mContext != null)
        {
            mFragment.setArguments(mBundle);
        }
        try
        {
            if (mContext != null && mFragment != null)
            {
                mContext.getSupportFragmentManager()
                        .beginTransaction()
                        .setCustomAnimations(R.anim.left_in, R.anim.left_out,
                                R.anim.right_in, R.anim.right_out_1)
                        .replace(R.id.main_content, mFragment)
                        .addToBackStack(null).commit();
                hideKeyBoard(mContext);
            }
        }
        catch (Exception e)
        {
            AppDelegate.LogE(e);
        }
    }


    /**
     * Function to Transaction between Fragments
     */
    public static void showFragmentAnimation(FragmentManager fragmentManager,
                                             Fragment fragment, int id)
    {
        try
        {
            if (fragmentManager != null && fragment != null)
            {
                fragmentManager
                        .beginTransaction()
                        .setCustomAnimations(R.anim.left_in, R.anim.left_out,
                                R.anim.right_in, R.anim.right_out_1)
                        .replace(id, fragment)
                        .addToBackStack(null).commit();
            }
        }
        catch (Exception e)
        {
            AppDelegate.LogE(e);
        }
    }

    // for oppose transmission..
    public static void showFragmentAnimationOppose(
            FragmentManager fragmentManager, Fragment fragment)
    {
        try
        {
            fragmentManager
                    .beginTransaction()
                    .setCustomAnimations(R.anim.right_in, R.anim.right_out_1,
                            R.anim.left_in, R.anim.left_out)
                    .replace(R.id.main_content, fragment).addToBackStack(null)
                    .commit();
        }
        catch (Exception e)
        {
            AppDelegate.LogE(e);
        }
    }

    public static boolean isContainAlpha(String name)
    {
        char[] chars = name.toCharArray();
        for (char c : chars)
        {
            if (Character.isLetter(c))
            {
                return true;
            }
        }
        return false;
    }

    public static void hideKeyBoard(final Activity mActivity, long timeAfter)
    {
        if (mActivity != null)
        {
            new Handler().postDelayed(new Runnable()
            {

                @Override
                public void run()
                {
                    AppDelegate.hideKeyBoard(mActivity);
                }
            }, timeAfter);
        }
    }

    public static void hideKeyBoard(Activity mActivity)
    {
        if (mActivity == null)
        {
            return;
        }
        else
        {
            InputMethodManager inputManager = (InputMethodManager) mActivity
                    .getSystemService(Context.INPUT_METHOD_SERVICE);

            // check if no view has focus:
            View v = mActivity.getCurrentFocus();
            if (v == null)
            {
                return;
            }

            AppDelegate.LogT("hideKeyBoard viewNot null");
            inputManager.hideSoftInputFromWindow(v.getWindowToken(),
                    InputMethodManager.HIDE_NOT_ALWAYS);
        }
    }

    public static void animateCamera(GoogleMap map_business, LatLng latLng)
    {
        AppDelegate.animateCamera(map_business, latLng, AppDelegate.TILT, null);
    }

    public static void animateCamera(GoogleMap map_business, LatLng latLng,
                                     float tilt, GoogleMap.CancelableCallback callback)
    {
        if (map_business != null && latLng != null && latLng.latitude != -1.0
                && latLng.latitude != 0.0)
        {
            map_business.animateCamera(CameraUpdateFactory
                    .newCameraPosition(new CameraPosition.Builder()
                            .target(latLng).zoom(AppDelegate.MAP_ZOOM)
                            .bearing(0).tilt(tilt).build()), 500, callback);
        }
        else
        {
            AppDelegate
                    .LogE("at AppDelegate.animateCamera map_business is null.");
        }
    }

    public static MarkerOptions getMarkerOptions(Context mContext,
                                                 LatLng latLng, int icon_id)
    {
        return getMarkerOptions(mContext, latLng, icon_id, 74, 64);
    }

    public static MarkerOptions getMarkerOptions(Context mContext,
                                                 LatLng latLng, int icon_id, String title)
    {
        return getMarkerOptionsWithTitleSnippet(mContext, latLng, icon_id, 74, 64, title);
    }

    public static MarkerOptions getMarkerOptions(Context mContext,
                                                 LatLng latLng, int icon_id, int width, int height)
    {
        markerOptions = new MarkerOptions();
        if (mContext != null)
        {
            try
            {
                if (latLng != null && latLng.latitude != -1.0 && icon_id != -1)
                {
                    markerOptions.icon(BitmapDescriptorFactory
                            .fromBitmap(AppDelegate.convertToBitmap(mContext
                                            .getResources().getDrawable(icon_id),
                                    width, height)));
                    markerOptions.position(latLng);
                }
            }
            catch (Exception e)
            {
                AppDelegate.LogE(e);
            }
        }
        return markerOptions;
    }

    public static MarkerOptions getMarkerOptions(Context mContext, LatLng latLng)
    {
        markerOptions = new MarkerOptions();
        if (mContext != null)
        {
            try
            {
                if (latLng != null && latLng.latitude != -1.0)
                {
                    markerOptions.position(latLng);
                }
            }
            catch (Exception e)
            {
                AppDelegate.LogE(e);
            }
        }
        return markerOptions;
    }

    public static Bitmap createDrawableFromView(Context context, View view)
    {

        DisplayMetrics displayMetrics = new DisplayMetrics();
        ((Activity) context).getWindowManager().getDefaultDisplay().getMetrics(displayMetrics);
        view.setLayoutParams(new RelativeLayout.LayoutParams(RelativeLayout.LayoutParams.WRAP_CONTENT, RelativeLayout.LayoutParams.WRAP_CONTENT));
        view.measure(displayMetrics.widthPixels, displayMetrics.heightPixels);
        view.layout(0, 0, displayMetrics.widthPixels, displayMetrics.heightPixels);
        view.buildDrawingCache();
        Bitmap bitmap = null;
        try
        {
            bitmap = Bitmap.createBitmap(view.getMeasuredWidth(), view.getMeasuredHeight(), Bitmap.Config.ARGB_8888);
            Canvas canvas = new Canvas(bitmap);
            view.draw(canvas);
            AppDelegate.LogT("Bitmap" + bitmap);
        }
        catch (Exception e)
        {
            AppDelegate.LogE(e);
        }
        return bitmap;
    }

    public static String getCurrentTime()
    {
        return new SimpleDateFormat("HH:mm:ss").format(Calendar.getInstance().getTime());
    }

    public static MarkerOptions getMarkerOptionsWithTitleSnippet(
            Context mContext, LatLng latLng, int icon_id, int width,
            int height, String title)
    {
        markerOptions = new MarkerOptions();
        try
        {
            if (icon_id != -1)
            {
                markerOptions.icon(BitmapDescriptorFactory
                        .fromBitmap(AppDelegate.convertToBitmap(mContext
                                        .getResources().getDrawable(icon_id), width,
                                height)));
            }
            markerOptions.position(latLng);
            markerOptions.title(title);
        }
        catch (Exception e)
        {
            AppDelegate.LogE(e);
        }
        return markerOptions;
    }

    public static MarkerOptions getMarkerOptionsWithTitleSnippet(
            Context mContext, LatLng latLng, int icon_id, int width,
            int height, String title, String snippet)
    {
        markerOptions = new MarkerOptions();
        try
        {
            if (icon_id != -1)
            {
                markerOptions.icon(BitmapDescriptorFactory
                        .fromBitmap(AppDelegate.convertToBitmap(mContext
                                        .getResources().getDrawable(icon_id), width,
                                height)));
            }
            markerOptions.position(latLng);
            markerOptions.title(title);
            markerOptions.snippet(snippet);
        }
        catch (Exception e)
        {
            AppDelegate.LogE(e);
        }
        return markerOptions;
    }

    public static Bitmap convertToBitmap(Drawable drawable, int widthPixels,
                                         int heightPixels)
    {
        Bitmap mutableBitmap = Bitmap.createBitmap(widthPixels, heightPixels,
                Bitmap.Config.ARGB_8888);
        Canvas canvas = new Canvas(mutableBitmap);
        drawable.setBounds(0, 0, widthPixels, heightPixels);
        drawable.draw(canvas);
        return mutableBitmap;
    }

    public static String convertStreamToString(InputStream is)
    {
        BufferedReader reader = new BufferedReader(new InputStreamReader(is));
        StringBuilder sb = new StringBuilder();

        String line = null;
        try
        {
            while ((line = reader.readLine()) != null)
            {
                sb.append(line + "\n");
            }
        }
        catch (IOException e)
        {
            AppDelegate.LogE(e);
        }
        finally
        {
            try
            {
                is.close();
            }
            catch (IOException e)
            {
                AppDelegate.LogE(e);
            }
        }
        return sb.toString();
    }

    public static HttpParams getHttpParameters()
    {
        HttpParams httpParameters = new BasicHttpParams();
        // Set the timeout in milliseconds until a connection is established.
        HttpConnectionParams.setConnectionTimeout(httpParameters, 20000);
        // Set the default socket timeout (SO_TIMEOUT)
        // in milliseconds which is the timeout for waiting for data.
        HttpConnectionParams.setSoTimeout(httpParameters, 20000);
        return httpParameters;
    }

    public static Bitmap rotateImage(int angle, Bitmap OriginalPhoto)
    {
        Matrix matrix = new Matrix();
        matrix.postRotate(angle);

        OriginalPhoto = Bitmap.createBitmap(OriginalPhoto, 0, 0,
                OriginalPhoto.getWidth(), OriginalPhoto.getHeight(), matrix,
                true);
        return OriginalPhoto;
    }

//    public static void loadImageFromPicasaWithCornor(Context mContext,
//                                                     ImageView imageView, String str_image, int value) {
//        if (str_image != null && !str_image.equalsIgnoreCase("")
//                && Patterns.WEB_URL.matcher(str_image).matches()) {
//            Bitmap bitmapMask = BitmapFactory.decodeResource(
//                    mContext.getResources(), R.drawable.select);
//            imageView.setImageBitmap(bitmapMask);
//            try {
//
//                Picasso.with(mContext).load(str_image).placeholder(mContext.getResources().getDrawable(R.drawable.select))
//                        .error(mContext.getResources().getDrawable(R.drawable.select))
//                        .transform(new RoundedTransformation(value, 0)).fit().into(imageView);
//
////                Picasso.with(mContext).load(str_image)
////                        .transform(new RoundedTransformation(value, 0)).fit()
////                        .into(imageView);
//
//            } catch (OutOfMemoryError e) {
//                AppDelegate.LogE(e);
//            } catch (Exception e) {
//                AppDelegate.LogE(e);
//            }
//        } else {
//            Bitmap bitmapMask = BitmapFactory.decodeResource(
//                    mContext.getResources(), R.drawable.missionlogo);
//            imageView.setImageBitmap(bitmapMask);
//        }
//    }

    public static int convertdp(Context context, int x)
    {
        DisplayMetrics displayMetrics = context.getResources().getDisplayMetrics();
        int dp = Math.round(x * (displayMetrics.xdpi / DisplayMetrics.DENSITY_DEFAULT));
        return dp;
    }

    public static Bitmap getRoundedCornerBitmap(Bitmap bitmap, int pixels)
    {
        Bitmap output = Bitmap.createBitmap(bitmap.getWidth(), bitmap
                .getHeight(), Bitmap.Config.ARGB_8888);
        Canvas canvas = new Canvas(output);

        final int color = 0xff424242;
        final Paint paint = new Paint();
        final Rect rect = new Rect(0, 0, bitmap.getWidth(), bitmap.getHeight());
        final RectF rectF = new RectF(rect);
        final float roundPx = pixels;

        paint.setAntiAlias(true);
        canvas.drawARGB(0, 0, 0, 0);
        paint.setColor(color);
        canvas.drawRoundRect(rectF, roundPx, roundPx, paint);

        paint.setXfermode(new PorterDuffXfermode(PorterDuff.Mode.SRC_IN));
        canvas.drawBitmap(bitmap, rect, rect, paint);
        return output;
    }

//    public static void loadImageFromPicasa(Context mContext,
//                                           ImageView imageView, String str_image) {
//        if (str_image != null && !str_image.equalsIgnoreCase("")
//                && Patterns.WEB_URL.matcher(str_image).matches()) {
//            try {
//                if (bitmapMask != null && !bitmapMask.isRecycled()) {
//                    bitmapMask.recycle();
//                    bitmapMask = null;
//                }
//                bitmapMask = BitmapFactory.decodeResource(
//                        mContext.getResources(), R.drawable.missionlogo);
//                imageView.setImageBitmap(bitmapMask);
//            } catch (OutOfMemoryError e) {
//                AppDelegate.LogE(e);
//            } catch (Exception e) {
//                AppDelegate.LogE(e);
//            }
//            try {
//                Picasso.with(mContext).load(str_image)
////                        .transform(new CircleTransform()).resize(100, 100)
////                        .memoryPolicy(MemoryPolicy.NO_CACHE)
//                        .into(imageView);
//
//            } catch (OutOfMemoryError e) {
//                AppDelegate.LogE(e);
//            } catch (Exception e) {
//                AppDelegate.LogE(e);
//            }
//        } else {
//            try {
//                if (bitmapMask != null && !bitmapMask.isRecycled()) {
//                    bitmapMask.recycle();
//                    bitmapMask = null;
//                }
//                bitmapMask = BitmapFactory.decodeResource(
//                        mContext.getResources(), R.drawable.missionlogo);
//                imageView.setImageBitmap(bitmapMask);
//            } catch (OutOfMemoryError e) {
//                AppDelegate.LogE(e);
//            } catch (Exception e) {
//                AppDelegate.LogE(e);
//            }
//        }
//    }
//
//    public static void loadImageFromPicasaWithCornorWithNoImage(Context mContext,
//                                                                ImageView imageView, String str_image, int value) {
//        if (str_image != null && !str_image.equalsIgnoreCase("")
//                && Patterns.WEB_URL.matcher(str_image).matches()) {
//            try {
//                Picasso.with(mContext).load(str_image)
//                        .fit().into(imageView);
//            } catch (OutOfMemoryError e) {
//                AppDelegate.LogE(e);
//            } catch (Exception e) {
//                AppDelegate.LogE(e);
//            }
//        } else {
//            try {
//                if (bitmapMask != null && !bitmapMask.isRecycled()) {
//                    bitmapMask.recycle();
//                    bitmapMask = null;
//                }
//                bitmapMask = BitmapFactory.decodeResource(
//                        mContext.getResources(), R.drawable.missionlogo);
//                imageView.setImageBitmap(bitmapMask);
//            } catch (OutOfMemoryError e) {
//                AppDelegate.LogE(e);
//            } catch (Exception e) {
//                AppDelegate.LogE(e);
//            }
//        }
//    }
//
//    public static void loadImageFromPicasaRactangle(Context mContext,
//                                                    ImageView imageView, String str_image) {
//        try {
//            if (bitmapMask != null && !bitmapMask.isRecycled()) {
//                bitmapMask.recycle();
//                bitmapMask = null;
//            }
//            bitmapMask = BitmapFactory.decodeResource(
//                    mContext.getResources(), R.drawable.missionlogo);
//            imageView.setImageBitmap(bitmapMask);
//        } catch (OutOfMemoryError e) {
//            AppDelegate.LogE(e);
//        } catch (Exception e) {
//            AppDelegate.LogE(e);
//        }
//
//        try {
//            if (str_image.equalsIgnoreCase("null") || str_image.equalsIgnoreCase("")) {
//                return;
//            }
//        } catch (Exception e) {
//            AppDelegate.LogE(e);
//            return;
//        }
//
//        if (mContext != null && str_image != null && !str_image.equalsIgnoreCase("")
//                && Patterns.WEB_URL.matcher(str_image).matches()) {
//            try {
//                bitmapMask = BitmapFactory.decodeResource(
//                        mContext.getResources(), R.drawable.missionlogo);
//                imageView.setImageBitmap(bitmapMask);
//                Picasso.with(mContext).load(str_image).into(imageView);
//            } catch (OutOfMemoryError e) {
//                AppDelegate.LogE(e);
//            } catch (Exception e) {
//                AppDelegate.LogE(e);
//            }
//        }
//    }

    /**
     * it will true if your device having mounted SD card else false
     */
    public static boolean isSDcardAvailable()
    {
        return Environment.getExternalStorageState().equals(Environment.MEDIA_MOUNTED);
    }

    public static String getHashKey(Context mContext)
    {
        String str_HashKey = null;
        try
        {
            PackageInfo info = mContext.getPackageManager().getPackageInfo(
                    mContext.getPackageName(),
                    PackageManager.GET_SIGNATURES);
            for (Signature signature : info.signatures)
            {
                MessageDigest md = MessageDigest.getInstance("SHA");
                md.update(signature.toByteArray());
                str_HashKey = Base64.encodeToString(md.digest(), Base64.DEFAULT);
                AppDelegate.LogT("HashKey = " + str_HashKey);
            }
        }
        catch (PackageManager.NameNotFoundException e)
        {
            AppDelegate.LogE(e);
        }
        catch (NoSuchAlgorithmException e)
        {
            AppDelegate.LogE(e);
        }
        return str_HashKey;
    }

    /*
     * Masking of an image
	 */
    public static Bitmap masking(Bitmap original, Bitmap mask)
    {
        original = Bitmap.createScaledBitmap(original, mask.getWidth(),
                mask.getHeight(), true);

        Bitmap result = Bitmap.createBitmap(mask.getWidth(), mask.getHeight(),
                Bitmap.Config.ARGB_8888);
        Canvas mCanvas = new Canvas(result);
        Paint paint = new Paint(Paint.ANTI_ALIAS_FLAG);
        paint.setXfermode(new PorterDuffXfermode(PorterDuff.Mode.DST_IN));
        mCanvas.drawBitmap(original, 0, 0, null);
        mCanvas.drawBitmap(mask, 0, 0, paint);
        paint.setXfermode(null);
        return result;
    }

    /* convert drawable to bitmap */
    public static Bitmap integerToBitmap(Context ctx, Integer integer)
    {

        BitmapFactory.Options o = new BitmapFactory.Options();
        o.inScaled = false;
        o.inJustDecodeBounds = false;
        Bitmap icon = BitmapFactory.decodeResource(ctx.getResources(), integer,
                o);
        icon.setDensity(Bitmap.DENSITY_NONE);
        return icon;
    }

    /*
     * Reduce image size
     */
    public static Bitmap Shrinkmethod(String file, int width, int height)
    {
        BitmapFactory.Options bitopt = new BitmapFactory.Options();
        bitopt.inJustDecodeBounds = true;
        Bitmap bit = BitmapFactory.decodeFile(file, bitopt);

        int h = (int) Math.ceil(bitopt.outHeight / (float) height);
        int w = (int) Math.ceil(bitopt.outWidth / (float) width);

        if (h > 1 || w > 1)
        {
            if (h > w)
            {
                bitopt.inSampleSize = h;

            }
            else
            {
                bitopt.inSampleSize = w;
            }
        }
        bitopt.inJustDecodeBounds = false;
        bit = BitmapFactory.decodeFile(file, bitopt);

        System.out.println("HEIGHT WIDTH:::::::" + bit.getWidth() + "::::"
                + bit.getHeight());

        return bit;

    }

    public static synchronized AppDelegate getInstance(Context mContext)
    {
        if (mInstance == null)
        {
            mInstance = (AppDelegate) mContext.getApplicationContext();
        }
        return mInstance;
    }

    public static String getUUID(Context mContext)
    {
        TelephonyManager tManager = (TelephonyManager) mContext.getSystemService(Context.TELEPHONY_SERVICE);
        AppDelegate.LogD("getUUID = " + tManager.getDeviceId());
        return tManager.getDeviceId();
//        return "359774050367819";
    }

    public static String getBase64String(String value)
    {
        byte[] data;
        String str_base64 = "";
        String str = "xyzstring";
        try
        {
            data = str.getBytes("UTF-8");
            str_base64 = Base64.encodeToString(data, Base64.DEFAULT);
            Log.i("Base 64 ", str_base64);
        }
        catch (UnsupportedEncodingException e)
        {
            AppDelegate.LogE(e);
        }
        return str_base64;
    }

    public static boolean isValidDate(String inDate)
    {
        SimpleDateFormat dateFormat = new SimpleDateFormat("dd-MM-yyyy");
        dateFormat.setLenient(false);
        try
        {
            dateFormat.parse(inDate.trim());
        }
        catch (ParseException pe)
        {
            AppDelegate.LogE(pe);
            return false;
        }
        return true;
    }

    public static boolean setMyLocationBottomRightButton(Activity mActivity,
                                                         SupportMapFragment fragment)
    {
        try
        {
            if (fragment == null)
            {
                return false;
            }
            View myLocationButton = fragment.getView().findViewById(0x2);
            if (myLocationButton != null
                    && myLocationButton.getLayoutParams() instanceof RelativeLayout.LayoutParams)
            {
                // ZoomControl is inside of RelativeLayout

                RelativeLayout.LayoutParams params = (RelativeLayout.LayoutParams) myLocationButton
                        .getLayoutParams();
                // Align it to - parent BOTTOM|LEFT
                params.addRule(RelativeLayout.ALIGN_PARENT_BOTTOM);
                params.addRule(RelativeLayout.ALIGN_PARENT_RIGHT);

                params.addRule(RelativeLayout.ALIGN_PARENT_LEFT, 0);
                params.addRule(RelativeLayout.ALIGN_PARENT_TOP, 0);

                // Update margins, set to 10dp
                final int margin = (int) TypedValue.applyDimension(
                        TypedValue.COMPLEX_UNIT_DIP, 10, mActivity
                                .getResources().getDisplayMetrics());
                params.setMargins(margin, margin, margin, margin);

                myLocationButton.setLayoutParams(params);
            }
            return true;
        }
        catch (Exception e)
        {
            AppDelegate.LogE(e);
            return false;
        }
    }

    public static boolean setMyLocationBottomRightButton(Activity mActivity, MapFragment fragment)
    {
        try
        {
            View myLocationButton = fragment.getView().findViewById(0x2);
            if (myLocationButton != null
                    && myLocationButton.getLayoutParams() instanceof RelativeLayout.LayoutParams)
            {
                // ZoomControl is inside of RelativeLayout

                RelativeLayout.LayoutParams params = (RelativeLayout.LayoutParams) myLocationButton
                        .getLayoutParams();
                // Align it to - parent BOTTOM|LEFT
                params.addRule(RelativeLayout.ALIGN_PARENT_BOTTOM);
                params.addRule(RelativeLayout.ALIGN_PARENT_RIGHT);

                params.addRule(RelativeLayout.ALIGN_PARENT_LEFT, 0);
                params.addRule(RelativeLayout.ALIGN_PARENT_TOP, 0);

                // Update margins, set to 10dp
                final int margin = (int) TypedValue.applyDimension(
                        TypedValue.COMPLEX_UNIT_DIP, 10, mActivity
                                .getResources().getDisplayMetrics());
                params.setMargins(margin, margin, margin, margin);

                myLocationButton.setLayoutParams(params);
            }
            return true;
        }
        catch (Exception e)
        {
            AppDelegate.LogE(e);
            return false;
        }
    }

    public static String getFormatedAddress(String str_value_1, String str_value_2)
    {
        String st_value = "";
        if (isValidString(str_value_1) && isValidString(str_value_2))
        {
            st_value = str_value_1 + " - " + str_value_2;
        }
        else if (isValidString(str_value_1))
        {
            st_value = str_value_1;
        }
        else if (isValidString(str_value_2))
        {
            st_value = str_value_2;
        }
        return st_value;
    }

    public static boolean isValidString(String string)
    {
        if (string != null && !string.equalsIgnoreCase("null") && string.length() > 0)
        {
            return true;
        }
        else
        {
            return false;
        }
    }

    public static boolean isValidDouble(String string)
    {
        try
        {
            if (string != null && !string.equalsIgnoreCase("null") && string.length() > 0 && Double.parseDouble(string) > 1)
            {
                return true;
            }
            else
            {
                AppDelegate.LogT("isValidDouble false => " + string);
                return false;
            }
        }
        catch (Exception e)
        {
            AppDelegate.LogE(e);
            return false;
        }
    }

    public static float getValidFloat(String string)
    {
        float value = 0;
        try
        {
            if (string != null && !string.equalsIgnoreCase("null") && string.length() > 0 && Float.parseFloat(string) > 0)
            {
                value = Float.parseFloat(string);
            }
            else
            {
                AppDelegate.LogT("isValidDouble false => " + string);
                value = 0;
            }
        }
        catch (Exception e)
        {
            AppDelegate.LogE(e);
            value = 0;
        }
        return value;
    }

    public static void showAlert(Context mContext, String Title, String
            Message)
    {
        try
        {
            mAlert = new AlertDialog.Builder(mContext);
            mAlert.setCancelable(false);
            mAlert.setTitle(Title);
            mAlert.setMessage(Message);
            mAlert.setPositiveButton(
                    "Yes",
                    new DialogInterface.OnClickListener()
                    {

                        @Override
                        public void onClick(DialogInterface dialog, int which)
                        {
                            dialog.dismiss();
                        }
                    });
            mAlert.setNegativeButton(
                    "No",
                    new DialogInterface.OnClickListener()
                    {

                        @Override
                        public void onClick(DialogInterface dialog, int which)
                        {
                            dialog.dismiss();
                        }
                    });
            mAlert.show();
        }
        catch (Exception e)
        {
            AppDelegate.LogE(e);
        }
    }

    public static void showAlert(Context mContext, String Message)
    {
        showAlert(mContext, "", Message, "OK");
    }

    public static void showAlert(Context mContext, String Title, String
            Message, String str_button_name)
    {
        try
        {
            mAlert = new AlertDialog.Builder(mContext);
            mAlert.setCancelable(false);
            mAlert.setTitle(Title);
            mAlert.setMessage(Message);
            mAlert.setPositiveButton(
                    str_button_name,
                    new DialogInterface.OnClickListener()
                    {

                        @Override
                        public void onClick(DialogInterface dialog, int which)
                        {
                            dialog.dismiss();
                        }
                    });
            mAlert.show();
        }
        catch (Exception e)
        {
            AppDelegate.LogE(e);
        }
    }

    public static void showAlert(Context mContext, String Title, String
            Message, final String str_button_name, final View.OnClickListener onClickListener)
    {
        try
        {
            mAlert = new AlertDialog.Builder(mContext);
            mAlert.setCancelable(false);
            mAlert.setTitle(Title);
            mAlert.setMessage(Message);
            mAlert.setPositiveButton(
                    str_button_name,
                    new DialogInterface.OnClickListener()
                    {

                        @Override
                        public void onClick(DialogInterface dialog, int which)
                        {
                            if (onClickListener != null)
                            {
                                onClickListener.onClick(null);
                            }
                            dialog.dismiss();
                        }
                    });
            mAlert.show();
        }
        catch (Exception e)
        {
            AppDelegate.LogE(e);
        }
    }

    public static void showAlert(Context mContext, String Title, String
            Message, String str_button_name, final View.OnClickListener onClickListener, String str_button_name_1, final View.OnClickListener onClickListener1)
    {
        try
        {
            mAlert = new AlertDialog.Builder(mContext);
            mAlert.setCancelable(false);
            mAlert.setTitle(Title);
            mAlert.setMessage(Message);
            mAlert.setPositiveButton(
                    str_button_name,
                    new DialogInterface.OnClickListener()
                    {

                        @Override
                        public void onClick(DialogInterface dialog, int which)
                        {
                            if (onClickListener != null)
                            {
                                onClickListener.onClick(null);
                            }
                            dialog.dismiss();
                        }
                    });

            mAlert.setNegativeButton(
                    str_button_name_1,
                    new DialogInterface.OnClickListener()
                    {

                        @Override
                        public void onClick(DialogInterface dialog, int which)
                        {
                            if (onClickListener1 != null)
                            {
                                onClickListener1.onClick(null);
                            }
                            dialog.dismiss();
                        }
                    });
            mAlert.show();
        }
        catch (Exception e)
        {
            AppDelegate.LogE(e);
        }
    }

    public static String decodeFile(String path, int DESIREDWIDTH, int DESIREDHEIGHT)
    {
        String strMyImagePath = null;
        Bitmap scaledBitmap = null;

        try
        {
            // Part 1: Decode image
            Bitmap unscaledBitmap = ScalingUtilities.decodeFile(path, DESIREDWIDTH, DESIREDHEIGHT, ScalingUtilities.ScalingLogic.FIT);

            if (!(unscaledBitmap.getWidth() <= DESIREDWIDTH && unscaledBitmap.getHeight() <= DESIREDHEIGHT))
            {
                // Part 2: Scale image
                scaledBitmap = ScalingUtilities.createScaledBitmap(unscaledBitmap, DESIREDWIDTH, DESIREDHEIGHT, ScalingUtilities.ScalingLogic.FIT);
            }
            else
            {
                unscaledBitmap.recycle();
                return path;
            }

            // Store to tmp file

            String extr = Environment.getExternalStorageDirectory().toString();
            File mFolder = new File(extr + "/Stux");
            if (!mFolder.exists())
            {
                mFolder.mkdir();
            }

            String s = "tmp.png";

            File f = new File(mFolder.getAbsolutePath(), s);

            strMyImagePath = f.getAbsolutePath();
            FileOutputStream fos = null;
            try
            {
                fos = new FileOutputStream(f);
                scaledBitmap.compress(Bitmap.CompressFormat.JPEG, 75, fos);
                fos.flush();
                fos.close();
            }
            catch (FileNotFoundException e)
            {
                AppDelegate.LogE(e);
            }
            catch (Exception e)
            {
                AppDelegate.LogE(e);
            }

            scaledBitmap.recycle();
        }
        catch (Throwable e)
        {
        }

        if (strMyImagePath == null)
        {
            return path;
        }
        return strMyImagePath;
    }

    public static Bitmap decodeFile(File f)
    {
        Bitmap b = null;
        try
        {
            BitmapFactory.Options o = new BitmapFactory.Options();
            o.inJustDecodeBounds = true;

            FileInputStream fis = new FileInputStream(f);
            try
            {
                BitmapFactory.decodeStream(fis, null, o);
            }
            finally
            {
                fis.close();
            }

            int scale = 1;
            for (int size = Math.max(o.outHeight, o.outWidth);
                 (size >> (scale - 1)) > IMAGE_MAX_SIZE; ++scale)
                ;

            // Decode with inSampleSize
            BitmapFactory.Options o2 = new BitmapFactory.Options();
            o2.inSampleSize = scale;
            fis = new FileInputStream(f);
            try
            {
                b = BitmapFactory.decodeStream(fis, null, o2);
            }
            finally
            {
                fis.close();
            }
        }
        catch (IOException e)
        {
            AppDelegate.LogE(e);
        }
        return b;
    }

    public static void lanchAppIfInstalled(Context mContext,
                                           String appPackageName)
    {
        try
        {
            mContext.startActivity(mContext.getPackageManager()
                    .getLaunchIntentForPackage(appPackageName));
        }
        catch (ActivityNotFoundException anfe)
        {
            AppDelegate.LogE(anfe);
            try
            {
                mContext.startActivity(new Intent(Intent.ACTION_VIEW, Uri
                        .parse("market://details?id=" + appPackageName)));
            }
            catch (Exception e)
            {
                AppDelegate.LogE(e);
                mContext.startActivity(new Intent(Intent.ACTION_VIEW, Uri
                        .parse("https://play.google.com/store/apps/details?id="
                                + appPackageName)));
            }
        }
        catch (Exception e)
        {
            AppDelegate.LogE(e);
            try
            {
                mContext.startActivity(new Intent(Intent.ACTION_VIEW, Uri
                        .parse("market://details?id=" + appPackageName)));
            }
            catch (Exception e1)
            {
                AppDelegate.LogE(e1);
                mContext.startActivity(new Intent(Intent.ACTION_VIEW, Uri
                        .parse("https://play.google.com/store/apps/details?id="
                                + appPackageName)));
            }
        }
    }

    public static int dpToPix(Context mContext, int value)
    {
        int calculatedValue = value;
        try
        {
            calculatedValue = Math.round(TypedValue.applyDimension(TypedValue.COMPLEX_UNIT_DIP, value, mContext.getResources().getDisplayMetrics()));
        }
        catch (Exception e)
        {
            AppDelegate.LogE(e);
        }
        return calculatedValue;

    }

    public static float pixToDP(Context mContext, int value)
    {
        Resources r = mContext.getResources();
        float calculatedValue = value;
        try
        {
            calculatedValue = TypedValue.applyDimension(TypedValue.COMPLEX_UNIT_DIP, value, r.getDisplayMetrics());
        }
        catch (Exception e)
        {
            AppDelegate.LogE(e);
        }
        return calculatedValue;

    }


    public static int getFileSize(File file)
    {
        try
        {
            return Integer.parseInt(String.valueOf(file.length() / 1024));
        }
        catch (Exception e)
        {
            AppDelegate.LogE(e);
            return 0;
        }
    }


    /**
     * reduces the size of the image
     *
     * @param image
     * @param maxSize
     * @return
     */
    public static Bitmap getResizedBitmap(Bitmap image, int maxSize)
    {
        int width = image.getWidth();
        int height = image.getHeight();

        float bitmapRatio = (float) width / (float) height;
        if (bitmapRatio > 0)
        {
            width = maxSize;
            height = (int) (width / bitmapRatio);
        }
        else
        {
            height = maxSize;
            width = (int) (height * bitmapRatio);
        }
        return Bitmap.createScaledBitmap(image, width, height, true);
    }

    public static Bitmap decodeSampledBitmapFromFile(String path, int reqWidth, int reqHeight)
    { // BEST QUALITY MATCH
        //First decode with inJustDecodeBounds=true to check dimensions
        final BitmapFactory.Options options = new BitmapFactory.Options();
        options.inJustDecodeBounds = true;
        BitmapFactory.decodeFile(path, options);

        // Calculate inSampleSize, Raw height and width of image
        final int height = options.outHeight;
        final int width = options.outWidth;
        options.inPreferredConfig = Bitmap.Config.RGB_565;
        int inSampleSize = 1;

        if (height > reqHeight)
        {
            inSampleSize = Math.round((float) height / (float) reqHeight);
        }
        int expectedWidth = width / inSampleSize;

        if (expectedWidth > reqWidth)
        {
            //if(Math.round((float)width / (float)reqWidth) > inSampleSize) // If bigger SampSize..
            inSampleSize = Math.round((float) width / (float) reqWidth);
        }

        options.inSampleSize = inSampleSize;

        // Decode bitmap with inSampleSize set
        options.inJustDecodeBounds = false;

        return BitmapFactory.decodeFile(path, options);
    }

    public static void setListViewHeightBasedOnChildren(GridView listView, ListAdapter gridAdapter)
    {
        try
        {
            if (gridAdapter == null)
            {
                // pre-condition
                AppDelegate.LogE("Adapter is null");
                ViewGroup.LayoutParams params = listView.getLayoutParams();
                params.height = 80;
                listView.setLayoutParams(params);
                listView.requestLayout();
                return;
            }
            int totalHeight = 0;

            float itemCount = gridAdapter.getCount();
            if (itemCount >= 3)
            {
                itemCount = itemCount / 3;
            }
            else
            {
                itemCount = 1;
            }

            int desiredWidth = View.MeasureSpec.makeMeasureSpec(listView.getWidth(),
                    View.MeasureSpec.AT_MOST);
            for (int i = 0; i < itemCount; i++)
            {
                View listItem = gridAdapter.getView(i, null, listView);
                listItem.measure(desiredWidth, View.MeasureSpec.UNSPECIFIED);
                totalHeight += listItem.getMeasuredHeight();
            }
            ViewGroup.LayoutParams params = listView.getLayoutParams();
            params.height = totalHeight
                    + (listView.getVerticalSpacing() * (gridAdapter.getCount() - 1));
            listView.setLayoutParams(params);
            listView.requestLayout();
        }
        catch (Exception e)
        {
            AppDelegate.LogE(e);
        }
    }

    public static void setGridViewHeight(Context mContext, GridView listView, ListAdapter gridAdapter, View view)
    {
        try
        {
            if (gridAdapter == null)
            {
                // pre-condition
                AppDelegate.LogE("Adapter is null");
                ViewGroup.LayoutParams params = listView.getLayoutParams();
                params.height = 80;
                listView.setLayoutParams(params);
                listView.requestLayout();
                return;
            }
            int totalHeight = 0;

            float itemCount = gridAdapter.getCount();
            float value = itemCount / 2;
            AppDelegate.LogT("height = after device = " + value);

            String string_value = value + "";
            string_value = string_value.substring(string_value.lastIndexOf(".") + 1, string_value.length());
            int intValue = (int) value;
            if (Integer.parseInt(string_value) > 0)
            {
                intValue += 1;
            }
            AppDelegate.LogT("intValue = " + intValue);
            int desiredWidth = View.MeasureSpec.makeMeasureSpec(listView.getWidth(),
                    View.MeasureSpec.AT_MOST);
            for (int i = 0; i < intValue; i++)
            {
                View listItem = gridAdapter.getView(i, null, listView);
                listItem.measure(desiredWidth, View.MeasureSpec.UNSPECIFIED);
                totalHeight += AppDelegate.dpToPix(mContext, 150);
            }
            if (view != null && view.getVisibility() == View.VISIBLE)
            {
                totalHeight += AppDelegate.dpToPix(mContext, 60);
            }
            AppDelegate.LogT("totalHeight = " + totalHeight);
            ViewGroup.LayoutParams params = listView.getLayoutParams();
            params.height = totalHeight + (listView.getVerticalSpacing() * intValue);
            listView.setLayoutParams(params);
            listView.requestLayout();
        }
        catch (Exception e)
        {
            AppDelegate.LogE(e);
        }
    }

    public static void setListViewHeight(Context mContext, ListView listView, ListAdapter gridAdapter, View view)
    {
        try
        {
            if (gridAdapter == null)
            {
                // pre-condition
                AppDelegate.LogE("Adapter is null");
                ViewGroup.LayoutParams params = listView.getLayoutParams();
                params.height = 80;
                listView.setLayoutParams(params);
                listView.requestLayout();
                return;
            }
            int totalHeight = 0;
            int itemCount = gridAdapter.getCount();

            int desiredWidth = View.MeasureSpec.makeMeasureSpec(listView.getWidth(),
                    View.MeasureSpec.AT_MOST);
            for (int i = 0; i < itemCount; i++)
            {
                View listItem = gridAdapter.getView(i, null, listView);
                listItem.measure(desiredWidth, View.MeasureSpec.UNSPECIFIED);
                totalHeight += listItem.getMeasuredHeight();
            }
            if (view != null && view.getVisibility() == View.VISIBLE)
            {
                totalHeight += AppDelegate.dpToPix(mContext, 60);
            }
            ViewGroup.LayoutParams params = listView.getLayoutParams();
            params.height = totalHeight;
            listView.setLayoutParams(params);
            listView.requestLayout();
        }
        catch (Exception e)
        {
            AppDelegate.LogE(e);
        }
    }

    public static void setListViewHeight(Context mContext, ListView listView, ListAdapter gridAdapter)
    {
        try
        {
            if (gridAdapter == null)
            {
                // pre-condition
                AppDelegate.LogE("Adapter is null");
                ViewGroup.LayoutParams params = listView.getLayoutParams();
                params.height = 80;
                listView.setLayoutParams(params);
                listView.requestLayout();
                return;
            }
            int totalHeight = 0;
            int itemCount = gridAdapter.getCount();

            int desiredWidth = View.MeasureSpec.makeMeasureSpec(listView.getWidth(),
                    View.MeasureSpec.AT_MOST);
            for (int i = 0; i < itemCount; i++)
            {
                View listItem = gridAdapter.getView(i, null, listView);
                listItem.measure(desiredWidth, View.MeasureSpec.UNSPECIFIED);
                totalHeight += listItem.getMeasuredHeight();
            }
            ViewGroup.LayoutParams params = listView.getLayoutParams();
            params.height = totalHeight;
            listView.setLayoutParams(params);
            listView.requestLayout();
        }
        catch (Exception e)
        {
            AppDelegate.LogE(e);
        }
    }

    public static void setListViewHeightNull(ListView listView)
    {
        try
        {
            int itemCount = 0;
            ViewGroup.LayoutParams params = listView.getLayoutParams();
            params.height = itemCount;
            listView.setLayoutParams(params);
            listView.requestLayout();
        }
        catch (Exception e)
        {
            AppDelegate.LogE(e);
        }
    }

    public static void setGridViewHeightNull(GridView listView)
    {
        try
        {
            int itemCount = 0;
            ViewGroup.LayoutParams params = listView.getLayoutParams();
            params.height = itemCount;
            listView.setLayoutParams(params);
            listView.requestLayout();
        }
        catch (Exception e)
        {
            AppDelegate.LogE(e);
        }
    }

    public static String getNewFile(Context mContext)
    {
        File directoryFile, capturedFile;
        if (AppDelegate.isSDcardAvailable())
        {
            directoryFile = new File(Environment.getExternalStorageDirectory()
                    + "/" + mContext.getResources().getString(R.string.app_name));
        }
        else
        {
            directoryFile = mContext.getDir(mContext.getResources().getString(R.string.app_name), Context.MODE_PRIVATE);
        }
        if (directoryFile.exists() && directoryFile.isDirectory()
                || directoryFile.mkdirs())
        {
            capturedFile = new File(directoryFile, "Image_" + System.currentTimeMillis()
                    + ".png");
            try
            {
                if (capturedFile.createNewFile())
                {
                    AppDelegate.LogT("File created = "
                            + capturedFile.getAbsolutePath());
                    return capturedFile.getAbsolutePath();
                }
            }
            catch (IOException e)
            {
                AppDelegate.LogE(e);
            }
        }
        AppDelegate.LogE("no file created.");
        return null;
    }

    public static String getDefaultFile(Context mContext)
    {
        File directoryFile, capturedFile;
        if (AppDelegate.isSDcardAvailable())
        {
            directoryFile = new File(Environment.getExternalStorageDirectory()
                    + "/" + mContext.getResources().getString(R.string.app_name));
        }
        else
        {
            directoryFile = mContext.getDir(mContext.getResources().getString(R.string.app_name), Context.MODE_PRIVATE);
        }
        AppDelegate.LogT("getDefaultFile directory Exists.");
        if (directoryFile.exists() && directoryFile.isDirectory()
                || directoryFile.mkdirs())
        {
            AppDelegate.LogT("getDefaultFile directory Exists and file create.");
            capturedFile = new File(directoryFile, "Image_Craftdeal.png");
            try
            {
                if (capturedFile.createNewFile())
                {
                    AppDelegate.LogT("File created = "
                            + capturedFile.getAbsolutePath());
                    return capturedFile.getAbsolutePath();
                }
                else
                {
                    return capturedFile.getAbsolutePath();
                }
            }
            catch (IOException e)
            {
                AppDelegate.LogE(e);
            }
        }
        else
        {
            AppDelegate.LogT("getDefaultFile directory Exists and file not create.");
        }
        AppDelegate.LogE("no file created.");
        return null;
    }

    public static void openGallery(Activity mActivity, int SELECT_PICTURE)
    {
        mActivity.startActivityForResult(new Intent(Intent.ACTION_PICK,
                MediaStore.Images.Media.EXTERNAL_CONTENT_URI), SELECT_PICTURE);
    }

    public static Uri getImageUri(Context inContext, Bitmap inImage)
    {
        ByteArrayOutputStream bytes = new ByteArrayOutputStream();
        inImage.compress(Bitmap.CompressFormat.JPEG, 100, bytes);
        String path = MediaStore.Images.Media.insertImage(inContext.getContentResolver(), inImage, "CropTitle", null);
        return Uri.parse(path);
    }

    public static Uri performCrop(Activity mActivity, Uri picUri)
    {
        Uri cropimageUri = null;
        try
        {
            ContentValues values = new ContentValues();

            values.put(MediaStore.Images.Media.TITLE, "cropuser");

            values.put(MediaStore.Images.Media.DESCRIPTION, "cropuserPic");

            cropimageUri = mActivity.getContentResolver().insert(
                    MediaStore.Images.Media.EXTERNAL_CONTENT_URI, values);

            Intent cropIntent = new Intent("com.android.camera.action.CROP");
            // indicate image type and Uri
            cropIntent.setDataAndType(picUri, "image/*");
            // set crop properties
            cropIntent.putExtra("crop", "true");

            // indicate aspect of desired crop
            cropIntent.putExtra("aspectX", 1);
            cropIntent.putExtra("aspectY", 1);
            // indicate output X and Y
            cropIntent.putExtra("outputX", 1000);
            cropIntent.putExtra("outputY", 1000);


            // retrieve data on return
            cropIntent.putExtra("return-data", false);
//            cropIntent.putExtra(MediaStore.EXTRA_OUTPUT, cropimageUri);
            // start the activity - we handle returning in onActivityResult
            AppDelegate.LogT("at performCrop cropimageUri = " + cropimageUri + ", picUri = " + picUri);
            mActivity.startActivityForResult(cropIntent, AppDelegate.PIC_CROP);
        }
        // respond to users whose devices do not support the crop action
        catch (ActivityNotFoundException anfe)
        {
            // display an error message
            String errorMessage = "Whoops - your device doesn't support the crop action!";
            Toast toast = Toast.makeText(mActivity, errorMessage, Toast.LENGTH_SHORT);
            toast.show();
        }
        return cropimageUri;
    }

    public static String getFilterdUrl(String str_url)
    {
        if (str_url != null && str_url.length() > 0)
        {
            str_url = str_url.replace("[", "%5B");
            str_url = str_url.replace("@", "%40");
            str_url = str_url.replace(" ", "%20");
        }
        return str_url;
    }

    public static void showKeyboard(final Context mContext, final EditText editText)
    {
        if (editText != null && mContext != null)
        {
            editText.postDelayed(new Runnable()
            {
                @Override
                public void run()
                {
                    editText.requestFocus();
                    editText.setFocusable(true);
                    editText.setFocusableInTouchMode(true);
                    InputMethodManager imm = (InputMethodManager) mContext.getSystemService(Context.INPUT_METHOD_SERVICE);
                    imm.showSoftInput(editText, InputMethodManager.SHOW_IMPLICIT);
                }
            }, 100);
        }
    }

    public static LatLng getRoundedLatLng(LatLng arg0)
    {
        LatLng latLng = arg0;
        try
        {
            String str_lat = "", str_lng = "";
            str_lat = arg0.latitude + "";
            str_lng = arg0.longitude + "";
            if (str_lat.length() > 9 && str_lng.length() > 9)
            {
                str_lat = str_lat.substring(0, str_lat.indexOf(".") + 7);
                str_lng = str_lng.substring(0, str_lng.indexOf(".") + 7);
                latLng = new LatLng(Double.parseDouble(str_lat),
                        Double.parseDouble(str_lng));
            }
        }
        catch (Exception e)
        {
            AppDelegate.LogE(e);
            return arg0;
        }
        return latLng;
    }

    public static void disableButton(View view)
    {
        disableButton(view, 200);
    }

    public static void disableButton(final View view, long delayTime)
    {
        view.setEnabled(false);
        view.setClickable(false);
        new Handler().postAtTime(new Runnable()
        {
            @Override
            public void run()
            {
                view.setEnabled(true);
                view.setClickable(true);
            }
        }, delayTime);
    }

    public static String getcurrenttime()
    {
        try
        {
            Date date1 = Calendar.getInstance().getTime();
            DateFormat dateFormater = new SimpleDateFormat("EEE, dd MMM yyyy HH:mm:ss zzz");  //("yyyy-MM-dd HH:mm:ss"); (format)Sat, 16 Jan 2016 11:55:23 GMT

            TimeZone gmtTime = TimeZone.getTimeZone("GMT+00");
            dateFormater.setTimeZone(gmtTime);
            String newgmtdate = dateFormater.format(date1);

            newgmtdate = newgmtdate.substring(0, newgmtdate.indexOf("+") != -1 ? newgmtdate.indexOf("+") : newgmtdate.length());
            Log.d("currentLocalTime", " date " + newgmtdate);

            return newgmtdate;
        }
        catch (Exception e)
        {
            AppDelegate.LogE(e);
        }
        return "";
    }

    public static String getcurrenttime(String timexoneconverteddate, String dateFormat)
    {
        try
        {
            DateFormat inputDateFormat = new SimpleDateFormat(dateFormat);
            Date date1 = Calendar.getInstance().getTime();
            date1 = inputDateFormat.parse(timexoneconverteddate + " " + AppDelegate.getCurrentTime());

            DateFormat dateFormater = new SimpleDateFormat("EEE, dd MMM yyyy HH:mm:ss zzz");  //("yyyy-MM-dd HH:mm:ss"); (format)Sat, 16 Jan 2016 11:55:23 GMT

            TimeZone gmtTime = TimeZone.getTimeZone("GMT");
            AppDelegate.LogT("getcurrenttime gmtTime => " + gmtTime);
            dateFormater.setTimeZone(gmtTime);
            String newgmtdate = dateFormater.format(date1);

            AppDelegate.LogT("getcurrenttime newgmtdate => " + newgmtdate);
            newgmtdate = newgmtdate.substring(0, newgmtdate.indexOf("+") != -1 ? newgmtdate.indexOf("+") : newgmtdate.length());
            AppDelegate.LogT("getcurrenttime currentLocalTime newgmtdate => " + newgmtdate);

            return newgmtdate;
        }
        catch (Exception e)
        {
            AppDelegate.LogE(e);
            return timexoneconverteddate;
        }
    }

    public static int getDeviceWith(Context mContext)
    {
        if (displayMetrics == null)
        {
            displayMetrics = mContext.getResources().getDisplayMetrics();
        }
        int width = displayMetrics.widthPixels;
//        AppDelegate.LogT("getDeviceWidth = " + width);
        return width;
    }

    public static int getDeviceHeight(Context mContext)
    {
        if (displayMetrics == null)
        {
            displayMetrics = mContext.getResources().getDisplayMetrics();
        }
        int height = displayMetrics.heightPixels;
//        AppDelegate.LogT("getDeviceHeight = " + height);
        return height;
    }

    public static Bitmap loadBitmap(Context context, String picName)
    {
        Bitmap b = null;
        FileInputStream fis;
        try
        {
            fis = context.openFileInput(picName);
            b = BitmapFactory.decodeStream(fis);
            fis.close();

        }
        catch (FileNotFoundException e)
        {
            AppDelegate.LogE(e);
        }
        catch (IOException e)
        {
            AppDelegate.LogE(e);
        }
        return b;
    }

    public static void saveFile(Context context, Bitmap b, String picName)
    {
        FileOutputStream fos;
        try
        {
            fos = new FileOutputStream(new File(picName));
            b.compress(Bitmap.CompressFormat.PNG, 100, fos);
            fos.close();
//            AppDelegate.showToast(context, "Image saved BitmapImageViewTarget called " + picName);
        }
        catch (FileNotFoundException e)
        {
            AppDelegate.LogE(e);
        }
        catch (IOException e)
        {
            AppDelegate.LogE(e);
        }
    }

    public static String getValidDatePostFixFormate(String string)
    {
        if (string.endsWith("1") && !string.endsWith("11"))
        {
            return "st";
        }
        else if (string.endsWith("2") && !string.endsWith("12"))
        {
            return "nd";
        }
        else if (string.endsWith("3") && !string.endsWith("13"))
        {
            return "rd";
        }
        else
        {
            return "th";
        }
    }

    public static String getDecodedStringMessage(String string)
    {
        if (string.contains("\\n") || string.contains("\n"))
        {
            string = string.replace("\\n", System.getProperty("line.separator"));
            string = string.replace("\n", System.getProperty("line.separator"));
        }
        string = string.replaceAll("%20", " ");
        string = string.replaceAll("%29", "'");
        return string;
    }

    public static String getEncodedStringMessage(String string)
    {
        if (string.contains(System.getProperty("line.separator")))
        {
            AppDelegate.LogT("list.seperator is present");
            string = string.replaceAll(System.getProperty("line.separator"), "%5Cn");
        }
        string = string.replaceAll(" ", "%20");
        string = string.replaceAll("'", "%29");
        return string;
    }

    public static String getDateForChat(String long_date)
    {
        Calendar calendar = Calendar.getInstance();
        try
        {
            calendar.setTimeInMillis(Long.parseLong(long_date));
        }
        catch (Exception e)
        {
            AppDelegate.LogE(e);
        }
        return new SimpleDateFormat("dd MMMM yyyy").format(calendar.getTime());
    }

    public static String getTimeForChat(String long_date)
    {
        Calendar calendar = Calendar.getInstance();
        try
        {
            calendar.setTimeInMillis(Long.parseLong(long_date));
        }
        catch (Exception e)
        {
            AppDelegate.LogE(e);
        }
        return new SimpleDateFormat("hh:mm aa").format(calendar.getTime());
    }

    public static String getTimeForchat(Date dateObject)
    {
        Calendar now = Calendar.getInstance();
        Calendar then = Calendar.getInstance();
        now.setTime(new Date());
        then.setTime(dateObject);

        /*finding the system time zone and getting the difference in miliseconds*/
        TimeZone tz = TimeZone.getDefault();
        tz.getRawOffset();
        AppDelegate.Log("timeZone", "" + tz.getRawOffset());

        // Get the represented date in milliseconds
        long nowMs = now.getTimeInMillis();
        long thenMs = then.getTimeInMillis();

        // Calculate difference in milliseconds
//        long diff = nowMs - thenMs ;
        long diff = Math.abs(nowMs - (thenMs + tz.getRawOffset()));

        // Calculate difference in seconds
        long diffMinutes = diff / (60 * 1000);
        long diffHours = diff / (60 * 60 * 1000);
        long diffDays = diff / (24 * 60 * 60 * 1000);

//        AppDelegate.LogT("getDateDifference " + then.getTime() + ", diffMinutes = " + diffMinutes + ", diffHours = " + diffHours + ", diffDays = " + diffDays);
        if (diffMinutes < 60)
        {
            if (diffMinutes == 1)
            {
                return diffMinutes + " min";
            }
            else
            {
                return diffMinutes + " mins";
            }
        }
        else if (diffHours < 24)
        {
            if (diffHours == 1)
            {
                return diffHours + " hour";
            }
            else
            {
                return diffHours + " hours";
            }
        }
        else if (diffDays < 30)
        {
            if (diffDays == 1)
            {
                return diffDays + " day";
            }
            else
            {
                return diffDays + " days";
            }
        }
        else
        {
            return "months";
        }
    }

    @Override
    public void onCreate()
    {
        try
        {
            MultiDex.install(this);
        }
        catch (Exception e)
        {
            AppDelegate.LogE(e);
        }
        super.onCreate();
        Fabric.with(this, new Crashlytics());
        mInstance = this;
    }

    protected void attachBaseContext(Context base)
    {
        super.attachBaseContext(base);
        try
        {
            MultiDex.install(this);
        }
        catch (Exception e)
        {
            AppDelegate.LogE(e);
        }
    }

    synchronized public static Tracker getDefaultTracker()
    {
        if (mTracker == null)
        {
            GoogleAnalytics analytics = GoogleAnalytics.getInstance(mInstance);
            mTracker = analytics.newTracker(R.xml.global_tracker);
        }
        return mTracker;
    }

    public static void trackDealNameAndtype(String dealname, int type, String screen_name)
    {
        // [START screen_view_hit]
        mTracker = AppDelegate.getDefaultTracker();

        if (type == DEAL_HERO)
        {
            mTracker.setScreenName(screen_name + "(Deal name:- " + dealname + ", and type:- " + "HERO DEAL)");
            mTracker.send(new HitBuilders.ScreenViewBuilder().setPromotionAction("Deal name:- " + dealname + ", and type:- " + "HERO DEAL").build());
        }
        else
        {
            mTracker.setScreenName(screen_name + "(Deal name:- " + dealname + ", and type:- " + "ADHOC DEAL)");
            mTracker.send(new HitBuilders.ScreenViewBuilder().set("Deal name:- " + dealname, ", and type:- " + "ADHOC DEAL").build());
        }
    }

    public static void trackRedeemDeal(String dealname, int type, String screen_name)
    {
        // [START screen_view_hit]
        mTracker = AppDelegate.getDefaultTracker();
        if (type == DEAL_HERO)
        {
            mTracker.send(new HitBuilders.EventBuilder().setCategory("Deals REDEEM").setAction("REDEEM HERO DEAL").setLabel("Screen name:- " + screen_name + " Deal name:- " + dealname).build());
        }
        else
        {
            mTracker.send(new HitBuilders.EventBuilder().setCategory("Deals REDEEM").setAction("REDEEM ADHOC DEAL").setLabel("Screen name:- " + screen_name + " Deal name:- " + dealname).build());
        }

    }

    public static void trackUpgradeAccount(String username, int userid, String screen_name)
    {
        // [START screen_view_hit]
        mTracker = AppDelegate.getDefaultTracker();
        mTracker.send(new HitBuilders.EventBuilder().setCategory("Upgrade Account").setAction("UPGRADE ACCOUNT").setLabel("Screen name:- " + screen_name + ", User Name:- " + username + ", USER ID:-" + userid).build());
    }

    public static void trackPurchaseDeal(String dealname, String screen_name)
    {
        // [START screen_view_hit]
        mTracker = AppDelegate.getDefaultTracker();
        mTracker.send(new HitBuilders.EventBuilder().setCategory("Deals PURCHASE").setAction("PURCHASE ADHOC DEAL").setLabel("Screen name:- " + screen_name + ", Deal name:- " + dealname).build());


    }

    public static void trackSharedDeal(String dealname, int type, String screen_name)
    {
        // [START screen_view_hit]
        mTracker = AppDelegate.getDefaultTracker();
        if (type == DEAL_HERO)
        {
            mTracker.send(new HitBuilders.EventBuilder().setCategory("Deals Share").setAction("SHARE HERO DEAL").setLabel("Screen name:- " + screen_name + ", Deal name:- " + dealname).build());
        }

//        mTracker.setTitle("Deal name " + dealname + "and type= " + "HERO DEAL");
        else
        {
            mTracker.send(new HitBuilders.EventBuilder().setCategory("Deals Share").setAction("SHARE ADHOC DEAL").setLabel("Screen name:- " + screen_name + ", Deal name:- " + dealname).build());
        }

//        mTracker.setTitle("Deal name " + dealname + "and type= " + "ADHOC DEAL");
//        mTracker.setScreenName("Screen name " + screen_name);
//        mTracker.send(new HitBuilders.EventBuilder().setCategory("Deals").setAction("Share").setLabel().build());
//        // [END screen_view_hit]
    }

    public boolean isValidPassword(String pass2)
    {
        String PASSWORD_PATTERN = "((?=.*\\d)(?=.*[a-z])(?=.*[!@#$%&*]).{6,20})";
        Pattern pattern = Pattern.compile(PASSWORD_PATTERN);
        Matcher matcher = pattern.matcher(pass2);
        return matcher.matches();
    }

    public ArrayList<PostAysnc_Model> setPostParamsSecond(
            ArrayList<PostAysnc_Model> mArrPostParam, String key, Object Value,
            String ParamType)
    {
        PostAysnc_Model mBma_POSTModel = new PostAysnc_Model();
        mBma_POSTModel.setStr_PostParamKey(key);
        mBma_POSTModel.setObj_PostParamValue(Value);
        mBma_POSTModel.setStr_PostParamType(ParamType);
        mArrPostParam.add(mBma_POSTModel);
        return mArrPostParam;
    }

    public ArrayList<PostAysnc_Model> setPostParamsSecond(
            ArrayList<PostAysnc_Model> mArrPostParam, String key, Object Value)
    {
        return setPostParamsSecond(mArrPostParam, key, Value,
                ServerRequestConstants.Key_PostStrValue);
    }

    public static void openURL(Activity mActivity, String str_url)
    {
        try
        {
            if (!AppDelegate.isValidString(str_url))
            {
                AppDelegate.showToast(mActivity, "Website url is null.");
                return;
            }
            if (!str_url.startsWith("http://") && !str_url.startsWith("https://"))
            {
                str_url = "http://" + str_url;
            }
            Intent browserIntent = new Intent(Intent.ACTION_VIEW, Uri.parse(str_url));
            mActivity.startActivity(browserIntent);
        }
        catch (Exception e)
        {
            AppDelegate.LogE(e);
        }
    }

    public static void makeCall(Activity mActivity, String str_url)
    {
        try
        {
            if (mActivity == null || !AppDelegate.isValidString(str_url))
            {
                AppDelegate.showToast(mActivity, "Mobile Number is black.");
                return;
            }
            Intent intent = new Intent(Intent.ACTION_CALL);
            intent.setData(Uri.parse("tel:" + str_url));
            mActivity.startActivity(intent);
        }
        catch (Exception e)
        {
            AppDelegate.LogE(e);
        }
    }

    public static int getIntValue(String string)
    {
        int value = 0;
        try
        {
            value = Integer.parseInt(string);
        }
        catch (Exception e)
        {
            AppDelegate.LogE(e);
        }
        return value;
    }


    public static String getPriceFormatted(int value)
    {
        DecimalFormat formatter = new DecimalFormat("#,###,###");
        return formatter.format(value);
    }

    public static String getDayOfMonthSuffix(final int n)
    {
        if (n >= 11 && n <= 13)
        {
            return "th";
        }
        switch (n % 10)
        {
            case 1:
                return "st";
            case 2:
                return "nd";
            case 3:
                return "rd";
            default:
                return "th";
        }
    }

    public static String getDayOfMonthSuffix(final String string)
    {
        try
        {
            int n = Integer.parseInt(string);
            if (n >= 11 && n <= 13)
            {
                return "th";
            }
            switch (n % 10)
            {
                case 1:
                    return "st";
                case 2:
                    return "nd";
                case 3:
                    return "rd";
                default:
                    return "th";
            }
        }
        catch (Exception e)
        {
            AppDelegate.LogE(e);
        }
        return "th";
    }

    public static Bitmap blurRenderScript(Context context, Bitmap smallBitmap)
    {
        try
        {
            smallBitmap = RGB565toARGB888(smallBitmap);
        }
        catch (Exception e)
        {
            e.printStackTrace();
        }

        Bitmap bitmap = Bitmap.createBitmap(
                smallBitmap.getWidth(), smallBitmap.getHeight(),
                Bitmap.Config.ARGB_8888);

        RenderScript renderScript = RenderScript.create(context);

        Allocation blurInput = Allocation.createFromBitmap(renderScript, smallBitmap);
        Allocation blurOutput = Allocation.createFromBitmap(renderScript, bitmap);

        ScriptIntrinsicBlur blur = ScriptIntrinsicBlur.create(renderScript,
                Element.U8_4(renderScript));
        blur.setInput(blurInput);
        //  blur.setRadius(radius); // radius must be 0 < r <= 25
        blur.forEach(blurOutput);
        blurOutput.copyTo(bitmap);
        renderScript.destroy();

        return bitmap;

    }

    private static Bitmap RGB565toARGB888(Bitmap img) throws Exception
    {
        int numPixels = img.getWidth() * img.getHeight();
        int[] pixels = new int[numPixels];

        //Get JPEG pixels.  Each int is the color values for one pixel.
        img.getPixels(pixels, 0, img.getWidth(), 0, 0, img.getWidth(), img.getHeight());

        //Create a Bitmap of the appropriate format.
        Bitmap result = Bitmap.createBitmap(img.getWidth(), img.getHeight(), Bitmap.Config.ARGB_8888);

        //Set RGB pixels.
        result.setPixels(pixels, 0, result.getWidth(), 0, 0, result.getWidth(), result.getHeight());
        return result;
    }

    public static boolean isLegalPassword(String pass)
    {
        if (pass.length() < 6 || pass.length() > 20)
        {
            return false;
        }
        if (!pass.matches(".*[A-Z].*"))
        {
            return false;
        }

        if (!pass.matches(".*[a-z].*"))
        {
            return false;
        }

        if (!pass.matches(".*\\d.*"))
        {
            return false;
        }

        // if (!pass.matches(".*[~!.......].*")) return false;

        return true;
    }

    public static void showDialog_okCancel(Context context, final String Title, final String id, final String id2, final OnDialogClickListener onDialogClickListener)
    {
        if (builder != null && builder.isShowing())
        {
            builder.dismiss();
        }
        builder = new Dialog(context);
        builder.requestWindowFeature(Window.FEATURE_NO_TITLE);
        builder.setContentView(R.layout.dialog_twobuttons);
        builder.getWindow().setBackgroundDrawable(new ColorDrawable(android.graphics.Color.TRANSPARENT));
        carbon.widget.TextView ok, cancel, title;
        ok = (carbon.widget.TextView) builder.findViewById(R.id.ok);
        title = (carbon.widget.TextView) builder.findViewById(R.id.title);
        cancel = (carbon.widget.TextView) builder.findViewById(R.id.cancel);
        title.setText(Title);
        builder.show();
        cancel.setOnClickListener(new View.OnClickListener()
        {
            @Override
            public void onClick(View v)
            {
                // onDialogClickListener.setOnDialogClickListener(id2);
                builder.dismiss();
            }
        });
        ok.setOnClickListener(new View.OnClickListener()
        {
            @Override
            public void onClick(View v)
            {
                onDialogClickListener.setOnDialogClickListener(id);
                builder.dismiss();
            }
        });

    }

    public static void showAnimatedProgressDialog(Context context)
    {
        if (builder != null && builder.isShowing())
        {
            builder.dismiss();
        }
        builder = new Dialog(context);
        builder.requestWindowFeature(Window.FEATURE_NO_TITLE);
        builder.setContentView(R.layout.progress_dialog);
        builder.getWindow().setBackgroundDrawable(new ColorDrawable(android.graphics.Color.TRANSPARENT));
        ImageView img_c_loading1;
        img_c_loading1 = (ImageView) builder.findViewById(R.id.img_c_loading1);
        img_c_loading1.setVisibility(View.VISIBLE);
        AnimationDrawable frameAnimation = (AnimationDrawable) img_c_loading1.getDrawable();
        frameAnimation.setCallback(img_c_loading1);
        frameAnimation.setVisible(true, true);
        frameAnimation.start();
        builder.show();
    }

    public static void hideAnimatedProgressDialog(Context mContext)
    {
        try
        {
            if (mContext != null)
            {
                if (builder != null && builder.isShowing())
                {
                    builder.dismiss();
                }
                else
                {
                    builder = new ProgressDialog(mContext);
                    builder.dismiss();
                }
            }
            else
            {
            }
        }
        catch (Exception e)
        {
            AppDelegate.LogE(e);
        }
    }

//    public void showNewDealNotification(Map<String, String> bundle) {
//        try {
////            NotificationModel notificationModel = null;
////            notificationModel = getNotificationModel(bundle);
////            store_intoRealm(notificationModel);
//            showPugNewDealNotification(this, notificationModel);
////            Notification.Builder notificationBuilder = null;
////            if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.JELLY_BEAN) {
////                notificationBuilder = new Notification.Builder(this)
////                        .setSmallIcon(R.drawable.logo)
////                        .setPriority(Notification.PRIORITY_DEFAULT)
////                        .setContentTitle(this.getString(R.string.app_name))
////                        .setStyle(new Notification.BigTextStyle().bigText(notificationModel.getMessage()))
////                        .setContentText(notificationModel.getMessage());
////            } else {
////                notificationBuilder = new Notification.Builder(this)
////                        .setSmallIcon(R.drawable.logo)
////                        .setContentTitle(this.getString(R.string.app_name))
////                        .setContentText(notificationModel.getMessage());
////            }
////            Intent intent;
////            if (notificationModel.getDeal_type().equalsIgnoreCase("1")) {
////                intent = new Intent(this, AdHocDealDetailActivity.class);
////                try {
////                    intent.putExtra(Tags.deal_id, Integer.parseInt(notificationModel.getDeal_id()));
////                    intent.putExtra(Tags.realm_index_id, notificationModel.getId());
////                } catch (Exception e) {
////                    AppDelegate.LogE(e);
////                }
////            } else {
////                intent = new Intent(this, VendorProfileActivity.class);
////                try {
////                    intent.putExtra(Tags.deal_id, Integer.parseInt(notificationModel.getDeal_id()));
////                    intent.putExtra(Tags.vendor_id, Integer.parseInt(notificationModel.getVendor_id()));
////                    intent.putExtra(Tags.realm_index_id, notificationModel.getId());
////                } catch (Exception e) {
////                    AppDelegate.LogE(e);
////                }
////            }
////            intent.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK | Intent.FLAG_ACTIVITY_CLEAR_TOP);
////
////            PendingIntent fullScreenPendingIntent = PendingIntent.getActivity(this, 0, intent, PendingIntent.FLAG_UPDATE_CURRENT);
////            notificationBuilder.setContentIntent(fullScreenPendingIntent);
////            Notification notification = null;
////            if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.LOLLIPOP) {
////                notificationBuilder.setCategory(Notification.CATEGORY_MESSAGE).setFullScreenIntent(fullScreenPendingIntent, true);
////                notification = notificationBuilder.build();
////            } else if (Build.VERSION.SDK_INT < Build.VERSION_CODES.JELLY_BEAN) {
////                notification = new Notification(R.mipmap.ic_launcher,
////                        notificationModel.getMessage(), System.currentTimeMillis());
////            } else {
////                notification = notificationBuilder.build();
////            }
////            notification.defaults = Notification.DEFAULT_SOUND | Notification.DEFAULT_VIBRATE;
////            notification.flags |= Notification.FLAG_AUTO_CANCEL;
////            ((NotificationManager) this.getSystemService(Context.NOTIFICATION_SERVICE)).notify(getRandomNumer(), notification);
//        } catch (Exception e) {
//            AppDelegate.LogE(e);
//        }
//    }
/*
synchronized public Tracker getDefaultTracker() {
    if (mTracker == null) {
        GoogleAnalytics analytics = GoogleAnalytics.getInstance(this);
        // To enable debug logging use: adb shell setprop log.tag.GAv4 DEBUG
        mTracker = analytics.newTracker(R.xml.global_tracker);
    }
    return mTracker;
}
*/


    public static void setStictModePermission()
    {
        StrictMode.setThreadPolicy(new StrictMode.ThreadPolicy.Builder().permitAll().build());
    }

    public static double roundOff(double value)
    {
        long factor = (long) Math.pow(10, 2);
        value = value * factor;
        long tmp = Math.round(value);
        return (double) tmp / factor;
    }

}
