package com.craftedeals.interfaces;

/**
 * Created by Bharat on 07/21/2016.
 */
public interface OnLoadMoreListener {
    void onLoadMore();
}
