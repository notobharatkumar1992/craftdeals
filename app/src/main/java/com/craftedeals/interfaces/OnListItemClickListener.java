package com.craftedeals.interfaces;

/**
 * Created by bharat on 26/12/15.
 */
public interface OnListItemClickListener {

    public void setOnListItemClickListener(String name, int position);
    public void setOnListItemClickListener(String name, int position,boolean like_dislike);
}
