package com.craftedeals.interfaces;

import android.os.Bundle;

/**
 * Created by parul on 3/2/16.
 */
public interface FacebookData {

    public void onFacebookResult(String name, Bundle bundle);

}
