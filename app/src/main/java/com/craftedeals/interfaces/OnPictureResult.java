package com.craftedeals.interfaces;


import android.net.Uri;

public interface OnPictureResult {
    public void setOnReceivePictureResult(String apiName, Uri picUri);
}
