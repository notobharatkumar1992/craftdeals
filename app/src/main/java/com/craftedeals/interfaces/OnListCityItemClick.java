package com.craftedeals.interfaces;

import com.craftedeals.Models.CityModel;

/**
 * Created by Heena on 25-Nov-16.
 */

public interface OnListCityItemClick {

    public void setOnListItemClickListener(String name, int position, CityModel cityModel);
}
