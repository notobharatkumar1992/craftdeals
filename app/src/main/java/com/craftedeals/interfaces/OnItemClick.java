package com.craftedeals.interfaces;

import com.craftedeals.Models.CityModel;
import com.craftedeals.Models.ItemsModel;

import java.util.ArrayList;

/**
 * Created by Heena on 25-Nov-16.
 */

public interface OnItemClick {

    public void setOnItemClickListener(String name, int position, ArrayList<ItemsModel> itemsModelArrayList);
}
