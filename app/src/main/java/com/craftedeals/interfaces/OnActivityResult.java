package com.craftedeals.interfaces;

import android.content.Intent;

public interface OnActivityResult {

    public void onUserDefinedActivityResult(int requestCode, int resultCode, Intent data);

}