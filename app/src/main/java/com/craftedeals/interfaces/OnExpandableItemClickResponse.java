package com.craftedeals.interfaces;

public interface OnExpandableItemClickResponse {

    public void setOnGroupItemClick(String tag, int position);

    public void setOnChildItemClick(String tag, int group_position,
                                    int child_position);

}
