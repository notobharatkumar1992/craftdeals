package com.craftedeals.interfaces;

import com.craftedeals.Models.Place;
import com.google.android.gms.maps.model.LatLng;

public interface GetLocationResult {

    public void onReciveApiResult(String ApiName, String ActionName, LatLng latLng);

    public void onReciveApiResult(String ApiName, String ActionName, Place placeDetail);

}
