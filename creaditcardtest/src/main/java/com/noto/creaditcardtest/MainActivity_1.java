package com.noto.creaditcardtest;

import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v7.app.AppCompatActivity;
import android.util.Log;
import android.view.View;
import android.widget.EditText;
import android.widget.Toast;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by Juned on 13-Sep-17.
 */

public class MainActivity_1 extends AppCompatActivity
{
    EditText editText;
    int number = 121231145;
    int counter = 0;

    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState)
    {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main_1);
        editText = findViewById(R.id.editText);
    }

    public void onClickCount(View view)
    {
        int integer = number;

        int num = Integer.parseInt(editText.getText().toString());
        Log.e("find", num + "");

        while (integer != 0)
        {
            int mod = (int) (integer % Math.pow(10, editText.getText().toString().length()));
            integer = (int) (integer / Math.pow(10, editText.getText().toString().length()));
            Log.e("number", integer + "");
            Log.e("mod", mod + "");
            if (num == mod)
            {
                counter++;
            }
            else
            {
                Log.e("true", swapCheck(num, mod) + "");
                if (swapCheck(num, mod))
                {
                    counter++;
                }
            }
        }

        Toast.makeText(MainActivity_1.this, counter + "", Toast.LENGTH_SHORT).show();
        counter = 0;
    }

    private boolean swapCheck(int num, int mod)
    {
        String modStr = mod + "";
        String numStr = num + "";
        String reverse = new StringBuffer(numStr).reverse().toString();
        if (modStr.equals(reverse))
        {
            return true;
        }
        else
        {
            return false;
        }
    }

    private List<String> getParts(String string, int partitionSize, String find)
    {
        ArrayList<String> parts = new ArrayList<>();
        int len = string.length();
        for (int i = 0; i < len; i += partitionSize)
        {
            parts.add(string.substring(i, Math.min(len, i + partitionSize)));
        }

        for (String as : parts)
        {
            Log.e("char", as);
            if (as.contains(find))
            {
                counter++;
            }
        }

        return parts;
    }
}